# Contributing

Thank you for your interest and effort in contributing to the EuroDaT platform. We welcome and appreciate your support.

You can report bugs by
[opening an issue](https://gitlab.com/eurodat/trustee-platform/-/issues/new?issuable_template=bug).
Please use the provided issue template for bug reports.

In order to make a contribution, please follow the steps outlined in this guideline:

- Mail us at <a href="mailto:contributions@eurodat.org">contributions@eurodat.org</a> stating your interest in contributing and whether you are contributing as an individual or as an entity.
- We shall then start the DocuSign process based on the [individual](clas/individual-contributor-license-agreement.pdf) or [entity](clas/entity-contributor-license-agreement.pdf) Contributor License Agreement (CLA), respectively.
- Once the CLA is signed and counter-signed, fork the project, create your contribution, and make a merge request.

Please be mindful that the final word on features and implementation details remains on our side. To avoid work on your side that we cannot integrate, please align with us upfront on your plans.
