import { Injectable, inject } from '@angular/core';
import Keycloak from 'keycloak-js';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    private readonly keycloak = inject(Keycloak);

    roles: string[] = [];

    async register() {
        await this.keycloak.register({
            redirectUri: window.location.origin,
        });
    }

    async login() {
        await this.keycloak.login({
            redirectUri: window.location.origin,
        });
    }

    async logout() {
        await this.keycloak.logout({
            redirectUri: window.location.origin,
        });
    }

    get userLoggedIn() {
        return this.keycloak.authenticated;
    }
}
