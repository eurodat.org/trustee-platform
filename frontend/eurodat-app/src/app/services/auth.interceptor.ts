import { Injectable, inject } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import Keycloak from 'keycloak-js';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    private keycloak = inject(Keycloak);

    intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        if (this.keycloak.authenticated) {
            const token = this.keycloak.token;
            if (token) {
                const cloned = req.clone({
                    setHeaders: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                return next.handle(cloned);
            }
        }
        return next.handle(req);
    }
}
