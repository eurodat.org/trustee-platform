import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class HeadingService {
    private headingSource = new BehaviorSubject<string>('Default Heading');
    currentHeading = this.headingSource.asObservable();

    setHeading(heading: string) {
        this.headingSource.next(heading);
    }
}
