import { Injectable, Injector, inject } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, catchError, Observable, of, switchMap, tap, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalItemComponent } from '../common/modal-item/modal-item.component';
import { ConfirmModalComponent } from '../common/confirm-modal/confirm-modal.component';

@Injectable()
export class ContractService {
    private http = inject(HttpClient);
    private injector = inject(Injector);

    contractServiceApiUrl = `${environment.clusterUrl}api/v1/contract-service/contracts`;
    private _contracts = new BehaviorSubject<Contract[]>([]);
    public contracts$ = this._contracts.asObservable();

    private handleError(error: HttpErrorResponse, message: string, customClass?: string) {
        console.error('HTTP error occurred:', error);
        const modalService = this.injector.get(NgbModal);
        const modalRef = modalService.open(ModalItemComponent, {});
        modalRef.componentInstance.title = 'An unexpected error has occurred';
        modalRef.componentInstance.content = message;
        modalRef.componentInstance.customClass = () => customClass;
        return throwError(() => new Error(error.message));
    }

    private handleSuccess(message: string, customClass?: string, onCloseCallback?: () => void): void {
        const modalService = this.injector.get(NgbModal);
        const modalRef = modalService.open(ModalItemComponent, {});
        modalRef.componentInstance.title = 'Your request was successful!';
        modalRef.componentInstance.content = message;
        modalRef.componentInstance.customClass = () => customClass;
        modalRef.result.then(
            () => {
                if (onCloseCallback) {
                    onCloseCallback();
                }
            },
            () => {
                if (onCloseCallback) {
                    onCloseCallback();
                }
            },
        );
    }

    /** POST: add the company registration contract */
    addCompanyRegistration(): Observable<Contract> {
        const contractRequest: ContractRequest = { contractType: 'ONBOARDING_COMPANY_MASTERDATA', appId: '' };
        return this.http.post<Contract>(this.contractServiceApiUrl, contractRequest).pipe(
            tap(() => this.handleSuccess('Please check your E-mails for the Registration Contract.', 'alert-success')),
            catchError((error) =>
                this.handleError(
                    error,
                    'We encountered an issue processing your request. Please contact support if the problem persists.',
                    'alert-danger',
                ),
            ),
        );
    }

    /** GET: get all contracts of the user */
    getContractStatus() {
        this.http.get<Contract[]>(`${this.contractServiceApiUrl}`).subscribe((contracts: Contract[]) => this._contracts.next(contracts));
    }

    /** DELETE: terminate the contract and other onboarding-related contracts */
    terminateContract(contract: Contract): Observable<void> {
        return this.openConfirmationModal('Confirm Termination', 'Are you sure you want to terminate this contract?', 'alert-warning').pipe(
            switchMap((confirmed) => {
                if (confirmed) {
                    return this.deleteContract(contract);
                } else {
                    return of(undefined);
                }
            }),
        );
    }

    private openConfirmationModal(title: string, message: string, customClass?: string): Observable<boolean> {
        const modalService = this.injector.get(NgbModal);
        const modalRef = modalService.open(ConfirmModalComponent);
        modalRef.componentInstance.title = title;
        modalRef.componentInstance.message = message;
        modalRef.componentInstance.customClass = () => customClass;

        return new Observable<boolean>((observer) => {
            modalRef.result
                .then(
                    (result) => observer.next(result),
                    () => observer.next(false), // Modal dismissed
                )
                .finally(() => observer.complete());
        });
    }

    private deleteContract(contract: Contract): Observable<void> {
        return this.http.delete<void>(`${this.contractServiceApiUrl}/${contract.id}`).pipe(
            tap(() => {
                this.handleSuccess('Contract successfully terminated.', 'alert-success', () => window.location.reload());
            }),
            catchError((error) =>
                this.handleError(
                    error,
                    'We encountered an issue processing your request. Please contact support if the problem persists.',
                    'alert-danger',
                ),
            ),
        );
    }
}

export interface ContractRequest {
    contractType: string;
    appId?: string;
}

export interface Participant {
    firstName: string;
    lastName: string;
    email: string;
    role: string;
    status: string;
}
export interface Contract {
    id: string;
    contractType: string;
    appId?: string;
    status: string;
    participants?: Participant[];
    companyId?: string;
}
