import { Component, Input } from '@angular/core';
import { NgClass } from '@angular/common';
import { ProgressCard } from '../progress-card/progress-card.component';

@Component({
    selector: 'ead-progress-multiple-bars',
    imports: [NgClass],
    templateUrl: './progress-multiple-bars.component.html',
    styleUrl: './progress-multiple-bars.component.scss',
})
export class ProgressMultipleBarsComponent {
    @Input() progressCards: ProgressCard[] = [];
    @Input() totalTasks: number = 0;

    makeStyle(decimal: number): string {
        return (decimal * 100).toString() + '%';
    }
}
