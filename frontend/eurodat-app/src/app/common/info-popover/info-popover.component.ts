import { Component, AfterViewInit, input } from '@angular/core';
import { Popover } from 'bootstrap';

@Component({
    selector: 'ead-info-popover',
    imports: [],
    templateUrl: './info-popover.component.html',
    styleUrl: './info-popover.component.scss',
})
export class InfoPopoverComponent implements AfterViewInit {
    readonly popoverTitle = input<string>('');
    readonly popoverContent = input<string>('');

    ngAfterViewInit() {
        // Initialize popovers
        const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]');
        Array.from(popoverTriggerList).forEach((popoverTriggerEl) => new Popover(popoverTriggerEl));
    }
}
