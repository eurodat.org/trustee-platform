import { Component, inject, signal } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgClass } from '@angular/common';

@Component({
    selector: 'app-confirm-modal',
    templateUrl: './confirm-modal.component.html',
    styleUrls: ['./confirm-modal.component.scss'],
    imports: [NgClass],
})
export class ConfirmModalComponent {
    activeModal = inject(NgbActiveModal);

    readonly title = signal<string>('Modal Title');
    readonly message = signal<string>('This is a Modal');
    readonly customClass = signal<string>('');
}
