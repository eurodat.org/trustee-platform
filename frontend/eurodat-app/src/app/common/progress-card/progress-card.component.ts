import { Component, input } from '@angular/core';
import { NgClass } from '@angular/common';

@Component({
    selector: 'ead-progress-card',
    imports: [NgClass],
    templateUrl: './progress-card.component.html',
    styleUrl: './progress-card.component.scss',
})
export class ProgressCardComponent {
    readonly progressCard = input<ProgressCard>();

    makeStyle(decimal: number = 0) {
        return (decimal * 100).toString() + '%';
    }
}

export interface ProgressCard {
    title: string;
    statusNumber: number;
    progress: number;
    progressStatus: ProgressStatus;
}

export enum ProgressStatus {
    green = 'success',
    yellow = 'warning',
    red = 'danger',
}
