import { Component, OnInit, input, Input } from '@angular/core';
import { NgClass } from '@angular/common';
import { InfoPopoverComponent } from '../info-popover/info-popover.component';
import { ProgressStatus } from '../progress-card/progress-card.component';

@Component({
    selector: 'ead-accordion-item',
    imports: [InfoPopoverComponent, NgClass],
    templateUrl: './accordion-item.component.html',
    styleUrls: ['./accordion-item.component.scss'],
})
export class AccordionItemComponent implements OnInit {
    readonly title = input<string>('Accordion Item');
    readonly documentStatus = input<ProgressStatus>(ProgressStatus.red);
    readonly popoverContent = input<string>('write your tips here');
    readonly popoverTitle = input<string>('Subcomponent-Tip');
    readonly id = input<string>("'One'");
    readonly customClass = input<string>('');
    readonly isDisabled = input<boolean>(false);
    @Input() isExpanded: string = '';

    headingId: string = '';
    collapseId: string = '';

    ngOnInit() {
        this.headingId = `flush-heading${this.id()}`;
        this.collapseId = `flush-collapse${this.id()}`;
    }
}
