import { Component, inject, input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgClass } from '@angular/common';

@Component({
    selector: 'ead-modal-item',
    templateUrl: './modal-item.component.html',
    styleUrls: ['./modal-item.component.scss'],
    imports: [NgClass],
})
export class ModalItemComponent {
    activeModal = inject(NgbActiveModal);

    title = input<string>('Modal Title');
    content = input<string>('This is a Modal');
    customClass = input<string>('');

    onClose(): void {
        this.activeModal.close('closed');
    }
}
