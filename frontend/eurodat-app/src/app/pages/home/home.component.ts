import { AfterViewInit, Component, OnInit, ChangeDetectorRef, inject } from '@angular/core';
import { Router } from '@angular/router';
import { ProgressCard, ProgressCardComponent, ProgressStatus } from '../../common/progress-card/progress-card.component';
import { HeadingService } from '../../services/heading.service';

@Component({
    selector: 'app-home',
    imports: [ProgressCardComponent],
    templateUrl: './home.component.html',
    styleUrl: './home.component.scss',
})
export class HomeComponent implements AfterViewInit, OnInit {
    private headingService = inject(HeadingService);
    private cdr = inject(ChangeDetectorRef);
    private router = inject(Router);

    title = 'Home';
    registrationStatus: ProgressStatus = ProgressStatus.red;
    onboardingStatus: ProgressStatus = ProgressStatus.red;
    applicationStatus: ProgressStatus = ProgressStatus.red;

    navigateToOnboarding(event: Event) {
        event.stopPropagation();
        this.router.navigate(['/onboarding']).then((r) => console.log('Navigated to onboarding.'));
    }

    navigateToAppCatalog(event: Event) {
        event.stopPropagation();
        this.router.navigate(['/app-catalog']).then((r) => console.log('Navigated to app catalog.'));
    }

    todoCard: ProgressCard = {
        title: 'To Do',
        statusNumber: 3,
        progress: 3 / (1 + 1 + 1 + 1 + 1 + 1),
        progressStatus: ProgressStatus.red,
    };
    inProgressCard: ProgressCard = {
        title: 'In Progress',
        statusNumber: 1,
        progress: 1 / (1 + 1 + 1 + 1 + 1 + 1),
        progressStatus: ProgressStatus.yellow,
    };
    doneCard: ProgressCard = {
        title: 'Done',
        statusNumber: 2,
        progress: 2 / (1 + 1 + 1 + 1 + 1 + 1),
        progressStatus: ProgressStatus.green,
    };

    ngOnInit(): void {
        setTimeout(() => {
            this.headingService.setHeading('EuroDaT Platform Dashboard');
        });
    }

    ngAfterViewInit(): void {
        this.cdr.detectChanges(); // Manually trigger change detection
    }

    progressCards: ProgressCard[] = [this.todoCard, this.inProgressCard, this.doneCard];
    totalTasks: number = this.todoCard.statusNumber + this.inProgressCard.statusNumber + this.doneCard.statusNumber;
}
