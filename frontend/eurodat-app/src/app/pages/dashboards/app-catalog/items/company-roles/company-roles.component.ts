import { Component, inject } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ReactiveFormsModule, ValidationErrors, ValidatorFn } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';
import { NgClass } from '@angular/common';
import { CheckboxModule } from 'primeng/checkbox';
import { InfoPopoverComponent } from '../../../../../common/info-popover/info-popover.component';
import { ThemeProvider } from 'primeng/config';
import Lara from '@primeng/themes/lara';

@Component({
    selector: 'ead-company-roles',
    imports: [ReactiveFormsModule, MultiSelectModule, CheckboxModule, NgClass, InfoPopoverComponent],
    templateUrl: './company-roles.component.html',
    styleUrl: './company-roles.component.scss',
})
export class CompanyRolesComponent {
    private themeProvider = inject(ThemeProvider);

    private _submitted = false;
    modelDefaultValues = {
        dataProvider: false,
        dataServiceProvider: false,
        eurodatApps: [],
    };
    dataProvider = new FormControl(this.modelDefaultValues.dataProvider);
    dataServiceProvider = new FormControl(this.modelDefaultValues.dataServiceProvider);
    eurodatApps = new FormControl<string[]>(this.modelDefaultValues.eurodatApps);
    model = new FormGroup(
        {
            dataProvider: this.dataProvider,
            dataServiceProvider: this.dataServiceProvider,
            eurodatApps: this.eurodatApps,
        },
        { validators: atLeastOneSelectedValidator },
    );
    eurodatAppsOptions = ['safeAML', 'MiNaBo'];

    onSubmit() {
        if (this.model.hasError('atLeastOneSelectedValidator')) {
            return;
        }
        this.submitted = true;
        // here backend api call with the submitted values
    }

    prefillForm() {
        // here backend api call to fill the form with saved values. for now just reset form
        this.model.reset({
            dataProvider: this.modelDefaultValues.dataProvider,
            dataServiceProvider: this.modelDefaultValues.dataServiceProvider,
            eurodatApps: this.modelDefaultValues.eurodatApps,
        });
    }

    set submitted(status: boolean) {
        this._submitted = status;
        this.disableView(status);
    }

    get submitted(): boolean {
        return this._submitted;
    }

    private disableView(status: boolean): void {
        if (status) {
            this.model.disable();
        } else {
            this.model.enable();
        }
    }

    ngOnInit() {
        this.themeProvider.setThemeConfig({ theme: { preset: Lara } });
    }
}

/** An actor's name can't match the actor's role */
export const atLeastOneSelectedValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const dataProvider = control.get('dataProvider')?.value as boolean;
    const dataServiceProvider = control.get('dataServiceProvider')?.value as boolean;
    const eurodatApps = control.get('eurodatApps')?.value as string[];
    const atLeastOneSelected = dataProvider || dataServiceProvider || eurodatApps.length > 0;
    return !atLeastOneSelected ? { noneSelected: true } : null;
};
