import { Component, ChangeDetectorRef, inject } from '@angular/core';
import { HeadingService } from '../../../services/heading.service';
import { AccordionItemComponent } from '../../../common/accordion-item/accordion-item.component';
import { CompanyRolesComponent } from './items/company-roles/company-roles.component';
import { ProgressStatus } from '../../../common/progress-card/progress-card.component';

@Component({
    selector: 'adm-app-catalog',
    imports: [AccordionItemComponent, CompanyRolesComponent],
    templateUrl: './app-catalog.component.html',
    styleUrl: './app-catalog.component.scss',
})
export class AppCatalogComponent {
    private headingService = inject(HeadingService);
    private cdr = inject(ChangeDetectorRef);

    title = 'EuroDaT Platform App Catalogue';
    applicationStatus: ProgressStatus = ProgressStatus.red;
    protected readonly ProgressStatus = ProgressStatus;

    ngOnInit(): void {
        setTimeout(() => {
            this.headingService.setHeading('EuroDaT Platform App Catalogue');
        });
    }

    ngAfterViewInit(): void {
        this.cdr.detectChanges(); // Manually trigger change detection
    }
}
