import { Component, OnInit, inject } from '@angular/core';
import { Contract, ContractService } from '../../../../../services/contract.service';
import { AsyncPipe, LowerCasePipe } from '@angular/common';
import { Observable } from 'rxjs';

@Component({
    selector: 'ead-company-registration',
    imports: [LowerCasePipe, AsyncPipe],
    templateUrl: './company-registration.component.html',
    styleUrls: ['./company-registration.component.scss'],
})
export class CompanyRegistrationComponent implements OnInit {
    private contractService = inject(ContractService);

    loadingSetup: boolean = false;
    loadingTerminate: boolean = false;
    contract: Contract | undefined;
    contracts$: Observable<Contract[]>;
    onboardingSigned: boolean = false;
    termsAndConditionsContract: Contract | undefined;

    constructor() {
        this.contracts$ = this.contractService.contracts$;
    }

    ngOnInit() {
        // Subscribe to the contracts observable to check the onboarding contract status
        this.contracts$.subscribe((contracts) => {
            const onboardingContract = contracts.find((contract) => contract.contractType === 'ONBOARDING_COMPANY_MASTERDATA');
            this.onboardingSigned = !!(onboardingContract && onboardingContract.status === 'SIGNED');
        });
    }

    setUpCompanyClick() {
        this.loadingSetup = true;
        this.contractService.addCompanyRegistration().subscribe({
            next: (data) => {
                this.contract = { ...data };
                this.contractService.getContractStatus();
                this.loadingSetup = false;
            },
            error: (error) => {
                this.loadingSetup = false;
                console.error('Error adding company registration:', error);
            },
        });
    }

    terminateContractClick(contract: Contract) {
        this.loadingTerminate = true;
        this.contractService.terminateContract(contract).subscribe({
            next: () => {
                this.loadingTerminate = false;
                this.termsAndConditionsContract = undefined;
                this.onboardingSigned = false;
            },
            error: (error) => {
                this.loadingTerminate = false;
                console.error('Error terminating contract:', error);
            },
        });
    }

    protected readonly confirm = confirm;
}
