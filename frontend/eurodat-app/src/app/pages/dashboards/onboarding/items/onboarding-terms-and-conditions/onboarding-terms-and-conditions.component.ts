import { Component, OnInit, inject } from '@angular/core';
import { ContractService, Contract, Participant } from '../../../../../services/contract.service';
import { NgClass } from '@angular/common';

@Component({
    selector: 'ead-onboarding-terms-and-conditions',
    imports: [NgClass],
    templateUrl: './onboarding-terms-and-conditions.component.html',
    styleUrls: ['./onboarding-terms-and-conditions.component.scss'],
})
export class OnboardingTermsAndConditionsComponent implements OnInit {
    private contractService = inject(ContractService);

    confirmed = false;
    onboardingSigned = false;
    termsAndConditionsContract: Contract | null = null;
    participants: Participant[] = [];

    ngOnInit() {
        this.contractService.getContractStatus();
        // Fetch all contracts and find the TERMS_AND_CONDITIONS contract
        this.contractService.contracts$.subscribe((contracts: Contract[]) => {
            const termsContract = contracts.find((contract) => contract.contractType.startsWith('TERMS_AND_CONDITIONS'));
            if (termsContract) {
                this.termsAndConditionsContract = termsContract;
                this.participants = termsContract.participants || [];
                this.confirmed = termsContract.status === 'SIGNED';
            } else {
                this.termsAndConditionsContract = null;
                this.participants = [];
                this.confirmed = false;
            }
            const onboardingContract = contracts.find((contract) => contract.contractType.startsWith('ONBOARDING'));
            this.onboardingSigned = onboardingContract?.status === 'SIGNED';
        });
    }
}
