import { AfterViewInit, ChangeDetectorRef, Component, OnInit, inject } from '@angular/core';
import { HeadingService } from '../../../services/heading.service';
import { AccordionItemComponent } from '../../../common/accordion-item/accordion-item.component';
import { CompanyRegistrationComponent } from './items/company-registration/company-registration.component';

import { OnboardingTermsAndConditionsComponent } from './items/onboarding-terms-and-conditions/onboarding-terms-and-conditions.component';
import { ProgressCard, ProgressStatus } from '../../../common/progress-card/progress-card.component';
import { Contract, ContractService } from '../../../services/contract.service';

@Component({
    selector: 'onboarding',
    imports: [AccordionItemComponent, CompanyRegistrationComponent, OnboardingTermsAndConditionsComponent],
    providers: [ContractService],
    templateUrl: './onboarding.component.html',
    styleUrl: './onboarding.component.scss',
})
export class OnboardingComponent implements AfterViewInit, OnInit {
    private headingService = inject(HeadingService);
    private cdr = inject(ChangeDetectorRef);
    private contractService = inject(ContractService);

    title = 'EuroDaT Platform Onboarding';
    registrationStatus: ProgressStatus = ProgressStatus.red;
    onboardingStatus: ProgressStatus = ProgressStatus.red;
    loadingStatus: boolean = false;
    lastUpdate: string = '';
    showUpdateButton: boolean = false;
    contracts: Contract[] = [];

    todoCard: ProgressCard = {
        title: 'To Do',
        statusNumber: 3,
        progress: 3 / (1 + 1 + 1 + 1 + 1 + 1),
        progressStatus: ProgressStatus.red,
    };
    inProgressCard: ProgressCard = {
        title: 'In Progress',
        statusNumber: 1,
        progress: 1 / (1 + 1 + 1 + 1 + 1 + 1),
        progressStatus: ProgressStatus.yellow,
    };
    doneCard: ProgressCard = {
        title: 'Done',
        statusNumber: 2,
        progress: 2 / (1 + 1 + 1 + 1 + 1 + 1),
        progressStatus: ProgressStatus.green,
    };

    progressCards: ProgressCard[] = [this.todoCard, this.inProgressCard, this.doneCard];
    totalTasks: number = this.todoCard.statusNumber + this.inProgressCard.statusNumber + this.doneCard.statusNumber;

    CompanyIsRegistered = false;

    ngOnInit(): void {
        this.getContractStatus();
        setTimeout(() => {
            this.headingService.setHeading('EuroDaT Platform Onboarding');
        });
    }

    ngAfterViewInit(): void {
        this.cdr.detectChanges(); // Manually trigger change detection
    }

    getContractStatus() {
        this.loadingStatus = true;
        setTimeout(() => {
            this.contractService.getContractStatus();
            this.contractService.contracts$.subscribe((contracts: Contract[]) => {
                this.CompanyIsRegistered = contracts.some((contract) => contract.contractType !== 'ONBOARDING_COMPANY_MASTERDATA');
                if (contracts.length !== 0) {
                    this.showUpdateButton = true;
                    this.showLastUpdate();
                } else {
                    this.showUpdateButton = false;
                    this.hideLastUpdate();
                }
                this.loadingStatus = false;
                this.updateProgressColors(contracts);
            });
        }, 250);
    }

    showLastUpdate() {
        const now = new Date();
        const hours = String(now.getHours()).padStart(2, '0');
        const minutes = String(now.getMinutes()).padStart(2, '0');
        this.lastUpdate = `Last update on ${String(now.getDate()).padStart(2, '0')}.${String(now.getMonth() + 1).padStart(2, '0')}.${now.getFullYear()} at ${hours}:${minutes}`;
    }

    hideLastUpdate() {
        this.lastUpdate = '';
    }

    updateProgressColors(contracts: Contract[]) {
        this.contracts = contracts;
        const onBoardingContract = contracts.find((contract) => contract.contractType === 'ONBOARDING_COMPANY_MASTERDATA');
        const termsContract = contracts.find((contract) => contract.contractType.startsWith('TERMS_AND_CONDITIONS'));
        if (onBoardingContract) {
            if (onBoardingContract.status === 'SENT') {
                this.registrationStatus = ProgressStatus.yellow;
            } else if (onBoardingContract.status === 'SIGNED') {
                this.registrationStatus = ProgressStatus.green;
            }
        } else {
            this.registrationStatus = ProgressStatus.red;
            this.onboardingStatus = ProgressStatus.red;
        }
        if (onBoardingContract && termsContract) {
            const allParticipantsSigned = termsContract.participants?.every((participant) => participant.status === 'completed');
            if (allParticipantsSigned) {
                this.onboardingStatus = ProgressStatus.green;
            } else {
                this.onboardingStatus = ProgressStatus.yellow;
            }
        }
    }
}
