import { Component, OnInit, Renderer2, inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { RouterLink, RouterModule, RouterOutlet } from '@angular/router';
import { AuthService } from './services/auth.service';
import { environment } from '../../environments/environment';
import { HeadingService } from './services/heading.service';

@Component({
    selector: 'app-root',
    imports: [RouterOutlet, RouterLink, RouterModule],
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
    private readonly authService: AuthService | null = null;
    private headingService = inject(HeadingService);
    private renderer = inject(Renderer2);
    private _document = inject<Document>(DOCUMENT);

    headingText: string = '';
    title = 'eurodat-app';
    currentYear: number = new Date().getFullYear();
    authenticated: boolean | undefined;

    constructor() {
        if (environment.useKeycloak !== 'noKeycloak') {
            this.authService = inject(AuthService);
        }

        this.loadExternalCSS();
    }

    public loadExternalCSS() {
        const node = this.renderer.createElement('link');
        node.href = environment.eurodatAppCssSource;
        node.rel = 'stylesheet';
        this.renderer.appendChild(this._document.head, node);
    }

    ngOnInit() {
        if (this.authService !== null) {
            this.authenticated = !!this.authService.userLoggedIn;
        } else {
            this.authenticated = true;
        }

        this.headingService.currentHeading.subscribe((heading) => (this.headingText = heading));
    }

    async register() {
        if (this.authService !== null) {
            await this.authService.register();
        }
    }

    async login() {
        if (this.authService !== null) {
            await this.authService.login();
        }
    }

    async logout() {
        if (this.authService !== null) {
            await this.authService.logout();
        }
    }
}
