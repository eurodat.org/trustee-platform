import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';
import { provideKeycloak } from 'keycloak-angular';
import { routes } from './app.routes';
import { environment } from '../../environments/environment';
import { HTTP_INTERCEPTORS, HttpClient, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { provideAnimations } from '@angular/platform-browser/animations';
import { AuthInterceptor } from './services/auth.interceptor';
import { providePrimeNG } from 'primeng/config';

let selectiveConfig: ApplicationConfig;

selectiveConfig = {
    providers: [
        provideRouter(routes),
        HttpClient,
        CommonModule,
        provideAnimations(),
        provideHttpClient(withInterceptorsFromDi()),
        providePrimeNG(),
    ],
};

if (environment.useKeycloak !== 'noKeycloak') {
    selectiveConfig = {
        providers: [
            provideRouter(routes),
            {
                provide: HTTP_INTERCEPTORS,
                useClass: AuthInterceptor,
                multi: true,
            },
            provideKeycloak({
                config: {
                    url: environment.keycloakUrl,
                    realm: environment.keycloakRealm,
                    clientId: environment.keycloakClient,
                },
                initOptions: {
                    checkLoginIframe: false,
                    onLoad: 'login-required',
                },
            }),
            HttpClient,
            CommonModule,
            provideAnimations(),
            provideHttpClient(withInterceptorsFromDi()),
            providePrimeNG(),
        ],
    };
}

export const appConfig = selectiveConfig;
