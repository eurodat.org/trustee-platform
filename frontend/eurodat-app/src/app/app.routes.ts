import { Routes } from '@angular/router';

const possibleRoute: Routes = [
    { path: '', loadComponent: () => import('./pages/home/home.component').then((m) => m.HomeComponent), title: 'EuroDaT Platform Dashboard' },
    {
        path: 'onboarding',
        loadComponent: () => import('./pages/dashboards/onboarding/onboarding.component').then((m) => m.OnboardingComponent),
        title: 'EuroDaT Platform Onboarding',
    },
    {
        path: 'app-catalog',
        loadComponent: () => import('./pages/dashboards/app-catalog/app-catalog.component').then((m) => m.AppCatalogComponent),
        title: 'EuroDaT Platform App Catalogue',
    },
];

export const routes: Routes = possibleRoute;
