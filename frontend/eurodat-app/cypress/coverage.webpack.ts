export default {
    module: {
        rules: [
            {
                test: /\.(ts)$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        plugins: ['babel-plugin-istanbul'],
                    },
                },
                enforce: 'post',
                // eslint-disable-next-line @typescript-eslint/no-var-requires
                include: require('path').join(__dirname, '..', 'src'),
                exclude: [/node_modules/, /cypress/, /(ngfactory|ngstyle)\.js/],
            },
        ],
    },
};
