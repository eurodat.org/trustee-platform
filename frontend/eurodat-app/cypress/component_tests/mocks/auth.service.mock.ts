import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class MockAuthService {
    private isLoggedIn = false;

    register() {
        this.isLoggedIn = true;
    }
    login() {
        this.isLoggedIn = true;
    }

    logout() {
        this.isLoggedIn = false;
    }

    get userLoggedIn() {
        return this.isLoggedIn;
    }
}
