import { MockAuthService } from '../mocks/auth.service.mock';
import { AppComponent } from '../../../src/app/app.component';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { AuthService } from '../../../src/app/services/auth.service';

function mountAppComponent(mockAuthService: MockAuthService) {
    cy.mount(AppComponent, {
        providers: [
            {
                provide: ActivatedRoute,
                useValue: {
                    params: of({ id: 123 }),
                },
            },
            {
                provide: AuthService,
                useValue: mockAuthService,
            },
        ],
    });
}

describe('Component tests for page accessibility', () => {
    const mockAuthService = new MockAuthService();
    beforeEach(() => {
        mountAppComponent(mockAuthService);
    });

    it('Navbar displays Login button for unauthenticated user', () => {
        cy.get('.navbar').contains('Login').as('loginBtn');
        cy.get('@loginBtn').should('be.visible');
        cy.get('@loginBtn').click();
        cy.get('.navbar').contains('Sign-up').as('signUpBtn');
        cy.get('@signUpBtn').should('be.visible');
        cy.get('@signUpBtn').click();
    });

    it('Navbar displays Logout button for authenticated user', () => {
        mountAppComponent(mockAuthService);
        mockAuthService.login();
        cy.get('.navbar').contains('Logout').as('logoutBtn');
        cy.get('@logoutBtn').should('be.visible');
        cy.get('@logoutBtn').click();
    });
});
