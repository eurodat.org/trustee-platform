import { OnboardingTermsAndConditionsComponent } from '../../../src/app/pages/dashboards/onboarding/items/onboarding-terms-and-conditions/onboarding-terms-and-conditions.component';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { Contract, ContractRequest, ContractService } from '../../../src/app/services/contract.service';
import Keycloak from 'keycloak-js';

describe('Test the onboarding-terms-and-conditions component.', () => {
    it('should show the correct participants list when TERMS_AND_CONDITIONS contract is present', () => {
        cy.intercept('GET', '**/contract-service/contracts', {
            statusCode: 200,
            body: [
                {
                    id: '1',
                    contractType: 'TERMS_AND_CONDITIONS',
                    status: 'SENT',
                    participants: [
                        { firstName: 'Dummy1', lastName: 'Test1', email: 'one@example.com', role: 'Geschäftsführer:in', status: 'completed' },
                        { firstName: 'Dummy2', lastName: 'Test2', email: 'two@example.com', role: 'Erb:in', status: 'sent' },
                    ],
                },
                {
                    id: '2',
                    contractType: 'ONBOARDING',
                    status: 'SIGNED',
                    participants: [],
                },
            ],
        }).as('getContracts');

        cy.mount(OnboardingTermsAndConditionsComponent, {
            providers: [provideHttpClient(withInterceptorsFromDi()), { provide: Keycloak, useValue: { authenticated: true } }, ContractService],
        });

        cy.get('p').should('contain.text', 'The Terms and Conditions contract is not yet signed by all participants.');

        cy.get('[data-test="participant-list"]').should('exist');
        cy.get('[data-test="participant-list"]').children().should('have.length', 2);

        // Check that the participants' names and roles are correctly displayed
        cy.get('[data-test="participant-list"]')
            .should('contain.text', 'Dummy1')
            .should('contain.text', 'Test1')
            .should('contain.text', 'Geschäftsführer:in')
            .should('contain.text', 'Signed');
        cy.get('[data-test="participant-list"]')
            .should('contain.text', 'Dummy2')
            .should('contain.text', 'Test2')
            .should('contain.text', 'Erb:in')
            .should('contain.text', 'Pending');

        // Verify that the status icons have the correct classes (success or danger)
        cy.get('[data-test="participant-list"] .border-right').each(($el) => {
            cy.wrap($el)
                .should('have.attr', 'class')
                .and('match', /border-(success|warning)/);
        });
    });

    it('should display the correct message when all participants have signed the TERMS_AND_CONDITIONS contract', () => {
        // Update the mock data to mark all participants as having completed signing
        const updatedContractsData = [
            {
                id: '1',
                contractType: 'TERMS_AND_CONDITIONS',
                status: 'SIGNED',
                participants: [
                    { firstName: 'Dummy1', lastName: 'Test1', email: 'one@example.com', role: 'Geschäftsführer:in', status: 'completed' },
                    { firstName: 'Dummy2', lastName: 'Test2', email: 'two@example.com', role: 'Erb:in', status: 'completed' },
                ],
            },
            {
                id: '2',
                contractType: 'ONBOARDING',
                status: 'SIGNED',
                participants: [],
            },
        ];

        cy.intercept('GET', '**/contract-service/contracts', {
            statusCode: 200,
            body: updatedContractsData,
        }).as('getUpdatedContracts');

        cy.mount(OnboardingTermsAndConditionsComponent, {
            providers: [provideHttpClient(withInterceptorsFromDi()), { provide: Keycloak, useValue: { authenticated: true } }, ContractService],
        });
        cy.wait('@getUpdatedContracts');

        cy.get('p').should('contain.text', 'The Terms and Conditions contract is signed by all participants.');
    });

    it('should show empty participants list if TERMS_AND_CONDITIONS contract is present but participants were not sent', () => {
        cy.intercept('GET', '**/contract-service/contracts', {
            statusCode: 200,
            body: [
                {
                    id: '1',
                    contractType: 'TERMS_AND_CONDITIONS',
                    status: 'SENT',
                },
                {
                    id: '2',
                    contractType: 'ONBOARDING',
                    status: 'SIGNED',
                    participants: [],
                },
            ],
        }).as('getContracts');

        cy.mount(OnboardingTermsAndConditionsComponent, {
            providers: [provideHttpClient(withInterceptorsFromDi()), { provide: Keycloak, useValue: { authenticated: true } }, ContractService],
        });

        cy.get('p').should('contain.text', 'The Terms and Conditions contract is not yet signed by all participants.');
    });

    it('should not display participant list if onboarding is not signed', () => {
        // mark onboarding as not signed
        const updatedContractsData = [
            {
                id: '1',
                contractType: 'TERMS_AND_CONDITIONS',
                status: 'SENT',
                participants: [
                    { firstName: 'Dummy1', lastName: 'Test1', email: 'one@example.com', role: 'Geschäftsführer:in', status: 'completed' },
                    { firstName: 'Dummy2', lastName: 'Test2', email: 'two@example.com', role: 'Erb:in', status: 'sent' },
                ],
            },
            {
                id: '2',
                contractType: 'ONBOARDING',
                status: 'SENT',
                participants: [],
            },
        ];

        cy.intercept('GET', '**/contract-service/contracts', {
            statusCode: 200,
            body: updatedContractsData,
        }).as('getUpdatedContractsWithoutOnboarding');
        cy.mount(OnboardingTermsAndConditionsComponent, {
            providers: [provideHttpClient(withInterceptorsFromDi()), { provide: Keycloak, useValue: { authenticated: true } }, ContractService],
        });
        cy.wait('@getUpdatedContractsWithoutOnboarding');

        cy.get('p').should('not.exist');
    });
});
