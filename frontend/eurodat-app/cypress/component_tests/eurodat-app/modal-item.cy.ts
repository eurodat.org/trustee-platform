import { mount } from 'cypress/angular';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalItemComponent } from '../../../src/app/common/modal-item/modal-item.component';

describe('ModalItemComponent', () => {
    it('should display the title and content', () => {
        mount(ModalItemComponent, {
            componentProperties: {
                title: 'Test Modal Title',
                content: 'This is a test modal content.',
                customClass: 'custom-class',
            },
            providers: [NgbActiveModal],
        });

        cy.get('.modal-header').should('have.class', 'alert custom-class');
        cy.get('.modal-title').should('contain.text', 'Test Modal Title');
        cy.get('.modal-body p').should('contain.text', 'This is a test modal content.');
    });

    it('should close the modal on close button click', () => {
        const onCloseSpy = cy.spy().as('onCloseSpy');

        mount(ModalItemComponent, {
            componentProperties: {
                title: 'Test Modal Title',
                content: 'This is a test modal content.',
                customClass: 'custom-class',
                activeModal: {
                    close: onCloseSpy,
                } as unknown as NgbActiveModal,
            },
            providers: [NgbActiveModal],
        });

        cy.get('.btn-close').as('closeBtn');
        cy.get('@closeBtn').should('exist');
        cy.get('@closeBtn').click();
        cy.get('@onCloseSpy').should('have.been.calledOnce');
    });
});
