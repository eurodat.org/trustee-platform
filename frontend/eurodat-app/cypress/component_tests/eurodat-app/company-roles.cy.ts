import { CompanyRolesComponent } from '../../../src/app/pages/dashboards/app-catalog/items/company-roles/company-roles.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('Test the company-roles component', () => {
    it('When selecting nothing, I expect the validation to give a hint and the submit button to be non-clickable.', () => {
        cy.mount(CompanyRolesComponent, { imports: [NoopAnimationsModule] });
        expectValidationError();
    });
    it('Fill some values in form, submit it, and edit again', () => {
        cy.mount(CompanyRolesComponent, { imports: [NoopAnimationsModule] });
        cy.get('[data-test="dataProvider"]').next('label').should('contain.text', 'Apply for safe deposit box');
        cy.get('[data-test="dataServiceProvider"]').next('label').should('contain.text', 'Apply for EuroDaT application developer');
        cy.get('[data-test="eurodatApps"]').should('contain.text', 'Select EuroDaT Apps');
        checkForValues(false, false, []);
        cy.get('[data-test="dataProvider"]').should('be.visible').click();

        submit();
        expectFormSubmitted();
        checkForValues(true, false, []);

        cy.get('[data-test="edit-button"]').as('editBtn');
        cy.get('@editBtn').should('contain.text', 'Edit');
        cy.get('@editBtn').click();
        checkForValues(true, false, []);
        cy.get('[data-test="dataProvider"]').should('be.visible').click();

        cy.get('[data-test="dataServiceProvider"]').should('be.visible').click();
        cy.get('.p-multiselect').click();
        cy.get('.p-mulitselectitem, [ng-reflect-label="safeAML"]').click();
        cy.get('body').type('{esc}');

        submit();
        expectFormSubmitted();
        checkForValues(false, true, ['safeAML']);

        cy.get('[data-test="edit-button"]').as('editBtn');
        cy.get('@editBtn').should('contain.text', 'Edit');
        cy.get('@editBtn').click();

        cy.get('[data-test="cancel-button"]').as('cancelBtn');
        cy.get('@cancelBtn').should('be.visible');
        cy.get('@cancelBtn').should('contain.text', 'Cancel');
        cy.get('@cancelBtn').click();
        expectValidationError();
        checkForValues(false, false, []);
        cy.get('[data-test="cancel-button"]').should('be.disabled');
    });
});

function submit() {
    cy.get('[data-test="submit-button"]').as('submitBtn');
    cy.get('@submitBtn').should('be.visible');
    cy.get('@submitBtn').should('contain.text', 'Submit');
    cy.get('@submitBtn').click();
    cy.get('[data-test="submit-button"]').should('not.exist');
}

function expectFormSubmitted() {
    cy.get('[data-test="edit-button"]').should('be.visible');
}

function expectValidationError() {
    cy.get('[data-test="submit-button"]').should('be.visible').should('contain.text', 'Submit');
    cy.get('[data-test="edit-button"]').should('not.exist');
    cy.get('[data-test="validation-message"]').should('be.visible');
}

function checkForValues(dataProviderValue: boolean, dataServiceProviderValue: boolean, eurodatAppsValue: string[]) {
    cy.get('[data-test="dataProvider"]')
        .children('.p-checkbox-checked')
        .should(dataProviderValue ? 'exist' : 'not.exist');
    cy.get('[data-test="dataServiceProvider"]')
        .children('.p-checkbox-checked')
        .should(dataServiceProviderValue ? 'exist' : 'not.exist');
    cy.get('[data-test="eurodatApps"]').should('contain.text', eurodatAppsValue.length ? eurodatAppsValue : 'Select EuroDaT');
}
