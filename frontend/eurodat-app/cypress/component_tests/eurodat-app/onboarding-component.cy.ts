import { OnboardingComponent } from '../../../src/app/pages/dashboards/onboarding/onboarding.component';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { Contract, ContractService } from '../../../src/app/services/contract.service';
import Keycloak from 'keycloak-js';
import { OnboardingTermsAndConditionsComponent } from '../../../src/app/pages/dashboards/onboarding/items/onboarding-terms-and-conditions/onboarding-terms-and-conditions.component';

const mockedContract: Contract = {
    id: '1',
    contractType: 'ONBOARDING_COMPANY_MASTERDATA',
    appId: 'TestApp',
    status: 'SENT',
    participants: [],
};

describe('Test the AppComponent component.', () => {
    beforeEach(() => {
        cy.on('uncaught:exception', (err) => {
            return !err.message.includes('500 Internal Server Error');
        });
        const ngOnInitStub = cy.stub(OnboardingComponent.prototype, 'ngOnInit').as('ngOnInitStub');
        cy.stub(OnboardingTermsAndConditionsComponent.prototype, 'ngOnInit').as('ngOnInitStub');

        cy.mount(OnboardingComponent, {
            providers: [
                provideHttpClient(withInterceptorsFromDi()),
                {
                    provide: Keycloak,
                    useValue: { authenticated: true },
                },
                ContractService,
            ],
        }).then((response) => {
            const component = response.component as OnboardingComponent;
            component.showUpdateButton = true;
        });
    });

    it('Check page rendering of static objects', () => {
        cy.contains('Tasks & Status').should('be.visible');

        // Check the status icons classes and configuration
        cy.get('.accordion-button:contains("Company Registration Contract")').should('have.class', 'border-right');
        cy.get('.accordion-button:contains("Onboarding Terms and Conditions")').should('have.class', 'border-right');
    });

    it('Check page rendering with no contracts', () => {
        cy.contains('Tasks & Status').should('be.visible');
        cy.contains('.accordion-button', 'Company Registration Contract').should('not.have.class', 'button-disabled');
        cy.contains('.accordion-button', 'Onboarding Terms and Conditions').should('have.class', 'button-disabled');
    });

    it('Check page rendering with ONBOARDING_COMPANY_MASTERDATA contract', () => {
        cy.intercept('GET', '**/contract-service/contracts', {
            statusCode: 200,
            body: [{ contractType: 'ONBOARDING_COMPANY_MASTERDATA', id: '1', participantId: '1', status: 'SENT', submissionId: '1' }],
        }).as('getContracts');

        cy.contains('Tasks & Status').should('be.visible');

        cy.contains('.accordion-button', 'Company Registration Contract').should('not.have.class', 'button-disabled');
        cy.contains('.accordion-button', 'Onboarding Terms and Conditions').should('have.class', 'button-disabled');
    });

    it('Check page rendering with TERMS_AND_CONDITIONS_2 contract', () => {
        cy.intercept('POST', '**/contract-service/contracts', { statusCode: 200, body: mockedContract }).as('addCompanyRegistration');
        cy.intercept('GET', '**/contract-service/contracts', {
            statusCode: 200,
            body: [
                {
                    id: '1',
                    contractType: 'TERMS_AND_CONDITIONS',
                    status: 'SENT',
                    participants: [
                        { firstName: 'Dummy1', lastName: 'Test1', email: 'one@example.com', role: 'Geschäftsführer:in', status: 'completed' },
                        { firstName: 'Dummy2', lastName: 'Test2', email: 'two@example.com', role: 'Erb:in', status: 'sent' },
                    ],
                },
                {
                    id: '2',
                    contractType: 'ONBOARDING',
                    status: 'SIGNED',
                    participants: [],
                },
            ],
        }).as('getContracts');

        cy.get('[data-test="setup-button"]').as('setupBtn');
        cy.get('@setupBtn').should('be.visible');
        cy.get('@setupBtn').click();
        cy.get('.modal-header .btn-close').as('closeBtn');
        cy.get('@closeBtn').should('be.visible');
        cy.get('@closeBtn').click();
        cy.get('.card-body.p-4.pt-4 [data-test="update-button"]').as('updateBtn');
        cy.get('@updateBtn').should('be.visible');
        cy.get('@updateBtn').click();
        cy.get('[data-test="update-spinner"]').should('be.visible');

        cy.wait('@getContracts');

        cy.contains('Tasks & Status').should('be.visible');

        cy.contains('.accordion-button', 'Company Registration Contract').should('not.have.class', 'button-disabled');
        cy.get('#flush-collapseOne').should('have.class', 'show');
        cy.contains('.accordion-button', 'Onboarding Terms and Conditions').should('not.have.class', 'button-disabled');
        cy.get('#flush-collapseTwo').should('have.class', 'show');
        cy.get('[data-bs-toggle="popover"]').should('exist');
    });
});
