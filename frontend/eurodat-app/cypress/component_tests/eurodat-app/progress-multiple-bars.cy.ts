import { mount } from 'cypress/angular';
import { ProgressMultipleBarsComponent } from '../../../src/app/common/progress-multiple-bars/progress-multiple-bars.component';
import { ProgressCard, ProgressStatus } from '../../../src/app/common/progress-card/progress-card.component';

describe('ProgressMultipleBarsComponent', () => {
    const doneCard: ProgressCard = {
        title: 'Done',
        statusNumber: 4,
        progress: 4 / (4 + 2 + 2),
        progressStatus: ProgressStatus.green,
    };
    const inProgressCard: ProgressCard = {
        title: 'In Progress',
        statusNumber: 2,
        progress: 2 / (4 + 2 + 2),
        progressStatus: ProgressStatus.yellow,
    };
    const todoCard: ProgressCard = {
        title: 'To Do',
        statusNumber: 2,
        progress: 2 / (4 + 2 + 2),
        progressStatus: ProgressStatus.red,
    };

    const progressCards: ProgressCard[] = [doneCard, inProgressCard, todoCard];
    const totalTasks: number = doneCard.statusNumber + inProgressCard.statusNumber + todoCard.statusNumber;

    it('renders progress bars correctly', () => {
        cy.viewport(375, 667);
        mount(ProgressMultipleBarsComponent, {
            componentProperties: {
                progressCards,
                totalTasks,
            },
        });
        cy.get('.progress-bar').should('have.length', progressCards.length);
        cy.get('.progress-bar.status-success')
            .should('have.attr', 'style')
            .and('match', /width: 50%/);
        cy.get('.progress-bar.status-warning')
            .should('have.attr', 'style')
            .and('match', /width: 25%/);
        cy.get('.progress-bar.status-danger').contains(todoCard.statusNumber);
    });

    it('displays check icon when progress is 100%', () => {
        cy.viewport(375, 667);
        mount(ProgressMultipleBarsComponent, {
            componentProperties: {
                progressCards: [doneCard],
                totalTasks: doneCard.statusNumber,
            },
        });
        cy.get('.progress-bar.status-success').find('i.bi-check2-circle').should('exist');
    });

    it('renders error message when totalTasks is 0', () => {
        const componentProperties = {
            progressCards: [],
            totalTasks: 0,
        };
        mount(ProgressMultipleBarsComponent, {
            componentProperties,
        });
        cy.get('.progress.style.status-number').contains('Error: something went wrong.');
    });
});
