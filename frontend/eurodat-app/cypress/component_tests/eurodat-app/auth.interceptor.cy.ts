import { TestBed } from '@angular/core/testing';
import { HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import Keycloak from 'keycloak-js';
import { AuthInterceptor } from '../../../src/app/services/auth.interceptor';

describe('AuthInterceptor', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [AuthInterceptor, { provide: Keycloak, useValue: {} }],
        });
    });
    it('Should successfully handle token', () => {
        TestBed.overrideProvider(Keycloak, { useValue: { authenticated: true, token: 'bearer-token' } });
        const authInterceptor = TestBed.inject(AuthInterceptor);

        const mockRequest = new HttpRequest('GET', '/test');
        const mockHandler: HttpHandler = {
            handle: (req: HttpRequest<unknown>): Observable<HttpEvent<unknown>> => {
                expect(req.headers.get('Authorization')).to.equal('Bearer bearer-token');
                return of({} as HttpEvent<unknown>);
            },
        };
        const clonedRequest = authInterceptor.intercept(mockRequest, mockHandler);
        expect(clonedRequest).to.exist;
        clonedRequest.subscribe();
    });

    it('Should handle request without token', () => {
        TestBed.overrideProvider(Keycloak, { useValue: { authenticated: false } });
        const authInterceptor = TestBed.inject(AuthInterceptor);

        const mockRequest = new HttpRequest('GET', '/test');
        const mockHandler: HttpHandler = {
            handle: (req: HttpRequest<unknown>): Observable<HttpEvent<unknown>> => {
                expect(req.headers.get('Authorization')).to.be.null;
                return of({} as HttpEvent<unknown>);
            },
        };
        const handledRequest = authInterceptor.intercept(mockRequest, mockHandler);
        expect(handledRequest).to.exist;
        handledRequest.subscribe();
    });
});
