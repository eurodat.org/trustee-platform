import { mount } from 'cypress/angular';
import { ProgressCard, ProgressCardComponent, ProgressStatus } from '../../../src/app/common/progress-card/progress-card.component';
import { signal } from '@angular/core';

describe('Test for progressbar functionality', () => {
    it('Check visibility of all components, color and progress', () => {
        const progressCard = signal<ProgressCard>({
            title: 'Done',
            statusNumber: 5,
            progress: 0.75,
            progressStatus: ProgressStatus.green,
        });
        mount(ProgressCardComponent, {
            componentProperties: {
                progressCard: progressCard as any,
            },
        });
        cy.get('.card-title').contains('Done').should('be.visible');
        cy.get('.status-number').contains('5').should('be.visible');
        cy.get('.progress-bar').should('have.class', 'status-success');
        cy.get('.progress-bar').should('have.attr', 'style', 'width: 75%;');
    });

    it('If progress is undefined no progressbar should be visible', () => {
        const progressCard = signal<ProgressCard>({
            title: 'To Do',
            statusNumber: 2,
            progress: 0.25,
            progressStatus: ProgressStatus.red,
        });
        mount(ProgressCardComponent, {
            componentProperties: {
                progressCard: progressCard as any,
            },
        });
        cy.get('.card-title').contains('To Do').should('be.visible');
        cy.get('.status-number').contains('2').should('be.visible');
    });

    it('Check progress bar with zero progress', () => {
        const progressCard = signal<ProgressCard>({
            title: 'In Progress',
            statusNumber: 0,
            progress: 0,
            progressStatus: ProgressStatus.yellow,
        });
        mount(ProgressCardComponent, {
            componentProperties: {
                progressCard: progressCard as any,
            },
        });
        cy.get('.card-title').contains('In Progress').should('be.visible');
        cy.get('.status-number').contains('0').should('be.visible');
        cy.get('.progress-bar').should('have.class', 'status-warning');
        cy.get('.progress-bar').should('have.attr', 'style', 'width: 0%;');
    });

    it('Check makeStyle default parameter value', () => {
        const progressCard = signal<ProgressCard>({
            title: 'Pending',
            statusNumber: 0,
            progress: 0,
            progressStatus: ProgressStatus.yellow,
        });
        mount(ProgressCardComponent, {
            componentProperties: {
                progressCard: progressCard as any,
            },
        }).then(({ fixture }) => {
            const component = fixture.componentInstance;
            const result = component.makeStyle();
            expect(result).to.equal('0%');
        });
    });
});
