import { InfoPopoverComponent } from '../../../src/app/common/info-popover/info-popover.component';
import { mount } from 'cypress/angular';

describe('PopoverComponent', () => {
    it('should render the popover component with the correct attributes', () => {
        mount(InfoPopoverComponent);

        // Check that the popover span is rendered with the correct attributes
        cy.get('span[data-bs-toggle="popover"]').should('exist');
        cy.get('span[data-bs-toggle="popover"]').should('have.attr', 'data-bs-trigger', 'hover focus');
        cy.get('span[data-bs-toggle="popover"]').should('have.attr', 'data-placement', 'left');

        // Check the title and content attributes
        cy.get('span[data-bs-toggle="popover"]').should('have.attr', 'title');
        cy.get('span[data-bs-toggle="popover"]').should('have.attr', 'data-bs-content');

        // Check that the button is rendered and disabled
        cy.get('button.info-button').as('infoBtn');
        cy.get('@infoBtn').should('exist');
        cy.get('@infoBtn').should('be.disabled');
    });
});
