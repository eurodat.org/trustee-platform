import { AuthService } from '../../../src/app/services/auth.service';
import { TestBed } from '@angular/core/testing';
import Keycloak from 'keycloak-js';

describe('Unit test for auth.service', () => {
    let keycloakMock: any;

    beforeEach(() => {
        keycloakMock = {
            authenticated: false,
            login: () => {
                keycloakMock.authenticated = true;
                return Promise.resolve();
            },
            logout: () => {
                keycloakMock.authenticated = false;
                return Promise.resolve();
            },
            register: () => {
                keycloakMock.authenticated = true;
                return Promise.resolve();
            },
        };

        TestBed.overrideProvider(Keycloak, { useValue: keycloakMock });
    });

    it('Check for get logged-in functionality', () => {
        const authService = TestBed.inject(AuthService);
        expect(authService.userLoggedIn).to.equal(false);
    });

    it('Check for login() functionality', () => {
        const authService = TestBed.inject(AuthService);
        expect(authService.userLoggedIn).to.equal(false);

        return authService.login().then(() => {
            expect(authService.userLoggedIn).to.equal(true);
        });
    });

    it('Check for logout() functionality', () => {
        keycloakMock.authenticated = true;
        const authService = TestBed.inject(AuthService);
        expect(authService.userLoggedIn).to.equal(true);

        return authService.logout().then(() => {
            expect(authService.userLoggedIn).to.equal(false);
        });
    });

    it('Check for register() functionality', () => {
        const authService = TestBed.inject(AuthService);

        return authService.register().then(() => {
            expect(authService.userLoggedIn).to.equal(true);
        });
    });
});
