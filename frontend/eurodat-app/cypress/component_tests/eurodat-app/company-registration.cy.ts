import { CompanyRegistrationComponent } from '../../../src/app/pages/dashboards/onboarding/items/company-registration/company-registration.component';
import { Contract, ContractRequest, ContractService } from '../../../src/app/services/contract.service';
import Keycloak from 'keycloak-js';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';

const mockedContract: Contract = {
    id: '1',
    contractType: 'ONBOARDING_COMPANY_MASTERDATA',
    appId: 'TestApp',
    status: 'SENT',
    participants: [],
};

const expectedOnboardingRequestBody: ContractRequest = { contractType: 'ONBOARDING_COMPANY_MASTERDATA', appId: '' };

describe('Test the company-registration component.', () => {
    beforeEach(() => {
        cy.on('uncaught:exception', (err) => {
            return !err.message.includes('500 Internal Server Error');
        });
        cy.mount(CompanyRegistrationComponent, {
            providers: [
                provideHttpClient(withInterceptorsFromDi()),
                {
                    provide: Keycloak,
                    useValue: { authenticated: true },
                },
                ContractService,
            ],
        });
    });

    it('terminateContract makes a DELETE request and shows a success modal on success', () => {
        // Arrange: Mock the GET request that returns the contract list
        cy.intercept('GET', '**/contract-service/contracts', {
            statusCode: 200,
            body: [
                {
                    id: '1',
                    contractType: 'ONBOARDING_COMPANY_MASTERDATA',
                    appId: 'TestApp',
                    status: 'SENT',
                },
            ],
        }).as('getContracts');

        // Arrange: User has pressed the "Set up company" button, triggering a network request.
        cy.intercept('POST', '**/contract-service/contracts', { statusCode: 200, body: mockedContract }).as('addCompanyRegistration');

        // Arrange: Mock the DELETE request to terminate the contract
        cy.intercept('DELETE', '**/contract-service/contracts/1', { statusCode: 200 }).as('terminateContract');

        // Act: Click the setup button.
        cy.get('.body [data-test="setup-button"]').as('setupBtn');
        cy.get('@setupBtn').should('be.visible');
        cy.get('@setupBtn').click();

        // Assert: Verify the contract list is rendered
        cy.get('.body').should('contain.text', 'The document for the company onboarding is sent.');
        cy.get('.modal-header .btn-close').as('closeBtn');
        cy.get('@closeBtn').should('be.visible');
        cy.get('@closeBtn').click();

        // Act: Click the terminate button.
        cy.get('.body [data-test="terminate-onboarding-contract-button"]').as('terminateBtn');
        cy.get('@terminateBtn').should('be.visible');
        cy.get('@terminateBtn').click();

        // Assert: Check that the confirmation modal appears and has the correct text.
        cy.get('.modal-content', { timeout: 10000 }).should('be.visible');
        cy.get('.modal-title').should('contain.text', 'Confirm Termination');
        cy.get('.modal-body p').should('contain.text', 'Are you sure you want to terminate this contract?');

        // Act: Click the "Yes" button in the confirmation modal.
        cy.get('.modal-footer .btn-danger').as('dangerBtn');
        cy.get('@dangerBtn').should('be.visible');
        cy.get('@dangerBtn').click();

        // Assert: Verify DELETE request was made with the right contract ID.
        cy.wait('@terminateContract').its('request.url').should('include', '/contract-service/contracts/1');

        // Assert: Check that the success modal appears and has the correct text.
        cy.get('.modal-content', { timeout: 10000 }).should('be.visible');
        cy.get('.modal-title').should('contain.text', 'Your request was successful!');
        cy.get('.modal-body p').should('contain.text', 'Contract successfully terminated.');
        cy.get('.modal-header .btn-close').should('be.visible');
    });

    it('addCompanyRegistration makes a POST request and shows a success modal on success', () => {
        // Arrange: Mock the GET request that returns the contract list
        cy.intercept('GET', '**/contract-service/contracts', {
            statusCode: 200,
            body: [
                {
                    id: '1',
                    contractType: 'ONBOARDING_COMPANY_MASTERDATA',
                    appId: 'TestApp',
                    status: 'SENT',
                },
            ],
        }).as('getContracts');

        // Arrange: User has pressed the "Set up company" button, triggering a network request.
        cy.intercept('POST', '**/contract-service/contracts', { statusCode: 200, body: mockedContract }).as('addCompanyRegistration');

        // Act: Click the setup button.
        cy.get('.body [data-test="setup-button"]').as('setupBtn');
        cy.get('@setupBtn').should('be.visible');
        cy.get('@setupBtn').click();

        // Assert: Verify POST request was made with the right body.
        cy.wait('@addCompanyRegistration').its('request.body').should('deep.equal', expectedOnboardingRequestBody);

        // Assert: Check that the success modal appears and has the correct text.
        cy.get('.modal-content', { timeout: 10000 }).should('be.visible');
        cy.get('.modal-title').should('contain.text', 'Your request was successful!');
        cy.get('.modal-body p').should('contain.text', 'Please check your E-mails for the Registration Contract.');

        // Assert: After successful POST request, the setup button should disappear.
        cy.get('.body [data-test="setup-button"]').should('not.exist');

        // Assert: The success message appears on the screen.
        cy.get('.body p').should('contain.text', `The document for the company onboarding is ${mockedContract.status.toLowerCase()}.`);
    });

    it('Test for loading indicator of setup button if backend is slow', () => {
        cy.intercept('GET', '**/contract-service/contracts', {
            statusCode: 200,
            body: [
                {
                    id: '1',
                    contractType: 'ONBOARDING_COMPANY_MASTERDATA',
                    appId: 'TestApp',
                    status: 'SENT',
                },
            ],
        }).as('getContracts');
        cy.intercept('POST', '**/contract-service/contracts', {
            statusCode: 200,
            body: mockedContract,
            delay: 1000,
        }).as('DELAYED RESPONSE');
        cy.get('.body [data-test="setup-button"]').as('setupBtn');
        cy.get('@setupBtn').should('be.visible');
        cy.get('@setupBtn').click();
        cy.get('[data-test="setup-spinner"]').should('be.visible');
    });

    it('addCompanyRegistration makes a POST request and shows an error modal on failure', () => {
        // Arrange: User has pressed the "Set up company" button, triggering a network request that will fail.
        cy.intercept('POST', '**/contract-service/contracts', { statusCode: 500 }).as('addCompanyRegistration');

        // Act: Click the button.
        cy.get('.body [data-test="setup-button"]').as('setupBtn');
        cy.get('@setupBtn').should('be.visible');
        cy.get('@setupBtn').click();

        // Assert: Check that the error modal appears and has the correct text.
        cy.get('.modal-content', { timeout: 10000 }).should('be.visible');
        cy.get('.modal-title').should('contain.text', 'An unexpected error has occurred');
        cy.get('.modal-body p').should(
            'contain.text',
            'We encountered an issue processing your request. Please contact support if the problem persists.',
        );
    });

    it('Test for loading indicator of terminate button if backend is slow', () => {
        cy.intercept('GET', '**/contract-service/contracts', {
            statusCode: 200,
            body: [
                {
                    id: '1',
                    contractType: 'ONBOARDING_COMPANY_MASTERDATA',
                    appId: 'TestApp',
                    status: 'SENT',
                },
            ],
        }).as('getContracts');
        cy.intercept('POST', '**/contract-service/contracts', { statusCode: 200, body: mockedContract }).as('addCompanyRegistration');
        cy.intercept('DELETE', '**/contract-service/contracts/1', {
            statusCode: 200,
            body: mockedContract,
            delay: 1000,
        }).as('DELAYED RESPONSE');
        cy.get('.body [data-test="setup-button"]').as('setupBtn');
        cy.get('@setupBtn').should('be.visible');
        cy.get('@setupBtn').click();
        cy.get('.modal-header .btn-close').as('closeBtn');
        cy.get('@closeBtn').should('be.visible');
        cy.get('@closeBtn').click();
        cy.get('.body [data-test="terminate-onboarding-contract-button"]').as('terminateBtn');
        cy.get('@terminateBtn').should('be.visible');
        cy.get('@terminateBtn').click();
        cy.get('[data-test="terminate-spinner"]').should('be.visible');
    });

    it('terminateContract makes a DELETE request and shows an error modal on failure', () => {
        // Arrange: Mock the GET request that returns the contract list
        cy.intercept('GET', '**/contract-service/contracts', {
            statusCode: 200,
            body: [
                {
                    id: '1',
                    contractType: 'ONBOARDING_COMPANY_MASTERDATA',
                    appId: 'TestApp',
                    status: 'SENT',
                },
            ],
        }).as('getContracts');

        // Arrange: User has pressed the "Set up company" button, triggering a network request.
        cy.intercept('POST', '**/contract-service/contracts', { statusCode: 200, body: mockedContract }).as('addCompanyRegistration');

        // Arrange: Mock the DELETE request to terminate the contract
        cy.intercept('DELETE', '**/contract-service/contracts/1', { statusCode: 500 }).as('terminateContract');

        // Act: Click the setup button.
        cy.get('.body [data-test="setup-button"]').as('setupBtn');
        cy.get('@setupBtn').should('be.visible');
        cy.get('@setupBtn').click();

        // Assert: Verify the contract list is rendered
        cy.get('.body').should('contain.text', 'The document for the company onboarding is sent.');
        cy.get('.modal-header .btn-close').as('closeBtn');
        cy.get('@closeBtn').should('be.visible');
        cy.get('@closeBtn').click();

        // Act: Click the terminate button.
        cy.get('.body [data-test="terminate-onboarding-contract-button"]').as('terminateBtn');
        cy.get('@terminateBtn').should('be.visible');
        cy.get('@terminateBtn').click();

        // Act: Click the "Yes" button in the confirmation modal.
        cy.get('.modal-footer .btn-danger').as('dangerBtn');
        cy.get('@dangerBtn').should('be.visible');
        cy.get('@dangerBtn').click();

        // Assert: Verify DELETE request was made with the right contract ID.
        cy.wait('@terminateContract').its('request.url').should('include', '/contract-service/contracts/1');

        // Assert: Check that the success modal appears and has the correct text.
        cy.get('.modal-content', { timeout: 10000 }).should('be.visible');
        cy.get('.modal-title').should('contain.text', 'An unexpected error has occurred');
        cy.get('.modal-body p').should(
            'contain.text',
            'We encountered an issue processing your request. Please contact support if the problem persists.',
        );
    });

    it('should display the onboarding status component when onboardingSigned is true and termsAndConditionsContract is present', () => {
        // Arrange: Mock contracts with a signed onboarding contract
        const signedContract = {
            id: '1',
            contractType: 'ONBOARDING_COMPANY_MASTERDATA',
            appId: 'TestApp',
            status: 'SIGNED',
        };
        const termsContract = {
            id: '2',
            contractType: 'TERMS_AND_CONDITIONS_1',
            appId: 'TestApp',
            status: 'SENT',
            participants: [],
        };

        cy.intercept('GET', '**/contract-service/contracts', {
            statusCode: 200,
            body: [signedContract, termsContract],
        }).as('getContracts');

        // Arrange: User has pressed the "Set up company" button, triggering a network request.
        cy.intercept('POST', '**/contract-service/contracts', { statusCode: 200, body: mockedContract }).as('addCompanyRegistration');

        // Act: Click the setup button.
        cy.get('.body [data-test="setup-button"]').as('setupBtn');
        cy.get('@setupBtn').should('be.visible');
        cy.get('@setupBtn').click();

        // Assert: Verify POST request was made with the right body.
        cy.wait('@addCompanyRegistration');

        // Assert: Verify the contract list is rendered
        cy.get('.body').should('contain.text', 'The document for the company onboarding is signed.');
        cy.get('.modal-header .btn-close').as('closeBtn');
        cy.get('@closeBtn').should('be.visible');
        cy.get('@closeBtn').click();
    });
});
