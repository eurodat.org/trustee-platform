import { provideRouter } from '@angular/router';
import { routes } from '../../../src/app/app.routes';
import { HttpClient, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { provideAnimations } from '@angular/platform-browser/animations';
import { appConfig } from '../../../src/app/app.config';

describe('AppConfig', () => {
    it('Should check that correct providers are set', () => {
        const expectedProviders = [
            provideRouter(routes),
            HttpClient,
            CommonModule,
            provideAnimations(),
            provideHttpClient(withInterceptorsFromDi()),
        ];

        const actualProviders = appConfig.providers;

        expectedProviders.forEach((expectedProvider) => {
            const matchingProvider = actualProviders.find((actualProvider) => {
                return actualProvider.toString() === expectedProvider.toString();
            });
            expect(matchingProvider).to.exist;
        });
    });
});
