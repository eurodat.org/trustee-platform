describe('Setup company, sign terms & conditions and restart onboarding', () => {
    let testOnboardingSubmissionId = 276907;
    let testTnCSubmissionId = 276908;

    function createSubmissionCompletedBody(submissionId: any) {
        return {
            event: 'submission.completed',
            object: {
                completed_at: '2024-07-12T10:49:41.814Z',
                submittable: {
                    submission_id: submissionId,
                },
                aggregated_submit_event_values: [
                    {
                        slug: 'companydirectors_slug',
                        value: ['12345678-ab12-34cd-ef56-123456789abc'],
                        mapped_value: '["12345678-ab12-34cd-ef56-123456789abc"]',
                    },
                    {
                        slug: 'companydirectors_slug.12345678-ab12-34cd-ef56-123456789abc.email_slug',
                        value: 'info+cypress@eurodat.org',
                        mapped_value: 'info+cypress@eurodat.org',
                    },
                    {
                        slug: 'companydirectors_slug.12345678-ab12-34cd-ef56-123456789abc.companydirectorsfirstname_slug',
                        value: 'Cypress',
                        mapped_value: 'Cypress',
                    },
                    {
                        slug: 'companydirectors_slug.12345678-ab12-34cd-ef56-123456789abc.companydirectorslastname_slug',
                        value: 'Tester',
                        mapped_value: 'Tester',
                    },
                    {
                        slug: 'companydirectors_slug.12345678-ab12-34cd-ef56-123456789abc.radio3',
                        mapped_value: 'Partner',
                    },
                    {
                        slug: 'companyname_slug',
                        value: 'Test Company',
                    },
                    {
                        slug: 'companystreet_slug',
                        value: 'Test Street 2',
                        mapped_value: 'Test Street 2',
                    },
                    {
                        slug: 'companyzip_slug',
                        value: '11232',
                    },
                    {
                        slug: 'companytown_slug',
                        value: 'FFM',
                    },
                    {
                        slug: 'companycountry_slug',
                        value: 'asd',
                    },
                    {
                        slug: 'companyregisterlocation_slug',
                        value: 'loc',
                    },
                    {
                        slug: 'companyregisterno_slug',
                        value: 'asdfkblw',
                    },
                    {
                        slug: 'companyvatid_slug',
                        value: 'de123456',
                    },
                ],
            },
        };
    }

    beforeEach(() => {
        cy.visit('/');
        cy.title().should('equal', 'Sign in to eurodat');
        cy.url().should('include', '/auth');
        cy.get('input#username').type('info+cypress@eurodat.org');
        cy.get('input#password').type(Cypress.env('userPassword'));
        cy.get('button#kc-login').click();
        // Visit dashboard
        cy.visit('/onboarding');
        cy.title().should('equal', 'EuroDaT Platform Onboarding');
    });

    it('should successfully terminate existing contract.', () => {
        // Check if the onboarding contract has been already sent, if so, terminate it
        cy.get('body').then(($body) => {
            if ($body.find('[data-test="terminate-onboarding-contract-button"]').length) {
                cy.get('[data-test="terminate-onboarding-contract-button"]').as('terminateBtn');
                cy.get('@terminateBtn').should('be.visible');
                cy.get('@terminateBtn').click();
                cy.get('.modal-footer .btn-danger').as('dangerBtn');
                cy.get('@dangerBtn').should('be.visible');
                cy.get('@dangerBtn').click();
                cy.get('.modal-title').should('include.text', 'Your request was successful!');
                cy.get('.modal-body p').should('include.text', 'Contract successfully terminated.');
                cy.get('.modal-header .btn-close').first().click();
            }
        });

        // Check that the update button and last update are not visible
        cy.get('.card-body.p-4.pt-4 [data-test="update-button"]').should('not.exist');
        cy.get('[data-test="last-update"]').should('not.be.visible');
    });

    it('should successfully send the onboarding contract.', () => {
        // Send the onboarding contract
        cy.get('.body [data-test="setup-button"]').as('setupBtn');
        cy.get('@setupBtn').should('exist');
        cy.get('@setupBtn').click();
        cy.get('.modal-content', { timeout: 10000 }).should('be.visible');
        cy.get('.modal-title').should('include.text', 'Your request was successful!');
        cy.get('.modal-body p').should('include.text', 'Please check your E-mails for the Registration Contract.');
        cy.get('.modal-header .btn-close').as('closeBtn');
        cy.get('@closeBtn').should('be.visible');
        cy.get('@closeBtn').click();
        cy.get('.body [data-test="setup-button"]').should('not.exist');

        // Check that the update button and last update are visible after sending the contract
        cy.get('.card-body.p-4.pt-4 [data-test="update-button"]').should('be.visible');
        cy.get('[data-test="last-update"]').should('be.visible');
    });

    it('should visit again and find the current state', () => {
        // Visit the homepage and then the dashboard again to test ngOnInit and getContractStatus
        cy.visit('/');
        cy.visit('/onboarding');
        cy.get('.card-body.p-4.pt-4 [data-test="update-button"]').should('be.visible');
        cy.get('[data-test="last-update"]').should('be.visible');
    });

    it('should find participants after onboarding contract is signed', () => {
        // Mock external POST request to the /submissions endpoint which is triggered when all onboarding contracts are signed
        cy.request({
            method: 'POST',
            url: `${Cypress.env('apiBaseUrl')}api/v1/contract-service/submissions/?token=${encodeURIComponent(Cypress.env('paperlessToken'))}`,
            headers: {
                'Content-Type': 'application/json',
            },
            body: createSubmissionCompletedBody(testOnboardingSubmissionId),
        }).then((response) => {
            expect(response.status).to.eq(200);
        });
        // After the external POST, verify that the onboarding document is signed
        cy.get('.card-body.p-4.pt-4 [data-test="update-button"]').as('updateBtn');
        cy.get('@updateBtn').should('be.visible');
        cy.get('@updateBtn').click();
        cy.get('.body');
        cy.contains('The document for the company onboarding is signed.').should('be.visible');

        // Check if a participant list is displayed
        cy.get('.accordion-button:contains("Onboarding Terms and Conditions")').should('be.visible');
        cy.get('.accordion-button:contains("Onboarding Terms and Conditions")').should('not.be.disabled');
        cy.contains('The Terms and Conditions contract is not yet signed by all participants.').should('be.visible');

        // Ensure there is at least 1 participant before asserting the number of children
        cy.get('.card-body.p-4.pt-4 [data-test="participant-list"]').children().should('have.length', 1);
    });

    it('should find updated participant status after Terms and Conditions is signed', () => {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('apiBaseUrl')}api/v1/contract-service/submissions/?token=${encodeURIComponent(Cypress.env('paperlessToken'))}`,
            headers: {
                'Content-Type': 'application/json',
            },
            body: createSubmissionCompletedBody(testTnCSubmissionId),
        }).then((response) => {
            expect(response.status).to.eq(200);
        });
        // After the external POST, verify that the onboarding document is signed
        cy.get('.card-body.p-4.pt-4 [data-test="update-button"]').as('updateBtn');
        cy.get('@updateBtn').should('be.visible');
        cy.get('@updateBtn').click();
        // Check if a participant list is displayed
        cy.get('.accordion-button:contains("Onboarding Terms and Conditions")').should('be.visible');
        cy.get('.accordion-button:contains("Onboarding Terms and Conditions")').should('not.be.disabled');
        cy.contains('The Terms and Conditions contract is signed by all participants.').should('be.visible');
    });

    it('should terminate all contracts', () => {
        // Terminate the onboarding contract and with that all valid contracts associated with the admin
        cy.get('.body [data-test="terminate-onboarding-contract-button"]').as('terminateBtn');
        cy.get('@terminateBtn').should('exist');
        cy.get('@terminateBtn').click();
        cy.get('.modal-footer .btn-danger').as('dangerBtn');
        cy.get('@dangerBtn').should('be.visible');
        cy.get('@dangerBtn').click();
        cy.get('.modal-content', { timeout: 10000 }).should('be.visible');
        cy.get('.modal-title').should('include.text', 'Your request was successful!');
        cy.get('.modal-body p').should('include.text', 'Contract successfully terminated.');
        cy.get('.modal-header .btn-close').first().click();
        cy.get('.body [data-test="setup-button"]').should('exist');

        // Check that the update button and last update are not visible after terminating the contract
        cy.get('.card-body.p-4.pt-4 [data-test="update-button"]').should('not.exist');
        cy.get('[data-test="last-update"]').should('not.be.visible');
    });

    it('Check if contracts have been terminated correctly', () => {
        cy.visit('/');
        cy.visit('/onboarding');
        // Check if the onboarding contract has been terminated, if not, terminate it
        cy.get('body').then(($body) => {
            if ($body.find('[data-test="terminate-onboarding-contract-button"]').length) {
                cy.get('[data-test="terminate-onboarding-contract-button"]').as('terminateBtn');
                cy.get('@terminateBtn').should('be.visible');
                cy.get('@terminateBtn').click();
            }
        });
    });
});
