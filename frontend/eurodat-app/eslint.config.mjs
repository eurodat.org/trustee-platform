// @ts-check

import eslint from "@eslint/js";
import prettierConfig from "eslint-config-prettier";
import globals from "globals";
import { FlatCompat } from "@eslint/eslintrc";

const compat = new FlatCompat();

export default [
    {
        ignores: [".angular/", "node_modules/", "coverage-component/", "dist/"],
    },
    {
        languageOptions: {
            globals: {
                ...globals.browser,
                window: "readonly",
            },
        },
    },
    ...compat.config(eslint.configs.recommended),
    ...compat.config(prettierConfig),
];
