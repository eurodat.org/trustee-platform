export const environment = {
    clusterUrl: 'https://YOUR_FQDN/', // This URL must be ended with an slash
    keycloakUrl: 'https://YOUR_FQDN/auth',
    keycloakRealm: 'eurodat',
    keycloakClient: 'eurodat-app',
    useKeycloak: '', // set to noKeycloak in order to disable keycloak entirely
    eurodatAppCssSource: 'https://eurodat.org/fileadmin/templates/css/styles.css',
};
