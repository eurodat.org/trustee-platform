export const environment = {
    clusterUrl: (window as never)['env']['clusterUrl'],
    keycloakUrl: (window as never)['env']['keycloakUrl'],
    keycloakRealm: (window as never)['env']['keycloakRealm'],
    keycloakClient: (window as never)['env']['keycloakClient'],
    useKeycloak: (window as never)['env']['useKeycloak'], // set to noKeycloak in order to disable keycloak entirely
    eurodatAppCssSource: (window as never)['env']['eurodatAppCssSource'],
};
