import { defineConfig } from 'cypress';
import coverageWebpack from './cypress/coverage.webpack';
import { environment } from './environments/environment.standalone';

export default defineConfig({
    viewportWidth: 1200,
    viewportHeight: 1000,
    e2e: {
        baseUrl: environment.clusterUrl,
        video: false,
        reporter: 'junit',
        reporterOptions: {
            mochaFile: 'results/cypress-e2e.xml',
            toConsole: true,
        },
    },
    component: {
        devServer: {
            framework: 'angular',
            bundler: 'webpack',
            webpackConfig: coverageWebpack,
        },
        reporter: 'junit',
        reporterOptions: {
            mochaFile: 'results/cypress-component.xml',
            toConsole: true,
        },
        specPattern: 'cypress/component_tests/**/*.cy.ts',
        setupNodeEvents(on, config) {
            // eslint-disable-next-line @typescript-eslint/no-var-requires
            require('@cypress/code-coverage/task')(on, config);
            return config;
        },
    },
    env: {
        codeCoverage: {
            exclude: ['**/cypress/**.*'],
        },
        userPassword: '',
        paperlessToken: '',
        apiBaseUrl: environment.clusterUrl,
    },
});
