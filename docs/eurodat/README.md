---
hide:
  - toc
---

# Getting Started

We compiled comprehensive documentation on how to deploy and operate an own EuroDaT instance and how you can take part
in the development of EuroDaT.

## Connect to EuroDaT APIs

To connect to the EuroDaT platform, learn how to use the [EuroDaT APIs](../eurodat-api-integration/README.md) for integration. These APIs enable management of transactions, workflows, and data storage.

## For Developers

As a developer, you probably want to have a EuroDaT instance running on your local machine for easy development and
debugging. Learn about how to [build and run EuroDaT locally](local-development/README.md), starting with nothing but a fresh clone
of the repository.

### Requirements for Development

These are essential or strongly recommended if you want to take part in the development of EuroDaT:

- Java 17
- Kotlin 1.7
- Git
- Git Bash (if on Windows)
- [Docker Desktop :material-launch:](https://www.docker.com/products/docker-desktop) (with WSL2 if on Windows)
- [kubectl :material-launch:](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [Helm :material-launch:](https://helm.sh/docs/intro/install/)
- [terraform :material-launch:](https://www.hashicorp.com/products/terraform)

Check out our [tools](../tools/README.md) chapter for tips and tricks for various tools we use.

### Bugs

Please report bugs or request a feature in our [gitlab issues :material-launch:](https://gitlab.com/eurodat/trustee-platform/-/issues).
