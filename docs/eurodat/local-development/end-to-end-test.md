# Run End-to-End Tests

The End-to-End (e2e) tests are testing our platform from the external
point of view.
Therefore, to execute the e2e tests, you need a running instance of
the platform.

## Run the e2e test

- Ensure that your kubeconfig includes your platform cluster context.
- Export the IMG_TAG environment variable to specify the image that should
  be pulled for the workflow images or use the `-a` option in the script,
  which automatically exports the tag for your current local git branch.
- You have to provide a docker registry with the `-i` option where the
  images will be pushed to and pulled from. You can also provide credentials
  with `-u` and `-p`.
- Run the `cluster/scripts/execute_e2e.sh` script from the root of the repository
  in bash, e.g.,

```bash
./cluster/scripts/execute_e2e.sh -c <your cluster context> -i <image registry> -u <registry username> -p <registry password> -a
```

- There are several other options available for the script. To see them,
  please take a look at the documentation in the script itself.
- If you test your local cluster, the local cluster context is
  `kind-eurodat` and you should use IMG_TAG=latest (which is the default
  in the e2e test). Do not use `-a` in this case.

### Debug the automatically running e2e tests

As these test get executed repeatedly, the debugger would lose the
connection once the first iteration has finished. Therefore, we have
to set the debugger to listen on a certain port. The JVM has to connect
to that port each time a new iteration is started.

Start a debugger that listens to a remote JVM on socket
localhost:5030: ![e2e-debugger.png](e2e-debugger.png)

In the file `services/e2e/build.gradle.kts`, in section
`tasks.withType<Test>` add the line

```kotlin
jvmArgs("-agentlib:jdwp=transport=dt_socket,server=n,address=localhost:5030,suspend=y,onuncaught=n")
```

to tell the JVM to connect to that port.
Note that the test will fail if above jvm arguments were added and the
debugger is not listening on this port.
