# Run EuroDaT on your Local Machine

This guide will give you an overview of the essential steps required to set up and configure a local development environment
that can build and run a EuroDaT instance for convenient development and debugging.

Refer to the guide on [local development](../local-development/guide-local-development.md) to get started with the local development environment, starting from the
very beginning. We also provide instructions for setting up essential components such as Docker, Helm, and Tilt.

By the end of this guide, you will have a fully functional local development environment up and running. Refer to
[End-to-End Test](end-to-end-test.md) to learn how to run ent to end tests.
