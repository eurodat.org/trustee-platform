# Register a EuroDaT Client

You need to register a EuroDaT Client before it is able to connect to your EuroDaT instance.
If you are the administrator of a EuroDaT instance, you can use the script
`cluster/scripts/register_client.sh`.

The `tls.crt` corresponding to the client secret is required, see our tutorial [how to generate a client secret](generate-client-secret.md)

## Registration Script Usage

Run the following command to register a client:

```bash
    ./cluster/scripts/register_client.sh \
      --context $KUBECTL_CONTEXT \
      --tls-cert-path $PATH_TO_CLIENT_TLS_CRT\
      --client-id $CLIENT_ID\
      --client-selector $CLIENT_SELECTOR\
      --purpose $PURPOSE\
```

- **`--context`**: Kubernetes context to use for registration.
- **`--tls-cert-path`**: Path to client's certificate.
- **`--client-id`**: Unique identifier for the client being registered.
- **`--client-selector`**: The row-based security identifier for the client.
- **`--purpose`**: The purpose of the client (*APPS*, *TRANSACTIONS*, *SAFEDEPOSITS*)

If you want to register a multi-purpose client, you have to repeat **`--purpose`** for each purpose.
The token will contain all registered purposes.

## Registration Workflow at EuroDaT

```mermaid
sequenceDiagram
    participant CB as Client Backend
    participant EC as EuroDaT Controller
    participant KC as Keycloak
    CB->>EC: Call registration endpoint<br>with client name and certificate
    alt New client
        EC->>KC: Create client with certificate
    else Client exists
        EC->>KC: Throw error
    end
```
