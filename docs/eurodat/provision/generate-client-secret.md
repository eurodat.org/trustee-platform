# How to Generate a Client Secret

The EuroDaT Client uses a secret for authentication at EuroDaT's API that must be registered beforehand.

## Create a keystore file

You may use the keystore files in the [local-certs :material-launch:](https://gitlab.com/eurodat/trustee-platform/-/tree/main/cluster/scripts/local-certs) folder for testing purposes. For other cases,
either use provided keystores or create your own keystore using `keytool`:

```sh
keytool -genkeypair -v -keystore keystore.jks -keyalg RSA -keysize 4096 -validity 365 -alias certificate
```

Please always specify the alias "certificate" during
creation or configure the EuroDaT Client accordingly.

!!! Warning

    Use a strong password during keystore creation and keep it secret.

## Create the matching PEM and tls files

The EuroDaT Client expects a keystore (`.jks`) and a PEM file. Generate the PEM and tls files from the keystore `keystore.jks` using

```sh
keytool -importkeystore -srckeystore keystore.jks -destkeystore keystore.p12 -deststoretype PKCS12
openssl pkcs12 -in keystore.p12 -clcerts -nokeys | openssl x509 -out tls.crt
openssl pkcs12 -in keystore.p12 -out keystore.pem
rm keystore.p12
rm keystore.jks
```

You might want to execute the above lines one-by-one as they require some intermediate input.
You should end up with two files: the private key `keystore.pem` and the public key `tls.crt`. If you need the keystore in .p12 or .jks format,
do not execute the corresponding `rm` commands.

## Register a client
The client registration process is not yet complete. You need to provide the public key of the client to the EuroDaT
development team. Please get in touch with us to complete the registration process.

## Use PEM file to obtain client token
Once your client was registered, you can use the PEM file to obtain a client token. For this, you need to create
a signed JWT according to [https://datatracker.ietf.org/doc/html/rfc7523]. The trustee-platform repository provides a
script for convenience.

```sh
./cluster/scripts/create_jwt.sh \
  --client_id test \
  --url https://app.int.eurodat.org \
  --key_path ./your_path_to_your/keystore.pem \
  --key_password your_keystore_password \
  --cacerts ./your_path_to_your/cacerts.crt
```

The `--cacerts` argument is optional and not necessary if you are retrieving an access token from a EuroDaT cluster
with trusted certificates. The EuroDaT integration and production clusters provide trusted certificates.
