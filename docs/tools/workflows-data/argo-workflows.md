# Argo Workflows

[Argo Workflows :material-launch:](https://argoproj.github.io/argo-workflows/) is a workflow engine for orchestrating parallel jobs on *Kubernetes* which we use to define
workflows. Each step in the workflow is a container. *Argo workflows* handles the (de)provisioning of pods and can be used to pass artifacts
between them.

We use workflow templates and strict workflows to ensure that workflows cannot be directly sent to *Argo Workflows*,
but instead that only workflow templates can be referenced to start a workflow. This means that no workflows can be
started on the *Workflows* tab of the UI, and instead the *Workflow Templates* tab must be used to first create a
workflow template, which must then be submitted.

The UI of *Argo Workflows* can be accessed with a local *Tilt* setup at `localhost:2746` or by creating a port
forward from the remote cluster. If your browser complains that the server did not send any data, make sure that you
have entered the URL with https. Logging in works with token authentication, and the transaction-service's service account may be
used to generate a token. After running the local setup, you may call:

````sh
kubectl --context <CONTEXT> -n control-plane create token transaction-service-service-account
````

It is important to note that the value you paste should be `Bearer $TOKEN`. If you are not automatically redirected to
the correct namespace when you open the workflow or workflow templates windows, enter your namespace after a slash in
the URL bar of your browser, e.g. `localhost:2746/workflows/data-plane`.

Since token auto-mounting of *Kubernetes* pods are disabled, *Argo Workflows* requires a service account name for the
executor to be set. Hence, if you generate a new template using the `Create New Workflow Template` button, you may
encounter a ***serviceAccountName* error**. To handle this error, add the following block under the top level `spec` field
of the auto-generated template:

````yaml
executor:
  serviceAccountName: "data-plane-service-account"
````

Currently, all workflows orchestrated by the `Argo Workflow controller` are archived for 30 days in a
*PostgresSQL* database.
