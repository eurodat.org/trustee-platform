# Workflows and Data

[Argo Workflows](../../tools/workflows-data/argo-workflows.md) simplifies the management of complex tasks within *Kubernetes*
clusters by organizing them into separate pods. This orchestration involves pod provisioning, deprovisioning, and
artifact transfer.

**Basics of *Argo Workflow* in the EuroDaT realm:**

- **Workflow Templates:** *Argo Workflows* promotes structured workflow management through Workflow Templates.
Workflows cannot be initiated directly. Instead, users reference a workflow template to start a workflow.
This approach restricts the use of the database, increases security and enables efficient workflow processing.
- **User Interface (UI):** The UI is accessible via a local *Tilt* setup or a port forwarding from the remote *Kubernetes* cluster.
Token-based authentication ensures security and can be generated via the transaction-service's service account.
- **Archival of Workflows:** All workflows orchestrated by the `Argo Workflow controller` are archived for a duration of
30 days in a *PostgreSQL* database.

[PostgreSQL](../../tools/workflows-data/postgresql.md) is a database management system. In this guide you will
learn its usage, ports, and how to interact with it effectively.
