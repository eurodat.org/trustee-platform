# Cluster

On our clusters, we work with containerized software deployments. We package our code and all its dependencies into
*Docker* images that become [Docker containers :material-launch:](https://www.docker.com/resources/what-container/)
when they run on a [Docker engine :material-launch:](https://www.docker.com/products/container-runtime/).
With [Kubernetes :material-launch:](https://kubernetes.io/de/) we manage and scale our containers. To deploy a service on *Kubernetes* using
*Docker* images, we use [Helm Charts :material-launch:](https://helm.sh/docs/topics/charts/) that help to provide a list of all services needed.
