# Helm

## How-to Guide

### Notes

#### Use the cluster folder (the folder one level above Helm) to run these instructions

- You need to add the required environment variables to your session (check all `values.yaml` for requirements
and pipeline secrets for the actual values).

#### The following instructions can be executed using Git Bash in Windows

- **Add repositories**

  ```sh
    helm repo add <repo_name> <repo_url>
  ```

- **update and build *Helm* dependencies**

  ```sh
    helm dependency update <path_to_helm_folder>
    helm dependency build <path_to_helm_folder>
  ```
  
- **Create a new namespace with local files:**

  ```sh
    envsubst < helm/eurodat/values.yaml | helm upgrade --install eurodat helm/eurodat --namespace $NAMESPACE --create-namespace --values -
  ```

- **Update namespace after changing configurations:**

  ```sh
    envsubst < helm/eurodat/values.yaml | helm upgrade eurodat helm/eurodat --namespace $NAMESPACE --create-namespace --values -
  ```

- **Add `kubeconfig file` as a variable in Git Bash (and ensure your k8s context is correct since operator will use this context):**
  Note that `KUBECONFIG` is a special environment variable that, when set, overwrites the `kubectl` context

  ```sh
    export KUBECONFIG=$(cat ~/.kube/config | dos2unix | base64 -w 0)
  ```
