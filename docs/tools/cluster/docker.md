# Docker

## How-to Guide

- **Start a container:** ```docker run image-name```
- **Get a list of containers:** ```docker ps```
- **Stop a container:** ```docker stop container-name```
- **Force stop and remove a container:** ```docker rm -f container-name```
- **Forcefully stop and remove all containers:** ```docker rm -f $(docker ps -aq)```
- **Build an image and start it:** ```docker build -t image-name . && docker run image-name```
- **Login to another registry:** ```docker login repository-name```
- **Tag an image:** ```docker tag source-name target-name```

## Problems and Solutions

- **When using *Podman*, weird network problems happen:** Don't use *Podman*, because even though most things work similarly to *Docker*, some things (such as communication between containers and localhost) don't
- ***Docker* containers do not communicate with each other:** If you are using localhost, ensure that the localhost network is enabled with --network=host flag, or use the ip of the host machine (which might be simplified with host.docker.internal as endpoint)
- **I cannot use *Docker*:** To test services locally, one might require a data connector(s).
  If you do not have *Docker*, it is possible to replace the "docker" commands with "podman".
  *Podman* may act differently compared to *Docker* and cause unexpected networking issues.
