# Pre-Commit Hooks

We use some pre-commit hooks to check our code:

- [ktlint :material-launch:](https://github.com/pinterest/ktlint): kotlin files
- [markdownlint-cli2 :material-launch:](https://github.com/DavidAnson/markdownlint-cli2): markdown files
- [Talisman :material-launch:](https://thoughtworks.github.io/talisman/): scans for possibly sensitive data
- [eslint](https://eslint.org/): typescript/javascript files
- [stylelint](https://stylelint.io/): scss files
- [htmlhint](https://htmlhint.com/): html files

## Install the hooks in your repository

The script `cluster/scripts/install-pre-commit-hook.sh` installs *Talisman* and *ktlint* only for the repository the file
was executed from and creates the `.git/hooks/pre-commit` file. `markdownlint-cli2` needs to be installed manually.
Make sure to run the script from inside the repository you want the hooks to be installed in. If you run Git from a
different OS than you ran the installation script from, please rerun the script in that OS.

## What to do if Talisman throws a false positive  

If you are absolutely sure that there is no sensitive information you are about to commit you have two options:  

- Preferably, add to allowed patterns/files if you think it could be a reoccurring incident.
- After discussing with others, commit anyway by disabling Git Hooks for this commit.

### Add allowed patterns/files

The *Talisman GitHub page* has an in depth explanation on how to allow specific detectors, patterns or files:
<https://github.com/thoughtworks/talisman#ignoring-specific-detectors>
To add something to the allowed patterns please use the `.talisman.rc` file in the root folder of the project and refer
to it for example implementations.

**Escape regex functions!**  
Please remember to escape ":" if you don't need the regex operator but the actual string otherwise everything after
the ":" will not be interpreted correctly by *Talisman*. **So do "\\:"**  

### Disable Git Hooks

If you use the IntelliJ IDEA you can disable the Git Hook by unchecking the `Run Git Hooks` box. To find it press the
`gears button` that is in line with the **Commit** and **Commit and Push...** buttons. This change is only
applied for this one commit and will reset itself for the next one.  

If you want to use the Git CLI you can simply add the `--no-verify` flag.
