# Static Analysis

EuroDaT uses three [pre-commit hooks](../../tools/static-analysis/pre-commit_hooks.md) to check code changes locally before committing them to git.
In the "[Code Quality Checks](../../tools/static-analysis/static-analysis_sonarcloud.md)" section we describe the *Talisman* configuration and HTML report access,
along with *SonarCloud*-specific insights for our CI/CD pipeline.
We address SonarCloud's coverage threshold, branch handling, and code quality checks, ensuring our code meets high standards throughout development.

**The key tools include:**

- *SonarCloud*: To check the coverage of our unit tests.
- *ktlint*: Enforces Kotlin code standards.
- *markdownlint*: Ensures consistent markdown documentation.
- *Talisman*: Guards against sensitive data leaks.
- *sslyze*: Evaluates TLS security.
- *OWASP-dependency-check*: Detects dependency vulnerabilities.
