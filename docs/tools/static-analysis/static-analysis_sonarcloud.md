# Code Quality Checks

The CI/CD pipeline has a stage `code_quality`. In this stage, various tools run and create reports:

- [`SonarCloud` :material-launch:](https://www.sonarsource.com/products/sonarcloud/).
- [`ktlint` :material-launch:](https://ktlint.github.io/).
- [`markdownlint` :material-launch:](https://github.com/DavidAnson/markdownlint-cli2)
- [`talisman` :material-launch:](https://github.com/thoughtworks/talisman#ignoring-specific-detectors)
- tls-report is [`sslyze` :material-launch:](https://github.com/nabla-c0d3/sslyze).
- *OWASP-dependency-check* is [`Dependency-Check` :material-launch:](https://owasp.org/www-project-dependency-check/).

The reports from *markdownlint* and *Talisman*, as well as *OWASP-dependency-check* appear in their artifacts always.
*SonarCloud* analysis also produces a report as an artifact for pipelines with it.
The report from *ktlint*, tls-report appear in their artifacts only in case of failure.

## Talisman options

By default, a report from *Talisman* has json-format and contains all information about errors, warnings and their severity levels.
If you prefer html-report (which looks nice, but doesn't give information about warnings and severity level), please open file `cluster/pipeline/misc.gitlab-ci.yml` and change the following line

```shell
- ./.git/hooks/bin/talisman --scan --ignoreHistory --reportDirectory=talisman
```

to:

```shell
- mkdir ~/.talisman
- curl https://github.com/jaydeepc/talisman-html-report/archive/v1.3.zip -o ~/.talisman/talisman_html_report.zip -J -L
- cd ~/.talisman && unzip talisman_html_report.zip -d . && mv talisman-html-report-1.3 talisman_html_report && rm talisman_html_report.zip
- cd -
- ./.git/hooks/bin/talisman --scanWithHtml --ignoreHistory
```

To use a correct path to an artifact, change `talisman` to `talisman_html_report` in `artifacts:paths:` in job `linter:talisman:`.

After downloading the artifact and extracting it, run two commands:

```shell
cd Downloads/talisman_html_report/
python -m http.server 8080
```

Then go to `localhost:8080` in your browser and see the results.

For more information about *Talisman* (for example, what to do with false positives), please check [our documentation on pre-commit hooks](pre-commit_hooks.md).

## SonarCloud Notes & Problems

We are using [SonarCloud :material-launch:](https://www.sonarsource.com/products/sonarcloud/) and its quality gates to check the coverage of our unit tests. The current threshold for it to pass is **70%**.

Previously, *SonarCloud* only performed a coverage scan after the feature branch has been merged with `main`.
This lead in some cases to post-merge errors that needed to be additionally fixed. This problem was due to *SonarCloud*
only checking **long-lived branches**, while **short-lived branches** only get a coverage estimate.

Our current solution for this is to manually edit the **long-lived branches pattern** inside *SonarCloud*'s UI,
which can be found at `Branches` > `Top right corner`. The current regex-pattern is set to `(EDTBUC|edtbuc|hotfix|story|subtask).*`,
Accordingly, any branch that has any of those words as a prefix in its name will be set as a **long-lived branch**.
The main drawback of this method is that **long-lived branches** will stay in *SonarCloud*'s UI, while **short-lived branches** get
sorted out automatically after some time. Note that this pattern has to be changed manually for every project.
