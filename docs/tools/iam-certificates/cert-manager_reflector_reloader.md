# Cert/Trust-Manager, Secrets and Certificates Update

We use [cert-manager :material-launch:](https://cert-manager.io/) to handle the automatic renewal of certificates
(self-signed or by a CA).
**Cert-manager** requires a number of CRD resources, which can be installed manually using kubectl:

```sh
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v<VERSION>/cert-manager.crds.yaml
```

or using the `installCRDs` option when installing the *Helm Chart*:

```sh
envsubst < <PATH>/values.yaml | helm upgrade --install eurodat-cluster <PATH> --set installCRDs=true --values -
```

You can read more [here :material-launch:](https://cert-manager.io/docs/installation/helm/#option-1-installing-crds-with-kubectl).
Due to certain conflicts in our system we are currently forced to use the second method.

We use [trust-manager :material-launch:](https://cert-manager.io/docs/projects/trust-manager/) to distribute a bundle of
trusted CA's certificates in our cluster and provision a (almost) single source of truth for trust.

## Issuers and Certificates

We have

- a cluster-wide `ClusterIssuer` that is using ACME to get its certificate from *Let's Encrypt*,
- a self-signed cluster issuer that is used for *ECK's* self-signed certificate and
- for the CA of our in-cluster `local-issuer` that we use for the certificates of the in-cluster components.

### Notes

When creating a new DNS name (e.g. for a new cluster) it may take ~5 minutes to work.

## Renewing certificates

Certificates can be renewed using the cert-manager command line
tool [cmctl :material-launch:](https://cert-manager.io/docs/reference/cmctl/).

For example for the `eurodat-certificate` in the local *Tilt* setup:

```shell
cmctl --context kind-eurodat -n developer-base renew eurodat-certificate
```

## Reflector

[Reflector :material-launch:](https://github.com/emberstack/kubernetes-reflector) is used to automatically copy secrets, such as
certificates, in *Kubernetes* between namespaces.

## Reloader

[Reloader :material-launch:](https://github.com/stakater/Reloader) triggers a rolling update of an annotated *Kubernetes* `Deployment`
when a `ConfigMap` or `Secret` has changed.
