# IAM and Certificates

The identity and access management is done with
[cert-manager :material-launch:](https://cert-manager.io/) including issuers, ingresses and a [Kubernetes Reflector :material-launch:](https://github.com/emberstack/kubernetes-reflector).
A Vault controls the access tokens, passwords and certificates. [Keycloak :material-launch:](https://www.keycloak.org/) provides client authentication functionalities.
