# Images

All images (but a few cloud provider images) in the eurodat cluster
are pulled via digests.
The image format is:

`<registry>/<repository>:<tag>@<digest>`,

where the tag is optional and will be ignored if a
digest is given.

It is not possible to use an external or self-built image
without a digest except for the images used by the cloud provider.
There is a gitlab job `verify-digest-usage-in-cluster`
which enforces this by checking all the images pulled after deployment
by the cluster. It only runs once in the pipeline and does not check
the images afterwards again.

## How to pull an image

To allow an external image to be pulled, it has to be included in the
`external_images` file with a digest or it has to be directly hardcoded
with a digest.

To use a self-built image, you have to include the repository name of
the image in the `eurodat-images` file. Since the digest is probably
not known prior to the image build, you can't directly provide a
digest for this image.

A build job must publish a file with arbitrary name in
`image_references/*` that contains the image ID with a digest.
You can use the `.build:save-image-reference` job for that purpose by including

`- !reference [".build:save-image-reference", "script"]`

in the respective gitlab build job.

## How does the pipeline determine the digest

The `create-images-lock-file` job takes the external images and the
digests from the self-built images in the build stage before and
creates an `images.lock` file containing a list of images IDs with
digests.

During deployment, the image IDs of the respective app-of-apps ArgoCD
application are checked. The `override-dev-image-refs` job injects the
image digests from the `images.lock` file for all image IDs without a
digest into a modified app-of-apps which will then be used for the
deployment in the `app-of-apps` job.

Note that the `override-dev-image-refs` job matches the image IDs with
tags. If no tag is provided, the first digest matching the respective
image in the `images.lock` file will be taken.
Images with digests will not be changed.
