# Documentation

This section provides an introduction to OpenAPI and *Swagger* UI within the Trustee platform,
offering insights into how developers interact with its API services.

*OpenAPI* serves as the foundation for defining and documenting REST APIs. It is accessible via the `transaction-service`.
*Swagger UI* provides an interactive interface for exploring and testing APIs.

The "[OpenAPI and Swagger UI](openapi_swagger.md)" section will guide you on utilizing them effectively to interact with the Trustee platform's APIs.
Additionally, we'll explore [MkDocs](mkdocs.md), a tool for creating documentation, enabling you to contribute to the Trustee platform's documentation ecosystem effectively.
