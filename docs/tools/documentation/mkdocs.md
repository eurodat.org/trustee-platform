# MkDocs

## How-to Guide

### Setup for Local Development

Make sure you completed the `make prep` command that sets up a python venv and installs the required dependencies.

### Run the Local Development Server

Run in a terminal:

On Windows:

```shell
./venv/Scripts/python3.exe -m mkdocs serve
```

On *Unix*-like systems with

```shell
./venv/bin/python3 -m mkdocs serve
```

After the initialisation is complete, the terminal output displays a link for the browser to view the docs, including
hot reloading.
