# OpenAPI and Swagger UI

## OpenAPI

Our [OpenAPI :material-launch:](https://swagger.io/docs/specification/about/) specification is made accessible by
the `transaction-service`. It is available at the following
URL `https://<cluster_fqdn>/api/v1/openapi` 
where `<cluster_fqdn>` is the cluster's FQDN, e.g. `app.int.eurodat.org`.

## Swagger UI

The [Swagger UIs :material-launch:](https://swagger.io/tools/swagger-ui/) of our cluster are provided at:

- `https://<cluster_fqdn>/api/v1/swagger-ui/` for managing transactions & workflows
- `https://<cluster_fqdn>/api/v1/app-service/swagger-ui/` for managing apps & images
- `https://<cluster_fqdn>/api/v1/participants/swagger-ui/` for managing data
- `https://<cluster_fqdn>/api/v1/contract-service/swagger-ui/` for managing contracts

