# Opentelemetry-collector

OpenTelemetry is an open source tool to collect telemetry data like traces from services and
export them to analysis services like Grafana Tempo.
OpenTelemetry uses the grpc protocol on port 4317.

OpenTelemetry is supported by Quarkus. To enable it in your services,
please follow the guide [here](https://quarkus.io/guides/opentelemetry).
Note that the Bearer token in the `application.properties` has to be set like:

`quarkus.otel.exporter.otlp.traces.headers=authorization=Bearer ${OTLP_SECRET}`

where `OTLP_SECRET` is the environment variable that has to be set to the `opentelemetry-token-secret` secret.
