# Monitoring

Efficient monitoring in *Kubernetes* environments is crucial for optimizing resource allocation, analysing security
incidents and debug bugs within a running setup.
Further information about the used monitoring tools can be found below:

- [Grafana](grafana.md)
- [OpenTelemetry-Collector](opentelemetry-collector.md)
- [Tempo](tempo.md)
- [Prometheus](prometheus.md)
- [Logs](logs.md)
- [Kotlin-Logging](kotlin_logging.md)
