# Logs

## Promtail

[Promtail :material-launch:](https://grafana.com/docs/loki/latest/send-data/promtail/) is responsible for gathering the
logs from the Kubernetes nodes directly. It runs as a `DaemonSet` i.e. once per node.
This means that all logs that are written to either `stdout` or `stderr` are collected.

## Loki

[Loki :material-launch:](https://grafana.com/oss/loki/) ingests and stores the logs sent by Promtail in a persistent volume.
It does not index the log contents like Elastic but only metadata. The logs can be queried directly using the Loki API or
can be explored using Grafana. The service is disabled by default in the local setup but can be enabled by setting the
variable `loki_enabled` to `True` in the `Tiltfile`.
