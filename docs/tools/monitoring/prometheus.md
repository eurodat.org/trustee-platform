# Prometheus

Prometheus is an open source metrics monitoring tool.
It is deployed within the prometheus namespace
(deactivated in local development by default).
Prometheus metrics can be queried and visualized in Grafana.
