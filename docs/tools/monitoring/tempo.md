# Tempo

Tempo is an open source tracing backend. It sits between OpenTelemetry Collector, which collects the traces,
and Grafana, which offers a GUI frontend. Tempo persists the data via a persistent volume claim.
Port 4317 is used between OpenTelemetry Collector and Tempo. Port 3100 is used between Tempo and Grafana.
Communication with Tempo is secured with mTLS, so all clients have to provide a valid client certificate
provided by the cluster CA.
