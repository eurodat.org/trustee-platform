# Grafana

[Grafana :material-launch:](https://grafana.com/) is an open source analytics and visualization application.
We use Grafana for the visualization of HTTP traces, logs and metrics.
Grafana is deployed in the base namespace.
To use Grafana, you need to forward port 3000 of the (internal) cluster.
Then you can reach the Grafana UI by `https://localhost:3000`.
To log in use the username `admin` and the password encoded in the secret `grafana-credentials-secret`.
