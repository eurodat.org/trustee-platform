# Kotlin Logging

We use [Kotlin Logging :material-launch:](https://github.com/oshai/kotlin-logging/wiki) as a lightweight logging framework for Kotlin.
It is a wrapper around the logging facade slf4j API. JBoss Logging is the logging backend. The following `api` dependencies are used for the configuration.

`api("io.github.oshai:kotlin-logging-jvm:$kotlinLoggingVersion")`
`api("org.slf4j:slf4j-api:$slf4jVersion")`
`api("org.jboss.slf4j:slf4j-jboss-logmanager:$jbossLogManagerVersion")`

Furthermore, the following dependencies are used to configure the log message format.

`com.fasterxml.jackson.module:jackson-module-kotlin`
`com.fasterxml.jackson.datatype:jackson-datatype-jsr310`

## Logger

To obtain a logger, please add the following to your class:

`import io.github.oshai.kotlinlogging.KotlinLogging`

And place the definition above the class declaration, below imports,
in order to make the field static and accessible only within the file:

`private val logger = KotlinLogging.logger {}`

## MDC Contextual Logging

We add implicit context data such as appId and transactionId to possible log messages via
[Mapped Diagnostic Context (MDC):material-launch:](https://quarkus.io/guides/logging#use-mdc-to-add-contextual-log-information).
We import MDC via:

`import org.slf4j.MDC`

A key-value pair is used at every endpoint via `MDC.put(key, value)` to add a key and the corresponding value to the MDC.
