---
hide:
  - toc
---

# Usage of the EuroDaT API

The EuroDaT API enables management of data, transactions and workflows. Learn about the concepts of:

- [Apps](app-definition.md)
- [Transactions and workflows](transactions.md)
- [Images](images.md)
- [Messaging](messaging.md)
- [Data Management](data-management.md)
