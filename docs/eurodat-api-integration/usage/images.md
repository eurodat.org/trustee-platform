# Image

Every application provides workflows that need to run a specific image.

## How to upload an image?

To upload one or more images for an already registered app, use the
`apps/{appId}/images` POST endpoint where appId is the ID of your application.
The image location must be provided in form of a path to the image in a public
registry like `docker.io/hello-world:latest`. If your image is hosted in
a protected registry, you can also pass the required username
and password. As soon as the image upload is finished or failed, this
data will be deleted.

The request triggers the image pull process.
A success response means that
the image is going to be pulled by EuroDaT.
It does not mean that the image is successfully uploaded at this point.
To view the status of the image, please see below.

## How to view the status of the image?

To view the status of your image, use the `apps/{appId}/images/{imageId}`
GET endpoint where appId is the ID of your application and imageId is the
ID of your image that you get from the POST endpoint.
If the status is _ACTIVE_, your image is ready to be used.
To view all images, use the `apps/{appId}/images` GET endpoint.

## How to delete an image?

To delete an image, use the `apps/{appId}/images/{imageId}`
DELETE endpoint. Note that you need to delete all images
of an app before you can delete the app. A deleted image will still
appear in the list of images but with status _DELETED_.
