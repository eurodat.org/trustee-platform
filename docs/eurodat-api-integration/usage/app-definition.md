# Configuration of the per-transaction database

An app definition consists of:

1. An id for this app.
2. A list of workflows which can be started for this app (
   see [Start Workflow](transactions.md))
    - id to start the argo workflow
    - name of the argo workflow template
    - the entry-point in the argo workflow
3. The ddl statement for the creation of the tables of the transaction database.
   A database for a transaction can be created by triggering the endpoint [Start Transaction](transactions.md),
   using the app id as a parameter.<br>
   **The database will always contain three schemata**: `input`, `intermediate` and `output`<br>
   The DDL statement should be provided in one file. It is not allowed to create additional schemata
   in the per-transaction database. You can enable row based security for tables in the `output` schema which is
   configurable in a format like this. <br><br>
   ``create table output.table_name(
   transaction_id uuid not null primary key,
   security_column text not null); ALTER TABLE output.table_name ENABLE ROW LEVEL SECURITY;``<br><br>
   On the output schema, you can use **row level security** which gives you the possibility to provide individual data
   for each participant. This is done by adding a column to the table which contains the participant id. This column is
   then used to filter the data for the participant. The column name and the value which should be written into the
   column are defined in the client mapping.
4. The ddl statement for the creation of the safe deposit box<br> ([Safe Deposit Database](transactions.md))
5. The list of table security mappings: <br><br>
   ``
   "tableSecurityMapping": [
   {
   "rowBaseOutputSecurityColumn": "string",
   "tableName": "string"
   }
   ]
   `` <br><br>
   This ensures that only the correct client can see their view on the output data.
   Every participant can only see the results in the given table where the value in the "security_column" matches
   their participant identifier, e.g. their client selector as defined upon client registration. Each mapping affects only one table and contains
   2 inputs:
    - the column name of which the row level security should be applied e.g. "security_column"
    - the name of the table which the rule should apply on.  <br>
6. Optionally, the timeout for transactions in days.
   If this optional value is not supplied then the default value of 30 is taken. An active transaction gets automatically terminated once it gets
   older than the timeout. 
   
