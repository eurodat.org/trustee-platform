# Transaction and Workflows

This section explains how to manage transactions and workflows using the EuroDaT APIs. All the following actions can be started via the [EuroDaT Swagger UI](../../tools/documentation/openapi_swagger.md) or with the provided
curl commands.

## 1. Start Transaction

To start a transaction, you need to provide an app ID of the app for which you
want to start a transaction. You can specify a list of partaking data consumers
and a list of partaking data providers. This restriction is optional. If not specified
all registered clients can participate in the transaction.

The start of a transaction provides you with the corresponding transaction ID
and creates a *PostgreSQL* database for this transaction.
For information on how this is created see [Configuration of a database](app-definition.md)
The name of the database contains the transaction ID.

```bash
curl -X 'POST'
'https://<api_host>/api/v1/transactions'
-H 'accept: application/json'
-H 'Content-Type: application/json'
-d '{ "appId": "yourAppId", "consumer": ["selector_consumer"], "provider": ["selector_provider"] }'
```

## 2. Start Workflow

You need to provide the transaction ID as well as the workflow definition ID of
the workflow you want to start.
Only participants which are specified as consumers for this transaction are permitted to start workflows.
The start of the workflow triggers the start of the *Argo* template which is
stored under the app ID.

```bash
curl -X 'POST'
'https://<api_host>/api/v1/transactions/<transactionId>/workflows'
-H 'accept: application/json'
-H 'Content-Type: application/json'
-d '{ "workflowDefinitionId": "HelloWorld" }'
```

## 3. End Workflow

To end a transaction and delete its associated transaction
specific database created in the start transaction step, use the following endpoint:

```bash
curl -X 'DELETE'
'https://<api_host>/api/v1/transactions/<transactionId>'
-H 'accept: application/json'
-d ''
```

## 4. Safe Deposit Database

With this POST endpoint a database is created which can be used as a safe deposit box for data storage.
This database can be deleted with the DELETE endpoint.

```bash
curl -X 'POST' 
  'https://<api_host>/api/v1/safe-deposit-database' 
  -H 'accept: application/json' 
  -H 'Content-Type: application/json' 
  -d '{"appId": "yourAppId" }'
```

```bash
curl -X 'DELETE' 
  'https://<api_host>/api/v1/safe-deposit-database/<appId>' 
  -H 'accept: application/json'
  -d ''
```
