# Workflows

To run a specific task on the EuroDaT platform, you need to provide
a workflow that can then be executed inside a transaction. The workflow
mainly consists of the image that should be ran with a specified start
command.

## How to upload a workflow?

To upload a workflow, you need to already have registered an app and
an image. The workflow can be uploaded on the `apps/{appId}/workflows`
POST endpoint. You need to provide the image id, for the image that
should be executed, a start command with which the image will be
started and a workflow name. The workflow name must be unique within
the application and can only consist of lower case alphanumeric
characters, '-' or '.', and must start and end with an alphanumeric
character (RFC1123).

## How to view the registered workflows for an app?

You can view the registered workflows for the app on the
`apps/{appId}/workflows` GET endpoint.

## How to view the details of a registered workflow for an app?

You can view the details of a registered workflow for the app on the
`apps/{appId}/workflows/{workflowId}` GET endpoint.

## How to delete a workflow?

You can delete a workflow on the
`apps/{appId}/workflows/{workflowId}` DELETE endpoint.
