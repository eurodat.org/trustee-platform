# Messaging Resource API

The Messaging Resource API provides endpoints to fetch messages for specific apps and posting new messages.
Messages are tied to the lifecycle of a transaction and are only available while the transaction is active.
I.e., messages can only be retrieved while the associated transaction is running and they are automatically deleted when the transaction ends.
Ensure you fetch messages during the transaction lifecycle to avoid losing access.

### Message

A `Message` object has the following fields:

- `appId` (String): The ID of the app associated with the message.
- `transactionId` (String): The ID of the transaction associated with the message.
- `workflowId` (String): The ID of the workflow associated with the message.
- `label` (String): The message content.
- `selector` (String): The selector defining the recipient/participant.

## Endpoints

### Fetch Messages for an App

**Endpoint**:  
`GET /api/v1/apps/{appId}/messages?=excludeUntil`

**Description**:  
Fetches messages for a specific app.

**Path Parameter**: `appId` (String)

**Query Parameters**: `excludeUntil` (Long, Optional) excludes messages before this timestamp.

**Response**: Returns a list of messages for the app, filtered by `excludeUntil` if provided.
  ```json
  [
    {
      "appId": "string",
      "transactionId": "string",
      "workflowId": "string",
      "label": "string",
      "selector": "string"
    }
  ]
  ```

**Example Request**:
```bash
curl -X GET 'https://<api_host>/api/v1/apps/123/messages?excludeUntil=1622548800' \
  -H 'Authorization: Bearer <your_token>'
```

### Post a New Message 

***Note**: This post endpoint is not publicly exposed. Only workflow containers part of an app can post messages.*

**Endpoint**:  
`POST /api/v1/messages`

**Description**:  
Creates new messages for the specified selectors.

**Request Body**:
```json
{
  "message": "string",
  "selectors": ["string1", "string2"]
}
```

**Header Parameters**: `Authorization` (String, Required) is the Bearer token for authorization.

**Response**: Returns a list of created messages.
  ```json
  [
    {
      "appId": "string",
      "transactionId": "string",
      "workflowId": "string",
      "label": "string",
      "selector": "string"
    }
  ]
  ```
  
- **400 Bad Request**: Returned when invalid selectors are provided in the request.
- **401 Unauthorized**: If the `Authorization` header is missing or invalid.

**Example Request**:
```bash
curl -X POST 'https://<api_host>/api/v1/messages' \
  -H 'Authorization: Bearer <your_token>' \
  -H 'Content-Type: application/json' \
  -d '{
    "message": "Sample message",
    "selectors": ["selector1", "selector2"]
  }'
```
