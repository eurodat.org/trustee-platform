# Data Management API

The Data Management API allows participants to upload and retrieve data associated with transactions and the safe deposit database.

## Endpoints

### Upload Data to transaction database

- This endpoint inserts data into the specified table of the `input` schema within the transaction database.
- The path parameter `participantId` acts as the client selector for authentication and authorization.
- Data is inserted using the `json_to_record` function.
- The specified `table` must already exist in the database.
- The `transactionId` ensures that data is associated with the correct transaction.
- The query is limited to 1000 records per request.

**Endpoint**:  
`POST /api/v1/participants/{participantId}/data`

**Payload**:

```json
{
  "data": [
      "{\"key1\":\"value1\",\"key2\":\"value2\"}"
  ],
  "table": "string",
  "transactionId": "string"
}
```

### Retrieve Data from the transaction database

- This endpoint retrieves data from the `output` schema of the transaction database.
- The path parameter `participantId` acts as the client selector for authentication and authorization.
- Results are returned in JSON format.
- Data is fetched using the `row_to_json` function.
- If `correlationId` is provided:
  - The database table needs to have a `correlationId` column.
  - A `WHERE` clause is added to filter results from the database:

    ```sql
    WHERE correlationId = {correlationId}
    ```

**Endpoint**:  
`GET /api/v1/participants/{participantId}/data`

**Query Parameters**:

- `table` (required): The name of the table to query.
- `transactionId` (required): The ID of the transaction to retrieve data from.
- `correlationId` (optional): Filters results by correlation ID if provided.

**Response**:

```json
[
  "{\"key1\":\"value1\",\"key2\":\"value2\"}",
  "{\"key1\":\"value3\",\"key2\":\"value4\"}"
]
```

### Upload Data to safe deposit database

- This endpoint inserts data into the specified table of the `safedeposit` schema within the safedeposit database.
- The path parameter `participantId` acts as the client selector for authentication and authorization.
- Data is inserted using the `json_to_record` function.
- The specified `table` must already exist in the database.
- The `appId` ensures that data is associated with the correct app.
- The query is limited to 1000 records per request.

**Endpoint**:  
`POST /api/v1/participants/{participantId}/persistentdata`

**Payload**:

```json
{
  "data": [
      "{\"key1\":\"value1\",\"key2\":\"value2\"}"
  ],
  "table": "string",
  "appId": "string"
}
```

### Retrieve Data from the safe deposit database

- This endpoint retrieves data from the `safedeposit` schema of the safedeposit database.
- The path parameter `participantId` acts as the client selector for authentication and authorization.
- Results are returned in JSON format.
- Data is fetched using the `row_to_json` function.

**Endpoint**:  
`GET /api/v1/participants/{participantId}/persistentdata`

**Query Parameters**:

- `table` (required): The name of the table to query.
- `appId` (required): The ID of the app to retrieve data from.

**Response**:

```json
[
  "{\"key1\":\"value1\",\"key2\":\"value2\"}",
  "{\"key1\":\"value3\",\"key2\":\"value4\"}"
]
```

## Unsupported Data Types

Certain data types are not supported:

- Data types with ranges, such as `char(4)`, are automatically handled as `char`, which may result in range exceptions.
- `ENUM` fields and other custom types are not supported.

## Security and Authorization

- The `participantId` in the path ensures that only authorized participants can access or modify the respective data. It needs to match the clientSelector provided in the [client registration](../../eurodat/provision/register-eurodat-client.md)
- Connections are established dynamically using credentials retrieved from the credential service.
