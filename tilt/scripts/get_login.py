#!/usr/bin/env python3
import os
import getpass

if __name__ == "__main__":
    try:
        print(os.getlogin(), end="")
    except OSError:
        print(getpass.getuser(), end="")
