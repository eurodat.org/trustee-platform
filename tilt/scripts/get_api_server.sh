#!/usr/bin/env bash

kubectl get endpoints --namespace default kubernetes -o json | jq ".subsets[0].addresses[0].ip" -r
