#!/bin/sh
COMMIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
export COMMIT_BRANCH
echo COMMIT_BRANCH="$COMMIT_BRANCH"
export CI_COMMIT_REF_PROTECTED=true
echo CI_COMMIT_REF_PROTECTED=$CI_COMMIT_REF_PROTECTED
CI_COMMIT_SHA="$(echo "$EXPECTED_REF" | cut -d: -f3)"
export CI_COMMIT_SHA
echo CI_COMMIT_SHA="$CI_COMMIT_SHA"
IMG_REGISTRY_NAME=$(ctlptl get cluster kind-eurodat -o template --template '{{.status.localRegistryHosting.host}}')
export IMG_REGISTRY_NAME
echo IMG_REGISTRY_NAME="$IMG_REGISTRY_NAME"
export GRADLE_OPTS=-Dquarkus.container-image.push=true
echo GRADLE_OPTS=$GRADLE_OPTS
export JAVA_OPTS="-Djib.allowInsecureRegistries=true -Dquarkus.container-image.push=true -Dquarkus.container-image.image=$EXPECTED_REF -Dquarkus.container-image.insecure=true -Dquarkus.jib.base-jvm-image=cgr.dev/chainguard/jre:latest-dev -Dquarkus.jib.platforms=linux/amd64,linux/arm64/v8"
echo JAVA_OPTS="$JAVA_OPTS"
