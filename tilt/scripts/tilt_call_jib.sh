#!/usr/bin/env sh
# shellcheck disable=SC1091
set -e
. ./tilt/scripts/tilt_set_build_env.sh
echo "Inputs $*"
FOLDER_NAME="$1"
"$FOLDER_NAME"/gradlew -x test -p "$@"
