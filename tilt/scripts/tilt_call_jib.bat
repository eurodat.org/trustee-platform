@ECHO OFF
CALL tilt/scripts/tilt_set_build_env

SET _string=-p %1 %2 %3 %4 %5 %6
:: Remove quotes
SET _string=%_string:"=%

CALL %1/gradlew -x test %_string%
