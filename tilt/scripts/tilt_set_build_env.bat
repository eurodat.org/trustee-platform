@ECHO OFF
FOR /F "tokens=* USEBACKQ" %%F IN (`git rev-parse --abbrev-ref HEAD`) DO (
SET COMMIT_BRANCH=%%F
)
ECHO COMMIT_BRANCH=%COMMIT_BRANCH%
SET CI_COMMIT_REF_PROTECTED=true
ECHO CI_COMMIT_REF_PROTECTED=%CI_COMMIT_REF_PROTECTED%
ECHO EXPECTED_REF=%EXPECTED_REF%
FOR /F "tokens=3 delims=: " %%A in ("%EXPECTED_REF%") DO (
SET CI_COMMIT_SHA=%%A
)
ECHO CI_COMMIT_SHA=%CI_COMMIT_SHA%
SET IMG_REGISTRY_NAME=localhost:5002
ECHO IMG_REGISTRY_NAME=%IMG_REGISTRY_NAME%
SET GRADLE_OPTS=-Dquarkus.container-image.push=true
ECHO GRADLE_OPTS=%GRADLE_OPTS%
SET JAVA_OPTS=-Djib.allowInsecureRegistries=true -Dquarkus.container-image.push=true -Dquarkus.container-image.image=%EXPECTED_REF% -Dquarkus.container-image.insecure=true
ECHO JAVA_OPTS=%JAVA_OPTS%
SET QUARKUS_JIB_BASE_JVM_IMAGE=cgr.dev/chainguard/jre:latest-dev
