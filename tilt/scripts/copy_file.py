#!/usr/bin/env python3
import argparse
import filecmp
import os
import shutil


def copy_file(file_source, file_destination):
    if os.path.isfile(file_destination):
        print(f"File {file_destination} already exists.")
        if filecmp.cmp(file_source, file_destination) is False:
            print("But the file has changed..")
            shutil.copyfile(file_source, file_destination)
    else:
        shutil.copyfile(file_source, file_destination)
        print(f"Copied file {file_source} to {file_destination}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--source", required=True)
    parser.add_argument("--dest", required=True)
    args = parser.parse_args()
    copy_file(args.source, args.dest)
