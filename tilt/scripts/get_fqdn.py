#!/usr/bin/env python3
import socket
from sys import platform


def get_host_fqdn() -> str:
    if (
        platform == "linux"
    ):  # avoid changing logic for normal windows setup because of WSL
        return "localhost"
    if platform == "darwin":  # macOS required local TLD
        return socket.gethostname().lower() + ".local"
    return socket.getfqdn().lower()


if __name__ == "__main__":
    print(get_host_fqdn(), end="")
