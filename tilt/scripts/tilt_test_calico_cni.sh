#!/usr/bin/env bash

# This script demonstrates the selected Calico CNI fulfills the requirement to enable network policies on your
# local tilt cluster. Execute the script after "make setup" has run successfully.

# Switch to internal cluster
kubectx kind-eurodat

# Create namespace to test
kubectl create namespace network-policy-test

# Run nginx to test against
kubectl -n network-policy-test run web --image=nginx --labels="app=web" --expose --port=80

# Run a temporary pod to check request -> you should see the html written to stdout.
kubectl -n network-policy-test run --rm -it --restart=Never --image=busybox test-$RANDOM -- sh -c "wget -qO- --timeout=20 http://web && exit"

# Apply network policy
cat <<EOF | kubectl -n network-policy-test apply -f -
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: web-deny-all
spec:
  podSelector:
    matchLabels:
      app: web
  ingress: []
EOF

# Check again if the pod is reachable. If everything works as expected, after 20 seconds the bot should return a timeout.
kubectl -n network-policy-test run --rm -it --restart=Never --image=busybox test-$RANDOM -- sh -c "wget -qO- --timeout=20 http://web && exit"

# Cleanup
kubectl -n network-policy-test delete pod web
kubectl -n network-policy-test delete service web
kubectl -n network-policy-test delete networkpolicy web-deny-all
kubectl delete namespace network-policy-test
