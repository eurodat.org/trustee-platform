#!/usr/bin/env bash

# Script used to build mock images in the local development setup
# Usage: ./build_mock_image.sh <app-name> <context>

set -e
APP_NAME=$1

docker build -t localhost:5002/"$APP_NAME":latest cluster/scripts/"$APP_NAME"
docker image push localhost:5002/"$APP_NAME"
echo "$APP_NAME" image build and pushed to local registry
