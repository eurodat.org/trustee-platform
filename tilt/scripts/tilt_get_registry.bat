@ECHO OFF
FOR /F "tokens=* USEBACKQ" %%F IN (`ctlptl get cluster kind-eurodat -o template --template '{{.status.localRegistryHosting.host}}'`) DO (
SET IMG_REGISTRY_NAME=%%F
)
:: Remove quotes
SET IMG_REGISTRY_NAME=%IMG_REGISTRY_NAME:'=%
ECHO %IMG_REGISTRY_NAME%
