---
.test-setup:
  stage: extended-test
  needs: []
  image: cypress/browsers:node-22.0.0-chrome-124.0.6367.60-1-ff-125.0.2-edge-124.0.2478.51-1@sha256:02eaae11b7d3dc9f408a5f64c3990faea75943d612ea86b8b218d95b48f22ce8
  rules:
    - if: $EXTENDED_TESTS == "true"
  variables:
    BROWSER_SCRIPT: test-component-test
  # Create the environment.standalone.ts file referenced in cypress.config.ts
  before_script:
    - |
      #!/usr/bin/env sh
      cd $CI_PROJECT_DIR/frontend/eurodat-app
      mkdir -p environments
      cat << EOF > environments/environment.standalone.ts
      export const environment = {
        clusterUrl: 'https://$CLUSTER_FQDN/',
        keycloakUrl: 'https://$CLUSTER_FQDN/auth/',
        keycloakRealm: 'eurodat',
        keycloakClient: 'eurodat-app',
        useKeycloak: '',
        eurodatAppCssSource: 'https://eurodat.org/fileadmin/templates/css/styles.css'
      }
      EOF
      cat environments/environment.standalone.ts
  script:
    - |
      #!/usr/bin/env sh
      cd $CI_PROJECT_DIR/frontend/eurodat-app
      npm ci
      npm run $BROWSER_SCRIPT


frontend:
  stage: build
  needs: []
  rules:
    - if: $BUILD == "true"
  image: docker:24.0.6-dind-alpine3.18
  services:
    - name: docker:24.0.6-dind
      alias: thedockerhost
      # Explicitly disable tls to avoid docker startup interruption
      command: ["--tls=false"]
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://thedockerhost:2375/
    DOCKER_TLS_CERTDIR: "" # location of cert shared by the job-container and svc-container
  script:
    - |
      #!/usr/bin/env sh
    - apk add uuidgen
    - echo "$IMG_REGISTRY_SECRET" | docker login -u "$IMG_REGISTRY_USERNAME" --password-stdin "$IMG_REGISTRY_NAME"
    - sh "${CI_PROJECT_DIR}/cluster/scripts/publish_frontend_image.sh" "${CI_PROJECT_DIR}/frontend" "$IMG_REGISTRY_NAME" "$IMG_TAG"
    - export IMG_NAME="eurodat-app"
    - export IMAGE_REFERENCE_WITH_DIGEST="$IMG_REGISTRY_NAME/$IMG_NAME:$IMG_TAG@$(docker inspect --format='{{index .RepoDigests 0}}' "$IMG_REGISTRY_NAME/$IMG_NAME:$IMG_TAG" | cut -d '@' -f 2)"
    - . "$CI_PROJECT_DIR/cluster/scripts/fail_if_image_not_present.sh" "$IMAGE_REFERENCE_WITH_DIGEST"
    - # shellcheck disable=SC1035 # gitlab-ci-syntax misinterpreted by shellcheck
      !reference [".build:save-image-reference", "script"]
  artifacts:
    paths:
      - image_references/*
    expire_in: 1 day

####################################
# Frontend Cypress component-tests #
####################################

# tests using a non-default browser
chrome-component-test:
  extends:
    - .test-setup
  variables:
    BROWSER_SCRIPT: chrome-component-test
firefox-component-test:
  extends:
    - .test-setup
  variables:
    BROWSER_SCRIPT: firefox-component-test

edge-component-test:
  extends:
    - .test-setup
  variables:
    BROWSER_SCRIPT: edge-component-test

# tests using the latest images
chrome-latest-component-test:
  extends:
    - .test-setup
  image: cypress/browsers:latest

firefox-latest-component-test:
  extends:
    - .test-setup
  image: cypress/browsers:latest
  variables:
    BROWSER_SCRIPT: firefox-component-test

edge-latest-component-test:
  extends:
    - .test-setup
  image: cypress/browsers:latest
  variables:
    BROWSER_SCRIPT: edge-component-test

########################################
# Frontend coverage of component tests #
########################################

frontend:component-test:
  # runs default browser component tests
  extends:
    - .test-setup
  stage: build
  rules:
    - if: $BUILD == "true"
  script:
    - |
      #!/usr/bin/env sh
      cd $CI_PROJECT_DIR/frontend/eurodat-app
      npm ci
      npm run test-component-test
  artifacts:
    paths:
      - frontend/eurodat-app/.nyc_output
      - frontend/eurodat-app/coverage-component/lcov.info
    expire_in: 1 day

frontend:sonarcloud:
  # Creates the sonarcloud coverage report
  interruptible: true
  stage: code_quality
  needs:
    - frontend:component-test
  rules:
    - if: $BUILD == "true"
  variables:
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
    SONAR_HOST_URL: "https://sonarcloud.io"
    SONAR_ORGANISATION: "eurodat-org"
    SONAR_USER_HOME: "$CI_PROJECT_DIR/.sonar" # Defines the location of the analysis task cache
    SONAR_PROJECT_KEY: $SONARCLOUD_TEAM_IDENTIFIER-frontend
  image:
    name: sonarsource/sonar-scanner-cli:10
    entrypoint: [""]
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - |
      #!/bin/bash
      cd frontend/eurodat-app
      sonar-scanner -Dsonar.javascript.lcov.reportPaths=coverage-component/lcov.info \
        -Dsonar.qualitygate.wait=true \
        -Dsonar.projectKey="$SONAR_PROJECT_KEY" \
        -Dsonar.projectName="$SONAR_PROJECT_KEY" \
        -Dsonar.organization="$SONAR_ORGANISATION" \
        -Dsonar.host.url="$SONAR_HOST_URL" \
        -Dsonar.token="$SONAR_TOKEN"

########################################################################
# eslint, stylelint, htmlhint (code quality) and prettier (formatting) #
########################################################################
.linter-rules:
  rules:
    - if: $LINT == "true"

.npm-setup:
  image: node:20
  needs: []
  variables:
    NPM_SCRIPT: prettier-ci
  script:
    - |
      #!/usr/bin/env sh
      cd $CI_PROJECT_DIR/frontend/eurodat-app
      npm ci --cache $CI_PROJECT_DIR/.npm --prefer-offline
      npm run $NPM_SCRIPT
  cache:
    key: npm # see https://how-to.dev/how-to-setup-npm-caching-on-gitlab
    paths:
      - .npm/

linter:prettier:
  stage: code_quality
  extends:
    - .npm-setup
    - .linter-rules
  variables:
    NPM_SCRIPT: prettier-ci

linter:stylelint:
  stage: code_quality
  extends:
    - .npm-setup
    - .linter-rules
  variables:
    NPM_SCRIPT: stylelint-ci

linter:eslint:
  stage: code_quality
  extends:
    - .npm-setup
    - .linter-rules
  variables:
    NPM_SCRIPT: eslint-ci

linter:htmlhint:
  stage: code_quality
  extends:
    - .npm-setup
    - .linter-rules
  variables:
    NPM_SCRIPT: htmlhint
