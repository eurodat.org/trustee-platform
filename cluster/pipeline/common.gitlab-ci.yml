#################################
# Snippets for widely used Jobs #
#################################

# Build template for gradle jobs
.build:template:
  # Interrupt job if new pipeline has started
  interruptible: true
  stage: build
  # Disable gradle daemon for by setting the corresponding CL parameter and aligning the client JVM settings with
  # those required by the build JVM.
  variables:
    GRADLE_OPTS: "-Dorg.gradle.daemon=false"
    JAVA_OPTS: "-Xmx4g -XX:MaxMetaspaceSize=1g -XX:+HeapDumpOnOutOfMemoryError -Dfile.encoding=UTF-8"
# Creates a file in image_references/* containing $IMAGE_REFERENCE_WITH_DIGEST
# File is collected in a later stage
.build:save-image-reference:
  script:
    - |
      #!/usr/bin/env sh
    - mkdir -p "$CI_PROJECT_DIR/image_references"
    - echo "Full image reference is $IMAGE_REFERENCE_WITH_DIGEST"
    - echo "$IMAGE_REFERENCE_WITH_DIGEST" > "$CI_PROJECT_DIR/image_references/$(uuidgen)"

# sonarcloud-analysis template
.sonarcloud:template:
  stage: code_quality
  extends:
    - .build:template
  interruptible: true
  cache:
    policy: pull
    key: "$CI_JOB_NAME"
    paths:
      - .sonar/cache
  variables:
    SONAR_HOST_URL: "https://sonarcloud.io"
    SONAR_ORGANISATION: "eurodat-org"
    SONAR_USER_HOME: "$CI_PROJECT_DIR/.sonar" # Defines the location of the analysis task cache
    GIT_DEPTH: "0" # Tells git to fetch all the branches of the project, required by the analysis task
    GRADLE_OPTS: "-Dorg.gradle.daemon=true -Dorg.gradle.warning.mode=all" # Jacoco needs the daemon.
  script:
    - |
      #!/usr/bin/env sh
      set -e
      apk add jq gettext && apk add curl
      cd "$CI_PROJECT_DIR/services/$DIRECTORY" || exit 1
      echo "$CI_COMMIT_MESSAGE" | grep -Fq '[info]' && INF0='--info'
      # $EXTRA_TASK needs to run in separate gradle call
      [ -z "$EXTRA_TASK" ] || gradle $INF0 --build-cache "$EXTRA_TASK"
      gradle $INF0 --build-cache jacocoTestReport  sonar \
        -Dsonar.qualitygate.wait=true \
        -Dsonar.projectKey="$SONAR_PROJECT_KEY" \
        -Dsonar.projectName="$SONAR_PROJECT_KEY" \
        -Dsonar.organization="$SONAR_ORGANISATION" \
        -Dsonar.host.url="$SONAR_HOST_URL" \
        -Dsonar.token="$SONAR_TOKEN"

.get-images:
  before_script:
    - |
      #!/usr/bin/env sh
    - cat eurodat_images
    - apk add --no-cache jq

.clone-iac-repo:
  script:
    - |
      #!/usr/bin/env sh
      apk add git
    # login to git and clone IaC repo
    - |
      export GIT_USER_NAME
      GIT_USER_NAME="$(echo "$CI_COMMIT_AUTHOR" | tr -d '<>' | cut -d ' ' -f 1)"
    - |
      export USER_EMAIL
      USER_EMAIL="$(echo "$CI_COMMIT_AUTHOR" | tr -d '<>' | cut -d ' ' -f 2)"
    - git config --global user.name "$GIT_USER_NAME" && git config --global user.email "USER_EMAIL"
    # go one up to get out of the trustee-platform repo
    - cd ..
    # prepare a shallow git clone if $IAC_BRANCH is set
    - echo "IAC_BRANCH is $IAC_BRANCH"
    - |
      # shellcheck disable=1089
      if [ ! -z "$IAC_BRANCH" ]; then
        export COMMAND_OPTION="--depth 1 --branch $IAC_BRANCH";
      fi
    - git clone $COMMAND_OPTION "https://IAC_REPO_READ_ALL_TOKEN:${IAC_REPO_READ_ALL_TOKEN}@gitlab.com/eurodat/eurodat-trustee-iac.git"
    - cd eurodat-trustee-iac
    - export IAC_REPO_DIR="$(pwd)"

.gcloud-authenticate:
  - |
    #!/usr/bin/env sh
  # Make it work for both, apk and apt
  - command -v apk >/dev/null 2>&1 && apk update && apk add jq || true
  - command -v apt >/dev/null 2>&1 && apt update && apt install -y jq || true
  - gcloud auth configure-docker europe-west3-docker.pkg.dev

.fetch-kubeconfig:
  script:
    - |
      #!/usr/bin/env sh
    - set -e
    - !reference [ ".gcloud-authenticate" ]
    - echo "Fetching kubeconfig for $TARGET_CLUSTER_NAME"
    - |
      if [ -z "$TARGET_CLUSTER_NAME" ]; then
        echo "TARGET_CLUSTER_NAME is not set"
        exit 1
      fi

      if [ -z "$TARGET_KUBECONFIG_PATH" ]; then
        echo "TARGET_KUBECONFIG_PATH is not set"
        exit 1
      fi
    # Make it work for both, apk and apt
    - command -v apk >/dev/null 2>&1 && apk update && apk add bash || true
    - command -v apt >/dev/null 2>&1 && apt update && apt install -y bash || true
    - CLUSTER_LOCATION="$(gcloud container clusters list --project "$KUBE_GCP_PROJECT" --filter="name<=$TARGET_CLUSTER_NAME AND name>=$TARGET_CLUSTER_NAME" --format json | jq -r '.[0].location')"
    - echo "cluster location $CLUSTER_LOCATION"
    - mkdir -p "$(dirname $TARGET_KUBECONFIG_PATH)"
    - # gcloud cli writes kubeconfig file to $KUBECONFIG, must be exported
      export KUBECONFIG="$TARGET_KUBECONFIG_PATH"
    - gcloud container clusters get-credentials "$TARGET_CLUSTER_NAME" --region "$CLUSTER_LOCATION" --project "$KUBE_GCP_PROJECT"

.fetch-platform-kubeconfig:
  script:
    - |
      #!/usr/bin/env sh
    - set -e
    # Prepare variables that are needed for .fetch-kubeconfig job
    # Extending the .fetch-kubeconfig job and passing the variables via the variables keyword does not work because those variables can't
    # access other variables and must be hardcoded which is not possible here.
    - TARGET_CLUSTER_NAME="$CLUSTER_NAME"
    - |
      case "$ENVIRONMENT" in
        "development")
          export KUBE_GCP_PROJECT="$GCP_PROJECT_ID_DEV"
          ;;
        "development-nightly")
          export KUBE_GCP_PROJECT="$GCP_PROJECT_ID_DEV"
          ;;
        "qa")
          export KUBE_GCP_PROJECT="$GCP_PROJECT_ID_QA"
          ;;
        "integration")
          export KUBE_GCP_PROJECT="$GCP_PROJECT_ID_INT"
          ;;
        "production")
          export KUBE_GCP_PROJECT="$GCP_PROJECT_ID_PROD"
          ;;
        *)
          echo "No kubeconfig for stage $ENVIRONMENT available!"
          exit 1
          ;;
      esac
    - TARGET_KUBECONFIG_PATH="$CI_PROJECT_DIR/.kube/$KUBE_CONFIG_FILE_NAME_INT"
    - echo "Target cluster name is $TARGET_CLUSTER_NAME"
    - echo "Target kubeconfig path is $TARGET_KUBECONFIG_PATH"
    - # shellcheck disable=SC1035 # gitlab-ci-syntax misinterpreted by shellcheck
      !reference [ ".fetch-kubeconfig", "script" ]

.fetch-mgmt-kubeconfig:
  script:
    - |
      #!/usr/bin/env sh
    - set -e
    # Prepare variables that are needed for .fetch-kubeconfig job
    # Extending the .fetch-kubeconfig job and passing the variables via the variables keyword does not work because those variables can't
    # access other variables and must be hardcoded which is not possible here.
    - TARGET_CLUSTER_NAME="$CLUSTER_NAME_MGMT"
    - |
      case "$ENVIRONMENT" in
        "development")
          export KUBE_GCP_PROJECT="$GCP_PROJECT_ID_DEV_MGMT"
          ;;
        "development-nightly")
          export KUBE_GCP_PROJECT="$GCP_PROJECT_ID_DEV_MGMT"
          ;;
        "qa")
          export KUBE_GCP_PROJECT="$GCP_PROJECT_ID_QA_MGMT"
          ;;
        "integration")
          export KUBE_GCP_PROJECT="$GCP_PROJECT_ID_INT_MGMT"
          ;;
        "production")
          export KUBE_GCP_PROJECT="$GCP_PROJECT_ID_PROD_MGMT"
          ;;
        *)
          echo "No kubeconfig for stage $ENVIRONMENT available!"
          exit 1
          ;;
      esac
    - TARGET_KUBECONFIG_PATH="$CI_PROJECT_DIR/.kube/kubeconfig-$ENVIRONMENT-mgmt-gcp"
    - echo "Target cluster name is $TARGET_CLUSTER_NAME"
    - echo "Target kubeconfig path is $TARGET_KUBECONFIG_PATH"
    - # shellcheck disable=SC1035 # gitlab-ci-syntax misinterpreted by shellcheck
      !reference [ ".fetch-kubeconfig", "script" ]

.install-gcloud-cli-apt:
  - |
    #!/usr/bin/env sh
  - apt update && apt --yes install bash python3 curl
  - curl -sSL https://sdk.cloud.google.com | bash
  - export PATH=$PATH:/root/google-cloud-sdk/bin
  - gcloud components install gke-gcloud-auth-plugin


