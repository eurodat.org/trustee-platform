#################################
# Job Definitions | App-Service #
#################################

# Template applied to all jobs concerning app-service
---
.app-service:template:
  artifacts:
    reports:
      junit:
        - services/app-service/build/test-results/**/TEST-*.xml
    paths:
      - services/app-service/build
      - image_references/*
    expire_in: 1 day
  before_script:
    - |
      #!/usr/bin/env sh
      cd services/app-service || exit 1
      cp -r "$CI_PROJECT_DIR/.m2" ~/.m2 || echo "Local Maven Repositories not found"
  # Cache all gradle caches between jobs to speed up building and testing
  cache:
    key:
      files:
        - services/app-service/build.gradle.kts
        - services/app-service/settings.gradle.kts
    paths:
      - .gradle/caches
      - services/app-service/.gradle
  variables:
    DEFAULT_NAMESPACE: base

# Build for e2e and main branch
app-service:
  services:
    - name: docker:24.0.6-dind
      # Explicitly disable tls to avoid docker startup interruption
      command: ["--tls=false"]
  variables:
    DOCKER_HOST: "tcp://docker:2375"
    DOCKER_TLS_CERTDIR: ""
    DOCKER_DRIVER: overlay2
  extends:
    - .app-service:template
    - .build:template
  needs:
    - argo-client
  script:
    - |
      #!/usr/bin/env sh
      echo "$CI_COMMIT_MESSAGE" | grep -Fq '[info]' && INFO='--info'
    - echo "Using image tag $IMG_TAG"
    - echo "Using image registry name $IMG_REGISTRY_NAME"
    - |
      gradle $INFO build -Dquarkus.container-image.username="$IMG_REGISTRY_USERNAME" \
        -Dquarkus.container-image.password="$IMG_REGISTRY_SECRET" \
        -Dquarkus.container-image.build=true \
        -Dquarkus.container-image.push=true \
        -Dquarkus.container-image.image="$IMG_REGISTRY_NAME/app-service:$IMG_TAG" \
        -Dquarkus.jib.image-digest-file="$CI_PROJECT_DIR/img_digest_file"
    - mv build/reports/jacoco/test/jacoco.xml build/reports/jacoco/test/jacocoTestReport.xml
    - export IMAGE_REFERENCE_WITH_DIGEST
    - IMAGE_REFERENCE_WITH_DIGEST="$IMG_REGISTRY_NAME/app-service:$IMG_TAG@$(cat "$CI_PROJECT_DIR"/img_digest_file)"
    - . "$CI_PROJECT_DIR/cluster/scripts/fail_if_image_not_present.sh" "$IMAGE_REFERENCE_WITH_DIGEST"
    - # shellcheck disable=SC1035
      !reference [".build:save-image-reference", "script"]
  rules:
    - if: $BUILD == "true"

app-service:uploader:
  image: docker:24.0.6-dind-alpine3.18
  stage: build
  needs: []
  rules:
    - if: $BUILD == "true"
  services:
    - name: docker:24.0.6-dind
      alias: thedockerhost
      # Explicitly disable tls to avoid docker startup interruption
      command: ["--tls=false"]
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://thedockerhost:2375/
    DOCKER_TLS_CERTDIR: "" # location of cert shared by the job-container and svc-container
    IMG_NAME: app-service-uploader
  script:
    - |
      #!/usr/bin/env sh
    - apk add uuidgen
    - cd services/app-service-uploader || exit 1
    - echo "$IMG_REGISTRY_SECRET" | docker login -u "$IMG_REGISTRY_USERNAME" --password-stdin "$IMG_REGISTRY_NAME"
    - docker build -f Dockerfile -t "$IMG_REGISTRY_NAME/$IMG_NAME:$IMG_TAG" .
    - docker push "$IMG_REGISTRY_NAME/$IMG_NAME:$IMG_TAG"
    - IMAGE_REFERENCE_WITH_DIGEST="$IMG_REGISTRY_NAME/$IMG_NAME:$IMG_TAG@$(docker inspect --format='{{index .RepoDigests 0}}' "$IMG_REGISTRY_NAME/$IMG_NAME:$IMG_TAG" | cut -d '@' -f 2)"
    - . "$CI_PROJECT_DIR/cluster/scripts/fail_if_image_not_present.sh" "$IMAGE_REFERENCE_WITH_DIGEST"
    - # shellcheck disable=SC1035 # gitlab-ci-syntax misinterpreted by shellcheck
      !reference [".build:save-image-reference", "script"]
  after_script:
    - |
      #!/usr/bin/env sh
      docker logout
  artifacts:
    paths:
      - image_references/*
    expire_in: 1 day

# SonarScan
app-service:sonarcloud:
  image: gradle:8.8-jdk17-alpine
  variables:
    SONAR_PROJECT_KEY: $SONARCLOUD_TEAM_IDENTIFIER-app-service
    DIRECTORY: "app-service"
  extends:
    - .app-service:template
    - .sonarcloud:template
  needs:
    - app-service
    - argo-client
  rules:
    - if: $BUILD == "true"
