#!/usr/bin/env bash
set -euo pipefail
# fail if today's nightly does not exist
LATEST_NIGHTLY_VERSION="$(curl -s "https://gitlab.com/api/v4/projects/33611450/packages?package_type=helm&sort=desc&per_page=5000" \
    | jq -rc '.[] | select( .version | contains("nightly")) | .version' \
    | sort -ur | head -n 1)"

DATE=$(date +%Y%m%d)
if ! echo "$LATEST_NIGHTLY_VERSION" | grep -q "nightly$DATE"; then
  >&2 echo "Nightly builds for $DATE not found, exiting"
  exit 1
else
  # not quoting because we want only the first value
  # shellcheck disable=SC2086
  echo $LATEST_NIGHTLY_VERSION
fi
