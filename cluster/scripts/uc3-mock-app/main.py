import os
from psycopg2.extras import RealDictCursor
import psycopg2
import logging

logger = logging.getLogger(__name__)
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.INFO)

db_host = os.getenv("PG_HOSTNAME")
db_name = os.getenv("PG_DATABASENAME")
user_name = os.getenv("PG_USERNAME")
pwd = os.getenv("PG_PASSWORD")


def main():
    connection = {}
    cursor = {}
    try:
        logger.info("Using user password for authentication.")

        connection["psql"] = psycopg2.connect(
            host=db_host, dbname=db_name, user=user_name, password=pwd
        )
        connection["psql"].autocommit = True
        cursor["psql"] = connection["psql"].cursor(cursor_factory=RealDictCursor)

        logger.info("Reading data from input table")
        query = """SELECT
                    input_id,
                    poll_id,
                    submission,
                    participant_identifier
                    FROM input.input;"""
        cursor["psql"].execute(query)
        queried_data = cursor["psql"].fetchall()

        unique_poll_ids = list({row["poll_id"] for row in queried_data})
        result_dict = {}
        for poll_id in unique_poll_ids:
            result_dict[poll_id] = sum(
                [row["submission"] for row in queried_data if row["poll_id"] == poll_id]
            )
        logger.info("Inserting data to output table")
        for row in queried_data:
            values = (
                row["input_id"],
                row["poll_id"],
                row["submission"],
                row["participant_identifier"],
                result_dict[row["poll_id"]],
            )

            query = """INSERT INTO output.output (
                      output_id,
                      poll_id,
                      submission,
                      participant_identifier,
                      result
                      ) VALUES (%s, %s, %s, %s, %s);"""

            cursor["psql"].execute(query, values)
    except psycopg2.Error as exc:
        logger.error(f"Database error: {exc}")
        raise
    except Exception as exc:
        logger.error(f"Unexpected error: {exc}")
        raise
    finally:
        if cursor.get("psql"):
            cursor["psql"].close()
        if connection.get("psql"):
            connection["psql"].close()


if __name__ == "__main__":
    main()
