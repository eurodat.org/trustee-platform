#!/usr/bin/env bash
# Checks if all images are in provided ACR and if not copies docker images from main branch.
# read the options
TEMP=$(getopt -o r:u:p:b: --long registry:,user:,password:,branch: -- "$@")
eval set -- "$TEMP"
while true ; do
  case "$1" in
    -r|--registry) REGISTRY="$2"; shift 2 ;;
    -u|--user) USER="$2"; shift 2 ;;
    -p|--password) PASSWORD="$2"; shift 2 ;;
    -b|--branch) BRANCH="$2"; shift 2 ;;
    --) shift ; break ;;
  esac
done

[ -z "$BRANCH" ] && BRANCH="$(echo "$COMMIT_BRANCH" | tr '/' '-')"

_image_exists() {
  skopeo inspect "docker://$REGISTRY/$1:$2" >/dev/null 2>&1
  echo $?
  }

copy_image() {
  local image="$1"
  local REGISTRY="$2"

  echo "Checking if image for $image exists for branch $BRANCH"
  if [ ! "$(_image_exists "$image" "$BRANCH-latest")" = 0 ]; then
    echo "Image $image does not exist for branch $BRANCH"
    if [ ! "$(_image_exists "$image" "main-latest")" = 0 ]; then
      echo "Image $image does not exist for main branch. You may need to run the whole pipeline."
      exit 1
    else
      echo "Image $image not found, will use the main-branch version."
      docker pull "$REGISTRY/$image:main-latest"
      docker tag \
        "$REGISTRY/$image:main-latest" \
        "$REGISTRY/$image:$BRANCH-latest"
      docker push "$REGISTRY/$image:$BRANCH-latest"
    fi
    else
      echo "Found image for $image"
  fi
}

echo "$PASSWORD" | skopeo login -u "$USER" --password-stdin "$REGISTRY"

while IFS= read -r image; do
  copy_image "$image" "$REGISTRY"
done < eurodat_images
