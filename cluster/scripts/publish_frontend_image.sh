#!/usr/bin/env sh
# Builds and creates a docker image for the EuroDaT Web frontend.
# $1 is the path to the EuroDaT Web frontend Dockerfile.
# $2 is the registry name that the image should be pushed to.
# $3 is the tag that should be used for this image.
set -eu

image_name="eurodat-app"
full_image_path="${2}/${image_name}:${3}"
docker build -t "${full_image_path}" "${1}"
docker tag "${full_image_path}" "${full_image_path}"
echo "Push image to ${full_image_path}"
docker push "${full_image_path}"
