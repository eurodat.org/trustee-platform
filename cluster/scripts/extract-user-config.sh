#!/usr/bin/env sh

# Extracts the keys from the given UNIQUE_VARIABLES file for the given user.
# Requires jq to be installed
# This script will likely be sourced by other scripts, so pay attention
# when exiting or setting/unsetting stuff
# First input is the path to the UNIQUE_VARIABLES json file
# Second input is the user name

set -e

UNIQUE_VARIABLES_PATH=$1
USER=$2

USER_VARIABLES=$(jq --arg USER "$USER" '.[$USER] // empty' "$UNIQUE_VARIABLES_PATH" )
if [ -z "$USER_VARIABLES" ]; then
  echo "User $USER not found in UNIQUE_VARIABLES"
  exit 1
else
  UNIQUE_NAME="$(echo "$USER_VARIABLES" | jq -r '.UNIQUE_NAME // empty')"
  export UNIQUE_NAME

  if [ -z "$UNIQUE_NAME" ]; then
    echo "Unique name is not set for user $USER. Check the UNIQUE_VARIABLES CI/CD variable."
    exit 1 # Here we want to exit since this is a show stopper
  fi

  CONFIG_DIR="eurodat-stages/dev/dev$UNIQUE_NAME"
  export CONFIG_DIR
  EXTERNAL_LB="$(echo "$USER_VARIABLES" | jq -r '.EXTERNAL_LB // empty')"
  if [ "$EXTERNAL_LB" = "true" ]; then
  CLUSTER_FQDN="app$UNIQUE_NAME.dev.eurodat.org"
  else
  CLUSTER_FQDN="app$UNIQUE_NAME.dev.private.eurodat.org"
  fi
  export CLUSTER_FQDN

fi
