from psycopg2.extras import RealDictCursor
import psycopg2
import os
import json
import logging

logger = logging.getLogger(__name__)
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.INFO)


def main():
    connection_trans_db = {}
    cursor_trans_db = {}
    connection_safe_deposit = {}
    cursor_safe_deposit = {}

    try:
        logger.info(
            "Using user password for authentication to login to transaction db."
        )
        connection_trans_db["psql"] = psycopg2.connect(
            host=db_host, dbname=db_name, user=user_name, password=pwd
        )
        connection_trans_db["psql"].autocommit = True
        cursor_trans_db["psql"] = connection_trans_db["psql"].cursor(
            cursor_factory=RealDictCursor
        )

        logger.info("Reading json ids form input table")
        cursor_trans_db["psql"].execute("""SELECT uuid_json from input.json""")
        requested_json_ids = cursor_trans_db["psql"].fetchall()
        requested_json_id_list = list({row["uuid_json"] for row in requested_json_ids})

        logger.info("Reading pdf ids form input table")
        cursor_trans_db["psql"].execute("""SELECT uuid_pdf from input.pdf""")
        requested_pdf_ids = cursor_trans_db["psql"].fetchall()
        requested_pdf_id_list = list({row["uuid_pdf"] for row in requested_pdf_ids})

        logger.info(
            "Using user password for authentication to login to the safe deposit db."
        )
        app_id = "minaboapp"
        client_id = "data_consumer"
        db_name_safe_deposit = "safedeposit_" + app_id + "_" + client_id
        connection_safe_deposit["psql"] = psycopg2.connect(
            host=db_host, dbname=db_name_safe_deposit, user=user_name, password=pwd
        )
        connection_safe_deposit["psql"].autocommit = True
        cursor_safe_deposit["psql"] = connection_safe_deposit["psql"].cursor(
            cursor_factory=RealDictCursor
        )

        logger.info("Reading JSON data form table in the safe deposit db")
        query = (
            "SELECT uuid_json, blob_json from safedeposit.json WHERE uuid_json IN (%s)"
            % ", ".join(list(map(lambda x: "%s", requested_json_id_list)))
        )
        cursor_safe_deposit["psql"].execute(query, requested_json_id_list)
        safe_deposit_json_data = cursor_safe_deposit["psql"].fetchall()

        logger.info("Reading PDF data form table in the safe deposit db")
        query = (
            "SELECT uuid_pdf, blob_pdf from safedeposit.pdf WHERE uuid_pdf IN (%s)"
            % ", ".join(list(map(lambda x: "%s", requested_pdf_id_list)))
        )
        cursor_safe_deposit["psql"].execute(query, requested_pdf_id_list)
        safe_deposit_pdf_data = cursor_safe_deposit["psql"].fetchall()

        cursor_safe_deposit["psql"].close()
        connection_safe_deposit["psql"].close()

        logger.info("Writing json data to output table")
        query = """INSERT INTO output.json (
                                  uuid_json,
                                  blob_json
                                  ) VALUES (%s, %s);"""
        for row in safe_deposit_json_data:
            cursor_trans_db["psql"].execute(
                query, (row["uuid_json"], json.dumps(row["blob_json"]))
            )

        logger.info("Writing pdf data to output table")
        query = """INSERT INTO output.pdf (
                                  uuid_pdf,
                                  blob_pdf
                                  ) VALUES (%s, %s);"""
        for row in safe_deposit_pdf_data:
            cursor_trans_db["psql"].execute(query, (row["uuid_pdf"], row["blob_pdf"]))
    except psycopg2.Error as exc:
        logger.error(f"Database error: {exc}")
        raise
    except Exception as exc:
        logger.error(f"Unexpected error: {exc}")
        raise
    finally:
        if cursor_trans_db.get("psql"):
            cursor_trans_db["psql"].close()
        if connection_trans_db.get("psql"):
            connection_trans_db["psql"].close()
        if cursor_safe_deposit.get("psql"):
            cursor_safe_deposit["psql"].close()
        if connection_safe_deposit.get("psql"):
            connection_safe_deposit["psql"].close()


db_host = os.environ["PG_HOSTNAME"]
db_name = os.environ["PG_DATABASENAME"]
user_name = os.environ["PG_USERNAME"]
pwd = os.environ["PG_PASSWORD"]

if __name__ == "__main__":
    main()
