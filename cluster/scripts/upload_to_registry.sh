#!/usr/bin/env bash

# This script uploads an image to a cluster's associated UC image registry.

set -euo pipefail

VALID_ARGS=$(getopt -o arkcid --long app-name:,app-registry:,kubeconfig:,context:,src-image-ref:,src-creds: -- "$@")
if [[ $? -ne 0 ]]; then
    exit 1;
fi

if [ -z "$APP_REGISTRY" ]; then
  echo "No app registry is set in environment. Aborting"
  exit 1;
fi

eval set -- "$VALID_ARGS"

usage() {
  echo "Usage: $0 -a <app-name> -k <kubeconfig> -c <context> -i <src-image-ref> -d <src-creds>"
  echo "Example: $0 -a my-app -k ~/.kube/config -c my-cluster -i my-image:latest -d my-username:my-password"
  exit 1
}

while true
do
  case "$1" in
    --app-name)
        APP_NAME="$2"
        shift 2 ;;
    --app-registry)
        APP_REGISTRY="$2"
        shift 2 ;;
    --src-image-ref)
        SRC_IMAGE_REF="docker://$2"
        shift 2 ;;
    --src-creds)
        SRC_CREDS="--src-creds $2"
        shift 2 ;;
    --) shift;
        break
        ;;
    *) usage
        ;;
  esac
done

TAG=$(echo "$SRC_IMAGE_REF" | rev | cut -d':' -f1 | rev)
IMAGE_REF=$APP_NAME:$TAG

DEST_IMAGE_REF=docker://$APP_REGISTRY/$IMAGE_REF
echo "Copying $IMAGE_REF to $DEST_IMAGE_REF"
# shellcheck disable=SC2086
skopeo --insecure-policy copy --format=oci $SRC_CREDS $SRC_IMAGE_REF $DEST_IMAGE_REF
