from psycopg2.extras import RealDictCursor, execute_values
import os
import psycopg2
import logging

logger = logging.getLogger(__name__)
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.INFO)


def main():
    connection = {}
    cursor = {}
    try:
        logger.info("Using user password for authentication.")
        connection["psql"] = psycopg2.connect(
            host=db_host, dbname=db_name, user=user_name, password=pwd
        )
        connection["psql"].autocommit = True
        cursor["psql"] = connection["psql"].cursor(cursor_factory=RealDictCursor)

        logger.info("reading data form input table")
        query = "SELECT transaction_id, bank_transaction_id, created_at, originator_iban, counterparty_iban, transaction_type, amount, originating_bank_bic, counterparty_bank_bic, currency FROM input.input_transactions;"
        cursor["psql"].execute(query)

        logger.info("inserting data into intermediate table")
        dictionary = cursor["psql"].fetchall()
        for entry in dictionary:
            values = tuple(entry.values())
            query = "INSERT INTO intermediate.intermediate_transactions (transaction_id, bank_transaction_id, created_at, originator_iban, beneficiary_iban, transaction_type, amount, originator_bank_bic, beneficiary_bank_bic, currency) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
            cursor["psql"].execute(query, values)

        logger.info(
            "reading from intermediate (inserted by hand) and writing into output table as user internal"
        )
        query = "SELECT transaction_id, bank_transaction_id, created_at, originator_bank_bic, beneficiary_bank_bic, originator_iban, beneficiary_iban, amount,  currency, transaction_type FROM intermediate.intermediate_transactions;"
        cursor["psql"].execute(query)
        queried_data = cursor["psql"].fetchall()

        logger.info("inserting data into output table readable for user A")
        for row in queried_data:
            queryA = """INSERT INTO output.output_transactions (
                transaction_id,
                bank_transaction_id,
                transaction_timestamp,
                originating_bank_bic,
                counterparty_bank_bic,
                originator_iban,
                counterparty_iban,
                amount,
                currency,
                transaction_type,
                security_column
                ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"""
            valuesA = tuple(
                [
                    row["transaction_id"],
                    row["bank_transaction_id"],
                    # transaction_timestamp should not be filled with info from created_at in production
                    row["created_at"],
                    row["originator_bank_bic"],
                    row["beneficiary_bank_bic"],
                    row["originator_iban"],
                    row["beneficiary_iban"],
                    row["amount"],
                    row["currency"],
                    row["transaction_type"],
                    "cobaselector",
                ]
            )
            cursor["psql"].execute(queryA, valuesA)

        logger.info("inserting data into output table readable for user B")
        for row in queried_data:
            queryB = """INSERT INTO output.output_transactions (
                transaction_id,
                bank_transaction_id,
                transaction_timestamp,
                originating_bank_bic,
                counterparty_bank_bic,
                originator_iban,
                counterparty_iban,
                amount,
                currency,
                transaction_type,
                security_column
                ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"""
            valuesB = tuple(
                [
                    row["transaction_id"],
                    row["bank_transaction_id"],
                    # transaction_timestamp should not be filled with info from created_at in production
                    row["created_at"],
                    row["originator_bank_bic"],
                    row["beneficiary_bank_bic"],
                    row["originator_iban"],
                    row["beneficiary_iban"],
                    row["amount"],
                    row["currency"],
                    row["transaction_type"],
                    "dbselector",
                ]
            )
            cursor["psql"].execute(queryB, valuesB)
    except psycopg2.Error as exc:
        logger.error(f"Database error: {exc}")
        raise
    except Exception as exc:
        logger.error(f"Unexpected error: {exc}")
        raise
    finally:
        if cursor.get("psql"):
            cursor["psql"].close()
        if connection.get("psql"):
            connection["psql"].close()


db_host = os.environ["PG_HOSTNAME"]
db_name = os.environ["PG_DATABASENAME"]
user_name = os.environ["PG_USERNAME"]
pwd = os.environ["PG_PASSWORD"]

if __name__ == "__main__":
    main()
