#!/usr/bin/env bash
set -e

cd /tmp || exit
rm -rf kubernetes-secret-generator
mkdir kubernetes-secret-generator
cd kubernetes-secret-generator || exit

wget https://github.com/mittwald/kubernetes-secret-generator/archive/refs/tags/v3.4.0.tar.gz
tar xvf v3.4.0.tar.gz

cd kubernetes-secret-generator-3.4.0 || exit

# Determine underlying architecture of Docker environment
export arch
if [ "$(uname -m)" = "arm64" ] || [ "$(uname -m)" = "aarch64" ]; then arch=arm64; else arch=amd64; fi
GOOS=linux GOARCH=$arch CGO_ENABLED=0 go build -o build/_output/bin/kubernetes-secret-generator ./cmd/manager/main.go

# Fix paths in Dockerfile (remove the build/ folder)
sed -i 's/build\///' build/Dockerfile
docker build -t localhost:5002/kubernetes-secret-generator:latest build && docker image push localhost:5002/kubernetes-secret-generator

rm -rf /tmp/kubernetes-secret-generator
