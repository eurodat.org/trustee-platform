import json
import os
import subprocess
import sys
import logging

logger = logging.getLogger(__name__)
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.INFO)


def main():
    try:
        if not messaging_url or not messaging_token:
            logger.error(f"Messaging URL or TOKEN missing! Exiting ...")
            sys.exit(1)

        payload = {"message": "Test Message from Mock App", "selectors": ["dbselector"]}

        curl_command = [
            "curl",
            "-X",
            "POST",
            f"{messaging_url}/messages",
            "-H",
            f"Authorization: Bearer {messaging_token}",
            "-H",
            "Content-Type: application/json",
            "--cacert",
            cert_path,  # only needed for self-signed certs
            "-d",
            json.dumps(payload),
        ]

        result = subprocess.run(curl_command, capture_output=True, text=True)

        if result.returncode == 0:
            sys.exit(0)
        else:
            logger.error(f"Non-zero return code: {result.stderr}")
            sys.exit(1)
    except Exception as e:
        logger.error(f"{__name__} caused exception: {e}")
        raise


messaging_url = os.environ["MESSAGING_URL"]
messaging_token = os.environ["MESSAGING_TOKEN"]
cert_path = os.environ["CA_BUNDLE"]


if __name__ == "__main__":
    main()
