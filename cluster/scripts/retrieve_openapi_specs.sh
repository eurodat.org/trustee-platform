#!/usr/bin/env bash
set -euo pipefail

INTERNAL_CLUSTER_CONFIG="$1"

# Define the URLs of the microservices' OpenAPI specifications
openapi_spec_urls=(
  "https://$CLUSTER_FQDN/api/v1/openapi"
  "https://$CLUSTER_FQDN/api/v1/credential-service/openapi"
  "https://$CLUSTER_FQDN/api/v1/database-service/openapi"
  "https://$CLUSTER_FQDN/api/v1/contract-service/openapi"
  "https://$CLUSTER_FQDN/api/v1/app-service/openapi"
)

output_dir="./openapi_yamls"
mkdir -p $output_dir

# Function to retrieve and concatenate OpenAPI specs for the microservices
function retrieve_openapi_specs {
  local connect_timeout=5      # How many seconds to wait for a connection
  local max_time=30           # Max total time the curl command may take
  local max_attempts=3        # Number of retry attempts per endpoint
  local attempt_delay=2        # Seconds to wait between attempts

  for url in "${openapi_spec_urls[@]}"; do
    
    filename="$(echo "$url" | awk -F/ '{print $(NF-1)}').yaml"
    output="${output_dir}/${filename}"

    echo "Attempting to fetch OpenAPI from: $url"
    local success=false

    for ((counter=1; counter<=max_attempts; counter++)); do
      if curl -sSk --fail-with-body \
                  --connect-timeout "$connect_timeout" \
                  --max-time "$max_time" \
                  "$url" -o "$output"; then
        echo "Successfully fetched $url"
        success=true
        break
      else
        echo "Attempt $counter/$max_attempts to retrieve $url failed."
        if [[ $counter -lt $max_attempts ]]; then
          sleep "$attempt_delay"
        fi
      fi
    done

    if [ "$success" = false ]; then
      echo "ERROR: Failed to retrieve OpenAPI spec from $url after $max_attempts attempts."
      exit 1
    fi

  done
}

# Retrieve and concatenate OpenAPI specs
retrieve_openapi_specs
