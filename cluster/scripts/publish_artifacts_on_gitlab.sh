#!/usr/bin/env sh
set -eu

# Requires $VERSION $HELM_PATH $CHART_REPOSITORY_URL $HELM_REPOSITORY_URL and variables for registry access to be set up
# Set unbound variables to empty string
CI_COMMIT_TAG=${CI_COMMIT_TAG-}

echo "Publishing charts"
cat eurodat_charts

while IFS= read -r chart; do
  helm package "$HELM_PATH/$chart" --version "$VERSION" --app-version "$VERSION" -u
  curl -s -request POST \
    --user "gitlab-ci-token:$CI_JOB_TOKEN" \
    --form "chart=@$chart-$VERSION.tgz" \
    "$CHART_REPOSITORY_URL"
done < eurodat_charts

for chart in "eurodat" "eurodat-cluster"
do
  helm package "$HELM_PATH/$chart" --version "$VERSION" --app-version "$VERSION" -u
  curl -s --request POST \
    --user "gitlab-ci-token:$CI_JOB_TOKEN" \
    --form "chart=@$chart-$VERSION.tgz" \
    "$CHART_REPOSITORY_URL"
done
# upload docker images
export IAC_CRED_PATH="./acr-cred.json"
export CI_CR_CRED_PATH="./ci-cr-cred.json"
skopeo login -u="$IMG_REGISTRY_USERNAME" -p="$IMG_REGISTRY_SECRET" --authfile=$IAC_CRED_PATH --tls-verify=true "$IMG_REGISTRY_NAME"
skopeo login -u="$CI_REGISTRY_USER" -p="$CI_REGISTRY_PASSWORD" --authfile=$CI_CR_CRED_PATH --tls-verify=true "$CI_REGISTRY"

echo "Publishing images"
cat eurodat_images

while IFS= read -r image; do
  skopeo copy \
  --src-authfile $IAC_CRED_PATH \
  --dest-authfile $CI_CR_CRED_PATH \
  "docker://$IMG_REGISTRY_NAME/$image:$COMMIT_BRANCH-latest" \
  "docker://$CI_REGISTRY/eurodat/trustee-platform/$image:$VERSION"
done < eurodat_images
skopeo logout --all

if ! [ "$CI_COMMIT_TAG" = "" ]; then
  sed -n "/## \[ [0-9.]* \] ${CI_COMMIT_TAG}.*/,/---/p" docs/release_notes.md | sed '/^---$/,$d' >> release_notes.md
fi
