#!/usr/bin/env bash
# Generate and publish argo cd's java client to maven local
# Run the script with the '-s' option to skip tha JAVA and GIT tests
# args.:
# -s|--skip-tests  - (optional) will skip system tests


_test_java_home() {
    if [ ! -x "$JAVA_HOME/bin/javac" ]; then
      echo "Your JAVA_HOME environment variable is not set correctly, look into the java.md documentation."
      exit 1
    fi
}

_test_git_long_paths() {
  if [ "$(git config --get core.longpaths)" = false ]; then
    echo 'Allow long path names in windows (you will need to be root): git config --system core.longpaths true'
    exit 1
  fi
}

_err_report() {
    echo "trapped (in line $1)!"
    cd "$2"
    rm -rf "$3"
    exit 42
}

_test_before_start() {
  _test_java_home
  _test_git_long_paths
}

# read the options
TEMP=$(getopt -o s --long skip-tests -- "$@")
eval set -- "$TEMP"
while true ; do
  case "$1" in
    -s|--skip-tests) NO_TEST=true; shift ;;
    --) shift ; break ;;
  esac
done
[ -z "$NO_TEST" ] && _test_before_start

INITIAL_DIRECTORY=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
EURODAT_REPOSITORY_ROOT=$(git rev-parse --show-toplevel)
set -e
trap '_err_report $LINENO $INITIAL_DIRECTORY $TEMPDIR' EXIT

### START
TEMPDIR="/tmp/ARGO_JAVA_CLIENT_BUILD"

# Change tempdir if mingw
UNAME=$(uname -a)
NAME_STRING=${UNAME:0:5}

if [ "$NAME_STRING" = "MINGW" ]; then
  IS_MINGW=true
fi

if [ "$IS_MINGW" ]; then
  TEMPDIR=~/AppData/Local/Temp/ARGO_JAVA_CLIENT_BUILD
fi

# Start installation
rm -rf "$TEMPDIR"
mkdir $TEMPDIR

if [ -d ~/.m2/repository/io/argoproj/workflow/argo-client-java ]; then
  echo "Removing old version of the Argo CD Java Client"
  rm -rf ~/.m2/repository/io/argoproj/workflow/argo-client-java
else
  echo "No previous version of the Argo CD Java Client was found"
fi

git clone https://github.com/argoproj/argo-workflows "$TEMPDIR"
cd "$TEMPDIR/sdks/java"
git checkout tags/v3.5.10

# reimplement install target of ./Makefile due to shell issues
WD="$(pwd)/client"
VERSION="$(git describe --exact-match --tags --abbrev=0 2> /dev/null || echo "Commit not tagged with version")"
STRIPPED_VERSION="${VERSION:1}"
CHOWN="chown -R $(id -u):$(id -g)"

rm -Rf "$WD"
mkdir -p "$WD"
cp settings.xml "$WD/settings.xml"
< "../../api/openapi-spec/swagger.json" sed 's/io.k8s.api.core.v1.//' | sed 's/io.k8s.apimachinery.pkg.apis.meta.v1.//' \
		> "$WD/swagger.json"

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    MSYS_NT*)   machine=Git;;
    *)          machine="UNKNOWN:${unameOut}"
esac

if [ "$IS_MINGW" ]; then
    powershell "docker run --rm -v $WD:/wd --workdir /wd openapitools/openapi-generator-cli:v6.6.0" \
        "generate" \
        "-i /wd/swagger.json" \
        "-g java" \
        "-o /wd" \
        "-p hideGenerationTimestamp=true" \
        "-p library=microprofile" \
        "-p serializationLibrary=jackson" \
        "-p dateLibrary=java8" \
        "-p microprofileMutiny=true" \
        "-p configKey=argo-client" \
        "-p useJakartaEe=true" \
        "-p microprofileRestClientVersion=3.0" \
        "-p openApiNullable=true" \
        "--api-package io.argoproj.workflow.apis" \
        "--invoker-package io.argoproj.workflow" \
        "--model-package io.argoproj.workflow.models" \
        "--skip-validate-spec" \
        "--group-id io.argoproj.workflow" \
        "--artifact-id argo-client-java" \
        "--artifact-version $STRIPPED_VERSION" \
        "--generate-alias-as-model"

        # https://vsupalov.com/docker-shared-permissions/#set-the-docker-user-when-running-your-container
        $CHOWN "$WD" || sudo "$CHOWN" "$WD"
elif [ "$machine" == "Mac" ]; then
    OPENAPI_GENERATOR_VERSION=6.6.0 openapi-generator generate \
        -i client/swagger.json \
        -g java \
        -o client \
        -p hideGenerationTimestamp=true \
        -p library=microprofile \
        -p serializationLibrary=jackson \
        -p dateLibrary=java8 \
        -p microprofileMutiny=true \
        -p configKey=argo-client \
        -p useJakartaEe=true \
        -p microprofileRestClientVersion=3.0 \
        -p openApiNullable=true \
        --api-package io.argoproj.workflow.apis \
        --invoker-package io.argoproj.workflow \
        --model-package io.argoproj.workflow.models \
        --skip-validate-spec \
        --group-id io.argoproj.workflow \
        --artifact-id argo-client-java \
        --artifact-version "$STRIPPED_VERSION" \
        --generate-alias-as-model
else
    OPENAPI_GENERATOR_VERSION=6.6.0 openapi-generator-cli generate \
    		-i client/swagger.json \
    		-g java \
    		-o client \
    		-p hideGenerationTimestamp=true \
    		-p library=microprofile \
    		-p serializationLibrary=jackson \
    		-p dateLibrary=java8 \
    		-p microprofileMutiny=true \
    		-p configKey=argo-client \
    		-p useJakartaEe=true \
    		-p microprofileRestClientVersion=3.0 \
    		-p openApiNullable=true \
    		--api-package io.argoproj.workflow.apis \
    		--invoker-package io.argoproj.workflow \
    		--model-package io.argoproj.workflow.models \
    		--skip-validate-spec \
    		--group-id io.argoproj.workflow \
    		--artifact-id argo-client-java \
    		--artifact-version "$STRIPPED_VERSION" \
    		--generate-alias-as-model
fi

# adding kubernetes-client
find ./client/src/main/java/io/argoproj/workflow/apis -type f -name '*Api.java' -exec sed -i '/^@RegisterRestClient/a @RegisterClientHeaders(AuthorizationHeaderFactory.class)' {} \;
find ./client/src/main/java/io/argoproj/workflow/apis -type f -name '*Api.java' -exec sed -i '/^import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;/a import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders;' {} \;
find ./client/src/main/java/io/argoproj/workflow/apis -type f -name '*Api.java' -exec sed -i '/^import org.apache.cxf.jaxrs.ext.multipart.*;/d' {} \;
sed -n '1N;2N;/org.apache.cxf/{N;N;N;d};P;N;D' client/pom.xml > tmp
sed 's/<dependencies>/<dependencies><dependency><groupId>jakarta.enterprise<\/groupId><artifactId>jakarta.enterprise.cdi-api<\/artifactId><version>4.0.1<\/version><scope>provided<\/scope><\/dependency><dependency><groupId>io.smallrye.reactive<\/groupId><artifactId>mutiny<\/artifactId><version>2.5.8<\/version><\/dependency><dependency><groupId>org.openapitools<\/groupId><artifactId>jackson-databind-nullable<\/artifactId><version>0.2.4<\/version><\/dependency><dependency><groupId>io.quarkus<\/groupId><artifactId>quarkus-resteasy-reactive<\/artifactId><version>3.9.5<\/version><\/dependency><dependency><groupId>io.quarkus.resteasy.reactive<\/groupId><artifactId>resteasy-reactive-client<\/artifactId><version>3.9.5<\/version><\/dependency>/g' tmp > tmp2 && mv tmp2 client/pom.xml
cp "$EURODAT_REPOSITORY_ROOT"/cluster/scripts/AuthorizationHeaderFactory.java ./client/src/main/java/io/argoproj/workflow/apis/
cp "$EURODAT_REPOSITORY_ROOT"/cluster/scripts/TokenService.java ./client/src/main/java/io/argoproj/workflow/apis/
mvn source:jar -f client -DskipTests install

trap EXIT
cd "$INITIAL_DIRECTORY"
