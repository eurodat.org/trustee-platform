#!/usr/bin/env bash
# Check if the ingress has an ip address
# args.:
# -i|--ingress-name     Name of the ingress resource
# -n|--namespace        Name of the ingress namespace

# read the options
TEMP=$(getopt -o i:n: --long ingress-name:,namespace: -- "$@")
eval set -- "$TEMP"
while true ; do
  case "$1" in
    -i|--ingress-name) INGRESS="$2"; shift 2 ;;
    -n|--namespace) NAMESPACE="$2"; shift 2 ;;
    --) shift ; break ;;
  esac
done

t0="$(date +%s)";
while true; do
  external_ip="$(kubectl -n "$NAMESPACE" get "ingress/$INGRESS" --output=jsonpath='{.status.loadBalancer.ingress[0].ip}')"
  if [[ "$external_ip" =~ ^([0-9]+\.)+[0-9]+$ ]]; then
    echo Application can now be reached at http://"$external_ip"
    break
  else
    echo "No external IP provided, only $external_ip."
    echo "Waiting for endpoint... [10s]" && sleep 10;
  fi
  [ "$(echo "$(date +%s) - $t0" | bc)" -gt 300 ] && echo 'timeout...' && exit 1;
done
