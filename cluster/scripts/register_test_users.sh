#!/usr/bin/env bash

set -euo pipefail

help_text () {
  cat << EOF
This script generates certificates and registers the test users with those certificates.
This requires that the kubeconfig contains the configuration for the platform cluster.
  Options:
    --context <context> The context/name of the EuroDaT platform cluster [REQUIRED]
    --cert-target-dir <cert-target-dir> The directory in which the generated certificates and private keys will be stored. [REQUIRED]
EOF
}

while [[ "$#" -gt 0 ]]; do
  case "$1" in
    --context)
        CLUSTER_CONTEXT="$2"
        shift 2 ;;
    --cert-target-dir)
        CERT_TARGET_DIR="$2"
        shift 2 ;;
    *)
        break
        ;;
  esac
done

rm -f "$CERT_TARGET_DIR/data-consumer-private-key.pem" \
  "$CERT_TARGET_DIR/data-consumer-cert.pem" \
  "$CERT_TARGET_DIR/data-provider-private-key.pem" \
  "$CERT_TARGET_DIR/data-provider-cert.pem" \
  "$CERT_TARGET_DIR/app-provider-private-key.pem" \
  "$CERT_TARGET_DIR/app-provider-cert.pem"
MSYS_NO_PATHCONV=1 openssl req -x509 -newkey rsa:4096 \
  -keyout "$CERT_TARGET_DIR/data-consumer-private-key.pem" \
  -out "$CERT_TARGET_DIR/data-consumer-cert.pem" \
  -days 365 -nodes -subj "/C=DE"
MSYS_NO_PATHCONV=1 openssl req -x509 -newkey rsa:4096 \
  -keyout "$CERT_TARGET_DIR/data-provider-private-key.pem" \
  -out "$CERT_TARGET_DIR/data-provider-cert.pem" \
  -days 365 -nodes -subj "/C=DE"
MSYS_NO_PATHCONV=1 openssl req -x509 -newkey rsa:4096 \
  -keyout "$CERT_TARGET_DIR/app-provider-private-key.pem" \
  -out "$CERT_TARGET_DIR/app-provider-cert.pem" \
  -days 365 -nodes -subj "/C=DE"

SCRIPT_DIR=$(dirname "$0")

bash "$SCRIPT_DIR/register_client.sh" \
  --context "$CLUSTER_CONTEXT"\
  --tls-cert-path "$CERT_TARGET_DIR/data-consumer-cert.pem"\
  --client-id "data_consumer"\
  --client-selector "dbselector"\
  --purpose "TRANSACTIONS"\
  --purpose "SAFEDEPOSITS"

bash "$SCRIPT_DIR/test_client.sh" \
  --context "$CLUSTER_CONTEXT"\
  --private-key-path "$CERT_TARGET_DIR/data-consumer-private-key.pem"\
  --client-id "data_consumer"

bash "$SCRIPT_DIR/register_client.sh" \
  --context "$CLUSTER_CONTEXT"\
  --tls-cert-path "$CERT_TARGET_DIR/data-provider-cert.pem"\
  --client-id "data_provider"\
  --client-selector "cobaselector"\
  --purpose "TRANSACTIONS"\
  --purpose "SAFEDEPOSITS"

bash "$SCRIPT_DIR/test_client.sh" \
  --context "$CLUSTER_CONTEXT"\
  --private-key-path "$CERT_TARGET_DIR/data-provider-private-key.pem"\
  --client-id "data_provider"

bash "$SCRIPT_DIR/register_client.sh" \
  --context "$CLUSTER_CONTEXT"\
  --tls-cert-path "$CERT_TARGET_DIR/app-provider-cert.pem"\
  --client-id "app_provider"\
  --client-selector "appselector"\
  --purpose "APPS"

bash "$SCRIPT_DIR/test_client.sh" \
  --context "$CLUSTER_CONTEXT"\
  --private-key-path "$CERT_TARGET_DIR/app-provider-private-key.pem"\
  --client-id "app_provider"
