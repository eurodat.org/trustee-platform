#!/bin/sh
# Fails if the image in $1 does not exist.
# Needs the variables $IMG_REGISTRY_USERNAME, $IMG_REGISTRY_SECRET, and $IMG_REGISTRY_NAME

eval set -e
DID_LOGIN=false

logout()
{
  if $DID_LOGIN; then
    echo "Logging out of $IMG_REGISTRY_NAME"
    docker logout "$IMG_REGISTRY_NAME"
  fi
}

if grep "$IMG_REGISTRY_NAME" ~/.docker/config.json >/dev/null 2>&1; then
  echo "Using existing login to $IMG_REGISTRY_NAME"
else
  echo "$IMG_REGISTRY_SECRET" | docker login -u "$IMG_REGISTRY_USERNAME" --password-stdin "$IMG_REGISTRY_NAME"
  DID_LOGIN=true
  echo "Logged in to $IMG_REGISTRY_NAME"
fi

if ! docker buildx imagetools inspect "$1" >/dev/null 2>&1; then
  echo "Image push did not succeed, no image $1 was found in the registry or docker buildx failed"
  logout "$@"
  exit 1
else
  logout "$@"
fi
