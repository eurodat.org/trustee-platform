#!/usr/bin/env bash

# Lint all files
# - that match *.sh
# - that contain a shebang
# - that are not gradlew
# - that can be found inlined at before-script, script and after-script blocks within *.gitlab-ci.yml files

# Set default severity for shellcheck to style
severity="style"

while getopts ":ewis" opt; do
  case "$opt" in
    e) severity="error";;
    w) severity="warning";;
    i) severity="info";;
    s) severity="style";;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
  esac
done

export rc=0

# *.sh
for file in $(git ls-files --exclude='*.sh' --ignored -c -z | xargs -0r)
do
  if ! shellcheck --severity="$severity" -x "${file}"; then
    rc=1
  fi
done

# contain shebang
for file in $(git ls-files ./*[gradlew] -c)
do
  if head -n1 "${file}" | grep -E -q '^#!(/usr)?(/bin/)(env )?(sh|bash|ksh)$' ; then
    if ! shellcheck --severity="$severity" -x "${file}"; then
      rc=1
    fi
  fi
done

# inlined with before_script, script, after_script in *.gitlab-ci.yml
for file in $(git ls-files --exclude='*.gitlab-ci.yml' --ignored -c)
do
  for selector in $(yq eval '.[] | select(tag=="!!map") | (.before_script,.script,.after_script) | select(. != null ) | path | ".[\"" + join("\"].[\"") + "\"]"' "${file}")
  do
    # shellcheck disable=SC2154 # false positive on usage of newline
    script=$(yq eval "${selector} | join(\"\\n${newline}\")" "${file}")

    if ! printf '%s' "${script}" | shellcheck --severity="$severity" -; then
      >&2 printf "\nError in %s in the script specified in %s:\n%s\n" "${file}" "${selector}" "${script}"
      rc=1
    fi
  done
done

exit ${rc}
