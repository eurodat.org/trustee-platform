#!/usr/bin/env bash

set -e

if [ "$1" = "no-md" ]; then
  echo "markdownlint-cli2 won't be used"
else
  if ! command -v markdownlint-cli2 &> /dev/null
  then
      echo "'markdownlint-cli2' is not installed. Please install it or run this script with 'no-md' as input."
      exit
  else
    npm install markdownlint-cli2-formatter-junit -g
  fi
fi

# Make sure everything is executed from the projects root
cd "$(git rev-parse --git-dir)/.."

rm -f ".git/hooks/pre-commit"

# Install Talisman
# Depending on the os the executables are different. That needs to be considered
if [ "$OSTYPE" = "linux-gnu" ]; then
  curl https://thoughtworks.github.io/talisman/install.sh > ~/install-talisman.sh
else
  curl https://thoughtworks.github.io/talisman/install.sh | sed 's/shasum -a 256 -c/sha256sum -c/' > ~/install-talisman.sh
fi
chmod +x ~/install-talisman.sh
~/install-talisman.sh pre-commit
rm ~/install-talisman.sh
# rename the file so it fits the os_type
mv .git/hooks/bin/talisman .git/hooks/bin/talisman-"$OSTYPE"

# Install ktlint
curl -SLO https://github.com/pinterest/ktlint/releases/download/1.2.1/ktlint
chmod a+x ktlint
mv ktlint ./.git/hooks/bin

# Create the pre-commit hook script
cat > ".git/hooks/pre-commit" << "END_OF_HOOK"
#!/usr/bin/env bash

set -e

# Get a list of files staged for deletion and ignore them in all subsequent commands
deleted_files=$(git diff --cached --name-only --diff-filter=D)

####################
# Frontend linting #
####################

cd frontend/eurodat-app

# ------ #
# ESLint #
# ------ #

eslint_files=$(git diff --cached --name-only | grep '\.ts\|\.scss' || true)

for deleted_file in $deleted_files; do
  eslint_files=$(echo "$eslint_files" | grep -v "^$deleted_file$" || true)
done

eslint_files=$(echo "$eslint_files" | cut -c 22- | tr '\n' ' ')

if [ -n "$eslint_files" ] && [ ! -z "$(echo $eslint_files | tr -d '[:space:]')" ]; then
  npx eslint $eslint_files --fix
fi

# -------- #
# Prettier #
# -------- #

prettier_files=$(git diff --cached --name-only | grep '\.ts\|\.scss\|\.html' || true)

for deleted_file in $deleted_files; do
  prettier_files=$(echo "$prettier_files" | grep -v "^$deleted_file$" || true)
done

prettier_files=$(echo "$prettier_files" | cut -c 22- | tr '\n' ' ')

if [ -n "$prettier_files" ] && [ ! -z "$(echo $prettier_files | tr -d '[:space:]')" ]; then
  npx prettier $prettier_files --write
fi

# -------- #
# HTMLHint #
# -------- #

html_files=$(git diff --cached --name-only | grep '\.html' || true)

for deleted_file in $deleted_files; do
  html_files=$(echo "$html_files" | grep -v "^$deleted_file$" || true)
done

html_files=$(echo "$html_files" | cut -c 22- | tr '\n' ' ')

if [ -n "$html_files" ] && [ ! -z "$(echo $html_files | tr -d '[:space:]')" ]; then
  npx htmlhint $html_files --ignore="**/coverage-component/**, **/cypress/**, **/dist/**"
fi

# --------- #
# Stylelint #
# --------- #

scss_files=$(git diff --cached --name-only | grep '\.scss' || true)

for deleted_file in $deleted_files; do
  scss_files=$(echo "$scss_files" | grep -v "^$deleted_file$" || true)
done

scss_files=$(echo "$scss_files" | cut -c 22- | tr '\n' ' ')

if [ -n "$scss_files" ] && [ ! -z "$(echo $scss_files | tr -d '[:space:]')" ]; then
  npx stylelint $scss_files --fix
fi

cd ../..

###################
# Backend linting #
###################

kt_files=$(git diff --cached --name-only | grep '\.kt$' || true)

for deleted_file in $deleted_files; do
  kt_files=$(echo "$kt_files" | grep -v "^$deleted_file$" || true)
done

if [ -n "$kt_files" ] && [ ! -z "$(echo $kt_files | tr -d '[:space:]')" ]; then
  echo "$kt_files" | xargs -I@ ./.git/hooks/bin/ktlint -F '@'
fi

END_OF_HOOK

# Add markdownlint if not ignored
if [ "$1" != "no-md" ]; then
  cat >> ".git/hooks/pre-commit" << "END_OF_HOOK"

#################
# Other linting #
#################

md_files=$(git diff --cached --name-only | grep '\.md' || true)

for deleted_file in $deleted_files; do
  md_files=$(echo "$md_files" | grep -v "^$deleted_file$" || true)
done

if [ -n "$md_files" ] && [ ! -z "$(echo $md_files | tr -d '[:space:]')" ]; then
  markdownlint-cli2 --config ./cluster/scripts/.markdownlint-cli2.jsonc $md_files
fi

END_OF_HOOK
fi

# Add Talisman as the last command
cat >> ".git/hooks/pre-commit" << END_OF_HOOK
# This HAS TO BE the last command in this file
.git/hooks/bin/talisman-$OSTYPE --githook pre-commit
END_OF_HOOK
