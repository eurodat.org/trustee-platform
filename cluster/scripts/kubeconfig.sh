#!/usr/bin/env bash

# Check parameters
if [ $# -lt 2 ]; then
    echo "Usage: $0 <cloud_provider> <cluster_name> [resource-group-or-project]"
    exit 1
fi

CLOUD_PROVIDER=$1
CLUSTER_NAME=$2

# If third parameter isn't provided, set defaults based on cloud provider
if [ -z "$3" ]; then
    if [ "$CLOUD_PROVIDER" = "gcp" ]; then
        RESOURCE_OR_PROJECT="safeaml-platform-dev-01"
    else
        echo "Unsupported cloud provider: $CLOUD_PROVIDER"
        exit 1
    fi
else
    RESOURCE_OR_PROJECT=$3
fi

if [ "$CLOUD_PROVIDER" = "gcp" ]; then
    # Update Kubeconfig for GCP using gcloud CLI
    if gcloud container clusters get-credentials "$CLUSTER_NAME" --zone=europe-west3-b --project="$RESOURCE_OR_PROJECT"; then
        echo "Kubeconfig for GCP cluster $CLUSTER_NAME has been updated."
    else
        echo "Failed to update Kubeconfig for GCP cluster $CLUSTER_NAME."
        exit 1
    fi
else
    echo "Unsupported cloud provider: $CLOUD_PROVIDER"
    exit 1
fi
