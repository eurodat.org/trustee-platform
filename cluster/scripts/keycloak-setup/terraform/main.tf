resource "keycloak_realm" "eurodat" {

    realm                          = "eurodat"
    ssl_required                   = "external"
    registration_email_as_username = true
    verify_email                   = true
    registration_allowed           = true
    password_policy                = "hashIterations(27500) and notUsername(undefined) and notEmail(undefined) and upperCase(1) and lowerCase(1) and digits(1) and length(12)"
    smtp_server {
        starttls = false
        ssl      = true
        auth {
            password = var.smtp_password
            username = var.smtp_username
        }
        from = var.smtp_from
        host = var.smtp_host
        port = var.smtp_port
    }
}

resource "keycloak_openid_client_scope" "safedeposit_write" {
    name     = "safedeposit:write"
    realm_id = keycloak_realm.eurodat.id
}

resource "keycloak_openid_client_scope" "transaction_write" {
    name     = "transaction:write"
    realm_id = keycloak_realm.eurodat.id
}

resource "keycloak_openid_client_scope" "app_write" {
    name     = "app:write"
    realm_id = keycloak_realm.eurodat.id
}

resource "keycloak_openid_client_scope" "client_write" {
    name     = "client:write"
    realm_id = keycloak_realm.eurodat.id
}

resource "keycloak_openid_client" "user_registrator" {

    access_type               = "CONFIDENTIAL"
    client_id                 = "user-registrator"
    realm_id                  = keycloak_realm.eurodat.id
    client_authenticator_type = "client-secret"
    service_accounts_enabled  = true
    client_secret             = var.user_registrator_secret
    extra_config = {
        "token.endpoint.auth.signing.alg" = "RS256",
    }
}

resource "keycloak_openid_client_default_scopes" "user_registrator_default_scopes" {
    client_id = keycloak_openid_client.user_registrator.id
    realm_id  = keycloak_realm.eurodat.id
    default_scopes = [keycloak_openid_client_scope.client_write.name]
}

resource "keycloak_openid_client_scope" "vault" {

    name                   = "vault"
    realm_id               = keycloak_realm.eurodat.id
    include_in_token_scope = true
}

resource "keycloak_openid_audience_protocol_mapper" "vault_audience_mapper" {

    client_scope_id          = keycloak_openid_client_scope.vault.id
    name                     = "aud"
    realm_id                 = keycloak_realm.eurodat.id
    add_to_access_token      = true
    included_custom_audience = "eurodat-vault"
}

resource "keycloak_openid_client" "vault" {

    access_type              = "CONFIDENTIAL"
    client_id                = "eurodat-vault"
    realm_id                 = keycloak_realm.eurodat.id
    service_accounts_enabled = true
    client_secret            = var.vault_secret
}

resource "keycloak_openid_client_default_scopes" "vault" {

    client_id = keycloak_openid_client.vault.id
    default_scopes = [keycloak_openid_client_scope.vault.name]
    realm_id  = keycloak_realm.eurodat.id
}

resource "keycloak_required_action" "frontend_totp" {

    name           = "Configure TOTP"
    alias          = "CONFIGURE_TOTP"
    enabled        = true
    default_action = true
    priority       = 10
    realm_id       = keycloak_realm.eurodat.id
}

resource "keycloak_user" "info_e2e" {

    realm_id       = keycloak_realm.eurodat.id
    first_name     = "E2E"
    last_name      = "Tester"
    username       = "info+e2e@eurodat.org"
    email          = "info+e2e@eurodat.org"
    email_verified = true
    initial_password {
        value = var.frontend_password
    }
}

resource "keycloak_user" "cypress_e2e" {

    realm_id       = keycloak_realm.eurodat.id
    first_name     = "Cypress"
    last_name      = "Tester"
    username       = "info+cypress@eurodat.org"
    email          = "info+cypress@eurodat.org"
    email_verified = true
    initial_password {
        value = var.frontend_password
    }
}

resource "keycloak_openid_client" "frontend_client" {

    access_type                  = "PUBLIC"
    client_id                    = "eurodat-app"
    realm_id                     = keycloak_realm.eurodat.id
    standard_flow_enabled        = true
    direct_access_grants_enabled = true
    valid_redirect_uris = var.frontend_redirect_uris
}

resource "keycloak_openid_client" "transaction-scheduler-client" {

    access_type              = "CONFIDENTIAL"
    client_id                = "transaction-scheduler-client"
    realm_id                 = keycloak_realm.eurodat.id
    service_accounts_enabled = true
    client_secret            = var.transaction_scheduler_client_secret
    extra_config = {
        "token.endpoint.auth.signing.alg" = "RS256",
    }
}

resource "keycloak_openid_client_default_scopes" "transaction-scheduler-client_default_scopes" {
    client_id = keycloak_openid_client.transaction-scheduler-client.id
    realm_id  = keycloak_realm.eurodat.id
    default_scopes = [keycloak_openid_client_scope.transaction_write.name]
}
