provider "keycloak" {
    client_id = "admin-cli"
    url                      = "https://${var.keycloak_host}/auth"
    username                 = var.keycloak_username
    password                 = var.keycloak_password
    tls_insecure_skip_verify = true
}

terraform {
    required_providers {
        keycloak = {
            source  = "keycloak/keycloak"
            version = "5.1.1"
        }
    }
}
