terraform {
    backend "local" {
        path = "/keycloak-config/terraform.tfstate"
    }
}
