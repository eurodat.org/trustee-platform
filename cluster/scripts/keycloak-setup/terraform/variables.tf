variable "keycloak_username" {
    type = string
}

variable "keycloak_password" {
    type      = string
    sensitive = true
}

variable "keycloak_host" {
    type = string
}

variable "eurodat_password" {
    type      = string
    sensitive = true
}

variable "user_registrator_secret" {
    type      = string
    sensitive = true
}

variable "vault_secret" {
    type      = string
    sensitive = true
}

variable "frontend_password" {
    type      = string
    sensitive = true
}

variable "frontend_redirect_uris" {
    type = list(string)
}

variable "smtp_host" {
    type = string
}

variable "smtp_port" {
    type = string
}

variable "smtp_from" {
    type = string
}

variable "smtp_username" {
    type = string
}

variable "smtp_password" {
    type      = string
    sensitive = true
}

variable "transaction_scheduler_client_secret" {
    type      = string
    sensitive = true
}
