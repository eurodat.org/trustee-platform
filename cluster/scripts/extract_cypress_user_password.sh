#!/usr/bin/env bash

help_text () {
  cat << EOF
This script extracts the user password and the paperless callback secret needed for Cypress e2e-tests and stores it in the cypress.env.json file.
  Options:
    -c, --context <context> The context/name of the EuroDaT platform cluster [REQUIRED]
    -h, --help Display this help text
EOF
}

while [[ "$#" -gt 0 ]]; do
  case "$1" in
    -c|--context)
        CONTEXT="$2"
        shift 2 ;;
    -h|--help)
        help_text
        exit 0;;
  esac
done

: "${CONTEXT:?Specify platform context with -c}"

# Extract the Cypress user password
CYPRESS_USER_PASSWORD=$(kubectl get secret --context "$CONTEXT" -n control-plane frontend-init-secret -o jsonpath='{.data.KC_FRONTEND_PASSWORD}' | base64 -d)
# Extract the paperless callback secret
PAPERLESS_CALLBACK_SECRET=$(kubectl get secret --context "$CONTEXT" -n control-plane paperless-callback-secret -o jsonpath='{.data.paperlessKey}' | base64 -d)

# Update the cypress.env.json file with the extracted password
jq --arg password "$CYPRESS_USER_PASSWORD" '.userPassword = $password' frontend/eurodat-app/cypress.env.json > frontend/eurodat-app/cypress.env.json.tmp && mv frontend/eurodat-app/cypress.env.json.tmp frontend/eurodat-app/cypress.env.json
# Update the cypress.env.json file with the extracted paperless callback secret
jq --arg secret "$PAPERLESS_CALLBACK_SECRET" '.paperlessToken = $secret' frontend/eurodat-app/cypress.env.json > frontend/eurodat-app/cypress.env.json.tmp && mv frontend/eurodat-app/cypress.env.json.tmp frontend/eurodat-app/cypress.env.json
