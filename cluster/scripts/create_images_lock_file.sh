#!/usr/bin/env sh

set -eu

DID_LOGIN=false

if grep "$IMG_REGISTRY_NAME" ~/.docker/config.json >/dev/null 2>&1; then
  echo "Using existing login to $IMG_REGISTRY_NAME"
else
  echo "$IMG_REGISTRY_SECRET" | docker login -u "$IMG_REGISTRY_USERNAME" --password-stdin "$IMG_REGISTRY_NAME"
  DID_LOGIN=true

  echo "Logged in to $IMG_REGISTRY_NAME"
fi

# If number of produced digests matches is larger than expected something went wrong
# Touch in case no image was built
touch image_references
EXPECTED_NUM_IMAGES_BUILD=$(wc -l < eurodat_images)
NUM_REFERENCES_CREATED=$(find "$CI_PROJECT_DIR"/image_references -maxdepth 1 -type f | wc -l)

if [ "$NUM_REFERENCES_CREATED" -gt "$EXPECTED_NUM_IMAGES_BUILD" ]; then
  echo "There were more images built ($NUM_REFERENCES_CREATED) than expected ($EXPECTED_NUM_IMAGES_BUILD)."
  echo "Expected:"
  cat eurodat_images
  printf "\n\n"
  echo "Present:"
  # shellcheck disable=SC2231
  for f in $CI_PROJECT_DIR/image_references/*; do
    cat "$f";
  done
  exit 1
fi

# Add external image references to images.lock
echo "Adding external images"
cp "$CI_PROJECT_DIR/external_images.lock" "$IMAGE_LOCK_FILE_PATH"

# Add image references of images built by this pipeline to images.lock
# Fail if images were built that are not listed in eurodat_images
echo "Adding images built by this pipeline"
for f in "$CI_PROJECT_DIR"/image_references/*; do
  # handle empty directory case
  [ -e "$f" ] || break
  IMAGE_REFERENCE=$(cat "$f")
  # Check if image reference is in eurodat_images
  image_found=0
  while IFS= read -r line; do
    # Check if a line is a substring of the image reference
    if echo "$IMAGE_REFERENCE" | grep -qF "$line"; then
      image_found=1
      break
    fi
  done < eurodat_images
  if [ "$image_found" -eq 0 ]; then
    echo "Image $IMAGE_REFERENCE was built but is not listed in eurodat_images"
    exit 1
  else
    echo "Adding $IMAGE_REFERENCE to $IMAGE_LOCK_FILE_PATH"
    echo "$IMAGE_REFERENCE" >> "$IMAGE_LOCK_FILE_PATH"
  fi
done

# Finally, search for images listed in eurodat_images that were not built but fetched in the check_images job
# and add them to images.lock. After check-images, it can be assumed that they are available as image:branch-latest
echo "Adding images that were fetched from other pipelines or main"

while IFS= read -r image; do
  if ! grep -qF "$IMG_REGISTRY_NAME/$image" "$IMAGE_LOCK_FILE_PATH"; then
    DIGEST=$(docker buildx imagetools inspect "$IMG_REGISTRY_NAME/$image:$IMG_TAG" | grep -oE '\w+:\w+$')
    IMAGE_REFERENCE_WITH_DIGEST="$IMG_REGISTRY_NAME/$image:$IMG_TAG@$DIGEST"
    echo "Adding $IMAGE_REFERENCE_WITH_DIGEST to $IMAGE_LOCK_FILE_PATH"
    echo "$IMAGE_REFERENCE_WITH_DIGEST" >> "$IMAGE_LOCK_FILE_PATH"
  fi
done < eurodat_images

if $DID_LOGIN; then
  echo "Logging out of $IMG_REGISTRY_NAME"
  docker logout "$IMG_REGISTRY_NAME"
fi
