#!/bin/sh
# Apply a default service account configuration for a given namespace without token automounting
# args.:
# $1 - namespace

cat << SERVICE_ACCOUNT | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: default
  namespace: "$1"
automountServiceAccountToken: false
SERVICE_ACCOUNT
