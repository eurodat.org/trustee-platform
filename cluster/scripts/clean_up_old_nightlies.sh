#!/usr/bin/env sh
set -eu

# Removes nightly build artifacts from earlier than today

echo "Keeping nightly artifacts of nightly$DATE"
# make curl adding line breaks to end of response body
# shellcheck disable=SC2028
echo '-w "\\n"' >> ~/.curlrc

echo "Cleaning up image registry"
# don't use skopeo delete since that deletes the manifest associated with that tag and all related tags,
# potentially deleting a release
# assumes gitlab offset_pagination_limit>=5000 and less than 5000 image repositories exist
REPOSITORIES=$(curl -s --header "PRIVATE-TOKEN: $NIGHTLY_CLEANUP_ACCESS_TOKEN" \
  "$CI_API_V4_URL/projects/$CI_PROJECT_ID/registry/repositories?per_page=5000" \
  --fail-with-body \
  | jq -c '.[] | {id: .id, name: .name}')

for repo in $REPOSITORIES; do
  # continue if curl fails, but print the error body
  # this request fails if it is performed twice within an hour for the same repository,
  # we can ignore that and clean up next time
  echo "Deleting nightly tags of image repository $repo, keeping nightly$DATE"
  id=$(echo "$repo" | jq '.id' )
  curl -s -X DELETE --header "PRIVATE-TOKEN: $NIGHTLY_CLEANUP_ACCESS_TOKEN" \
    "$CI_API_V4_URL/projects/$CI_PROJECT_ID/registry/repositories/$id/tags" \
    --data 'name_regex_delete=.*nightly.*' \
    --data "name_regex_keep=.*nightly$DATE.*" \
    --fail-with-body || true
done

# delete all nightly helm chart versions except the current
echo "Cleaning up helm charts"
# assumes gitlab offset_pagination_limit>=5000 and less than 5000 packages exist
# produces list of {"id":19539513,"version":"v0.0.18-nightly20231019","name":"eurodat"}
PACKAGES=$(curl -s --header "PRIVATE-TOKEN: $NIGHTLY_CLEANUP_ACCESS_TOKEN" \
  "$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages?package_type=helm&sort=desc&per_page=5000" \
  --fail-with-body \
  | jq -c '.[] | select( .version | contains("nightly")) | {id: .id, version: .version, name: .name}')

for package in $PACKAGES; do
  # skip if it is an artifacts of today's nightly
  if echo "$package" | jq '.version' | grep -q "$DATE"; then
    continue
  fi
  # continue if curl fails, but print the error body
  # this request fails if it is performed twice within an hour for the same repository,
  # we can ignore that and clean up next time
  id=$(echo "$package" | jq '.id' )
  echo "Deleting package $package"
  curl -s -X DELETE --header "PRIVATE-TOKEN: $NIGHTLY_CLEANUP_ACCESS_TOKEN" \
    "$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/$id" \
    --fail-with-body || true
done

# Clean up old nightly releases
echo "Cleaning up releases"

RELEASES=$(curl -s --header "PRIVATE-TOKEN: $NIGHTLY_CLEANUP_ACCESS_TOKEN" \
  "$CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/tags?sort=desc&per_page=5000&search=nightly" \
  --fail-with-body | jq -c '.[] | {name: .name, tag_name: .release.tag_name}')

for release in $RELEASES; do
  # skip if it is a release of today's nightly
  if echo "$release" | jq '.name' | grep -q "$DATE"; then
    echo "s"
    continue
  fi

  # continue if curl fails, but print the error body
  tag_name=$(echo "$release" | jq '.tag_name' )

  echo "Deleting release $release"
  curl -s -X DELETE --header "PRIVATE-TOKEN: $NIGHTLY_CLEANUP_ACCESS_TOKEN" \
    "$CI_API_V4_URL/projects/$CI_PROJECT_ID/releases/$tag_name" \
    --fail-with-body || true

  echo "Deleting tag $tag_name"
    curl -s -X DELETE --header "PRIVATE-TOKEN: $NIGHTLY_CLEANUP_ACCESS_TOKEN" \
      "$CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/tags/$tag_name" \
      --fail-with-body || true
done
