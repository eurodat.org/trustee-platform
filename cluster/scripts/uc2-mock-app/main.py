from psycopg2.extras import RealDictCursor, execute_values
import os
import psycopg2
import logging

logger = logging.getLogger(__name__)
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.INFO)


def main():
    connection = {}
    cursor = {}
    try:
        logger.info(
            "Using user password for authentication to login to the safe deposit db."
        )
        app_id = "uc2appmock"
        client_id = "data_consumer"
        db_name_safe_deposit = "safedeposit_" + app_id + "_" + client_id

        logging.info("Connection to safedeposit db")
        connection["psql"] = psycopg2.connect(
            host=db_host, dbname=db_name_safe_deposit, user=user_name, password=pwd
        )
        connection["psql"].autocommit = True
        cursor["psql"] = connection["psql"].cursor(cursor_factory=RealDictCursor)

        logger.info("reading data form table in the safe deposit db")
        query = """SELECT * from safedeposit.input;"""
        cursor["psql"].execute(query)
        queried_data_safe_deposit_box = cursor["psql"].fetchall()
    except Exception as e:
        logger.error(f"Could not read data from table in safe deposit db: {e}")
        raise
    finally:
        if cursor.get("psql"):
            cursor["psql"].close()
        if connection.get("psql"):
            connection["psql"].close()
    try:
        logging.info("Connection to transaction db")
        connection["psql"] = psycopg2.connect(
            host=db_host, dbname=db_name, user=user_name, password=pwd
        )
        connection["psql"].autocommit = True
        cursor["psql"] = connection["psql"].cursor(cursor_factory=RealDictCursor)

        logger.info("reading data form input table in the transaction db")
        query_read = """SELECT * from input.input;"""
        cursor["psql"].execute(query_read)
        queried_data_transaction_db = cursor["psql"].fetchall()

        logger.info("inserting data to output table of the transaction db")
        for row in queried_data_safe_deposit_box:
            values = tuple(
                [
                    row["id"],
                    row["created_at"],
                    row["created_by"],
                    row["file_content"],
                    row["file_type"],
                    row["dataset_id"],
                    "dbselector",
                ]
            )

            query = """INSERT INTO output.output (
                id,
                created_at,
                recipient_id,
                file_content,
                report_type,
                dataset_id,
                participant_identifier) VALUES (%s, %s, %s, %s, %s, %s, %s);"""

            cursor["psql"].execute(query, values)
        for row in queried_data_transaction_db:
            values = tuple(
                [
                    row["id"],
                    row["created_at"],
                    row["created_by"],
                    row["file_content"],
                    row["file_type"],
                    row["dataset_id"],
                    "dbselector",
                ]
            )

            query = """INSERT INTO output.output (
                id,
                created_at,
                recipient_id,
                file_content,
                report_type,
                dataset_id,
                participant_identifier) VALUES (%s, %s, %s, %s, %s, %s, %s);"""

            cursor["psql"].execute(query, values)
    except Exception as e:
        logger.error(f"Problems with transaction db interaction: {e}")
        raise
    finally:
        if cursor.get("psql"):
            cursor["psql"].close()
        if connection.get("psql"):
            connection["psql"].close()


db_host = os.environ["PG_HOSTNAME"]
db_name = os.environ["PG_DATABASENAME"]
user_name = os.environ["PG_USERNAME"]
pwd = os.environ["PG_PASSWORD"]

if __name__ == "__main__":
    main()
