from psycopg2.extras import RealDictCursor, execute_values
import os
import psycopg2
import logging

logger = logging.getLogger(__name__)
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.INFO)


def main():

    connection = {}
    cursor = {}

    try:
        logger.info("Using user password for authentication.")
        connection["psql"] = psycopg2.connect(
            host=db_host, dbname=db_name, user=user_name, password=pwd
        )
        connection["psql"].autocommit = True
        cursor["psql"] = connection["psql"].cursor(cursor_factory=RealDictCursor)

        logger.info(
            "reading from intermediate (inserted by hand) and writing into output table as user internal"
        )

        query_first = """INSERT INTO output.output_first
            (transaction_id,name,security_column)
            VALUES
            ('001','Bank1','cobaselector'),
            ('002','Bank1','cobaselector'),
            ('003','Bank2','dbselector');"""

        cursor["psql"].execute(query_first)

        query_second = """INSERT INTO output.output_second
            (transaction_id,name,security_column)
            VALUES
            ('001','Bank1','cobaselector'),
            ('002','Bank1','cobaselector'),
            ('003','Bank2','dbselector');"""

        cursor["psql"].execute(query_second)

    except psycopg2.Error as exc:
        logger.error(f"Database error: {exc}")
        raise
    except Exception as exc:
        logger.error(f"Unexpected error: {exc}")
        raise
    finally:
        if cursor.get("psql"):
            cursor["psql"].close()
        if connection.get("psql"):
            connection["psql"].close()


db_host = os.environ["PG_HOSTNAME"]
db_name = os.environ["PG_DATABASENAME"]
user_name = os.environ["PG_USERNAME"]
pwd = os.environ["PG_PASSWORD"]

if __name__ == "__main__":
    main()
