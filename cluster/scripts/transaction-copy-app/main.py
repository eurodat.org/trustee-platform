import json
import os
import psycopg2
import subprocess
import sys
from psycopg2.extras import RealDictCursor
import logging

logger = logging.getLogger(__name__)
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.INFO)

# Database connection parameters
db_host = os.environ["PG_HOSTNAME"]
db_name = os.environ["PG_DATABASENAME"]
user_name = os.environ["PG_USERNAME"]
pwd = os.environ["PG_PASSWORD"]

# Messaging connection parameters
messaging_url = os.environ["MESSAGING_URL"]
messaging_token = os.environ["MESSAGING_TOKEN"]
cert_path = os.environ["CA_BUNDLE"]


def get_correlation_id():
    if not messaging_url or not messaging_token:
        logger.info("Message URL or TOKEN missing!")
        sys.exit(1)

    payload = {
        "message": "Test Message from Transaction Copy App",
        "selectors": ["dbselector"],
    }

    curl_command = [
        "curl",
        "-X",
        "POST",
        f"{messaging_url}/messages",
        "-H",
        f"Authorization: Bearer {messaging_token}",
        "-H",
        "Content-Type: application/json",
        "--cacert",
        cert_path,
        "-d",
        json.dumps(payload),
    ]

    result = subprocess.run(curl_command, capture_output=True, text=True)

    if result.returncode == 0:
        response_json = json.loads(result.stdout)
        if isinstance(response_json, list) and len(response_json) > 0:
            correlation_id = response_json[0].get("correlationId")
            if correlation_id:
                return correlation_id
            else:
                logger.error("No correlation id!")
                sys.exit(1)
        else:
            logger.error("Problems caused by response json!")
            sys.exit(1)
    else:
        logger.error("Got non-zero return code from subprocess curl command!")
        sys.exit(1)


def main():
    connection = {}
    cursor = {}
    try:
        # Connect to the database
        connection = psycopg2.connect(
            host=db_host, dbname=db_name, user=user_name, password=pwd
        )
        connection.autocommit = True
        cursor = connection.cursor(cursor_factory=RealDictCursor)

        # Read data from input.input_transactions
        cursor.execute("SELECT * FROM input.input_transactions")
        input_transactions = cursor.fetchall()

        correlation_id = get_correlation_id()

        # Insert data into output.output_transactions
        for transaction in input_transactions:
            cursor.execute(
                """
                INSERT INTO output.output_transactions (
                    transaction_id, bank_transaction_id, transaction_timestamp, created_at,
                    originating_bank_bic, counterparty_bank_bic, originator_iban, counterparty_iban,
                    amount, currency, transaction_type, security_column, correlation_id
                ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
            """,
                (
                    transaction["transaction_id"],
                    transaction["bank_transaction_id"],
                    transaction["transaction_timestamp"],
                    transaction["created_at"],
                    transaction["originating_bank_bic"],
                    transaction["counterparty_bank_bic"],
                    transaction["originator_iban"],
                    transaction["counterparty_iban"],
                    transaction["amount"],
                    transaction["currency"],
                    transaction["transaction_type"],
                    "dummy_security_column",
                    correlation_id,
                ),
            )
    except Exception as e:
        logger.error(f"{e}")
        raise
    finally:
        # Close the cursor and connection
        cursor.close()
        connection.close()


if __name__ == "__main__":
    main()
