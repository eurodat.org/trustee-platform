#!/usr/bin/env bash
set -e
# Tests a client in a EuroDaT cluster by retrieving a JWT token
# --context is the context to use in the kubeconfig file
# --private-key-path is the path to the private key .pem certificate for the client
# --client-id is the ID of the client to register

VALID_ARGS=$(getopt -o kcps --long kubeconfig:,context:,private-key-path:,client-id: -- "$@")
if [[ $? -ne 0 ]]; then
    exit 1;
fi

eval set -- "$VALID_ARGS"
while true
do
  case "$1" in
    --context)
        CONTEXT="$2"
        shift 2 ;;
    --private-key-path)
        PRIVATE_KEY_PATH="$2"
        shift 2 ;;
    --client-id)
        CLIENT_ID="$2"
        shift 2 ;;
    --) shift;
        break
        ;;
  esac
done

SCRIPT_DIR=$(dirname "$0")
CA_BUNDLE_PATH=$SCRIPT_DIR/cacerts.crt
kubectl --context "$CONTEXT" get cm -n control-plane local-ca-bundle -ojsonpath='{.data.ca-bundle\.crt}' > "$CA_BUNDLE_PATH"
FQDN=$(kubectl get secret --context "$CONTEXT" -n control-plane user-management-secret -o jsonpath='{.data.FQDN}' | base64 -d)
echo "Testing token retrieval of client $CLIENT_ID in $FQDN cluster"

# Test the registration of the client
TOKEN=$(bash "$SCRIPT_DIR/create_jwt.sh" --key_path "$PRIVATE_KEY_PATH" --client_id "$CLIENT_ID" --url "https://$FQDN" --cacert "$CA_BUNDLE_PATH")

# Make the API request
RESPONSE_CODE_GET=$(curl -s -o /dev/null -w "%{http_code}" -X 'GET' \
  "https://$FQDN/api/v1/clients/clientselector/$CLIENT_ID" \
  -H 'accept: text/plain' \
  -H "Authorization: Bearer $TOKEN" \
  --fail-with-body \
  --cacert $CA_BUNDLE_PATH)

if [[ "RESPONSE_CODE_GET" -ne 200 ]];
then
    echo "Unexpected error when smoketesting client. Error code $RESPONSE_CODE_GET."
    exit 1
fi
