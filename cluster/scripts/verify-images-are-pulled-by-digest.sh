#!/usr/bin/env bash
# Verifies that all images used by pods and jobs are pulled by digests.
# The arguments to this function are the k8s contexts to be checked.

set -euo pipefail

# List of image patterns that are allowed to be pulled without digests
white_list=(
  "^gke\.gcr\.io/cluster-proportional-autoscaler:.+"
  "^.+[a-z0-9-]+/kube-proxy-amd64:.+"
  "^gke\.gcr\.io/ip-masq-agent:.+"
  "^europe-west3-docker\.pkg\.dev/[a-z0-9-]+/[a-z0-9-]+/uc2-mock-app:.+"
  "^europe-west3-docker\.pkg\.dev/[a-z0-9-]+/[a-z0-9-]+/uc3-mock-app:.+"
  "^europe-west3-docker\.pkg\.dev/[a-z0-9-]+/[a-z0-9-]+/row-level-security-mock-app:.+"
  "^europe-west3-docker\.pkg\.dev/[a-z0-9-]+/[a-z0-9-]+/mock-safeaml-app:.+"
  "^europe-west3-docker\.pkg\.dev/[a-z0-9-]+/[a-z0-9-]+/minabo-app:.+"
  "^europe-west3-docker\.pkg\.dev/[a-z0-9-]+/[a-z0-9-]+/hello-world:.+"
  "^europe-west3-docker\.pkg\.dev/[a-z0-9-]+/[a-z0-9-]+/message-app:.+"
  "^europe-west3-docker\.pkg\.dev/[a-z0-9-]+/[a-z0-9-]+/transaction-copy-app:.+"
  )

# Checks if the image is white-listed
check_exception() {
  for pattern in "${white_list[@]}"; do
    if [[ "$1" =~ ${pattern} ]]; then
      return 0
    fi
  done
  return 1
}

images_without_digest=()
# Checks if the provided image contains a digest or is white-listed. If not, then the image is appended to the global
# array images_without_digest
check_image_digest() {
  local image=$1
  if ! echo "${image}" | grep -E "sha256:[a-fA-F0-9]{64}" > /dev/null; then
    if check_exception "$image"; then
      echo "Exception for: ${image}"
    else
      echo "Missing digest for: ${image}"
      images_without_digest+=("${image}")
    fi
  fi
}

for ctx in "$@" ; do
  echo "Check images in ${ctx}..."

  # Check images from pods
  # shellcheck disable=SC2207 # splitting is desired here
  pod_images=($(kubectl get pods --context "$ctx" --all-namespaces -o json | jq -r '.items[].spec.containers[].image')) || exit 1
  for image in "${pod_images[@]}"; do
    check_image_digest "$image"
  done

  # Check images from jobs
  # shellcheck disable=SC2207 # splitting is desired here
  job_images=($(kubectl get jobs --context "$ctx" --all-namespaces -o json | jq -r '.items[].spec.template.spec.containers[].image'))
  for image in "${job_images[@]}"; do
    check_image_digest "$image"
  done
done

if [ "${#images_without_digest[@]}" -eq 0 ]; then
  echo "All images use digests or are white-listed"
  exit 0
else
  echo "The following images are pulled without digests:"
  for image in "${images_without_digest[@]}"; do
    echo "${image}"
  done
  exit 1
fi
