package io.argoproj.workflow.apis;

public interface TokenService {
    String getToken();
}