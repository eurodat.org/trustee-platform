#!/usr/bin/env bash

# Script apparently depends on a missing -o pipefail
set -eu

keyCloakHost="https://$CLUSTER_FQDN"
vaultNamespace=transaction-plane
curlTimeout=3
trace=1

echo "-----------------------------------------------------------------"

# POD state in "Succeeded" or "Running"
hasError=0
namespaces=("$(kubectl get namespaces --no-headers -o custom-columns=":metadata.name")")
if [ ${#namespaces[@]} != 0 ]; then
    echo "-----------------------------------------------------------------"
    echo "Checking PODs in Namespaces for State not in Running or Succeeded"

    hasUnhealthyPod=0
    for i in "${namespaces[@]}"
    do
        echo "$i"
        result=$(kubectl get pods --namespace "$i" --no-headers -o custom-columns="POD:metadata.name,PHASE:status.phase" | grep -v Succeeded | grep -v Running | wc -m)
        if [[ $((result)) != 0 ]]; then
            echo "ERROR : Namespace: $i - There are not running POD(s)"
            hasUnhealthyPod=1
        fi
    done
    if [[ $hasUnhealthyPod -gt 0 ]]; then
        hasError=1
        echo "ERROR : There was at least one POD not in Running or Succeeded state!"
      else
        echo "Running : All PODs in Running or Succeeded state."
    fi
    echo "-----------------------------------------------------------------"
fi

# Verify : Keycloak is accessible
if curl -s -k --connect-timeout $curlTimeout -o /dev/null -w '%{http_code}' "$keyCloakHost/auth/admin/master/console/" | grep -q 200 ;
    then
      echo "Running : Keycloak accessible."
    else
      echo "ERROR : Keycloak not accessible."
      hasError=1
      if [[ trace -gt 0 ]]; then
          curl -s -k --connect-timeout $curlTimeout "$keyCloakHost/auth/health/ready"
      fi
fi

echo "----------------------------------------"
echo "List of PODs within NS=$vaultNamespace"
kubectl get pods --no-headers --namespace="$vaultNamespace" -o custom-columns="POD:metadata.name"
echo "----------------------------------------"

# Verify : Vault Health check
vaultPOD=$(kubectl get pods --namespace="$vaultNamespace" -o custom-columns="POD:metadata.name" | grep "transaction-vault.*0")
echo "Vault POD: " "$vaultPOD"
vaultStatus="kubectl exec --namespace=$vaultNamespace $vaultPOD -- vault read /sys/health"
if $vaultStatus | grep -q "initialized                          true" && $vaultStatus | grep -q "performance_standby                  false" && $vaultStatus | grep -q "sealed                               false" && $vaultStatus | grep -q "standby                              false" ;
    then {
      echo "Running : Vault accessible."
    }
    else {
      echo "ERROR : Vault is not accessible."
      hasError=1
    }
fi
echo "-----------------------------------------------------------------"
if [ $hasError -gt 0 ]; then
    echo "There have been errors, exiting..."
    exit 1
fi
