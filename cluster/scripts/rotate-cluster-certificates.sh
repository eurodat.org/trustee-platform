#!/usr/bin/env bash

help_text () {
  cat << EOF
This script rotates all certificates including the root-ca in the
supplied cluster/context. Note that the context has to be included
in the current kubeconfig.
EOF
}

while [[ "$#" -gt 0 ]]; do
  case "$1" in
    -c|--context)
        CONTEXT="$2"
        shift 2 ;;
    -h|--help)
        help_text
        exit 0;;
    *)
        echo "Unknown parameter passed: $1"
        help_text
        exit 1
        ;;
  esac
done

if [[ -z "$CONTEXT" ]]; then
    echo "No context was passed"
    exit 1
fi

NAMESPACES=$(kubectl --context "$CONTEXT" get ns --no-headers -o custom-columns=":metadata.name")

# Renew root certificate in namespace cert-manger before the others
echo "Renewing certificates in namespace cert-manager"
cmctl renew  --context "$CONTEXT" --all -n cert-manager

for ns in $NAMESPACES; do
  if [ "$ns" = "cert-manager" ]; then
    continue
  fi
  echo "Renewing certificates in namespace $ns"
  cmctl renew --context "$CONTEXT" --all -n "$ns"
done

# The pods are configured (via the reloader annotation) to restart
# when the secrets or configmaps were updated.

echo "Done rotating certificates. Now wait for pods to become healthy again..."
NUM_PODS=1
while [ "$NUM_PODS" -gt "0" ]; do
  sleep 10s
  # Get all pods that are
  # - neither in Running, nor in Succeeded phase
  # - in Running phase but have containers with ready=false
  POD_LIST="$(kubectl --context "$CONTEXT" get pods --all-namespaces -o json \
    | jq -r '.items[] | select(.status.phase != "Succeeded") | select(.status.phase != "Running" or (.status.containerStatuses[] | select(.ready==false) > 0)) | .metadata.namespace + "/" + .metadata.name')"
  NUM_PODS=$(echo -n "$POD_LIST" | wc -w)
  echo "Waiting for $NUM_PODS pods to become healthy:"
  echo "$POD_LIST"
  echo -e "\n"
done
