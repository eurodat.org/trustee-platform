#!/usr/bin/env bash

show_help() {
cat << EOF

Usage: ${0##*/} <file containing markdown table>

Checks before GitLab pipeline executions that all mandatory environment variables are set and have non-zero length
values.

Status of variable is classified as follows:

 - OK                               : required variable has been exported to environment variables with non-empty value assigned.
 - MISSING                          : required variable has not been exported to environment variables
 - EMPTY                            : required variable has been exported to environment variables but has an empty string assigned
 - NOT REQUIRED                     : variable is optional and its value is not evaluated further (it could be empty or missing, though)

EOF
}

# format the variable output on shell for better highlighting
print_style () {
    case "$2" in
      "NOT REQUIRED")               COLOR="96m";;
      "OK")                         COLOR="92m";;
      "EMPTY")                      COLOR="93m";;
      "MISSING")                    COLOR="91m";;
      *)                            COLOR="0m";;
    esac

    START_COLOR="\e[$COLOR";
    END_COLOR="\e[0m";

    if [[ "$2" != "IGNORE" ]]; then printf "$START_COLOR%-30s %-8s %-7s\n$END_COLOR" "$1" "$2"; fi
}

# Shows status of optional variables in output
print_optional_variables=0

# Limit the cases
treat_empty_as_missing=1

if [ $# -eq 0 ]; then
  show_help
  exit 0
fi

input_markdown_file="$1"

# Check that argument are not empty
if [[ -z "$input_markdown_file" ]];
then
  echo "File name has zero length. Please provide a valid value!" >&2 ;
  exit 1
fi

# Extract list of GitLab variables from given markdown file to check for availability
string_gitlab_table=$(sed -n '/\"gitlab_required_ci_cd_variables\"/,/\[\/\/\]: # \x28<\/div>/p' "$input_markdown_file" | sed '0,/|---.*$/d' | awk -F '|' '{print $2 $3}' | sed "s/\`//g; /^$/d")
[ -z "$string_gitlab_table" ] && echo "Wrong file specified, DIV container not available or no variables in DIV container found!" >&2 && exit 1

string_gitlab_variable_name=$(echo "$string_gitlab_table" | awk '{print $1}')
string_gitlab_variable_required=$(echo "$string_gitlab_table" | awk '{print $2}')

# Get names of current available environment variables
string_environment_variables_name=$(env | cut -f 1 -d '=')

# Get values of current available environment variable
string_environment_variables_value=$(env | cut -f 2 -d '=')

# Convert to arrays
mapfile -t array_gitlab_variables_name <<< "$string_gitlab_variable_name"
mapfile -t array_gitlab_variables_required <<< "$string_gitlab_variable_required"
mapfile -t array_environment_variables_name <<< "$string_environment_variables_name"
mapfile -t array_environment_variables_value <<< "$string_environment_variables_value"

# Tracker for error handling if values are not set or empty
return_code=0

# log output
echo ""
print_style "------------------------------" "--------------------------";
print_style "VARIABLE" "STATUS";
print_style "------------------------------" "--------------------------";

# Loop through all required GitLab variables
for i in "${!array_gitlab_variables_name[@]}"
do
  result=

  # Only take care of required variables
  if [[ "${array_gitlab_variables_required[$i]}" == "Y" ]]
  then
    # Check every current assigned environment variable for a match within the GitLab variables
    for j in "${!array_environment_variables_name[@]}"
    do
      # environment variable found
      if [[ "${array_gitlab_variables_name[$i]}" == "${array_environment_variables_name[$j]}" ]]
      then
        # make sure it has value, i.e. length of the assigned variable value is greater than zero
        if [[ $(echo "${array_environment_variables_value[$j]}" | awk '{print length}') -gt 0 ]]
        then
          result=OK
        else
          if [[ $treat_empty_as_missing == 1 ]]
          then
            result=MISSING
          else
            result=EMPTY
          fi
          return_code=1
        fi
        # variables are unique, i.e. we can stop at the first occurrence
        break
      fi
    done

    # We have to take care if we got no match at all
    if [[ $result != OK && $result != EMPTY ]]
    then
      result=MISSING
      return_code=1
    fi
  else
    if [[ $print_optional_variables == 1 ]]
    then
      result="NOT REQUIRED"
    else
      result="IGNORE"
    fi
  fi

  print_style "${array_gitlab_variables_name[$i]}" "$result"
done

print_style "------------------------------" "--------------------------";
print_style ""

exit $return_code
