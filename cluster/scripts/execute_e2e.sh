#!/usr/bin/env bash

set -euo pipefail

TESTS_DEFAULT="org.eurodat.e2e.*"
REPETITIONS_DEFAULT=1
SKIP_REGISTER_TEST_USERS_DEFAULT=false
AUTO_EXPORT_IMAGE_TAG_DEFAULT=false

help_text () {
  cat << EOF
This script registers the test users and executes the e2e tests.
This requires that the kubeconfig contains the configuration for the platform cluster.
  Options:
    -c, --context <context> The context/name of the EuroDaT platform cluster [REQUIRED]
    -t, --test  <tests> The tests to execute. Default $TESTS_DEFAULT [OPTIONAL]
    -r, --repetitions <repetitions> Repeat the test. Negative values for infinite repetitions. Default false [OPTIONAL]
    -s, --skip-register-test-users Skips the registration of the test users [OPTIONAL]
    -a, --auto-export-image-tag Exports <your current git branch name>-latest as IMG_TAG. This variable is required during the e2e execution. [OPTIONAL]
    -i, --image-registry Exports the passed image registry as IMAGE_UPLOADER_REGISTRY.
    -u, --image-registry-username Exports the passed registry username as IMAGE_UPLOAD_REGISTRY_USERNAME.
    -p, --image-registry-password Exports the passed registry password as IMAGE_UPLOAD_REGISTRY_PASSWORD.
    -h, --help Display this help text
EOF
}

ring_bell () {
    for ((i=1; i<=10; i++))
        do
          tput bel
          sleep 1
        done
}

TESTS=$TESTS_DEFAULT
REPETITIONS=$REPETITIONS_DEFAULT
SKIP_REGISTER_TEST_USERS=$SKIP_REGISTER_TEST_USERS_DEFAULT
AUTO_EXPORT_IMAGE_TAG=$AUTO_EXPORT_IMAGE_TAG_DEFAULT
IMAGE_UPLOADER_REGISTRY=""
IMAGE_UPLOAD_REGISTRY_USERNAME=""
IMAGE_UPLOAD_REGISTRY_PASSWORD=""

while [[ "$#" -gt 0 ]]; do
  case "$1" in
    -c|--context)
        CONTEXT="$2"
        shift 2 ;;
    -r|--repetitions)
        REPETITIONS="$2"
        shift 2 ;;
    -t|--tests)
        TESTS="$2"
        shift 2 ;;
    -s|--skip-register-test-users)
        SKIP_REGISTER_TEST_USERS=true
        shift 1 ;;
    -a|--auto-export-image-tag)
        AUTO_EXPORT_IMAGE_TAG=true
        shift 1 ;;
    -i|--image-registry)
        export IMAGE_UPLOADER_REGISTRY="$2"
        shift 2 ;;
    -u|--image-registry-username)
        export IMAGE_UPLOAD_REGISTRY_USERNAME="$2"
        shift 2 ;;
    -p|--image-registry-password)
        export IMAGE_UPLOAD_REGISTRY_PASSWORD="$2"
        shift 2 ;;
    -h|--help)
        help_text
        exit 0;;
    *)
        echo "Unknown parameter passed: $1"
        help_text
        exit 1
        ;;
  esac
done

if [[ -z "$IMAGE_UPLOADER_REGISTRY" ]]; then
    echo "Image registry is empty."
fi
if [[ -z "$IMAGE_UPLOAD_REGISTRY_USERNAME" ]]; then
    echo "Image registry username is empty."
fi
if [[ -z "$IMAGE_UPLOAD_REGISTRY_PASSWORD" ]]; then
    echo "Image registry password is empty."
fi

: "${CONTEXT:?Specify platform context with -c}"

cd "$(dirname "$0")"

PROJECT_DIR=${CI_PROJECT_DIR:-$(git rev-parse --show-toplevel)}
TARGET="$PROJECT_DIR/services/e2e/src/test/resources"

if [ "$SKIP_REGISTER_TEST_USERS" == false ]; then
  echo "Registering test users"
  bash register_test_users.sh --context "$CONTEXT" --cert-target-dir "$TARGET"
  echo "Finished registering test users"
else
  echo "Skipping registering test users"
fi


APP_SERVICE_OIDC_CLIENT_CREDENTIALS_SECRET=$(kubectl get secret --context "$CONTEXT" -n control-plane user-management-secret -o jsonpath='{.data.KC_USER_REGISTRATOR_SECRET}' | base64 -d)
CONTRACT_SERVICE_OIDC_CLIENT_CREDENTIALS_SECRET=$(kubectl get secret --context "$CONTEXT" -n control-plane frontend-init-secret -o jsonpath='{.data.KC_FRONTEND_PASSWORD}' | base64 -d)
REALM=$(kubectl get secret --context "$CONTEXT" -n control-plane user-management-secret -o jsonpath='{.data.REALM}' | base64 -d)
FQDN=$(kubectl get secret --context "$CONTEXT" -n control-plane user-management-secret -o jsonpath='{.data.FQDN}' | base64 -d)
QUARKUS_OIDC_CLIENT_AUTH_SERVER_URL=https://$FQDN/auth/realms/$REALM
CONTRACT_SERVICE_URL=https://$FQDN/api/v1/contract-service
EURODAT_API_URL=https://$FQDN/api/v1
APP_SERVICE_URL=https://$FQDN/api/v1/app-service
PAPERLESS_PASSWORD=$(kubectl get secret --context "$CONTEXT" -n control-plane paperless-callback-secret -o jsonpath='{.data.paperlessKey}' | base64 -d)

if [ ! -d "$TARGET" ]; then
  echo "Error: Parent directory does not exist!"
  exit 1
fi

kubectl --context "$CONTEXT" get cm -n control-plane local-ca-bundle -ojsonpath='{.binaryData.cacerts}' | base64 -d > "$TARGET/cacerts"

export APP_SERVICE_URL
export CONTRACT_SERVICE_URL
export APP_SERVICE_OIDC_CLIENT_CREDENTIALS_SECRET
export CONTRACT_SERVICE_OIDC_CLIENT_CREDENTIALS_SECRET
export QUARKUS_OIDC_CLIENT_AUTH_SERVER_URL
export EURODAT_API_URL
export PAPERLESS_PASSWORD

if [ "$AUTO_EXPORT_IMAGE_TAG" == true ]; then
  IMG_TAG="$(git rev-parse --abbrev-ref HEAD)-latest"
  echo "Exporting IMG_TAG $IMG_TAG"
  export IMG_TAG
fi

cd ../../services

COMMAND="gradle :e2e:test --parallel --tests $TESTS"

# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

counter=0
while [ $counter -ne "$REPETITIONS" ]
do
    if ! $COMMAND; then
        if [ "$REPETITIONS" -ne 1 ]; then
            echo -e "${RED}### Test failed (repetition $((counter+1))/$REPETITIONS). ###${NC}"
            ring_bell
        fi
        exit 1
    fi
    (( counter++ )) || true
    if [ "$REPETITIONS" -ne 1 ]; then
        echo -e "${GREEN}### Test succeeded (repetition $counter/$REPETITIONS). ###${NC}"
    fi
done
