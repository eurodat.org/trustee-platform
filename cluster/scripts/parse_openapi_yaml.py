import yaml
import os
import glob
import shutil
import sys
from collections import defaultdict


def parse_yaml_file(filename):
    with open(filename, "r") as file:
        return yaml.safe_load(file)


def find_paths(yaml_content):
    exceptions = ["/api/v1/contract-service/submissions"]
    unsecured_paths = defaultdict(list)
    if "paths" in yaml_content:
        for path, path_info in yaml_content["paths"].items():
            if path not in exceptions:
                for method in path_info:
                    # Check if 'security' exists in the method data
                    if "security" not in path_info[method]:
                        print(
                            f"The endpoint {method} {path} does not have 'security' key."
                        )
                        unsecured_paths[path].append(method)
    else:
        raise KeyError("No 'paths' found in the OpenAPI spec.")
    return unsecured_paths


if __name__ == "__main__":
    input_dir = sys.argv[1]

    # Loop over all files in the directory
    unsecured_paths = {}
    for yamlfile in glob.glob(os.path.join(input_dir, "*.yaml")):
        print(f"Checking OpenAPI spec: {yamlfile}.")
        data = parse_yaml_file(yamlfile)
        unsecured_paths.update(find_paths(data))

    if unsecured_paths:
        sys.exit(1)
