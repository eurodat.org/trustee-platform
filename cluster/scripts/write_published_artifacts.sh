#!/usr/bin/env sh

# Extends the external_images.lock file with the (self built and) released images and digests of a given version in the
# container registry in the trustee-platform GitLab repository and saves it into published_images.lock
# Argument: VERSION which is the name of the release-tag, e.g. v0.0.24

# Check parameters
if [ $# -lt 1 ]; then
    echo "Usage: <VERSION, eg 'v0.0.24'>"
    exit 1
fi

VERSION=$1

cp "external_images.lock" "published_images.lock"
while IFS= read -r image; do
  DIGEST=$(skopeo inspect --format "{{ .Digest}}" "docker://registry.gitlab.com/eurodat/trustee-platform/$image:$VERSION")
  echo "Write the published image $image and digest $DIGEST in published_images.lock artifact"
  echo "registry.gitlab.com/eurodat/trustee-platform/$image:$VERSION@$DIGEST" >> published_images.lock
done < eurodat_images
