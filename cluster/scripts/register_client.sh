#!/usr/bin/env bash
set -e
# Registers a new client in a EuroDaT cluster
# --context is the context to use in the kubeconfig file
# --tls-cert-path is the path to the TLS certificate for the client
# --client-id is the ID of the client to register
# --client-selector is the row-base security identifier for the client
# --purpose is the scope (Note: repeat flag if there is more than one scope)

VALID_ARGS=$(getopt -o kctcs --long kubeconfig:,context:,tls-cert-path:,client-id:,purpose:,client-selector: -- "$@")
if [[ $? -ne 0 ]]; then
    exit 1;
fi

PURPOSES=()

eval set -- "$VALID_ARGS"
while true
do
  case "$1" in
    --context)
        CONTEXT="$2"
        shift 2 ;;
    --tls-cert-path)
        TLS_CERT_PATH="$2"
        shift 2 ;;
    --client-id)
        CLIENT_ID="$2"
        shift 2 ;;
    --client-selector)
        SELECTOR="$2"
        shift 2 ;;
    --purpose)
        PURPOSES+=("$2")
        shift 2 ;;
    --) shift;
        break
        ;;
  esac
done

SCRIPT_DIR=$(dirname "$0")
CA_BUNDLE_PATH=$SCRIPT_DIR/cacerts.crt
kubectl --context "$CONTEXT" get cm -n control-plane local-ca-bundle -ojsonpath='{.data.ca-bundle\.crt}' > "$CA_BUNDLE_PATH"

FQDN=$(kubectl get secret --context "$CONTEXT" -n control-plane user-management-secret -o jsonpath='{.data.FQDN}' | base64 -d)
echo "Registering client $CLIENT_ID with selector $SELECTOR in $FQDN cluster"
USER_REGISTRATOR_SECRET=$(kubectl get secret --context "$CONTEXT" -n control-plane user-management-secret -o jsonpath='{.data.KC_USER_REGISTRATOR_SECRET}' | base64 -d)
TOKEN=$(curl -X POST "https://$FQDN/auth/realms/eurodat/protocol/openid-connect/token" \
    --header "Content-Type: application/x-www-form-urlencoded" \
    --data-urlencode "client_id=user-registrator" \
    --data-urlencode "grant_type=client_credentials" \
    --data-urlencode "client_secret=$USER_REGISTRATOR_SECRET" \
    --cacert $CA_BUNDLE_PATH -s | jq -r '.access_token')

CERT="$(cat "$TLS_CERT_PATH" | sed '1d; s/^-\+[BEGIN|END].*//; /^$/,$d' | tr -d '\n')"

if [[ ${#CA_BUNDLE_PATH} -eq 0 ]];
then
    echo "control-plane namespace or local-ca-bundle config map not found in the k8s cluster!"
    exit 1
fi
RESPONSE_CODE_DELETE=$(curl "https://$FQDN/api/v1/clients/$CLIENT_ID" \
   -o /dev/null \
   -X "DELETE" -s -w "%{http_code}" \
   -H "Content-Type: application/json" \
   -H "Authorization: Bearer $TOKEN" \
   --cacert $CA_BUNDLE_PATH )

if [[ "$RESPONSE_CODE_DELETE" -ne 200 ]];
then
    echo "client $CLIENT_ID was not deleted - $RESPONSE_CODE_DELETE"
    exit 1
fi

# Transform purpose array to list of purposes for POST method
PURPOSES_LIST=""
for item in "${PURPOSES[@]}"; do
  PURPOSES_LIST+="\"$item\","
done
PURPOSES_LIST=${PURPOSES_LIST%,}

RESPONSE_CODE_POST=$(curl "https://$FQDN/api/v1/clients" \
    -X "POST" -s -w "%{http_code}" -o /dev/null \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer $TOKEN" \
    --fail-with-body \
    --cacert $CA_BUNDLE_PATH \
    -d '{"name": "'"$CLIENT_ID"'", "certificate": "'"$CERT"'", "clientSelector": "'"$SELECTOR"'", "purpose": ['$PURPOSES_LIST']}')

if [[ "$RESPONSE_CODE_POST" -ne 200 ]];
then
    echo "Unexpected error when registering client. Error code $RESPONSE_CODE_POST."
    exit 1
fi
