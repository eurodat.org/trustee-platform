#!/usr/bin/env sh
# Verifies that all images in the file provided by the IMAGE_LOCK_PATH use a digest.
# $1 is the path to the image lock file.

set -eu

IMAGE_LOCK_PATH=$1
VALID=1

echo "Verifying that all images defined in ${IMAGE_LOCK_PATH} use digests ..."

if [ ! -f "$IMAGE_LOCK_PATH" ]; then
  echo "${IMAGE_LOCK_PATH} is not a regular file or it does not exist!"
  exit 1
fi

while IFS= read -r line; do
  # A digest is a SHA256 hash that has exactly 64 hexadecimal digits and is appended to the image with @sha256.
  if ! ( echo "$line" | grep -E '^.*@sha256:[0-9a-fA-F]{64}$' >/dev/null ); then
    echo "The image $line uses no valid digest!"
    VALID=0
  fi
done < "$IMAGE_LOCK_PATH"

if [ "$VALID" -eq 1 ]; then
    echo "All images use digests."
else
    exit 1
fi
