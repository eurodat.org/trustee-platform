#!/usr/bin/env bash

# This script resets the in cluster postgres DB by deleting the persistent volume claim.
# After the DB has been reset all pods that use the DB are restarted as they have to recreate their schemas.
# Finally, the keycloak-config-job is recreated to setup the realms in Keycloak.
# This is does not work for the local Tilt setup and there the keycloak-config-job has to be restarted manually.
set -euo pipefail

CONTEXT="$1"

kubectl --context "$CONTEXT" -n transaction-plane delete pvc data-eurodat-postgresql-0 &
pid="$!"
kubectl --context "$CONTEXT" -n transaction-plane delete pod eurodat-postgresql-0
echo "Waiting for completion of PVC deletion."
wait "$pid"

echo "Waiting for postgres to become ready."
sleep 5
kubectl --context "$CONTEXT" -n transaction-plane wait --for=condition=ready pod eurodat-postgresql-0 --timeout=300s

kubectl --context "$CONTEXT" -n base delete pod eurodat-keycloak-0
echo "Waiting for Keycloak to become ready."
sleep 5
kubectl --context "$CONTEXT" -n base wait --for=condition=ready pod eurodat-keycloak-0 --timeout=300s
kubectl --context "$CONTEXT" -n control-plane delete pod -l app.kubernetes.io/name=transaction-service
kubectl --context "$CONTEXT" -n transaction-plane delete pod -l app.kubernetes.io/part-of=argo-workflows
kubectl --context "$CONTEXT" -n control-plane delete pod -l app.kubernetes.io/name=app-service
kubectl --context "$CONTEXT" -n control-plane delete pod -l app.kubernetes.io/name=contract-service

if [ "$CONTEXT" != "kind-eurodat" ]; then
  JOB_NAME=$(kubectl --context "$CONTEXT" get job -n control-plane -o=json | jq -r '.items[] | select(.metadata.name | test("keycloak-config-job-")).metadata.name')
  kubectl --context "$CONTEXT" -n control-plane get job "$JOB_NAME" -o json \
    | jq 'del(.spec.selector)' \
    | jq 'del(.spec.template.metadata.labels)' \
    | jq '.spec.template.metadata += {"labels": {"app": "keycloak-config-job"}}' \
    | kubectl --context "$CONTEXT" replace --force -f -

  echo "Waiting for keycloak-config-job to complete."
  kubectl --context "$CONTEXT" -n control-plane wait job --for=condition=complete "$JOB_NAME" --timeout=300s
else
  echo "Please restart the keycloak-config-job in Tilt."
fi
