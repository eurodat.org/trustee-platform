#!/usr/bin/env bash

PROGRAM_NAME=${0##*/}

help_text () {
cat << EOF
Usage: $PROGRAM_NAME [inputs/options]
$PROGRAM_NAME will execute two stern commands that collect logs from the clusters/namespaces specified via the inputs.

  Inputs/Options:
    -h, --help                   Print this help message.
    -i, --int-config             REQUIRED - The path to the config file for the main cluster.
    -c, --context                REQUIRED - The context for the internal cluster (Local: kind-eurodat, Remote: eurodat-dev-<name>)
    -p, --exclude-postgres       OPTIONAL - If set to 'true', then the logs from postgres pods are excluded.
    --tail                       OPTIONAL - The number last lines from logs to show. Default is set to -1, showing all logs.
    --since                      OPTIONAL - Return logs newer than a relative duration. If not set, it defaults to '48h0m0s'.
EOF
}

[ $# -eq 0 ] && echo "No Arguments provided." && exit 1

TEMP=$(getopt -o h:e:i:c:p --long int-config:,ext-config:,context:\
exclude-postgres:,tail:,since:,help -- "$@") || help_text
eval set -- "$TEMP"

TAIL="-1"
SINCE="48h0m0s"

while true; do
  case "$1" in
    -i|--int-config) INT_CONFIG="$2"; shift 2 ;;
    -c|--context) CONTEXT="$2"; shift 2 ;;
    -p|--exclude-postgres) SQL_EXCLUDE=".*postgres.*"; shift 1 ;;
    --tail) TAIL="$2"; shift 2 ;;
    --since) SINCE="$2"; shift 2 ;;
    -h|--help) help_text; exit 0 ;;
    --) shift ; break ;;
    *) echo "invalid flag" >&2 exit 1 ;;
  esac
done

(trap 'kill 0' SIGINT;
# controller
stern --context "$CONTEXT" -n "control-plane" --kubeconfig "$INT_CONFIG" --exclude-pod "$SQL_EXCLUDE" \
  --tail "$TAIL" --since="$SINCE" --color=auto . &
# data-plane & FVs
stern --context "$CONTEXT" -n "data-plane" --kubeconfig "$INT_CONFIG" --exclude-pod "$SQL_EXCLUDE" \
  --tail "$TAIL" --since="$SINCE" --color=auto .
)
