#!/usr/bin/env bash
# Prepares the environment for a eurodat client.

export EURODAT_API_URL="$EURODAT_BASE_URL/api/v1"
export QUARKUS_HTTP_SSL_CERTIFICATE_KEY_STORE_FILE="$KEY_STORE_FILE_PATH"
export QUARKUS_HTTP_SSL_CERTIFICATE_KEY_STORE_PASSWORD="$KEY_STORE_FILE_PASSWORD"
export QUARKUS_OIDC_CLIENT_AUTH_SERVER_URL="$EURODAT_BASE_URL/auth/realms/eurodat"
export QUARKUS_OIDC_CLIENT_CLIENT_ID="$CLIENT_ID"
export QUARKUS_OIDC_CLIENT_CREDENTIALS_JWT_AUDIENCE="$EURODAT_BASE_URL/auth/realms/eurodat"
export QUARKUS_OIDC_CLIENT_CREDENTIALS_JWT_KEY_ID="${JWT_KEY_ID:-certificate}"
export QUARKUS_OIDC_CLIENT_CREDENTIALS_JWT_KEY_PASSWORD="$KEY_STORE_FILE_PASSWORD"
export QUARKUS_OIDC_CLIENT_CREDENTIALS_JWT_KEY_STORE_PASSWORD="$KEY_STORE_FILE_PASSWORD"
export QUARKUS_OIDC_CLIENT_CREDENTIALS_JWT_KEY_STORE_FILE="$KEY_STORE_FILE_PATH"
export QUARKUS_OIDC_CREDENTIALS_JWT_SIGNATURE_ALGORITHM="RS256"
export QUARKUS_OIDC_CLIENT_GRANT_TYPE="client"
export QUARKUS_OIDC_CLIENT_SCOPES="openid"
