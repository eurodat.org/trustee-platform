#!/usr/bin/env bash
# Reconfigure a kubeconfig file to use the rights of another service account

while getopts "n:s:" opt; do
  case "$opt" in
    n) export KUBECONFIG_NAMESPACE="$OPTARG" ;;
    s) export SERVICE_ACCOUNT_NAME="$OPTARG" ;;
    *) echo "Unknown Option $OPTARG"; exit 1 ;;
  esac
done

if [ -z "$KUBECONFIG_NAMESPACE" ]; then echo "No namespace set!"; exit 1; fi
if [ -z "$SERVICE_ACCOUNT_NAME" ]; then echo "No service account name set!"; exit 1; fi

kubectl config set-context --current --namespace="$KUBECONFIG_NAMESPACE"
SECRET_NAME="$SERVICE_ACCOUNT_NAME-secret"
export SECRET_NAME
NAMESPACE=$(kubectl get secret "$SECRET_NAME" -o jsonpath='{.data.namespace}' | base64 --decode)
export NAMESPACE
TOKEN=$(kubectl get secret "$SECRET_NAME" -o jsonpath='{.data.token}' | base64 --decode)
export TOKEN
CA_CRT=$(kubectl get secret "$SECRET_NAME" -o jsonpath='{.data.ca\.crt}')
export CA_CRT

CLUSTER_NAME=$(kubectl config view -o jsonpath='{.clusters[0].name}')
export CLUSTER_NAME
CLUSTER_SERVER=$(kubectl config view -o jsonpath='{.clusters[0].cluster.server}')
export CLUSTER_SERVER

envsubst < "$CI_PROJECT_DIR/cluster/scripts/kubectl_config_template.yaml" > ~/.kube/config
