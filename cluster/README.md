The cluster folder holds all infrastructure related code. `gitlab-ci.yml` files
manage the steps taken by GitLab. `.tf` files are used by terraform to create
and manage cloud resources.

The helm folder has several charts that are used in different stages. Charts
are used to deploy either [services](../services) hosted in this repository or
external services which manage logging, monitoring, networking, etc.

The kubernetes folder has additional `.tf` files for managing cloud resources.

The `scripts` folder hosts scripts that are mainly used by the pipeline to
interact with external services, such as authorizing new users. The scripts
can also be started manually, e.g. to update the local argo-client.

To test pipeline files, one can use GitLab's linter, start a pipeline, or start
the docker image used by a job and run commands locally. The pipeline run can
also be modified with the use of annotations, such as the built-in `[skip-ci]`
or other custom annotations which can be found in `gitlab-ci.yml` files. The
Terraform state can be downloaded from GitLab and ran locally after installing
Terraform. Helm charts can also be started locally.
