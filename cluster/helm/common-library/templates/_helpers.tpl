{{/* This chartlibrary is used for the global provision of functions to reduce redundant code. */}}
{{/* ----------------------------------------------------------------------------------------- */}}
{{/* Generate image-names based on registry, repository, tag and digest */}}
{{- define "common-library.image-id-builder" -}}
{{- $result := "" }}
{{- if .registry }}
{{- $result = .registry }}
{{- if .repository }}
{{- $result = printf "%s/%s" $result .repository }}
{{- if .tag }}
{{- $result = printf "%s:%s" $result .tag }}
{{- end }}
{{- if .digest }}
{{- $result = printf "%s@%s" $result .digest }}
{{- end }}
{{- end }}
{{- else if .repository }}
{{- $result = .repository }}
{{- if .tag }}
{{- $result = printf "%s:%s" $result .tag }}
{{- end }}
{{- if .digest }}
{{- $result = printf "%s@%s" $result .digest }}
{{- end }}
{{- end }}
{{- $result }}
{{- end }}
