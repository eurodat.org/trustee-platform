{{/* Expand the name of the chart. */}}
{{- define "transaction-service.name" -}}
{{ default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "transaction-service.namespace" -}}
{{ default .Release.Namespace .Values.namespaceOverride }}
{{- end }}

{{- define "transaction-service.dataplane.namespace" -}}
{{ default (include "transaction-service.namespace" .) .Values.dataplane.namespace }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "transaction-service.fullname" }}
{{- if .Values.fullnameOverride -}}
{{ .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else -}}
{{ $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name -}}
{{ .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else -}}
{{ printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/* Create chart name and version as used by the chart label. */}}
{{- define "transaction-service.chart" -}}
{{ printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Common labels */}}
{{- define "transaction-service.labels" -}}
helm.sh/chart: {{ include "transaction-service.chart" . }}
{{ include "transaction-service.selector.labels" . }}
{{ if .Chart.AppVersion -}}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/* Selector labels */}}
{{- define "transaction-service.selector.labels" -}}
app.kubernetes.io/name: {{ include "transaction-service.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "ingress.class.name" -}}
{{ default (print .Chart.Name "-nginx") .Values.ingress.className }}
{{- end }}

{{/* -------------------------------------- Resource names --------------------------------- */}}

{{- define  "transaction-service.configmap.name" -}}
{{ .Chart.Name }}-configmap
{{- end }}

{{- define  "transaction-service.init.secret.name" -}}
{{ .Chart.Name }}-init-secret
{{- end }}

{{- define "transaction-service.service.name" -}}
{{ include "transaction-service.fullname" . }}-svc
{{- end }}

{{- define "transaction-service.service.account.name" -}}
{{ default (print .Chart.Name "-service-account") .Values.serviceAccount.name }}
{{- end }}

{{/* --------------------- Certificate-keystore & keystore-password resources -------------- */}}

{{- define "transaction-service.keystore.password.secret.key" -}}
{{ .Values.certificate.secret.key }}
{{- end }}

{{- define "transaction-service.keystore.password.secret.name" -}}
{{ default (printf "%s-keystore-password-secret" .Chart.Name) .Values.certificate.secret.name }}
{{- end }}

{{/* If no password was specified, the chart will generate it itself based on the current unix time */}}
{{- define "transaction-service.password.secret.password" -}}
{{ default (now | unixEpoch | trunc 8) .Values.certificate.secret.password }}
{{- end }}

{{- define "transaction-service.iam.keystore.file" -}}
{{ .Values.certificate.keystore.iam.file }}
{{- end }}

{{- define "transaction-service.tls.keystore.file" -}}
{{ .Values.certificate.keystore.tls.file }}
{{- end }}

{{/* -------------------------------------- Certificates ----------------------------------- */}}

{{- define "transaction-service.certificate.name" -}}
{{ .Chart.Name }}-certificate
{{- end }}

{{- define "transaction-service.certificate.secret.name" -}}
{{ include "transaction-service.certificate.name" . }}-secret
{{- end }}

{{/* -------------------------------------- IAM service ------------------------------------ */}}

{{- define "eurodat.iam.service.internal.url" -}}
eurodat-keycloak-headless.base.svc:8443
{{- end }}

{{- define "eurodat.iam.realm" -}}
{{ .Values.iam.realm }}
{{- end }}

{{- define "eurodat.iam.fqdn" -}}
{{ .Values.iam.fqdn }}
{{- end }}

{{- define "eurodat.iam.endpoint.internal" -}}
{{ printf "%s/realms/%s" (include "eurodat.iam.base.url.internal" .) (include "eurodat.iam.realm" .) }}
{{- end }}

{{- define "eurodat.iam.endpoint.external" -}}
{{ printf "%s/realms/%s" (include "eurodat.iam.base.url.external" .) (include "eurodat.iam.realm" .) }}
{{- end }}

{{- define "eurodat.iam.base.url.internal" -}}
{{ printf "https://%s/auth" (include "eurodat.iam.service.internal.url" .) }}
{{- end }}

{{- define "eurodat.iam.base.url.external" -}}
{{ printf "https://%s/auth" .Values.fqdn }}
{{- end }}

{{- define "eurodat.iam.clientId" -}}
{{ .Values.iam.credentials.client.id }}
{{- end }}

{{/* -------------------------------------- Argo Workflows --------------------------------------- */}}

{{- define "eurodat.argo.url" -}}
{{ printf "https://eurodat-argo-workflows-server.%s.svc:%d" .Values.argo.namespace (.Values.ports.argoServer | int) }}
{{- end -}}

{{/* -------------------------------------- Services --------------------------------------- */}}

{{- define "eurodat.app-service.namespace" -}}
{{ include "transaction-service.namespace" . }}
{{- end -}}

{{- define "eurodat.app-service.name" -}}
eurodat-app-service-svc
{{- end -}}

{{- define "eurodat.app-service.path" -}}
api/v1/app-service
{{- end -}}

{{- define "eurodat.app-service.port" -}}
8443
{{- end -}}

{{- define "eurodat.credential-service.namespace" -}}
{{ include "transaction-service.namespace" . }}
{{- end -}}

{{- define "eurodat.credential-service.name" -}}
eurodat-credential-service-svc
{{- end -}}

{{- define "eurodat.credential-service.path" -}}
api/v1/credential-service
{{- end -}}

{{- define "eurodat.credential-service.port" -}}
8443
{{- end -}}

{{- define "eurodat.database-service.namespace" -}}
{{ include "transaction-service.namespace" . }}
{{- end -}}

{{- define "eurodat.database-service.name" -}}
eurodat-database-service-svc
{{- end -}}

{{- define "eurodat.database-service.path" -}}
api/v1/database-service
{{- end -}}

{{- define "eurodat.database-service.port" -}}
8443
{{- end -}}

{{- define "eurodat.transaction-service.namespace" -}}
{{ default .Release.Namespace .Values.namespaceOverride }}
{{- end -}}

{{- define "eurodat.transaction-service.name" -}}
eurodat-transaction-service-svc
{{- end -}}

{{- define "eurodat.transaction-service.path" -}}
api/v1
{{- end -}}

{{- define "eurodat.transaction-service.port" -}}
8443
{{- end -}}

