#!/bin/sh

KEY_OUTPUT_DIR=${KEY_OUTPUT_DIR:=$(pwd)}

if vault status 2>/dev/null | grep -q "Initialized.*false"; then
  echo "Initializing vault"
  vault operator init -format=yaml > "$KEY_OUTPUT_DIR/unseal-keys"
fi

if vault status 2>/dev/null | grep -q "Initialized.*false"; then
  echo "Vault initialization failed, exiting"
  exit 1
fi

if [ -s "$KEY_OUTPUT_DIR/unseal-keys" ]; then
        echo "Found unseal keys"
else
        echo "Did not find unseal keys. Maybe you need to run vault operator init again. Exiting"
        exit 1
fi

echo "Vault is initialized"

if vault status 2>/dev/null | grep -q "Sealed.*false"; then
  echo "Vault is already unsealed, exiting"
  exit 0
fi

threshold=$(vault status 2>/dev/null | grep "Threshold" | grep -Eo "[0-9]+")
shares=$(vault status 2>/dev/null | grep "Total Shares" | grep -Eo "[0-9]+")

echo "Need $threshold/$shares unseal keys"

i=1
while [ $i -le "$threshold" ]; do
  echo "Trying unseal key $i/$threshold"
  vault operator unseal -tls-skip-verify "$(grep -A 5 unseal_keys_b64 "$KEY_OUTPUT_DIR/unseal-keys" | head -$((i + 1))| tail -1| sed 's/- //g')" >/dev/null
  i=$(( i + 1 ))
done

if vault status 2>/dev/null | grep -q "Sealed.*false";
then
  echo "Vault is unsealed"
else
  echo "Unsealing vault failed, exiting"
  exit 1
fi
