#!/bin/sh
set -e # prevent the postSync hook from failing silently

printf "Starting setup_vault script"
KEY_OUTPUT_DIR=${KEY_OUTPUT_DIR:=$(pwd)}

vault login "$(grep "root_token" "$KEY_OUTPUT_DIR/unseal-keys" | grep -Eo "\S*$")"

if vault read auth/jwt/config 2>/dev/null; then
  printf "Auth already enabled and configured"

  printf "Reloading auth jwt plugin to refresh certificates"
  vault plugin reload -plugin jwt
  vault write auth/jwt/config \
      oidc_discovery_url="https://eurodat-keycloak-headless.base.svc:8443/auth/realms/$REALM" \
      oidc_client_id="" \
      bound_issuer="https://eurodat-keycloak-headless.base.svc:8443/auth/realms/$REALM" \
      oidc_discovery_ca_pem=@/etc/ssl/certs/ca-certificates.crt
else
  if vault auth list 2>/dev/null | grep -q "jwt/"; then
    # if jwt/ is already in the authentication list, it can not enabled twice
    printf "Auth already enabled"
    vault plugin reload -plugin jwt
  else
    vault auth enable jwt
  fi

  vault write auth/jwt/config \
    oidc_discovery_url="https://eurodat-keycloak-headless.base.svc:8443/auth/realms/$REALM" \
    oidc_client_id="" \
    bound_issuer="https://eurodat-keycloak-headless.base.svc:8443/auth/realms/$REALM" \
    oidc_discovery_ca_pem=@/etc/ssl/certs/ca-certificates.crt

  cd

  tee manager.hcl <<EOF
  path "/database/*" {
     capabilities = ["create", "read", "update", "delete", "list"]
  }
EOF

  vault policy write manager manager.hcl

  vault write auth/jwt/role/reader \
    bound_audiences="$KC_VAULT_CLIENT" \
    allowed_redirect_uris="https://$SERVICENAME.$NAMESPACE:8200/ui/vault/auth/oidc/oidc/callback, http://localhost:8250/oidc/callback" \
    policies="manager" \
    role_type=jwt \
    user_claim="sub" \
    token_ttl="1h" \
    token_max_ttl="1h"
fi

if vault secrets list 2>/dev/null | grep -q "database/"; then
  # if database/ is already in the secrets list, it can not be enabled twice
  printf "Database secrets engine already enabled"
else
  vault secrets enable database
  printf "Successfully initiated database"
fi

if vault audit list -detailed | grep -q "file"; then
  # if file/ is already in the audit devices list, it can not be mounted twice
  printf "Audit device already enabled"
else
  vault audit enable file file_path=stdout
  printf "Successfully initiated audit device"
fi

vault write sys/auth/token/tune default_lease_ttl=1h max_lease_ttl=1h

printf "Exiting setup_vault script"
