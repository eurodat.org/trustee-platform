# Based on helm chart example (ingress-nginx _helpers.tlp file)

{{/*
Expand the name of the chart.
*/}}
{{- define "eurodat.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "eurodat.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "eurodat.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}


{{/*
Common labels
*/}}
{{- define "eurodat.labels" -}}
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
{{ include "eurodat.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "eurodat.selectorLabels" -}}
app.kubernetes.io/name: {{ .Chart.Name }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}


{{/*
Grafana
*/}}

{{- define "eurodat.grafana.service.name"}}
{{- printf "%s.%s.svc" .Values.grafana.fullnameOverride .Values.namespaces.baseNamespace }}
{{- end}}

{{- define "eurodat.grafana.service.url"}}
{{- printf "https://%s:%d" (include "eurodat.grafana.service.name" .) (.Values.grafana.service.ports.grafana | int) }}
{{- end}}

{{/*
Grafana Tempo
*/}}

{{- define "eurodat.grafanaTempo.service.name"}}
{{- printf "%s.%s.svc" .Values.tempo.fullnameOverride .Values.namespaces.baseNamespace }}
{{- end}}

{{- define "eurodat.grafanaTempo.service.url"}}
{{- printf "https://%s:%d" (include "eurodat.grafanaTempo.service.name" .) (.Values.tempo.tempo.server.http_listen_port | int) }}
{{- end}}

{{/*
Prometheus
*/}}

{{- define "eurodat.prometheus.service.name"}}
{{- printf "%s-prometheus-server.%s.svc" .Chart.Name .Values.namespaces.prometheusNamespace }}
{{- end}}

{{/*
Loki
*/}}

{{- define "eurodat.loki.service.name"}}
{{- printf "%s.%s.svc" .Values.loki.fullnameOverride .Values.namespaces.baseNamespace }}
{{- end}}

{{/*
Promtail
*/}}

{{- define "eurodat.promtail.service.name"}}
{{- printf "%s.%s.svc" .Values.promtail.fullnameOverride .Values.namespaces.baseNamespace }}
{{- end}}
