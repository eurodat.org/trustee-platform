apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "app-service.fullname" . }}
  namespace: {{ include "app-service.namespace" . }}
  labels:
    {{- include "app-service.labels" . | nindent 4 }}
    app.kubernetes.io/component: {{ .Chart.Name }}
  annotations:
    reloader.stakater.com/auto: "true"
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      {{- include "app-service.selector.labels" . | nindent 6 }}
      app.kubernetes.io/component: {{ .Chart.Name }}
  template:
    metadata:
      labels:
        {{- include "app-service.labels" . | nindent 8 }}
        app.kubernetes.io/component: {{ .Chart.Name }}
    spec:
      {{- if .Values.serviceAccount.enabled }}
      serviceAccountName: {{ include "app-service.service.account.name" . }}
      {{- end }}
      containers:
        - name: app-service
          image: {{ include "common-library.image-id-builder" .Values.images.appService }}
          resources:
            requests:
              cpu: 350m
          imagePullPolicy: Always
          {{- if not .Values.global.debug }}
          readinessProbe:
            httpGet:
              path: /health
              port: {{ .Values.ports.apiTarget }}
              scheme: HTTPS
          livenessProbe:
            httpGet:
              path: /health
              port: {{ .Values.ports.apiTarget }}
              scheme: HTTPS
            failureThreshold: 5
          startupProbe:
            httpGet:
              path: /health
              port: {{ .Values.ports.apiTarget }}
              scheme: HTTPS
            failureThreshold: 5
            initialDelaySeconds: 20
            periodSeconds: 2
          {{- end }}
          ports:
            - containerPort: 5005
            - containerPort: {{ .Values.ports.apiTarget }}
          env:
            - name: EURODAT_TRANSACTION_SERVICE_API_URL
              value: {{ printf "https://%s.%s.svc:%s/%s"
                  (include "eurodat.transaction-service.name" .) (include "eurodat.transaction-service.namespace" .) (include "eurodat.transaction-service.port" .) (include "eurodat.transaction-service.path" .) }}
            - name: EURODAT_DATABASE_API_URL
              value: {{ printf "https://%s.%s.svc:%s/%s"
                (include "eurodat.database-service.name" .) (include "eurodat.database-service.namespace" .) (include "eurodat.database-service.port" .) (include "eurodat.database-service.path" .) }}
            - name: QUARKUS_HTTP_CORS
              value: "true"
            - name: QUARKUS_HTTP_SSL_CERTIFICATE_KEY_STORE_FILE
              {{- if .Values.certificate.keystore.separate.enabled }}
              value: {{ .Values.appService.mount.https }}/{{ include "app-service.tls.keystore.file" . }}
              {{- else }}
              value: {{ .Values.appService.mount.https }}/{{ include "app-service.iam.keystore.file" . }}
              {{- end }}
            - name: QUARKUS_HTTP_SSL_CERTIFICATE_KEY_STORE_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: {{ include "app-service.keystore.password.secret.key" . }}
                  name: {{ include "app-service.keystore.password.secret.name" . }}
            - name: EXTERNAL_OIDC
              value: {{ include "eurodat.iam.endpoint.external" . }}
            - name: INTERNAL_OIDC
              value: {{ include "eurodat.iam.endpoint.internal" . }}
            - name: QUARKUS_DATASOURCE_URL
              value: {{ printf "vertx-reactive:postgresql://%s:%d/%s"
              (.Values.database.url) (.Values.database.port | int) (.Values.database.name)}}
            - name: QUARKUS_DATASOURCE_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: APP_SERVICE_PASSWORD
                  name: {{ .Values.database.credentialsSecret }}
            - name: JAVA_TOOL_OPTIONS
              value: -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=localhost:5005
            - name: OTLP_SECRET
              valueFrom:
                secretKeyRef:
                  key: opentelemetryToken
                  name: opentelemetry-token-secret
            - name: OTLP_ENDPOINT
              value: {{ .Values.otlp.endpoint }}
            - name: EURODAT_ARGO_API_KEY_PATH
              value: /certs/tokens/service-account-token
            - name: EURODAT_ARGO_SERVER_BASE_URL
              value: {{ include "eurodat.argo.url" . }}
            - name: PROVISION_NAMESPACE
              value: {{ .Values.argo.provisionNamespace }}
            - name: REGISTRY_ADDRESS
              value: {{ .Values.registry }}
            - name: UPLOADER_IMAGE
              value: {{ include "common-library.image-id-builder" .Values.images.appServiceUploader }}
            - name: UPLOADER_NS
              value: {{ .Values.appServiceUploader.namespace }}
            - name: UPLOADER_SA
              value: {{ .Values.appServiceUploader.serviceAccount }}
            - name: UPLOADER_DEST_AUTH
              value: {{ .Values.appServiceUploader.destAuth | quote }}
            - name: UPLOADER_SRC_HTTPS
              value: {{ .Values.appServiceUploader.srcHttps | quote }}
            - name: UPLOADER_DEST_HTTPS
              value: {{ .Values.appServiceUploader.destHttps | quote }}
            - name: UPLOADER_GCR
              value: {{ .Values.appServiceUploader.gcr | quote }}
          volumeMounts:
            - name: app-service-keystore
              mountPath: {{ .Values.appService.mount.https }}
            - name: app-service-config
              mountPath: /home/jboss/config
            - mountPath: /certs/tokens
              name: service-account-token
            - name: ca-bundle
              mountPath: {{ .Values.certificate.cacertsPath }}
              subPath: cacerts
      volumes:
        # keystore from app-service-certificate
        - name: app-service-keystore
          secret:
            secretName: {{ include "app-service.certificate.secret.name" . }}
            items:
              - key: keystore.jks
              {{- if .Values.certificate.keystore.separate.enabled }}
                path: {{ include "app-service.tls.keystore.file" . }}
              {{- else }}
                path: {{ include "app-service.iam.keystore.file" . }}
              {{- end }}
              - key: tls.crt
                path: tls.crt
        - name: ca-bundle
          configMap:
            name: {{ .Values.global.pki.caBundleConfigMapName }}
            items:
              - key: {{ .Values.global.pki.caBundleConfigMapKeyJks }}
                path: cacerts
              - key: {{ .Values.global.pki.caBundleConfigMapKeyPem }}
                path: ca-bundle.crt
        - name: app-service-config
          configMap:
            name: app-service-configmap
            items:
              - key: application.properties
                path: application.properties
        - name: service-account-token
          projected:
            sources:
              - serviceAccountToken:
                  path: service-account-token
                  expirationSeconds: 3600
      {{- if not .Values.global.publicImageRepository }}
      imagePullSecrets:
        - name: regcred
      {{- end }}
