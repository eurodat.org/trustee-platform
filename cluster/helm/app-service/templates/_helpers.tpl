{{/* Expand the name of the chart. */}}
{{- define "app-service.name" -}}
{{ default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "app-service.namespace" -}}
{{ default .Release.Namespace .Values.namespaceOverride }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "app-service.fullname" }}
{{- if .Values.fullnameOverride -}}
{{ .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else -}}
{{ $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name -}}
{{ .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else -}}
{{ printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/* Create chart name and version as used by the chart label. */}}
{{- define "app-service.chart" -}}
{{ printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Common labels */}}
{{- define "app-service.labels" -}}
helm.sh/chart: {{ include "app-service.chart" . }}
{{ include "app-service.selector.labels" . }}
{{ if .Chart.AppVersion -}}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/* Selector labels */}}
{{- define "app-service.selector.labels" -}}
app.kubernetes.io/name: {{ include "app-service.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "ingress.class.name" -}}
{{ default (print .Chart.Name "-nginx") .Values.ingress.className }}
{{- end }}

{{/* -------------------------------------- Resource names --------------------------------- */}}

{{- define  "app-service.configmap.name" -}}
{{ .Chart.Name }}-configmap
{{- end }}

{{- define "app-service.service.name" -}}
{{ include "app-service.fullname" . }}-svc
{{- end }}

{{- define "app-service.service.account.name" -}}
{{ default (print .Chart.Name "-service-account") .Values.serviceAccount.name }}
{{- end }}

{{- define "app-service.role" -}}
app-service-role
{{- end -}}

{{- define "image-job.role" -}}
image-job-role
{{- end -}}

{{/* --------------------- Certificate-keystore & keystore-password resources -------------- */}}

{{- define "app-service.keystore.password.secret.key" -}}
{{ .Values.certificate.secret.key }}
{{- end }}

{{- define "app-service.keystore.password.secret.name" -}}
{{ default (printf "%s-keystore-password-secret" .Chart.Name) .Values.certificate.secret.name }}
{{- end }}

{{/* If no password was specified, the chart will generate it itself based on the current unix time */}}
{{- define "app-service.password.secret.password" -}}
{{ default (now | unixEpoch | trunc 8) .Values.certificate.secret.password }}
{{- end }}

{{- define "app-service.iam.keystore.file" -}}
{{ .Values.certificate.keystore.iam.file }}
{{- end }}

{{- define "app-service.tls.keystore.file" -}}
{{ .Values.certificate.keystore.tls.file }}
{{- end }}

{{/* -------------------------------------- Certificates ----------------------------------- */}}

{{- define "app-service.certificate.name" -}}
{{ .Chart.Name }}-certificate
{{- end }}

{{- define "app-service.certificate.secret.name" -}}
{{ include "app-service.certificate.name" . }}-secret
{{- end }}

{{/* -------------------------------------- Argo Workflows --------------------------------------- */}}

{{- define "eurodat.argo.url" -}}
{{ printf "https://eurodat-argo-workflows-server.%s.svc:%d" .Values.argo.namespace (.Values.ports.argoServer | int) }}
{{- end -}}

{{/* -------------------------------------- IAM service ------------------------------------ */}}

{{- define "eurodat.iam.service.internal.url" -}}
eurodat-keycloak-headless.base.svc:8443
{{- end }}

{{- define "eurodat.iam.realm" -}}
{{ .Values.iam.realm }}
{{- end }}

{{- define "eurodat.iam.fqdn" -}}
{{ .Values.iam.fqdn }}
{{- end }}

{{- define "eurodat.iam.endpoint.internal" -}}
{{ printf "%s/realms/%s" (include "eurodat.iam.base.url.internal" .) (include "eurodat.iam.realm" .) }}
{{- end }}

{{- define "eurodat.iam.endpoint.external" -}}
{{ printf "%s/realms/%s" (include "eurodat.iam.base.url.external" .) (include "eurodat.iam.realm" .) }}
{{- end }}

{{- define "eurodat.iam.base.url.internal" -}}
{{ printf "https://%s/auth" (include "eurodat.iam.service.internal.url" .) }}
{{- end }}

{{- define "eurodat.iam.base.url.external" -}}
{{ printf "https://%s/auth" .Values.fqdn }}
{{- end }}

{{- define "eurodat.database-service.namespace" -}}
{{ default .Release.Namespace .Values.namespaceOverride }}
{{- end -}}

{{- define "eurodat.database-service.name" -}}
eurodat-database-service-svc
{{- end -}}

{{- define "eurodat.database-service.path" -}}
api/v1/database-service
{{- end -}}

{{- define "eurodat.database-service.port" -}}
8443
{{- end -}}

{{- define "eurodat.transaction-service.namespace" -}}
{{ default .Release.Namespace .Values.namespaceOverride }}
{{- end -}}

{{- define "eurodat.transaction-service.name" -}}
eurodat-transaction-service-svc
{{- end -}}

{{- define "eurodat.transaction-service.path" -}}
api/v1
{{- end -}}

{{- define "eurodat.transaction-service.port" -}}
8443
{{- end -}}
