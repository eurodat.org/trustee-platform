{{/*
Expand the name of the chart.
*/}}
{{- define "eurodat-app.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "eurodat-app.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "eurodat-app.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "eurodat-app.labels" -}}
helm.sh/chart: {{ include "eurodat-app.chart" . }}
{{ include "eurodat-app.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "eurodat-app.selectorLabels" -}}
app.kubernetes.io/name: {{ include "eurodat-app.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "eurodat-app.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "eurodat-app.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the name of the configmap to use
*/}}
{{- define  "eurodat-app.configmap.name" -}}
{{ .Chart.Name }}-configmap
{{- end }}

{{/*
Create full image ID (Assumes context of .Values.image)
*/}}
{{- define "eurodat-app.fullImageId" }}
{{- $result := "" }}
{{- if .registry}}
{{- $result = printf "%s/%s" .registry .repository }}
{{- else }}
{{- $result = .repository }}
{{- end }}
{{- if .tag}}
{{- $result = printf "%s:%s" $result .tag }}
{{- end }}
{{- if .digest}}
{{- $result = printf "%s@%s" $result .digest }}
{{- end }}
{{- $result }}
{{- end }}
