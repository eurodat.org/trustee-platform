{{/* Expand the name of the chart. */}}
{{- define "data-management-service.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "data-management-service.namespace" -}}
{{ default .Release.Namespace .Values.namespaceOverride }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "data-management-service.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "data-management-service.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "data-management-service.labels" -}}
helm.sh/chart: {{ include "data-management-service.chart" . }}
{{ include "data-management-service.selector.labels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "data-management-service.selector.labels" -}}
app.kubernetes.io/name: {{ include "data-management-service.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "ingress.class.name" -}}
{{ default (print .Chart.Name "-nginx") .Values.ingress.className }}
{{- end }}

{{/* -------------------------------------- Resource names --------------------------------- */}}

{{- define  "data-management-service.configmap.name" -}}
{{ .Chart.Name }}-configmap
{{- end }}

{{- define "data-management-service.service.name" -}}
{{ include "data-management-service.fullname" . }}-svc
{{- end }}

{{- define "data-management-service.service.account.name" -}}
{{ default (print .Chart.Name "-service-account") .Values.serviceAccount.name }}
{{- end }}

{{/* -------------------------------------- Certificates ----------------------------------- */}}

{{- define "data-management-service.certificate.name" -}}
{{ .Chart.Name }}-certificate
{{- end }}

{{- define "data-management-service.certificate.secret.name" -}}
{{ include "data-management-service.certificate.name" . }}-secret
{{- end }}

{{/* --------------------- Certificate-keystore & keystore-password resources -------------- */}}

{{- define "data-management-service.keystore.password.secret.key" -}}
{{ .Values.certificate.secret.key }}
{{- end }}

{{- define "data-management-service.keystore.password.secret.name" -}}
{{ default (printf "%s-keystore-password-secret" .Chart.Name) .Values.certificate.secret.name }}
{{- end }}

{{/* If no password was specified, the chart will generate it itself based on the current unix time */}}
{{- define "data-management-service.password.secret.password" -}}
{{ default (now | unixEpoch | trunc 8) .Values.certificate.secret.password }}
{{- end }}

{{- define "data-management-service.iam.keystore.file" -}}
{{ .Values.certificate.keystore.iam.file }}
{{- end }}

{{- define "data-management-service.tls.keystore.file" -}}
{{ .Values.certificate.keystore.tls.file }}
{{- end }}

{{/* -------------------------------------- IAM service ------------------------------------ */}}

{{- define "eurodat.iam.service.internal.url" -}}
eurodat-keycloak-headless.base.svc:8443
{{- end }}

{{- define "eurodat.iam.realm" -}}
{{ .Values.iam.realm }}
{{- end }}

{{- define "eurodat.iam.fqdn" -}}
{{ .Values.iam.fqdn }}
{{- end }}

{{- define "eurodat.iam.endpoint.internal" -}}
{{ printf "%s/realms/%s" (include "eurodat.iam.base.url.internal" .) (include "eurodat.iam.realm" .) }}
{{- end }}

{{- define "eurodat.iam.endpoint.external" -}}
{{ printf "%s/realms/%s" (include "eurodat.iam.base.url.external" .) (include "eurodat.iam.realm" .) }}
{{- end }}

{{- define "eurodat.iam.base.url.internal" -}}
{{ printf "https://%s/auth" (include "eurodat.iam.service.internal.url" .) }}
{{- end }}

{{- define "eurodat.iam.base.url.external" -}}
{{ printf "https://%s/auth" .Values.fqdn }}
{{- end }}

{{/* -------------------------------------- Services ------------------------------------ */}}

{{- define "eurodat.credential-service.namespace" -}}
{{ default .Release.Namespace .Values.namespaceOverride }}
{{- end -}}

{{- define "eurodat.credential-service.name" -}}
eurodat-credential-service-svc
{{- end -}}

{{- define "eurodat.credential-service.path" -}}
api/v1/credential-service
{{- end -}}

{{- define "eurodat.credential-service.port" -}}
8443
{{- end -}}
