{{/*
Create the :tag@digest part of an image reference from the values .tag and .digest
Can be :tag, @digest, or :tag@digest
*/}}

{{- define "helpers.createImageTagDigestString" -}}
{{- $result := "" }}
{{- if .tag }}
{{- $result = printf ":%s" .tag }}
{{- end }}
{{- if .digest }}
{{- $result = printf "%s@%s" $result .digest }}
{{- end }}
{{- $result }}
{{- end }}


{{/*
Create a the registry/repository part of an image reference from values .registry and .repository
*/}}

{{- define "helpers.createImageRegistryRepositoryString" -}}
{{- $result := "" }}
{{- if .registry }}
    {{- $result = printf "%s/" .registry }}
{{- end }}
{{- printf "%s%s" $result .repository}}
{{- end }}


{{/*
Create a full image reference from values .registry, .repository, .tag, and .digest.
*/}}

{{- define "helpers.createImageReference" -}}
{{- printf "%s%s" (include "helpers.createImageRegistryRepositoryString" .) (include "helpers.createImageTagDigestString" .) }}
{{- end }}

{{/*
Print the four lines of configuration values for an image reference
*/}}

{{- define "helpers.printImageConfig" -}}
registry: {{ .registry }}
repository: {{ .repository }}
tag: {{ .tag | quote }}
digest: {{ .digest }}
{{- end }}

{{/*
Print the three lines of configuration values for an image reference with tag value equal to dummy-tag-is-ignored@digest and no digest
*/}}

{{- define "helpers.printImageConfigWithMergedDummyTagDigest" -}}
registry: {{ .registry }}
repository: {{ .repository }}
tag: {{ include "helpers.createDummyTagDigest" . | quote }}
{{- end }}

{{/*
Print the three lines of configuration values for an image reference with repository value equal to registry/repository
and no registry
*/}}

{{- define "helpers.printImageConfigWithMergedRegistryRepository" -}}
repository: {{ include "helpers.createImageRegistryRepositoryString" . }}
tag: {{ .tag | quote }}
digest: {{ .digest }}
{{- end }}

{{/*
Creates dummy-tag-is-ignored@digest for a given image definition with digest.
*/}}

{{- define "helpers.createDummyTagDigest" -}}
{{- printf "dummy-tag-is-ignored@%s" (required "Missing digest" .digest) }}
{{- end }}

{{/*
Print the two lines of configuration values for an image reference with repository value equal to registry/repository,
tag value equal to dummy-tag-is-ignored@digest and no registry or digest. The actual tag (if any) will be replaced with
dummy-tag-is-ignored since tags are ignored when a digest is present.
*/}}

{{- define "helpers.printImageConfigWithMergedRegistryRepositoryAndDummyTagDigest" -}}
repository: {{ include "helpers.createImageRegistryRepositoryString" . }}
tag: {{ include "helpers.createDummyTagDigest" . | quote }}
{{- end }}

{{/*
Set the fqdn based on external flag
*/}}

{{- define "helpers.fqdn" -}}
{{- if .Values.useExternalLoadBalancer }}
{{- .Values.externalFqdn }}
{{- else }}
{{- .Values.cluster.fqdn }}
{{- end }}
{{- end }}
