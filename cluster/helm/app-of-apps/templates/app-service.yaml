apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: app-service-{{ .Values.environment }}
  namespace: base
  finalizers:
    - resources-finalizer.argocd.argoproj.io
  annotations:
    argocd.argoproj.io/sync-wave: {{ .Values.syncWave.microServices | quote }}
spec:
  project: {{ .Values.environment }}
  source:
    repoURL: {{ .Values.repoURL }}
    targetRevision: {{ .Values.targetRevision }}
    {{- if .Values.useReleasedCharts }}
    chart: app-service
    {{- else }}
    path: cluster/helm/app-service
    {{- end }}
    helm:
      releaseName: eurodat
      values: |
        global:
          useSelfSigned: {{ .Values.useSelfSigned }}
          publicImageRepository: {{ .Values.publicImageRepository }}
        fqdn: {{ include "helpers.fqdn" . }}
        registry: {{ .Values.cluster.registry }}
        iam:
          fqdn: {{ include "helpers.fqdn" . }}
        images:
          appService: {{ include "helpers.printImageConfig" .Values.images.appService | nindent 12 }}
          appServiceUploader: {{ include "helpers.printImageConfig" .Values.images.appServiceUploader | nindent 12 }}
        appServiceUploader:
          namespace: {{ .Values.appServiceUploader.namespace }}
          serviceAccount: {{ .Values.appServiceUploader.serviceAccount }}
          gcpServiceAccount: {{ .Values.appServiceUploader.gcpServiceAccount }}
          destAuth: {{ .Values.appServiceUploader.destAuth }}
          srcHttps: {{ .Values.appServiceUploader.srcHttps }}
          destHttps: {{ .Values.appServiceUploader.destHttps }}
          gcr: {{ .Values.appServiceUploader.gcr }}
        cluster:
          internalApiEndpoint: {{ .Values.cluster.internalApiEndpoint }}
  # Ignore differences that arise from auto-generated values
  ignoreDifferences:
    - group: "*"
      kind: Secret
      namespace: {{ .Values.namespaces.controlPlane }}
      name: app-service-keystore-password-secret
      jsonPointers:
        - /data
  destination:
    server: {{ .Values.cluster.apiAddress }}
    namespace: {{ .Values.namespaces.controlPlane }}
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
