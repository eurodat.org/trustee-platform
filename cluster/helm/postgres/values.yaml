securePasswords: true

postgresql:
  fullnameOverride: eurodat-postgresql
  auth:
    database: postgres
    enablePostgresUser: true
    existingSecret: postgresql-credentials-secret
    secretKeys:
      adminPasswordKey: postgresPassword
  tls:
    enabled: true
    certificatesSecret: postgresql-certificate-secret
    certFilename: tls.crt
    certKeyFilename: tls.key
  primary:
    ## Initdb configuration
    initdb:
      ## @param primary.initdb.scriptsConfigMap ConfigMap with scripts to be run at first boot
      scriptsConfigMap: postgresql-init-config
    preInitScripts:
      name: pre-init-scripts
      mountPath: /docker-entrypoint-preinitdb.d/
      configMap: postgresql-preinit-config
    customSqlScripts:
      name: sql-scripts
      mountPath: /opt/bitnami/postgresql/conf/sql_scripts
      configMap: postgresql-sql-config
    ## @param primary.annotations Annotations for PostgreSQL primary pods
    annotations:
      reloader.stakater.com/auto: "true"
    extraEnvVarsSecret: postgresql-user-credentials-secret
    customPgHbaConfiguration:
      name: pg-hba-conf
      mountPath: /opt/bitnami/postgresql/conf/custom_pg_hba
      configMap: postgresql-pg-hba-config
    extraEnvVars: |
      - name: "CUSTOM_PGHBA_FILE"
        value: {{ .Values.primary.customPgHbaConfiguration.mountPath }}/pg_hba.conf
      - name: "CUSTOM_INIT_SQL_DIR"
        value: {{ .Values.primary.customSqlScripts.mountPath }}
    extraVolumeMounts: |
      - name: {{ .Values.primary.customPgHbaConfiguration.name }}
        mountPath: {{ .Values.primary.customPgHbaConfiguration.mountPath }}
      - name: {{ .Values.primary.preInitScripts.name }}
        mountPath: {{ .Values.primary.preInitScripts.mountPath }}
      - name: {{ .Values.primary.customSqlScripts.name }}
        mountPath: {{ .Values.primary.customSqlScripts.mountPath }}
    extraVolumes: |
      - name: {{ .Values.primary.customPgHbaConfiguration.name }}
        configMap:
          name: {{ .Values.primary.customPgHbaConfiguration.configMap }}
      - name: {{ .Values.primary.preInitScripts.name }}
        configMap:
          name: {{ .Values.primary.preInitScripts.configMap }}
      - name: {{ .Values.primary.customSqlScripts.name }}
        configMap:
          name: {{ .Values.primary.customSqlScripts.configMap }}
    extendedConfiguration: |
      shared_preload_libraries = 'auth_delay'
      auth_delay.milliseconds = '1000'

# Cluster ingress settings
ingress:
  # ingressClassName
  className: cluster-nginx

argo:
  provisionNamespace: data-plane
  namespace: transaction-plane

vault:
  namespace: transaction-plane

ports:
  # Database port used for EDC resource persistence
  psql: 5432
