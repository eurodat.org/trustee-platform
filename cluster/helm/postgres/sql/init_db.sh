#!/usr/bin/env bash
# shellcheck disable=SC1091
set -euo pipefail
. /opt/bitnami/scripts/libpostgresql.sh
. /opt/bitnami/scripts/liblog.sh

info "Creating databases and users if they do not exist."
postgresql_execute_print_output "$POSTGRESQL_DATABASE" "$POSTGRESQL_INITSCRIPTS_USERNAME" "$POSTGRESQL_INITSCRIPTS_PASSWORD" << EOF
SET client_min_messages TO NOTICE;
DO \$\$
BEGIN
   IF NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = 'vault') THEN
      CREATE ROLE vault NOSUPERUSER CREATEDB CREATEROLE LOGIN PASSWORD '$VAULT_PASSWORD';
      RAISE NOTICE 'Role vault created';
   ELSE
      RAISE NOTICE 'Role vault already exists. Skipping...';
   END IF;
END
\$\$;

GRANT pg_signal_backend TO vault;


SELECT 'CREATE DATABASE transaction_service'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'transaction_service')\gexec

DO \$\$
BEGIN
   IF NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = 'transaction_service') THEN
      CREATE ROLE transaction_service LOGIN PASSWORD '$TRANSACTION_SERVICE_PASSWORD';
      RAISE NOTICE 'Role transaction_service created';
   ELSE
      RAISE NOTICE 'Role transaction_service already exists. Skipping...';
   END IF;
END
\$\$;

ALTER DATABASE transaction_service OWNER TO transaction_service;


SELECT 'CREATE DATABASE contract_service'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'contract_service')\gexec

DO \$\$
BEGIN
   IF NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = 'contract_service') THEN
      CREATE ROLE contract_service LOGIN PASSWORD '$CONTRACT_SERVICE_PASSWORD';
      RAISE NOTICE 'Role contract_service created';
   ELSE
      RAISE NOTICE 'Role contract_service already exists. Skipping...';
   END IF;
END
\$\$;

ALTER DATABASE contract_service OWNER TO contract_service;


SELECT 'CREATE DATABASE app_service'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'app_service')\gexec

DO \$\$
BEGIN
   IF NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = 'app_service') THEN
      CREATE ROLE app_service LOGIN PASSWORD '$APP_SERVICE_PASSWORD';
      RAISE NOTICE 'Role app_service created';
   ELSE
      RAISE NOTICE 'Role app_service already exists. Skipping...';
   END IF;
END
\$\$;

ALTER DATABASE app_service OWNER TO app_service;

SELECT 'CREATE DATABASE argo'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'argo')\gexec

DO \$\$
BEGIN
   IF NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = '$ARGO_USERNAME') THEN
      CREATE USER $ARGO_USERNAME WITH ENCRYPTED PASSWORD '$ARGO_PASSWORD';
      RAISE NOTICE 'Role $ARGO_USERNAME created';
   ELSE
      RAISE NOTICE 'Role $ARGO_USERNAME already exists. Skipping...';
   END IF;
END
\$\$;

ALTER DATABASE argo OWNER TO $ARGO_USERNAME;


SELECT 'CREATE DATABASE keycloak'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'keycloak')\gexec

DO \$\$
BEGIN
   IF NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = 'keycloak') THEN
      CREATE USER keycloak WITH ENCRYPTED PASSWORD '$KEYCLOAK_PASSWORD';
      RAISE NOTICE 'Role keycloak created';
   ELSE
      RAISE NOTICE 'Role keycloak already exists. Skipping...';
   END IF;
END
\$\$;

ALTER DATABASE keycloak OWNER TO keycloak;

EOF

info "Replacing auto-generated pg_hba.conf with custom file."
cp "$CUSTOM_PGHBA_FILE" "$POSTGRESQL_PGHBA_FILE"
