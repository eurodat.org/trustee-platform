# Based on helm chart example (ingress-nginx _helpers.tlp file)

{{/*
Expand the name of the chart.
*/}}
{{- define "eurodat.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "eurodat.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "eurodat.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}


{{/*
Common labels
*/}}
{{- define "eurodat.labels" -}}
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
{{ include "eurodat.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "eurodat.selectorLabels" -}}
app.kubernetes.io/name: {{ .Chart.Name }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}


{{/*
Create default fully qualified names.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}

{{- define "transaction-service.dataplane.fullname" -}}
{{- printf "%s-%s" (include "eurodat.fullname" .) "data-plane" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "transaction-service.scheduler.secret" -}}
transaction-service-scheduler-credentials-secret
{{- end -}}

{{/* -------------------------------------- IAM service ------------------------------------ */}}

{{- define "eurodat.iam.service.internal.url" -}}
eurodat-keycloak-headless.base.svc:8443
{{- end }}

{{- define "eurodat.vault.redirect.uri" -}}
{{ printf "https://%s.%s:%d/ui/vault/auth/oidc/oidc/callback" .Values.vault.serviceName .Values.vault.namespace (.Values.vault.vaultPort | int) }}
{{- end }}

{{- define "eurodat.fqdn" -}}
{{ printf "https://%s" .Values.iam.fqdn }}
{{- end }}

{{/*
Allowed keycloak redirect uris
*/}}
{{- define "eurodat.frontend.redirect.uris" -}}
{{- $result := list }}
{{- $result = append $result ((printf "%s/*" (include "eurodat.fqdn" .)) | quote) }}

{{- if .Values.frontendDevUri }}
{{- $result = append $result ((printf "http://%s/*" .Values.frontendDevUri) | quote) }}
{{- end }}

{{- printf "[%s]" (join ", " $result) | quote }}
{{- end }}
