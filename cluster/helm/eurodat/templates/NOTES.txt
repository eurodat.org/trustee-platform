Validating the following required values (since they have no default values):
{{ required "A valid iam.fqdn is required!" .Values.iam.fqdn }}
{{ required "A valid iam.smtpMailFrom is required!" .Values.iam.smtpMailFrom }}

{{ if not .Values.deploySmtpExternalSecret }}
{{ required "A valid localSmtpApiKeys.smtpApiKey is required if .Values.deploySmtpExternalSecret is false" .Values.localSmtpApiKeys.smtpApiKey }}
{{ required "A valid localSmtpApiKeys.smtpApiSecretKey is required if .Values.deploySmtpExternalSecret is false" .Values.localSmtpApiKeys.smtpApiSecretKey }}
{{ end }}
