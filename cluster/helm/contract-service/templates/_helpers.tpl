{{/* Expand the name of the chart. */}}
{{- define "contract-service.name" -}}
{{ default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "contract-service.namespace" -}}
{{ default .Release.Namespace .Values.namespaceOverride }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "contract-service.fullname" }}
{{- if .Values.fullnameOverride -}}
{{ .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else -}}
{{ $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name -}}
{{ .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else -}}
{{ printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/* Create chart name and version as used by the chart label. */}}
{{- define "contract-service.chart" -}}
{{ printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Common labels */}}
{{- define "contract-service.labels" -}}
helm.sh/chart: {{ include "contract-service.chart" . }}
{{ include "contract-service.selector.labels" . }}
{{ if .Chart.AppVersion -}}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/* Selector labels */}}
{{- define "contract-service.selector.labels" -}}
app.kubernetes.io/name: {{ include "contract-service.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "ingress.class.name" -}}
{{ default (print .Chart.Name "-nginx") .Values.ingress.className }}
{{- end }}

{{/* -------------------------------------- Resource names --------------------------------- */}}

{{- define "contract-service.service.name" -}}
{{ include "contract-service.fullname" . }}-svc
{{- end }}

{{- define "contract-service.service.account.name" -}}
{{ default (print .Chart.Name "-service-account") .Values.serviceAccount.name }}
{{- end }}

{{/* --------------------- Certificate-keystore & keystore-password resources -------------- */}}

{{- define "contract-service.keystore.password.secret.key" -}}
{{ .Values.certificate.secret.key }}
{{- end }}

{{- define "contract-service.keystore.password.secret.name" -}}
{{ default (printf "%s-keystore-password-secret" .Chart.Name) .Values.certificate.secret.name }}
{{- end }}
{{/* If no password was specified, the chart will generate it itself based on the current unix time */}}
{{- define "contract-service.password.secret.password" -}}
{{ default (now | unixEpoch | trunc 8) .Values.certificate.secret.password }}
{{- end }}

{{- define "contract-service.iam.keystore.file" -}}
{{ .Values.certificate.keystore.iam.file }}
{{- end }}

{{- define "contract-service.tls.keystore.file" -}}
{{ .Values.certificate.keystore.tls.file }}
{{- end }}

{{/* -------------------------------------- Certificates ----------------------------------- */}}

{{- define "contract-service.certificate.name" -}}
{{ .Chart.Name }}-certificate
{{- end }}

{{- define "contract-service.certificate.secret.name" -}}
{{ include "contract-service.certificate.name" . }}-secret
{{- end }}

{{/* -------------------------------------- IAM service ------------------------------------ */}}

{{- define "eurodat.iam.service.internal.url" -}}
eurodat-keycloak-headless.base.svc:8443
{{- end }}

{{- define "eurodat.iam.fqdn" -}}
{{ .Values.iam.fqdn }}
{{- end }}

{{- define "eurodat.iam.endpoint.internal" -}}
{{ printf "%s/realms/%s" (include "eurodat.iam.base.url.internal" .) .Values.iam.realm }}
{{- end }}

{{- define "eurodat.iam.endpoint.external" -}}
{{ printf "%s/realms/%s" (include "eurodat.iam.base.url.external" .) .Values.iam.realm  }}
{{- end }}

{{- define "eurodat.iam.base.url.internal" -}}
{{ printf "https://%s/auth" (include "eurodat.iam.service.internal.url" .) }}
{{- end }}

{{- define "eurodat.iam.base.url.external" -}}
{{ printf "https://%s/auth" .Values.fqdn }}
{{- end }}

{{- define "paperless.url" -}}
{{- if .Values.paperless.mocked -}}
http://localhost:5555
{{- else -}}
https://api.paperless.io
{{- end }}
{{- end }}
