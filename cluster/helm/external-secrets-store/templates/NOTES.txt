Validating the following required values (since they have no default values):
{{ required "Cluster name is required!" .Values.cluster.name }}
{{ required "Cluster location is required!" .Values.cluster.location }}
