secret_settings(disable_scrub=True)
# Parse configuration of credentials
# Be sure to set confidential / private values in ./tilt_config.json or specify on command line

config.define_string('SMTP_HOST')
config.define_string('SMTP_SERVER_IP')
config.define_string('SMTP_PORT')
config.define_string('SMTP_API_KEY')
config.define_string('SMTP_API_SECRET_KEY')
config.define_string('PAPERLESS_API_KEY')

config.define_string_list('disable', args=True)

# Set to True to enable log collection with Promtail and Loki
loki_enabled = False

cfg = config.parse()

# Configuration of non-confidential / non-private values
POSTGRES_PASSWORD = "pwd"
CERT_MANAGER_HELM_CHART_VERSION = "1.13.2"
CERT_MANAGER_IMAGE_REPOSITORY = "cgr.dev/chainguard"
CERT_MANAGER_CONTROLLER_DIGEST = "sha256:91f38ac46342a42e40a1ffc291d53a8e6770071301fb48d1a88292449c41fad9"
CERT_MANAGER_WEBHOOK_DIGEST = "sha256:d939ad37d14e2997d4bf61b3f29a6dc11df3ac7251a8605ec4a13e2c380693d1"
CERT_MANAGER_ACMESOLVER_DIGEST = "sha256:348f9609d937dbc8789badf4ca630523ee78ea9e4b6d81e5c452874c5a140728"
CERT_MANAGER_CAINJECTOR_DIGEST = "sha256:7208b28418036febcbc4a582df2c0ea7af8ada1f82fc3ff51428ec54ef5df082"
ADDITIONAL_CORS_ORIGIN = "/.*/"

# define group names do deactivate multiple services at once
groups = {
    'app-service': ['eurodat-app-service', 'app-service-ingress', 'app-service-argo'],
    'argo-workflows': [
        'argo-workflows-controller',
        'argo-workflows-server',
        'argo-workflows-config',
         ],
    'transaction-service': ['eurodat-transaction-service', 'transaction-service-config', 'transaction-service-ingress', 'transaction-service-apps-redirect-ingress', 'transaction-service-argo'],
    'contract-service': ['eurodat-contract-service','contract-service-config', 'contract-service-ingress'],
    'credential-service': ['credential-service', 'credential-service-config', 'credential-service-ingress'],
    'data-management-service': ['eurodat-data-management-service','data-management-service-config', 'data-management-service-ingress'],
    'eurodat-app': ['eurodat-app', 'eurodat-app-config', 'extract-cypress-user-password'],
    'database-service': ['database-service', 'database-service-config', 'database-service-ingress'],
    'config': [
        'cert-manager',
        'eurodat-cluster-reflector',
        'eurodat-cluster-reloader',
        'restart-reflector',
        'cluster-config',
        'cert-management',
        'trust-manager',
        'smtp-keys',
        'restart-reflector-config',
        'eurodat-certificates',
        'eurodat-cluster-kubernetes-secret-generator',
        'eurodat-network-policies'
        ],
    'data-plane': ['eurodat-data-plane'],
    'ingress': ['ingress-nginx'],
    'keycloak': ['keycloak', 'keycloak-config', 'keycloak-config-job'],
    'transaction-vault': ['eurodat-transaction-vault-agent-injector', 'eurodat-transaction-vault', 'eurodat-transaction-vault-config'],
    'monitoring': ['grafana', 'grafana-config', 'opentelemetry-collector', 'opentelemetry-collector-config'],
    'postgres': ['eurodat-postgresql', 'postgres-config'],
    'logs': ['loki', 'loki-config', 'promtail', 'promtail-config'],
    'default': []
}

# list of all resources needed to invert from selecting services to enable to disable
resources = [
    'eurodat-contract-service',
    'contract-service-config',
    'contract-service-ingress',
    'eurodat-app-service',
    'app-service-ingress',
    'app-service-argo',
    'app-service-config',
    'argo-workflows-controller',
    'argo-workflows-server',
    'argo-workflows-config',
    'argo-workflows-example',
    'eurodat-credential-service',
    'credential-service-config',
    'eurodat-database-service',
    'database-service-config',
    'eurodat-transaction-service',
    'eurodat-postgresql',
    'transaction-service-config',
    'transaction-service-ingress',
    'transaction-service-apps-redirect-ingress',
    'transaction-service-argo',
    'cert-manager',
    'eurodat-cluster-reflector',
    'eurodat-cluster-reloader',
    'restart-reflector',
    'cluster-config',
    'cert-management',
    'trust-manager',
    'smtp-keys',
    'restart-reflector-config',
    'eurodat-certificates',
    'eurodat-cluster-kubernetes-secret-generator',
    'eurodat-network-policies',
    'eurodat-data-plane',
    'ingress-nginx',
    'keycloak',
    'eurodat-transaction-vault-agent-injector',
    'eurodat-transaction-vault',
    'eurodat-transaction-vault-config',
    'grafana',
    'grafana-config',
    'opentelemetry-collector',
    'opentelemetry-collector-config',
    'eurodat-app',
    'eurodat-app-config',
    'extract-cypress-user-password',
    'push_hello-world',
    'eurodat-data-management-service',
    'data-management-service-config',
]

disabled_services = []

for arg in cfg.get('disable', []):
    if arg in groups:
        disabled_services += groups[arg]
    elif arg in resources:
        # disable individual services additionally to groups, e.g., `make start xcl='group1 group2 service1`
        disabled_services.append(arg)

if len(disabled_services)==0:
    # ensure that default enables all services (also services missing from resources list above)
    resources = []
else:
    # complement
    resources = [item for item in resources if item not in disabled_services]

config.set_enabled_resources(resources)


cluster_fqdn = local(
    'venv/bin/python3 ./tilt/scripts/get_fqdn.py', command_bat=['./venv/Scripts/python.exe', 'tilt/scripts/get_fqdn.py']
)

# Load dependencies

load('ext://helm_resource', 'helm_resource', 'helm_repo')
load('ext://secret', 'secret_from_dict')
load('./tilt/lib/Tiltfile', 'create_namespace')
load('./tilt/lib/Tiltfile', 'set_namespace')
load('ext://helm_remote', 'helm_remote')

# Create namespaces

create_namespace('cert-manager')
base_namespace = 'base'
create_namespace(base_namespace)
control_plane_namespace = 'control-plane'
create_namespace(control_plane_namespace)
data_plane_namespace = 'data-plane'
create_namespace(data_plane_namespace)
transaction_plane_namespace = 'transaction-plane'
create_namespace(transaction_plane_namespace)
frontend_namespace = 'frontend'
create_namespace(frontend_namespace)

custom_build(
    'transaction-service',
    './call tilt/scripts/tilt_call_jib.sh services/transaction-service build',
    command_bat='call tilt/scripts/tilt_call_jib services/transaction-service build',
    deps=[
        'services/app-service-client/build.gradle.kts',
        'services/app-service-client/gradle.properties',
        'services/app-service-client/settings.gradle.kts',
        'services/app-service-client/src/main',
        'services/transaction-service/build.gradle.kts',
        'services/transaction-service/gradle.properties',
        'services/transaction-service/settings.gradle.kts',
        'services/transaction-service/src/main',
        'services/credential-service-client/build.gradle.kts',
        'services/credential-service-client/gradle.properties',
        'services/credential-service-client/settings.gradle.kts',
        'services/credential-service-client/src/main',
        'services/database-service-client/build.gradle.kts',
        'services/database-service-client/gradle.properties',
        'services/database-service-client/settings.gradle.kts',
        'services/database-service-client/src/main',
        'services/common/build.gradle.kts',
        'services/common/gradle.properties',
        'services/common/settings.gradle.kts',
        'services/common/src/main',
    ],
    skips_local_docker=True,
)

custom_build(
    'credential-service',
    './call tilt/scripts/tilt_call_jib.sh services/credential-service build',
    command_bat='call tilt/scripts/tilt_call_jib services/credential-service build',
    deps=[
        'services/app-service-client/build.gradle.kts',
        'services/app-service-client/gradle.properties',
        'services/app-service-client/settings.gradle.kts',
        'services/app-service-client/src/main',
        'services/credential-service/build.gradle.kts',
        'services/credential-service/gradle.properties',
        'services/credential-service/settings.gradle.kts',
        'services/credential-service/src/main',
        'services/transaction-service-client/build.gradle.kts',
        'services/transaction-service-client/gradle.properties',
        'services/transaction-service-client/settings.gradle.kts',
        'services/transaction-service-client/src/main',
        'services/common/build.gradle.kts',
        'services/common/gradle.properties',
        'services/common/settings.gradle.kts',
        'services/common/src/main',
    ],
    skips_local_docker=True,
)

custom_build(
    'app-service',
    './call tilt/scripts/tilt_call_jib.sh services/app-service build',
    command_bat='call tilt/scripts/tilt_call_jib services/app-service build',
    deps=[
        'services/app-service/build.gradle.kts',
        'services/app-service/gradle.properties',
        'services/app-service/settings.gradle.kts',
        'services/app-service/src/main',
        'services/app-service-client/build.gradle.kts',
        'services/app-service-client/gradle.properties',
        'services/app-service-client/settings.gradle.kts',
        'services/app-service-client/src/main',
    ],
    skips_local_docker=True,
)

custom_build(
    'database-service',
    './call tilt/scripts/tilt_call_jib.sh services/database-service build',
    command_bat='call tilt/scripts/tilt_call_jib services/database-service build',
    deps=[
        'services/database-service/build.gradle.kts',
        'services/database-service/gradle.properties',
        'services/database-service/settings.gradle.kts',
        'services/database-service/src/main',
        'services/common/build.gradle.kts',
        'services/common/gradle.properties',
        'services/common/settings.gradle.kts',
        'services/common/src/main',
        'services/credential-service-client/build.gradle.kts',
        'services/credential-service-client/gradle.properties',
        'services/credential-service-client/settings.gradle.kts',
        'services/credential-service-client/src/main',
        'services/app-service-client/build.gradle.kts',
        'services/app-service-client/gradle.properties',
        'services/app-service-client/settings.gradle.kts',
        'services/app-service-client/src/main',
    ],
    skips_local_docker=True,
)

custom_build(
    'contract-service',
    './call tilt/scripts/tilt_call_jib.sh services/contract-service build',
    command_bat='call tilt/scripts/tilt_call_jib services/contract-service build',
    deps=[
        'services/contract-service/build.gradle.kts',
        'services/contract-service/gradle.properties',
        'services/contract-service/settings.gradle.kts',
        'services/contract-service/src/main',
    ],
    skips_local_docker=True,
)

custom_build(
    'data-management-service',
    './call tilt/scripts/tilt_call_jib.sh services/data-management-service build',
    command_bat='call tilt/scripts/tilt_call_jib services/data-management-service build',
    deps=[
        'services/data-management-service/build.gradle.kts',
        'services/data-management-service/gradle.properties',
        'services/data-management-service/settings.gradle.kts',
        'services/data-management-service/src/main',
    ],
    skips_local_docker=True,
)

docker_build(
    'keycloak-setup',
    'cluster/scripts/keycloak-setup',
)

postgres_rendered_chart = helm(
    './cluster/helm/postgres',
    name='eurodat',
    namespace=transaction_plane_namespace,
    set=['securePasswords=false'],
)

# Deploy resources
k8s_yaml(postgres_rendered_chart)

k8s_resource(
    workload='eurodat-postgresql',
    resource_deps=['postgres-config'],
    port_forwards=[port_forward(5434, 5432, name='eurodat-postgresql')],
    labels=['postgres'],
)

k8s_resource(
    new_name='postgres-config',
    objects=[
        'postgresql-certificate:certificate',
        'postgresql-network-policy:networkpolicy',
        'postgresql-credentials-secret:secret',
        'eurodat-postgresql-extended-configuration:configmap',
        'postgresql-init-config:configmap',
        'postgresql-pg-hba-config:configmap',
        'postgresql-preinit-config:configmap',
        'postgresql-sql-config:configmap',
        'eurodat-cluster-ingress-nginx-tcp:configmap',
        'eurodat-postgresql:serviceaccount',
        'eurodat-postgresql:networkpolicy',
        'eurodat-postgresql-read:networkpolicy',
        'postgresql-user-credentials-secret:secret',
    ],
    resource_deps=['cluster-config', 'eurodat-certificates'],
    labels=['postgres'],
)

custom_build(
    'eurodat-app',
    command='sh cluster/scripts/publish_frontend_image.sh frontend localhost:5002 $EXPECTED_TAG',
    command_bat='sh cluster/scripts/publish_frontend_image.sh frontend localhost:5002 %EXPECTED_TAG%',
    deps=[
        'frontend',
        'cluster/scripts/publish_frontend_image.sh',
    ],
    skips_local_docker=True,
)

eurodatAppCssFile = 'https://eurodat.org/fileadmin/templates/css/styles.css'
k8s_yaml(
    helm(
        './cluster/helm/eurodat-app',
        name='eurodat-app',
        set=[
            'fqdn={}'.format(cluster_fqdn),
            'eurodatApp.cssSource={}'.format(eurodatAppCssFile),
        ],
    )
)

k8s_resource(
    new_name='eurodat-app-config',
    objects=[
        'eurodat-app:serviceaccount',
        'eurodat-app-ingress:ingress',
        'eurodat-app-configmap:configmap',
    ],
    labels=['eurodat-app'],
    resource_deps=[
        'cluster-config',
        'ingress-nginx',
        'eurodat-certificates',
    ],
)

local_resource(
    name='extract-cypress-user-password',
    cmd='bash cluster/scripts/extract_cypress_user_password.sh -c kind-eurodat',
	resource_deps=[
	   'eurodat-app',
	   'keycloak-config-job',
	   'eurodat-contract-service'
	],
    labels=['eurodat-app'],
)

k8s_resource(
   workload='eurodat-app',
   resource_deps=[
   'cluster-config',
   'ingress-nginx',
   'eurodat-app-config',
   'keycloak-config-job',
   ],
   labels=['eurodat-app'],
)

for mock_app in [
    'mock-safeaml-app',
    'uc3-mock-app',
    'uc2-mock-app',
    'minabo-app',
    'message-app',
    'row-level-security-mock-app',
    'transaction-copy-app'
]:
    local_resource(
        name=mock_app,
        cmd='./tilt/scripts/build_mock_image.sh {} kind-eurodat'.format(mock_app),
        cmd_bat='wsl -e ./tilt/scripts/build_mock_image.sh {} kind-eurodat'.format(mock_app),
        labels=['mock-apps'],
        deps=[
            'cluster/scripts/{}'.format(mock_app)
        ],
    )

local_resource(
    name='app-service-uploader',
    cmd='docker build -t localhost:5002/app-service-uploader:latest services/app-service-uploader && docker push localhost:5002/app-service-uploader:latest',
    cmd_bat='wsl -e docker build -t localhost:5002/app-service-uploader:latest services/app-service-uploader && docker push localhost:5002/app-service-uploader:latest',
    labels=['app-service'],
)

local_resource(
    name='push_hello-world',
    cmd='docker pull docker.io/hello-world:latest && docker tag docker.io/hello-world:latest localhost:5002/hello-world:latest && docker push localhost:5002/hello-world:latest',
    cmd_bat='wsl -e docker pull docker.io/hello-world:latest && docker tag docker.io/hello-world:latest localhost:5002/hello-world:latest && docker push localhost:5002/hello-world:latest',
    labels=['mock-apps']
)

# Deploy cluster-wide resources with Helm
# Be sure to update the dependencies before running Tilt (use ./Makefile)

update_settings(k8s_upsert_timeout_secs=180)

# Render eurodat-cluster Helm chart

eurodat_cluster_rendered_template = helm(
    './cluster/helm/eurodat-cluster',
    'eurodat-cluster',
    namespace=base_namespace,
    values=[
        './cluster/helm/eurodat-cluster/values.yaml',
    ],
    set=[
        'prometheus.enabled=false',
        'global.useSelfSigned=true',
        'global.fqdn={}'.format(cluster_fqdn),
        'securePasswords=false',
        'grafana.persistence.size=1Gi',
        'tempo.persistence.size=1Gi',
        'grafana.persistence.size=1Gi',
        'global.certs.keycloak.keystore.password=pwd',
        'global.credentials.keycloak.admin.name=admin',
        'global.credentials.keycloak.admin.password=pwd',
        'global.credentials.keycloak.database.password=pwd',
        'keystore.password=pwd',
        'smtpServer.port={}'.format(cfg.get('SMTP_PORT','25')),
        'smtpServer.ip={}'.format(cfg.get('SMTP_SERVER_IP','0.0.0.0/0')),
        'grafana.password=grafanaPassword',
        'opentelemetry.token=opentelemetryToken',
        'prometheus.server.persistentVolume.size=1Gi',
        'loki.enabled={}'.format(str(loki_enabled).lower()),
        'loki.singleBinary.persistence.size=1Gi',
        'promtail.enabled={}'.format(str(loki_enabled).lower()),
    ],
)

# Deploy resources

# Render ingress nginx Helm chart
ingress_nginx_rendered_chart = helm(
    './cluster/helm/ingress-nginx',
    name='eurodat',
    namespace=base_namespace,
    values=[
      './tilt/values/tilt_ingress_nginx_values.yaml',
    ],
)
k8s_yaml(ingress_nginx_rendered_chart)

k8s_yaml(eurodat_cluster_rendered_template)
# base-chart
k8s_resource(
    new_name='cluster-config',
    objects=[
        'cert-challenge-network-policy:networkpolicy',
        'cert-manager:namespace',
        'default-network-policy:networkpolicy:{}'.format(base_namespace),
        'reflector-network-policy:networkpolicy',
        'secret-generator-network-policy:networkpolicy',
        '{}:namespace'.format(base_namespace),
        '{}:namespace'.format(control_plane_namespace),
        '{}:namespace'.format(data_plane_namespace),
        '{}:namespace'.format(transaction_plane_namespace),
        '{}:namespace'.format(frontend_namespace),
    ],
    labels=['config'],
)

helm_resource(
    'cert-manager',
    'jetstack/cert-manager',
    namespace='cert-manager',
    flags=[
        '--version', CERT_MANAGER_HELM_CHART_VERSION,
        '--set', 'webhook.timeoutSeconds=30',
        '--set', 'installCRDs=true',
        '--set', 'featureGates=AdditionalCertificateOutputFormats=true',
        '--set', 'image.repository={}/cert-manager-controller'.format(CERT_MANAGER_IMAGE_REPOSITORY),
        '--set', 'image.digest={}'.format(CERT_MANAGER_CONTROLLER_DIGEST),
        '--set', 'acmesolver.image.repository={}/cert-manager-acmesolver'.format(CERT_MANAGER_IMAGE_REPOSITORY),
        '--set', 'acmesolver.image.digest={}'.format(CERT_MANAGER_ACMESOLVER_DIGEST),
        '--set', 'cainjector.image.repository={}/cert-manager-cainjector'.format(CERT_MANAGER_IMAGE_REPOSITORY),
        '--set', 'cainjector.image.digest={}'.format(CERT_MANAGER_CAINJECTOR_DIGEST),
        '--set', 'webhook.image.repository={}/cert-manager-webhook'.format(CERT_MANAGER_IMAGE_REPOSITORY),
        '--set', 'webhook.image.digest={}'.format(CERT_MANAGER_WEBHOOK_DIGEST),
        '--set', 'startupapicheck.timeout=5m',
    ],
    labels=['config'],
    resource_deps=['cluster-config'],
)

helm_resource(
    'trust-manager',
    'jetstack/trust-manager',
    namespace='cert-manager',
    flags=[
        '--set',
        'app.webhook.timeoutSeconds=30',
        '--version',
        '0.12.0',
    ],
    labels=['config'],
    resource_deps=['cluster-config', 'cert-manager'],
)

k8s_resource(
    workload='eurodat-cluster-reloader',
    objects=[
        'eurodat-cluster-reloader:serviceaccount',
        'eurodat-cluster-reloader-role:clusterrole',
        'eurodat-cluster-reloader-role-binding:clusterrolebinding',
        'reloader-network-policy:networkpolicy',
    ],
    labels=['config'],
    resource_deps=['cluster-config'],
)

# cert-manager-chart
k8s_resource(
    new_name='cert-management',
    objects=[
        'eurodat-certificate:certificate',
        'eurodat-cluster-keystore-password-secret:secret',
        'eurodat-cluster-issuer',
        'keycloak-certificate:certificate',
        'local-issuer',
        'self-signed-issuer',
        'self-signed-root-certificate',
        'local-self-signed-ca-secret:secret',
        'local-ca-bundle:bundle',
    ],
    resource_deps=['trust-manager', 'cert-manager', 'cluster-config'],
    labels=['config'],
)

k8s_resource(
    new_name='eurodat-cluster-reflector',
    workload='eurodat-cluster-reflector',
    objects=[
        'eurodat-cluster-reflector:serviceaccount',
        'eurodat-cluster-reflector:clusterrole',
        'eurodat-cluster-reflector:clusterrolebinding',
    ],
    resource_deps=['cluster-config'],
    labels=['config'],
)

k8s_resource(
    new_name='restart-reflector-config',
    objects=[
        'restart-reflector-service-account:serviceaccount',
        'restart-reflector-role:role',
        'restart-reflector-role-binding:rolebinding',
    ],
    resource_deps=['cluster-config'],
    labels=['config']
)

k8s_resource(
    workload='restart-reflector',
    resource_deps=['restart-reflector-config'],
    labels=['config'],
)

# keycloak chart
k8s_resource(
    new_name='keycloak',
    workload='eurodat-keycloak',
    objects=[
        'eurodat-keycloak:serviceaccount',
        'keycloak-custom-service-account:serviceaccount',
        'keycloak-network-policy:networkpolicy',
        'keycloak-credentials-secret:secret',
        'keycloak-keystore-password-secret:secret',
        'eurodat-cluster-keycloak-ingress:ingress',
    ],
    extra_pod_selectors={'app.kubernetes.io/name': 'keycloak'},
    resource_deps=['eurodat-postgresql', 'cert-management', 'ingress-nginx', 'keycloak-config'],
    links=['https://{}/auth'.format(cluster_fqdn)],
    labels=['keycloak'],
)

k8s_resource(
    new_name='keycloak-config',
    objects=[
        'transaction-service-init-secret:secret',
        'credential-service-init-secret:secret',
        'user-management-secret:secret',
        'frontend-init-secret:secret',
        'vault-authentication-secret:secret',
        'keycloak-config-job-network-policy',
        'keycloak-config-volume:persistentvolumeclaim',
        'transaction-service-scheduler-credentials-secret:secret',
    ],
    resource_deps=['cluster-config'],
    labels=['keycloak'],
)
k8s_resource(
    new_name='ingress-nginx',
    workload='eurodat-cluster-ingress-nginx-controller',
    extra_pod_selectors={
        'app.kubernetes.io/name': 'ingress-nginx',
    },
    resource_deps=['ingress-nginx-config'],
    labels=['ingress'],
)

k8s_resource(
    new_name='ingress-nginx-config',
    objects=[
            'eurodat-cluster-ingress-nginx-root-issuer:issuer',
            'eurodat-cluster-ingress-nginx-self-signed-issuer:issuer',
            'eurodat-cluster-ingress-nginx-root-cert:certificate',
            'eurodat-cluster-ingress-nginx-admission:certificate',
            'eurodat-cluster-ingress-nginx-admission:validatingwebhookconfiguration',
            'eurodat-cluster-ingress-nginx-controller:configmap',
            'eurodat-cluster-ingress-nginx:clusterrole',
            'eurodat-cluster-ingress-nginx:clusterrolebinding',
            'eurodat-cluster-ingress-nginx:role',
            'eurodat-cluster-ingress-nginx:rolebinding',
            'eurodat-cluster-ingress-nginx:serviceaccount',
            'cluster-nginx:ingressclass',
            'ingress-network-policy:networkpolicy',
        ],
    resource_deps=['cluster-config', 'cert-management',],
    labels=['ingress'],
)

kube_endpoint = local(
    './tilt/scripts/get_api_server.sh', command_bat='sh tilt/scripts/get_api_server.sh',
)

# Render transaction-service helm chart

transaction_service_rendered_chart = helm(
    './cluster/helm/transaction-service',
    name='eurodat',
    namespace=control_plane_namespace,
    set=[
        'global.debug=true',
        'global.useSelfSigned=true',
        'global.securePasswords=false',
        'global.postgresql.auth.password=pwd',
        'global.imageCredentials.registry=localhost:5002',
        'global.imageCredentials.registry=username',
        'fqdn={}'.format(cluster_fqdn),
        'iam.fqdn={}'.format(cluster_fqdn),
        'certificate.secret.password=pwd',
        'postgresql.secret.vault.password=vault',
        'postgresql.secret.transaction-service.password=transactionService'
    ],
)
k8s_yaml(transaction_service_rendered_chart)

# Render credential-service Helm chart

credential_service_rendered_chart = helm(
    './cluster/helm/credential-service',
    name='eurodat',
    namespace=control_plane_namespace,
    set=[
        'fqdn={}'.format(cluster_fqdn),
        'iam.fqdn={}'.format(cluster_fqdn),
        'global.debug=true',
        'global.useSelfSigned=true',
        'global.securePasswords=false',
        'global.imageCredentials.registry=localhost:5002',
        'global.imageCredentials.registry=username',
        'certificate.secret.password=pwd',
    ],
)
k8s_yaml(credential_service_rendered_chart)

# Render database-service Helm chart

database_service_rendered_chart = helm(
    './cluster/helm/database-service',
    name='eurodat',
    namespace=control_plane_namespace,
    set=[
        'fqdn={}'.format(cluster_fqdn),
        'iam.fqdn={}'.format(cluster_fqdn),
        'global.debug=true',
        'global.useSelfSigned=true',
        'global.securePasswords=false',
        'global.imageCredentials.registry=localhost:5002',
        'global.imageCredentials.registry=username',
        'certificate.secret.password=pwd',
    ],
)
k8s_yaml(database_service_rendered_chart)


# Render app-service Helm chart

app_service_rendered_chart = helm(
    './cluster/helm/app-service',
    name='eurodat',
    namespace=control_plane_namespace,
    set=[
        'fqdn={}'.format(cluster_fqdn),
        'iam.fqdn={}'.format(cluster_fqdn),
        'global.debug=true',
        'global.useSelfSigned=true',
        'global.securePasswords=false',
        'global.imageCredentials.registry=localhost:5002',
        'global.imageCredentials.registry=username',
        'certificate.secret.password=pwd',
        'cluster.internalApiEndpoint={}'.format(str(kube_endpoint).strip()),
    ],
)
k8s_yaml(app_service_rendered_chart)

# Render data-management-service Helm chart

data_management_service_rendered_chart = helm(
    './cluster/helm/data-management-service',
    name='eurodat',
    namespace=control_plane_namespace,
    set=[
        'fqdn={}'.format(cluster_fqdn),
        'iam.fqdn={}'.format(cluster_fqdn),
        'global.debug=true',
        'global.useSelfSigned=true',
        'global.securePasswords=false',
        'global.imageCredentials.registry=localhost:5002',
        'global.imageCredentials.registry=username',
        'certificate.secret.password=pwd',
    ],
)
k8s_yaml(data_management_service_rendered_chart)
# Render eurodat Helm chart
eurodat_rendered_chart = helm(
    './cluster/helm/eurodat',
    name='eurodat',
    namespace=control_plane_namespace,
    set=[
        'fqdn={}'.format(cluster_fqdn),
        'iam.fqdn={}'.format(cluster_fqdn),
        'frontendDevUri={}'.format('localhost:4000'),
        'iam.smtpMailFrom=info@eurodat.org',
        'iam.smtpHost={}'.format(cfg.get('SMTP_HOST','localhost')),
        'iam.smtpPort={}'.format(cfg.get('SMTP_PORT','25')),
        'deploySmtpExternalSecret=false',
        'localSmtpApiKeys.smtpApiKey={}'.format(cfg.get('SMTP_API_KEY','default dummy value - set value in tilt_config.json')),
        'localSmtpApiKeys.smtpApiSecretKey={}'.format(cfg.get('SMTP_API_SECRET_KEY','default dummy value - set value in tilt_config.json')),
        'deployPaperlessExternalSecret=false',
        'localPaperlessApiKey={}'.format(cfg.get('PAPERLESS_API_KEY','default dummy value - set value in tilt_config.json')),
        'global.debug=true',
        'global.useSelfSigned=true',
        'global.securePasswords=false',
        'global.publicImageRepository=true',
        'global.imageCredentials.registry=localhost:5002',
        'global.imageCredentials.registry=username',
        'cluster.internalApiEndpoint={}'.format(str(kube_endpoint).strip()),
    ],
)
k8s_yaml(eurodat_rendered_chart)
k8s_resource(
    new_name='eurodat-certificates',
    objects=[
        'app-service-certificate:certificate',
        'app-service-keystore-password-secret:secret',
        'transaction-service-certificate:certificate',
        'transaction-service-keystore-password-secret:secret',
        'credential-service-certificate:certificate',
        'credential-service-keystore-password-secret:secret',
        'contract-service-certificate:certificate',
        'contract-service-keystore-password-secret:secret',
        'database-service-certificate:certificate',
        'database-service-keystore-password-secret:secret',
        'data-management-service-certificate:certificate',
        'data-management-service-keystore-password-secret:secret',
    ],
    resource_deps=['cert-management'],
    labels=['config'],
)

####################################################
# Required to extract the Keycloak-Config-Job name #
####################################################

# Get the templated helm chart as dictionary
parsed_yaml_stream = decode_yaml_stream(eurodat_rendered_chart)

def getKeycloakConfigJobName(yaml_dict):
    expected_kind = 'Job'
    label_key = 'app.kubernetes.io/component'
    label_value = 'keycloak-config-job'

    # Check if kind is 'Job' and labels match
    if yaml_dict.get('kind') == expected_kind:
        metadata = yaml_dict.get('metadata', {})
        labels = metadata.get('labels', {})
        if labels.get(label_key) == label_value:
            name = metadata.get('name')
            if name:
                return name

# Iterate over each document in the parsed YAML stream
for doc in parsed_yaml_stream:
    checkKeycloakConfigJobName = getKeycloakConfigJobName(doc)
    if checkKeycloakConfigJobName:
        keycloakConfigJobName = checkKeycloakConfigJobName
        break

k8s_resource(
    new_name='keycloak-config-job',
    workload=keycloakConfigJobName,
    resource_deps=['keycloak', 'cert-management'],
    labels=['keycloak'],
)

# Render contract-service Helm chart

contract_service_rendered_chart = helm(
    './cluster/helm/contract-service',
    name='eurodat',
    namespace=control_plane_namespace,
    set=[
        'fqdn={}'.format(cluster_fqdn),
        'iam.fqdn={}'.format(cluster_fqdn),
        'deployPaperlessExternalSecret=false',
        'global.debug=true',
        'global.useSelfSigned=true',
        'global.securePasswords=false',
        'global.imageCredentials.registry=localhost:5002',
        'global.imageCredentials.registry=username',
        'certificate.secret.password=pwd',
        'additionalCorsOrigin={}'.format(ADDITIONAL_CORS_ORIGIN),
    ],
)
k8s_yaml(contract_service_rendered_chart)

# Deploy resources

local_resource(
    'kubernetes-secret-generator-build',
    cmd='./cluster/scripts/createKubernetesSecretManagerDockerImage.sh',
    cmd_bat='wsl -e ./cluster/scripts/createKubernetesSecretManagerDockerImage.sh',
    labels=['kubernetes-secret-generator'],
)

helm_resource(
    'eurodat-cluster-kubernetes-secret-generator',
    'mittwald/kubernetes-secret-generator',
    namespace='base',
    flags=[
            '--set',
            'installCRDs=true',
            '--set',
            'image.registry=localhost:5002',
            '--set',
            'image.repository=kubernetes-secret-generator',
            '--set',
            'image.tag=latest',
    ],
    resource_deps=['kubernetes-secret-generator-build', 'cluster-config'],
    labels=['kubernetes-secret-generator'],
)

k8s_resource(
    new_name='eurodat-network-policies',
    objects=[
        'app-service-network-policy:networkpolicy',
        'upload-network-policy:networkpolicy',
        'transaction-service-network-policy:networkpolicy',
        'credential-service-network-policy:networkpolicy',
        'contract-service-network-policy:networkpolicy',
        'database-service-network-policy:networkpolicy',
        'data-management-service-network-policy:networkpolicy',
        'default-network-policy:networkpolicy:{}'.format(data_plane_namespace),
        'default-network-policy:networkpolicy:{}'.format(transaction_plane_namespace),
        'default-network-policy:networkpolicy:{}'.format(control_plane_namespace),
    ],
    resource_deps=['cluster-config', 'ingress-nginx'],
    labels=['config'],
)

k8s_resource(
   workload='eurodat-credential-service',
   resource_deps=[
        'eurodat-certificates',
        'eurodat-postgresql',
        'keycloak-config-job',
        'credential-service-config',
        'credential-service-ingress',
   ],
   labels=['credential-service'],
   port_forwards=[
           port_forward(5006, 5005, name='debug-credential-service'),
       ]
)

k8s_resource(
   workload='eurodat-app-service',
   resource_deps=[
       'eurodat-certificates',
       'eurodat-postgresql',
       'app-service-ingress',
       'keycloak',
   ],
   labels=['app-service'],
   port_forwards=[
           port_forward(5010, 5005, name='debug-app-service'),
           port_forward(12344, 8443, 'e2e-test'),
       ]
)
k8s_resource(
    new_name='app-service-ingress',
    objects=[
        'eurodat-app-service-ingress:ingress:{}'.format(control_plane_namespace),
    ],
    resource_deps=['cluster-config', 'ingress-nginx'],
    labels=['app-service'],
)

k8s_resource(
   workload='eurodat-contract-service',
   resource_deps=[
       'eurodat-certificates',
       'eurodat-postgresql',
       'keycloak',
       'contract-service-ingress',
   ],
   labels=['contract-service'],
   port_forwards=[
           port_forward(5011, 5005, name='debug-contract-service'),
       ]
)

k8s_resource(
   workload='eurodat-database-service',
   resource_deps=[
       'eurodat-certificates',
       'eurodat-postgresql',
       'keycloak',
       'database-service-config',
       'database-service-ingress',
   ],
   labels=['database-service'],
   port_forwards=[
           port_forward(5004, 5005, name='debug-database-service'),
       ]
)

k8s_resource(
    workload='eurodat-transaction-service',
    resource_deps=[
        'eurodat-certificates',
        'eurodat-postgresql',
        'keycloak-config-job',
        'transaction-service-config',
        'transaction-service-ingress',
        'transaction-service-apps-redirect-ingress',
    ],
    labels=['transaction-service'],
    port_forwards=[
        port_forward(5005, 5005, name='debug-transaction-service'),
    ],
)

k8s_resource(
    new_name='transaction-service-config',
    objects=[
        'transaction-service-configmap:configmap',
        'transaction-service-service-account:serviceaccount',
    ],
    resource_deps=['cluster-config', 'cert-management'],
    labels=['transaction-service'],
)

k8s_resource(
   workload='eurodat-data-management-service',
   resource_deps=[
       'eurodat-certificates',
       'eurodat-postgresql',
       'keycloak',
       'data-management-service-config',
       'data-management-service-ingress',
   ],
   labels=['data-management-service'],
   port_forwards=[
           port_forward(5012, 5005, name='debug-data-management-service'),
       ]
)

k8s_resource(
    new_name='app-service-config',
    objects=[
        'app-service-configmap:configmap',
        'app-service-service-account:serviceaccount',
        'upload-images-sa:serviceaccount',
        'app-service-role:role',
        'app-service-sa-role-binding:rolebinding',
        'image-job-role:role',
        'image-job-sa-role-binding:rolebinding',
    ],
    resource_deps=['cluster-config', 'cert-management'],
    labels=['app-service'],
)

k8s_resource(
    new_name='app-service-argo',
    objects=[
        'eurodat-app-service-argo-workflows-role:role',
        'eurodat-app-service-argo-workflows-rolebinding:rolebinding',
    ],
    resource_deps=['cluster-config'],
    labels=['app-service'],
)

k8s_resource(
    new_name='credential-service-config',
    objects=[
        'credential-service-configmap:configmap',
        'credential-service-service-account:serviceaccount',
    ],
    resource_deps=['cluster-config', 'cert-management'],
    labels=['credential-service'],
)
k8s_resource(
    new_name='contract-service-config',
    objects=[
        'contract-service-service-account:serviceaccount',
        'paperless-api-key:secret',
        'paperless-callback-secret:secret',
        'contract-service-configmap:configmap',
    ],
    resource_deps=['cluster-config', 'cert-management'],
    labels=['contract-service'],
)

k8s_resource(
    new_name='database-service-config',
    objects=[
        'database-service-configmap:configmap',
        'database-service-service-account:serviceaccount',
    ],
    resource_deps=['cluster-config', 'cert-management'],
    labels=['database-service'],
)

k8s_resource(
    new_name='data-management-service-config',
    objects=[
        'data-management-service-configmap:configmap',
        'data-management-service-service-account:serviceaccount',
    ],
    resource_deps=['cluster-config', 'cert-management'],
    labels=['data-management-service'],
)

k8s_resource(
    new_name='transaction-service-argo',
    objects=[
        'eurodat-transaction-service-argo-workflows-role:role',
        'eurodat-transaction-service-argo-workflows-rolebinding:rolebinding',
    ],
    resource_deps=['cluster-config'],
    labels=['transaction-service'],
)

k8s_resource(
    new_name='transaction-service-ingress',
    objects=[
        'eurodat-transaction-service-ingress:ingress:{}'.format(control_plane_namespace),
    ],
    resource_deps=['cluster-config', 'ingress-nginx'],
    labels=['transaction-service'],
)

k8s_resource(
    new_name='transaction-service-apps-redirect-ingress',
    objects=[
        'eurodat-transaction-service-apps-redirect-ingress:ingress:{}'.format(control_plane_namespace),
    ],
    resource_deps=['cluster-config', 'ingress-nginx'],
    labels=['transaction-service'],
)

k8s_resource(
    new_name='credential-service-ingress',
    objects=[
        'eurodat-credential-service-ingress:ingress:{}'.format(control_plane_namespace),
    ],
    resource_deps=['cluster-config', 'ingress-nginx'],
    labels=['credential-service'],
)

k8s_resource(
    new_name='database-service-ingress',
    objects=[
        'eurodat-database-service-ingress:ingress:{}'.format(control_plane_namespace),
    ],
    resource_deps=['cluster-config', 'ingress-nginx'],
    labels=['database-service'],
)

k8s_resource(
    new_name='contract-service-ingress',
    objects=[
        'eurodat-contract-service-ingress:ingress:{}'.format(control_plane_namespace),
    ],
    resource_deps=['cluster-config', 'ingress-nginx'],
    labels=['contract-service'],
)

k8s_resource(
    new_name='data-management-service-ingress',
    objects=[
        'eurodat-data-management-service-ingress:ingress:{}'.format(control_plane_namespace),
    ],
    resource_deps=['cluster-config', 'ingress-nginx'],
    labels=['data-management-service'],
)

k8s_resource(
    new_name='eurodat-data-plane',
    objects=[
        'data-plane-service-account:serviceaccount',
        'data-plane-service-account.service-account-token:secret',
    ],
    resource_deps=['eurodat-certificates'],
    labels=['data-plane'],
)

k8s_resource(
    new_name='smtp-keys',
    objects=[
      'smtp-api-key:secret',
      'smtp-api-secret-key:secret',
    ],
    labels=['config'],
    resource_deps=['cluster-config'],
)
workflow_rendered_chart = helm(
    './cluster/helm/argo-workflows',
    'eurodat',
    namespace=transaction_plane_namespace,
    set=[
        'argo-workflows.crds.install=true',
        'securePasswords=false',
        'postgresql.auth.postgresPassword=pwd',
    ],
)

k8s_yaml(workflow_rendered_chart)

k8s_resource(
    new_name='argo-workflows-config',
    objects=[
        'clusterworkflowtemplates.argoproj.io:customresourcedefinition',
        'cronworkflows.argoproj.io:customresourcedefinition',
        'workflowartifactgctasks.argoproj.io:customresourcedefinition',
        'workfloweventbindings.argoproj.io:customresourcedefinition',
        'workflows.argoproj.io:customresourcedefinition',
        'workflowtaskresults.argoproj.io:customresourcedefinition',
        'workflowtasksets.argoproj.io:customresourcedefinition',
        'workflowtemplates.argoproj.io:customresourcedefinition',
        'eurodat-argo-workflows-workflow-controller:serviceaccount',
        'eurodat-argo-workflows-server:serviceaccount',
        'eurodat-argo-workflows-workflow:role:{}'.format(data_plane_namespace),
        'eurodat-argo-workflows-workflow:role:{}'.format(transaction_plane_namespace),
        'eurodat-argo-workflows-view:clusterrole',
        'eurodat-argo-workflows-edit:clusterrole',
        'eurodat-argo-workflows-admin:clusterrole',
        'eurodat-argo-workflows-workflow-controller:clusterrole',
        'eurodat-argo-workflows-workflow-controller-cluster-template:clusterrole',
        'eurodat-argo-workflows-server:clusterrole',
        'eurodat-argo-workflows-server-cluster-template:clusterrole',
        'eurodat-argo-workflows-workflow:rolebinding:{}'.format(data_plane_namespace),
        'eurodat-argo-workflows-workflow:rolebinding:{}'.format(transaction_plane_namespace),
        'eurodat-argo-workflows-workflow-controller:clusterrolebinding',
        'eurodat-argo-workflows-workflow-controller-cluster-template:clusterrolebinding',
        'eurodat-argo-workflows-server:clusterrolebinding',
        'eurodat-argo-workflows-server-cluster-template:clusterrolebinding',
        'eurodat-argo-workflows-workflow-controller-configmap:configmap',
        'eurodat-argo-workflows-server-network-policy:networkpolicy',
        'eurodat-argo-workflows-controller-network-policy:networkpolicy',
    ],
    resource_deps=['cluster-config'],
    labels=['argo-workflows'],
)

k8s_resource(
    workload='eurodat-argo-workflows-server',
    new_name='argo-workflows-server',
    labels=['argo-workflows'],
    objects=[
        'argo-workflows-resource-role:role',
        'argo-workflows-resource-rolebinding:rolebinding',
        'argo-workflows-certificate:certificate'
    ],
    resource_deps=['argo-workflows-config', 'eurodat-postgresql'],
    port_forwards=[port_forward(2746, 2746, name='argo-ui')]
)

k8s_resource(
    workload='eurodat-argo-workflows-workflow-controller',
    new_name='argo-workflows-controller',
    resource_deps=['argo-workflows-config', 'eurodat-postgresql'],
    labels=['argo-workflows'],
)

k8s_resource(
    new_name='argo-workflows-example',
    objects=['example-workflow:workflowtemplate'],
    labels=['argo-workflows'],
    resource_deps=['argo-workflows-config'],
)

transaction_vault_rendered_chart = helm(
    './cluster/helm/transaction-vault',
    'eurodat-transaction',
    namespace=transaction_plane_namespace,
    set=[
        'vault.server.extraEnvironmentVars.fqdn={}'.format(cluster_fqdn),
        'vault.nameOverride=vault'
    ],
)

k8s_yaml(transaction_vault_rendered_chart)

k8s_resource(
    workload='eurodat-transaction-vault',
    new_name='eurodat-transaction-vault',
    resource_deps=['eurodat-transaction-vault-config'],
    labels=['transaction-vault'],
    port_forwards=[port_forward(8200, 8200, name='eurodat-transaction-vault')],
)

k8s_resource(
    new_name='eurodat-transaction-vault-config',
    objects=[
        'eurodat-transaction-vault-config:configmap',
        'eurodat-transaction-vault:serviceaccount',
        'vault-certificate:certificate',
        'vault-bootstrap-scripts:configmap',
        'eurodat-transaction-vault-server-binding:clusterrolebinding',
        'vault-network-policy:networkpolicy:{}'.format(transaction_plane_namespace),
        'vault-to-postgres-network-policy:networkpolicy',
    ],
    resource_deps=['cluster-config', 'eurodat-transaction-service'],
    labels=['transaction-vault'],
)

k8s_resource(
    new_name='grafana-config',
    objects=[
        'grafana:serviceaccount',
        'grafana-envvars:configmap',
        'grafana-configmap:configmap',
        'grafana-credentials-secret:secret',
        'grafana-certificate:certificate',
        'grafana-datasources-secret:secret',
        'grafana-network-policy:networkpolicy'

    ],
    labels=['grafana'],
    resource_deps=['cluster-config', 'ingress-nginx',]
)

k8s_resource(
    workload='grafana',
    objects=[
        'grafana:persistentvolumeclaim',
        'grafana:poddisruptionbudget',
        'grafana:networkpolicy',
    ],
    labels=['grafana'],
    resource_deps=['tempo', 'grafana-config',],
    port_forwards=[port_forward(3000, 3000, name='grafana-ui')]
)

k8s_resource(
    workload='opentelemetry-collector',
    resource_deps=['opentelemetry-collector-config', 'tempo',],
    labels=['opentelemetry'],
)

k8s_resource(
    new_name='opentelemetry-collector-config',
    objects=[
        'opentelemetry-collector:serviceaccount',
        'opentelemetry-collector:configmap',
        'opentelemetry-token-secret:secret',
        'opentelemetry-network-policy:networkpolicy',
        'opentelemetry-certificate:certificate'
    ],
    resource_deps=['cluster-config', 'ingress-nginx',],
    labels=['opentelemetry'],
)

k8s_resource(
    workload='tempo',
    labels=['tempo'],
    resource_deps=['tempo-config',],
)

k8s_resource(
    new_name='tempo-config',
    objects=[
        'tempo:serviceaccount',
        'tempo:configmap',
        'tempo-network-policy:networkpolicy',
        'tempo-certificate:certificate',
    ],
    resource_deps=['cluster-config', 'ingress-nginx', ],
    labels=['tempo',],
)


if loki_enabled:
    k8s_resource(
        new_name='loki-config',
        objects=[
            'loki:serviceaccount',
            'loki:configmap',
            'loki-runtime:configmap',
            'loki-network-policy:networkpolicy',
            'loki-certificate:certificate',
        ],
        resource_deps=['cert-management'],
        labels=['logs'],
    )

    k8s_resource(
        workload='loki',
        resource_deps=['loki-config'],
        labels=['logs'],
    )

    k8s_resource(
        new_name='promtail-config',
        objects=[
            'promtail:serviceaccount',
            'promtail:clusterrole',
            'promtail:clusterrolebinding',
            'promtail:secret',
            'promtail-network-policy:networkpolicy',
            'promtail-certificate:certificate',
        ],
        resource_deps=['cert-management'],
        labels=['logs'],
    )

    k8s_resource(
        workload='promtail',
        resource_deps=['promtail-config'],
        labels=['logs'],
    )

    k8s_resource(
                workload='loki-chunks-cache',
                objects=[
                    'loki-memcached-chunks-cache:poddisruptionbudget'
                ],
                resource_deps=['loki'],
                labels=['logs'],
            )

    k8s_resource(
                workload='loki-results-cache',
                resource_deps=['loki'],
                objects=[
                    'loki-memcached-results-cache:poddisruptionbudget'
                ],
                labels=['logs'],
            )
    k8s_resource(
                    workload='eurodat-cluster-memcached',
                    objects=[
                        'eurodat-cluster-memcached:serviceaccount',
                        'eurodat-cluster-memcached:poddisruptionbudget',
                        'eurodat-cluster-memcachednetworkpolicy',
                    ],
                    labels=['logs'],
                )
