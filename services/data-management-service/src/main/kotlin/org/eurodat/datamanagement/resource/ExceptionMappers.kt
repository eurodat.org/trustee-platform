package org.eurodat.datamanagement.resource

import io.smallrye.mutiny.Uni
import jakarta.ws.rs.ClientErrorException
import org.jboss.resteasy.reactive.RestResponse
import org.jboss.resteasy.reactive.server.ServerExceptionMapper

class ExceptionMappers {
    @ServerExceptionMapper
    fun mapClientError(e: ClientErrorException): Uni<RestResponse<ClientError>> =
        Uni.createFrom().item(RestResponse.status(e.response.statusInfo, ClientError(e.localizedMessage)))
}

data class ClientError(
    val error: String,
)
