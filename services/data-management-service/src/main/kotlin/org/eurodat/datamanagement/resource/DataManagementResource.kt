package org.eurodat.datamanagement.resource

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.vertx.http.security.AuthorizationPolicy
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.Produces
import jakarta.ws.rs.QueryParam
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eurodat.client.datamanagement.model.DataRequest
import org.eurodat.client.datamanagement.model.InsertResult
import org.eurodat.client.datamanagement.model.PersistentDataRequest
import org.eurodat.datamanagement.service.DataManagementService
import org.jboss.resteasy.reactive.RestResponse

/**
 * The DataManagementResource provides REST endpoints for data retrieval and insertion associated with participants.
 * Data can be inserted into the specified table for a transaction. The data can be retrieved from a specified table within a transaction.
 */
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@AuthorizationPolicy(name = "participant-claim")
class DataManagementResource(
    private val dataManagementService: DataManagementService,
    @ConfigProperty(name = "data-record-limit") private val dataRecordLimit: Int,
) {
    private val objectMapper = ObjectMapper()

    /**
     * Inserts data into the specified table for a transaction.
     *
     * @param dataRequest The request payload containing:
     *   - `data`: List of JSON strings to be inserted.
     *   - `table`: The target table where the data will be inserted.
     *   - `transactionId`: The ID of the transaction associated with the data.
     * @return A Uni containing a RestResponse with a success message on successful insertion.
     */
    @POST
    @Path("/{participantId}/data")
    @Produces(MediaType.APPLICATION_JSON)
    fun insertData(
        @PathParam("participantId") participantId: String,
        dataRequest: DataRequest,
    ): Uni<RestResponse<InsertResult>> =
        validateDataRequests(dataRequest.data).flatMap {
            dataManagementService.insertDataTransaction(
                it,
                dataRequest.table,
                dataRequest.transactionId,
            )
        }.map { RestResponse.status(Response.Status.CREATED, InsertResult("Data inserted successfully")) }

    /**
     * Retrieves data from the specified table for a transaction.
     *
     * @param table The name of the table to query.
     * @param transactionId The ID of the transaction associated with the data.
     * @param correlationId (Optional) Filters results by correlation ID if provided.
     * @return A Uni containing a list of JSON nodes representing the retrieved data.
     */
    @GET
    @Path("/{participantId}/data")
    @Produces(MediaType.APPLICATION_JSON)
    fun getData(
        @PathParam("participantId") participantId: String,
        @QueryParam("table") table: String,
        @QueryParam("transactionId") transactionId: String,
        @QueryParam("correlationId") correlationId: Long?,
    ): Uni<List<String>> =
        dataManagementService.getDataTransaction(table, transactionId, correlationId)
            .map { jsonNodes ->
                jsonNodes.map { jsonNode -> objectMapper.writeValueAsString(jsonNode) }
            }

    @POST
    @Path("/{participantId}/persistentdata")
    @Produces(MediaType.APPLICATION_JSON)
    fun insertPersistentData(
        @PathParam("participantId") participantId: String,
        persistentDataRequest: PersistentDataRequest,
    ): Uni<RestResponse<InsertResult>> =
        validateDataRequests(persistentDataRequest.data).flatMap {
            dataManagementService.insertDataPersistent(
                it,
                persistentDataRequest.table,
                persistentDataRequest.appId,
            ).map { RestResponse.status(Response.Status.CREATED, InsertResult("Data inserted successfully")) }
        }

    @GET
    @Path("/{participantId}/persistentdata")
    @Produces(MediaType.APPLICATION_JSON)
    fun getPersistentData(
        @PathParam("participantId") participantId: String,
        @QueryParam("table") table: String,
        @QueryParam("appId") appId: String,
    ): Uni<List<String>> =
        dataManagementService.getDataPersistent(table, appId)
            .map { jsonNodes ->
                jsonNodes.map { jsonNode -> objectMapper.writeValueAsString(jsonNode) }
            }

    private fun validateDataRequests(dataRequests: List<String>): Uni<List<JsonNode>> {
        val listJsonData: List<JsonNode> =
            try {
                dataRequests.map { dataString ->
                    objectMapper.readTree(dataString)
                }
            } catch (e: JsonProcessingException) {
                return Uni.createFrom().failure(BadRequestException("Invalid JSON data", e))
            }

        if (listJsonData.size > dataRecordLimit) {
            return Uni.createFrom().failure(BadRequestException("Too many data records"))
        }

        return Uni.createFrom().item(listJsonData)
    }
}
