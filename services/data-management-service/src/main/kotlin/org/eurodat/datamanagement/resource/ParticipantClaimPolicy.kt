package org.eurodat.datamanagement.resource

import io.quarkus.security.identity.SecurityIdentity
import io.quarkus.vertx.http.runtime.security.HttpSecurityPolicy
import io.quarkus.vertx.http.runtime.security.HttpSecurityPolicy.AuthorizationRequestContext
import io.quarkus.vertx.http.runtime.security.HttpSecurityPolicy.CheckResult
import io.smallrye.mutiny.Uni
import io.vertx.ext.web.RoutingContext
import jakarta.enterprise.context.ApplicationScoped
import org.eclipse.microprofile.jwt.JsonWebToken

@ApplicationScoped
class ParticipantClaimPolicy : HttpSecurityPolicy {
    override fun checkPermission(
        event: RoutingContext,
        identity: Uni<SecurityIdentity>,
        requestContext: AuthorizationRequestContext,
    ): Uni<CheckResult> =
        identity.map { securityIdentity ->
            if (securityIdentity.isAnonymous) {
                return@map CheckResult.DENY
            }

            val tokenParticipant = (securityIdentity.principal as JsonWebToken).getClaim<String>("participant")
            val participantId = extractParticipantIdFromPath(event.request().path())

            if (tokenParticipant != null && tokenParticipant.equals(participantId, ignoreCase = true)) {
                CheckResult.PERMIT
            } else {
                CheckResult.DENY
            }
        }

    override fun name(): String = "participant-claim"

    private fun extractParticipantIdFromPath(path: String): String {
        val parts = path.split("/")
        return if (parts.size > 1 && (parts.last() == "data" || parts.last() == "persistentdata")) parts[parts.size - 2] else ""
    }
}
