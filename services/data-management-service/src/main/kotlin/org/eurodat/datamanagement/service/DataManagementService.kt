package org.eurodat.datamanagement.service

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.smallrye.mutiny.Uni
import io.vertx.mutiny.pgclient.PgConnection
import io.vertx.mutiny.sqlclient.Tuple
import io.vertx.pgclient.PgConnectOptions
import io.vertx.pgclient.PgException
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.WebApplicationException
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.credential.Credentials
import org.eurodat.client.credential.DatabaseCredentialClient

@ApplicationScoped
class DataManagementService {
    @Inject
    @RestClient
    private lateinit var credentialClient: DatabaseCredentialClient

    @ConfigProperty(name = "eurodat.database.host")
    private lateinit var databaseHost: String

    @ConfigProperty(name = "eurodat.database.port")
    private lateinit var databasePort: String

    private val objectMapper = ObjectMapper()

    private fun insertData(
        connectionUni: Uni<PgConnection>,
        jsonData: List<JsonNode>,
        table: String,
        schema: String,
    ): Uni<Unit> =
        connectionUni.chain { connection ->
            checkTableExists(connection, table, schema).chain { tableExists ->
                if (!tableExists) {
                    Uni.createFrom().failure(
                        NotFoundException("Table $schema.$table not found"),
                    )
                } else {
                    val jsonDataString = objectMapper.writeValueAsString(jsonData)
                    determineJsonSchema(table, connection, schema).chain { jsonSchema ->
                        val sql =
                            """
                            INSERT INTO $schema.$table
                            SELECT * FROM json_to_recordset('$jsonDataString') AS x($jsonSchema)
                            """.trimIndent()

                        connection
                            .preparedQuery(sql)
                            .execute()
                            // Class 22 — Data Exception / Class 23 — Integrity Constraint Violation
                            .onFailure { it is PgException && listOf("22", "23").any { code -> it.sqlState.startsWith(code) } }
                            .transform { BadRequestException("Data Exception: ${it.message}", it) }
                            .map {}
                            .onItemOrFailure()
                            .call { _, _ -> connection.close() }
                    }
                }
            }
        }

    private fun determineJsonSchema(
        table: String,
        connection: PgConnection,
        schema: String,
    ): Uni<String> =
        connection
            .preparedQuery("SELECT column_name, data_type FROM information_schema.columns WHERE table_name = $1 AND table_schema=$2")
            .execute(Tuple.of(table, schema))
            .map { rowSet ->
                rowSet.joinToString(", ") { row ->
                    "\"${row.getString("column_name")}\" ${row.getString("data_type")}"
                }
            }

    private fun getData(
        connectionUni: Uni<PgConnection>,
        table: String,
        schema: String,
        correlationId: Long? = null,
    ): Uni<List<JsonNode>> =
        connectionUni.chain { connection ->
            checkTableExists(connection, table, schema).chain { tableExists ->
                if (!tableExists) {
                    Uni.createFrom().failure(NotFoundException("Table $schema.$table not found"))
                } else {
                    val sql =
                        buildString {
                            append("SELECT row_to_json(t) FROM $schema.$table t")
                            if (correlationId != null) {
                                append(" WHERE correlation_id='$correlationId'")
                            }
                        }
                    connection
                        .query(sql)
                        .execute()
                        .map { rowSet ->
                            rowSet.map { row ->
                                objectMapper.readTree(row.getValue("row_to_json").toString())
                            }
                        }.onItemOrFailure()
                        .call { _, _ -> connection.close() }
                }
            }
        }

    private fun checkTableExists(
        connection: PgConnection,
        table: String,
        schema: String,
    ): Uni<Boolean> {
        val checkTableExistsSql =
            """
            SELECT EXISTS (
                SELECT FROM information_schema.tables
                WHERE table_schema = '$schema'
                AND table_name = $1
            )
            """.trimIndent()

        return connection
            .preparedQuery(checkTableExistsSql)
            .execute(Tuple.of(table))
            .onItem()
            .transform { rowSet ->
                rowSet.first().getBoolean("exists")
            }
    }

    private fun establishPgConnection(transactionId: String): Uni<PgConnection> =
        credentialClient
            .readExternalAccessCredentials(transactionId)
            .onFailure {
                it is WebApplicationException && it.response.status == 404
            }.transform {
                NotFoundException("Transaction $transactionId not found", it)
            }
            .chain { credentials ->
                connectToDatabase(credentials)
            }

    private fun establishPersistentDataPgConnection(appId: String): Uni<PgConnection> =
        credentialClient
            .readSafeDepositAccessCredentials(appId)
            .chain { credentials ->
                connectToDatabase(credentials)
            }

    private fun connectToDatabase(credentials: Credentials): Uni<PgConnection> {
        return PgConnection.connect(
            io.vertx.mutiny.core.Vertx
                .currentContext()
                .owner(),
            PgConnectOptions().apply {
                host = databaseHost
                port = databasePort.toInt()
                database = credentials.jdbcUrl.substringAfterLast("/")
                user = credentials.username
                password = credentials.password
            },
        )
    }

    fun getDataTransaction(
        table: String,
        transactionId: String,
        correlationId: Long? = null,
    ): Uni<List<JsonNode>> {
        return getData(establishPgConnection(transactionId), table, "output", correlationId)
    }

    fun getDataPersistent(
        table: String,
        appId: String,
        correlationId: Long? = null,
    ): Uni<List<JsonNode>> {
        return getData(establishPersistentDataPgConnection(appId), table, "safedeposit", correlationId)
    }

    fun insertDataTransaction(
        jsonData: List<JsonNode>,
        table: String,
        transactionId: String,
    ): Uni<Unit> {
        return insertData(establishPgConnection(transactionId), jsonData, table, "input")
    }

    fun insertDataPersistent(
        jsonData: List<JsonNode>,
        table: String,
        appId: String,
    ): Uni<Unit> {
        return insertData(establishPersistentDataPgConnection(appId), jsonData, table, "safedeposit")
    }
}
