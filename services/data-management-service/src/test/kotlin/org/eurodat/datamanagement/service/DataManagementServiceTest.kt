package org.eurodat.datamanagement.service

import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.test.InjectMock
import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.MockitoConfig
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import io.vertx.pgclient.PgException
import jakarta.inject.Inject
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.ForbiddenException
import jakarta.ws.rs.NotAuthorizedException
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.WebApplicationException
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.credential.Credentials
import org.eurodat.client.credential.DatabaseCredentialClient
import org.eurodat.datamanagement.PostgresTestContainer
import org.eurodat.datamanagement.resource.TEST_APP_ID
import org.eurodat.datamanagement.resource.TEST_INPUT_TABLE
import org.eurodat.datamanagement.resource.TEST_OUTPUT_TABLE
import org.eurodat.datamanagement.resource.TEST_SAFEDEPOSIT_TABLE
import org.eurodat.datamanagement.resource.TEST_TRANSACTION_ID
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever

@QuarkusTest
@QuarkusTestResource(PostgresTestContainer::class)
@RunOnVertxContext
class DataManagementServiceTest {
    @Inject
    lateinit var dataManagementService: DataManagementService

    @InjectMock
    @MockitoConfig(convertScopes = true)
    @RestClient
    lateinit var credentialClient: DatabaseCredentialClient

    @ConfigProperty(name = "quarkus.datasource.reactive.url")
    internal lateinit var datasourceUrl: String

    @ConfigProperty(name = "quarkus.datasource.username")
    internal lateinit var datasourceUsername: String

    @ConfigProperty(name = "quarkus.datasource.password")
    internal lateinit var datasourcePassword: String

    private val objectMapper = ObjectMapper()

    private val credentials
        get() =
            Credentials(
                jdbcUrl = datasourceUrl,
                username = datasourceUsername,
                password = datasourcePassword,
            )

    @Test
    fun testInsertData(asserter: UniAsserter) {
        val testData =
            """
            {
              "transaction_id": "00000000-0000-0000-0000-000000000010",
              "bank_transaction_id": "CD7XE",
              "transaction_timestamp": "2019-01-01T12:00:00",
              "created_at": "2020-01-01T12:00:00",
              "originating_bank_bic": "COBASEFFXXX",
              "counterparty_bank_bic": "KARSDS66",
              "originator_iban": "959",
              "counterparty_iban": "450",
              "amount": 400,
              "currency": "EUR",
              "transaction_type": "TRANSFER"
            }
            """
        val jsonData = objectMapper.readTree(testData)

        whenever(credentialClient.readExternalAccessCredentials(any())).thenReturn(Uni.createFrom().item(credentials))
        asserter.assertEquals(
            { dataManagementService.insertDataTransaction(listOf(jsonData), TEST_INPUT_TABLE, TEST_TRANSACTION_ID) },
            Unit,
        )
    }

    @Test
    fun testGetDataPersistent(asserter: UniAsserter) {
        val jsonData =
            """
            [
                {
                  "transaction_id": "00000000-0000-0000-0000-000000000011",
                  "bank_transaction_id": "CD7XD",
                  "transaction_timestamp": "2018-01-01T12:00:00",
                  "created_at": "2021-01-01T12:00:00",
                  "originating_bank_bic": "COBASEFDXXX",
                  "counterparty_bank_bic": "KARSDA66",
                  "originator_iban": "958",
                  "counterparty_iban": "451",
                  "amount": 401,
                  "currency": "EUR",
                  "transaction_type": "TRANSFER"
                }
            ]
            """
        val expectedJsonNode = objectMapper.readTree(jsonData).elements().next()

        whenever(credentialClient.readSafeDepositAccessCredentials(any())).thenReturn(Uni.createFrom().item(credentials))
        asserter.assertThat(
            { dataManagementService.getDataPersistent(TEST_SAFEDEPOSIT_TABLE, TEST_APP_ID) },
            { data ->
                assert(data.isNotEmpty())

                val actualDataNodes = data.map { objectMapper.readTree(it.toString()) }
                val matches =
                    actualDataNodes.any { node ->
                        node == expectedJsonNode
                    }
                assert(matches)
            },
        )
    }

    @Test
    fun testInsertDataPersistent(asserter: UniAsserter) {
        val testData =
            """
            {
              "transaction_id": "00000000-0000-0000-0000-000000000010",
              "bank_transaction_id": "CD7XE",
              "transaction_timestamp": "2019-01-01T12:00:00",
              "created_at": "2020-01-01T12:00:00",
              "originating_bank_bic": "COBASEFFXXX",
              "counterparty_bank_bic": "KARSDS66",
              "originator_iban": "959",
              "counterparty_iban": "450",
              "amount": 400,
              "currency": "EUR",
              "transaction_type": "TRANSFER"
            }
            """
        val jsonData = objectMapper.readTree(testData)

        whenever(credentialClient.readSafeDepositAccessCredentials(any())).thenReturn(Uni.createFrom().item(credentials))
        asserter.assertEquals(
            { dataManagementService.insertDataPersistent(listOf(jsonData), TEST_SAFEDEPOSIT_TABLE, TEST_APP_ID) },
            Unit,
        )
    }

    @Test
    fun testGetAllData(asserter: UniAsserter) {
        val jsonData =
            """
            [
                {
                  "transaction_id": "00000000-0000-0000-0000-000000000010",
                  "bank_transaction_id": "CD7XE",
                  "transaction_timestamp": "2019-01-01T12:00:00",
                  "created_at": "2020-01-01T12:00:00",
                  "originating_bank_bic": "COBASEFFXXX",
                  "counterparty_bank_bic": "KARSDS66",
                  "originator_iban": "959",
                  "counterparty_iban": "450",
                  "amount": 400,
                  "currency": "EUR",
                  "transaction_type": "TRANSFER",
                  "security_column": "dummy_security_column",
                  "correlation_id": 1111111111111111111
                },
                {
                  "transaction_id": "00004090-0000-0000-0000-000000000010",
                  "bank_transaction_id": "BC8YF",
                  "transaction_timestamp": "2019-01-02T12:00:00",
                  "created_at": "2020-01-02T12:00:00",
                  "originating_bank_bic": "COBASDGGXXX",
                  "counterparty_bank_bic": "KARTES77",
                  "originator_iban": "678",
                  "counterparty_iban": "405",
                  "amount": 500,
                  "currency": "EUR",
                  "transaction_type": "TRANSFER",
                  "security_column": "dummy_security_column",
                  "correlation_id": 2222222222222222222
                }
            ]
            """
        val jsonNode = objectMapper.readTree(jsonData)
        val expectedData = jsonNode.elements().asSequence().toList()

        whenever(credentialClient.readExternalAccessCredentials(any())).thenReturn(Uni.createFrom().item(credentials))
        asserter.assertThat(
            { dataManagementService.getDataTransaction(TEST_OUTPUT_TABLE, TEST_TRANSACTION_ID, null) },
            { data ->
                assert(data.isNotEmpty())
                assert(data == expectedData)
            },
        )
    }

    @Test
    fun testGetSpecificData(asserter: UniAsserter) {
        val jsonData =
            """
            [
                {
                  "transaction_id": "00000000-0000-0000-0000-000000000010",
                  "bank_transaction_id": "CD7XE",
                  "transaction_timestamp": "2019-01-01T12:00:00",
                  "created_at": "2020-01-01T12:00:00",
                  "originating_bank_bic": "COBASEFFXXX",
                  "counterparty_bank_bic": "KARSDS66",
                  "originator_iban": "959",
                  "counterparty_iban": "450",
                  "amount": 400,
                  "currency": "EUR",
                  "transaction_type": "TRANSFER",
                  "security_column": "dummy_security_column",
                  "correlation_id": 1111111111111111111
                }
            ]
            """
        val jsonNode = objectMapper.readTree(jsonData)
        val expectedData = jsonNode.elements().asSequence().toList()

        whenever(credentialClient.readExternalAccessCredentials(any())).thenReturn(Uni.createFrom().item(credentials))
        asserter.assertThat(
            { dataManagementService.getDataTransaction(TEST_OUTPUT_TABLE, TEST_TRANSACTION_ID, 1111111111111111111) },
            { data ->
                assert(data.isNotEmpty())
                assert(data == expectedData)
            },
        )
    }

    /**
     * The client token is sent to the credential service. Invalid token response should be promoted to the client.
     */
    @Test
    fun testCredential401Failure(asserter: UniAsserter) {
        Mockito
            .`when`(credentialClient.readExternalAccessCredentials(any()))
            .thenReturn(Uni.createFrom().failure(NotAuthorizedException("Unauthorized")))

        asserter.assertFailedWith(
            {
                dataManagementService.insertDataTransaction(
                    listOf(),
                    TEST_INPUT_TABLE,
                    TEST_TRANSACTION_ID,
                )
            },
            { Assertions.assertTrue(it is NotAuthorizedException) },
        )
    }

    /**
     * The client token is sent to the credential service. Forbidden response should be promoted to the client.
     */
    @Test
    fun testCredential403Failure(asserter: UniAsserter) {
        Mockito
            .`when`(credentialClient.readExternalAccessCredentials(any()))
            .thenReturn(Uni.createFrom().failure(ForbiddenException("Forbidden")))

        asserter.assertFailedWith(
            {
                dataManagementService.insertDataTransaction(
                    listOf(),
                    TEST_INPUT_TABLE,
                    TEST_TRANSACTION_ID,
                )
            },
            { Assertions.assertTrue(it is ForbiddenException) },
        )
    }

    /**
     * After credentials were fetched from the credential service, this should succeed. Therefore, this error results in a 500 response.
     */
    @Test
    fun `connection failures are not caught`(asserter: UniAsserter) {
        whenever(credentialClient.readExternalAccessCredentials(any())).thenReturn(Uni.createFrom().item(credentials.copy(password = "wrong")))

        asserter.assertFailedWith(
            {
                dataManagementService.insertDataTransaction(
                    listOf(),
                    TEST_INPUT_TABLE,
                    TEST_TRANSACTION_ID,
                )
            },
            PgException::class.java,
        )
    }

    /**
     * The credential service returns 404 for unknown or ended transactions. This should be promoted to the client.
     */
    @Test
    fun testTransactionIdNotExistsInsertData(asserter: UniAsserter) {
        Mockito
            .`when`(credentialClient.readExternalAccessCredentials(any()))
            .thenReturn(Uni.createFrom().failure(WebApplicationException("Transaction ID $TEST_TRANSACTION_ID not found", 404)))

        asserter.assertFailedWith(
            {
                dataManagementService.insertDataTransaction(
                    listOf(),
                    TEST_INPUT_TABLE,
                    TEST_TRANSACTION_ID,
                )
            },
            { NotFoundException("Transaction ID $TEST_TRANSACTION_ID not found") },
        )
    }

    /**
     * The credential service returns 404 for unknown or ended transactions. This should be promoted to the client.
     */
    @Test
    fun testTransactionIdNotExistsGetData(asserter: UniAsserter) {
        Mockito
            .`when`(credentialClient.readExternalAccessCredentials(any()))
            .thenReturn(Uni.createFrom().failure(WebApplicationException("Transaction ID $TEST_TRANSACTION_ID not found", 404)))

        asserter.assertFailedWith(
            {
                dataManagementService.getDataTransaction(
                    TEST_INPUT_TABLE,
                    TEST_TRANSACTION_ID,
                    null,
                )
            },
            { NotFoundException("Transaction ID $TEST_TRANSACTION_ID not found") },
        )
    }

    @Test
    fun testCredential401FailurePersistent(asserter: UniAsserter) {
        Mockito
            .`when`(credentialClient.readSafeDepositAccessCredentials(any()))
            .thenReturn(Uni.createFrom().failure(NotAuthorizedException("Unauthorized")))

        asserter.assertFailedWith(
            {
                dataManagementService.insertDataPersistent(
                    listOf(),
                    TEST_SAFEDEPOSIT_TABLE,
                    TEST_APP_ID,
                )
            },
            { Assertions.assertTrue(it is NotAuthorizedException) },
        )
    }

    @Test
    fun testCredential403FailurePersistent(asserter: UniAsserter) {
        Mockito
            .`when`(credentialClient.readSafeDepositAccessCredentials(any()))
            .thenReturn(Uni.createFrom().failure(ForbiddenException("Forbidden")))

        asserter.assertFailedWith(
            {
                dataManagementService.insertDataPersistent(
                    listOf(),
                    TEST_SAFEDEPOSIT_TABLE,
                    TEST_APP_ID,
                )
            },
            { Assertions.assertTrue(it is ForbiddenException) },
        )
    }

    @Test
    fun `connection failures are not caught (persistent)`(asserter: UniAsserter) {
        whenever(
            credentialClient.readSafeDepositAccessCredentials(any()),
        ).thenReturn(Uni.createFrom().item(credentials.copy(password = "wrong")))

        asserter.assertFailedWith(
            {
                dataManagementService.insertDataPersistent(
                    listOf(),
                    TEST_SAFEDEPOSIT_TABLE,
                    TEST_APP_ID,
                )
            },
            PgException::class.java,
        )
    }

    @Test
    fun `generic input`(asserter: UniAsserter) {
        val genericInputTable = "generic_input"
        val data =
            listOf(
                objectMapper.readTree(
                    """
                    {
                    "uuid": "00000000-0000-0000-0000-000000000010",
                    "bigserial": 1234567890123456789,
                    "smallserial": 12345,
                    "serial": 12345678,
                    "bigint": 1234567890123456789,
                    "bit": 0,
                    "bit_varying": "1010101010101010101010101010101010101010101010101010101010101010",
                    "boolean": false,
                    "bytea": "YmFzZTY0",
                    "char": "b",
                    "varchar": "var char",
                    "cidr": "12.13.43.12/32",
                    "circle": "<(1,2),3>",
                    "date": "2021-01-01",
                    "float": 1.23,
                    "inet4": "51.42.134.65",
                    "inet6": "2001:0db8:85a3:0000:0000:8a2e:0370:7334",
                    "integer": 12345678,
                    "interval": "1 day 2 hours 3 minutes 4 seconds",
                    "json": {"key": "value"},
                    "jsonb": {"key": "value"},
                    "line": "{1,2,3}",
                    "lseg": "[(1,2),(3,4)]",
                    "macaddr": "08:00:2b:01:02:03",
                    "macaddr8": "08:00:2b:01:02:03:04:05",
                    "money": 123.45,
                    "numeric": 123.452134567,
                    "path": "[(1,2),(3,4)]",
                    "point": "(1,2)",
                    "polygon": "((1,2),(3,4),(5,6))",
                    "real": 1.23,
                    "smallint": 12345,
                    "text": "a lot of text here ©",
                    "time": "12:34:56",
                    "timetz": "12:34:56+02:00",
                    "timestamp": "2021-01-01T12:34:56",
                    "timestamptz": "2021-01-01T12:34:56Z",
                    "xml": "<root><child/></root>"
                    }
                    """.trimIndent(),
                ),
            )
        whenever(credentialClient.readExternalAccessCredentials(any())).thenReturn(Uni.createFrom().item(credentials))

        asserter.assertEquals({ dataManagementService.insertDataTransaction(data, genericInputTable, TEST_TRANSACTION_ID) }, Unit)
    }

    @Test
    fun `generic output`(asserter: UniAsserter) {
        val genericOutputTable = "generic_output"
        whenever(credentialClient.readExternalAccessCredentials(any())).thenReturn(Uni.createFrom().item(credentials))

        asserter.assertEquals(
            {
                dataManagementService.getDataTransaction(genericOutputTable, TEST_TRANSACTION_ID, null)
            },
            listOf(
                """{
                      "uuid": "00000000-0000-0000-0000-000000000010",
                      "bigserial": 12342546789123456,
                      "smallserial": 12345,
                      "serial": 12345678,
                      "bigint": 123456789654321124,
                      "bit": "1",
                      "bit_varying": "10101010101010101",
                      "boolean": true,
                      "bytea": "\\x3fff",
                      "char": "c",
                      "varchar": "abc ghj",
                      "cidr": "12.54.12.76/32",
                      "circle": "<(1,2),5>",
                      "date": "2019-01-01",
                      "float": 12.54123,
                      "inet4": "12.65.100.89/21",
                      "inet6": "2001:db8:85a3::8a2e:370:7334",
                      "integer": 123456,
                      "interval": "1 day 02:03:04",
                      "json": { "a": 1, "b": 2 },
                      "jsonb": { "a": 1, "b": 2 },
                      "line": "{1,2,3}",
                      "lseg": "[(1,2),(3,4)]",
                      "macaddr": "08:00:2b:01:02:03",
                      "macaddr8": "08:00:2b:01:02:03:04:05",
                      "money": "$12.34",
                      "numeric": 1.2324567654321176E13,
                      "path": "[(1,2),(3,4)]",
                      "point": "(1,2)",
                      "polygon": "((1,2),(3,4),(5,6))",
                      "real": 12.54,
                      "smallint": 12345,
                      "text": "hello ©",
                      "time": "12:00:00",
                      "timetz": "12:00:00+01",
                      "timestamp": "2019-01-01T12:00:00",
                      "timestamptz": "2019-01-01T11:00:00+00:00",
                      "tsquery": "'\"fat\"' & '\"rat\"'",
                      "tsvector": "'\"fat\"' '\"rat\"' '&'",
                      "xml": "<foo>bar</foo>"
                    }
                    """,
            ).map(objectMapper::readTree),
        )
    }

    @Test
    fun `data exceptions are promoted to api`(asserter: UniAsserter) {
        whenever(credentialClient.readExternalAccessCredentials(any())).thenReturn(Uni.createFrom().item(credentials))
        val unsupportedFields =
            mapOf(
                """{"bit3": "010"}""" to "22026",
                """{"char7": "abcdefg"}""" to "22001",
            ) // Invalid BIT type length & String exceeds CHAR length

        unsupportedFields.forEach { pair ->
            asserter.assertFailedWith(
                {
                    dataManagementService.insertDataTransaction(
                        listOf(objectMapper.readTree(pair.key)),
                        "not_supported",
                        TEST_TRANSACTION_ID,
                    )
                },
                {
                    Assertions.assertInstanceOf(BadRequestException::class.java, it)
                    Assertions.assertEquals(
                        pair.value,
                        (it.cause as PgException).sqlState,
                        "SQL state was expected to be ${pair.value} for ${pair.key}, but was ${it.message}",
                    )
                },
            )
        }
    }

    @Test
    fun `constraint violations are promoted to api`(asserter: UniAsserter) {
        whenever(credentialClient.readExternalAccessCredentials(any())).thenReturn(Uni.createFrom().item(credentials))
        val missingFieldsInput =
            """
            {
              "transaction_id": "00000000-0000-0000-0000-000000000010",
              "bank_transaction_id": "CD7XE",
              "transaction_timestamp": "2019-01-01T12:00:00",
              "created_at": "2020-01-01T12:00:00"
            }
            """
        val jsonData = objectMapper.readTree(missingFieldsInput)

        asserter.assertFailedWith(
            {
                dataManagementService.insertDataTransaction(
                    listOf(jsonData),
                    TEST_INPUT_TABLE,
                    TEST_TRANSACTION_ID,
                )
            },
            {
                Assertions.assertInstanceOf(BadRequestException::class.java, it)
                Assertions.assertEquals("23502", (it.cause as PgException).sqlState)
            },
        )
    }

    @Test
    fun `should handle non-existent table in insertData`(asserter: UniAsserter) {
        val validData =
            objectMapper.readTree(
                """{"transaction_id": "00000000-0000-0000-0000-000000000001"}""",
            )
        val nonExistentTableName = "non_existent_table"

        whenever(credentialClient.readExternalAccessCredentials(any()))
            .thenReturn(Uni.createFrom().item(credentials))

        asserter.assertFailedWith(
            {
                dataManagementService.insertDataTransaction(listOf(validData), nonExistentTableName, TEST_TRANSACTION_ID)
            },
            { exception ->
                Assertions.assertInstanceOf(NotFoundException::class.java, exception)
                Assertions.assertEquals(
                    "Table input.$nonExistentTableName not found",
                    exception.message,
                )
            },
        )
    }
}
