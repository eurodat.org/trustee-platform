package org.eurodat.datamanagement

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager
import org.testcontainers.containers.PostgreSQLContainer

class PostgresTestContainer : QuarkusTestResourceLifecycleManager {
    private lateinit var container: PostgreSQLContainer<*>

    override fun start(): MutableMap<String, String> {
        container =
            PostgreSQLContainer("postgres:16")
                .withDatabaseName("test_db")
                .withUsername("postgres")
                .withPassword("pwd")
                .withInitScript("ddl.sql")
        container.start()

        val databaseUrl = container.jdbcUrl.replace("jdbc", "vertx-reactive")

        val hostnameAndPort = databaseUrl.substringAfter("://").substringBefore("/")
        return mutableMapOf(
            "quarkus.datasource.reactive.url" to databaseUrl.replace("?loggerLevel=OFF", ""),
            "quarkus.datasource.username" to container.username,
            "quarkus.datasource.password" to container.password,
            "eurodat.database.host" to hostnameAndPort.substringBefore(":"),
            "eurodat.database.port" to hostnameAndPort.substringAfter(":"),
        )
    }

    override fun stop() {
        container.close()
    }
}
