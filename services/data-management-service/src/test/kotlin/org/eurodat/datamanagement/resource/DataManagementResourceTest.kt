package org.eurodat.datamanagement.resource

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.test.InjectMock
import io.quarkus.test.common.http.TestHTTPEndpoint
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.security.TestSecurity
import io.restassured.RestAssured.given
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.NotAuthorizedException
import jakarta.ws.rs.core.MediaType
import org.eurodat.client.datamanagement.model.DataRequest
import org.eurodat.client.datamanagement.model.PersistentDataRequest
import org.eurodat.datamanagement.service.DataManagementService
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever

const val TEST_INPUT_TABLE = "input_transactions"
const val TEST_OUTPUT_TABLE = "output_transactions"
const val TEST_SAFEDEPOSIT_TABLE = "input_transactions"
const val TEST_TRANSACTION_ID = "76be5822-2e49-4135-bb32-a9d33bfb80d2"
const val TEST_APP_ID = "testApp"
const val TEST_PARTICIPANT_ID = "00000000-0000-0000-0000-000000000010"
const val TEST_CORRELATION_ID = 1111111111111111111
const val TEST_DATA = """
{
  "transaction_id": "00000000-0000-0000-0000-000000000010",
  "bank_transaction_id": "CD7XE",
  "transaction_timestamp": "2019-01-01 12:00:00.000000",
  "created_at": "2020-01-01 12:00:00.000000",
  "originating_bank_bic": "COBASEFFXXX",
  "counterparty_bank_bic": "KARSDS66",
  "originator_iban": "959",
  "counterparty_iban": "450",
  "amount": 400,
  "currency": "EUR",
  "transaction_type": "TRANSFER"
}
"""

@QuarkusTest
@TestHTTPEndpoint(DataManagementResource::class)
@TestSecurity(authorizationEnabled = false)
class DataManagementResourceTest {
    @InjectMock
    lateinit var dataManagementService: DataManagementService

    private val objectMapper = ObjectMapper()

    private val jsonData: JsonNode = objectMapper.readTree(TEST_DATA)

    private val dataRequest =
        DataRequest(
            data = listOf(TEST_DATA),
            table = TEST_INPUT_TABLE,
            transactionId = TEST_TRANSACTION_ID,
        )

    private val persistentDataRequest =
        PersistentDataRequest(
            data = listOf(TEST_DATA),
            table = TEST_SAFEDEPOSIT_TABLE,
            appId = TEST_APP_ID,
        )

    @Test
    fun testInsertDataSuccessful() {
        whenever(dataManagementService.insertDataTransaction(listOf(jsonData), TEST_INPUT_TABLE, TEST_TRANSACTION_ID))
            .thenReturn(Uni.createFrom().item(Unit))

        given()
            .contentType(MediaType.APPLICATION_JSON)
            .body(
                """{
                "data": [
                "{\"transaction_id\":\"00000000-0000-0000-0000-000000000010\",\"bank_transaction_id\":\"CD7XE\",\"transaction_timestamp\":\"2019-01-01 12:00:00.000000\",\"created_at\":\"2020-01-01 12:00:00.000000\",\"originating_bank_bic\":\"COBASEFFXXX\",\"counterparty_bank_bic\":\"KARSDS66\",\"originator_iban\":\"959\",\"counterparty_iban\":\"450\",\"amount\":400,\"currency\":\"EUR\",\"transaction_type\":\"TRANSFER\"}"
                ],
                "table": "$TEST_INPUT_TABLE",
                "transactionId": "$TEST_TRANSACTION_ID"
                }""",
            ).post("/$TEST_PARTICIPANT_ID/data")
            .then()
            .statusCode(201)
            .body("result", equalTo("Data inserted successfully"))
    }

    @Test
    fun testInsertInvalidJsonData() {
        given()
            .contentType(MediaType.APPLICATION_JSON)
            .body(
                """{
                "data": ["Invalid Data"],
                "table": "$TEST_INPUT_TABLE",
                "transactionId": "$TEST_TRANSACTION_ID"
                }""",
            ).post("/$TEST_PARTICIPANT_ID/data")
            .then()
            .statusCode(400)
            .body("error", equalTo("Invalid JSON data"))
    }

    @Test
    fun testTooManyDataRecords() {
        given()
            .contentType(MediaType.APPLICATION_JSON)
            .body(
                """{
                    "data": ["{}","{}"],
                    "table": "$TEST_INPUT_TABLE",
                    "transactionId": "$TEST_TRANSACTION_ID"
                }""",
            ).post("/$TEST_PARTICIPANT_ID/data")
            .then()
            .statusCode(400)
            .body("error", equalTo("Too many data records"))
    }

    @Test
    fun testGetData() {
        val jsonData =
            """
            {
              "transaction_id": "00000000-0000-0000-0000-000000000010",
              "bank_transaction_id": "CD7XE",
              "transaction_timestamp": "2019-01-01T12:00:00",
              "created_at": "2020-01-01T12:00:00",
              "originating_bank_bic": "COBASEFFXXX",
              "counterparty_bank_bic": "KARSDS66",
              "originator_iban": "959",
              "counterparty_iban": "450",
              "amount": 400,
              "currency": "EUR",
              "transaction_type": "TRANSFER",
              "security_column": "dummy_security_column",
              "correlation_id": 1111111111111111111
            }
            """
        val jsonNode = objectMapper.readTree(jsonData)
        val expectedEscapedJson = objectMapper.writeValueAsString(jsonNode)
        val expectedData = listOf(jsonNode)

        whenever(dataManagementService.getDataTransaction(TEST_OUTPUT_TABLE, TEST_TRANSACTION_ID, TEST_CORRELATION_ID))
            .thenReturn(Uni.createFrom().item(expectedData))

        given()
            .queryParam("table", TEST_OUTPUT_TABLE)
            .queryParam("transactionId", TEST_TRANSACTION_ID)
            .queryParam("correlationId", TEST_CORRELATION_ID)
            .get("/$TEST_PARTICIPANT_ID/data")
            .then()
            .statusCode(200)
            .body("[0]", equalTo(expectedEscapedJson))
    }

    @Test
    fun testUnauthorized() {
        whenever(dataManagementService.insertDataTransaction(listOf(jsonData), TEST_INPUT_TABLE, TEST_TRANSACTION_ID))
            .thenReturn(Uni.createFrom().failure(NotAuthorizedException("Unauthorized")))

        given()
            .contentType(MediaType.APPLICATION_JSON)
            .body(dataRequest)
            .post("/$TEST_PARTICIPANT_ID/data")
            .then()
            .statusCode(401)
    }

    @Test
    fun testBadRequestExceptionFromService() {
        whenever(
            dataManagementService.insertDataTransaction(any(), any(), any()),
        ).thenReturn(Uni.createFrom().failure(BadRequestException("bad request")))

        given()
            .contentType(MediaType.APPLICATION_JSON)
            .body(dataRequest)
            .post("/$TEST_PARTICIPANT_ID/data")
            .then()
            .statusCode(400)
            .body("error", equalTo("bad request"))
    }

    // Tests for safe-deposit endpoints:

    @Test
    fun testInsertDataSuccessfulPersistent() {
        whenever(dataManagementService.insertDataPersistent(listOf(jsonData), TEST_SAFEDEPOSIT_TABLE, TEST_APP_ID))
            .thenReturn(Uni.createFrom().item(Unit))

        given()
            .contentType(MediaType.APPLICATION_JSON)
            .body(
                """{
                "data": [
                "{\"transaction_id\":\"00000000-0000-0000-0000-000000000010\",\"bank_transaction_id\":\"CD7XE\",\"transaction_timestamp\":\"2019-01-01 12:00:00.000000\",\"created_at\":\"2020-01-01 12:00:00.000000\",\"originating_bank_bic\":\"COBASEFFXXX\",\"counterparty_bank_bic\":\"KARSDS66\",\"originator_iban\":\"959\",\"counterparty_iban\":\"450\",\"amount\":400,\"currency\":\"EUR\",\"transaction_type\":\"TRANSFER\"}"
                ],
                "table": "$TEST_SAFEDEPOSIT_TABLE",
                "appId": "$TEST_APP_ID"
                }""",
            ).post("/$TEST_PARTICIPANT_ID/persistentdata")
            .then()
            .statusCode(201)
            .body("result", equalTo("Data inserted successfully"))
    }

    @Test
    fun testInsertInvalidJsonDataPersistent() {
        given()
            .contentType(MediaType.APPLICATION_JSON)
            .body(
                """{
                "data": ["Invalid Data"],
                "table": "$TEST_SAFEDEPOSIT_TABLE",
                "appId": "$TEST_APP_ID"
                }""",
            ).post("/$TEST_PARTICIPANT_ID/persistentdata")
            .then()
            .statusCode(400)
            .body("error", equalTo("Invalid JSON data"))
    }

    @Test
    fun testTooManyDataRecordsPersistent() {
        given()
            .contentType(MediaType.APPLICATION_JSON)
            .body(
                """{
                    "data": ["{}","{}"],
                    "table": "$TEST_SAFEDEPOSIT_TABLE",
                    "appId": "$TEST_APP_ID"
                }""",
            ).post("/$TEST_PARTICIPANT_ID/persistentdata")
            .then()
            .statusCode(400)
            .body("error", equalTo("Too many data records"))
    }

    @Test
    fun testGetDataPersistent() {
        val jsonData =
            """
            {
              "transaction_id": "00000000-0000-0000-0000-000000000010",
              "bank_transaction_id": "CD7XE",
              "transaction_timestamp": "2019-01-01T12:00:00",
              "created_at": "2020-01-01T12:00:00",
              "originating_bank_bic": "COBASEFFXXX",
              "counterparty_bank_bic": "KARSDS66",
              "originator_iban": "959",
              "counterparty_iban": "450",
              "amount": 400,
              "currency": "EUR",
              "transaction_type": "TRANSFER",
              "security_column": "dummy_security_column",
              "correlation_id": 1111111111111111111
            }
            """
        val jsonNode = objectMapper.readTree(jsonData)
        val expectedEscapedJson = objectMapper.writeValueAsString(jsonNode)
        val expectedData = listOf(jsonNode)

        whenever(dataManagementService.getDataPersistent(TEST_SAFEDEPOSIT_TABLE, TEST_APP_ID))
            .thenReturn(Uni.createFrom().item(expectedData))

        given()
            .queryParam("table", TEST_SAFEDEPOSIT_TABLE)
            .queryParam("appId", TEST_APP_ID)
            .get("/$TEST_PARTICIPANT_ID/persistentdata")
            .then()
            .statusCode(200)
            .body("[0]", equalTo(expectedEscapedJson))
    }

    @Test
    fun testUnauthorizedPersistent() {
        whenever(dataManagementService.insertDataPersistent(listOf(jsonData), TEST_SAFEDEPOSIT_TABLE, TEST_APP_ID))
            .thenReturn(Uni.createFrom().failure(NotAuthorizedException("Unauthorized")))

        given()
            .contentType(MediaType.APPLICATION_JSON)
            .body(persistentDataRequest)
            .post("/$TEST_PARTICIPANT_ID/persistentdata")
            .then()
            .statusCode(401)
    }

    @Test
    fun testBadRequestExceptionFromServicePersistent() {
        whenever(
            dataManagementService.insertDataPersistent(any(), any(), any()),
        ).thenReturn(Uni.createFrom().failure(BadRequestException("bad request")))

        given()
            .contentType(MediaType.APPLICATION_JSON)
            .body(persistentDataRequest)
            .post("/$TEST_PARTICIPANT_ID/persistentdata")
            .then()
            .statusCode(400)
            .body("error", equalTo("bad request"))
    }
}
