package org.eurodat.datamanagement.resource

import io.quarkus.security.identity.SecurityIdentity
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.quarkus.vertx.http.runtime.security.HttpSecurityPolicy
import io.smallrye.mutiny.Uni
import io.vertx.core.http.HttpServerRequest
import io.vertx.ext.web.RoutingContext
import org.eclipse.microprofile.jwt.JsonWebToken
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.mockito.kotlin.whenever

@QuarkusTest
@RunOnVertxContext
class ParticipantClaimPolicyTest {
    private lateinit var participantClaimPolicy: ParticipantClaimPolicy
    private val securityIdentity: SecurityIdentity = mock()
    private val jwtMock: JsonWebToken = mock()
    private val routingContext: RoutingContext = mock()
    private val httpServerRequest: HttpServerRequest = mock()

    @BeforeEach
    fun setUp() {
        participantClaimPolicy = ParticipantClaimPolicy()
        whenever(routingContext.request()).thenReturn(httpServerRequest)
    }

    private fun mockPath(path: String) {
        whenever(routingContext.request().path()).thenReturn(path)
    }

    private fun mockParticipantClaim(participantId: String?) {
        whenever(securityIdentity.principal).thenReturn(jwtMock)
        whenever(jwtMock.getClaim<String>("participant")).thenReturn(participantId)
    }

    @Test
    fun permitAccessWithMatchingParticipantClaim(asserter: UniAsserter) {
        mockPath("/api/v1/participants/$TEST_PARTICIPANT_ID/data")
        mockParticipantClaim(TEST_PARTICIPANT_ID)

        asserter.assertEquals(
            {
                participantClaimPolicy.checkPermission(
                    routingContext,
                    Uni.createFrom().item(securityIdentity),
                    mock(),
                )
            },
            HttpSecurityPolicy.CheckResult.PERMIT,
        )
    }

    @Test
    fun denyAccessAnonymousIdentity(asserter: UniAsserter) {
        whenever(securityIdentity.isAnonymous).thenReturn(true)

        asserter.assertEquals(
            {
                participantClaimPolicy.checkPermission(
                    routingContext,
                    Uni.createFrom().item(securityIdentity),
                    mock(),
                )
            },
            HttpSecurityPolicy.CheckResult.DENY,
        )
    }

    @Test
    fun denyAccessUnmatchingParticipantClaimAndPath(asserter: UniAsserter) {
        val tokenParticipantId = "invalidParticipantId"
        mockPath("/api/v1/participants/$TEST_PARTICIPANT_ID/data")
        mockParticipantClaim(tokenParticipantId)

        asserter.assertEquals(
            {
                participantClaimPolicy.checkPermission(
                    routingContext,
                    Uni.createFrom().item(securityIdentity),
                    mock(),
                )
            },
            HttpSecurityPolicy.CheckResult.DENY,
        )
    }

    @Test
    fun denyAccessWithMissingParticipantClaim(asserter: UniAsserter) {
        mockPath("/api/v1/participants/$TEST_PARTICIPANT_ID/data")
        mockParticipantClaim(null)

        asserter.assertEquals(
            {
                participantClaimPolicy.checkPermission(
                    routingContext,
                    Uni.createFrom().item(securityIdentity),
                    mock(),
                )
            },
            HttpSecurityPolicy.CheckResult.DENY,
        )
    }

    @Test
    fun denyAccessInvalidPath(asserter: UniAsserter) {
        mockPath("/api/v1/participants/data")
        mockParticipantClaim(TEST_PARTICIPANT_ID)
        asserter.assertEquals(
            {
                participantClaimPolicy.checkPermission(
                    routingContext,
                    Uni.createFrom().item(securityIdentity),
                    mock(),
                )
            },
            HttpSecurityPolicy.CheckResult.DENY,
        )
    }
}
