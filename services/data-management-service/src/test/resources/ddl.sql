CREATE SCHEMA input;

CREATE SCHEMA output;

CREATE SCHEMA safedeposit;

-- bank transactions

create table input.input_transactions
(
    transaction_id        uuid      not null primary key,
    bank_transaction_id   text      not null,
    transaction_timestamp timestamp not null,
    created_at            timestamp not null,
    originating_bank_bic  text      not null,
    counterparty_bank_bic text      not null,
    originator_iban       text      not null,
    counterparty_iban     text      not null,
    amount                float     not null,
    currency              text      not null,
    transaction_type      text      not null
);

create table safedeposit.input_transactions
(
    transaction_id        uuid      not null primary key,
    bank_transaction_id   text      not null,
    transaction_timestamp timestamp not null,
    created_at            timestamp not null,
    originating_bank_bic  text      not null,
    counterparty_bank_bic text      not null,
    originator_iban       text      not null,
    counterparty_iban     text      not null,
    amount                float     not null,
    currency              text      not null,
    transaction_type      text      not null
);

INSERT INTO safedeposit.input_transactions (transaction_id,
                                        bank_transaction_id,
                                        transaction_timestamp,
                                        created_at,
                                        originating_bank_bic,
                                        counterparty_bank_bic,
                                        originator_iban,
                                        counterparty_iban,
                                        amount,
                                        currency,
                                        transaction_type)
VALUES ('00000000-0000-0000-0000-000000000011',
        'CD7XD',
        '2018-01-01 12:00:00.000000',
        '2021-01-01 12:00:00.000000',
        'COBASEFDXXX',
        'KARSDA66',
        '958',
        '451',
        401,
        'EUR',
        'TRANSFER');

create table output.output_transactions
(
    transaction_id        uuid      not null,
    bank_transaction_id   text,
    transaction_timestamp timestamp not null,
    created_at            timestamp not null default NOW(),
    originating_bank_bic  text      not null,
    counterparty_bank_bic text      not null,
    originator_iban       text,
    counterparty_iban     text,
    amount                float     not null,
    currency              text      not null,
    transaction_type      text      not null,
    security_column       text      not null,
    correlation_id        bigint,
    CONSTRAINT primary_key_output PRIMARY KEY (transaction_id, security_column)
);

ALTER TABLE output.output_transactions ENABLE ROW LEVEL SECURITY;

-- Insert initial data into input.input_transactions
INSERT INTO output.output_transactions (transaction_id,
                                        bank_transaction_id,
                                        transaction_timestamp,
                                        created_at,
                                        originating_bank_bic,
                                        counterparty_bank_bic,
                                        originator_iban,
                                        counterparty_iban,
                                        amount,
                                        currency,
                                        transaction_type,
                                        security_column,
                                        correlation_id)
VALUES ('00000000-0000-0000-0000-000000000010',
        'CD7XE',
        '2019-01-01 12:00:00.000000',
        '2020-01-01 12:00:00.000000',
        'COBASEFFXXX',
        'KARSDS66',
        '959',
        '450',
        400,
        'EUR',
        'TRANSFER',
        'dummy_security_column',
        1111111111111111111);

INSERT INTO output.output_transactions (transaction_id,
                                        bank_transaction_id,
                                        transaction_timestamp,
                                        created_at,
                                        originating_bank_bic,
                                        counterparty_bank_bic,
                                        originator_iban,
                                        counterparty_iban,
                                        amount,
                                        currency,
                                        transaction_type,
                                        security_column,
                                        correlation_id)
VALUES ('00004090-0000-0000-0000-000000000010',
        'BC8YF',
        '2019-01-02 12:00:00.000000',
        '2020-01-02 12:00:00.000000',
        'COBASDGGXXX',
        'KARTES77',
        '678',
        '405',
        500,
        'EUR',
        'TRANSFER',
        'dummy_security_column',
        2222222222222222222);

-- data test tables

create table input.generic_input
(
    uuid          uuid        not null primary key,
    bigserial     bigserial   not null,
    smallserial   smallserial not null,
    serial        serial      not null,
    bigint        bigint,
    bit           bit,
    bit_varying   bit varying,
    boolean       boolean,
    bytea         bytea,
    char          character,
    varchar       character varying,
    cidr          cidr,
    circle        circle,
    date          date,
    float         float8,
    inet4         inet,
    inet6         inet,
    integer       int,
    interval interval,
    json          json,
    jsonb         jsonb,
    line          line,
    lseg          lseg,
    macaddr       macaddr,
    macaddr8      macaddr8,
    money         money,
    numeric       numeric,
    path          path,
    point         point,
    polygon       polygon,
    real          float4,
    smallint      int2,
    text          text,
    time          time,
    timetz        time with time zone,
    timestamp     timestamp,
    timestamptz   timestamp with time zone,
    tsquery       tsquery,
    tsvector      tsvector,
    txid_snapshot txid_snapshot,
    xml           xml
);

create table input.not_supported
(
    bit3  bit(3),
    char7 char(7)
);

create table output.generic_output
(
    uuid        uuid        not null primary key,
    bigserial   bigserial   not null,
    smallserial smallserial not null,
    serial      serial      not null,
    bigint      bigint,
    bit         bit,
    bit_varying bit varying,
    boolean     boolean,
    bytea       bytea,
    char        character,
    varchar     varchar,
    cidr        cidr,
    circle      circle,
    date        date,
    float       float8,
    inet4       inet,
    inet6       inet,
    integer     int,
    interval interval,
    json        json,
    jsonb       jsonb,
    line        line,
    lseg        lseg,
    macaddr     macaddr,
    macaddr8    macaddr8,
    money       money,
    numeric     numeric,
    path        path,
    point       point,
    polygon     polygon,
    real        float4,
    smallint    int2,
    text        text,
    time        time,
    timetz      time with time zone,
    timestamp   timestamp,
    timestamptz timestamp with time zone,
    tsquery     tsquery,
    tsvector    tsvector,
    xml         xml
);

insert into output.generic_output
values ('00000000-0000-0000-0000-000000000010',
        12342546789123456,
        12345,
        12345678,
        123456789654321124,
        B'1',
        B'10101010101010101',
        true,
        E'\\x3FFF',
        'c',
        'abc ghj',
        '12.54.12.76',
        '<(1,2),5>',
        '2019-01-01',
        12.54123,
        '12.65.100.89/21',
        '2001:0db8:85a3:0000:0000:8a2e:0370:7334',
        123456,
        '1 day 2 hours 3 minutes 4 seconds',
        '{ "a": 1, "b": 2 }',
        '{ "a": 1, "b": 2 }',
        '{1,2,3}',
        '((1,2),(3,4))',
        '08:00:2b:01:02:03',
        '08:00:2b:01:02:03:04:05',
        '$12.34',
        12324567654321.1765432,
        '[(1,2),(3,4)]',
        '(1,2)',
        '((1,2),(3,4),(5,6))',
        12.54,
        12345,
        'hello ©',
        '12:00:00',
        '12:00:00+01',
        '2019-01-01 12:00:00',
        '2019-01-01 12:00:00+01',
        '"fat" & "rat"',
        '"fat" & "rat"',
        '<foo>bar</foo>');
