plugins {
    java
    id("io.quarkus")
    kotlin("jvm")
    id("org.sonarqube") version "4.3.1.3277"
    id("jacoco")
}

jacoco {
    toolVersion = "0.8.10" // match quarkus' version
}

val eurodatVersion: String by project
val kotlinLoggingVersion: String by project
val quarkusPlatformArtifactId: String by project
val quarkusPlatformGroupId: String by project
val quarkusPlatformVersion: String by project
val quarkusPluginId: String by project
val wiremockVersion: String by project
val mockkVersion: String by project

dependencies {
    implementation(project(":app-service-client"))
    implementation(project(":credential-service-client"))
    implementation(project(":transaction-service-client"))

    api("io.github.oshai:kotlin-logging-jvm:$kotlinLoggingVersion")
    implementation("io.vertx:vertx-web-client")
    implementation("$quarkusPluginId:quarkus-container-image-jib:$quarkusPlatformVersion")
    implementation(enforcedPlatform("$quarkusPlatformGroupId:$quarkusPlatformArtifactId:$quarkusPlatformVersion"))
    implementation("$quarkusPluginId:quarkus-arc:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-oidc:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-rest-client-oidc-filter:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-rest-jackson:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-rest-client-jackson:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-smallrye-health:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-smallrye-openapi:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-vertx:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-scheduler:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-hibernate-reactive-panache-kotlin:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-opentelemetry:$quarkusPlatformVersion")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")

    testImplementation(project(":test-common"))
    testImplementation("$quarkusPluginId:quarkus-junit5:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-junit5-mockito:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-test-security-oidc:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-test-security:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-test-oidc-server:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-jacoco:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-jdbc-postgresql:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-hibernate-reactive-panache-kotlin:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-test-vertx:$quarkusPlatformVersion")

    testImplementation("io.mockk:mockk:$mockkVersion")
    testImplementation("org.testcontainers:testcontainers")
    testImplementation("org.testcontainers:postgresql")
    testImplementation("org.testcontainers:vault")
    testImplementation("org.testcontainers:junit-jupiter")
}

group = "org.eurodat"
version = eurodatVersion

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

tasks.withType<Test> {
    systemProperty("java.util.logging.manager", "org.jboss.logmanager.LogManager")
    finalizedBy("jacocoTestReport")

    configure<JacocoTaskExtension> {
        excludeClassLoaders = listOf("*QuarkusClassLoader")
        destinationFile =
            layout.buildDirectory
                .file("jacoco/jacoco-quarkus.exec")
                .get()
                .asFile
    }
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
    options.compilerArgs.add("-parameters")
}

// Fixes JvmMockKGateway error when using mockk + quarkusTest
tasks.withType<io.quarkus.gradle.tasks.QuarkusTest> {
    jvmArgs = jvmArgs.plus("-Djdk.attach.allowAttachSelf")
}

tasks.withType<Jar>().configureEach {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}
