package org.eurodat.credentials.database

import io.quarkus.security.Authenticated
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import jakarta.ws.rs.ClientErrorException
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.Produces
import jakarta.ws.rs.WebApplicationException
import jakarta.ws.rs.core.MediaType
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.credential.Credentials
import org.eurodat.client.transactionservice.Transaction
import org.eurodat.client.transactionservice.TransactionClient
import org.eurodat.credentials.convertPostgresUrlFromConfigProperty
import org.eurodat.credentials.dbNameFromTransactionId
import org.jboss.resteasy.reactive.RestResponse
import org.jboss.resteasy.reactive.server.ServerExceptionMapper
import org.slf4j.MDC

@Path("database")
@Authenticated
class TransactionDatabaseCredentialResource {
    @ConfigProperty(name = "provisiondb.username")
    internal lateinit var username: String

    @ConfigProperty(name = "provisiondb.password")
    internal lateinit var password: String

    @ConfigProperty(name = "provisiondb.url")
    internal lateinit var url: String

    @RestClient
    internal lateinit var transactionClient: TransactionClient

    @Inject
    internal lateinit var vaultHelperService: VaultHelperService

    @RestClient
    internal lateinit var databaseVaultService: DatabaseVaultService

    @Inject
    internal lateinit var jwt: JsonWebToken

    @ServerExceptionMapper
    fun handleException(exception: ClientErrorException): Uni<RestResponse<String>> =
        Uni.createFrom().item(RestResponse.status(exception.response.statusInfo, exception.message))

    private fun validateTransaction(transactionId: String): Uni<Transaction> {
        return transactionClient
            .findTransaction(
                transactionId,
            ).onFailure { it is WebApplicationException && it.response.status == 404 }
            .transform { failure ->
                NotFoundException("transactionId was not found", failure)
            }.chain { transaction ->
                if (transaction.endTime != null) {
                    return@chain Uni.createFrom().failure(NotFoundException("Transaction has already been ended"))
                }
                Uni.createFrom().item(transaction)
            }
    }

    private fun accessCredentials(
        transactionId: String,
        roleName: String,
    ): Uni<Credentials> {
        MDC.put("transactionId", transactionId)
        return validateTransaction(transactionId).chain { transaction ->
            val clientId = jwt.getClaim<String>("client_id")
            val role =
                if (roleName == "external") {
                    roleName + "_" + clientId + "_database"
                } else {
                    roleName + "_" + dbNameFromTransactionId(transaction.id)
                }
            return@chain vaultHelperService.getCredentials(
                role,
                dbNameFromTransactionId(transactionId),
            )
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/external/{transactionId}")
    fun readExternalAccessCredentials(
        @PathParam("transactionId") transactionId: String,
    ): Uni<Credentials> {
        MDC.put("transactionId", transactionId)
        return accessCredentials(transactionId, "external")
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/internal/{transactionId}")
    fun readInternalAccessCredentials(
        @PathParam("transactionId") transactionId: String,
    ): Uni<Credentials> {
        MDC.put("transactionId", transactionId)
        return accessCredentials(transactionId, "internal")
    }

    @POST
    @Path("/internal/{transactionId}")
    fun registerInternalConnection(
        @PathParam("transactionId") transactionId: String,
    ): Uni<Unit> {
        MDC.put("transactionId", transactionId)

        val dbName = "postgres"
        val internalRole = "internal_database_$transactionId"

        return validateTransaction(transactionId)
            .chain { _ ->
                vaultHelperService.createConnectionToVault(
                    internalRole,
                    dbName,
                    convertPostgresUrlFromConfigProperty(url),
                    username,
                    password,
                )
            }.chain { _ ->
                vaultHelperService.createInternalRole(internalRole, transactionId)
            }
    }

    @POST
    @Path("external/{clientId}")
    fun registerExternalConnection(
        @PathParam("clientId") clientId: String,
    ): Uni<Unit> {
        val externalRole = "external_${clientId}_database"
        val dbName = "postgres"
        return vaultHelperService
            .createConnectionToVault(
                externalRole,
                dbName,
                convertPostgresUrlFromConfigProperty(url),
                username,
                password,
            ).chain { _ ->
                vaultHelperService.createExternalRole(
                    externalRole,
                    clientId,
                )
            }
    }

    @DELETE
    @Path("/internal/{transactionId}")
    fun deleteInternalConnection(
        @PathParam("transactionId") transactionId: String,
    ): Uni<Unit> {
        val dbName = dbNameFromTransactionId(transactionId)
        return databaseVaultService
            .deleteRole("internal_$dbName")
            .chain { _ ->
                databaseVaultService.deleteConnection("internal_$dbName")
            }
    }

    @DELETE
    @Path("/external/{clientId}")
    fun deleteUserConnection(
        @PathParam("clientId") clientId: String,
    ): Uni<Unit> =
        databaseVaultService
            .deleteRole("external_${clientId}_database")
            .chain { _ ->
                databaseVaultService.deleteConnection("external_${clientId}_database")
            }
}
