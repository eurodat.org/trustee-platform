package org.eurodat.credentials

fun dbNameFromTransactionId(transactionId: String) = "database_" + transactionId

fun convertPostgresUrlFromConfigProperty(urlToBeConverted: String) =
    urlToBeConverted.replace("vertx-reactive", "jdbc").substringBeforeLast("/") + "/"
