package org.eurodat.credentials

import jakarta.annotation.Priority
import jakarta.inject.Inject
import jakarta.ws.rs.Priorities
import org.eurodat.credentials.database.VaultLoginHelperService
import org.jboss.resteasy.reactive.client.spi.ResteasyReactiveClientRequestContext
import org.jboss.resteasy.reactive.client.spi.ResteasyReactiveClientRequestFilter
import java.io.IOException

@Priority(Priorities.HEADER_DECORATOR)
class VaultTokenFilter : ResteasyReactiveClientRequestFilter {
    @Inject
    private lateinit var vaultLoginHelperService: VaultLoginHelperService

    @Throws(IOException::class)
    override fun filter(requestContext: ResteasyReactiveClientRequestContext) {
        requestContext.suspend()
        vaultLoginHelperService.getVaultToken().subscribe().with { token ->
            requestContext.headers.add("X-Vault-Token", token)
            requestContext.resume()
        }
    }
}
