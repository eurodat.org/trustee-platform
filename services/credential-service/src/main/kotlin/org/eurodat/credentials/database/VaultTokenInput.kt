package org.eurodat.credentials.database

data class VaultTokenInput(
    val role: String = "reader",
    var jwt: String = "uninitialized",
)
