package org.eurodat.credentials

import jakarta.enterprise.context.ApplicationScoped

@ApplicationScoped
class TcpPortHelperService {
    fun getTcpPortFromUrl(
        url: String,
        tcpportmappings: String,
    ): String {
        // Hack to get transaction-plane namespace
        val controlPlaneNamespace =
            url.split(".").find { str -> str.contains("transaction") }
                ?: throw Exception("Transaction plane namespace could not be determined from $url")
        return getTcpPortByNamespace(controlPlaneNamespace, tcpportmappings)
    }

    fun getTcpPortByNamespace(
        namespace: String,
        tcpportmappings: String,
    ): String {
        val portRegex = Regex("(\\d+):")
        val controlPlaneRegex = Regex("\"(\\S+)/\\S+:\\d+\"")
        val mapEntries =
            tcpportmappings.split(",").map {
                Pair(
                    portRegex
                        .find(it)
                        ?.groups
                        ?.get(1)
                        ?.value,
                    controlPlaneRegex
                        .find(it)
                        ?.groups
                        ?.get(1)
                        ?.value,
                )
            }
        val relevantEntry =
            mapEntries.find { it.second.equals(namespace) }?.first
                ?: throw Exception("Relevant mapping for $namespace not found as part of $tcpportmappings")
        return relevantEntry
    }
}
