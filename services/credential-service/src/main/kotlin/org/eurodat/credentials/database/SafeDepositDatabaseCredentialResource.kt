package org.eurodat.credentials.database

import io.quarkus.security.Authenticated
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.MediaType
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.credential.Credentials
import org.eurodat.credentials.TcpPortHelperService
import org.slf4j.MDC

@Path("database")
@Authenticated
class SafeDepositDatabaseCredentialResource {
    @Inject
    internal lateinit var vaultHelperService: VaultHelperService

    @ConfigProperty(name = "eurodat.transaction-db.url")
    internal lateinit var url: String

    @ConfigProperty(name = "eurodat.fqdn")
    internal lateinit var fqdn: String

    @ConfigProperty(name = "tcpportmapping")
    internal lateinit var tcpportmappings: String

    @Inject
    internal lateinit var tcpPortHelperService: TcpPortHelperService

    @RestClient
    internal lateinit var databaseVaultService: DatabaseVaultService

    @Inject
    internal lateinit var jwt: JsonWebToken

    private fun accessCredentialsSafeDeposit(appId: String): Uni<Credentials> {
        MDC.put("appId", appId)
        val clientId = jwt.getClaim<String>("client_id")
        val appInsert = appId.lowercase()
        val roleName = "external_${clientId}_database"
        val jdbcUrl = fqdn.replace("https", "jdbc:postgresql") + ":" + tcpPortHelperService.getTcpPortFromUrl(url, tcpportmappings) + "/"
        return databaseVaultService.getCredentials(roleName).map { vaultResponse ->
            Credentials(
                jdbcUrl + "safedeposit_${appInsert}_$clientId",
                vaultResponse["data"]["username"].asText(),
                vaultResponse["data"]["password"].asText(),
                vaultResponse["lease_duration"].asInt(),
            )
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/safedeposit/{appId}")
    fun readSafeDepositAccessCredentials(
        @PathParam("appId") appId: String,
    ): Uni<Credentials> {
        MDC.put("appId", appId)
        return accessCredentialsSafeDeposit(appId)
    }
}
