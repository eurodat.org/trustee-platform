package org.eurodat.credentials.database

import com.fasterxml.jackson.databind.JsonNode
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.Produces
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import org.eurodat.credentials.VaultTokenFilter

@RegisterRestClient(configKey = "eurodat-vault")
@RegisterProvider(VaultTokenFilter::class)
@Produces("application/json")
@Path("database")
interface DatabaseVaultService {
    @POST
    @Path("config/{connectionName}")
    fun createConnection(
        @PathParam("connectionName") connectionName: String,
        createConnectionBody: String,
    ): Uni<Unit>

    @POST
    @Path("roles/{roleName}")
    fun createRole(
        @PathParam("roleName") roleName: String,
        createRoleBody: VaultRoleCreationRequest,
    ): Uni<Unit>

    @GET
    @Path("creds/{roleName}")
    fun getCredentials(
        @PathParam("roleName") roleName: String,
    ): Uni<JsonNode>

    @DELETE
    @Path("roles/{roleName}")
    fun deleteRole(
        @PathParam("roleName") roleName: String,
    ): Uni<Unit>

    @DELETE
    @Path("config/{connectionName}")
    fun deleteConnection(
        @PathParam("connectionName") connectionName: String,
    ): Uni<Unit>
}
