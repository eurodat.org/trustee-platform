package org.eurodat.credentials.database

import jakarta.annotation.Priority
import jakarta.ws.rs.Priorities
import jakarta.ws.rs.core.GenericEntity
import jakarta.ws.rs.core.MediaType
import org.jboss.resteasy.reactive.client.spi.ResteasyReactiveClientRequestContext
import org.jboss.resteasy.reactive.client.spi.ResteasyReactiveClientRequestFilter
import java.io.IOException

/**
 * Client filter which converts a standard request with Authentication: Bearer <jwt> to Vault's specification for the login endpoint
 *
 * IN
 *
 * Authorization: Bearer <jwt>
 * ---
 * <empty body>
 *
 * OUT
 *
 * <no authorization token in header>
 * ---
 * {role: role, jwt: <jwt>}
 */

@Priority(Priorities.HEADER_DECORATOR)
class OidcVaultFilter : ResteasyReactiveClientRequestFilter {
    private val charactersToSkipInToken = "Bearer ".length

    @Throws(IOException::class)
    override fun filter(requestContext: ResteasyReactiveClientRequestContext) {
        requestContext.setEntity(
            GenericEntity(
                VaultTokenInput(jwt = (requestContext.headers.getFirst("Authorization") as String).drop(charactersToSkipInToken)),
                VaultTokenInput::class.java,
            ),
            requestContext.entityAnnotations,
            MediaType.APPLICATION_JSON_TYPE,
        )

        requestContext.headers.remove("Authorization")
    }
}
