package org.eurodat.credentials.database

import io.quarkus.oidc.client.filter.OidcClientFilter
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient

/**
 * REST client service which allows calls to Vault API endpoints which are authenticated via OIDC.
 */
@RegisterRestClient(configKey = "eurodat-vault-login")
@OidcClientFilter
@RegisterProvider(OidcVaultFilter::class)
@Path("/auth/jwt/login")
fun interface VaultLoginService {
    @POST
    fun getVaultToken(): Uni<String>
}
