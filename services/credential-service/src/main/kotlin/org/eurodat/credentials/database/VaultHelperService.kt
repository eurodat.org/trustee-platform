package org.eurodat.credentials.database

import io.github.oshai.kotlinlogging.KotlinLogging
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.credential.Credentials
import org.eurodat.credentials.TcpPortHelperService
import org.eurodat.credentials.convertPostgresUrlFromConfigProperty

private val logger = KotlinLogging.logger {}

@ApplicationScoped
class VaultHelperService {
    @ConfigProperty(name = "eurodat.transaction-db.url")
    internal lateinit var url: String

    @ConfigProperty(name = "eurodat.fqdn")
    internal lateinit var fqdn: String

    @RestClient
    internal lateinit var databaseVaultService: DatabaseVaultService

    @ConfigProperty(name = "tcpportmapping")
    internal lateinit var tcpportmappings: String

    @Inject
    internal lateinit var tcpPortHelperService: TcpPortHelperService

    companion object {
        const val SQL_CREATE_USER = "CREATE USER \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}';"
    }

    fun getCredentials(
        roleName: String,
        dbName: String,
    ): Uni<Credentials> {
        val jdbcUrl =
            when {
                roleName.startsWith("internal") -> convertPostgresUrlFromConfigProperty(url)
                roleName.startsWith("external") ->
                    fqdn.replace(
                        "https",
                        "jdbc:postgresql",
                    ) + ":" + tcpPortHelperService.getTcpPortFromUrl(url, tcpportmappings) + "/"

                else -> throw Exception("Wrong role name!")
            }
        return databaseVaultService.getCredentials(roleName).map { vaultResponse ->
            Credentials(
                jdbcUrl + dbName,
                vaultResponse["data"]["username"].asText(),
                vaultResponse["data"]["password"].asText(),
                vaultResponse["lease_duration"].asInt(),
            )
        }
    }

    fun createConnectionToVault(
        connectionName: String,
        dbName: String,
        dbUrl: String,
        username: String,
        password: String,
    ): Uni<Unit> {
        try {
            return databaseVaultService.createConnection(
                connectionName,
                """{
                    "plugin_name": "postgresql-database-plugin",
                    "allowed_roles": "$connectionName",
                    "connection_url": "postgresql://{{username}}:{{password}}@${dbUrl.split("//")[1]}$dbName",
                    "username": "$username",
                    "password": "$password",
                    "max_idle_connections": -1
                    }""",
            )
        } catch (e: Exception) {
            logger.error(e) { "Error creating connection: $e" }
            throw e
        }
    }

    fun createInternalRole(
        roleName: String,
        transactionId: String,
    ): Uni<Unit> {
        val dbRole = "transaction_${transactionId}_internal"
        val generalInternalCreationStatements =
            listOf(
                SQL_CREATE_USER,
                "GRANT $dbRole TO \"{{name}}\";",
            )
        val input = (generalInternalCreationStatements).joinToString(" ")
        val roleCreationRequest = VaultRoleCreationRequest(roleName, input)
        try {
            return databaseVaultService.createRole(roleName, roleCreationRequest)
        } catch (e: Exception) {
            logger.error(e) { "Error creating connection: $e" }
            throw e
        }
    }

    fun createExternalRole(
        roleName: String,
        clientId: String,
    ): Uni<Unit> {
        val dbRole = "db_${clientId.lowercase()}_external"
        val generalInternalCreationStatements =
            listOf(
                SQL_CREATE_USER,
                "GRANT $dbRole TO \"{{name}}\";",
            )
        val input = (generalInternalCreationStatements).joinToString(" ")
        val roleCreationRequest = VaultRoleCreationRequest(roleName, input)
        try {
            return databaseVaultService.createRole(roleName, roleCreationRequest)
        } catch (e: Exception) {
            logger.error(e) { "Error creating connection: $e" }
            throw e
        }
    }
}
