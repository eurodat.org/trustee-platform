package org.eurodat.credentials.database

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import org.eclipse.microprofile.rest.client.inject.RestClient

@ApplicationScoped
class VaultLoginHelperService {
    @RestClient
    private lateinit var vaultLoginService: VaultLoginService

    fun getVaultToken(): Uni<String> =
        vaultLoginService.getVaultToken().map { token ->
            val mapper = ObjectMapper()
            val jsonNode: JsonNode = mapper.readTree(token)
            val vaultToken = jsonNode["auth"]["client_token"].asText()
            vaultToken
        }
}
