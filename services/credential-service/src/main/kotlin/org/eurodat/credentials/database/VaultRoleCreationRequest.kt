package org.eurodat.credentials.database

import com.fasterxml.jackson.annotation.JsonProperty

data class VaultRoleCreationRequest(
    @JsonProperty("db_name") val dbName: String,
    @JsonProperty("creation_statements") val creationStatements: String,
    @JsonProperty("default_ttl") val defaultTTL: Int = 2764800,
    @JsonProperty("max_ttl") val maxTTL: Int = 2764800,
)
