CREATE SCHEMA input;
CREATE SCHEMA intermediate;
CREATE SCHEMA output;
CREATE SCHEMA safedeposit;
CREATE TABLE input.table_input (column1 INTEGER, column2 INTEGER);
CREATE TABLE intermediate.table_intermediate (column1 INTEGER, column2 INTEGER);
CREATE TABLE output.table_output (column1 INTEGER, column2 INTEGER, security_column VARCHAR);

create role transaction_60385_64289_external NOLOGIN;
GRANT USAGE ON SCHEMA input TO transaction_60385_64289_external;
GRANT SELECT, INSERT ON ALL TABLES IN SCHEMA input TO transaction_60385_64289_external;
GRANT USAGE ON SCHEMA output TO transaction_60385_64289_external;
GRANT SELECT ON ALL TABLES IN SCHEMA output TO transaction_60385_64289_external;

create role db_64289_external NOLOGIN INHERIT;
grant transaction_60385_64289_external to db_64289_external;

create role safedeposit_safeamlappmock_internal NOLOGIN;
GRANT USAGE ON SCHEMA safedeposit TO safedeposit_safeamlappmock_internal;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA safedeposit to safedeposit_safeamlappmock_internal;

create role transaction_60385_internal NOLOGIN;
GRANT USAGE ON SCHEMA input TO transaction_60385_internal;
GRANT SELECT ON ALL TABLES IN SCHEMA input TO transaction_60385_internal;
GRANT USAGE ON SCHEMA intermediate TO transaction_60385_internal;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA intermediate TO transaction_60385_internal;
GRANT USAGE ON SCHEMA output TO transaction_60385_internal;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA output TO transaction_60385_internal;
