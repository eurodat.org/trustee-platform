package org.eurodat.credentials.database

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.TestProfile
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import org.eurodat.testcommon.DevServicesDisabledProfile
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@QuarkusTest
@TestProfile(DevServicesDisabledProfile::class)
class VaultHelperServiceTest {
    private val databaseVaultService = mockk<DatabaseVaultService>()

    @Inject
    private lateinit var vaultHelperService: VaultHelperService

    @BeforeEach
    fun setUp() {
        vaultHelperService.databaseVaultService = databaseVaultService
    }

    @Test
    fun getCredentials() {
        val resultJsonString =
            "{\"request_id\":\"5bb5f07e-85e1-5005-f5be-1d7be6ab4e1d\"," +
                "\"lase_id\":\"database/creds/external_database_1810979f_b0c2_4083_b57b_f01b370de1ae/LUJzfNj7oxFttLSd2fLEinFH\"," +
                "\"renewable\":true,\"lease_duration\":2764800,\"data\":{\"password\":\"wfDFUJig6-mEF3M27NfK\"," +
                "\"username\":\"v-jwt-0e8e-external-MgiqtECXU1OxUSuTLyNK-1690314673\"},\"wrap_info\":null,\"warnings\":null,\"auth\":null}"
        val resultJsonNode: JsonNode = ObjectMapper().readTree(resultJsonString)
        every { databaseVaultService.getCredentials(any()) } returns Uni.createFrom().item(resultJsonNode)

        val credentials = vaultHelperService.getCredentials("internal", "dbName").await().indefinitely()
        Assertions.assertEquals(2764800, credentials.leaseDuration)
        Assertions.assertEquals("v-jwt-0e8e-external-MgiqtECXU1OxUSuTLyNK-1690314673", credentials.username)
        Assertions.assertEquals("wfDFUJig6-mEF3M27NfK", credentials.password)
    }

    @Test
    fun testCreateConnectionToVault() {
        val allowedRoles = "dummyRole1"
        every { databaseVaultService.createConnection(any(), any()) } returns Uni.createFrom().nullItem()
        vaultHelperService.createConnectionToVault(
            allowedRoles,
            "dummyDbName",
            "dummy//dummyUrl",
            "dummyUser",
            "dummyPassword",
        )
        verify(exactly = 1) { databaseVaultService.createConnection("dummyRole1", any()) }
    }

    @Test
    fun testCreateRole() {
        val dummyRoleName = "dummyRoleName"
        every { databaseVaultService.createRole(any(), any()) } returns Uni.createFrom().nullItem()
        vaultHelperService.createInternalRole(dummyRoleName, "transactionId")
        vaultHelperService.createExternalRole(dummyRoleName, "clientId")
        verify(exactly = 2) { databaseVaultService.createRole(any(), any()) }
    }
}
