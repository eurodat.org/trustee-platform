package org.eurodat.credentials

import io.mockk.every
import io.mockk.mockk
import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.TestProfile
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import jakarta.ws.rs.core.MultivaluedHashMap
import jakarta.ws.rs.core.MultivaluedMap
import org.eurodat.credentials.database.VaultLoginHelperService
import org.eurodat.testcommon.DevServicesDisabledProfile
import org.jboss.resteasy.reactive.client.spi.ResteasyReactiveClientRequestContext
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito

@QuarkusTest
@TestProfile(DevServicesDisabledProfile::class)
class VaultTokenFilterTest {
    @Inject
    lateinit var vaultTokenFilter: VaultTokenFilter

    @InjectMock
    lateinit var vaultLoginHelperService: VaultLoginHelperService

    @Test
    fun filter() {
        val clientRequestContextImpl: ResteasyReactiveClientRequestContext = mockk()
        val map: MultivaluedMap<String, Any> = MultivaluedHashMap<String, Any>()

        every { clientRequestContextImpl.getHeaders() } returns map
        every { clientRequestContextImpl.suspend() } returns Unit
        every { clientRequestContextImpl.resume() } returns Unit

        val resultVaultToken = "hvs.ThisIsAVaultToken"
        Mockito.`when`(vaultLoginHelperService.getVaultToken()).thenReturn(Uni.createFrom().item(resultVaultToken))

        vaultTokenFilter.filter(clientRequestContextImpl)

        Assertions.assertEquals(resultVaultToken, map["X-Vault-Token"]?.get(0))
    }
}
