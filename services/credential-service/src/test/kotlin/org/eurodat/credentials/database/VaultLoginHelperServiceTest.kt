package org.eurodat.credentials.database

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.TestProfile
import io.quarkus.test.junit.mockito.MockitoConfig
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.testcommon.DevServicesDisabledProfile
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito

@QuarkusTest
@TestProfile(DevServicesDisabledProfile::class)
class VaultLoginHelperServiceTest {
    @InjectMock
    @MockitoConfig(convertScopes = true)
    @RestClient
    private lateinit var vaultLoginService: VaultLoginService

    @Inject
    private lateinit var vaultLoginHelperService: VaultLoginHelperService

    @Test
    fun getVaultToken() {
        val resultJsonString =
            "{\"request_id\":\"663509bf-3efb-9355-7239-784c067b760b\",\"lease_id\":\"\",\"renewable\":false,\"lease_duration\":0,\"data\":null,\"wrap_info\":null,\"warnings\":null,\"auth\":{\"client_token\":\"hvs.CAESICS50_-dTai1Py5-W6KJd1WEs2m4QKR3CRqVOYo9qjVJGh4KHGh2cy5vb2VFU3d3bXpqOFE0NVhEemhQbng5N1c\",\"accessor\":\"c2Nd3Wty0K19WO9nKUTWrYst\",\"policies\":[\"default\",\"manager\"],\"token_policies\":[\"default\",\"manager\"],\"metadata\":{\"role\":\"reader\"},\"lease_duration\":2764800,\"renewable\":true,\"entity_id\":\"2ddc3b1f-0c1a-e371-37a8-e52deb6dcaae\",\"token_type\":\"service\",\"orphan\":true,\"mfa_requirement\":null,\"num_uses\":0}}"
        Mockito.`when`(vaultLoginService.getVaultToken()).thenReturn(Uni.createFrom().item(resultJsonString))

        Assertions.assertEquals(
            vaultLoginHelperService.getVaultToken().await().indefinitely(),
            "hvs.CAESICS50_-dTai1Py5-W6KJd1WEs2m4QKR3CRqVOYo9qjVJGh4KHGh2cy5vb2VFU3d3bXpqOFE0NVhEemhQbng5N1c",
        )
    }
}
