package org.eurodat.credentials.database

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.utility.MountableFile

class PostgresTestContainer : QuarkusTestResourceLifecycleManager {
    private lateinit var container: PostgreSQLContainer<*>

    override fun start(): MutableMap<String, String> {
        container =
            PostgreSQLContainer("postgres:12")
                .withDatabaseName("database_60385")
                .withUsername("superuser")
                .withPassword("pwd")
                .withCopyFileToContainer(
                    MountableFile.forClasspathResource("init.sql"),
                    "/docker-entrypoint-initdb.d/init.sql",
                )
        container.start()

        val databaseUrl = container.getJdbcUrl().replace("jdbc", "vertx-reactive")
        val databaseUrlWithoutPort = container.getJdbcUrl().substringBeforeLast(":")

        // The following hack transforms the url that is acquired by postgres (from the view of the docker host)
        // to a url from the view of the vault container. This is needed because the vault container has to talk to
        // the postgres container. Note that localhost in a container points again to the container itself.
        // If postgres is running in docker in docker then the host will be "docker".
        // If postgres is running in docker directly by the host system (like in local setup),
        // then the host will be localhost.
        // By using host.docker.internal docker knows that it should connect to the host system.
        val url: String
        if (databaseUrl.contains("docker")) {
            url = databaseUrl.replace("docker", "host.docker.internal")
        } else if (databaseUrl.contains("localhost")) {
            url = databaseUrl.replace("localhost", "host.docker.internal")
        } else {
            throw Exception("Url must contain either docker (for docker in docker in gitlab pipeline) or localhost (for local test runs).")
        }

        return mutableMapOf(
            "eurodat.fqdn" to databaseUrlWithoutPort,
            "eurodat.transaction-db.url" to databaseUrl,
            "provisiondb.url" to url,
            "provisiondb.password" to container.getPassword(),
            "provisiondb.username" to container.getUsername(),
            "quarkus.datasource.username" to container.getUsername(),
            "quarkus.datasource.password" to container.getPassword(),
        )
    }

    override fun stop() {
        container.close()
    }
}
