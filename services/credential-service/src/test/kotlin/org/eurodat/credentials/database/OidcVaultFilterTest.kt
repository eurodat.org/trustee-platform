package org.eurodat.credentials.database

import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.TestProfile
import jakarta.inject.Inject
import jakarta.ws.rs.core.GenericEntity
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.MultivaluedHashMap
import jakarta.ws.rs.core.MultivaluedMap
import org.eurodat.testcommon.DevServicesDisabledProfile
import org.jboss.resteasy.reactive.client.impl.ClientRequestContextImpl
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@QuarkusTest
@TestProfile(DevServicesDisabledProfile::class)
class OidcVaultFilterTest {
    @Inject
    lateinit var oidcVaultFilter: OidcVaultFilter

    @Test
    fun filter() {
        val clientRequestContextImpl: ClientRequestContextImpl = mockk()
        val map: MultivaluedMap<String, Any> = MultivaluedHashMap<String, Any>()
        map["Authorization"] = listOf("Bearer eyThisIsAJwtToken")
        val slot = slot<GenericEntity<VaultTokenInput>>()

        every { clientRequestContextImpl.headers } returns map
        every { clientRequestContextImpl.entityAnnotations } returns emptyArray()
        every {
            clientRequestContextImpl.setEntity(
                capture(slot),
                emptyArray(),
                MediaType.APPLICATION_JSON_TYPE,
            )
        } returns Unit

        oidcVaultFilter.filter(clientRequestContextImpl)

        Assertions.assertEquals("eyThisIsAJwtToken", slot.captured.entity.jwt)
    }
}
