package org.eurodat.credentials.database

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.core.Response
import org.eurodat.client.credential.Credentials
import org.eurodat.client.transactionservice.Transaction
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.time.LocalDateTime

@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TransactionDatabaseCredentialResourceTest {
    private var credentialEndpointResource: TransactionDatabaseCredentialResource =
        TransactionDatabaseCredentialResource().also {
            it.url = "test-url"
            it.username = "vault"
            it.password = "vault"
            it.transactionClient = mockk()
            it.vaultHelperService = mockk()
            it.databaseVaultService = mockk()
            it.jwt = mockk()
            every { it.jwt.getClaim<String>("client_id") } returns "data_consumer"
            every { it.vaultHelperService.getCredentials(any(), any()) } returns
                uni {
                    Credentials("jdbcUrl", "nocturnal_user", "safe_pw", 42)
                }
            every { it.transactionClient.findTransaction(any()) } returns
                uni {
                    Transaction("foo", "safeAMLAppMock")
                }
            every {
                it.vaultHelperService.createConnectionToVault(
                    any(),
                    any(),
                    any(),
                    any(),
                    any(),
                )
            } returns Uni.createFrom().nullItem()
            every { it.vaultHelperService.createInternalRole(any(), any()) } returns Uni.createFrom().nullItem()
            every { it.vaultHelperService.createExternalRole(any(), any()) } returns Uni.createFrom().nullItem()
            every { it.databaseVaultService.deleteRole(any()) } returns Uni.createFrom().nullItem()
            every { it.databaseVaultService.deleteConnection(any()) } returns Uni.createFrom().nullItem()
        }

    fun testReadAccessCredentials(role: String) {
        val appId = "safeAMLAppMock"
        val transaction = Transaction("foo", "safeAMLAppMock")
        if (role == "External") {
            credentialEndpointResource.readExternalAccessCredentials(transaction.id).await().indefinitely()
        }
        if (role == "Internal") {
            credentialEndpointResource.readInternalAccessCredentials(transaction.id).await().indefinitely()
        }
        verify { credentialEndpointResource.transactionClient.findTransaction(transaction.id) }
        verify { credentialEndpointResource.vaultHelperService.getCredentials(any(), any()) }
    }

    @Test
    fun testReadInternalAccessCredentials() {
        testReadAccessCredentials("Internal")
    }

    @Test
    fun testReadExternalAccessCredentials() {
        testReadAccessCredentials("External")
    }

    fun testReadNonExistentAccessCredentialsTransactionId(role: String) {
        val transactionId = "nonsense"
        every { credentialEndpointResource.transactionClient.findTransaction("nonsense") } throws NotFoundException()

        Assertions.assertThrows(NotFoundException::class.java) {
            if (role == "External") {
                credentialEndpointResource.readExternalAccessCredentials(transactionId).await().indefinitely()
            } else if (role == "Internal") {
                credentialEndpointResource.readInternalAccessCredentials(transactionId).await().indefinitely()
            }
        }
    }

    @Test
    fun testReadNonExistentExternalAccessCredentialsTransactionId() {
        testReadNonExistentAccessCredentialsTransactionId("External")
    }

    @Test
    fun testReadNonExistentInternalAccessCredentialsTransactionId() {
        testReadNonExistentAccessCredentialsTransactionId("Internal")
    }

    @Test
    @RunOnVertxContext
    fun testReadExternalAccessCredentialsWithNotFoundException(asserter: UniAsserter) {
        val transactionId = "nonsense"

        every { credentialEndpointResource.transactionClient.findTransaction(transactionId) } returns
            Uni.createFrom().failure(NotFoundException("transactionId was not found"))

        asserter.assertFailedWith(
            {
                credentialEndpointResource.readExternalAccessCredentials(transactionId)
            },
            { failure ->
                assert(failure is NotFoundException)
                Assertions.assertEquals("transactionId was not found", failure.message)
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testReadExternalAccessCredentialsWithEndTime(asserter: UniAsserter) {
        val transactionId = "withEndTime"
        val transaction = Transaction(transactionId, "safeAMLAppMock").also { it.endTime = LocalDateTime.of(1990, 1, 1, 7, 0) }

        every { credentialEndpointResource.transactionClient.findTransaction(transactionId) } returns
            Uni.createFrom().item(transaction)

        asserter.assertFailedWith(
            {
                credentialEndpointResource.readExternalAccessCredentials(transactionId)
            },
            { failure ->
                assert(failure is NotFoundException)
                Assertions.assertEquals("Transaction has already been ended", failure.message)
            },
        )
    }

    @Test
    fun testRegisterConnection() {
        credentialEndpointResource.registerInternalConnection("dummyTransactionId").await().indefinitely()
        credentialEndpointResource.registerExternalConnection("dummyClientId").await().indefinitely()

        verify {
            credentialEndpointResource.vaultHelperService.createConnectionToVault(
                any(),
                any(),
                any(),
                any(),
                any(),
            )
        }

        verify { credentialEndpointResource.vaultHelperService.createInternalRole(any(), any()) }
        verify { credentialEndpointResource.vaultHelperService.createExternalRole(any(), any()) }
    }

    @Test
    fun testDeleteInternalConnection() {
        credentialEndpointResource.deleteInternalConnection("dummyTransactionId").await().indefinitely()
        verify(exactly = 1) { credentialEndpointResource.databaseVaultService.deleteRole(any()) }
        verify { credentialEndpointResource.databaseVaultService.deleteConnection(any()) }
    }

    @Test
    fun testDeleteUserConnection() {
        credentialEndpointResource.deleteUserConnection("dummyClientId").await().indefinitely()
        verify { credentialEndpointResource.databaseVaultService.deleteRole(any()) }
        verify { credentialEndpointResource.databaseVaultService.deleteConnection(any()) }
    }

    @Test
    @RunOnVertxContext
    fun testExceptionHandling(asserter: UniAsserter) {
        val exceptionText = "Not Found"
        val notFoundException = NotFoundException(exceptionText)

        asserter.assertThat(
            {
                credentialEndpointResource.handleException(notFoundException)
            },
            { response ->
                Assertions.assertEquals(Response.Status.NOT_FOUND.statusCode, response.status)
                Assertions.assertEquals(exceptionText, response.entity)
            },
        )
    }
}
