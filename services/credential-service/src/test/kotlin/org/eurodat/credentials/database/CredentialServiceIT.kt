package org.eurodat.credentials.database

import io.quarkus.test.InjectMock
import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.MockitoConfig
import io.quarkus.test.security.TestSecurity
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.transactionservice.Transaction
import org.eurodat.client.transactionservice.TransactionClient
import org.eurodat.credentials.TcpPortHelperService
import org.eurodat.credentials.convertPostgresUrlFromConfigProperty
import org.eurodat.credentials.dbNameFromTransactionId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import java.sql.DriverManager
import java.sql.SQLException
import java.time.LocalDateTime

@QuarkusTest
@QuarkusTestResource(VaultTestContainer::class)
@QuarkusTestResource(PostgresTestContainer::class)
class CredentialServiceIT {
    @InjectMock
    private lateinit var vaultLoginHelperService: VaultLoginHelperService

    @InjectMock
    private lateinit var tcpPortHelperService: TcpPortHelperService

    @InjectMock
    @MockitoConfig(convertScopes = true)
    @RestClient
    private lateinit var transactionClient: TransactionClient

    @InjectMock
    internal lateinit var jwt: JsonWebToken

    @ConfigProperty(name = "eurodat.transaction-db.url")
    lateinit var jdbcUrl: String

    @Inject
    private lateinit var credentialEndpointResource: TransactionDatabaseCredentialResource

    val transactionId = "60385"
    val clientId = "64289"

    @BeforeEach
    fun mockSetup() {
        Mockito.`when`(vaultLoginHelperService.getVaultToken()).thenReturn(Uni.createFrom().item("my-root-token"))
        Mockito
            .`when`(tcpPortHelperService.getTcpPortFromUrl(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(jdbcUrl.substringAfterLast(":").substringBefore("/"))
        val transactionMockItem =
            Transaction(
                transactionId,
                "",
                clientId,
                listOf("consumer1", "consumer2"),
                listOf("provider1", "provider2"),
                LocalDateTime.now(),
                null,
            )
        Mockito
            .`when`(transactionClient.findTransaction(transactionId))
            .thenReturn(Uni.createFrom().item(transactionMockItem))
        Mockito.`when`(jwt.getClaim<String>("client_id")).thenReturn(clientId)
    }

    @Test
    @Order(1)
    @TestSecurity(authorizationEnabled = false)
    fun testVaultInitialization() {
        credentialEndpointResource.registerInternalConnection(transactionId).await().indefinitely()
        credentialEndpointResource.registerExternalConnection(clientId).await().indefinitely()
    }

    @Test
    @Order(2)
    @TestSecurity(authorizationEnabled = false)
    fun testInternalRoleAccess() {
        val internalCredentials =
            credentialEndpointResource.readInternalAccessCredentials(transactionId).await().indefinitely()
        val internalUserStatement =
            DriverManager
                .getConnection(
                    convertPostgresUrlFromConfigProperty(jdbcUrl) +
                        dbNameFromTransactionId(
                            transactionId,
                        ),
                    internalCredentials.username,
                    internalCredentials.password,
                ).createStatement()

        // internal user can only select on input
        val resultSelectOutput = internalUserStatement.executeQuery("SELECT count(*) from input.table_input; ")
        resultSelectOutput.next()
        Assertions.assertEquals(0, resultSelectOutput.getInt(1))

        Assertions.assertThrows(SQLException::class.java) {
            internalUserStatement.executeUpdate("INSERT INTO input.table_input VALUES (1, 2); ")
        }
        Assertions.assertThrows(SQLException::class.java) {
            internalUserStatement.executeUpdate("UPDATE input.table_input set column1=2 where column1=1; ")
        }
        Assertions.assertThrows(SQLException::class.java) {
            internalUserStatement.executeUpdate("DELETE FROM input.table_input WHERE column1 = 2; ")
        }

        // internal user can do all (select, insert, update, delete) on intermediate
        internalUserStatement.executeUpdate("INSERT INTO intermediate.table_intermediate VALUES (1, 2); ")
        val resultSelectIntermediate =
            internalUserStatement.executeQuery("SELECT count(*) from intermediate.table_intermediate; ")
        resultSelectIntermediate.next()
        Assertions.assertEquals(1, resultSelectIntermediate.getInt(1))
        internalUserStatement.executeUpdate("UPDATE intermediate.table_intermediate set column1=2 where column1=1; ")
        internalUserStatement.executeUpdate("DELETE FROM intermediate.table_intermediate WHERE column1 = 2; ")

        // internal user can do all (select, insert, update, delete) on output
        internalUserStatement.executeUpdate("INSERT INTO output.table_output VALUES (1, 2, 'abc'); ")
        val resultSelectOutputInternal =
            internalUserStatement.executeQuery("SELECT count(*) from output.table_output; ")
        resultSelectOutputInternal.next()
        Assertions.assertEquals(1, resultSelectOutputInternal.getInt(1))
        internalUserStatement.executeUpdate("UPDATE output.table_output set column1=2 where column1=1; ")
        internalUserStatement.executeUpdate("DELETE FROM output.table_output WHERE column1 = 2; ")
    }

    @Test
    @Order(2)
    @TestSecurity(authorizationEnabled = false)
    fun testExternalRoleAccess() {
        val externalCredentials =
            credentialEndpointResource.readExternalAccessCredentials(transactionId).await().indefinitely()
        val externalUserStatement =
            DriverManager
                .getConnection(
                    convertPostgresUrlFromConfigProperty(jdbcUrl) +
                        dbNameFromTransactionId(
                            transactionId,
                        ),
                    externalCredentials.username,
                    externalCredentials.password,
                ).createStatement()

        // external user can do select and insert on input
        externalUserStatement.executeUpdate("INSERT INTO input.table_input VALUES (1, 2); ")

        val resultSelectOutput = externalUserStatement.executeQuery("SELECT count(*) from input.table_input; ")
        resultSelectOutput.next()
        Assertions.assertEquals(1, resultSelectOutput.getInt(1))
        Assertions.assertThrows(SQLException::class.java) {
            externalUserStatement.executeUpdate("UPDATE input.table_input set column1=2 where column1=1; ")
        }
        Assertions.assertThrows(SQLException::class.java) {
            externalUserStatement.executeUpdate("DELETE FROM input.table_input WHERE column1 = 2; ")
        }

        // external user has no access to intermediate
        Assertions.assertThrows(SQLException::class.java) {
            externalUserStatement.executeUpdate("INSERT INTO intermediate.table_intermediate VALUES (1, 2); ")
        }
        Assertions.assertThrows(SQLException::class.java) {
            externalUserStatement.executeUpdate("UPDATE intermediate.table_intermediate set column1=2 where column1=1; ")
        }
        Assertions.assertThrows(SQLException::class.java) {
            externalUserStatement.executeUpdate("DELETE FROM intermediate.table_intermediate WHERE column1 = 2; ")
        }
        Assertions.assertThrows(SQLException::class.java) {
            externalUserStatement.executeUpdate("SELECT count(*) from intermediate.table_intermediate; ")
        }

        // external user can only select on output
        externalUserStatement.executeQuery("SELECT count(*) from output.table_output; ")
        Assertions.assertThrows(SQLException::class.java) {
            externalUserStatement.executeUpdate("INSERT INTO output.table_output VALUES (1, 2, 'abc'); ")
        }
        Assertions.assertThrows(SQLException::class.java) {
            externalUserStatement.executeUpdate("UPDATE output.table_output set column1=2 where column1=1; ")
        }
        Assertions.assertThrows(SQLException::class.java) {
            externalUserStatement.executeUpdate("DELETE FROM output.table_output WHERE column1 = 2; ")
        }
    }
}
