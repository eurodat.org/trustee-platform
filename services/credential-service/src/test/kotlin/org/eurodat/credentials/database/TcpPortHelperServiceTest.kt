package org.eurodat.credentials.database

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.TestProfile
import org.eurodat.credentials.TcpPortHelperService
import org.eurodat.testcommon.DevServicesDisabledProfile
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@QuarkusTest
@TestProfile(DevServicesDisabledProfile::class)
class TcpPortHelperServiceTest {
    val tcpPortHelperService: TcpPortHelperService = TcpPortHelperService()
    val standardTcpPortMapping =
        "5432: \"transaction-plane/eurodat-postgresql:5432\", 5433: \"transaction-plane-sample/eurodat-postgresql:5432\""

    @Test
    fun testStandard() {
        val url = "https://sample-url.transaction-plane.com"
        Assertions.assertEquals("5432", tcpPortHelperService.getTcpPortFromUrl(url, standardTcpPortMapping))
    }

    @Test
    fun testStandardReversedOrder() {
        val reversedTcpPortMapping =
            "5433: \"transaction-plane-sample/eurodat-postgresql:5432\", 5432: \"transaction-plane/eurodat-postgresql:5432\" "
        val url = "https://sample-url.transaction-plane.com"
        Assertions.assertEquals("5432", tcpPortHelperService.getTcpPortFromUrl(url, reversedTcpPortMapping))
    }

    @Test
    fun testUrlWithoutControlPlane() {
        val url = "jdbc:postgresql://eurodat-postgresql.data-plane.svc:5432/database_123456"
        assertThrows<Exception> { tcpPortHelperService.getTcpPortFromUrl(url, standardTcpPortMapping) }
    }

    @Test
    fun testWrongMappings() {
        val url = "jdbc:postgresql://eurodat-postgresql.transaction-plane.svc:5432/database_123456"
        val tcpportmappings = "5432"
        assertThrows<Exception> { tcpPortHelperService.getTcpPortFromUrl(url, tcpportmappings) }
    }
}
