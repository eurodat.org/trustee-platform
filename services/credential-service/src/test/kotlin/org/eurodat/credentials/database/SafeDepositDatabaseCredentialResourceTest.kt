package org.eurodat.credentials.database

import com.fasterxml.jackson.databind.ObjectMapper
import io.mockk.every
import io.mockk.mockk
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.TestProfile
import io.smallrye.mutiny.Uni
import org.eurodat.testcommon.DevServicesDisabledProfile
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestProfile(DevServicesDisabledProfile::class)
class SafeDepositDatabaseCredentialResourceTest {
    private val username = "v-jwt-0e8e-external-MgiqtECXU1OxUSuTLyNK-1690314673"
    private val password = "wfDFUJig6-mEF3M27NfK"
    private val leaseDuration = 2764800

    private var credentialSafeDepositDatabaseResource: SafeDepositDatabaseCredentialResource =
        SafeDepositDatabaseCredentialResource().also {
            it.vaultHelperService = mockk()
            it.databaseVaultService = mockk()
            it.jwt = mockk()
            it.fqdn = "https://eurodat.gmbh.local"
            it.url = "vertx-reactive:postgresql://eurodat-postgresql.transaction-plane.svc:5432/postgres"
            it.tcpportmappings = "5432: \"transaction-plane/eurodat-postgresql:5432\""
            it.tcpPortHelperService = mockk()
            every { it.jwt.getClaim<String>("client_id") } returns "data_consumer"
            every { it.tcpPortHelperService.getTcpPortFromUrl(any(), any()) } returns "5432"
            every { it.databaseVaultService.getCredentials(any()) } returns
                Uni.createFrom().item(
                    ObjectMapper().readTree(
                        """
                    {
                      "request_id": "5bb5f07e-85e1-5005-f5be-1d7be6ab4e1d",
                      "lease_id": "database/creds/database_test",
                      "renewable": true,
                      "lease_duration": $leaseDuration,
                      "data": {
                        "password": "$password",
                        "username": "$username"
                      },
                      "wrap_info": null,
                      "warnings": null,
                      "auth": null
                    }
                        """.trimMargin(),
                    ),
                )

            every {
                it.vaultHelperService.createConnectionToVault(
                    any(),
                    any(),
                    any(),
                    any(),
                    any(),
                )
            } returns Uni.createFrom().nullItem()
        }

    @Test
    fun readSafeDepositAccessCredentials() {
        val appId = "mockApp"
        val credentials = credentialSafeDepositDatabaseResource.readSafeDepositAccessCredentials(appId).await().indefinitely()
        Assertions.assertEquals(username, credentials.username)
        Assertions.assertEquals(password, credentials.password)
        Assertions.assertEquals(leaseDuration, credentials.leaseDuration)
        Assertions.assertEquals(
            "jdbc:postgresql://eurodat.gmbh.local:5432/safedeposit_mockapp_data_consumer",
            credentials.jdbcUrl,
        )
    }
}
