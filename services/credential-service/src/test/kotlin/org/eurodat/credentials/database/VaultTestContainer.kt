package org.eurodat.credentials.database
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager
import org.testcontainers.utility.DockerImageName
import org.testcontainers.vault.VaultContainer

class VaultTestContainer : QuarkusTestResourceLifecycleManager {
    private val vaultImage = DockerImageName.parse("hashicorp/vault:1.14").asCompatibleSubstituteFor("vault")
    private val vaultToken = "my-root-token"

    private lateinit var container: VaultContainer<*>

    override fun start(): MutableMap<String, String> {
        container =
            VaultContainer(vaultImage)
                .withVaultToken(vaultToken)
                .withInitCommand("secrets enable database")
                .withExtraHost("host.docker.internal", "host-gateway") // needed for docker in linux
        container.start()

        return mutableMapOf(
            "quarkus.rest-client.eurodat-vault-login.url" to container.getHttpHostAddress() + "/v1",
            "quarkus.rest-client.eurodat-vault.url" to container.getHttpHostAddress() + "/v1",
        )
    }

    override fun stop() {
        container.close()
    }
}
