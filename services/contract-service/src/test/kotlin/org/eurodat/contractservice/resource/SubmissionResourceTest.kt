package org.eurodat.contractservice.resource

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.hibernate.reactive.panache.PanacheEntityBase.persist
import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.common.http.TestHTTPEndpoint
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eurodat.contractservice.PostgresTestContainer
import org.eurodat.contractservice.entity.CompanyEntity
import org.eurodat.contractservice.entity.ContractEntity
import org.eurodat.contractservice.entity.ParticipantEntity
import org.eurodat.contractservice.model.ContractState
import org.eurodat.contractservice.model.ContractType
import org.eurodat.contractservice.repository.PanacheCompanyRepository
import org.eurodat.contractservice.repository.PanacheContractRepository
import org.hibernate.reactive.mutiny.Mutiny.SessionFactory
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.ZonedDateTime

@QuarkusTest
@QuarkusTestResource(PostgresTestContainer::class)
@TestHTTPEndpoint(SubmissionResource::class)
class SubmissionResourceTest {
    @Inject
    private lateinit var contractRepository: PanacheContractRepository

    @Inject
    private lateinit var companyRepository: PanacheCompanyRepository

    @Inject
    lateinit var sessionFactory: SessionFactory

    @ConfigProperty(name = "paperless.password")
    internal lateinit var paperlessToken: String

    private val objectMapper = ObjectMapper()

    private val testContractType = ContractType.ONBOARDING.type
    private val testOwnerId = "bc277bb1-0d69-4722-95e2-6e7ce049ad4b"

    private val testBusinessContractId = "${testContractType}_$testOwnerId"
    private val testSubmissionId = 335608
    private var testCompanyId = "123company"
    private val sentContractEntity =
        ContractEntity(
            businessId = testBusinessContractId,
            validFrom = ZonedDateTime.now().minusDays(1),
            validTo = null,
            contractType = testContractType,
            ownerId = testOwnerId,
            appId = "appId",
            contractValidFrom = null,
            contractValidTo = null,
            status = ContractState.SENT.status,
            submissionId = testSubmissionId,
            companyId = testCompanyId,
        )

    private val companyEntity =
        CompanyEntity(
            id = null,
            businessId = testCompanyId,
            name = "",
            streetAddress = "",
            zip = "",
            town = "",
            country = "",
            registerLocation = "",
            registerNo = "",
            vatId = null,
            nationalTaxId = null,
            createdAt = ZonedDateTime.now(),
        )

    private fun setup(): Uni<Void> =
        sessionFactory
            .withTransaction { _ ->
                contractRepository.deleteAll()
            }.chain { _ ->
                sessionFactory.withTransaction { _ ->
                    companyRepository.deleteAll()
                }
            }.chain { _ ->
                sessionFactory.withTransaction { _ ->
                    persist(companyEntity)
                }
            }

    @Test
    @RunOnVertxContext
    fun testProcessSubmission(uniAsserter: UniAsserter) {
        uniAsserter.assertEquals({
            setup().chain { _ ->
                sessionFactory
                    .withTransaction { _ ->
                        contractRepository.persist(sentContractEntity.copy(contractType = "dummy"))
                    }.map { it.businessId }
            }
        }, testBusinessContractId)

        val fileName = "submissionWebhookBody.json"
        val submissionBody: JsonNode = readJson(fileName)

        uniAsserter.assertThat(
            {
                Uni.createFrom().item(
                    given()
                        .queryParam("token", paperlessToken)
                        .contentType(ContentType.JSON)
                        .body(submissionBody.toString())
                        .`when`()
                        .post("/"),
                )
            },
            {
                it.then().statusCode(200)
            },
        )

        uniAsserter.assertThat(
            {
                sessionFactory.withSession {
                    contractRepository.findContractBySubmissionId(testSubmissionId)
                }
            },
            { contract ->
                Assertions.assertEquals(ContractState.SIGNED.status, contract?.status)
                Assertions.assertNotNull(contract?.contractValidFrom)
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testProcessOnboardingSubmissionAndTermsAndConditionsCreation(uniAsserter: UniAsserter) {
        uniAsserter.assertEquals({
            setup().chain { _ ->
                sessionFactory
                    .withTransaction { _ ->
                        contractRepository.persist(sentContractEntity)
                    }.map { it.businessId }
            }
        }, testBusinessContractId)

        val fileName = "submissionWebhookBody.json"
        val submissionBody: JsonNode = readJson(fileName)

        uniAsserter.assertThat(
            {
                Uni.createFrom().item(
                    given()
                        .queryParam("token", paperlessToken)
                        .contentType(ContentType.JSON)
                        .body(submissionBody.toString())
                        .`when`()
                        .post("/"),
                )
            },
            {
                it.then().statusCode(200)
            },
        )

        uniAsserter.assertThat(
            {
                sessionFactory.withSession {
                    contractRepository.findContractBySubmissionId(testSubmissionId)
                }
            },
            { contract ->
                Assertions.assertEquals(ContractState.SIGNED.status, contract?.status)
                Assertions.assertNotNull(contract?.contractValidFrom)
            },
        )

        uniAsserter.assertThat(
            {
                sessionFactory.withSession {
                    contractRepository.findContract("TERMS_AND_CONDITIONS_1_$testOwnerId")
                }
            },
            { contract ->
                Assertions.assertNotNull(contract)
                Assertions.assertEquals(ContractState.SENT.status, contract?.status)
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testUpdateParticipantStatus(uniAsserter: UniAsserter) {
        val submissionBody = objectMapper.createObjectNode()
        submissionBody.put("event", "participant.completed")

        val objectNode = submissionBody.putObject("object")
        objectNode.put("email", "one@example.com")
        objectNode.put("id", 111111)
        objectNode.put("state", "completed")

        val testBusinessID = "${ContractType.TNC_2.type}_$testOwnerId"
        val testContractEntity =
            createContractWithParticipants(testBusinessID)

        uniAsserter.assertEquals(
            {
                sessionFactory
                    .withTransaction { _ ->
                        contractRepository.persist(testContractEntity)
                    }.map { it.participants[0].documentId }
            },
            111111,
        )

        uniAsserter.assertThat(
            {
                Uni.createFrom().item(
                    given()
                        .queryParam("token", paperlessToken)
                        .contentType(ContentType.JSON)
                        .body(submissionBody.toString())
                        .`when`()
                        .post("/"),
                )
            },
            {
                it.then().statusCode(200)
            },
        )

        // Verify participant status update, but contract is not SIGNED
        uniAsserter.assertThat(
            {
                sessionFactory.withSession {
                    contractRepository.findContract(testBusinessID)
                }
            },
            { contract ->
                val updatedParticipant = contract?.participants?.find { it.documentId == 111111 }
                Assertions.assertEquals("completed", updatedParticipant?.status)
                Assertions.assertEquals("SENT", contract?.status) // contract should still be SENT
                val unchangedParticipant = contract?.participants?.find { it.documentId == 222222 }
                Assertions.assertEquals("sent", unchangedParticipant?.status) // the other participant should be unchanged
            },
        )
    }

    @Test
    fun testProcessDocument() {
        val fileName = "documentWebhookBody.json"
        val submissionBody: JsonNode = readJson(fileName)

        given()
            .queryParam("token", paperlessToken)
            .contentType(ContentType.JSON)
            .body(submissionBody.toString())
            .`when`()
            .post("/")
            .then()
            .statusCode(400)
    }

    @Test
    fun testUnauthorizedSubmission() {
        val fileName = "submissionWebhookBody.json"
        val submissionBody: JsonNode = readJson(fileName)

        given()
            .queryParam("token", "WrongToken")
            .contentType(ContentType.JSON)
            .body(submissionBody.toString())
            .`when`()
            .post("/")
            .then()
            .statusCode(401)
    }

    @Test
    fun testSubmissionNotFound() {
        val fileName = "submissionWebhookBody.json"
        val submissionBody: JsonNode = readJson(fileName)
        given()
            .queryParam("token", paperlessToken)
            .contentType(ContentType.JSON)
            .body(submissionBody.toString())
            .`when`()
            .post("/")
            .then()
            .statusCode(404)
    }

    private fun readJson(fileName: String): JsonNode {
        objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
        val submissionBodyJson =
            {}
                .javaClass.classLoader
                .getResourceAsStream(fileName)
                ?.bufferedReader()
                ?.readText()
        return objectMapper.readTree(submissionBodyJson)
    }

    private fun createContractWithParticipants(testBusinessID: String): ContractEntity {
        val testContractEntity =
            sentContractEntity.copy(
                businessId = testBusinessID,
                contractType = ContractType.TNC_2.type,
                participants = listOf(),
            )
        val testParticipantEntities =
            listOf(
                ParticipantEntity(
                    firstName = "Dummy1",
                    lastName = "User1",
                    emailAddress = "one@example.com",
                    status = "sent",
                    participantRole = "Signer-1",
                    documentId = 111111,
                    contract = testContractEntity,
                ),
                ParticipantEntity(
                    firstName = "Dummy2",
                    lastName = "User2",
                    emailAddress = "two@example.com",
                    status = "sent",
                    participantRole = "Signer-2",
                    documentId = 222222,
                    contract = testContractEntity,
                ),
            )

        testContractEntity.participants = testParticipantEntities
        testParticipantEntities.forEach { participant ->
            participant.contract = testContractEntity
        }
        return testContractEntity
    }
}
