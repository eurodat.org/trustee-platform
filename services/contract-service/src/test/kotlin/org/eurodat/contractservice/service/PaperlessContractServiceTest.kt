package org.eurodat.contractservice.service

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.test.InjectMock
import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import jakarta.ws.rs.BadRequestException
import org.eurodat.client.contractservice.model.ContractRequest
import org.eurodat.contractservice.PostgresTestContainer
import org.eurodat.contractservice.model.Contract
import org.eurodat.contractservice.model.ContractState
import org.eurodat.contractservice.model.ContractType
import org.eurodat.contractservice.model.Participant
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever

@QuarkusTest
@QuarkusTestResource(PostgresTestContainer::class)
class PaperlessContractServiceTest {
    @InjectMock
    private lateinit var paperlessService: PaperlessService

    @InjectMock
    private lateinit var contractService: ContractService

    @Inject
    private lateinit var paperlessContractService: PaperlessContractService

    private val onboardingContractRequest = ContractRequest(ContractType.ONBOARDING.type, "appId")

    private val mockedDocument: JsonNode =
        ObjectMapper().createObjectNode().apply {
            put("id", 12345)
            put("name", "Test Document")
            put("submission_id", 98765)
            put("completed_at", "2024-08-14T10:00:00.000Z")
        }

    private val name = "testName"
    private val email = "testEmail"
    private val userSubject = "1234567890"
    private val testCompanyId = "123456789L"

    @Test
    @RunOnVertxContext
    fun testCreateNewOnboardingContract(asserter: UniAsserter) {
        whenever(paperlessService.createCompanyMasterData(any(), any(), any())).thenReturn(Uni.createFrom().item(mockedDocument))
        whenever(paperlessService.verifyTemplateName(any())).thenReturn(true)
        whenever(contractService.createNewContract(any(), any(), any(), any(), any())).thenReturn(
            Uni.createFrom().item {
                Contract(
                    "ONBOARDING_COMPANY_MASTERDATA_1234567890",
                    onboardingContractRequest.contractType,
                    "userSubject",
                    "appId",
                    null,
                    null,
                    ContractState.SENT.status,
                    98765,
                    companyId = testCompanyId,
                )
            },
        )

        asserter.assertThat(
            {
                paperlessContractService.createNewOnboardingContract(email, name, userSubject, onboardingContractRequest)
            },
            { contract ->
                Assertions.assertNotNull(contract)
                Assertions.assertEquals(ContractState.SENT.status, contract.status)
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testCreateNewTermsAndConditionsContract(asserter: UniAsserter) {
        val participants =
            listOf(
                Participant(
                    firstName = "Participant",
                    lastName = "One",
                    email = "participantone@example.com",
                    role = "Role1",
                    status = "status",
                ),
                Participant(
                    firstName = "Participant",
                    lastName = "Two",
                    email = "participanttwo@example.com",
                    role = "Role2",
                    status = "status",
                ),
            )

        whenever(paperlessService.createTermsAndConditionsDocument(any(), any())).thenReturn(Uni.createFrom().item(mockedDocument))
        whenever(paperlessService.verifyTemplateName(any())).thenReturn(true)
        whenever(contractService.createNewContract(any(), any(), any(), any(), any())).thenReturn(
            Uni.createFrom().item {
                Contract(
                    "TERMS_AND_CONDITIONS_2_1234567890",
                    "TERMS_AND_CONDITIONS_2",
                    "userSubject",
                    "appId",
                    null,
                    null,
                    ContractState.SENT.status,
                    98765,
                    companyId = testCompanyId,
                )
            },
        )

        asserter.assertThat(
            {
                paperlessContractService.createNewTermsAndConditionsContract(
                    userSubject,
                    ContractRequest("TERMS_AND_CONDITIONS_2", "appId"),
                    companyId = testCompanyId,
                    participants,
                )
            },
            { contract ->
                Assertions.assertNotNull(contract)
                Assertions.assertEquals(ContractState.SENT.status, contract.status)
            },
        )
    }

    @Test
    fun testCreateNewTermsAndConditionsContractWithEmptySigners() {
        Assertions.assertThrows(BadRequestException::class.java) {
            paperlessContractService
                .createNewTermsAndConditionsContract(
                    userSubject,
                    ContractRequest(ContractType.TNC_1.type, "appId"),
                    companyId = testCompanyId,
                    emptyList(),
                ).await()
                .indefinitely()
        }
    }

    @Test
    @RunOnVertxContext
    fun testCreateNewContractInvalidTemplateName(asserter: UniAsserter) {
        whenever(paperlessService.verifyTemplateName(any())).thenReturn(false)

        asserter.assertFailedWith(
            {
                paperlessContractService.createNewOnboardingContract(
                    email,
                    name,
                    userSubject,
                    onboardingContractRequest,
                )
            },
            { exception ->
                Assertions.assertInstanceOf(BadRequestException::class.java, exception)
                Assertions.assertEquals(
                    "Contract type ${onboardingContractRequest.contractType} not found",
                    exception.message,
                )
            },
        )
    }
}
