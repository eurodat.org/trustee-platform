package org.eurodat.contractservice.resource

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.test.InjectMock
import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.common.http.TestHTTPEndpoint
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.security.TestSecurity
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import io.restassured.response.ValidatableResponse
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eurodat.client.contractservice.model.Contract
import org.eurodat.client.contractservice.model.ContractRequest
import org.eurodat.contractservice.PostgresTestContainer
import org.eurodat.contractservice.entity.CompanyEntity
import org.eurodat.contractservice.entity.ContractEntity
import org.eurodat.contractservice.model.ContractState
import org.eurodat.contractservice.paperless.PaperlessWireMockProducer
import org.eurodat.contractservice.repository.PanacheCompanyRepository
import org.eurodat.contractservice.repository.PanacheContractRepository
import org.eurodat.contractservice.service.PaperlessService
import org.hibernate.reactive.mutiny.Mutiny
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.kotlin.any
import java.time.ZonedDateTime

@QuarkusTest
@QuarkusTestResource(PostgresTestContainer::class)
@TestHTTPEndpoint(ContractResource::class)
class ContractResourceTest {
    @Inject
    private lateinit var companyRepository: PanacheCompanyRepository

    private val nameClaim = "testName"
    private val emailClaim = "testEmail"
    private val userSubjectClaim = "1234567890"
    private val dummyBusinessContractId = "dummyType_1234567890"

    private val testCompanyId = "123456789L"

    private val companyEntity =
        CompanyEntity(
            id = null,
            businessId = testCompanyId,
            name = "",
            streetAddress = "",
            zip = "",
            town = "",
            country = "",
            registerLocation = "",
            registerNo = "",
            vatId = null,
            nationalTaxId = null,
            createdAt = ZonedDateTime.now(),
        )

    private var sentContractEntity =
        ContractEntity(
            null,
            dummyBusinessContractId,
            ZonedDateTime.now(),
            null,
            "dummyType",
            userSubjectClaim,
            "1234567890",
            null,
            null,
            ContractState.SENT.status,
            276905,
            companyId = testCompanyId,
        )
    private var contract =
        Contract(
            id = dummyBusinessContractId,
            contractType = "dummyType",
            appId = "1234567890",
            contractValidFrom = null,
            contractValidTo = null,
            status = ContractState.SENT.status,
            participants = listOf(),
            companyId = testCompanyId,
        )

    @Inject
    private lateinit var contractRepository: PanacheContractRepository

    @Inject
    private lateinit var contractResource: ContractResource

    @InjectMock
    internal lateinit var paperlessService: PaperlessService

    @Inject
    lateinit var sessionFactory: Mutiny.SessionFactory

    private fun setupCleanDB(): Uni<Long> =
        sessionFactory
            .withTransaction { _ ->
                contractRepository.deleteAll()
            }.chain { _ ->
                sessionFactory.withTransaction { _ ->
                    companyRepository.deleteAll()
                }
            }

    private fun setupTestSentEntity(): Uni<Long> =
        sessionFactory
            .withTransaction { _ ->
                contractRepository.deleteAll()
            }.chain { _ ->
                sessionFactory.withTransaction { session ->
                    companyRepository
                        .deleteAll()
                        .call { _ ->
                            session.persist(companyEntity)
                        }.call { _ ->
                            session.persist(sentContractEntity)
                        }
                }
            }

    private fun initJsonWebToken() {
        val jwtMock = Mockito.mock(JsonWebToken::class.java)

        Mockito.`when`(jwtMock.getClaim<String>("name")).thenReturn(nameClaim)
        Mockito.`when`(jwtMock.getClaim<String>("email")).thenReturn(emailClaim)
        Mockito.`when`(jwtMock.getClaim<String>("sub")).thenReturn(userSubjectClaim)
        contractResource.jwt = jwtMock
    }

    @Test
    @RunOnVertxContext
    @TestSecurity(authorizationEnabled = false)
    fun testHealth(asserter: UniAsserter) {
        asserter.assertThat(
            {
                Uni.createFrom().item {
                    given()
                        .`when`()
                        .get("/health")
                }
            },
            {
                it.then().statusCode(200)
            },
        )
    }

    @Test
    @RunOnVertxContext
    @TestSecurity(authorizationEnabled = false)
    fun testFindContract(asserter: UniAsserter) {
        asserter.assertEquals(
            {
                setupTestSentEntity()
                    .chain { _ ->
                        Uni.createFrom().item {
                            given()
                                .`when`()
                                .get("/$dummyBusinessContractId")
                                .then()
                                .statusCode(200)
                                .extract()
                                .`as`(Contract::class.java)
                        }
                    }
            },
            contract,
        )
    }

    @Test
    @RunOnVertxContext
    @TestSecurity(authorizationEnabled = false)
    fun testFindContractNotFound(asserter: UniAsserter) {
        asserter.assertThat(
            {
                setupTestSentEntity().chain { _ ->
                    Uni.createFrom().item {
                        given()
                            .`when`()
                            .get("/invalidContractId")
                    }
                }
            },
            {
                it.then().statusCode(404)
            },
        )
    }

    @Test
    @RunOnVertxContext
    @TestSecurity(authorizationEnabled = false)
    fun testGetAllContracts(asserter: UniAsserter) {
        val jwtMock: JsonWebToken = Mockito.mock(JsonWebToken::class.java)
        contractResource.jwt = jwtMock
        Mockito.`when`(jwtMock.getClaim<String>("sub")).thenReturn(sentContractEntity.ownerId)

        asserter.assertThat(
            {
                setupTestSentEntity().chain { _ ->
                    Uni.createFrom().item {
                        given()
                            .`when`()
                            .get("/")
                            .then()
                            .statusCode(200)
                            .extract()
                            .`as`(MutableList::class.java)
                    }
                }
            },
            {
                Assertions.assertEquals(1, it.size)
            },
        )
    }

    @Test
    @RunOnVertxContext
    @TestSecurity(authorizationEnabled = false)
    fun testDeleteContract(asserter: UniAsserter) {
        asserter.assertThat(
            {
                setupTestSentEntity().chain { _ ->
                    Uni.createFrom().item {
                        given()
                            .`when`()
                            .delete("/$dummyBusinessContractId")
                    }
                }
            },
            {
                it.then().statusCode(204)
            },
        )
    }

    @Test
    @RunOnVertxContext
    @TestSecurity(authorizationEnabled = false)
    fun testDeleteContractNotFound(asserter: UniAsserter) {
        asserter.assertThat(
            {
                setupTestSentEntity().chain { _ ->
                    Uni.createFrom().item {
                        given()
                            .`when`()
                            .delete("/invalidContractId")
                    }
                }
            },
            {
                it.then().statusCode(404)
            },
        )
    }

    @Test
    @RunOnVertxContext
    @TestSecurity(authorizationEnabled = false)
    fun testCreateContract(asserter: UniAsserter) {
        initJsonWebToken()
        val contractRequest = ContractRequest(contract.contractType, "1234567890")

        val documentString = PaperlessWireMockProducer.fileToString("PaperlessCreateDocumentResult.json")
        val document: JsonNode = ObjectMapper().readTree(documentString)

        Mockito
            .`when`(
                paperlessService.createCompanyMasterData(emailClaim, nameClaim, contract.contractType),
            ).thenReturn(Uni.createFrom().item(document))

        Mockito
            .`when`(paperlessService.verifyTemplateName(any()))
            .thenReturn(true)

        asserter.assertThat(
            {
                setupCleanDB().chain { _ ->
                    Uni.createFrom().item {
                        given()
                            .`when`()
                            .contentType(ContentType.JSON)
                            .body(contractRequest)
                            .post("/")
                            .then()
                            .statusCode(200)
                            .extract()
                            .`as`(Contract::class.java)
                    }
                }
            },
            {
                Mockito
                    .verify(paperlessService)
                    .createCompanyMasterData(emailClaim, nameClaim, contract.contractType)
                Assertions.assertEquals(contract, it.copy(companyId = testCompanyId))
            },
        )
    }

    @Test
    @RunOnVertxContext
    @TestSecurity(authorizationEnabled = false)
    fun testContractNotCreatedTwiceHttp(asserter: UniAsserter) {
        initJsonWebToken()
        val contractRequest = ContractRequest("dummy", null)
        val documentString = PaperlessWireMockProducer.fileToString("PaperlessCreateDocumentResult.json")
        val document: JsonNode = ObjectMapper().readTree(documentString)

        Mockito.`when`(paperlessService.verifyTemplateName(any())).thenReturn(true)

        Mockito
            .`when`(
                paperlessService.createCompanyMasterData(emailClaim, nameClaim, "dummy"),
            ).thenReturn(
                Uni
                    .createFrom()
                    .item { Thread.sleep(1000) }
                    .chain { _ -> Uni.createFrom().item(document) },
            )

        asserter.assertThat(
            {
                val uniJoinBuilder = Uni.join().builder<ValidatableResponse>()
                uniJoinBuilder.add(
                    Uni.createFrom().item {
                        given()
                            .`when`()
                            .contentType(ContentType.JSON)
                            .body(contractRequest)
                            .post("/")
                            .then()
                    },
                )
                uniJoinBuilder.add(
                    Uni
                        .createFrom()
                        .item { Thread.sleep(200) }
                        .chain { _ ->
                            Uni.createFrom().item {
                                given()
                                    .`when`()
                                    .contentType(ContentType.JSON)
                                    .body(contractRequest)
                                    .post("/")
                                    .then()
                            }
                        },
                )
                uniJoinBuilder.joinAll().andFailFast()
            },
            { list ->

                Assertions.assertEquals(listOf(200, 409), list.map { it.extract().statusCode() })
                Assertions.assertEquals(
                    "A Contract with ID dummy_$userSubjectClaim already exists",
                    list[1]
                        .extract()
                        .body()
                        .`as`(ClientError::class.java)
                        .message,
                )

                Mockito
                    .verify(paperlessService)
                    .createCompanyMasterData(emailClaim, nameClaim, "dummy")
            },
        )
    }
}
