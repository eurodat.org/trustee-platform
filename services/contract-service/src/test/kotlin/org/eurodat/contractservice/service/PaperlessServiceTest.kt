package org.eurodat.contractservice.service

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import jakarta.ws.rs.BadRequestException
import org.eurodat.contractservice.model.ContractType
import org.eurodat.contractservice.model.Participant
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class PaperlessServiceTest {
    @Inject
    internal lateinit var paperlessService: PaperlessService

    @Test
    @RunOnVertxContext
    fun testSubmissionApi(asserter: UniAsserter) {
        asserter.assertThat(
            {
                paperlessService.createCompanyMasterData(
                    "test@example.com",
                    "Test User",
                    ContractType.ONBOARDING.type,
                )
            },
            { document ->
                Assertions.assertNotNull(document["id"])
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testTermsAndConditionsDocument(asserter: UniAsserter) {
        val participants =
            listOf(
                Participant(
                    firstName = "Participant",
                    lastName = "One",
                    email = "participantone@example.com",
                    role = "Role1",
                    status = "status",
                ),
                Participant(
                    firstName = "Participant",
                    lastName = "Two",
                    email = "participanttwo@example.com",
                    role = "Role2",
                    status = "status",
                ),
            )

        asserter.assertThat(
            {
                paperlessService.createTermsAndConditionsDocument(
                    participants,
                    "TERMS_AND_CONDITIONS_2",
                )
            },
            { document ->
                Assertions.assertNotNull(document["id"])
            },
        )
    }

    @Test
    fun testInvalidNumberOfSigners() {
        // Less than 1 signer
        val noParticipants = emptyList<Participant>()
        val exceptionNoSigners =
            Assertions.assertThrows(BadRequestException::class.java) {
                paperlessService.createTermsAndConditionsDocument(noParticipants, "TERMS_AND_CONDITIONS_0").await().indefinitely()
            }
        Assertions.assertEquals("Invalid number of signatories: 0", exceptionNoSigners.message)

        // More than 5 signers
        val tooManyParticipants =
            listOf(
                Participant(firstName = "Participant", lastName = "One", email = "participantone@example.com", role = "Role", status = "status"),
                Participant(firstName = "Participant", lastName = "Two", email = "participanttwo@example.com", role = "Role", status = "status"),
                Participant(
                    firstName = "Participant",
                    lastName = "Three",
                    email = "participantthree@example.com",
                    role = "Role",
                    status = "status",
                ),
                Participant(
                    firstName = "Participant",
                    lastName = "Four",
                    email = "participantfour@example.com",
                    role = "Role",
                    status = "status",
                ),
                Participant(
                    firstName = "Participant",
                    lastName = "Five",
                    email = "participantfive@example.com",
                    role = "Role",
                    status = "status",
                ),
                Participant(firstName = "Participant", lastName = "Six", email = "participantsix@example.com", role = "Role", status = "status"),
            )
        val exceptionTooManySigners =
            Assertions.assertThrows(BadRequestException::class.java) {
                paperlessService.createTermsAndConditionsDocument(tooManyParticipants, "TERMS_AND_CONDITIONS_6").await().indefinitely()
            }
        Assertions.assertEquals("Invalid number of signatories: 6", exceptionTooManySigners.message)
    }

    @Test
    fun testVerifyTemplateName() {
        Assertions.assertTrue(paperlessService.verifyTemplateName(ContractType.ONBOARDING.type))
        Assertions.assertTrue(paperlessService.verifyTemplateName(ContractType.TNC_2.type))
        Assertions.assertTrue(paperlessService.verifyTemplateName(ContractType.TNC_3.type))
        Assertions.assertTrue(paperlessService.verifyTemplateName(ContractType.TNC_4.type))
        Assertions.assertTrue(paperlessService.verifyTemplateName(ContractType.TNC_5.type))
        Assertions.assertFalse(paperlessService.verifyTemplateName("INVALID_TEMPLATE_NAME"))
    }

    @Test
    fun testMapTemplateNameToId() {
        Assertions.assertEquals(14509, paperlessService.mapTemplateNameToId(ContractType.ONBOARDING.type))
        Assertions.assertEquals(17337, paperlessService.mapTemplateNameToId(ContractType.TNC_1.type))
        Assertions.assertEquals(17226, paperlessService.mapTemplateNameToId(ContractType.TNC_5.type))

        val exception =
            assertThrows<IllegalArgumentException> {
                paperlessService.mapTemplateNameToId("INVALID_TEMPLATE_NAME")
            }
        Assertions.assertEquals("Unknown template name: INVALID_TEMPLATE_NAME", exception.message)
    }

    @Test
    fun testGetTermsAndConditionsTemplateName() {
        Assertions.assertEquals(ContractType.TNC_1.type, paperlessService.getTermsAndConditionsTemplateName(1))
        Assertions.assertEquals(ContractType.TNC_2.type, paperlessService.getTermsAndConditionsTemplateName(2))
        Assertions.assertEquals(ContractType.TNC_3.type, paperlessService.getTermsAndConditionsTemplateName(3))
        Assertions.assertEquals(ContractType.TNC_4.type, paperlessService.getTermsAndConditionsTemplateName(4))
        Assertions.assertEquals(ContractType.TNC_5.type, paperlessService.getTermsAndConditionsTemplateName(5))

        val exception =
            assertThrows<IllegalArgumentException> {
                paperlessService.getTermsAndConditionsTemplateName(0)
            }
        Assertions.assertEquals("Invalid number of signatories: 0", exception.message)

        val exception2 =
            assertThrows<IllegalArgumentException> {
                paperlessService.getTermsAndConditionsTemplateName(6)
            }
        Assertions.assertEquals("Invalid number of signatories: 6", exception2.message)
    }
}
