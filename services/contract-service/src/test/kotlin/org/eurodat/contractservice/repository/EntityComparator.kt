package org.eurodat.contractservice.repository

import org.eurodat.contractservice.entity.ContractEntity
import org.junit.jupiter.api.Assertions

class EntityComparator {
    companion object {
        /*
         * This method checks if the actual ContractEntity is the same as the expected ContractEntity except
         * for the id, validFrom, and contractValidFrom fields.
         */
        fun assertEquals(
            expected: ContractEntity,
            actual: ContractEntity,
        ) {
            Assertions.assertEquals(expected.businessId, actual.businessId, "businessId fields are not equal")
            Assertions.assertEquals(expected.contractType, actual.contractType, "contractType fields are not equal")
            Assertions.assertEquals(expected.ownerId, actual.ownerId, "ownerID fields are not equal")
            Assertions.assertEquals(expected.appId, actual.appId, "appId fields are not equal")
            Assertions.assertEquals(expected.contractValidTo, actual.contractValidTo, "contractValidTo fields are not equal")
            Assertions.assertEquals(expected.status, actual.status, "Status fields are not equal")
        }
    }
}
