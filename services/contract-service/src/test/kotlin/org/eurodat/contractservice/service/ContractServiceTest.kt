package org.eurodat.contractservice.service

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.test.InjectMock
import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import jakarta.ws.rs.ClientErrorException
import jakarta.ws.rs.WebApplicationException
import org.eurodat.client.contractservice.model.ContractRequest
import org.eurodat.contractservice.PostgresTestContainer
import org.eurodat.contractservice.model.Contract
import org.eurodat.contractservice.model.ContractState
import org.eurodat.contractservice.model.ContractType
import org.eurodat.contractservice.model.Participant
import org.eurodat.contractservice.repository.ContractRepository
import org.hibernate.exception.ConstraintViolationException
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.argThat
import org.mockito.kotlin.times
import org.mockito.kotlin.whenever
import java.sql.SQLException

@QuarkusTest
@QuarkusTestResource(PostgresTestContainer::class)
class ContractServiceTest {
    @InjectMock
    private lateinit var contractRepository: ContractRepository

    @Inject
    private lateinit var contractService: ContractService

    private val objectMapper = ObjectMapper()

    private val businessId = "${ContractType.ONBOARDING.type}_userSubject"
    private val onboardingContractRequest = ContractRequest(ContractType.ONBOARDING.type, "appId")
    private val testCompanyId = "123456789L"
    private val mockedDocument: JsonNode =
        objectMapper.createObjectNode().apply {
            put("id", 12345)
            put("name", "Test Document")
            put("state", "dispatched")
            put("submission_id", 98765)
            put("completed_at", "2024-08-14T10:00:00.000Z")
        }

    private val createdContract =
        Contract(
            id = businessId,
            contractType = onboardingContractRequest.contractType,
            ownerId = "userSubject",
            appId = "appId",
            contractValidFrom = null,
            contractValidTo = null,
            status = ContractState.CREATED.status,
            submissionId = null,
            companyId = testCompanyId,
        )

    private val sentContract =
        Contract(
            id = businessId,
            contractType = onboardingContractRequest.contractType,
            ownerId = "userSubject",
            appId = "appId",
            contractValidFrom = null,
            contractValidTo = null,
            status = ContractState.SENT.status,
            submissionId = 98765,
            companyId = testCompanyId,
        )

    @Test
    @RunOnVertxContext
    fun testCreateNewContractSuccess(asserter: UniAsserter) {
        whenever(contractRepository.createContract(any())).thenReturn(
            Uni.createFrom().item { createdContract },
        )
        whenever(contractRepository.setContractStatusToSent(any(), any())).thenReturn(
            Uni.createFrom().item { sentContract },
        )

        asserter.assertThat(
            {
                contractService.createNewContract(
                    "userSubject",
                    onboardingContractRequest,
                    testCompanyId,
                    { Uni.createFrom().item(mockedDocument) },
                )
            },
            { contract ->
                Assertions.assertNotNull(contract)
                Assertions.assertEquals(ContractState.SENT.status, contract.status)
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testCreateNewContractConstraintViolation(asserter: UniAsserter) {
        whenever(
            contractRepository.createContract(any()),
        ).thenReturn(Uni.createFrom().failure(ConstraintViolationException("Test", SQLException("Test SQL exception"), "constraint violation")))

        asserter.assertFailedWith(
            {
                contractService.createNewContract(
                    "userSubject",
                    onboardingContractRequest,
                    testCompanyId,
                    { Uni.createFrom().item(mockedDocument) },
                )
            },
            { exception ->
                Assertions.assertInstanceOf(ClientErrorException::class.java, exception)
                Assertions.assertEquals("A Contract with ID $businessId already exists", exception.message)
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testCreateDocumentAndSetStatusToSent(asserter: UniAsserter) {
        whenever(contractRepository.createContract(any())).thenReturn(
            Uni.createFrom().item(createdContract),
        )
        whenever(contractRepository.setContractStatusToSent(any(), any())).thenReturn(
            Uni.createFrom().item(sentContract),
        )

        asserter.assertThat(
            {
                contractService.createNewContract(
                    "userSubject",
                    onboardingContractRequest,
                    testCompanyId,
                    { Uni.createFrom().item(mockedDocument) },
                )
            },
            { contract ->
                Assertions.assertEquals(ContractState.SENT.status, contract.status)
                Mockito.verify(contractRepository).setContractStatusToSent(createdContract, 98765)
                Mockito.verify(contractRepository, times(0)).terminateContract(any())
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testSetStatusToSentFailureTerminatesContract(asserter: UniAsserter) {
        whenever(contractRepository.createContract(any())).thenReturn(
            Uni.createFrom().item { createdContract },
        )
        whenever(
            contractRepository.setContractStatusToSent(any(), any()),
        ).thenReturn(Uni.createFrom().failure(RuntimeException("Failed to set status to SENT")))

        whenever(contractRepository.terminateContract(any())).thenReturn(Uni.createFrom().nullItem())

        asserter.assertFailedWith(
            {
                contractService.createNewContract(
                    "userSubject",
                    onboardingContractRequest,
                    testCompanyId,
                    { Uni.createFrom().item(mockedDocument) },
                )
            },
            { exception ->
                Assertions.assertInstanceOf(RuntimeException::class.java, exception)
                Assertions.assertEquals("Failed to set status to SENT", exception.message)

                Mockito.verify(contractRepository).terminateContract(businessId)
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testDocumentSupplierFailureTerminatesContract(asserter: UniAsserter) {
        whenever(contractRepository.createContract(any())).thenReturn(
            Uni.createFrom().item { createdContract },
        )

        whenever(contractRepository.terminateContract(any())).thenReturn(Uni.createFrom().nullItem())

        asserter.assertFailedWith(
            {
                contractService.createNewContract("userSubject", onboardingContractRequest, testCompanyId, {
                    Uni.createFrom().failure(RuntimeException("Document creation failed"))
                })
            },
            { exception ->
                Assertions.assertInstanceOf(RuntimeException::class.java, exception)
                Assertions.assertEquals("Document creation failed", exception.message)

                Mockito.verify(contractRepository).terminateContract(businessId)
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testSubmissionIdNodeMissing(asserter: UniAsserter) {
        val mockedDocumentWithoutSubmissionId: JsonNode =
            objectMapper.createObjectNode().apply {
                put("id", 12345)
                put("name", "Test Document")
                put("state", "dispatched")
            }

        whenever(contractRepository.createContract(any())).thenReturn(
            Uni.createFrom().item { createdContract },
        )

        asserter.assertFailedWith(
            {
                contractService.createNewContract(
                    "userSubject",
                    onboardingContractRequest,
                    testCompanyId,
                    { Uni.createFrom().item(mockedDocumentWithoutSubmissionId) },
                )
            },
            { exception ->
                Assertions.assertInstanceOf(WebApplicationException::class.java, exception)
                Assertions.assertEquals(
                    "Response from external API could not be processed: Missing 'submission_id' in response body.",
                    exception.message,
                )
                Mockito.verify(contractRepository).terminateContract(businessId)
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testUpdateParticipantsFromResponse(asserter: UniAsserter) {
        val mockedDocumentWithParticipants: JsonNode =
            objectMapper.createObjectNode().apply {
                put("submission_id", 98765)
                val participantsNode = putArray("participants")
                val participantOne = participantsNode.addObject()
                participantOne.put("email", "one@example.com")
                participantOne.put("id", 111111)

                val participantTwo = participantsNode.addObject()
                participantTwo.put("email", "two@example.com")
                participantTwo.put("id", 222222)
            }

        val contractWithParticipants =
            createdContract.copy(
                participants =
                    listOf(
                        Participant(firstName = "Dummy1", lastName = "User1", email = "one@example.com", role = "Signer-1", status = "sent"),
                        Participant(firstName = "Dummy2", lastName = "User2", email = "two@example.com", role = "Signer-2", status = "sent"),
                    ),
            )

        val contractWithParticipantsAndIds =
            contractWithParticipants.copy(
                participants =
                    contractWithParticipants.participants.map { participant ->
                        when (participant.email) {
                            "one@example.com" -> participant.copy(documentId = 111111)
                            "two@example.com" -> participant.copy(documentId = 222222)
                            else -> participant
                        }
                    },
            )

        whenever(contractRepository.createContract(any())).thenReturn(
            Uni.createFrom().item { contractWithParticipants },
        )
        whenever(
            contractRepository.setContractStatusToSent(
                argThat { contract ->
                    contract.participants == contractWithParticipantsAndIds.participants
                },
                any(),
            ),
        ).thenReturn(
            Uni.createFrom().item { contractWithParticipantsAndIds.copy(status = ContractState.SENT.status) },
        )

        asserter.assertThat(
            {
                contractService.createNewContract(
                    "userSubject",
                    onboardingContractRequest,
                    testCompanyId,
                    { Uni.createFrom().item(mockedDocumentWithParticipants) },
                )
            },
            { contract ->
                val updatedParticipants = contract.participants

                Assertions.assertEquals(2, updatedParticipants.size)
                Assertions.assertEquals(111111, updatedParticipants.find { it.email == "one@example.com" }?.documentId)
                Assertions.assertEquals(222222, updatedParticipants.find { it.email == "two@example.com" }?.documentId)
            },
        )
    }
}
