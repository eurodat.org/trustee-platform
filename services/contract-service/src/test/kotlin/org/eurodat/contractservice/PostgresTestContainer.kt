package org.eurodat.contractservice

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager
import org.testcontainers.containers.PostgreSQLContainer

class PostgresTestContainer : QuarkusTestResourceLifecycleManager {
    private lateinit var container: PostgreSQLContainer<*>

    override fun start(): MutableMap<String, String> {
        container =
            PostgreSQLContainer("postgres:16")
                .withDatabaseName("test_db")
                .withUsername("postgres")
                .withPassword("pwd")
        container.start()

        val contractServiceUrl = container.getJdbcUrl().replace("jdbc", "vertx-reactive")

        return mutableMapOf(
            "quarkus.datasource.reactive.url" to contractServiceUrl,
            "quarkus.datasource.username" to container.username,
            "quarkus.datasource.password" to container.password,
        )
    }

    override fun stop() {
        container.close()
    }
}
