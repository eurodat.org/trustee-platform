package org.eurodat.contractservice.paperless

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.contractservice.model.ContractType
import org.eurodat.contractservice.model.Participant
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

@QuarkusTest
class PaperlessMockCreateDocumentRequestTest {
    @Inject
    @RestClient
    lateinit var documentApi: DocumentApi

    @Test
    @RunOnVertxContext
    fun createDocumentExpandParticipantsTest(uniAsserter: UniAsserter) {
        val request =
            createDocumentRequest()

        uniAsserter.assertThat(
            {
                documentApi.createDocument(listOf("participants"), "2023-06-23", request)
            },
            { response ->
                Assertions.assertNotNull(response)
                Assertions.assertTrue(response["name"].asText().contains("TERMS_AND_CONDITIONS_1"))
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun createDocumentWithoutExpandTest(uniAsserter: UniAsserter) {
        val request =
            createDocumentRequest()

        uniAsserter.assertThat(
            {
                documentApi.createDocument(emptyList(), "2023-06-23", request)
            },
            { response ->
                Assertions.assertNotNull(response)
                Assertions.assertTrue(response["name"].asText().contains("TestDocument"))
            },
        )
    }

    private fun createDocumentRequest(): JsonNode {
        val objectMapper = ObjectMapper()
        val participants =
            listOf(
                Participant(
                    firstName = "Dummy",
                    lastName = "User",
                    email = "one@example.com",
                    role = "Admin",
                    status = "created",
                ),
            )

        val participantsNode = objectMapper.createObjectNode()
        participants.forEachIndexed { index, participant ->
            val participantNode =
                objectMapper.createObjectNode().apply {
                    put("name", "${participant.firstName} ${participant.lastName}")
                    put("email", participant.email)
                }
            participantsNode.set<ObjectNode>("Signer-${index + 1}", participantNode)
        }

        val requestBody =
            objectMapper.createObjectNode().apply {
                put("workspace_id", 2981)
                put("template_id", 17337)
                put(
                    "name",
                    "${ContractType.TNC_1.type} for ${participants.joinToString(
                        ", ",
                    ) { "${it.firstName} ${it.lastName}" }} at ${LocalDateTime.now()}",
                )
                put("state", "dispatched")
                set<JsonNode>("participants", participantsNode)
                set<JsonNode>("tokens", objectMapper.createObjectNode().put("token1", "token-value"))
            }

        return requestBody
    }
}
