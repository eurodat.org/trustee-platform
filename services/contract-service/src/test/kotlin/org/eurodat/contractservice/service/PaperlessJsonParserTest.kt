package org.eurodat.contractservice.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.NotFoundException
import org.eurodat.contractservice.model.Company
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.ZonedDateTime

@QuarkusTest
class PaperlessJsonParserTest {
    @Inject
    internal lateinit var paperlessJsonParser: PaperlessJsonParser

    private val objectMapper = ObjectMapper()

    val testAddress =
        Company.Address(
            streetAddress = "Some Street",
            zip = "12345",
            town = "Town",
            country = "Country",
        )

    val testCompany =
        Company(
            id = "123456789L",
            name = "Test Company",
            address = testAddress,
            registerLocation = "Location",
            registerNo = "RegNo",
            vatId = "VatId",
            nationalTaxId = null,
        )

    @Test
    fun testParseSubmissionId() {
        val json =
            """
            {
                "object": {
                    "submittable": {
                        "submission_id": 12345
                    }
                }
            }
            """.trimIndent()
        val node = ObjectMapper().readTree(json)
        val submissionId = paperlessJsonParser.parseSubmissionIdFromDocument(node)
        Assertions.assertEquals(12345, submissionId)
    }

    @Test
    fun testParseCompletedAt() {
        val json =
            """
            {
                "object": {
                    "completed_at": "2024-01-01T10:00:00.000Z"
                }
            }
            """.trimIndent()
        val node = ObjectMapper().readTree(json)
        val completedAt = paperlessJsonParser.parseCompletedAtFromDocument(node)
        Assertions.assertEquals(ZonedDateTime.parse("2024-01-01T10:00:00.000Z"), completedAt)
    }

    @Test
    fun testParseSigners() {
        val json =
            """
            {
              "event": "submission.completed",
              "object": {
                "aggregated_submit_event_values": [
                  {"slug": "companyname_slug", "value": "Test Company"},
                  {"slug": "companydirectors_slug", "value": ["f270477f-a8b5-4816-adaa-1670f611cbc0", "f055bb11-6a9c-49dd-a83b-4855a1465854"]},
                  {"slug": "companydirectors_slug.f270477f-a8b5-4816-adaa-1670f611cbc0.companydirectorsfirstname_slug", "value": "Participant"},
                  {"slug": "companydirectors_slug.f270477f-a8b5-4816-adaa-1670f611cbc0.companydirectorslastname_slug", "value": "One"},
                  {"slug": "companydirectors_slug.f270477f-a8b5-4816-adaa-1670f611cbc0.email_slug", "value": "participantone@example.com"},
                  {"slug": "companydirectors_slug.f270477f-a8b5-4816-adaa-1670f611cbc0.radio3", "mapped_value": "Partner"},
                  {"slug": "companydirectors_slug.f055bb11-6a9c-49dd-a83b-4855a1465854.companydirectorsfirstname_slug", "value": "Participant"},
                  {"slug": "companydirectors_slug.f055bb11-6a9c-49dd-a83b-4855a1465854.companydirectorslastname_slug", "value": "Two"},
                  {"slug": "companydirectors_slug.f055bb11-6a9c-49dd-a83b-4855a1465854.email_slug", "value": "participanttwo@example.com"},
                  {"slug": "companydirectors_slug.f055bb11-6a9c-49dd-a83b-4855a1465854.radio3", "mapped_value": "Admin"}
                ]
              }
            }
            """.trimIndent()
        val node = ObjectMapper().readTree(json)
        val signers = paperlessJsonParser.parseSignersFromDocument(node)

        Assertions.assertAll(
            "signers",
            { Assertions.assertEquals(2, signers.size) },
            { Assertions.assertEquals("Participant", signers[0].firstName) },
            { Assertions.assertEquals("One", signers[0].lastName) },
            { Assertions.assertEquals("participantone@example.com", signers[0].email) },
            { Assertions.assertEquals("Partner", signers[0].role) },
            { Assertions.assertEquals("Participant", signers[1].firstName) },
            { Assertions.assertEquals("Two", signers[1].lastName) },
            { Assertions.assertEquals("participanttwo@example.com", signers[1].email) },
            { Assertions.assertEquals("Admin", signers[1].role) },
        )
    }

    @Test
    fun testParseSignersFromSubmissionWebhookBody() {
        val fileName = "submissionWebhookBody.json"
        val jsonNode = readJson(fileName)
        val signers = paperlessJsonParser.parseSignersFromDocument(jsonNode)
        Assertions.assertEquals(1, signers.size)
        Assertions.assertEquals("dummyFirstNameC dummyLastNameC", "${signers[0].firstName} ${signers[0].lastName}")
        Assertions.assertEquals("dummyfirstnamec.dummylastnamec@d-fine.com", signers[0].email)
    }

    @Test
    fun testCreateSignerFromIdWhenSignerDetailsMissing() {
        val json =
            """
            {
              "object": {
                "aggregated_submit_event_values": [
                  {
                    "slug": "companydirectors_slug",
                    "value": [
                      "f270477f-a8b5-4816-adaa-1670f611cbc0"
                    ]
                  }
                ]
              }
            }
            """.trimIndent()
        val node = objectMapper.readTree(json)

        val exception =
            assertThrows<NotFoundException> {
                paperlessJsonParser.parseSignersFromDocument(node)
            }

        Assertions.assertEquals("Missing signer details for userSubject: f270477f-a8b5-4816-adaa-1670f611cbc0", exception.message)
    }

    @Test
    fun testExtractSubjectIdsNotFound() {
        val json = """{"object": {"aggregated_submit_event_values":[{"slug": "other_slug", "value": "some_value"}]}}"""
        val node = objectMapper.readTree(json)

        val exception =
            assertThrows<NotFoundException> {
                paperlessJsonParser.parseSignersFromDocument(node)
            }

        Assertions.assertEquals("No director IDs found in the provided JSON.", exception.message)
    }

    @Test
    fun testParseSubmissionIdFromDocumentNonIntegerSubmissionId() {
        val invalidJson = """{"object": {"submittable": {"submission_id": "invalid-id"}}}"""
        val invalidNode = objectMapper.readTree(invalidJson)

        val exception =
            assertThrows<BadRequestException> {
                paperlessJsonParser.parseSubmissionIdFromDocument(invalidNode)
            }

        Assertions.assertEquals("Submission ID is not an integer", exception.message)
    }

    @Test
    fun testParseCompletedAtFromDocumentInvalidDate() {
        val invalidDateJson = """{"object": {"completed_at": "invalid-date"}}"""
        val invalidDateNode = objectMapper.readTree(invalidDateJson)

        val exception =
            assertThrows<BadRequestException> {
                paperlessJsonParser.parseCompletedAtFromDocument(invalidDateNode)
            }

        Assertions.assertTrue(exception.message!!.contains("completed_at is not a valid date"))
    }

    @Test
    fun testExtractCompanyNotFound() {
        val json = """{"object": {"aggregated_submit_event_values": [{"slug": "other_slug", "value": "some_value"}]}}"""
        val node = objectMapper.readTree(json)

        val exception =
            assertThrows<NotFoundException> {
                paperlessJsonParser.parseCompanyDataFromDocument(node, testCompany.id)
            }

        Assertions.assertEquals("No value found for slug: companyname_slug", exception.message)
    }

    @Test
    fun testParseCompanyDataFromDocument() {
        val jsonNode = readJson("paperlessOnboardingWebhookTwoSignersSubmission.json")
        val company = paperlessJsonParser.parseCompanyDataFromDocument(jsonNode, testCompany.id)

        Assertions.assertEquals("Test Company", company.name)
        Assertions.assertEquals("Test Street 2", company.address.streetAddress)
        Assertions.assertEquals("11232", company.address.zip)
        Assertions.assertEquals("FFm", company.address.town)
        Assertions.assertEquals("asd", company.address.country)
        Assertions.assertEquals("loc", company.registerLocation)
        Assertions.assertEquals("asdfkblw", company.registerNo)
        Assertions.assertEquals("de123456", company.vatId)
        Assertions.assertNull(company.nationalTaxId)
    }

    private fun readJson(fileName: String): JsonNode {
        objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
        val submissionBodyJson =
            {}
                .javaClass.classLoader
                .getResourceAsStream(fileName)
                ?.bufferedReader()
                ?.readText()
        return objectMapper.readTree(submissionBodyJson)
    }
}
