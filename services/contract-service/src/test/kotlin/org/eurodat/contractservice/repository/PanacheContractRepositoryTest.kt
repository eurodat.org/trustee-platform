package org.eurodat.contractservice.repository

import io.quarkus.test.TestReactiveTransaction
import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import org.eurodat.contractservice.PostgresTestContainer
import org.eurodat.contractservice.model.Contract
import org.eurodat.contractservice.model.ContractState
import org.eurodat.contractservice.model.ContractType
import org.eurodat.contractservice.model.Participant
import org.hibernate.exception.ConstraintViolationException
import org.hibernate.reactive.mutiny.Mutiny
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.time.Duration
import java.time.ZonedDateTime

@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@QuarkusTestResource(PostgresTestContainer::class)
class PanacheContractRepositoryTest {
    @Inject
    private lateinit var panacheContractRepository: PanacheContractRepository

    @Inject
    private lateinit var sessionFactory: Mutiny.SessionFactory

    @Inject
    private lateinit var panacheCompanyRepository: PanacheCompanyRepository

    private val appId = "mockApp"
    private val businessId = "mockBusinessId"
    private val ownerId = "mockOwner"
    private val contractType = "mockContract"
    private val status = ContractState.SENT.status
    private val submissionId = 123456789

    private lateinit var sentContract: Contract

    @BeforeEach
    fun setup() {
        sessionFactory
            .withTransaction { _ ->
                panacheCompanyRepository.createNewCompany().map { companyId ->
                    sentContract =
                        Contract(
                            id = businessId,
                            contractType = contractType,
                            ownerId = ownerId,
                            appId = appId,
                            contractValidFrom = null,
                            contractValidTo = null,
                            status = status,
                            submissionId = submissionId,
                            companyId = companyId,
                        )
                }
            }.await()
            .atMost(Duration.ofSeconds(1))
    }

    @AfterEach
    fun cleanup() {
        sessionFactory
            .withTransaction { _ ->
                panacheContractRepository.deleteAll()
            }.await()
            .atMost(Duration.ofSeconds(1))
    }

    @Test
    @TestReactiveTransaction
    @RunOnVertxContext
    fun testCreateContract(asserter: UniAsserter) {
        asserter.assertThat(
            {
                panacheContractRepository.createContract(sentContract)
            },
            { actual ->
                Assertions.assertEquals(sentContract, actual)
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testCreateContractsWithOverlappingTimeframe(asserter: UniAsserter) {
        asserter.assertFailedWith(
            {
                sessionFactory
                    .withTransaction { _ ->
                        panacheContractRepository
                            .createContract(sentContract)
                    }.chain { _ ->
                        sessionFactory.withTransaction { _ ->
                            panacheContractRepository.createContract(sentContract)
                        }
                    }
            },
            // Should violate no_overlapping_timeframes_per_business_id constraint
            ConstraintViolationException::class.java,
        )
    }

    @Test
    @TestReactiveTransaction
    @RunOnVertxContext
    fun testCreateContractsWithNonOverlappingTimeframe(asserter: UniAsserter) {
        asserter.assertNotNull {
            panacheContractRepository
                .createContract(sentContract)
                .chain { _ ->
                    panacheContractRepository.terminateContract(businessId)
                }.chain { _ ->
                    panacheContractRepository.flush()
                }.chain { _ ->
                    panacheContractRepository.createContract(sentContract)
                }
        }
    }

    @Test
    @RunOnVertxContext
    fun testTerminateContract(asserter: UniAsserter) {
        asserter.assertNull {
            panacheContractRepository
                .createContract(sentContract)
                .chain { _ ->
                    panacheContractRepository.terminateContract(businessId)
                } // .call { _ -> panacheContractRepository.flush() }
                .chain { createdContractEntity ->
                    panacheContractRepository.findContract(createdContractEntity.id)
                }
        }
    }

    @Test
    @TestReactiveTransaction
    @RunOnVertxContext
    fun testGetAllContractsByOwnerID(asserter: UniAsserter) {
        val secondBusinessId = businessId + "2"
        val thirdBusinessId = businessId + "3"
        val altOwnerId = ownerId + "2"
        val secondContract = sentContract.copy(id = secondBusinessId)
        val thirdContract = sentContract.copy(id = thirdBusinessId, ownerId = altOwnerId)

        asserter.assertThat(
            {
                panacheContractRepository
                    .createContract(sentContract)
                    .chain { _ ->
                        panacheContractRepository
                            .createContract(secondContract)
                    }.chain { _ ->
                        panacheContractRepository
                            .createContract(thirdContract)
                    }.chain { _ ->
                        panacheContractRepository.getAllContractsForOwner(ownerId)
                    }
            },
            { actual ->
                Assertions.assertEquals(2, actual.size)
                Assertions.assertEquals(sentContract, actual[0])
                Assertions.assertEquals(secondContract, actual[1])
            },
        )
    }

    @Test
    @TestReactiveTransaction
    @RunOnVertxContext
    fun testGetAllContractsByOwnerIDForNonExistentParticipant(asserter: UniAsserter) {
        val secondOwnerId = ownerId + "2"

        asserter.assertThat(
            {
                panacheContractRepository
                    .createContract(sentContract)
                    .chain { _ ->
                        panacheContractRepository.getAllContractsForOwner(secondOwnerId)
                    }
            },
            { actual ->
                Assertions.assertEquals(0, actual.size)
            },
        )
    }

    @Test
    @TestReactiveTransaction
    @RunOnVertxContext
    fun testFindContract(asserter: UniAsserter) {
        asserter.assertThat(
            {
                panacheContractRepository
                    .createContract(sentContract)
                    .chain { createdContract ->
                        panacheContractRepository.findContract(createdContract.id)
                    }
            },
            { actual -> Assertions.assertEquals(sentContract, actual) },
        )
    }

    @Test
    @TestReactiveTransaction
    @RunOnVertxContext
    fun testFindContractBySubmissionId(asserter: UniAsserter) {
        asserter.assertThat(
            {
                panacheContractRepository
                    .createContract(sentContract)
                    .chain { contractEntity ->
                        panacheContractRepository.findContractBySubmissionId(contractEntity.submissionId!!)
                    }
            },
            { actual -> Assertions.assertEquals(sentContract, actual) },
        )
    }

    @Test
    @TestReactiveTransaction
    @RunOnVertxContext
    fun testFindContractWithTerminatedContract(asserter: UniAsserter) {
        asserter.assertNull {
            panacheContractRepository
                .createContract(sentContract)
                .chain { createdContract ->
                    panacheContractRepository.terminateContract(businessId).chain { _ ->
                        panacheContractRepository.flush().chain { _ ->
                            panacheContractRepository.findContract(createdContract.id)
                        }
                    }
                }
        }
    }

    @Test
    @TestReactiveTransaction
    @RunOnVertxContext
    fun testFindContractWithRenewedContract(asserter: UniAsserter) {
        asserter.assertThat(
            {
                panacheContractRepository
                    .createContract(sentContract)
                    .chain { _ ->
                        panacheContractRepository.terminateContract(businessId)
                    }.chain { _ ->
                        panacheContractRepository.flush()
                    }.chain { _ ->
                        panacheContractRepository.createContract(sentContract)
                    }.chain { createdContract ->
                        panacheContractRepository.findContract(createdContract.id)
                    }
            },
            { actual ->
                Assertions.assertEquals(sentContract, actual)
            },
        )
    }

    @Test
    @TestReactiveTransaction
    @RunOnVertxContext
    fun testSetContractStatusToSent(asserter: UniAsserter) {
        asserter.assertThat(
            {
                panacheContractRepository
                    .createContract(sentContract.copy(status = ContractState.CREATED.status))
                    .chain { contract ->
                        panacheContractRepository.setContractStatusToSent(contract, submissionId)
                    }
            },
            { actual ->
                Assertions.assertEquals(sentContract, actual)
            },
        )
    }

    @Test
    @TestReactiveTransaction
    @RunOnVertxContext
    fun testSetContractStatusToSigned(asserter: UniAsserter) {
        val contractValidFrom = ZonedDateTime.now()
        asserter.assertThat(
            {
                panacheContractRepository
                    .createContract(sentContract)
                    .chain { _ ->
                        panacheContractRepository.setContractStatusToSigned(
                            businessId,
                            contractValidFrom,
                        )
                    }
            },
            { actual ->
                val signedContract = sentContract.copy(status = ContractState.SIGNED.status, contractValidFrom = contractValidFrom)
                Assertions.assertEquals(signedContract, actual)
            },
        )
    }

    @Test
    @TestReactiveTransaction
    @RunOnVertxContext
    fun testUpdateContractWithParticipants(asserter: UniAsserter) {
        val participants =
            listOf(
                Participant(firstName = "John", lastName = "Doe", email = "john.doe@example.com", role = "Signer", status = "status"),
                Participant(firstName = "Jane", lastName = "Doe", email = "jane.doe@example.com", role = "Signer", status = "status"),
            )

        asserter.assertThat(
            {
                panacheContractRepository
                    .createContract(sentContract)
                    .chain { createdContract ->
                        panacheContractRepository.updateContractWithParticipants(createdContract.id, participants)
                    }.chain { updatedContractEntity ->
                        panacheContractRepository.findContract(updatedContractEntity.id)
                    }
            },
            { actual ->
                Assertions.assertEquals(sentContract.id, actual?.id)
                Assertions.assertEquals(participants.size, actual?.participants?.size)
                actual?.participants?.forEachIndexed { index, participant ->
                    Assertions.assertEquals(participants[index].firstName, participant.firstName)
                    Assertions.assertEquals(participants[index].lastName, participant.lastName)
                    Assertions.assertEquals(participants[index].email, participant.email)
                    Assertions.assertEquals(participants[index].role, participant.role)
                }
            },
        )
    }

    @Test
    @TestReactiveTransaction
    @RunOnVertxContext
    fun testTerminateAllContractsForOwnerIfOnboarding(asserter: UniAsserter) {
        val testOwnerId = "12345ID"
        val onboardingContract =
            sentContract.copy(
                id = "onboardingContractId",
                contractType = ContractType.ONBOARDING.type,
                ownerId = testOwnerId,
            )

        val otherContract =
            sentContract.copy(
                id = "otherContractId",
                contractType = "OTHER_CONTRACT_TYPE",
                ownerId = testOwnerId,
            )
        val anotherContract =
            sentContract.copy(
                id = "anotherContractId",
                contractType = "ANOTHER_CONTRACT_TYPE",
                ownerId = testOwnerId,
            )

        asserter.assertThat(
            {
                panacheContractRepository
                    .createContract(onboardingContract)
                    .chain { _ -> panacheContractRepository.createContract(otherContract) }
                    .chain { _ -> panacheContractRepository.createContract(anotherContract) }
                    .chain { _ -> panacheContractRepository.getAllContractsForOwner(testOwnerId) }
            },
            { contracts ->
                Assertions.assertEquals(3, contracts.size)
            },
        )

        asserter.assertThat(
            {
                panacheContractRepository
                    .terminateContract(onboardingContract.id)
                    .chain { terminatedContract ->
                        panacheContractRepository.terminateAllContractsForOwnerIfOnboarding(terminatedContract)
                    }.chain { _ -> panacheContractRepository.getAllContractsForOwner(testOwnerId) }
            },
            { contracts ->
                Assertions.assertEquals(0, contracts.size)
            },
        )
    }
}
