package org.eurodat.contractservice.repository

import io.quarkus.test.TestReactiveTransaction
import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import jakarta.ws.rs.NotFoundException
import org.eurodat.contractservice.PostgresTestContainer
import org.eurodat.contractservice.model.Company
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@QuarkusTest
@QuarkusTestResource(PostgresTestContainer::class)
class PanacheCompanyRepositoryTest {
    @Inject
    lateinit var panacheCompanyRepository: PanacheCompanyRepository

    @Test
    @TestReactiveTransaction
    @RunOnVertxContext
    fun testCreateNewCompany(asserter: UniAsserter) {
        asserter.assertThat(
            {
                panacheCompanyRepository.createNewCompany()
            },
            { companyId ->
                Assertions.assertFalse(companyId.isNullOrBlank(), "Company ID is null or empty.")
            },
        )
    }

    @Test
    @TestReactiveTransaction
    @RunOnVertxContext
    fun testUpdateCompany(asserter: UniAsserter) {
        asserter.assertThat(
            {
                panacheCompanyRepository
                    .createNewCompany()
                    .map { companyBusinessId ->
                        Company(
                            id = companyBusinessId,
                            name = "Updated Company Name",
                            address =
                                Company.Address(
                                    streetAddress = "Updated Street",
                                    zip = "12345",
                                    town = "Updated Town",
                                    country = "Updated Country",
                                ),
                            registerLocation = "Updated Location",
                            registerNo = "Updated Reg No",
                            vatId = "Updated VAT",
                            nationalTaxId = "Updated Tax ID",
                        )
                    }.flatMap { updatedCompany ->
                        panacheCompanyRepository.updateCompany(updatedCompany)
                    }
            },
            { updatedCompany ->
                Assertions.assertEquals("Updated Company Name", updatedCompany.name)
                Assertions.assertEquals("Updated Street", updatedCompany.address.streetAddress)
                Assertions.assertEquals("12345", updatedCompany.address.zip)
                Assertions.assertEquals("Updated Town", updatedCompany.address.town)
                Assertions.assertEquals("Updated Country", updatedCompany.address.country)
                Assertions.assertEquals("Updated Location", updatedCompany.registerLocation)
                Assertions.assertEquals("Updated Reg No", updatedCompany.registerNo)
                Assertions.assertEquals("Updated VAT", updatedCompany.vatId)
                Assertions.assertEquals("Updated Tax ID", updatedCompany.nationalTaxId)
            },
        )
    }

    @Test
    @TestReactiveTransaction
    @RunOnVertxContext
    fun testUpdateCompanyNotFound(asserter: UniAsserter) {
        asserter.assertFailedWith(
            {
                val nonExistentCompany =
                    Company(
                        id = "id",
                        name = "Non-existent Company",
                        address =
                            Company.Address(
                                streetAddress = "Non-existent Street",
                                zip = "00000",
                                town = "Non-existent Town",
                                country = "Non-existent Country",
                            ),
                        registerLocation = "Non-existent Location",
                        registerNo = "Non-existent Reg No",
                        vatId = "Non-existent VAT",
                        nationalTaxId = "Non-existent Tax ID",
                    )
                panacheCompanyRepository.updateCompany(nonExistentCompany)
            },
            NotFoundException::class.java,
        )
    }
}
