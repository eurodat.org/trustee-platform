package org.eurodat.contractservice.entity

import io.quarkus.hibernate.reactive.panache.kotlin.PanacheEntityBase
import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.OneToMany
import jakarta.persistence.Table
import java.time.ZonedDateTime

@Entity
@Table(name = "contract")
data class ContractEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null,
    @Column(name = "business_id", nullable = false)
    val businessId: String,
    @Column(name = "valid_from", nullable = false)
    var validFrom: ZonedDateTime?,
    @Column(name = "valid_to")
    var validTo: ZonedDateTime?,
    @Column(name = "contract_type", nullable = false)
    val contractType: String,
    @Column(name = "owner_id", nullable = false)
    val ownerId: String,
    @Column(name = "app_id", nullable = true)
    val appId: String?,
    @Column(name = "contract_valid_from")
    val contractValidFrom: ZonedDateTime?,
    @Column(name = "contract_valid_to")
    var contractValidTo: ZonedDateTime?,
    @Column(name = "status", nullable = false)
    var status: String,
    @Column(name = "submission_id", nullable = true)
    var submissionId: Int?,
    @Column(name = "company_business_id", nullable = false)
    var companyId: String,
    @OneToMany(
        mappedBy = "contract",
        fetch = FetchType.EAGER,
        cascade = [CascadeType.ALL],
        targetEntity = ParticipantEntity::class,
        orphanRemoval = true,
    )
    var participants: List<ParticipantEntity> = listOf(),
) : PanacheEntityBase {
    override fun toString(): String {
        return "ContractEntity(id=$id)" // avoid circular references in debugger
    }
}
