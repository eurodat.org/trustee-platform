package org.eurodat.contractservice.service

import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.ws.rs.BadRequestException
import org.eurodat.client.contractservice.model.ContractRequest
import org.eurodat.contractservice.model.Contract
import org.eurodat.contractservice.model.Participant
import org.eurodat.contractservice.repository.CompanyRepository

@ApplicationScoped
class PaperlessContractService {
    @Inject
    internal lateinit var paperlessService: PaperlessService

    @Inject
    internal lateinit var contractService: ContractService

    @Inject
    internal lateinit var companyRepository: CompanyRepository

    fun createNewOnboardingContract(
        email: String,
        name: String,
        userSubject: String,
        contractRequest: ContractRequest,
    ): Uni<Contract> =
        verifyPaperlessTemplate(contractRequest.contractType)
            .chain { _ ->
                companyRepository.createNewCompany()
            }.chain { companyId ->
                contractService.createNewContract(
                    userSubject,
                    contractRequest,
                    companyId,
                    { paperlessService.createCompanyMasterData(email, name, contractRequest.contractType) },
                )
            }

    fun createNewTermsAndConditionsContract(
        ownerId: String,
        contractRequest: ContractRequest,
        companyId: String,
        participants: List<Participant>,
    ): Uni<Contract> {
        if (participants.isEmpty()) {
            return Uni.createFrom().failure(BadRequestException("A list of signers must be provided for TERMS_AND_CONDITIONS"))
        }

        return verifyPaperlessTemplate(contractRequest.contractType)
            .chain { _ ->
                contractService.createNewContract(
                    ownerId,
                    contractRequest,
                    companyId,
                    { paperlessService.createTermsAndConditionsDocument(participants, contractRequest.contractType) },
                    participants,
                )
            }
    }

    private fun verifyPaperlessTemplate(contractType: String): Uni<Unit> =
        if (!paperlessService.verifyTemplateName(contractType)) {
            Uni.createFrom().failure(BadRequestException("Contract type $contractType not found"))
        } else {
            Uni.createFrom().item(Unit)
        }
}
