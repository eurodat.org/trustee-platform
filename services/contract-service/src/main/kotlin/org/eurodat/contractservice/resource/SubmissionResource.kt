package org.eurodat.contractservice.resource

import com.fasterxml.jackson.databind.JsonNode
import io.quarkus.security.UnauthorizedException
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.replaceWithUnit
import jakarta.inject.Inject
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.QueryParam
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eurodat.client.contractservice.model.ContractRequest
import org.eurodat.contractservice.model.Contract
import org.eurodat.contractservice.model.ContractState
import org.eurodat.contractservice.model.ContractType
import org.eurodat.contractservice.repository.CompanyRepository
import org.eurodat.contractservice.repository.ContractRepository
import org.eurodat.contractservice.service.PaperlessContractService
import org.eurodat.contractservice.service.PaperlessJsonParser
import org.eurodat.contractservice.service.PaperlessService
import org.jboss.resteasy.reactive.ResponseStatus

/**
 * The SubmissionResource class represents an API POST endpoint for updating and creating contracts depending on submission body.
 */
@Path("/submissions")
class SubmissionResource {
    @Inject
    internal lateinit var contractRepository: ContractRepository

    @Inject
    internal lateinit var paperlessService: PaperlessService

    @Inject
    internal lateinit var paperlessContractService: PaperlessContractService

    @Inject
    internal lateinit var companyRepository: CompanyRepository

    @Inject
    internal lateinit var parsingUtil: PaperlessJsonParser

    @Inject
    @ConfigProperty(name = "paperless.password")
    internal lateinit var paperlessToken: String

    /**
     * Processes a submission by validating the provided token, checking if the event in the JSON
     * response body is `submission.completed`, and updating the contract status to signed.
     * If the contract type is `ONBOARDING_COMPANY_MASTERDATA`, it triggers the creation
     * of the Terms and Conditions contract.
     *
     * @param passToken The token passed to authenticate the submission process.
     * @param submission The JSON payload containing the submission details.
     * @return A `Uni<Unit>` object on completion.
     * @throws UnauthorizedException if the provided token is invalid.
     * @throws BadRequestException if the event is not `submission.completed`.
     * @throws NotFoundException if no contract is found with the specified submission ID.
     */
    @POST
    @ResponseStatus(200)
    fun processSubmission(
        @QueryParam("token") passToken: String,
        submission: JsonNode,
    ): Uni<Unit> {
        if (passToken != paperlessToken) {
            throw UnauthorizedException("Invalid Password.")
        }
        return when (val event = submission["event"].asText()) {
            "submission.completed" -> handleSubmissionCompleted(submission)
            "participant.completed" -> handleParticipantCompleted(submission)
            else -> throw BadRequestException("Invalid event type: $event. Expected submission.completed or participant.completed.")
        }
    }

    private fun handleSubmissionCompleted(submission: JsonNode): Uni<Unit> {
        val submissionId = parsingUtil.parseSubmissionIdFromDocument(submission)

        return contractRepository
            .findContractBySubmissionId(submissionId)
            .onItem()
            .ifNull()
            .failWith(NotFoundException("Contract with submissionId $submissionId not found."))
            .chain { contract ->
                contractRepository
                    .setContractStatusToSigned(
                        contract!!.id,
                        parsingUtil.parseCompletedAtFromDocument(submission),
                    ).chain { signedContract ->
                        if (contract.contractType == ContractType.ONBOARDING.type) {
                            parseDataAndCreateTermsAndConditions(submission, signedContract)
                        } else {
                            Uni.createFrom().item(Unit)
                        }
                    }
            }.replaceWith(Unit)
    }

    private fun handleParticipantCompleted(submission: JsonNode): Uni<Unit> {
        val email = submission["object"]["email"].asText()
        val participantId = submission["object"]["id"].asInt()
        val status = submission["object"]["state"].asText()
        return contractRepository
            .findTnCContractByParticipant(email, participantId)
            .chain { contract ->
                // Only update the individual participant status, if the contract is not signed by all participants already
                if (contract?.status != ContractState.SIGNED.status) {
                    val updatedParticipants =
                        contract!!.participants.map { participant ->
                            if (participant.email == email) {
                                participant.copy(status = status)
                            } else {
                                participant
                            }
                        }
                    return@chain contractRepository.updateContractWithParticipants(contract.id, updatedParticipants)
                } else {
                    Uni.createFrom().item(Unit)
                }
            }.replaceWithUnit()
    }

    private fun parseDataAndCreateTermsAndConditions(
        submission: JsonNode,
        contract: Contract,
    ): Uni<Contract> =
        companyRepository
            .updateCompany(parsingUtil.parseCompanyDataFromDocument(submission, contract.companyId))
            .chain { _ ->
                val participants = parsingUtil.parseSignersFromDocument(submission)
                val contractType = paperlessService.getTermsAndConditionsTemplateName(participants.size)
                val contractRequest =
                    ContractRequest(
                        contractType,
                        contract.appId,
                    )
                paperlessContractService.createNewTermsAndConditionsContract(
                    contract.ownerId,
                    contractRequest,
                    contract.companyId,
                    participants,
                )
            }
}
