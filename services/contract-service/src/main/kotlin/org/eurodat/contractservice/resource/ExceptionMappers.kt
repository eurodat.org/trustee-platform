package org.eurodat.contractservice.resource

import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.ws.rs.ClientErrorException
import org.jboss.resteasy.reactive.RestResponse
import org.jboss.resteasy.reactive.server.ServerExceptionMapper

class ExceptionMappers {
    @ServerExceptionMapper
    fun mapClientError(e: ClientErrorException): Uni<RestResponse<ClientError>> =
        uni { RestResponse.status(e.response.statusInfo, ClientError(e.localizedMessage)) }
}

data class ClientError(
    val message: String,
)
