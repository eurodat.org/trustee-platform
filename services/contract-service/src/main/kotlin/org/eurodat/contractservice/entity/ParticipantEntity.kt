package org.eurodat.contractservice.entity

import io.quarkus.hibernate.reactive.panache.kotlin.PanacheEntityBase
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
@Table(name = "participant")
class ParticipantEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var participantId: Long? = null,
    @Column(name = "first_name")
    val firstName: String,
    @Column(name = "last_name")
    val lastName: String,
    @Column(name = "email_address")
    val emailAddress: String,
    @Column(name = "role")
    val participantRole: String,
    @Column(name = "status")
    val status: String,
    @Column(name = "document_id")
    val documentId: Int? = null,
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "contract_id", nullable = false)
    var contract: ContractEntity,
) : PanacheEntityBase {
    override fun toString(): String {
        return "ParticipantEntity(id=$participantId)" // overwrite toString method here to avoid circular references in debugger
    }
}
