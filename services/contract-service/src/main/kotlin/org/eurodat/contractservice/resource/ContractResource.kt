package org.eurodat.contractservice.resource

import io.quarkus.security.Authenticated
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.MediaType
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eurodat.client.contractservice.model.Contract
import org.eurodat.client.contractservice.model.ContractRequest
import org.eurodat.contractservice.repository.ContractRepository
import org.eurodat.contractservice.repository.NoContractException
import org.eurodat.contractservice.service.PaperlessContractService
import org.jboss.logging.MDC
import org.jboss.resteasy.reactive.ResponseStatus
import org.jboss.resteasy.reactive.RestPath

/**
 * The ContractResource class represents the RESTful API endpoints for managing contracts.
 *
 * It provides operations for creating, retrieving, and terminating contracts.
 */
@Path("/contracts")
@Authenticated
class ContractResource {
    @Inject
    internal lateinit var contractRepository: ContractRepository

    @Inject
    internal lateinit var paperlessContractService: PaperlessContractService

    @Inject
    internal lateinit var jwt: JsonWebToken

    /**
     * Retrieves the health status of the contract service.
     *
     * @return A [Uni] containing the health status message.
     */
    @GET
    @Path("/health")
    @Produces(MediaType.TEXT_PLAIN)
    fun health(): Uni<String> = Uni.createFrom().item("Healthy!")

    /**
     * Retrieves a contract based on the specified contract ID.
     *
     * @param contractId The ID of the contract to retrieve.
     * @return A [Uni] containing the retrieved contract entity.
     */
    @GET
    @Path("/{contractId}")
    @Produces(MediaType.APPLICATION_JSON)
    fun findContract(
        @RestPath contractId: String,
    ): Uni<Contract> {
        MDC.put("ContractId", contractId)
        return contractRepository
            .findContract(contractId)
            .onItem()
            .ifNull()
            .failWith(NotFoundException("Could not find element with contract ID $contractId"))
            .map {
                it!!.toClientContract()
            }
    }

    /**
     * Retrieves all contracts.
     *
     * @return A [Uni] containing a list of all contract entities.
     */
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    fun getAllContracts(): Uni<List<Contract>> {
        val userSubject = jwt.getClaim<String>("sub")
        return contractRepository
            .getAllContractsForOwner(userSubject)
            .map { contracts -> contracts.map { it.toClientContract() } }
    }

    /**
     * Terminates a contract based on the specified contract ID by setting the validation-to-time to now.
     *
     * @param contractId The ID of the contract to terminate.
     * @return A [Uni] containing the terminated contract entity.
     */
    @DELETE
    @Path("/{contractId}")
    @ResponseStatus(204)
    fun terminateContract(
        @RestPath contractId: String,
    ): Uni<Unit> =
        contractRepository
            .terminateContract(contractId)
            .chain { terminatedContract ->
                contractRepository.terminateAllContractsForOwnerIfOnboarding(terminatedContract)
            }.onFailure(NoContractException::class.java)
            .transform { _ -> NotFoundException("Contract not found") }
            .map { }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    fun createContract(contractRequest: ContractRequest): Uni<Contract> {
        val name = jwt.getClaim<String>("name")
        val email = jwt.getClaim<String>("email")
        val userSubject = jwt.getClaim<String>("sub")

        return paperlessContractService
            .createNewOnboardingContract(email, name, userSubject, contractRequest)
            .map { it.toClientContract() }
    }
}
