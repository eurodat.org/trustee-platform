package org.eurodat.contractservice.entity

import io.quarkus.hibernate.reactive.panache.kotlin.PanacheEntityBase
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.Table
import java.time.ZonedDateTime

@Entity
@Table(name = "company")
data class CompanyEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null,
    @Column(name = "business_id")
    var businessId: String,
    @Column(name = "name")
    var name: String,
    @Column(name = "created_at", nullable = false)
    val createdAt: ZonedDateTime,
    @Column(name = "street_address")
    var streetAddress: String,
    @Column(name = "zip")
    var zip: String,
    @Column(name = "town")
    var town: String,
    @Column(name = "country")
    var country: String,
    @Column(name = "register_location")
    var registerLocation: String,
    @Column(name = "register_no")
    var registerNo: String,
    @Column(name = "vat_id", nullable = true)
    var vatId: String?,
    @Column(name = "national_tax_id", nullable = true)
    var nationalTaxId: String?,
) : PanacheEntityBase
