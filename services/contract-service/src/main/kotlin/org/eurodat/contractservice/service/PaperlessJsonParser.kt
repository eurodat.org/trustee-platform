package org.eurodat.contractservice.service

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.TextNode
import jakarta.enterprise.context.ApplicationScoped
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.NotFoundException
import org.eurodat.contractservice.model.Company
import org.eurodat.contractservice.model.Participant
import java.time.ZonedDateTime

@ApplicationScoped
class PaperlessJsonParser {
    fun parseSubmissionIdFromDocument(submissionNode: JsonNode): Int {
        val submissionId = submissionNode["object"]["submittable"]["submission_id"]
        if (submissionId.isIntegralNumber) {
            return submissionId.asInt()
        } else {
            throw BadRequestException("Submission ID is not an integer")
        }
    }

    fun parseCompletedAtFromDocument(submissionNode: JsonNode): ZonedDateTime {
        try {
            return ZonedDateTime.parse(submissionNode["object"]["completed_at"].asText())
        } catch (exception: Exception) {
            throw BadRequestException("completed_at is not a valid date", exception)
        }
    }

    fun parseSignersFromDocument(submissionNode: JsonNode): List<Participant> {
        val aggregatedSubmitEventValues = submissionNode["object"]["aggregated_submit_event_values"]

        val subjectIds = extractSubjectIds(aggregatedSubmitEventValues)

        return subjectIds.map { id ->
            createSignerFromId(id, aggregatedSubmitEventValues)
        }
    }

    fun parseCompanyDataFromDocument(
        submissionNode: JsonNode,
        companyId: String,
    ): Company {
        val aggregatedSubmitEventValues = submissionNode["object"]["aggregated_submit_event_values"]

        val companyName = extractValueFromSlug(aggregatedSubmitEventValues, "companyname_slug")
        val companyStreetAddress = extractValueFromSlug(aggregatedSubmitEventValues, "companystreet_slug")
        val companyZip = extractValueFromSlug(aggregatedSubmitEventValues, "companyzip_slug")
        val companyTown = extractValueFromSlug(aggregatedSubmitEventValues, "companytown_slug")
        val companyCountry = extractValueFromSlug(aggregatedSubmitEventValues, "companycountry_slug")
        val companyRegisterLocation = extractValueFromSlug(aggregatedSubmitEventValues, "companyregisterlocation_slug")
        val companyRegisterNo = extractValueFromSlug(aggregatedSubmitEventValues, "companyregisterno_slug")
        val companyVatId =
            try {
                extractValueFromSlug(aggregatedSubmitEventValues, "companyvatid_slug")
            } catch (e: NotFoundException) {
                null
            }

        val companyNationalTaxId =
            try {
                extractValueFromSlug(aggregatedSubmitEventValues, "companynationaltaxid_slug")
            } catch (e: NotFoundException) {
                null
            }

        val address =
            Company.Address(
                streetAddress = companyStreetAddress,
                zip = companyZip,
                town = companyTown,
                country = companyCountry,
            )

        return Company(
            id = companyId,
            name = companyName,
            address = address,
            registerLocation = companyRegisterLocation,
            registerNo = companyRegisterNo,
            vatId = companyVatId,
            nationalTaxId = companyNationalTaxId,
        )
    }

    private fun extractSubjectIds(eventValues: JsonNode): List<String> {
        for (eventValue in eventValues) {
            if (eventValue["slug"].asText() == "companydirectors_slug") {
                val valueNode = eventValue["value"]
                return valueNode.map { (it as TextNode).asText() }
            }
        }
        throw NotFoundException("No director IDs found in the provided JSON.")
    }

    private fun extractValueFromSlug(
        eventValues: JsonNode,
        slug: String,
    ): String {
        for (eventValue in eventValues) {
            if (eventValue["slug"].asText() == slug) {
                return eventValue["value"].asText()
            }
        }
        throw NotFoundException("No value found for slug: $slug")
    }

    private fun createSignerFromId(
        userSubject: String,
        eventValues: JsonNode,
    ): Participant {
        var email: String? = null
        var lastName: String? = null
        var firstName: String? = null
        var role: String? = null

        for (eventValue in eventValues) {
            val slug = eventValue["slug"].asText()

            when (slug) {
                "companydirectors_slug.$userSubject.email_slug" -> email = eventValue["value"].asText()
                "companydirectors_slug.$userSubject.companydirectorslastname_slug" -> lastName = eventValue["value"].asText()
                "companydirectors_slug.$userSubject.companydirectorsfirstname_slug" -> firstName = eventValue["value"].asText()
                "companydirectors_slug.$userSubject.radio3" -> role = eventValue["mapped_value"].asText()
            }
        }

        if (email == null || lastName == null || firstName == null || role == null) {
            throw NotFoundException("Missing signer details for userSubject: $userSubject")
        }

        return Participant(
            firstName = firstName,
            lastName = lastName,
            email = email,
            role = role,
            status = "sent",
        )
    }
}
