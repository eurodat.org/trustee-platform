/**
* Paperless Public API
*
* Contact: developers@paperless.io
*
*/

package org.eurodat.contractservice.paperless

import com.fasterxml.jackson.databind.JsonNode
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.HeaderParam
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.ProcessingException
import jakarta.ws.rs.Produces
import jakarta.ws.rs.QueryParam
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient

@RegisterClientHeaders(APIKeyHeaderFactory::class)
@RegisterRestClient(configKey = "paperless-client")
@Path("/api/v1/documents")
interface DocumentApi {
    /**
     * Create a new Document
     *
     * Creates a new Document. Creating a new Document is possible in different ways:  - From scratch (new empty document)  - Provide a &#x60;document_id&#x60; of an existing document which then gets duplicated  - Provide a &#x60;template_id&#x60; which then will be used as source to prepare the new document  - Provide a signed id of a Blob within the &#x60;pdf&#x60; parameter. The PDF must be uploaded before creating the   document (see &#x60;createBlob&#x60; operation) and be a valid, unencrypted PDF without password protection.  When a document is duplicated/prepared you can also provide the same parameters as when you update a document to overwrite the values on the resulting document on creation. The &#x60;workspace_id&#x60; must be submitted in any case. When duplicating/preparing a document and the &#x60;workspace_id&#x60; differs from the workspace of the document or template, the new document gets duplicated \&quot;into the workspace\&quot; for the given &#x60;workspace_id&#x60;. Be aware that, to duplicate/prepare \&quot;into another workspace\&quot;, the user needs to be able to manage documents on both, the source and the destination, workspaces for the operation to succeed.  ## Advanced document creation  Please read [Advanced document creation](docs/public/advanced_document_creation.md) for more information.  ## Initial participants dispatch strategy  Normally the client creating a document will never be able to read tokens of any participants and the tokens are only distributed directly to the participants via email. Because this applies some restrictions for external clients integrating with Paperless, it is possible to set an &#x60;participants_dispatch_strategy&#x60; parameter when a document is created and directly dispatched. Providing this parameter will mark the participants which are at first turn as &#x60;state &#x3D; dispatch_completed&#x60; and assigned them the &#x60;dispatch_strategy&#x60; &#x60;api_response_initial_participants&#x60;. As long as those participants are in a state which allows them to participate their token will be serialized in the response for the document. The client is then responsible to deliver the tokens / url to the participants as they will not get an email with their token from Paperless. The token is given as a raw value (&#x60;token&#x60;) and as a complete url (&#x60;participation_url&#x60;). If the workspace&#39;s organisation has a custom domain configured (via &#x60;submission_frontend_base_url&#x60;), this domain will be reflected in &#x60;participation_url&#x60;.  NOTE: Only the participants at the first turn will be marked with the &#x60;dispatch_strategy&#x60; &#x60;api_response_initial_participants&#x60;. All participants further in the participation flow will get their token via email.  ##### Expandable properties  The type &#x60;Document&#x60; contains the following expandable properties:    - &#x60;creator&#x60;   - &#x60;blocks&#x60;   - &#x60;designs&#x60;   - &#x60;participation_flow&#x60;   - &#x60;participants&#x60;   - &#x60;tokens&#x60;
     *
     */
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Throws(
        ProcessingException::class,
    )
    fun createDocument(
        @QueryParam("expand[]") expand: List<String?>?,
        @HeaderParam("Paperless-Version") paperlessVersion: String?,
        requestBody: JsonNode?,
    ): Uni<JsonNode>
}
