package org.eurodat.contractservice.repository

import io.quarkus.hibernate.reactive.panache.PanacheRepositoryBase
import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.replaceWithUnit
import jakarta.enterprise.context.ApplicationScoped
import org.eurodat.contractservice.entity.ContractEntity
import org.eurodat.contractservice.entity.ParticipantEntity
import org.eurodat.contractservice.model.Contract
import org.eurodat.contractservice.model.ContractState
import org.eurodat.contractservice.model.ContractType
import org.eurodat.contractservice.model.Participant
import java.time.ZonedDateTime

@ApplicationScoped
class PanacheContractRepository :
    ContractRepository,
    PanacheRepositoryBase<ContractEntity, Long> {
    @WithTransaction
    override fun createContract(contract: Contract): Uni<Contract> {
        val contractEntity =
            ContractEntity(
                id = null,
                businessId = contract.id,
                validFrom = null,
                validTo = null,
                contractType = contract.contractType,
                ownerId = contract.ownerId,
                appId = contract.appId,
                contractValidFrom = contract.contractValidFrom,
                contractValidTo = contract.contractValidTo,
                status = contract.status,
                submissionId = contract.submissionId,
                companyId = contract.companyId,
                participants = listOf(),
            )
        val participantEntities = mapToParticipantAndContractEntity(contract.participants, contractEntity)
        contractEntity.participants = participantEntities

        participantEntities.forEach { participant ->
            participant.contract = contractEntity
        }

        return Uni.createFrom().item { ZonedDateTime.now() }.chain { timestamp ->
            contractEntity.validFrom = timestamp
            persist(contractEntity).map { it.toContract() }
        }
    }

    @WithSession
    override fun findContract(contractId: String): Uni<Contract?> =
        Uni.createFrom().item { ZonedDateTime.now() }.chain { timestamp ->
            find(
                "businessId = ?1 and validFrom <= ?2 and (validTo is null or validTo >= ?3)",
                contractId,
                timestamp,
                timestamp,
            ).firstResult<ContractEntity>().map { it?.toContract() }
        }

    @WithSession
    override fun findContractBySubmissionId(submissionId: Int): Uni<Contract?> =
        Uni.createFrom().item { ZonedDateTime.now() }.chain { timestamp ->
            find(
                "submissionId = ?1 and validFrom <= ?2 and (validTo is null or validTo >= ?3)",
                submissionId,
                timestamp,
                timestamp,
            ).firstResult<ContractEntity>().map { it?.toContract() }
        }

    @WithSession
    override fun getAllContractsForOwner(ownerId: String): Uni<List<Contract>> =
        Uni.createFrom().item { ZonedDateTime.now() }.chain { timestamp ->
            find(
                "ownerId = ?1 and validFrom <= ?2 and (validTo is null or validTo >= ?3)",
                ownerId,
                timestamp,
                timestamp,
            ).list<ContractEntity>()
                .chain { list ->
                    Uni.createFrom().item { list.map { entity -> entity.toContract() } }
                }
        }

    @WithTransaction
    override fun terminateContract(contractId: String): Uni<Contract> =
        Uni.createFrom().item { ZonedDateTime.now() }.chain { timestamp ->
            find(
                "businessId = ?1 and validFrom <= ?2 and (validTo is null or validTo >= ?3)",
                contractId,
                timestamp,
                timestamp,
            ).firstResult<ContractEntity>()
                .onItem()
                .ifNull()
                .failWith(NoContractException())
                .map { contractEntity -> contractEntity!!.apply { validTo = timestamp } }
                .call { c -> c.flush() }
                .map { it.toContract() }
        }

    @WithTransaction
    override fun setContractStatusToSent(
        contract: Contract,
        submissionId: Int,
    ): Uni<Contract> =
        terminateContract(contract.id)
            .chain { c ->
                createContract(c.copy(status = ContractState.SENT.status, submissionId = submissionId, participants = contract.participants))
            }

    @WithTransaction
    override fun updateContractWithParticipants(
        contractId: String,
        participants: List<Participant>,
    ): Uni<Contract> =
        terminateContract(contractId)
            .chain { c -> createContract(c.copy(participants = participants)) }

    @WithTransaction
    override fun setContractStatusToSigned(
        contractId: String,
        contractValidFrom: ZonedDateTime,
    ): Uni<Contract> =
        terminateContract(contractId)
            .chain { c ->
                createContract(
                    c.copy(
                        status = ContractState.SIGNED.status,
                        contractValidFrom = contractValidFrom,
                        participants =
                            c.participants.map { participant ->
                                participant.copy(status = "completed")
                            },
                    ),
                )
            }

    @WithTransaction
    override fun findTnCContractByParticipant(
        email: String,
        documentId: Int,
    ): Uni<Contract?> =
        Uni.createFrom().item { ZonedDateTime.now() }.chain { timestamp ->
            find(
                """
                SELECT c FROM ContractEntity c
                JOIN c.participants p
                WHERE p.emailAddress = ?1
                AND p.documentId = ?2
                AND c.validFrom <= ?3
                AND (c.validTo IS NULL OR c.validTo >= ?4)
                AND c.contractType LIKE ?5
                """,
                email,
                documentId,
                timestamp,
                timestamp,
                "TERMS_AND_CONDITIONS_%",
            ).firstResult<ContractEntity>().map { it?.toContract() }
        }

    private fun mapToParticipantAndContractEntity(
        participants: List<Participant>,
        contractEntity: ContractEntity,
    ): List<ParticipantEntity> =
        participants.map { participant ->
            ParticipantEntity(
                firstName = participant.firstName,
                lastName = participant.lastName,
                emailAddress = participant.email,
                participantRole = participant.role,
                status = participant.status,
                contract = contractEntity,
                documentId = participant.documentId,
            )
        }

    override fun terminateAllContractsForOwnerIfOnboarding(terminatedContract: Contract): Uni<Unit> =
        if (terminatedContract.contractType == ContractType.ONBOARDING.type) {
            getAllContractsForOwner(terminatedContract.ownerId)
                .chain { activeContracts ->
                    if (activeContracts.isNotEmpty()) {
                        var terminationChain = Uni.createFrom().item(Unit)
                        // Chain the termination of each active contract sequentially
                        activeContracts.forEach { contract ->
                            terminationChain = terminationChain.chain { _ -> terminateContract(contract.id).replaceWithUnit() }
                        }
                        terminationChain
                    } else {
                        Uni.createFrom().item(Unit)
                    }
                }
        } else {
            Uni.createFrom().item(Unit)
        }
}

private fun mapToParticipants(participantEntities: List<ParticipantEntity>): List<Participant> =
    participantEntities.map { participantEntity ->
        Participant(
            firstName = participantEntity.firstName,
            lastName = participantEntity.lastName,
            email = participantEntity.emailAddress,
            role = participantEntity.participantRole,
            status = participantEntity.status,
            documentId = participantEntity.documentId,
        )
    }

fun ContractEntity.toContract(): Contract =
    Contract(
        id = this.businessId,
        contractType = this.contractType,
        ownerId = this.ownerId,
        appId = this.appId,
        contractValidFrom = this.contractValidFrom,
        contractValidTo = this.contractValidTo,
        status = this.status,
        submissionId = this.submissionId,
        companyId = this.companyId,
        participants = mapToParticipants(this.participants),
    )
