package org.eurodat.contractservice.model

import java.time.ZonedDateTime

data class Contract(
    val id: String,
    val contractType: String,
    val ownerId: String,
    val appId: String?,
    val contractValidFrom: ZonedDateTime?,
    var contractValidTo: ZonedDateTime?,
    val status: String,
    val submissionId: Int?,
    val participants: List<Participant> = listOf(),
    val companyId: String,
) {
    fun toClientContract() =
        org.eurodat.client.contractservice.model.Contract(
            id = this.id,
            contractType = this.contractType,
            appId = this.appId,
            contractValidTo = this.contractValidTo,
            contractValidFrom = this.contractValidFrom,
            status = this.status,
            participants = this.participants.map { it.toClientParticipant() },
            companyId = this.companyId,
        )
}
