package org.eurodat.contractservice.repository

import io.quarkus.hibernate.reactive.panache.PanacheRepositoryBase
import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.ws.rs.NotFoundException
import org.eurodat.contractservice.entity.CompanyEntity
import org.eurodat.contractservice.model.Company
import java.time.ZonedDateTime
import java.util.UUID

@ApplicationScoped
class PanacheCompanyRepository :
    CompanyRepository,
    PanacheRepositoryBase<CompanyEntity, Long> {
    @WithTransaction
    override fun createNewCompany(): Uni<String> {
        val companyEntity =
            CompanyEntity(
                id = null,
                businessId = UUID.randomUUID().toString(),
                name = "",
                streetAddress = "",
                zip = "",
                town = "",
                country = "",
                registerLocation = "",
                registerNo = "",
                vatId = null,
                nationalTaxId = null,
                createdAt = ZonedDateTime.now(),
            )

        return persist(companyEntity).map { it.businessId }
    }

    @WithTransaction
    override fun updateCompany(company: Company): Uni<Company> =
        find("businessId = ?1", company.id)
            .firstResult<CompanyEntity>()
            .onItem()
            .ifNull()
            .failWith(NotFoundException("Company with id ${company.id} not found"))
            .chain { existingCompanyEntity ->
                existingCompanyEntity.name = company.name
                existingCompanyEntity.streetAddress = company.address.streetAddress
                existingCompanyEntity.zip = company.address.zip
                existingCompanyEntity.town = company.address.town
                existingCompanyEntity.country = company.address.country
                existingCompanyEntity.registerLocation = company.registerLocation
                existingCompanyEntity.registerNo = company.registerNo
                existingCompanyEntity.vatId = company.vatId
                existingCompanyEntity.nationalTaxId = company.nationalTaxId

                persist(existingCompanyEntity).map { it.toCompany() }
            }
}

fun CompanyEntity.toCompany(): Company =
    Company(
        id = this.businessId,
        name = this.name,
        address =
            Company.Address(
                streetAddress = this.streetAddress,
                zip = this.zip,
                town = this.town,
                country = this.country,
            ),
        registerLocation = this.registerLocation,
        registerNo = this.registerNo,
        vatId = this.vatId,
        nationalTaxId = this.nationalTaxId,
    )
