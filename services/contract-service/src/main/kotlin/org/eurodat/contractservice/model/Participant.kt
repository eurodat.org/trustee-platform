package org.eurodat.contractservice.model

data class Participant(
    val firstName: String,
    val lastName: String,
    val email: String,
    val role: String,
    var status: String,
    val documentId: Int? = null,
) {
    fun toClientParticipant() =
        org.eurodat.client.contractservice.model.Participant(
            firstName = this.firstName,
            lastName = this.lastName,
            email = this.email,
            role = this.role,
            status = this.status,
        )
}
