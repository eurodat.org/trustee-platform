package org.eurodat.contractservice.repository

import io.smallrye.mutiny.Uni
import org.eurodat.contractservice.model.Company

interface CompanyRepository {
    fun createNewCompany(): Uni<String>

    fun updateCompany(company: Company): Uni<Company>
}
