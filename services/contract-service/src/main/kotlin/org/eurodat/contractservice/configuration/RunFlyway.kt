package org.eurodat.contractservice.configuration

import io.quarkus.runtime.StartupEvent
import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.event.Observes
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.flywaydb.core.Flyway

/**
 * The RunFlyway class is responsible for running Flyway database migrations on application startup.
 *
 * It retrieves the necessary configuration properties and initializes Flyway with the provided parameters.
 * The database migrations are then executed using the `migrate()` method.
 */
@ApplicationScoped
class RunFlyway {
    @ConfigProperty(name = "quarkus.datasource.reactive.url")
    internal lateinit var datasourceUrl: String

    @ConfigProperty(name = "quarkus.datasource.username")
    internal lateinit var datasourceUsername: String

    @ConfigProperty(name = "quarkus.datasource.password")
    internal lateinit var datasourcePassword: String

    @ConfigProperty(name = "quarkus.flyway.schemas")
    internal lateinit var schema: String

    /**
     * Runs Flyway database migrations on application startup.
     *
     * @param event The startup event.
     */
    fun runFlywayMigration(
        @Observes event: StartupEvent?,
    ) {
        val datasourceJDBCUrl: String = datasourceUrl.replaceFirst("vertx-reactive:", "jdbc:")
        val flyway: Flyway =
            Flyway
                .configure()
                .dataSource(datasourceJDBCUrl, datasourceUsername, datasourcePassword)
                .schemas(schema)
                .load()
        flyway.migrate()
    }
}
