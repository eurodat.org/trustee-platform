package org.eurodat.contractservice.model

enum class ContractType(
    val type: String,
) {
    ONBOARDING("ONBOARDING_COMPANY_MASTERDATA"),
    TNC_1("TERMS_AND_CONDITIONS_1"),
    TNC_2("TERMS_AND_CONDITIONS_2"),
    TNC_3("TERMS_AND_CONDITIONS_3"),
    TNC_4("TERMS_AND_CONDITIONS_4"),
    TNC_5("TERMS_AND_CONDITIONS_5"),
}
