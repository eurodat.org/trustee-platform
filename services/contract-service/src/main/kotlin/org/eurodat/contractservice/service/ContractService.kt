package org.eurodat.contractservice.service

import com.fasterxml.jackson.databind.JsonNode
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.ws.rs.ClientErrorException
import jakarta.ws.rs.WebApplicationException
import org.eurodat.client.contractservice.model.ContractRequest
import org.eurodat.contractservice.model.Contract
import org.eurodat.contractservice.model.ContractState
import org.eurodat.contractservice.model.Participant
import org.eurodat.contractservice.repository.ContractRepository
import org.hibernate.exception.ConstraintViolationException

@ApplicationScoped
class ContractService {
    @Inject
    internal lateinit var contractRepository: ContractRepository

    fun createNewContract(
        ownerId: String,
        contractRequest: ContractRequest,
        companyId: String,
        jsonSupplier: () -> Uni<JsonNode>,
        participants: List<Participant> = listOf(),
    ): Uni<Contract> {
        val businessId = "${contractRequest.contractType}_$ownerId"

        return contractRepository
            .createContract(
                Contract(
                    id = businessId,
                    contractType = contractRequest.contractType,
                    ownerId = ownerId,
                    appId = contractRequest.appId,
                    contractValidFrom = null,
                    contractValidTo = null,
                    status = ContractState.CREATED.status,
                    submissionId = null,
                    companyId = companyId,
                    participants = participants,
                ),
            ).onFailure { it is ConstraintViolationException }
            .transform { _ -> ClientErrorException("A Contract with ID $businessId already exists", 409) }
            .chain { createdContract ->
                createDocumentAndSetStatusToSent(jsonSupplier, createdContract)
            }
    }

    private fun createDocumentAndSetStatusToSent(
        jsonSupplier: () -> Uni<JsonNode>,
        contract: Contract,
    ): Uni<Contract> =
        jsonSupplier()
            .chain { paperlessResponseJson ->
                val submissionIdNode = paperlessResponseJson["submission_id"]
                val submissionId =
                    submissionIdNode?.asInt()
                        ?: throw WebApplicationException(
                            "Response from external API could not be processed: Missing 'submission_id' in response body.",
                            500,
                        )

                // Extract participants from the response, if available
                val participantsNode = paperlessResponseJson["participants"] ?: null

                val updatedContract =
                    if (participantsNode != null) {
                        updateContractWithParticipantDocumentIds(participantsNode, contract)
                    } else {
                        contract
                    }
                contractRepository.setContractStatusToSent(updatedContract, submissionId)
            }.onFailure()
            .call { _ ->
                contractRepository.terminateContract(contract.id)
            }

    private fun updateContractWithParticipantDocumentIds(
        participantsNode: JsonNode,
        contract: Contract,
    ): Contract {
        // Update each participant's document ID in the contract from the paperless response
        val updatedParticipants =
            contract.participants.map { participant ->
                participantsNode
                    .find { participantJson ->
                        participantJson["email"].asText() == participant.email
                    }?.let { participantJson ->
                        participant.copy(
                            documentId = participantJson["id"].asInt(),
                        )
                    } ?: participant
            }
        return contract.copy(participants = updatedParticipants)
    }
}
