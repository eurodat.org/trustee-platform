package org.eurodat.contractservice.paperless

import jakarta.enterprise.context.ApplicationScoped
import jakarta.ws.rs.core.MultivaluedHashMap
import jakarta.ws.rs.core.MultivaluedMap
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.rest.client.ext.ClientHeadersFactory

@ApplicationScoped
class APIKeyHeaderFactory : ClientHeadersFactory {
    @ConfigProperty(name = "paperless.api-key")
    internal lateinit var paperlessApiKey: String

    override fun update(
        incomingHeaders: MultivaluedMap<String, String>,
        clientOutgoingHeaders: MultivaluedMap<String, String>,
    ): MultivaluedMap<String, String> = MultivaluedHashMap(mapOf(Pair("Authorization", "Bearer $paperlessApiKey")))
}
