package org.eurodat.contractservice.repository

import io.smallrye.mutiny.Uni
import org.eurodat.contractservice.model.Contract
import org.eurodat.contractservice.model.Participant
import java.time.ZonedDateTime

interface ContractRepository {
    fun createContract(contract: Contract): Uni<Contract>

    fun findContract(contractId: String): Uni<Contract?>

    fun findContractBySubmissionId(submissionId: Int): Uni<Contract?>

    fun getAllContractsForOwner(ownerId: String): Uni<List<Contract>>

    fun terminateContract(contractId: String): Uni<Contract>

    fun setContractStatusToSigned(
        contractId: String,
        contractValidFrom: ZonedDateTime,
    ): Uni<Contract>

    fun setContractStatusToSent(
        contract: Contract,
        submissionId: Int,
    ): Uni<Contract>

    fun updateContractWithParticipants(
        contractId: String,
        participants: List<Participant>,
    ): Uni<Contract>

    fun findTnCContractByParticipant(
        email: String,
        documentId: Int,
    ): Uni<Contract?>

    fun terminateAllContractsForOwnerIfOnboarding(terminatedContract: Contract): Uni<Unit>
}
