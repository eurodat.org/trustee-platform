package org.eurodat.contractservice.paperless

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.common.ConsoleNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.extension.ResponseTransformerV2
import com.github.tomakehurst.wiremock.http.HttpHeader
import com.github.tomakehurst.wiremock.http.HttpHeaders
import com.github.tomakehurst.wiremock.http.Response
import com.github.tomakehurst.wiremock.stubbing.ServeEvent
import com.marcinziolo.kotlin.wiremock.contains
import com.marcinziolo.kotlin.wiremock.equalTo
import com.marcinziolo.kotlin.wiremock.get
import com.marcinziolo.kotlin.wiremock.post
import com.marcinziolo.kotlin.wiremock.returns
import io.github.oshai.kotlinlogging.KotlinLogging
import io.quarkus.arc.lookup.LookupIfProperty
import io.quarkus.runtime.Startup
import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.inject.Produces
import jakarta.inject.Inject
import org.eurodat.contractservice.model.ContractType
import org.eurodat.contractservice.service.PaperlessService
import java.io.FileNotFoundException

val logger = KotlinLogging.logger { }

@ApplicationScoped
@LookupIfProperty(name = "quarkus.rest-client.paperless-client.url", stringValue = "http://localhost:5555")
class PaperlessWireMockProducer {
    @Inject
    internal lateinit var paperlessService: PaperlessService

    @Produces
    @Startup
    @ApplicationScoped
    fun paperlessWireMock(): WireMockServer =
        WireMockServer(
            WireMockConfiguration
                .options()
                .port(5555)
                .extensions(ConnectionCloseExtension())
                .notifier(ConsoleNotifier("HttpRequestBuilder", true)),
        ).setup()

    private fun WireMockServer.setup(): WireMockServer {
        // Stub for cypress test user document creation
        this.post {
            url equalTo "/api/v1/documents"
            priority = 1
            body contains "name" contains "Cypress Tester"
        } returns {
            statusCode = 201
            header = "Content-Type" to "application/json"
            body = fileToString("wiremock/PaperlessCreateDocumentResultCypressTest.json")
        }

        // Stub for Terms and Conditions 1 document creation
        this.post {
            url equalTo "/api/v1/documents"
            priority = 2
            body contains "template_id" equalTo paperlessService.mapTemplateNameToId(ContractType.TNC_1.type)
        } returns {
            statusCode = 201
            header = "Content-Type" to "application/json"
            body = fileToString("wiremock/PaperlessCreateTnCDocumentResult.json")
        }

        // Stub for general document creation
        this.post {
            url equalTo "/api/v1/documents"
            priority = 3
        } returns {
            statusCode = 201
            header = "Content-Type" to "application/json"
            body = fileToString("wiremock/PaperlessCreateDocumentResult.json")
        }

        // stub for cypress e2e test with expanded properties: 1 participant
        this.post {
            url equalTo "/api/v1/documents?expand%5B%5D=participants"
            body contains "name" contains "Cypress Tester"
            priority = 1
        } returns {
            statusCode = 201
            header = "Content-Type" to "application/json"
            body = fileToString("wiremock/PaperlessCreateTnCDocumentExpandOneParticipantResultCypressTest.json")
        }

        // stub for e2e test with expanded properties: 1 participant
        this.post {
            url equalTo "/api/v1/documents?expand%5B%5D=participants"
            body contains "template_id" equalTo paperlessService.mapTemplateNameToId(ContractType.TNC_1.type)
            priority = 2
        } returns {
            statusCode = 201
            header = "Content-Type" to "application/json"
            body = fileToString("wiremock/PaperlessCreateTnCDocumentExpandOneParticipantResult.json")
        }

        // stub for e2e test with expanded properties: 2 participants
        this.post {
            url equalTo "/api/v1/documents?expand%5B%5D=participants"
            priority = 3
        } returns {
            statusCode = 201
            header = "Content-Type" to "application/json"
            body = fileToString("wiremock/PaperlessCreateTnCDocumentExpandTwoParticipantsResult.json")
        }

        this.get {
            url equalTo "/"
        } returns {
            statusCode = 200
            header = "Content-Type" to "application/json"
            body = "{Status: Healthy}"
        }
        this.start()
        logger.info { "PaperlessWireMock setup completed w/ wiremock running being ${this.isRunning}" }
        return this
    }

    companion object {
        // Can't be fixed by ktlint, always leads to violation
        @Suppress("ktlint:standard:curly-spacing")
        fun fileToString(fileName: String): String =
            {}
                .javaClass.classLoader
                .getResourceAsStream(fileName)
                ?.bufferedReader()
                ?.readText()
                ?: throw FileNotFoundException("$fileName not found")
    }
}

class ConnectionCloseExtension : ResponseTransformerV2 {
    override fun transform(
        response: Response,
        serviceEvent: ServeEvent,
    ): Response =
        Response.Builder
            .like(response)
            .headers(
                HttpHeaders
                    .copyOf(response.headers)
                    .plus(HttpHeader("Connection", "close")),
            ).build()

    override fun getName(): String = "ConnectionCloseExtension"
}
