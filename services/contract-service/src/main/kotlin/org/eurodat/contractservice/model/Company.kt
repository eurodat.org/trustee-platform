package org.eurodat.contractservice.model

data class Company(
    val id: String,
    val name: String,
    val address: Address,
    val registerLocation: String,
    val registerNo: String,
    val vatId: String?,
    val nationalTaxId: String?,
) {
    data class Address(
        val streetAddress: String,
        val zip: String,
        val town: String,
        val country: String,
    )
}
