package org.eurodat.contractservice.model

enum class ContractState(
    val status: String,
) {
    CREATED("CREATED"),
    SENT("SENT"),
    SIGNED("SIGNED"),
}
