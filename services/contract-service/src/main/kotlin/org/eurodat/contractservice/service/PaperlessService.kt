package org.eurodat.contractservice.service

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.ws.rs.BadRequestException
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.contractservice.model.ContractType
import org.eurodat.contractservice.model.Participant
import org.eurodat.contractservice.paperless.DocumentApi
import java.time.LocalDateTime

@ApplicationScoped
class PaperlessService {
    @RestClient
    internal lateinit var documentApi: DocumentApi
    private val objectMapper = ObjectMapper()

    fun verifyTemplateName(templateName: String): Boolean =
        when (templateName) {
            ContractType.ONBOARDING.type -> true
            ContractType.TNC_1.type -> true
            ContractType.TNC_2.type -> true
            ContractType.TNC_3.type -> true
            ContractType.TNC_4.type -> true
            ContractType.TNC_5.type -> true
            else -> false
        }

    fun mapTemplateNameToId(templateName: String): Int =
        when (templateName) {
            ContractType.ONBOARDING.type -> 14509
            ContractType.TNC_1.type -> 17337
            ContractType.TNC_2.type -> 17336
            ContractType.TNC_3.type -> 17335
            ContractType.TNC_4.type -> 17334
            ContractType.TNC_5.type -> 17226
            else -> throw IllegalArgumentException("Unknown template name: $templateName")
        }

    fun getTermsAndConditionsTemplateName(signatoriesCount: Int): String =
        if (signatoriesCount in 1..5) {
            "TERMS_AND_CONDITIONS_$signatoriesCount"
        } else {
            throw IllegalArgumentException("Invalid number of signatories: $signatoriesCount")
        }

    fun createCompanyMasterData(
        email: String,
        name: String,
        templateName: String,
    ): Uni<JsonNode> {
        val participantNode =
            objectMapper.createObjectNode().apply {
                put("email", email)
                put("name", name)
            }

        val requestBody =
            objectMapper.createObjectNode().apply {
                put("workspace_id", 2981)
                put("template_id", mapTemplateNameToId(templateName))
                put("name", "$templateName of $name at ${LocalDateTime.now()}")

                put("state", "dispatched")

                val participantsMap =
                    objectMapper.createObjectNode().apply {
                        set<JsonNode>("Super-Admin", participantNode)
                    }
                set<JsonNode>("participants", participantsMap)

                val tokensNode =
                    objectMapper.createObjectNode().apply {
                        put("token1", "token-value")
                    }
                set<JsonNode>("tokens", tokensNode)
            }

        return documentApi.createDocument(emptyList(), "2023-06-23", requestBody)
    }

    fun createTermsAndConditionsDocument(
        participants: List<Participant>,
        templateName: String,
    ): Uni<JsonNode> {
        if (participants.isEmpty() || participants.size > 5) {
            throw BadRequestException("Invalid number of signatories: ${participants.size}")
        }

        val participantsNode = objectMapper.createObjectNode()
        participants.forEachIndexed { index, participant ->
            val participantNode =
                objectMapper.createObjectNode().apply {
                    put("name", "${participant.firstName} ${participant.lastName}")
                    put("email", participant.email)
                }
            participantsNode.set<ObjectNode>("Signer-${index + 1}", participantNode)
        }

        val requestBody =
            objectMapper.createObjectNode().apply {
                put("workspace_id", 2981)
                put("template_id", mapTemplateNameToId(templateName))
                put(
                    "name",
                    "$templateName for ${participants.joinToString(", ") { "${it.firstName} ${it.lastName}" }} at ${LocalDateTime.now()}",
                )
                put("state", "dispatched")
                set<JsonNode>("participants", participantsNode)
                set<JsonNode>("tokens", objectMapper.createObjectNode().put("token1", "token-value"))
            }

        return documentApi.createDocument(listOf("participants"), "2023-06-23", requestBody)
    }
}
