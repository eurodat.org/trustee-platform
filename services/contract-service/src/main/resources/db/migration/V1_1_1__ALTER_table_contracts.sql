ALTER TABLE contracts ALTER COLUMN contract_valid_from DROP NOT NULL;
ALTER TABLE contracts DROP COLUMN signature;
ALTER TABLE contracts DROP COLUMN pdf_url;
