ALTER TABLE participant ADD status varchar(255) not null;
ALTER TABLE participant ADD document_id integer;

ALTER TABLE participant RENAME COLUMN participant_id TO id;
ALTER TABLE participant RENAME COLUMN participant_role TO role;

ALTER TABLE contracts RENAME TO contract;
