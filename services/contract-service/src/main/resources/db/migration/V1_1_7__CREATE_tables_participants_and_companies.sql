create table company
(
    id                bigserial                   not null,
    name              varchar(255)                not null,
    street_address    varchar(255)                not null,
    zip               varchar(255)                not null,
    town              varchar(255)                not null,
    business_id       varchar(255)                not null,
    country           varchar(255)                not null,
    register_location varchar(255)                not null,
    register_no       varchar(255)                not null,
    created_at        timestamp(6) with time zone not null,
    vat_id            varchar(255),
    national_tax_id   varchar(255),
    primary key (id)
);

ALTER TABLE company
ADD CONSTRAINT unique_company_business_id UNIQUE (business_id);

create table participant
(
    participant_id   bigserial                   not null,
    first_name       varchar(255)                not null,
    last_name        varchar(255)                not null,
    email_address    varchar(255)                not null,
    participant_role varchar(255)                not null,
    contract_id      bigserial                   not null,
    primary key (participant_id),
    foreign key (contract_id) references contracts (id)
);
