ALTER TABLE contracts
ADD COLUMN company_business_id varchar(255),
ADD CONSTRAINT fk_company_business_id FOREIGN KEY (company_business_id) REFERENCES company (business_id);
