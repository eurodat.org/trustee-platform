create table contracts
(
    contract_valid_from timestamp(6) with time zone not null,
    contract_valid_to   timestamp(6) with time zone,
    id                  bigserial                   not null,
    valid_from          timestamp(6) with time zone not null,
    valid_to            timestamp(6) with time zone,
    app_id              varchar(255)                not null,
    business_id         varchar(255)                not null,
    contract_raw_data   varchar(255)                not null,
    contract_type       varchar(255)                not null,
    participant_id      varchar(255)                not null,
    pdf_url             varchar(255)                not null,
    signature           varchar(255)                not null,
    primary key (id)
);

create extension if not exists btree_gist;

alter table contracts add constraint no_overlapping_timeframes_per_business_id exclude using gist (business_id with =, tstzrange(valid_from, coalesce(valid_to, 'infinity'), '[]') with &&);
