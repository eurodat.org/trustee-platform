package org.eurodat.client.transactionservice

data class AppRequest(
    val appId: String = "",
    val consumer: List<String> = emptyList(),
    val provider: List<String> = emptyList(),
)
