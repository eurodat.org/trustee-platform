package org.eurodat.client.transactionservice

data class TransactionRequest(
    val clientId: String = "",
    val appId: String = "",
    val consumer: List<String> = listOf(),
    val provider: List<String> = listOf(),
)
