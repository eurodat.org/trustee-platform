package org.eurodat.client.transactionservice

import io.quarkus.oidc.token.propagation.reactive.AccessTokenRequestReactiveFilter
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient

@RegisterRestClient(configKey = "transaction-service")
@Path("clients")
@RegisterProvider(AccessTokenRequestReactiveFilter::class)
interface UserClient {
    @GET
    @Path("/clientmappings")
    fun getClientSelectorMapping(): Uni<List<ClientSelectorMapping>>

    @GET
    @Path("/clientselector")
    fun getAllClientSelectors(): Uni<List<String>>

    @GET
    @Path("/clientselector/{clientId}")
    fun getClientSelector(
        @PathParam("clientId") clientId: String,
    ): Uni<String>
}
