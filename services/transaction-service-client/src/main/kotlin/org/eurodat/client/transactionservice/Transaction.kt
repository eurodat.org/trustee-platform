package org.eurodat.client.transactionservice

import java.time.LocalDateTime

data class Transaction(
    val id: String = "",
    val appId: String = "",
    val clientId: String = "",
    val consumer: List<String> = listOf(),
    val provider: List<String> = listOf(),
    val startTime: LocalDateTime? = null,
    var endTime: LocalDateTime? = null,
)
