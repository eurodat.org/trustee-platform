package org.eurodat.client.transactionservice

data class WorkflowStartResponse(
    val transactionId: String = "",
    val workflowDefinitionId: String = "",
    val workflowRunId: String = "",
)
