package org.eurodat.client.transactionservice

import io.quarkus.oidc.token.propagation.reactive.AccessTokenRequestReactiveFilter
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient

@RegisterRestClient(configKey = "transaction-service")
@Path("/transactions")
@RegisterProvider(AccessTokenRequestReactiveFilter::class)
interface TransactionClient {
    @POST
    @Consumes("application/json")
    fun createTransaction(transactionRequest: TransactionRequest): Uni<Transaction>

    @GET
    @Path("/{transactionId}")
    @Consumes("application/json")
    fun findTransaction(
        @PathParam("transactionId") transactionId: String,
    ): Uni<Transaction>

    @GET
    @Consumes("application/json")
    fun getAllTransactionIds(): Uni<List<String>>

    @DELETE
    @Path("/{transactionId}")
    @Consumes("application/json")
    fun endTransaction(
        @PathParam("transactionId") transactionId: String,
    ): Uni<Transaction>
}
