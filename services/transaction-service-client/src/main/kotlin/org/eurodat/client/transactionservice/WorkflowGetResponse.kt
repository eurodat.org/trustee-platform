package org.eurodat.client.transactionservice

enum class WorkflowStatusPhase {
    UNKNOWN,
    PENDING,
    RUNNING,
    SUCCEEDED,
    FAILED,
    ERROR,
}

data class WorkflowStatus(
    val phase: WorkflowStatusPhase = WorkflowStatusPhase.UNKNOWN,
    val startedAt: String = "",
    val finishedAt: String = "",
) {
    companion object {
        fun fromString(
            phase: String?,
            startedAt: String?,
            finishedAt: String?,
        ): WorkflowStatus {
            val uppercaseValue = phase?.uppercase() ?: "UNKNOWN"
            return WorkflowStatus(
                try {
                    WorkflowStatusPhase.valueOf(uppercaseValue)
                } catch (e: IllegalArgumentException) {
                    throw IllegalArgumentException("Invalid workflow status: $phase")
                },
                startedAt ?: "",
                finishedAt ?: "",
            )
        }
    }
}

data class WorkflowMetadata(
    val name: String = "",
)

data class WorkflowGetResponse(
    val metadata: WorkflowMetadata = WorkflowMetadata(),
    val status: WorkflowStatus = WorkflowStatus(),
)
