package org.eurodat.client.transactionservice

data class ClientSelectorMapping(
    val clientId: String = "",
    val clientSelector: String = "",
)
