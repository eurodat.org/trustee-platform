package org.eurodat.client.transactionservice

import java.time.ZonedDateTime

data class Message(
    val correlationId: Long? = null,
    val appId: String,
    val transactionId: String,
    val workflowId: String,
    val label: String,
    val selector: String,
    val createdAt: ZonedDateTime? = null,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Message

        if (correlationId != other.correlationId) return false
        if (appId != other.appId) return false
        if (transactionId != other.transactionId) return false
        if (workflowId != other.workflowId) return false
        if (label != other.label) return false
        if (selector != other.selector) return false

        // the "zone" field is not the same type after the message was persisted, but the datetime is the same
        return createdAt?.isEqual(other.createdAt) ?: (other.createdAt == null)
    }

    override fun hashCode(): Int {
        var result = correlationId?.hashCode() ?: 0
        result = 31 * result + appId.hashCode()
        result = 31 * result + transactionId.hashCode()
        result = 31 * result + workflowId.hashCode()
        result = 31 * result + label.hashCode()
        result = 31 * result + selector.hashCode()
        result = 31 * result + (createdAt?.hashCode() ?: 0)
        return result
    }
}
