package org.eurodat.client.transactionservice

data class MessageRequest(
    val message: String,
    val selectors: List<String>,
)
