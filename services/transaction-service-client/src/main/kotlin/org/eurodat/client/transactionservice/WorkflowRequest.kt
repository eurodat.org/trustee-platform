package org.eurodat.client.transactionservice

data class WorkflowRequest(
    val workflowDefinitionId: String = "",
)
