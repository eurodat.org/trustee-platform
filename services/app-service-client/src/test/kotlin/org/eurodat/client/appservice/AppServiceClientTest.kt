package org.eurodat.client.appservice

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.common.ConsoleNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.http.RequestMethod
import com.marcinziolo.kotlin.wiremock.contains
import com.marcinziolo.kotlin.wiremock.equalTo
import com.marcinziolo.kotlin.wiremock.get
import com.marcinziolo.kotlin.wiremock.returns
import com.marcinziolo.kotlin.wiremock.verify
import io.quarkus.security.credential.TokenCredential
import io.quarkus.test.junit.QuarkusTest
import jakarta.enterprise.inject.Produces
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test

@QuarkusTest
class AppServiceClientTest {
    @RestClient
    private lateinit var appServiceClient: AppServiceClient

    companion object {
        private val mockServer =
            WireMockServer(
                WireMockConfiguration
                    .options()
                    .port(5555)
                    .notifier(ConsoleNotifier("HttpRequestBuilder", true)),
            )

        @BeforeAll
        @JvmStatic
        fun setup() {
            mockServer.get {
                url equalTo "/apps/safeAMLApp"
            } returns {
                statusCode = 200
            }

            mockServer.start()
        }

        @JvmStatic
        @AfterAll
        fun teardown() {
            mockServer.resetAll()
            mockServer.stop()
        }
    }

    @Test
    fun testFindApp() {
        appServiceClient
            .findApp("safeAMLApp")
            .await()
            .indefinitely()
        mockServer.verify {
            urlPath equalTo "/apps/safeAMLApp"
            headers contains "Authorization" equalTo "Bearer TOKEN"
            exactly = 1
            method = RequestMethod.GET
        }
    }

    @Produces
    fun createToken(): TokenCredential = TokenCredential("TOKEN", "bearer")
}
