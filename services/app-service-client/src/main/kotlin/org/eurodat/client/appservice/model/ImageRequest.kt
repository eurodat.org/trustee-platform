package org.eurodat.client.appservice.model

data class ImageRequest(
    val location: String,
    val registryUserName: String? = null,
    val registryPassword: String? = null,
)
