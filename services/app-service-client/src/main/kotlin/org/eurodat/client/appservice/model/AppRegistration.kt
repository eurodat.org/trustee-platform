package org.eurodat.client.appservice.model

data class AppRegistration(
    val id: String,
    val transactionDDL: String,
    val safeDepositDDL: String,
    val tableSecurityMapping: List<TableSecurityMapping>,
    val transactionTimeoutInDays: Int? = null,
)
