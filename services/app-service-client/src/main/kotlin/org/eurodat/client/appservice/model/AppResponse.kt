package org.eurodat.client.appservice.model

data class AppResponse(
    val id: String = "",
    val owner: String = "",
    val transactionDDL: String = "",
    val safeDepositDDL: String = "",
    val tableSecurityMapping: List<TableSecurityMapping> = emptyList(),
    val workflows: List<WorkflowDefinition> = emptyList(),
    val transactionTimeoutInDays: Int = 0,
)
