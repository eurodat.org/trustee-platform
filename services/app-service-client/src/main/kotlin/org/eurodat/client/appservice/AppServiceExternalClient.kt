package org.eurodat.client.appservice

import io.quarkus.oidc.client.filter.OidcClientFilter
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient

@RegisterRestClient(configKey = "app-service-external")
@OidcClientFilter("app-service-external")
interface AppServiceExternalClient : AppServiceClientBase
