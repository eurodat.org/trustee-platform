package org.eurodat.client.appservice

import io.quarkus.oidc.token.propagation.reactive.AccessTokenRequestReactiveFilter
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient

@RegisterRestClient(configKey = "app-service")
@RegisterProvider(AccessTokenRequestReactiveFilter::class)
interface AppServiceClient : AppServiceClientBase
