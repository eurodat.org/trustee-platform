package org.eurodat.client.appservice.model

data class WorkflowDefinition(
    val id: String = "",
    val resourceName: String = "",
    val entryPoint: String = "",
    val startCommand: List<String> = emptyList(),
    val imageId: String = "",
)
