package org.eurodat.client.appservice.model

import java.time.ZonedDateTime

data class Image(
    val id: String,
    val sourceLocation: String,
    val location: String?,
    val status: ImageStatus,
    val createdAt: ZonedDateTime,
)

enum class ImageStatus {
    UPLOADING,
    ACTIVE,
    FAILED,
    DELETING,
    DELETED,
}
