package org.eurodat.client.appservice

import io.smallrye.mutiny.Uni
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.Produces
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.appservice.model.AppResponse
import org.eurodat.client.appservice.model.Image
import org.eurodat.client.appservice.model.ImageRequest
import org.eurodat.client.appservice.model.WorkflowRegistrationRequest

@Path("/apps")
interface AppServiceClientBase {
    @POST
    @Produces("application/json")
    fun startAppRegistration(app: AppRegistration): Uni<AppResponse>

    @GET
    @Path("/{appId}")
    @Produces("application/json")
    fun findApp(
        @PathParam("appId") appId: String,
    ): Uni<AppResponse>

    @GET
    @Produces("application/json")
    fun getAllAppIds(): Uni<List<String>>

    @DELETE
    @Path("/{appId}")
    @Produces("application/json")
    fun deleteApp(
        @PathParam("appId") appId: String,
    ): Uni<Void>

    @POST
    @Path("/{appId}/images")
    @Produces("application/json")
    fun createImage(
        @PathParam("appId") appId: String,
        request: ImageRequest,
    ): Uni<Image>

    @GET
    @Path("/{appId}/images")
    @Produces("application/json")
    fun getAllImages(
        @PathParam("appId") appId: String,
    ): Uni<List<Image>>

    @GET
    @Path("/{appId}/images/{imageId}")
    @Produces("application/json")
    fun findImage(
        @PathParam("appId") appId: String,
        @PathParam("imageId") imageId: String,
    ): Uni<Image>

    @DELETE
    @Path("/{appId}/images/{imageId}")
    @Produces("application/json")
    fun deleteImage(
        @PathParam("appId") appId: String,
        @PathParam("imageId") imageId: String,
    ): Uni<Image>

    @POST
    @Path("{appId}/workflows")
    @Produces("application/json")
    fun addWorkflow(
        @PathParam("appId") appId: String,
        request: WorkflowRegistrationRequest,
    ): Uni<Unit>

    @DELETE
    @Path("{appId}/workflows/{workflowId}")
    @Produces("application/json")
    fun deleteWorkflow(
        @PathParam("appId") appId: String,
        @PathParam("workflowId") workflowId: String,
    ): Uni<Unit>

    @GET
    @Path("{appId}/workflows")
    @Produces("application/json")
    fun getAllWorkflowIdsForApp(
        @PathParam("appId") appId: String,
    ): Uni<List<String>>
}
