package org.eurodat.client.appservice.model

data class WorkflowRegistrationRequest(
    val workflowId: String,
    val imageId: String,
    val startCommand: List<String>,
)
