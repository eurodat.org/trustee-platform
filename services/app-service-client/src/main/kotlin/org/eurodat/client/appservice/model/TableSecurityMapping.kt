package org.eurodat.client.appservice.model

data class TableSecurityMapping(
    val rowBaseOutputSecurityColumn: String = "",
    val tableName: String = "",
)
