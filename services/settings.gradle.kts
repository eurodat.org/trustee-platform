// The settings file is used to specify which projects to include in your build.
rootProject.name = "services"

include("app-service")
include("app-service-client")
include("contract-service")
include("contract-service-client")
include("transaction-service")
include("transaction-service-client")
include("eurodat-client")
include("data-management-service")
include("data-management-service-client")
include("e2e")
include("credential-service-client")
include("credential-service")
include("test-common")
include("database-service")
include("database-service-client")

pluginManagement {
    val quarkusPluginVersion: String by settings
    val quarkusPluginId: String by settings
    repositories {
        mavenCentral()
        gradlePluginPortal()
        mavenLocal()
    }
    plugins {
        id(quarkusPluginId) version quarkusPluginVersion
    }
}
