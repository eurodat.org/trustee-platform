package org.eurodat.client.contractservice.model

data class ContractRequest(
    val contractType: String,
    val appId: String?,
)
