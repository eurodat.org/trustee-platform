package org.eurodat.client.contractservice.model

data class Participant(
    val firstName: String,
    val lastName: String,
    val email: String,
    val role: String,
    val status: String,
)
