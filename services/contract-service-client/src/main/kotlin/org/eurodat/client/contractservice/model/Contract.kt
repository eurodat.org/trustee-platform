package org.eurodat.client.contractservice.model

import java.time.ZonedDateTime

data class Contract(
    val id: String,
    val contractType: String,
    val appId: String?,
    val contractValidFrom: ZonedDateTime?,
    var contractValidTo: ZonedDateTime?,
    val status: String,
    val participants: List<Participant>? = null,
    val companyId: String? = null,
)
