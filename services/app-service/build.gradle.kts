plugins {
    java
    id("io.quarkus")
    kotlin("jvm")
    id("org.sonarqube") version "4.3.1.3277"
    id("jacoco")
    kotlin("plugin.allopen") version "2.1.0"
    kotlin("plugin.jpa") version "2.1.0"
}

jacoco {
    toolVersion = "0.8.10"
}

allOpen {
    annotations("jakarta.persistence.Entity")
}

val eurodatVersion: String by project
val quarkusPlatformArtifactId: String by project
val quarkusPlatformGroupId: String by project
val quarkusPlatformVersion: String by project
val quarkusPluginId: String by project
val argoClientVersion: String by project
val mockitoKotlinVersion: String by project
val javaxBindApiVersion: String by project
val yassonVersion: String by project
val javaxJsonVersion: String by project
val geronimoJsonSpecVersion: String by project
val johnzonJsonbVersion: String by project
val kotlinLoggingVersion: String by project
val kubernetesClientVersion: String by project
val wiremockVersion: String by project

group = "org.eurodat"
version = eurodatVersion

dependencies {
    implementation(project(":app-service-client"))

    implementation(enforcedPlatform("$quarkusPlatformGroupId:$quarkusPlatformArtifactId:$quarkusPlatformVersion"))
    implementation("$quarkusPluginId:quarkus-oidc:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-container-image-jib:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-hibernate-reactive-panache-kotlin:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-smallrye-health:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-smallrye-openapi:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-scheduler:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-opentelemetry:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-rest-client-jackson:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-rest-jackson:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-flyway:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-jdbc-postgresql:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-reactive-pg-client:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-smallrye-jwt:$quarkusPlatformVersion")

    implementation("io.argoproj.workflow:argo-client-java:$argoClientVersion")
    implementation("io.github.oshai:kotlin-logging-jvm:$kotlinLoggingVersion")
    implementation("io.kubernetes:client-java:$kubernetesClientVersion")

    implementation("javax.json.bind:javax.json.bind-api:$javaxBindApiVersion")
    implementation("org.eclipse:yasson:$yassonVersion")
    implementation("org.glassfish:javax.json:$javaxJsonVersion")
    implementation("org.apache.geronimo.specs:geronimo-json_1.1_spec:$geronimoJsonSpecVersion")
    implementation("org.apache.johnzon:johnzon-jsonb:$johnzonJsonbVersion")

    testImplementation("$quarkusPluginId:quarkus-test-security-oidc:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-test-oidc-server:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-jacoco:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-junit5:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-junit5-mockito:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-test-vertx:$quarkusPlatformVersion")
    testImplementation("com.marcinziolo:kotlin-wiremock:$wiremockVersion")
    testImplementation("org.testcontainers:testcontainers")
    testImplementation("org.testcontainers:postgresql")
    testImplementation("io.rest-assured:rest-assured")

    testImplementation("org.mockito.kotlin:mockito-kotlin:$mockitoKotlinVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

tasks.withType<Test> {
    systemProperty("java.util.logging.manager", "org.jboss.logmanager.LogManager")
    finalizedBy("jacocoTestReport")

    configure<JacocoTaskExtension> {
        excludeClassLoaders = listOf("*QuarkusClassLoader")
        destinationFile =
            layout.buildDirectory
                .file("jacoco/jacoco-quarkus.exec")
                .get()
                .asFile
    }
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
    options.compilerArgs.add("-parameters")
}

tasks.withType<Jar>().configureEach {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}
