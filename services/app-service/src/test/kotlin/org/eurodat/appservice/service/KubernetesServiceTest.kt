package org.eurodat.appservice.service

import io.kubernetes.client.openapi.apis.BatchV1Api
import io.kubernetes.client.openapi.apis.CoreV1Api
import io.kubernetes.client.openapi.models.V1ConfigMap
import io.kubernetes.client.openapi.models.V1Job
import io.kubernetes.client.openapi.models.V1Secret
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.argThat
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.eq
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

@QuarkusTest
class KubernetesServiceTest {
    @Inject
    private lateinit var kubernetesService: KubernetesService
    private val jobStatusReadRequest: BatchV1Api.APIreadNamespacedJobStatusRequest =
        mock {
            on { execute() } doReturn mock<V1Job>()
        }
    private val createJobRequest: BatchV1Api.APIcreateNamespacedJobRequest =
        mock {
            on { execute() } doReturn mock<V1Job>()
        }
    private val namespace = "some-namespace"
    private val resourceName = "some-resource-name"
    private val someKey = "some-key"
    private val someValue = "some-value"
    private val someData = mapOf(someKey to someValue)
    private val secretDataAsByteArray = someData.mapValues { it.value.toByteArray() }
    private val v1Secret = V1Secret().data(secretDataAsByteArray)
    private val v1Configmap = V1ConfigMap().data(someData)
    private val readConfigmapRequest: CoreV1Api.APIreadNamespacedConfigMapRequest =
        mock {
            on { execute() } doReturn v1Configmap
        }
    private val deleteConfigMapRequest: CoreV1Api.APIdeleteNamespacedConfigMapRequest = mock()
    private val readSecretRequest: CoreV1Api.APIreadNamespacedSecretRequest =
        mock {
            on { execute() } doReturn v1Secret
        }
    private val createSecretRequest: CoreV1Api.APIcreateNamespacedSecretRequest =
        mock {
            on { execute() } doReturn v1Secret
        }
    private val deleteSecretRequest: CoreV1Api.APIdeleteNamespacedSecretRequest = mock()
    private val batchV1Api: BatchV1Api =
        mock {
            on { readNamespacedJobStatus(any(), any()) } doReturn jobStatusReadRequest
            on { createNamespacedJob(any(), any()) } doReturn createJobRequest
        }
    private val coreV1Api: CoreV1Api =
        mock {
            on { readNamespacedSecret(any(), any()) } doReturn readSecretRequest
            on { createNamespacedSecret(any(), any()) } doReturn createSecretRequest
            on { deleteNamespacedSecret(any(), any()) } doReturn deleteSecretRequest
            on { readNamespacedConfigMap(any(), any()) } doReturn readConfigmapRequest
            on { deleteNamespacedConfigMap(any(), any()) } doReturn deleteConfigMapRequest
        }

    @BeforeEach
    fun setup() {
        kubernetesService.batchApi = batchV1Api
        kubernetesService.coreApi = coreV1Api
    }

    @Test
    fun testReadJob() {
        kubernetesService.readJob(resourceName, namespace)
        verify(batchV1Api).readNamespacedJobStatus(resourceName, namespace)
    }

    @Test
    fun testCreateJob() {
        val v1Job: V1Job = mock()
        kubernetesService.createJob(v1Job, namespace)
        verify(batchV1Api).createNamespacedJob(namespace, v1Job)
    }

    @Test
    fun testReadSecret() {
        val secret = kubernetesService.readSecret(resourceName, namespace)
        verify(coreV1Api).readNamespacedSecret(resourceName, namespace)
        assert(secret[someKey] == someValue)
    }

    @Test
    fun createSecret() {
        kubernetesService.createSecret(resourceName, someData, namespace)
        verify(coreV1Api).createNamespacedSecret(
            eq(namespace),
            argThat { data[someKey].contentEquals(secretDataAsByteArray[someKey]) && this.metadata.name == resourceName },
        )
    }

    @Test
    fun deleteSecret() {
        kubernetesService.deleteConfigMap(resourceName, namespace)
        verify(coreV1Api).deleteNamespacedConfigMap(resourceName, namespace)
    }

    @Test
    fun readConfigMap() {
        val configMap = kubernetesService.readConfigMap(resourceName, namespace)
        verify(coreV1Api).readNamespacedConfigMap(resourceName, namespace)
        assert(configMap.getValue(someKey) == someValue)
    }
}
