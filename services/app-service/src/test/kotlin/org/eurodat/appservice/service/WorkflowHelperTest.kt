package org.eurodat.appservice.service

import io.argoproj.workflow.apis.ApiException
import io.argoproj.workflow.apis.WorkflowTemplateServiceApi
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1WorkflowTemplate
import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.MockitoConfig
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.inject.Inject
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.core.Response
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.appservice.model.AppEntity
import org.eurodat.appservice.model.WorkflowEntity
import org.eurodat.appservice.repository.AppRepository
import org.eurodat.appservice.repository.WorkflowRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@QuarkusTest
@RunOnVertxContext
class WorkflowHelperTest {
    @ConfigProperty(name = "workflow-template.namespace")
    private lateinit var workflowTemplateNamespace: String

    @Inject
    private lateinit var workflowHelper: WorkflowHelper

    @InjectMock
    private lateinit var workflowRepository: WorkflowRepository

    @InjectMock
    private lateinit var appRepository: AppRepository

    @InjectMock
    @MockitoConfig(convertScopes = true)
    @RestClient
    private lateinit var workflowTemplateClient: WorkflowTemplateServiceApi

    private val appId = "some-app-id"
    private val workflowId = "some-workflow-id"
    private val workflowDatabaseId = 64
    private val sampleWorkflow: WorkflowEntity =
        mock {
            on { workflowId } doReturn workflowId
            on { workflowDatabaseId } doReturn workflowDatabaseId
            on { resourceName } doReturn "some-resource-name"
        }

    private val sampleApp: AppEntity =
        mock {
            on { appId } doReturn appId
            on { workflows } doReturn mutableListOf(sampleWorkflow)
        }

    @Test
    fun `deleting non existent workflow returns 404`(uniAsserter: UniAsserter) {
        uniAsserter.assertFailedWith({
            whenever(appRepository.findApp(appId)).doReturn(uni { sampleApp })
            workflowHelper.deleteWorkflow(appId, "workflowTemplateYok")
        }, NotFoundException::class.java)
    }

    @Test
    fun `delete workflow with missing workflow template`(uniAsserter: UniAsserter) {
        uniAsserter.assertThat({
            whenever(appRepository.findApp(appId)).doReturn(uni { sampleApp })
            whenever(workflowRepository.deleteWorkflowById(workflowDatabaseId)).doReturn(uni { true })
            whenever(
                workflowTemplateClient.workflowTemplateServiceDeleteWorkflowTemplate(
                    workflowTemplateNamespace,
                    sampleWorkflow.resourceName,
                    "0",
                    null,
                    null,
                    true,
                    "Background",
                    null,
                ),
            ).doReturn(Uni.createFrom().failure { ApiException(Response.status(404).build()) })
            workflowHelper.deleteWorkflow(appId, workflowId)
        }, {
            verify(workflowRepository).deleteWorkflowById(workflowDatabaseId)
            verify(workflowTemplateClient).workflowTemplateServiceDeleteWorkflowTemplate(
                workflowTemplateNamespace,
                sampleWorkflow.resourceName,
                "0",
                null,
                null,
                true,
                "Background",
                null,
            )
        })
    }

    @Test
    fun `delete workflow with workflow template error`(uniAsserter: UniAsserter) {
        uniAsserter.assertFailedWith({
            whenever(appRepository.findApp(appId)).doReturn(uni { sampleApp })
            whenever(workflowRepository.deleteWorkflowById(workflowDatabaseId)).doReturn(uni { true })
            whenever(
                workflowTemplateClient.workflowTemplateServiceDeleteWorkflowTemplate(
                    workflowTemplateNamespace,
                    sampleWorkflow.resourceName,
                    "0",
                    null,
                    null,
                    true,
                    "Background",
                    null,
                ),
            ).doReturn(Uni.createFrom().failure { ApiException(Response.status(500).build()) })
            workflowHelper.deleteWorkflow(appId, workflowId)
        }, {
            Assertions.assertInstanceOf(ApiException::class.java, it)
            Assertions.assertEquals(500, (it as ApiException).response.status)
        })
    }

    @Test
    fun `delete workflow`(uniAsserter: UniAsserter) {
        uniAsserter.assertThat({
            whenever(appRepository.findApp(appId)).doReturn(uni { sampleApp })
            whenever(workflowRepository.deleteWorkflowById(workflowDatabaseId)).doReturn(uni { true })
            workflowHelper.deleteWorkflow(appId, workflowId)
        }, {
            verify(workflowRepository).deleteWorkflowById(workflowDatabaseId)
            verify(workflowTemplateClient).workflowTemplateServiceDeleteWorkflowTemplate(
                workflowTemplateNamespace,
                sampleWorkflow.resourceName,
                "0",
                null,
                null,
                true,
                "Background",
                null,
            )
        })
    }

    @Test
    fun `creating workflow template with duplicate ID returns 409`(uniAsserter: UniAsserter) {
        uniAsserter.assertFailedWith(
            {
                val templateMock: IoArgoprojWorkflowV1alpha1WorkflowTemplate = mock()
                whenever(appRepository.findApp(appId)).doReturn(uni { sampleApp })
                whenever(workflowTemplateClient.workflowTemplateServiceCreateWorkflowTemplate(any(), any())).doReturn(
                    Uni.createFrom().failure(ApiException(Response.status(409).build())),
                )
                workflowHelper.addWorkflowTemplateToArgo(templateMock)
            },
            { it is ApiException && it.response.status == 409 },
        )
    }

    @Test
    fun `create workflow template`(uniAsserter: UniAsserter) {
        uniAsserter.assertThat(
            {
                val templateMock: IoArgoprojWorkflowV1alpha1WorkflowTemplate = mock()
                whenever(appRepository.findApp(appId)).doReturn(uni { sampleApp })
                whenever(workflowRepository.addWorkflow(any())).doReturn(uni { sampleWorkflow })
                workflowHelper.addWorkflowTemplateToArgo(templateMock)
            },
            {
                verify(workflowTemplateClient).workflowTemplateServiceCreateWorkflowTemplate(any(), any())
            },
        )
    }
}
