package org.eurodat.appservice.service

import io.kubernetes.client.openapi.ApiException
import io.kubernetes.client.openapi.models.V1Job
import io.kubernetes.client.openapi.models.V1JobStatus
import io.kubernetes.client.openapi.models.V1ObjectMeta
import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eurodat.appservice.model.AppEntity
import org.eurodat.appservice.model.ImageEntity
import org.eurodat.appservice.model.ImageStatus
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.argThat
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.eq
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import java.util.UUID

@QuarkusTest
class KubernetesImageJobSchedulerTest {
    @ConfigProperty(name = "registry.address")
    private lateinit var destinationRegistry: String

    @ConfigProperty(name = "uploader.namespace")
    private lateinit var namespace: String

    @ConfigProperty(name = "uploader.service-account")
    private lateinit var serviceAccount: String

    private val runningJob = "running-job"
    private val username: String = "AbdiSued"
    private val password: String = "Celo385"

    @InjectMock
    private lateinit var kubeService: KubernetesService

    @Inject
    private lateinit var scheduler: KubernetesImageJobScheduler

    private fun setupJobApiCalls() {
        whenever(kubeService.createJob(any(), eq(namespace))).thenReturn(
            V1Job().apply {
                metadata = V1ObjectMeta().apply { name = "image-upload" }
            },
        )
        whenever(kubeService.readJob(runningJob, namespace)).doReturn(V1Job().apply { status = V1JobStatus().apply { active = 1 } })
    }

    @Test
    fun `scheduler initializes`() {
        assertNotNull(scheduler)
    }

    @Test
    @RunOnVertxContext
    fun `scheduler starts upload job`(uniAsserter: UniAsserter) {
        val entity =
            ImageEntity(
                imageId = UUID.randomUUID().toString(),
                sourceLocation = "localhost:5002/app-service",
                status = ImageStatus.UPLOADING,
                app = AppEntity(appId = "appid", appOwner = "appOwner", transactionTimeoutInDays = 30),
                credentialsSupplied = false,
            )
        uniAsserter.assertThat(
            {
                setupJobApiCalls()
                scheduler.startImageUpload(entity)
            },
            {
                verify(kubeService).createJob(
                    argThat {
                        metadata.name == "upload-${entity.imageId}" &&
                            metadata.namespace == namespace &&
                            spec.template.metadata.labels["app"] == "upload-app-image" &&
                            spec.template.spec.serviceAccountName == serviceAccount &&
                            spec.template.spec.containers[0]
                                .args[1] == "upload" &&
                            spec.template.spec.containers[0]
                                .args[3] == entity.sourceLocation &&
                            spec.template.spec.containers[0]
                                .args[5] == "$destinationRegistry/${entity.app.appId.lowercase()}/${entity.imageId}"
                    },
                    eq(namespace),
                )
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun `scheduler starts delete job`(uniAsserter: UniAsserter) {
        val entity =
            ImageEntity(
                imageId = UUID.randomUUID().toString(),
                sourceLocation = "localhost:5002/hello",
                status = ImageStatus.DELETING,
                app = AppEntity(appId = "appid", appOwner = "appOwner", transactionTimeoutInDays = 30),
                credentialsSupplied = false,
            )

        uniAsserter.assertThat(
            {
                setupJobApiCalls()
                scheduler.startImageDeletion(entity)
            },
            {
                verify(kubeService).createJob(
                    argThat {
                        metadata.name == "delete-${entity.imageId}" &&
                            metadata.namespace == namespace &&
                            spec.template.metadata.labels["app"] == "upload-app-image" &&
                            spec.template.spec.serviceAccountName == serviceAccount &&
                            spec.template.spec.containers[0]
                                .args[1] == "delete" &&
                            spec.template.spec.containers[0]
                                .args[3] == "$destinationRegistry/${entity.app.appId.lowercase()}/${entity.imageId}"
                    },
                    eq(namespace),
                )
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun `scheduler finds job status`(uniAsserter: UniAsserter) {
        uniAsserter.assertEquals(
            {
                setupJobApiCalls()
                scheduler.findImageJobStatus(runningJob)
            },
            ImageJobStatus.RUNNING,
        )
    }

    @Test
    @RunOnVertxContext
    fun `scheduler adds credentials to job template`(uniAsserter: UniAsserter) {
        val entity =
            ImageEntity(
                imageId = UUID.randomUUID().toString(),
                sourceLocation = "localhost:5002/app-service",
                status = ImageStatus.UPLOADING,
                app = AppEntity(appId = "appid", appOwner = "appOwner", transactionTimeoutInDays = 30),
                credentialsSupplied = true,
            )
        uniAsserter.assertThat(
            {
                setupJobApiCalls()
                whenever(kubeService.readSecret(eq(entity.imageId), eq(namespace))).doReturn(
                    mutableMapOf(
                        "username" to username,
                        "password" to password,
                    ),
                )
                scheduler.startImageUpload(entity)
            },
            {
                verify(kubeService).createJob(
                    argThat {
                        spec.template.spec.containers[0]
                            .env[0]
                            .name == "IMAGE_CRED_USERNAME" &&
                            spec.template.spec.containers[0]
                                .env[0]
                                .value == username &&
                            spec.template.spec.containers[0]
                                .env[1]
                                .name == "IMAGE_CRED_PASSWORD" &&
                            spec.template.spec.containers[0]
                                .env[1]
                                .value == password
                    },
                    eq(namespace),
                )
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun `scheduler emits terminal failure if credentials are missing`(uniAsserter: UniAsserter) {
        val entity =
            ImageEntity(
                imageId = UUID.randomUUID().toString(),
                sourceLocation = "localhost:5002/app-service",
                status = ImageStatus.UPLOADING,
                app = AppEntity(appId = "appid", appOwner = "appOwner", transactionTimeoutInDays = 30),
                credentialsSupplied = true,
            )
        uniAsserter.assertFailedWith(
            {
                setupJobApiCalls()
                whenever(kubeService.readSecret(eq(entity.imageId), eq(namespace))).then {
                    throw ApiException(404, "")
                }
                scheduler.startImageUpload(entity)
            },
            TerminalJobCreationException::class.java,
        )
    }

    @Test
    @RunOnVertxContext
    fun `scheduler emits kubernetes api exception if it is not 404`(uniAsserter: UniAsserter) {
        val entity =
            ImageEntity(
                imageId = UUID.randomUUID().toString(),
                sourceLocation = "localhost:5002/app-service",
                status = ImageStatus.UPLOADING,
                app = AppEntity(appId = "appid", appOwner = "appOwner", transactionTimeoutInDays = 30),
                credentialsSupplied = true,
            )
        uniAsserter.assertFailedWith(
            {
                setupJobApiCalls()
                whenever(kubeService.readSecret(eq(entity.imageId), eq(namespace))).then {
                    throw ApiException()
                }
                scheduler.startImageUpload(entity)
            },
            ApiException::class.java,
        )
    }
}
