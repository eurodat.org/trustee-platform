package org.eurodat.appservice.service

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.inject.Inject
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eurodat.appservice.model.AppEntity
import org.eurodat.appservice.model.ImageEntity
import org.eurodat.appservice.model.ImageStatus
import org.eurodat.appservice.repository.AppRepository
import org.hibernate.reactive.mutiny.Mutiny.SessionFactory
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.argThat
import org.mockito.kotlin.never
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import java.util.UUID

@QuarkusTest
@RunOnVertxContext
class ImageLifecycleTest {
    @ConfigProperty(name = "uploader.namespace")
    private lateinit var namespace: String

    @Inject
    private lateinit var appRepository: AppRepository

    @Inject
    private lateinit var imageLifecycle: ImageLifecycle

    @Inject
    private lateinit var sessionFactory: SessionFactory

    @InjectMock
    private lateinit var imageJobScheduler: ImageJobScheduler

    @InjectMock
    private lateinit var kubernetesService: KubernetesService

    private fun uuid() = UUID.randomUUID().toString()

    private val appOwner = "appOwner"

    private fun getConfigMapName(imageId: String) = "image-location-$imageId"

    private val newLocation = "bogus-location"

    private fun kubernetesServiceConfigMapMockSetup() {
        whenever(kubernetesService.readConfigMap(any(), any())).thenReturn(mapOf("image-location" to newLocation))
    }

    @Test
    fun `transitions submitted images`(asserter: UniAsserter) {
        asserter.assertThat(
            {
                whenever(imageJobScheduler.startImageUpload(any())).thenReturn(uni { "jobId" })
                whenever(imageJobScheduler.findImageJobStatus("jobId")).thenReturn(uni { ImageJobStatus.RUNNING })
                transitionImagesAndReturnApp(
                    {
                        ImageEntity(
                            imageId = uuid(),
                            sourceLocation = "localhost:5002/hello",
                            status = ImageStatus.UPLOADING,
                            app = it,
                            credentialsSupplied = false,
                        )
                    },
                    {
                        ImageEntity(
                            imageId = uuid(),
                            sourceLocation = "localhost:5002/hello",
                            status = ImageStatus.UPLOADING,
                            app = it,
                            credentialsSupplied = false,
                        )
                    },
                )
            },
            {
                verify(imageJobScheduler, times(2)).startImageUpload(any())
                Assertions.assertEquals(listOf(ImageStatus.UPLOADING, ImageStatus.UPLOADING), it.images.map { i -> i.status })
            },
        )
    }

    @Test
    fun `does not transition image if failing to start upload`(asserter: UniAsserter) {
        asserter.assertThat(
            {
                whenever(imageJobScheduler.startImageUpload(argThat { sourceLocation.endsWith("failing") })).thenReturn(
                    Uni.createFrom().failure(Exception("error starting upload")),
                )
                transitionImagesAndReturnApp({
                    ImageEntity(
                        imageId = uuid(),
                        sourceLocation = "localhost:5002/failing",
                        status = ImageStatus.UPLOADING,
                        app = it,
                        credentialsSupplied = false,
                    )
                })
            },
            {
                verify(imageJobScheduler).startImageUpload(any())
                Assertions.assertEquals(listOf(ImageStatus.UPLOADING), it.images.map { i -> i.status })
                Assertions.assertNull(it.images[0].uploadJob)
            },
        )
    }

    @Test
    fun `cleans up secret if failing with terminal exception`(asserter: UniAsserter) {
        val imageId = uuid()
        asserter.assertThat(
            {
                whenever(imageJobScheduler.startImageUpload(argThat { sourceLocation.endsWith("failing") })).thenReturn(
                    Uni.createFrom().failure(TerminalJobCreationException(Exception())),
                )
                transitionImagesAndReturnApp({
                    ImageEntity(
                        imageId = imageId,
                        sourceLocation = "localhost:5002/failing",
                        status = ImageStatus.UPLOADING,
                        app = it,
                        credentialsSupplied = true,
                    )
                })
            },
            {
                verify(kubernetesService).deleteSecret(imageId, namespace)
            },
        )
    }

    @Test
    fun `transitions one image if failing to start upload for another`(asserter: UniAsserter) {
        asserter.assertThat(
            {
                whenever(imageJobScheduler.startImageUpload(argThat { sourceLocation.endsWith("hello") })).thenReturn(uni { "jobId" })
                whenever(imageJobScheduler.startImageUpload(argThat { sourceLocation.endsWith("failing") })).thenReturn(
                    Uni.createFrom().failure(Exception("error starting upload")),
                )
                whenever(imageJobScheduler.findImageJobStatus(any())).thenReturn(uni { ImageJobStatus.RUNNING })
                transitionImagesAndReturnApp(
                    {
                        ImageEntity(
                            imageId = uuid(),
                            sourceLocation = "localhost:5002/hello",
                            status = ImageStatus.UPLOADING,
                            app = it,
                            credentialsSupplied = false,
                        )
                    },
                    {
                        ImageEntity(
                            imageId = uuid(),
                            sourceLocation = "localhost:5002/failing",
                            status = ImageStatus.UPLOADING,
                            app = it,
                            credentialsSupplied = false,
                        )
                    },
                )
            },
            {
                val images = it.images
                verify(imageJobScheduler).startImageUpload(argThat { imageId == images[0].imageId })
                verify(imageJobScheduler).startImageUpload(argThat { imageId == images[1].imageId })
                Assertions.assertEquals(listOf(ImageStatus.UPLOADING, ImageStatus.UPLOADING), it.images.map { i -> i.status })
                Assertions.assertEquals(listOf(null, "jobId"), it.images.map { i -> i.uploadJob })
            },
        )
    }

    @Test
    fun `transitions completed uploading image`(asserter: UniAsserter) {
        asserter.assertThat(
            {
                whenever(imageJobScheduler.findImageJobStatus(any())).thenReturn(uni { ImageJobStatus.COMPLETED })
                kubernetesServiceConfigMapMockSetup()
                transitionImagesAndReturnApp({
                    ImageEntity(
                        imageId = uuid(),
                        sourceLocation = "localhost:5002/hello",
                        status = ImageStatus.UPLOADING,
                        app = it,
                        uploadJob = "job",
                        credentialsSupplied = false,
                    )
                })
            },
            {
                Assertions.assertTrue(it.images.size == 1)
                Assertions.assertEquals(listOf(ImageStatus.ACTIVE), it.images.map { i -> i.status })
                Assertions.assertEquals(newLocation, it.images[0].location)
            },
        )
    }

    @Test
    fun `transitions running uploading image`(asserter: UniAsserter) {
        val imageId = uuid()
        asserter.assertThat(
            {
                whenever(imageJobScheduler.findImageJobStatus(any())).thenReturn(uni { ImageJobStatus.RUNNING })
                transitionImagesAndReturnApp({
                    ImageEntity(
                        imageId = imageId,
                        sourceLocation = "localhost:5002/hello",
                        status = ImageStatus.UPLOADING,
                        app = it,
                        uploadJob = "job",
                        credentialsSupplied = false,
                    )
                })
            },
            {
                verify(imageJobScheduler, never()).startImageUpload(argThat { this.imageId == imageId })
                Assertions.assertEquals(listOf(ImageStatus.UPLOADING), it.images.map { i -> i.status })
            },
        )
    }

    @Test
    fun `credentials and configmap are cleaned up after upload succeeded`(asserter: UniAsserter) {
        val imageId = uuid()
        asserter.assertThat(
            {
                whenever(imageJobScheduler.findImageJobStatus(any())).thenReturn(uni { ImageJobStatus.COMPLETED })
                kubernetesServiceConfigMapMockSetup()
                transitionImagesAndReturnApp({
                    ImageEntity(
                        imageId = imageId,
                        sourceLocation = "localhost:5002/hello",
                        status = ImageStatus.UPLOADING,
                        app = it,
                        uploadJob = "job",
                        credentialsSupplied = true,
                    )
                })
            },
            {
                verify(kubernetesService).deleteSecret(imageId, namespace)
                verify(kubernetesService).deleteConfigMap(getConfigMapName(imageId), namespace)
            },
        )
    }

    @Test
    fun `credentials are cleaned up after upload failed`(asserter: UniAsserter) {
        val imageId = uuid()
        asserter.assertThat(
            {
                whenever(imageJobScheduler.findImageJobStatus(any())).thenReturn(uni { ImageJobStatus.FAILED })
                transitionImagesAndReturnApp({
                    ImageEntity(
                        imageId = imageId,
                        sourceLocation = "localhost:5002/hello",
                        status = ImageStatus.UPLOADING,
                        app = it,
                        uploadJob = "job",
                        credentialsSupplied = true,
                    )
                })
            },
            {
                verify(kubernetesService).deleteSecret(imageId, namespace)
            },
        )
    }

    @Test
    fun `transitions failed uploading image`(asserter: UniAsserter) {
        asserter.assertThat(
            {
                whenever(imageJobScheduler.findImageJobStatus(any())).thenReturn(uni { ImageJobStatus.FAILED })
                transitionImagesAndReturnApp({
                    ImageEntity(
                        imageId = uuid(),
                        sourceLocation = "localhost:5002/hello",
                        status = ImageStatus.UPLOADING,
                        app = it,
                        uploadJob = "job",
                        credentialsSupplied = false,
                    )
                })
            },
            {
                Assertions.assertEquals(listOf(ImageStatus.FAILED), it.images.map { i -> i.status })
            },
        )
    }

    @Test
    fun `transitions completed image when one request is failing`(asserter: UniAsserter) {
        asserter.assertThat(
            {
                whenever(imageJobScheduler.findImageJobStatus(any())).thenReturn(uni { ImageJobStatus.COMPLETED })
                whenever(imageJobScheduler.findImageJobStatus("jobFailing")).thenReturn(
                    Uni.createFrom().failure(Exception("error getting upload status")),
                )
                kubernetesServiceConfigMapMockSetup()
                transitionImagesAndReturnApp(
                    {
                        ImageEntity(
                            imageId = uuid(),
                            sourceLocation = "localhost:5002/hello",
                            status = ImageStatus.UPLOADING,
                            app = it,
                            uploadJob = "job",
                            credentialsSupplied = false,
                        )
                    },
                    {
                        ImageEntity(
                            imageId = uuid(),
                            sourceLocation = "localhost:5002/failing",
                            status = ImageStatus.UPLOADING,
                            app = it,
                            uploadJob = "jobFailing",
                            credentialsSupplied = false,
                        )
                    },
                )
            },
            {
                Assertions.assertEquals(listOf(ImageStatus.UPLOADING, ImageStatus.ACTIVE), it.images.map { i -> i.status })
            },
        )
    }

    @Test
    fun `transitions deleting image without a job`(asserter: UniAsserter) {
        asserter.assertThat(
            {
                whenever(imageJobScheduler.startImageDeletion(any())).thenReturn(uni { "deleteJobId" })
                whenever(imageJobScheduler.findImageJobStatus("deleteJobId")).thenReturn(uni { ImageJobStatus.RUNNING })
                transitionImagesAndReturnApp({
                    ImageEntity(
                        imageId = uuid(),
                        sourceLocation = "localhost:5002/hello",
                        status = ImageStatus.DELETING,
                        app = it,
                        uploadJob = "jobFailing",
                        credentialsSupplied = false,
                    )
                })
            },
            {
                val imageEntity = it.images[0]
                verify(imageJobScheduler).startImageDeletion(imageEntity)
                Assertions.assertEquals(ImageStatus.DELETING, imageEntity.status)
                Assertions.assertEquals("deleteJobId", imageEntity.deleteJob)
            },
        )
    }

    @Test
    fun `transitions completed deleting image with a job`(asserter: UniAsserter) {
        asserter.assertThat(
            {
                whenever(imageJobScheduler.findImageJobStatus(any())).thenReturn(uni { ImageJobStatus.COMPLETED })
                kubernetesServiceConfigMapMockSetup()
                transitionImagesAndReturnApp({
                    ImageEntity(
                        imageId = uuid(),
                        sourceLocation = "localhost:5002/hello",
                        status = ImageStatus.DELETING,
                        app = it,
                        deleteJob = "deleteJobId",
                        credentialsSupplied = false,
                    )
                })
            },
            {
                val imageEntity = it.images[0]
                verify(imageJobScheduler, never()).startImageDeletion(imageEntity)
                Assertions.assertEquals(ImageStatus.DELETED, imageEntity.status)
            },
        )
    }

    @Test
    fun `transitions running deleting image with a job`(asserter: UniAsserter) {
        asserter.assertThat(
            {
                whenever(imageJobScheduler.findImageJobStatus(any())).thenReturn(uni { ImageJobStatus.RUNNING })
                transitionImagesAndReturnApp({
                    ImageEntity(
                        imageId = uuid(),
                        sourceLocation = "localhost:5002/hello",
                        status = ImageStatus.DELETING,
                        app = it,
                        deleteJob = "deleteJobId",
                        credentialsSupplied = false,
                    )
                })
            },
            {
                val imageEntity = it.images[0]
                verify(imageJobScheduler, never()).startImageDeletion(imageEntity)
                Assertions.assertEquals(ImageStatus.DELETING, imageEntity.status)
            },
        )
    }

    @Test
    fun `transitions failed deleting image with a job`(asserter: UniAsserter) {
        asserter.assertThat(
            {
                whenever(imageJobScheduler.findImageJobStatus(any())).thenReturn(uni { ImageJobStatus.FAILED })
                transitionImagesAndReturnApp({
                    ImageEntity(
                        imageId = uuid(),
                        sourceLocation = "localhost:5002/hello",
                        status = ImageStatus.DELETING,
                        app = it,
                        deleteJob = "deleteJobId",
                        credentialsSupplied = false,
                    )
                })
            },
            {
                val imageEntity = it.images[0]
                verify(imageJobScheduler, never()).startImageDeletion(imageEntity)
                Assertions.assertEquals(ImageStatus.FAILED, imageEntity.status)
            },
        )
    }

    private fun transitionImagesAndReturnApp(vararg images: (AppEntity) -> ImageEntity): Uni<AppEntity> {
        val appEntity = AppEntity(appId = uuid(), appOwner = appOwner, transactionTimeoutInDays = 30)
        return sessionFactory
            .withTransaction { _ -> appRepository.startAppRegistration(appEntity) }
            .chain { app ->
                sessionFactory.withTransaction { _ ->
                    appRepository
                        .findApp(app.appId)
                        .map {
                            it!!.apply {
                                images.forEach { image -> addImage(image.invoke(it)) }
                            }
                        }
                }
            }.chain { _ ->
                sessionFactory.withTransaction { _ -> imageLifecycle.transitionImages() }
            }.chain { _ ->
                sessionFactory.withSession {
                    appRepository.findApp(appEntity.appId).map { it!! }
                }
            }.call { _ ->
                sessionFactory.withTransaction { _ -> appRepository.deleteApp(appEntity.appId) }
            }
    }
}
