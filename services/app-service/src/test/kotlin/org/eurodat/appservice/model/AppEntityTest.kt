package org.eurodat.appservice.model

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource
import java.util.UUID

class AppEntityTest {
    @ParameterizedTest
    @EnumSource(ImageStatus::class, mode = EnumSource.Mode.EXCLUDE, names = ["DELETED"])
    fun `find image returns image`(status: ImageStatus) {
        val entity = AppEntity(null, UUID.randomUUID().toString(), transactionTimeoutInDays = 30)
        ImageEntity(
            imageId = UUID.randomUUID().toString(),
            sourceLocation = "image1",
            status = ImageStatus.ACTIVE,
            app = entity,
            credentialsSupplied = false,
        ).also { entity.addImage(it) }
        val image2 =
            ImageEntity(
                imageId = UUID.randomUUID().toString(),
                sourceLocation = "image2",
                status = status,
                app = entity,
                credentialsSupplied = false,
            ).also {
                entity.addImage(
                    it,
                )
            }

        Assertions.assertEquals(image2, entity.findImage(image2.imageId))
    }

    @Test
    fun `find image returns null`() {
        val entity = AppEntity(null, UUID.randomUUID().toString(), transactionTimeoutInDays = 30)
        ImageEntity(
            imageId = UUID.randomUUID().toString(),
            sourceLocation = "image1",
            status = ImageStatus.ACTIVE,
            app = entity,
            credentialsSupplied = false,
        ).also { entity.addImage(it) }

        Assertions.assertNull(entity.findImage("something"))
    }

    @Test
    fun `find image also returns deleted image`() {
        val entity = AppEntity(null, UUID.randomUUID().toString(), transactionTimeoutInDays = 30)
        val image =
            ImageEntity(
                imageId = UUID.randomUUID().toString(),
                sourceLocation = "image1",
                status = ImageStatus.DELETED,
                app = entity,
                credentialsSupplied = false,
            ).also { entity.addImage(it) }
        Assertions.assertEquals(image, entity.findImage(image.imageId))
    }
}
