package org.eurodat.appservice.resource

import io.quarkus.test.common.http.TestHTTPEndpoint
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.security.TestSecurity
import io.restassured.RestAssured
import org.hamcrest.Matchers.not
import org.junit.jupiter.api.Test

@TestHTTPEndpoint(AppResource::class)
@QuarkusTest
class AppResourceScopeTest {
    @Test
    @TestSecurity(user = "client", permissions = ["openid"])
    fun testAppRegistrationWithoutPermission() {
        RestAssured
            .given()
            .contentType("application/json")
            .body(
                """
                   {
                       "id": "1234",
                       "transactionDDL": "transactionDDL",
                       "safedepositDDL": "safedepositDDL",
                       "tableSecurityMapping":
                       {
                           "rowBaseOutputSecurityColumn": "123",
                           "tableName": "123"
                       }
                   }
                """,
            )
            .post()
            .then()
            .statusCode(403)
    }

    @Test
    @TestSecurity(user = "client", permissions = ["openid", "app:write"])
    fun testAppRegistrationWithPermission() {
        RestAssured
            .given()
            .contentType("application/json")
            .body(
                """
                   {
                       "id": "1234",
                       "transactionDDL": "transactionDDL",
                       "safedepositDDL": "safedepositDDL",
                       "tableSecurityMapping":
                       {
                           "rowBaseOutputSecurityColumn": "123",
                           "tableName": "123"
                       }
                   }
                """,
            )
            .post()
            .then()
            .statusCode(not(403))
    }
}
