package org.eurodat.appservice.resource

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.MockitoConfig
import io.quarkus.test.security.TestSecurity
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.inject.Inject
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.ClientErrorException
import jakarta.ws.rs.ForbiddenException
import jakarta.ws.rs.NotFoundException
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eurodat.appservice.model.AppEntity
import org.eurodat.appservice.model.ImageEntity
import org.eurodat.appservice.model.ImageStatus
import org.eurodat.appservice.model.TableSecurityMappingEntity
import org.eurodat.appservice.model.WorkflowEntity
import org.eurodat.appservice.repository.AppRepository
import org.eurodat.appservice.service.WorkflowHelper
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.appservice.model.AppResponse
import org.eurodat.client.appservice.model.TableSecurityMapping
import org.eurodat.client.appservice.model.WorkflowDefinition
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.kotlin.argThat
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@QuarkusTest
@RunOnVertxContext
@TestSecurity(user = "testUser", permissions = ["app:write"])
class AppResourceTest {
    @InjectMock
    @MockitoConfig(convertScopes = true)
    private lateinit var appRepository: AppRepository

    @Inject
    private lateinit var appResource: AppResource

    @InjectMock
    @MockitoConfig(convertScopes = true)
    private lateinit var jwt: JsonWebToken

    @InjectMock
    @MockitoConfig(convertScopes = true)
    internal lateinit var workflowHelper: WorkflowHelper

    private val appId = "testAppId"
    private val appOwner = "app_provider"
    private val transactionDDL = "testTransactionDDL"
    private val safeDepositDDL = "testSafeDepositDDL"
    private val tableSecurityMappings =
        listOf(
            TableSecurityMapping("testRowBaseOutputSecurityColumn_1", "testTableName_1"),
            TableSecurityMapping("testRowBaseOutputSecurityColumn_2", "testTableName_2"),
        )

    private val sampleAppRegistration =
        AppRegistration(
            appId,
            transactionDDL,
            safeDepositDDL,
            tableSecurityMappings,
        )

    private val sampleAppRegistrationWithExplicitTimeout =
        AppRegistration(
            appId,
            transactionDDL,
            safeDepositDDL,
            tableSecurityMappings,
            11,
        )

    private val sampleImage: ImageEntity = mock { on { imageId } doReturn "image-60439" }
    private val sampleWorkflow: WorkflowEntity =
        mock {
            on { workflowId } doReturn "workflow-60529"
            on { resourceName } doReturn "resource-60529"
            on { entryPoint } doReturn "start-60529"
            on { image } doReturn sampleImage
            on { startCommand } doReturn listOf("start", "63263")
        }
    private val sampleWorkflows =
        listOf(
            WorkflowDefinition(
                sampleWorkflow.workflowId,
                sampleWorkflow.resourceName,
                sampleWorkflow.entryPoint,
                sampleWorkflow.startCommand,
                sampleImage.imageId,
            ),
        )
    private val sampleApp: AppEntity =
        mock {
            on { appId } doReturn appId
            on { appOwner } doReturn appOwner
            on { transactionDDL } doReturn transactionDDL
            on { safeDepositDDL } doReturn safeDepositDDL
            on { tableSecurityMappings } doReturn mapTSMDataClassesToEntities(tableSecurityMappings)
            on { workflows } doReturn mutableListOf(sampleWorkflow)
        }

    private fun mapTSMDataClassesToEntities(entities: List<TableSecurityMapping>): List<TableSecurityMappingEntity> =
        entities.map {
            TableSecurityMappingEntity(
                null,
                it.rowBaseOutputSecurityColumn,
                it.tableName,
            )
        }

    private fun correctAppWithDefaultTimeoutForRegistrationExp(
        actual: AppEntity,
        expected: AppRegistration,
    ): Boolean =
        actual.appId == expected.id &&
            actual.safeDepositDDL == expected.safeDepositDDL &&
            actual.transactionDDL == expected.transactionDDL &&
            actual.tableSecurityMappings ==
            mapTSMDataClassesToEntities(expected.tableSecurityMapping) &&
            actual.transactionTimeoutInDays == 30

    private fun correctAppForRegistrationExp(
        actual: AppEntity,
        expected: AppRegistration,
    ): Boolean =
        actual.appId == expected.id &&
            actual.safeDepositDDL == expected.safeDepositDDL &&
            actual.transactionDDL == expected.transactionDDL &&
            actual.tableSecurityMappings ==
            mapTSMDataClassesToEntities(expected.tableSecurityMapping) &&
            actual.transactionTimeoutInDays == expected.transactionTimeoutInDays

    @Test
    fun `register app with default timeout`(asserter: UniAsserter) {
        val newApp =
            AppEntity(
                null,
                appId,
                appOwner,
                safeDepositDDL = sampleAppRegistration.safeDepositDDL,
                transactionDDL = sampleAppRegistration.transactionDDL,
                transactionTimeoutInDays = 30,
            )
        asserter.assertThat(
            {
                whenever(jwt.getClaim<String>("participant")).doReturn("app_provider")
                whenever(
                    appRepository.startAppRegistration(
                        argThat {
                            correctAppWithDefaultTimeoutForRegistrationExp(
                                this,
                                sampleAppRegistration,
                            )
                        },
                    ),
                ).doReturn(uni { newApp })
                appResource.startAppRegistration(sampleAppRegistration)
            },
            {
                assertEquals(201, it.status)
                assertEquals(
                    AppResponse(
                        newApp.appId,
                        newApp.appOwner,
                        newApp.transactionDDL,
                        newApp.safeDepositDDL,
                        transactionTimeoutInDays = newApp.transactionTimeoutInDays,
                    ),
                    it.entity,
                )
            },
        )
    }

    @Test
    fun `register app with explicit timeout`(asserter: UniAsserter) {
        val newApp =
            AppEntity(
                null,
                appId,
                appOwner,
                safeDepositDDL = sampleAppRegistrationWithExplicitTimeout.safeDepositDDL,
                transactionDDL = sampleAppRegistrationWithExplicitTimeout.transactionDDL,
                transactionTimeoutInDays = sampleAppRegistrationWithExplicitTimeout.transactionTimeoutInDays!!,
            )
        asserter.assertThat(
            {
                whenever(jwt.getClaim<String>("participant")).doReturn("app_provider")
                whenever(
                    appRepository.startAppRegistration(
                        argThat {
                            correctAppForRegistrationExp(
                                this,
                                sampleAppRegistrationWithExplicitTimeout,
                            )
                        },
                    ),
                ).doReturn(uni { newApp })
                appResource.startAppRegistration(sampleAppRegistrationWithExplicitTimeout)
            },
            {
                assertEquals(201, it.status)
                assertEquals(
                    AppResponse(
                        newApp.appId,
                        newApp.appOwner,
                        newApp.transactionDDL,
                        newApp.safeDepositDDL,
                        transactionTimeoutInDays = newApp.transactionTimeoutInDays,
                    ),
                    it.entity,
                )
            },
        )
    }

    @Test
    fun `registration with duplicate appId returns 409`(asserter: UniAsserter) {
        asserter.assertFailedWith({
            whenever(appRepository.startAppRegistration(argThat { correctAppWithDefaultTimeoutForRegistrationExp(this, sampleAppRegistration) }))
                .doReturn(Uni.createFrom().failure(ClientErrorException("", 409)))
            appResource.startAppRegistration(sampleAppRegistration)
        }, { it is ClientErrorException && it.response.status == 409 })
    }

    @Test
    fun `find existing app`(asserter: UniAsserter) {
        asserter.assertThat({
            whenever(appRepository.findApp(appId)).doReturn(uni { sampleApp })
            appResource.findApp(appId)
        }, {
            assertEquals(appId, it.id)
            assertEquals(transactionDDL, it.transactionDDL)
            assertEquals(safeDepositDDL, it.safeDepositDDL)
            assertEquals(tableSecurityMappings, it.tableSecurityMapping)
            assertEquals(sampleWorkflows, it.workflows)
        })
    }

    @Test
    fun `find non existent app`(asserter: UniAsserter) {
        asserter.assertFailedWith({
            whenever(appRepository.findApp(appId)).doReturn(uni { null })
            appResource.findApp(appId)
        }, NotFoundException::class.java)
    }

    @Test
    fun `get all app IDs`(asserter: UniAsserter) {
        val appIds = listOf("app60385", "app64289")
        asserter.assertEquals({
            whenever(appRepository.getAllAppIds()).doReturn(uni { appIds })
            appResource.getAllAppIds()
        }, appIds)
    }

    @Test
    fun `delete non existent app`(asserter: UniAsserter) {
        asserter.assertFailedWith({
            whenever(jwt.getClaim<String>("participant")).doReturn("app_provider")
            whenever(appRepository.findApp(appId)).doReturn(uni { null })
            appResource.deleteApp(appId)
        }, { it is NotFoundException && it.message == "App with id $appId not found" })
    }

    @Test
    fun `delete proprietary app`(asserter: UniAsserter) {
        asserter.assertThat({
            whenever(jwt.getClaim<String>("participant")).doReturn("app_provider")
            whenever(appRepository.findApp(appId)).doReturn(uni { sampleApp })
            whenever(appRepository.deleteApp(appId)).doReturn(uni { true })
            appResource.deleteApp(appId)
        }, { verify(workflowHelper).deleteWorkflow(appId, sampleWorkflow.workflowId) })
    }

    @Test
    fun `delete foreign app`(asserter: UniAsserter) {
        asserter.assertFailedWith({
            whenever(jwt.getClaim<String>("participant")).doReturn("not_app_provider")
            whenever(appRepository.findApp(appId)).doReturn(uni { sampleApp })
            appResource.deleteApp(appId)
        }, ClientErrorException::class.java)
    }

    @Test
    fun `delete app with participant null type`(asserter: UniAsserter) {
        asserter.assertFailedWith({
            whenever(jwt.getClaim<String>("participant")).doReturn(null)
            whenever(appRepository.findApp(appId)).doReturn(uni { sampleApp })
            appResource.deleteApp(appId)
        }, ForbiddenException::class.java)
    }

    @Test
    fun `delete app with non deleted image fails`(asserter: UniAsserter) {
        val sampleImage: ImageEntity = mock { on { status } doReturn ImageStatus.ACTIVE }
        val sampleAppWithActiveImage: AppEntity =
            mock {
                on { appId } doReturn appId
                on { appOwner } doReturn appOwner
                on { workflows } doReturn mutableListOf(sampleWorkflow)
                on { images } doReturn mutableListOf(sampleImage)
            }
        asserter.assertFailedWith({
            whenever(appRepository.findApp(appId)).doReturn(uni { sampleAppWithActiveImage })
            whenever(jwt.getClaim<String>("participant")).doReturn("app_provider")
            appResource.deleteApp(appId)
        }, BadRequestException::class.java)
    }
}
