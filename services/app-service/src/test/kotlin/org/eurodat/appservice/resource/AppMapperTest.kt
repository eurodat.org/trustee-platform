package org.eurodat.appservice.resource

import org.eurodat.appservice.model.AppEntity
import org.eurodat.appservice.model.ImageEntity
import org.eurodat.appservice.model.TableSecurityMappingEntity
import org.eurodat.appservice.model.WorkflowEntity
import org.eurodat.client.appservice.model.TableSecurityMapping
import org.eurodat.client.appservice.model.WorkflowDefinition
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

class AppMapperTest {
    @Test
    fun mapsEntityToResponse() {
        val mockImage: ImageEntity = mock { on { imageId } doReturn "some-image-id" }
        val entity =
            AppEntity(
                appId = "appId",
                appOwner = "appOwner",
                workflows =
                    mutableListOf(
                        WorkflowEntity(
                            workflowId = "aWorkflow",
                            resourceName = "someResource",
                            entryPoint = "someEntryPoint",
                            startCommand = listOf("some", "command"),
                            image = mockImage,
                        ),
                        WorkflowEntity(
                            workflowId = "anotherWorkflow",
                            resourceName = "otherResource",
                            entryPoint = "secondEntryPoint",
                            startCommand = listOf("another", "command"),
                            image = mockImage,
                        ),
                    ),
                transactionDDL = "CREATE TABLE ...",
                safeDepositDDL = "CREATE TABLE SAFE...",
                tableSecurityMappings =
                    listOf(
                        TableSecurityMappingEntity(
                            rowBaseOutputSecurityColumn = "columnA",
                            tableName = "appTable",
                        ),
                        TableSecurityMappingEntity(
                            rowBaseOutputSecurityColumn = "columnX",
                            tableName = "otherTable",
                        ),
                    ),
                transactionTimeoutInDays = 10,
            )

        val response = createResponseFromEntity(entity)

        Assertions.assertEquals("appId", response.id)
        Assertions.assertEquals("CREATE TABLE ...", response.transactionDDL)
        Assertions.assertEquals("CREATE TABLE SAFE...", response.safeDepositDDL)
        Assertions.assertIterableEquals(
            listOf(
                TableSecurityMapping("columnA", "appTable"),
                TableSecurityMapping("columnX", "otherTable"),
            ),
            response.tableSecurityMapping,
        )
        Assertions.assertEquals(
            listOf(
                WorkflowDefinition("aWorkflow", "someResource", "someEntryPoint", listOf("some", "command"), "some-image-id"),
                WorkflowDefinition("anotherWorkflow", "otherResource", "secondEntryPoint", listOf("another", "command"), "some-image-id"),
            ),
            response.workflows,
        )
    }
}
