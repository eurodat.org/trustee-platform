package org.eurodat.appservice.resource

import io.argoproj.workflow.apis.ApiException
import io.quarkus.test.InjectMock
import io.quarkus.test.TestReactiveTransaction
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.security.TestSecurity
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.inject.Inject
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.ClientErrorException
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.ServerErrorException
import jakarta.ws.rs.core.Response
import org.eurodat.appservice.model.AppEntity
import org.eurodat.appservice.model.ImageEntity
import org.eurodat.appservice.model.ImageStatus
import org.eurodat.appservice.model.WorkflowEntity
import org.eurodat.appservice.repository.AppRepository
import org.eurodat.appservice.repository.WorkflowRepository
import org.eurodat.appservice.service.WorkflowHelper
import org.eurodat.client.appservice.model.WorkflowDefinition
import org.eurodat.client.appservice.model.WorkflowRegistrationRequest
import org.hibernate.exception.ConstraintViolationException
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import java.sql.SQLException

@QuarkusTest
@RunOnVertxContext
@TestReactiveTransaction
@TestSecurity(user = "testUser")
class WorkflowRegistrationResourceTest {
    @InjectMock
    private lateinit var appRepository: AppRepository

    @InjectMock
    private lateinit var workflowRepository: WorkflowRepository

    @InjectMock
    private lateinit var workflowHelper: WorkflowHelper

    @Inject
    private lateinit var workflowRegistrationResource: WorkflowRegistrationResource

    private val appId = "someAppId"
    private val workflowId = "some-workflow-id"
    private val workflowResourceName = "some-resource-name"
    private val workflowStartCommand = listOf("start", "commands")
    private val workflowEntryPoint = "enter-here"
    private val imageId = "some-image-id"
    private val workflowDatabaseId = 64

    private val image: ImageEntity =
        mock {
            on { imageId } doReturn imageId
            on { status } doReturn ImageStatus.ACTIVE
            on { location } doReturn "some-location"
        }
    private val workflow: WorkflowEntity =
        mock {
            on { workflowId } doReturn workflowId
            on { workflowDatabaseId } doReturn workflowDatabaseId
            on { resourceName } doReturn workflowResourceName
            on { startCommand } doReturn workflowStartCommand
            on { entryPoint } doReturn workflowEntryPoint
            on { image } doReturn image
        }
    private val appMock: AppEntity =
        mock {
            on { workflows } doReturn mutableListOf(workflow)
            on { images } doReturn mutableListOf(image)
        }

    @Test
    fun testDeleteNonExistingAppOrWorkflow(uniAsserter: UniAsserter) {
        uniAsserter.assertFailedWith({
            whenever(workflowHelper.deleteWorkflow(appId, workflowId)).doReturn(Uni.createFrom().failure(NotFoundException()))
            workflowRegistrationResource.deleteWorkflow(appId, workflowId)
        }, NotFoundException::class.java)
    }

    @Test
    fun testDeleteWorkflow(uniAsserter: UniAsserter) {
        uniAsserter.assertThat({
            whenever(workflowRepository.deleteWorkflowById(workflowDatabaseId)).doReturn(uni { true })
            workflowRegistrationResource.deleteWorkflow(appId, workflowId)
        }, {
            verify(workflowHelper).deleteWorkflow(appId, workflowId)
        })
    }

    @Test
    fun testAddWorkflowNonExistentAppId(uniAsserter: UniAsserter) {
        uniAsserter.assertFailedWith({
            whenever(appRepository.findApp(appId)).doReturn(Uni.createFrom().nullItem())
            workflowRegistrationResource.addWorkflow(appId, WorkflowRegistrationRequest(workflowId, "bogusImage", listOf("bogus", "command")))
        }, NotFoundException::class.java)
    }

    @Test
    fun testAddWorkflowNonCompliantWorkflowId(uniAsserter: UniAsserter) {
        uniAsserter.assertFailedWith({
            whenever(appRepository.findApp(appId)).doReturn(uni { appMock })
            workflowRegistrationResource.addWorkflow(
                appId,
                WorkflowRegistrationRequest("NeverHeardOfRFC1123", "bogusImage", listOf("bogus", "command")),
            )
        }, BadRequestException::class.java)
    }

    @Test
    fun testAddWorkflowAlreadyExistingWorkflowName(uniAsserter: UniAsserter) {
        uniAsserter.assertFailedWith(
            {
                whenever(appRepository.findApp(appId)).doReturn(uni { appMock })
                whenever(workflowRepository.addWorkflow(any())).doReturn(
                    Uni.createFrom().failure(ConstraintViolationException("", SQLException(""), "unique_app_workflow_id")),
                )
                workflowRegistrationResource.addWorkflow(
                    appId,
                    WorkflowRegistrationRequest(workflowId, imageId, listOf("bogus", "command")),
                )
            },
            { it is ClientErrorException && it.response.status == 409 },
        )
    }

    @Test
    fun testAddWorkflowAlreadyExistingResourceName(uniAsserter: UniAsserter) {
        uniAsserter.assertFailedWith(
            {
                whenever(appRepository.findApp(appId)).doReturn(uni { appMock })
                whenever(workflowHelper.addWorkflowTemplateToArgo(any())).doReturn(
                    Uni.createFrom().failure(ApiException(Response.status(409).build())),
                )
                workflowRegistrationResource.addWorkflow(
                    appId,
                    WorkflowRegistrationRequest(workflowId, imageId, listOf("bogus", "command")),
                )
            },
            { it is ServerErrorException && it.response.status == 500 },
        )
    }

    @Test
    fun testAddWorkflowImageNotFound(uniAsserter: UniAsserter) {
        uniAsserter.assertFailedWith(
            {
                whenever(appRepository.findApp(appId)).doReturn(uni { appMock })
                whenever(workflowRepository.addWorkflow(any())).doReturn(uni { workflow })
                workflowRegistrationResource.addWorkflow(
                    appId,
                    WorkflowRegistrationRequest(workflowId, "imageIdYok", listOf("bogus", "command")),
                )
            },
            BadRequestException::class.java,
        )
    }

    @Test
    fun testAddWorkflow(uniAsserter: UniAsserter) {
        uniAsserter.assertThat(
            {
                whenever(appRepository.findApp(appId)).doReturn(uni { appMock })
                whenever(workflowRepository.addWorkflow(any())).doReturn(uni { workflow })
                workflowRegistrationResource.addWorkflow(
                    appId,
                    WorkflowRegistrationRequest(workflowId, imageId, listOf("bogus", "command")),
                )
            },
            {
                verify(workflowHelper).addWorkflowTemplateToArgo(any())
                verify(workflowRepository).addWorkflow(any())
            },
        )
    }

    @Test
    fun testGetAllWorkflowsForApp(uniAsserter: UniAsserter) {
        uniAsserter.assertEquals(
            {
                whenever(appRepository.findApp(appId)).doReturn(uni { appMock })
                workflowRegistrationResource.getWorkflows(appId)
            },
            listOf(workflowId),
        )
    }

    @Test
    fun testGetAllWorkflowsForAppIfEmpty(uniAsserter: UniAsserter) {
        val emptyAppMock: AppEntity =
            mock {
                on { workflows } doReturn mutableListOf()
            }
        uniAsserter.assertEquals(
            {
                whenever(appRepository.findApp(appId)).doReturn(uni { emptyAppMock })
                workflowRegistrationResource.getWorkflows(appId)
            },
            emptyList(),
        )
    }

    @Test
    fun testGetAllWorkflowsForNonExistentApp(uniAsserter: UniAsserter) {
        uniAsserter.assertFailedWith(
            {
                whenever(appRepository.findApp(appId)).doReturn(Uni.createFrom().nullItem())
                workflowRegistrationResource.getWorkflows(appId)
            },
            NotFoundException::class.java,
        )
    }

    @Test
    fun testGetWorkflowForNonExistentApp(uniAsserter: UniAsserter) {
        uniAsserter.assertFailedWith(
            {
                whenever(appRepository.findApp(appId)).doReturn(Uni.createFrom().nullItem())
                workflowRegistrationResource.getWorkflow(appId, workflowId)
            },
            NotFoundException::class.java,
        )
    }

    @Test
    fun testGetWorkflowForNonExistentWorkflowId(uniAsserter: UniAsserter) {
        uniAsserter.assertFailedWith(
            {
                whenever(appRepository.findApp(appId)).doReturn(uni { appMock })
                workflowRegistrationResource.getWorkflow(appId, "not-existent")
            },
            NotFoundException::class.java,
        )
    }

    @Test
    fun testGetWorkflow(uniAsserter: UniAsserter) {
        uniAsserter.assertEquals(
            {
                whenever(appRepository.findApp(appId)).doReturn(uni { appMock })
                workflowRegistrationResource.getWorkflow(appId, workflowId)
            },
            WorkflowDefinition(
                workflowId,
                workflowResourceName,
                workflowEntryPoint,
                workflowStartCommand,
                imageId,
            ),
        )
    }
}
