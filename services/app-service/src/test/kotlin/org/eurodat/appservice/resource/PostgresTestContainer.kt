package org.eurodat.appservice.resource

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager
import org.testcontainers.containers.PostgreSQLContainer

class PostgresTestContainer : QuarkusTestResourceLifecycleManager {
    private lateinit var container: PostgreSQLContainer<*>

    override fun start(): MutableMap<String, String> {
        container =
            PostgreSQLContainer("postgres:16")
                .withDatabaseName("app_service_test_db")
                .withUsername("superuser")
                .withPassword("pwd")
        container.start()

        val appServiceUrl = container.getJdbcUrl().replace("jdbc", "vertx-reactive")

        return mutableMapOf(
            "quarkus.datasource.reactive.url" to appServiceUrl,
            "quarkus.datasource.username" to container.username,
            "quarkus.datasource.password" to container.password,
        )
    }

    override fun stop() {
        container.close()
    }
}
