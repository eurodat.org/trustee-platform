package org.eurodat.appservice.resource

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.security.TestSecurity
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.uni
import jakarta.inject.Inject
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.NotFoundException
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eurodat.appservice.model.AppEntity
import org.eurodat.appservice.model.ImageEntity
import org.eurodat.appservice.model.ImageStatus
import org.eurodat.appservice.repository.AppRepository
import org.eurodat.appservice.service.KubernetesService
import org.eurodat.client.appservice.model.Image
import org.eurodat.client.appservice.model.ImageRequest
import org.hibernate.reactive.mutiny.Mutiny.SessionFactory
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.kotlin.argThat
import org.mockito.kotlin.eq
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@QuarkusTest
@RunOnVertxContext
@TestSecurity(user = "testUser")
class ImageResourceTest {
    @ConfigProperty(name = "uploader.namespace")
    private lateinit var namespace: String

    @InjectMock
    private lateinit var appRepository: AppRepository

    @InjectMock
    private lateinit var kubeService: KubernetesService

    @Inject
    private lateinit var imageResource: ImageResource

    @Inject
    private lateinit var sessionFactory: SessionFactory

    private val appId = "appId"
    private val appOwner = "appOwner"
    private val appDatabaseId = 1
    private val appEntity = AppEntity(appDatabaseId = appDatabaseId, appId = appId, appOwner = appOwner, transactionTimeoutInDays = 30)

    @Test
    fun createImageReturnsCreatedImage(asserter: UniAsserter) {
        val location = "ucApp:latest"
        whenever(appRepository.findApp(appId)).thenReturn(uni { appEntity })

        asserter.assertThat(
            {
                sessionFactory.withSession {
                    imageResource.createImage(appId, ImageRequest(location = location))
                }
            },
            { response ->
                Assertions.assertEquals(201, response.status)
                val entity = response.entity as Image
                Assertions.assertNotNull(entity.id)
                Assertions.assertEquals(location, entity.sourceLocation)
                Assertions.assertEquals(org.eurodat.client.appservice.model.ImageStatus.UPLOADING, entity.status)
            },
        )
    }

    @Test
    fun createImageWithCredentialsCreatesSecretAndReturnsCreatedImage(asserter: UniAsserter) {
        val location = "ucApp:latest"
        val username = "AbdiSued"
        val password = "Celo385"
        asserter.assertThat(
            {
                whenever(appRepository.findApp(appId)).thenReturn(uni { appEntity })
                sessionFactory.withSession {
                    imageResource.createImage(appId, ImageRequest(location, username, password))
                }
            },
            { response ->
                Assertions.assertEquals(201, response.status)
                val entity = response.entity as Image
                Assertions.assertNotNull(entity.id)
                Assertions.assertEquals(location, entity.sourceLocation)
                Assertions.assertEquals(org.eurodat.client.appservice.model.ImageStatus.UPLOADING, entity.status)
                verify(kubeService).createSecret(
                    eq(entity.id),
                    argThat { get("username") == username && get("password") == password },
                    eq(namespace),
                )
            },
        )
    }

    @Test
    fun createImageWithOnlyUsernameFails(asserter: UniAsserter) {
        asserter.assertFailedWith(
            {
                whenever(appRepository.findApp(appId)).thenReturn(uni { appEntity })
                sessionFactory.withSession {
                    imageResource.createImage(appId, ImageRequest("ucApp:latest", "AbdiSued"))
                }
            },
            BadRequestException::class.java,
        )
    }

    @Test
    fun createImageWithOnlyPasswordFails(asserter: UniAsserter) {
        asserter.assertFailedWith(
            {
                whenever(appRepository.findApp(appId)).thenReturn(uni { appEntity })
                sessionFactory.withSession {
                    imageResource.createImage(appId, ImageRequest("ucApp:latest", registryPassword = "Celo385"))
                }
            },
            BadRequestException::class.java,
        )
    }

    @Test
    fun createImageReturnsNotFoundForUnknownApp(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { imageResource.createImage(appId, ImageRequest(location = "ucApp:latest")) },
            { exception ->
                Assertions.assertInstanceOf(NotFoundException::class.java, exception)
                Assertions.assertEquals("App with id appId not found", exception.message)
            },
        )
    }

    @Test
    fun getImagesReturnsImages(asserter: UniAsserter) {
        val imageList =
            mutableListOf(
                ImageEntity(
                    imageId = "image1",
                    sourceLocation = "uc1App",
                    status = ImageStatus.UPLOADING,
                    app = appEntity,
                    credentialsSupplied = false,
                ),
            )
        whenever(appRepository.findApp(appId)).thenReturn(
            uni {
                AppEntity(
                    images = imageList,
                    appOwner = appOwner,
                    transactionTimeoutInDays = 30,
                )
            },
        )

        asserter.assertThat(
            { imageResource.getImages(appId) },
            { images ->
                Assertions.assertIterableEquals(
                    imageList.map {
                        Image(
                            it.imageId,
                            it.sourceLocation,
                            it.location,
                            org.eurodat.client.appservice.model.ImageStatus.UPLOADING,
                            it.createdAt,
                        )
                    },
                    images,
                )
            },
        )
    }

    @Test
    fun getImagesThrowsNotFoundForUnknownApp(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { imageResource.getImages(appId) },
            { exception ->
                Assertions.assertInstanceOf(NotFoundException::class.java, exception)
                Assertions.assertEquals("App with id appId not found", exception.message)
            },
        )
    }

    @Test
    fun getImageReturnsImage(asserter: UniAsserter) {
        val image1 =
            ImageEntity(
                imageId = "image1",
                sourceLocation = "uc1App",
                status = ImageStatus.ACTIVE,
                app = appEntity,
                credentialsSupplied = false,
            )
        whenever(appRepository.findApp(appId)).thenReturn(
            uni {
                AppEntity(
                    images = mutableListOf(image1),
                    appOwner = appOwner,
                    transactionTimeoutInDays = 30,
                )
            },
        )

        asserter.assertEquals(
            { imageResource.getImage(appId, "image1") },
            Image(
                image1.imageId,
                image1.sourceLocation,
                image1.location,
                org.eurodat.client.appservice.model.ImageStatus.ACTIVE,
                image1.createdAt,
            ),
        )
    }

    @Test
    fun getImageThrowsNotFoundForUnknownApp(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { imageResource.getImage(appId, "image1") },
            { exception ->
                Assertions.assertInstanceOf(NotFoundException::class.java, exception)
                Assertions.assertEquals("App with id appId not found", exception.message)
            },
        )
    }

    @Test
    fun getImageThrowsNotFoundForUnknownImage(asserter: UniAsserter) {
        whenever(appRepository.findApp(appId)).thenReturn(uni { AppEntity(appId = appId, appOwner = appOwner, transactionTimeoutInDays = 30) })

        asserter.assertFailedWith(
            { imageResource.getImage(appId, "image1") },
            { exception ->
                Assertions.assertInstanceOf(NotFoundException::class.java, exception)
                Assertions.assertEquals("Image with id image1 not found", exception.message)
            },
        )
    }

    @Test
    fun getImageForDeletedImage(asserter: UniAsserter) {
        val image1 =
            ImageEntity(
                imageId = "image1",
                sourceLocation = "uc1App",
                status = ImageStatus.DELETED,
                app = appEntity,
                credentialsSupplied = false,
            )
        whenever(appRepository.findApp(appId)).thenReturn(
            uni {
                AppEntity(
                    appId = appId,
                    images = mutableListOf(image1),
                    appOwner = appOwner,
                    transactionTimeoutInDays = 30,
                )
            },
        )

        asserter.assertEquals(
            { imageResource.getImage(appId, "image1") },
            Image(
                image1.imageId,
                image1.sourceLocation,
                image1.location,
                org.eurodat.client.appservice.model.ImageStatus.DELETED,
                image1.createdAt,
            ),
        )
    }

    @Test
    fun deleteImageReturnsDeletingImage(asserter: UniAsserter) {
        val image1 =
            ImageEntity(
                imageId = "image1",
                sourceLocation = "uc1App",
                status = ImageStatus.ACTIVE,
                app = appEntity,
                credentialsSupplied = false,
            )
        whenever(appRepository.findApp(appId)).thenReturn(
            uni {
                AppEntity(
                    images = mutableListOf(image1),
                    appOwner = appOwner,
                    transactionTimeoutInDays = 30,
                )
            },
        )

        asserter.assertEquals(
            { imageResource.deleteImage(appId, "image1") },
            Image(
                image1.imageId,
                image1.sourceLocation,
                image1.location,
                org.eurodat.client.appservice.model.ImageStatus.DELETING,
                image1.createdAt,
            ),
        )
    }

    @Test
    fun deleteImageThrowsNotFoundForUnknownApp(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { imageResource.deleteImage(appId, "image1") },
            { exception ->
                Assertions.assertInstanceOf(NotFoundException::class.java, exception)
                Assertions.assertEquals("App with id appId not found", exception.message)
            },
        )
    }

    @Test
    fun deleteImageThrowsNotFoundForUnknownImage(asserter: UniAsserter) {
        whenever(appRepository.findApp(appId)).thenReturn(uni { AppEntity(appId = appId, appOwner = appOwner, transactionTimeoutInDays = 30) })

        asserter.assertFailedWith(
            { imageResource.deleteImage(appId, "image1") },
            { exception ->
                Assertions.assertInstanceOf(NotFoundException::class.java, exception)
                Assertions.assertEquals("Image with id image1 not found", exception.message)
            },
        )
    }

    @Test
    fun deleteImageNotAllowedForUploadingImage(asserter: UniAsserter) {
        deleteImageNotAllowedForStatus(asserter, ImageStatus.UPLOADING)
    }

    @Test
    fun deleteImageNotAllowedForDeletingImage(asserter: UniAsserter) {
        deleteImageNotAllowedForStatus(asserter, ImageStatus.DELETING)
    }

    private fun deleteImageNotAllowedForStatus(
        asserter: UniAsserter,
        status: ImageStatus,
    ) {
        val image1 =
            ImageEntity(
                imageId = "image1",
                sourceLocation = "uc1App",
                status = status,
                app = appEntity,
                credentialsSupplied = false,
            )
        whenever(appRepository.findApp(appId)).thenReturn(
            uni {
                AppEntity(
                    images = mutableListOf(image1),
                    appOwner = appOwner,
                    transactionTimeoutInDays = 30,
                )
            },
        )

        asserter.assertFailedWith(
            { imageResource.deleteImage(appId, "image1") },
            { exception ->
                Assertions.assertInstanceOf(BadRequestException::class.java, exception)
                Assertions.assertEquals("Image cannot be deleted in status $status", exception.message)
            },
        )
    }
}
