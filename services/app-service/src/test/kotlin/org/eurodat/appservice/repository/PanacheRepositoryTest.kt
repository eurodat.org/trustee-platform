package org.eurodat.appservice.repository

import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import org.eurodat.appservice.model.AppEntity
import org.eurodat.appservice.model.TableSecurityMappingEntity
import org.eurodat.appservice.resource.PostgresTestContainer
import org.hibernate.reactive.mutiny.Mutiny.SessionFactory
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.ZonedDateTime

@QuarkusTest
@QuarkusTestResource(PostgresTestContainer::class)
class PanacheRepositoryTest {
    @Inject
    lateinit var sessionFactory: SessionFactory

    @Inject
    lateinit var appRepository: PanacheAppRepository

    private val testApp =
        AppEntity(
            null,
            "testAppOwner",
            "testAppId",
            ZonedDateTime.now(),
            ZonedDateTime.now().plusDays(20),
            mutableListOf(),
            "testTransactionDDL",
            "testSafeDepositDDL",
            listOf(
                TableSecurityMappingEntity(null, "testRowBaseOutputSecurityColumn_1", "testTableName_1"),
                TableSecurityMappingEntity(null, "testRowBaseOutputSecurityColumn_2", "testTableName_2"),
            ),
            transactionTimeoutInDays = 30,
        )

    @Test
    @RunOnVertxContext
    fun testIntegration(asserter: UniAsserter) {
        asserter.assertThat(
            {
                sessionFactory.withTransaction { _ -> appRepository.startAppRegistration(testApp) }
            },
            { result ->
                Assertions.assertNotNull(result)
            },
        )
        asserter.assertThat(
            {
                sessionFactory.withSession { _ -> appRepository.findApp("testAppId") }
            },
            { app ->
                Assertions.assertEquals(testApp, app)
            },
        )
        asserter.assertThat(
            {
                sessionFactory.withSession { _ -> appRepository.findApp("testAppNotExist") }
            },
            { app ->
                Assertions.assertEquals(null, app)
            },
        )
        asserter.assertThat(
            {
                sessionFactory.withSession { appRepository.getAllAppIds() }
            },
            { mappings ->
                Assertions.assertEquals(1, mappings.size)
            },
        )
        asserter.assertThat(
            {
                sessionFactory.withSession { appRepository.deleteApp("testAppId") }
            },
            { result ->
                Assertions.assertTrue(result)
            },
        )

        asserter.assertThat(
            {
                sessionFactory.withSession { appRepository.getAllAppIds() }
            },
            { mappings ->
                Assertions.assertEquals(0, mappings.size)
            },
        )
    }
}
