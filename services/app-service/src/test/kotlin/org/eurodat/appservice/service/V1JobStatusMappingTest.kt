package org.eurodat.appservice.service

import io.kubernetes.client.openapi.models.V1JobStatus
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class V1JobStatusMappingTest {
    @Test
    fun `job with succeeded pods is completed`() {
        assertJobStatus(ImageJobStatus.COMPLETED) { succeeded = 1 }
    }

    @Test
    fun `job with failed pods is failed`() {
        assertJobStatus(ImageJobStatus.FAILED) { failed = 1 }
    }

    @Test
    fun `job with active pods is running`() {
        assertJobStatus(ImageJobStatus.RUNNING) { active = 1 }
    }

    @Test
    fun `job with no ready pods is running`() {
        assertJobStatus(ImageJobStatus.RUNNING) { ready = 0 }
    }
}

private fun assertJobStatus(
    expected: ImageJobStatus,
    status: V1JobStatus.() -> Unit,
) = assertEquals(expected, V1JobStatus().apply(status).toImageJobStatus())
