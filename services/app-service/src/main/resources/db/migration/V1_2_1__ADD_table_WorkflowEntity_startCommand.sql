create table WorkflowEntity_startCommand (WorkflowEntity_workflow_database_id integer not null, start_command varchar(255));
alter table if exists WorkflowEntity_startCommand add constraint FKlhsgb1dk7f8q9mnv3j0cn35d8 foreign key (WorkflowEntity_workflow_database_id) references workflows;

alter table workflows add column image_image_database_id integer;
alter table if exists workflows add constraint FKswj7tx0xyydaa0obevjlqoitt foreign key (image_image_database_id) references images;

update workflows set entry_point = '' where entry_point is null;
alter table workflows alter column image_image_database_id set not null;
