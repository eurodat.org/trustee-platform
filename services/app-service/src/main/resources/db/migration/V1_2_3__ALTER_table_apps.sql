ALTER TABLE apps
    ADD COLUMN transaction_timeout_in_days smallint NOT NULL DEFAULT 30;
