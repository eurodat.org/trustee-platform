INSERT INTO apps(app_database_id, app_id, valid_from, valid_to, transaction_ddl, safe_deposit_ddl)
VALUES (10, 'minaboApp', '2024-03-01 00:00:00', '9999-12-30 00:00:00', '
CREATE TABLE IF NOT EXISTS input.json (uuid_json uuid NOT NULL );
CREATE TABLE IF NOT EXISTS input.pdf ( uuid_pdf uuid NOT NULL );

CREATE TABLE IF NOT EXISTS output.json (
uuid_json uuid NOT NULL,
blob_json JSONB NULL,
PRIMARY KEY (uuid_json));

CREATE TABLE IF NOT EXISTS output.pdf (
uuid_pdf uuid NOT NULL,
blob_pdf BYTEA NULL,
PRIMARY KEY (uuid_pdf));
', '

CREATE TABLE IF NOT EXISTS safedeposit.json (
uuid_json uuid NOT NULL,
blob_json JSONB NULL,
PRIMARY KEY (uuid_json));

CREATE TABLE IF NOT EXISTS safedeposit.pdf (
uuid_pdf uuid NOT NULL,
blob_pdf BYTEA NULL,
PRIMARY KEY (uuid_pdf));

');

INSERT INTO workflows(workflow_database_id, workflow_id, resource_name, entry_point, app_database_id)
VALUES (11, 'start-minabo', 'minabo-workflow', 'start-workflow', 10);