alter table images
    add column created_at timestamp(6) with time zone;
alter table images
    add column delete_job varchar(255);
alter table images
    add column upload_job varchar(255);
