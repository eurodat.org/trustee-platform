alter table images
    add column credentials_supplied BOOL;

UPDATE images SET credentials_supplied=FALSE WHERE credentials_supplied IS NULL;
