CREATE TABLE images
(
    image_database_id INTEGER NOT NULL UNIQUE,
    image_id          VARCHAR NOT NULL UNIQUE,
    location          VARCHAR NOT NULL,
    status            VARCHAR,
    app_database_id   INTEGER NOT NULL,
    PRIMARY KEY (image_database_id),
    FOREIGN KEY (app_database_id) REFERENCES apps (app_database_id)
);

create sequence images_SEQ increment by 50;
