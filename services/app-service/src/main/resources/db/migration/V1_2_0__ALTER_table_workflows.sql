ALTER TABLE workflows

ADD CONSTRAINT unique_app_workflow_id UNIQUE (app_database_id, workflow_id);
