ALTER TABLE images ADD COLUMN source_location VARCHAR;
UPDATE images SET source_location = location;
ALTER TABLE images ALTER COLUMN location DROP NOT NULL;
UPDATE images SET location = NULL;
