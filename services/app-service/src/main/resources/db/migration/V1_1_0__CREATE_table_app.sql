CREATE TABLE apps (
    app_database_id     INTEGER NOT NULL UNIQUE,
    app_id              VARCHAR NOT NULL,
    valid_from          TIMESTAMP NOT NULL,
    valid_to            TIMESTAMP,
    transaction_ddl     TEXT NOT NULL,
    safe_deposit_ddl    TEXT NOT NULL,
    PRIMARY KEY (app_database_id),
    CONSTRAINT uq_app_id_valid_from UNIQUE (app_id, valid_from)
);

CREATE TABLE workflows (
    workflow_database_id    INTEGER NOT NULL UNIQUE,
    workflow_id             VARCHAR NOT NULL,
    resource_name           VARCHAR NOT NULL,
    entry_point             VARCHAR,
    app_database_id         INTEGER NOT NULL,
    PRIMARY KEY (workflow_database_id),
    FOREIGN KEY (app_database_id) REFERENCES apps (app_database_id)
);

CREATE TABLE table_security_mapping (
    security_mapping_database_id        INTEGER NOT NULL UNIQUE,
    row_base_output_security_column     VARCHAR NOT NULL,
    table_name                          VARCHAR NOT NULL,
    app_database_id                     INTEGER NOT NULL,
    PRIMARY KEY (app_database_id, table_name),
    FOREIGN KEY (app_database_id) REFERENCES apps(app_database_id),
    CONSTRAINT uq_app_database_id_table_name UNIQUE (app_database_id, table_name)
);
