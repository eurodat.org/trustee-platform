package org.eurodat.appservice.service

import io.github.oshai.kotlinlogging.KotlinLogging
import io.quarkus.arc.DefaultBean
import io.quarkus.runtime.Startup
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.enterprise.context.ApplicationScoped
import org.eurodat.appservice.model.ImageEntity
import java.util.UUID

private val logger = KotlinLogging.logger {}

/**
 * Does not start an image job. Moves the job status a step further on every invocation of [findImageJobStatus].
 */
@DefaultBean
@ApplicationScoped
class NoopSteppingImageJobScheduler : ImageJobScheduler {
    @Startup
    fun init() {
        logger.info { "SimpleSteppingImageJobScheduler initialized" }
    }

    private val imageJobs: MutableMap<String, ImageJobStatus> = mutableMapOf()

    override fun startImageUpload(imageEntity: ImageEntity) = createJob()

    override fun findImageJobStatus(jobName: String): Uni<ImageJobStatus> {
        val imageUploadStatus = imageJobs[jobName] ?: return Uni.createFrom().failure(Exception("Image job not found"))
        if (imageUploadStatus == ImageJobStatus.RUNNING) {
            imageJobs[jobName] = ImageJobStatus.COMPLETED
        }
        return uni { imageUploadStatus }
    }

    override fun startImageDeletion(imageEntity: ImageEntity) = createJob()

    private fun createJob(): Uni<String> {
        val job = UUID.randomUUID().toString()
        imageJobs[job] = ImageJobStatus.RUNNING
        return uni { job }
    }
}
