package org.eurodat.appservice.repository

import io.smallrye.mutiny.Uni
import org.eurodat.appservice.model.ImageEntity

interface ImageRepository {
    fun findTransitioningImages(): Uni<List<ImageEntity>>
}
