package org.eurodat.appservice.service

import io.kubernetes.client.openapi.ApiException
import io.kubernetes.client.openapi.models.V1EnvVar
import io.kubernetes.client.openapi.models.V1Job
import io.kubernetes.client.openapi.models.V1JobStatus
import io.kubernetes.client.util.Yaml
import io.quarkus.logging.Log
import io.quarkus.runtime.Startup
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eurodat.appservice.model.ImageEntity
import java.io.FileNotFoundException
import java.io.Reader

@ApplicationScoped
class KubernetesImageJobScheduler(
    private val kubeService: KubernetesService,
    @ConfigProperty(name = "uploader.image") private val image: String,
    @ConfigProperty(name = "uploader.namespace") private val namespace: String,
    @ConfigProperty(name = "uploader.service-account") private val serviceAccount: String,
    @ConfigProperty(name = "uploader.src-https", defaultValue = "false") private val srcHttps: Boolean,
    @ConfigProperty(name = "uploader.dest-auth", defaultValue = "false") private val destAuth: Boolean,
    @ConfigProperty(name = "uploader.dest-https", defaultValue = "false") private val destHttps: Boolean,
    @ConfigProperty(name = "uploader.gcr", defaultValue = "false") private val gcr: Boolean,
    @ConfigProperty(name = "registry.address") private val destinationRegistry: String,
) : ImageJobScheduler {
    @Startup
    fun init() {
        Log.info("KubernetesImageJobScheduler initialized")
    }

    override fun startImageUpload(imageEntity: ImageEntity): Uni<String> {
        var userNameAndPassword = Pair("", "")
        if (imageEntity.credentialsSupplied) {
            try {
                val data = kubeService.readSecret(imageEntity.imageId, namespace)
                userNameAndPassword = Pair(data.getValue("username"), data.getValue("password"))
            } catch (e: ApiException) {
                return when (e.code) {
                    404 -> {
                        Log.error("Credentials for image ${imageEntity.imageId} should be supplied, but none were found")
                        Uni.createFrom().failure(TerminalJobCreationException(e))
                    }

                    else -> Uni.createFrom().failure(e)
                }
            }
        }

        val appliedYaml =
            Yaml.loadAs(
                uploadTemplate
                    .replace("{{JOB_NAME}}", "upload-${imageEntity.imageId}")
                    .replace("{{NAMESPACE}}", namespace)
                    .replace("{{SERVICE_ACCOUNT}}", serviceAccount)
                    .replace("{{IMAGE}}", image)
                    .replace("{{SRC_LOCATION}}", imageEntity.sourceLocation)
                    .replace("{{DEST_LOCATION}}", imageDestination(imageEntity))
                    .replace("{{IMAGE_ID}}", imageEntity.imageId)
                    .replace("{{NDA}}", if (destAuth) "" else "- \"-nda\"")
                    .replace("{{NSH}}", if (srcHttps) "" else "- \"-nsh\"")
                    .replace("{{NDH}}", if (destHttps) "" else "- \"-ndh\""),
                V1Job::class.java,
            )
        val envVariables =
            listOf(
                V1EnvVar().name("IMAGE_CRED_USERNAME").value(userNameAndPassword.first),
                V1EnvVar().name("IMAGE_CRED_PASSWORD").value(userNameAndPassword.second),
            )
        appliedYaml.spec.template.spec.containers[0]
            .env(envVariables)
        return Uni
            .createFrom()
            .item {
                kubeService.createJob(appliedYaml, namespace)
            }.map {
                it.metadata.name
            }
    }

    private fun imageDestination(imageEntity: ImageEntity) = "$destinationRegistry/${imageEntity.app.appId.lowercase()}/${imageEntity.imageId}"

    override fun startImageDeletion(imageEntity: ImageEntity): Uni<String> =
        Uni
            .createFrom()
            .item {
                kubeService.createJob(
                    Yaml.loadAs(
                        deleteTemplate
                            .replace("{{JOB_NAME}}", "delete-${imageEntity.imageId}")
                            .replace("{{NAMESPACE}}", namespace)
                            .replace("{{SERVICE_ACCOUNT}}", serviceAccount)
                            .replace("{{IMAGE}}", image)
                            .replace("{{LOCATION}}", imageDestination(imageEntity))
                            .replace("{{NDA}}", if (destHttps) "" else "- \"-nda\"")
                            .replace("{{NDH}}", if (destHttps) "" else "- \"-ndh\"")
                            .replace("{{GCR}}", if (gcr) "- \"--gcr-delete\"" else ""),
                        V1Job::class.java,
                    ),
                    namespace,
                )
            }.map {
                it.metadata.name
            }

    override fun findImageJobStatus(jobName: String): Uni<ImageJobStatus> =
        Uni
            .createFrom()
            .item {
                kubeService.readJob(jobName, namespace)
            }.map {
                it.status.toImageJobStatus()
            }

    companion object {
        private val uploadTemplate = getResourceAsReaderSync("uploader/upload_template.yaml").readText()
        private val deleteTemplate = getResourceAsReaderSync("uploader/delete_template.yaml").readText()

        // Can't be fixed by ktlint, always leads to violation
        @Suppress("ktlint:standard:curly-spacing")
        private fun getResourceAsReaderSync(fileName: String): Reader =
            {}
                .javaClass.classLoader
                .getResourceAsStream(fileName)
                ?.bufferedReader()
                ?: throw FileNotFoundException("$fileName not found")
    }
}

fun V1JobStatus.toImageJobStatus() =
    when {
        (failed != null && failed > 0) -> ImageJobStatus.FAILED
        (succeeded != null && succeeded > 0) -> ImageJobStatus.COMPLETED
        (active != null && active > 0) -> ImageJobStatus.RUNNING
        (ready != null) -> ImageJobStatus.RUNNING
        else -> throw IllegalStateException("Could not determine job status from $this")
    }
