package org.eurodat.appservice.model

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
@Table(name = "table_security_mapping")
class TableSecurityMappingEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "security_mapping_database_id", unique = true)
    val securityMappingDatabaseId: Int? = null,
    @Column(name = "row_base_output_security_column")
    val rowBaseOutputSecurityColumn: String = "",
    @Column(name = "table_name")
    val tableName: String = "",
    @ManyToOne
    @JoinColumn(name = "app_database_id", nullable = false)
    var app: AppEntity? = null,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TableSecurityMappingEntity

        if (rowBaseOutputSecurityColumn != other.rowBaseOutputSecurityColumn) return false
        if (tableName != other.tableName) return false

        return true
    }

    override fun hashCode(): Int {
        var result = rowBaseOutputSecurityColumn.hashCode()
        result = 31 * result + tableName.hashCode()
        return result
    }
}
