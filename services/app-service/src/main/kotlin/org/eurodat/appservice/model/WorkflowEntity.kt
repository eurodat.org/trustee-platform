package org.eurodat.appservice.model

import jakarta.persistence.Column
import jakarta.persistence.ElementCollection
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table
import jakarta.persistence.UniqueConstraint

@Entity
@Table(
    name = "workflows",
    uniqueConstraints = [
        UniqueConstraint(columnNames = ["app_database_id", "workflow_id"]),
    ],
)
class WorkflowEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "workflow_database_id", unique = true)
    val workflowDatabaseId: Int? = null,
    @Column(name = "workflow_id")
    val workflowId: String = "",
    @Column(name = "resource_name")
    val resourceName: String = "",
    @Column(name = "entry_point")
    val entryPoint: String = "",
    @Column(name = "start_command")
    @ElementCollection(fetch = FetchType.EAGER)
    val startCommand: List<String> = emptyList(),
    @ManyToOne
    val image: ImageEntity,
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "app_database_id", nullable = false)
    var app: AppEntity? = null,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as WorkflowEntity

        return workflowId == other.workflowId
    }

    override fun hashCode(): Int = workflowId.hashCode()
}
