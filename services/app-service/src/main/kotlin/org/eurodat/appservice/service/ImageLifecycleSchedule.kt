package org.eurodat.appservice.service

import io.quarkus.arc.profile.UnlessBuildProfile
import io.quarkus.scheduler.Scheduled
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject

@ApplicationScoped
class ImageLifecycleSchedule {
    @Inject
    private lateinit var imageLifecycle: ImageLifecycle

    @Scheduled(every = "\${imagelifecycle.schedule}", delayed = "2s")
    @UnlessBuildProfile("test")
    fun schedule(): Uni<Void> = imageLifecycle.transitionImages().replaceWithVoid()
}
