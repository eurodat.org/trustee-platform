package org.eurodat.appservice.model

enum class ImageStatus {
    UPLOADING,
    ACTIVE,
    FAILED,
    DELETING,
    DELETED,
}
