package org.eurodat.appservice.resource

import org.eurodat.appservice.model.AppEntity
import org.eurodat.client.appservice.model.AppResponse
import org.eurodat.client.appservice.model.TableSecurityMapping
import org.eurodat.client.appservice.model.WorkflowDefinition

/**
 * Converts an app entity to a client app data class.
 *
 * @param appEntity app entity to be converted
 *
 * @return the app data class object
 */
fun createResponseFromEntity(appEntity: AppEntity): AppResponse =
    AppResponse(
        appEntity.appId,
        appEntity.appOwner,
        appEntity.transactionDDL,
        appEntity.safeDepositDDL,
        appEntity.tableSecurityMappings.map {
            TableSecurityMapping(
                it.rowBaseOutputSecurityColumn,
                it.tableName,
            )
        },
        appEntity.workflows.map {
            WorkflowDefinition(
                it.workflowId,
                it.resourceName,
                it.entryPoint,
                it.startCommand,
                it.image.imageId,
            )
        },
        appEntity.transactionTimeoutInDays,
    )
