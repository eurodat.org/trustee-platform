package org.eurodat.appservice.repository

import io.quarkus.hibernate.reactive.panache.PanacheRepositoryBase
import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.ws.rs.ClientErrorException
import org.eurodat.appservice.model.AppEntity
import java.time.ZonedDateTime

@ApplicationScoped
class PanacheAppRepository :
    AppRepository,
    PanacheRepositoryBase<AppEntity, String> {
    @WithTransaction
    override fun startAppRegistration(app: AppEntity): Uni<AppEntity> =
        findApp(app.appId).onItem().transformToUni { existingApp ->
            if (existingApp != null) {
                Uni.createFrom().failure(ClientErrorException("App with id ${app.appId} already exists.", 409))
            } else {
                app.workflows.forEach { workflow ->
                    workflow.app = app
                }
                app.tableSecurityMappings.forEach { tableSecurityMapping ->
                    tableSecurityMapping.app = app
                }
                persist(app)
            }
        }

    @WithSession
    override fun findApp(appId: String): Uni<AppEntity?> {
        val now = ZonedDateTime.now()
        return list("appId = ?1 and validFrom < ?2 and validTo > ?3", appId, now, now).map { appEntityList ->
            appEntityList.singleOrNull()
        }
    }

    @WithSession
    override fun getAllAppIds(): Uni<List<String>> {
        val now = ZonedDateTime.now()
        return listAll().map { entities ->
            entities.filter { entity -> entity.validFrom < now && entity.validTo!! > now }.map { filteredEntity ->
                filteredEntity.appId
            }
        }
    }

    @WithSession
    override fun deleteApp(appId: String): Uni<Boolean> {
        val now = ZonedDateTime.now()
        return list("appId = ?1 and validFrom < ?2 and validTo > ?3", appId, now, now)
            .map { appEntityList ->
                appEntityList[0]
            }.chain { entity ->
                if (entity != null) {
                    update(
                        "validTo = ?1 where appDatabaseId = ?2",
                        ZonedDateTime.now(),
                        entity.appDatabaseId,
                    ).map { true }
                } else {
                    Uni.createFrom().item(false)
                }
            }
    }
}
