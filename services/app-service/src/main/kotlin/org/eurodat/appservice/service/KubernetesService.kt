package org.eurodat.appservice.service

import io.kubernetes.client.openapi.apis.BatchV1Api
import io.kubernetes.client.openapi.apis.CoreV1Api
import io.kubernetes.client.openapi.models.V1Job
import io.kubernetes.client.openapi.models.V1ObjectMeta
import io.kubernetes.client.openapi.models.V1Secret
import io.kubernetes.client.util.Config
import jakarta.enterprise.context.ApplicationScoped
import org.eclipse.microprofile.config.inject.ConfigProperty

@ApplicationScoped
class KubernetesService(
    @ConfigProperty(name = "kubernetesUrl", defaultValue = "default") private val kubeUrl: String,
) {
    internal var batchApi: BatchV1Api =
        if (kubeUrl == "default") {
            BatchV1Api(Config.defaultClient())
        } else {
            BatchV1Api(Config.fromUrl(kubeUrl))
        }

    internal var coreApi: CoreV1Api =
        if (kubeUrl == "default") {
            CoreV1Api(Config.defaultClient())
        } else {
            CoreV1Api(Config.fromUrl(kubeUrl))
        }

    fun readJob(
        jobName: String,
        namespace: String,
    ): V1Job = batchApi.readNamespacedJobStatus(jobName, namespace).execute()

    fun createJob(
        job: V1Job,
        namespace: String,
    ): V1Job =
        batchApi
            .createNamespacedJob(
                namespace,
                job,
            ).execute()

    fun readSecret(
        secretName: String,
        namespace: String,
    ): Map<String, String> =
        coreApi
            .readNamespacedSecret(secretName, namespace)
            .execute()
            .data
            .mapValues { String(it.value) }

    fun createSecret(
        secretName: String,
        data: Map<String, String>,
        namespace: String,
    ) {
        val dataAsByteArray = data.mapValues { it.value.toByteArray() }
        val secret =
            V1Secret()
                .metadata(V1ObjectMeta().name(secretName))
                .type("Opaque")
                .data(dataAsByteArray)
        coreApi.createNamespacedSecret(namespace, secret).execute()
    }

    fun deleteSecret(
        secretName: String,
        namespace: String,
    ) {
        coreApi.deleteNamespacedSecret(secretName, namespace).execute()
    }

    fun readConfigMap(
        configMapName: String,
        namespace: String,
    ): Map<String, String> = coreApi.readNamespacedConfigMap(configMapName, namespace).execute().data

    fun deleteConfigMap(
        configMapName: String,
        namespace: String,
    ) {
        coreApi.deleteNamespacedConfigMap(configMapName, namespace).execute()
    }
}
