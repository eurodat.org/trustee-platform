package org.eurodat.appservice.service

import io.github.oshai.kotlinlogging.KotlinLogging
import io.kubernetes.client.openapi.ApiException
import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.quarkus.logging.Log
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.enterprise.context.ApplicationScoped
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eurodat.appservice.model.ImageEntity
import org.eurodat.appservice.model.ImageStatus
import org.eurodat.appservice.repository.ImageRepository

private val logger = KotlinLogging.logger {}

@ApplicationScoped
class ImageLifecycle(
    private val imageRepository: ImageRepository,
    private val imageJobScheduler: ImageJobScheduler,
    private val kubeService: KubernetesService,
    @ConfigProperty(name = "uploader.namespace") private val namespace: String,
) {
    private fun getConfigMapName(imageId: String) = "image-location-$imageId"

    private fun postProcessForUpload(image: ImageEntity) {
        if (image.status == ImageStatus.ACTIVE) {
            val newLocation = kubeService.readConfigMap(getConfigMapName(image.imageId), namespace).getValue("image-location")
            image.location = newLocation
        }
        if (image.status == ImageStatus.ACTIVE || image.status == ImageStatus.FAILED) {
            if (image.credentialsSupplied) {
                deleteKubernetesImageCredentialsSecret(image.imageId)
            }
            deleteKubernetesImageLocationConfigMap(image.imageId)
        }
    }

    private fun deleteKubernetesImageLocationConfigMap(imageId: String) {
        try {
            kubeService.deleteConfigMap(getConfigMapName(imageId), namespace)
        } catch (e: ApiException) {
            when (e.code) {
                404 -> Log.warn("Tried to delete configmap for image $imageId, but none were found")
                else -> Log.error("Configmap for image $imageId could not be deleted")
            }
        }
    }

    private fun deleteKubernetesImageCredentialsSecret(imageId: String) {
        try {
            kubeService.deleteSecret(imageId, namespace)
        } catch (e: ApiException) {
            when (e.code) {
                404 -> Log.warn("Tried to delete credentials for image $imageId, but none were found")
                else -> Log.error("Credentials for image $imageId could not be deleted")
            }
        }
    }

    /**
     * Transitions images through their lifecycle states.
     */
    @WithTransaction
    fun transitionImages(): Uni<List<ImageEntity>> = imageRepository.findTransitioningImages().call { images -> transitionImages(images) }

    private fun transitionImages(images: List<ImageEntity>): Uni<List<ImageEntity>> =
        if (images.isEmpty()) {
            uni { listOf() }
        } else {
            Uni
                .join()
                .all(
                    images.map {
                        when (it.status) {
                            ImageStatus.UPLOADING -> handleUploading(it)
                            ImageStatus.DELETING -> handleDeleting(it)
                            else -> uni { it }
                        }
                    },
                ).andCollectFailures()
        }

    /**
     * Start the image upload process. Deletes image credentials after the upload job finished.
     */
    private fun handleUploading(imageEntity: ImageEntity): Uni<ImageEntity> =
        if (imageEntity.uploadJob == null) {
            startUpload(imageEntity)
        } else {
            updateFromJobStatus(imageEntity, ImageStatus.ACTIVE).invoke { image ->
                postProcessForUpload(image)
            }
        }

    /**
     * Start the image upload process.
     */
    private fun startUpload(imageEntity: ImageEntity): Uni<ImageEntity> =
        imageJobScheduler
            .startImageUpload(imageEntity)
            .map { imageEntity.apply { uploadJob = it } }
            .onFailure()
            .recoverWithItem { e ->
                if (e is TerminalJobCreationException) {
                    Log.error("Job creation failed terminally with exception $e")
                    imageEntity.status = ImageStatus.FAILED
                    postProcessForUpload(imageEntity)
                }
                logger.warn(e) { "Failed to start image upload for $imageEntity" }
                imageEntity
            }

    /**
     * Start the image deletion process.
     */
    private fun handleDeleting(imageEntity: ImageEntity): Uni<ImageEntity> =
        if (imageEntity.deleteJob == null) {
            startDeletion(imageEntity)
        } else {
            updateFromJobStatus(imageEntity, ImageStatus.DELETED)
        }

    private fun startDeletion(imageEntity: ImageEntity): Uni<ImageEntity> =
        imageJobScheduler
            .startImageDeletion(imageEntity)
            .map { imageEntity.apply { deleteJob = it } }
            .onFailure()
            .recoverWithItem { e ->
                logger.warn(e) { "Failed to start image deletion for $imageEntity" }
                imageEntity
            }

    /**
     * Update the image status based on the running jobs.
     */
    private fun updateFromJobStatus(
        imageEntity: ImageEntity,
        completionStatus: ImageStatus,
    ): Uni<ImageEntity> {
        val jobName =
            (if (imageEntity.status == ImageStatus.UPLOADING) imageEntity.uploadJob else imageEntity.deleteJob)
                ?: throw IllegalStateException("$imageEntity has no corresponding job to update status")

        return imageJobScheduler
            .findImageJobStatus(jobName)
            .map { it: ImageJobStatus ->
                when (it) {
                    ImageJobStatus.RUNNING -> imageEntity
                    ImageJobStatus.COMPLETED -> {
                        imageEntity.apply { status = completionStatus }
                    }

                    ImageJobStatus.FAILED -> {
                        Log.error("Job for status ${imageEntity.status} failed for ${imageEntity.imageId}")
                        imageEntity.apply { status = ImageStatus.FAILED }
                    }
                }
            }.onFailure()
            .recoverWithItem { e ->
                logger.warn(e) { "Failed to update upload status for $imageEntity" }
                imageEntity
            }
    }
}
