package org.eurodat.appservice.service

import io.argoproj.workflow.apis.ApiException
import io.argoproj.workflow.apis.WorkflowTemplateServiceApi
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1WorkflowTemplate
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1WorkflowTemplateCreateRequest
import io.github.oshai.kotlinlogging.KotlinLogging
import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.ws.rs.NotFoundException
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.appservice.model.AppEntity
import org.eurodat.appservice.model.WorkflowEntity
import org.eurodat.appservice.repository.AppRepository
import org.eurodat.appservice.repository.WorkflowRepository

private val logger = KotlinLogging.logger {}

@ApplicationScoped
class WorkflowHelper {
    @ConfigProperty(name = "workflow-template.namespace")
    private lateinit var workflowTemplateNamespace: String

    @Inject
    private lateinit var workflowRepository: WorkflowRepository

    @Inject
    private lateinit var appRepository: AppRepository

    @RestClient
    private lateinit var workflowTemplateClient: WorkflowTemplateServiceApi

    @WithSession
    fun deleteWorkflow(
        appId: String,
        workflowId: String,
    ): Uni<Unit> =
        getAppOrFailure(appId).flatMap { app ->
            findWorkflowInApp(app, workflowId)
                .flatMap { workflow ->
                    app.workflows.removeAll { it.workflowId == workflowId }
                    workflowRepository.deleteWorkflowById(workflow.workflowDatabaseId!!).flatMap {
                        workflowTemplateClient.workflowTemplateServiceDeleteWorkflowTemplate(
                            workflowTemplateNamespace,
                            workflow.resourceName,
                            "0",
                            null,
                            null,
                            true,
                            "Background",
                            null,
                        ).onFailure { it is ApiException && it.response.status == 404 }
                            .recoverWithItem { logger.warn { "Missing workflow template when deleting workflow $workflowId" } }
                    }
                }.map { }
        }

    fun addWorkflowTemplateToArgo(
        workflowTemplate: IoArgoprojWorkflowV1alpha1WorkflowTemplate,
    ): Uni<IoArgoprojWorkflowV1alpha1WorkflowTemplate> =
        workflowTemplateClient.workflowTemplateServiceCreateWorkflowTemplate(
            workflowTemplateNamespace,
            IoArgoprojWorkflowV1alpha1WorkflowTemplateCreateRequest()
                .template(workflowTemplate)
                .namespace(workflowTemplateNamespace),
        )

    private fun getAppOrFailure(appId: String): Uni<AppEntity> =
        appRepository.findApp(appId).flatMap { app ->
            if (app == null) {
                Uni.createFrom().failure(NotFoundException("App with id $appId not found"))
            } else {
                Uni.createFrom().item(app)
            }
        }

    private fun findWorkflowInApp(
        app: AppEntity,
        workflowId: String,
    ): Uni<WorkflowEntity> {
        val workflow = app.workflows.find { it.workflowId == workflowId }
        return if (workflow == null) {
            Uni.createFrom().failure(NotFoundException("Workflow with id $workflowId not found in app ${app.appId}"))
        } else {
            Uni.createFrom().item(workflow)
        }
    }
}
