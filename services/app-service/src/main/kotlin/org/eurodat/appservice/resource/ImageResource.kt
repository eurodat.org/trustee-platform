package org.eurodat.appservice.resource

import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.quarkus.security.Authenticated
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses
import org.eurodat.appservice.model.AppEntity
import org.eurodat.appservice.model.ImageEntity
import org.eurodat.appservice.model.ImageStatus
import org.eurodat.appservice.repository.AppRepository
import org.eurodat.appservice.service.KubernetesService
import org.eurodat.client.appservice.model.Image
import org.eurodat.client.appservice.model.ImageRequest
import org.jboss.resteasy.reactive.ResponseStatus
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.RestResponse
import java.util.UUID

@Path("apps/{appId}/images")
@Authenticated
class ImageResource(
    private val appRepository: AppRepository,
    private val kubeService: KubernetesService,
    @ConfigProperty(name = "uploader.namespace") private val namespace: String,
) {
    @POST
    @WithTransaction
    @Produces("application/json")
    @APIResponses(
        value = [
            APIResponse(
                responseCode = "201",
                description = "Image created",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = Image::class))],
            ),
            APIResponse(responseCode = "404", description = "App not found"),
        ],
    )
    fun createImage(
        @RestPath appId: String,
        imageRequest: ImageRequest,
    ): Uni<RestResponse<Image>> =
        appRepository.findApp(appId).flatMap { app ->
            if (app == null) {
                Uni.createFrom().failure(NotFoundException("App with id $appId not found"))
            } else {
                val imageId = UUID.randomUUID().toString()
                var credentialsSupplied = false
                when {
                    imageRequest.registryUserName != null && imageRequest.registryPassword != null -> {
                        credentialsSupplied = true
                        kubeService.createSecret(
                            imageId,
                            mutableMapOf(
                                "username" to imageRequest.registryUserName!!,
                                "password" to imageRequest.registryPassword!!,
                            ),
                            namespace,
                        )
                    }

                    imageRequest.registryUserName != null ->
                        return@flatMap Uni
                            .createFrom()
                            .failure(BadRequestException("username was supplied, but no password"))

                    imageRequest.registryPassword != null ->
                        return@flatMap Uni
                            .createFrom()
                            .failure(BadRequestException("password was supplied, but no username"))
                }

                ImageEntity(
                    null,
                    imageId,
                    imageRequest.location,
                    null,
                    ImageStatus.UPLOADING,
                    app,
                    credentialsSupplied = credentialsSupplied,
                ).let { entity ->
                    app.addImage(entity)
                    uni { entity.toImage() }
                }
            }.map { RestResponse.status(RestResponse.Status.CREATED, it) }
        }

    abstract class ListImages : List<Image>

    @GET
    @Produces("application/json")
    @APIResponses(
        value = [
            APIResponse(
                responseCode = "200",
                description = "Images found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ListImages::class))],
            ),
            APIResponse(responseCode = "404", description = "App not found"),
        ],
    )
    fun getImages(
        @RestPath appId: String,
    ): Uni<List<Image>> =
        appRepository.findApp(appId).flatMap { app ->
            app?.let { uni { it.images.map { it.toImage() } } }
                ?: Uni.createFrom().failure(NotFoundException("App with id $appId not found"))
        }

    @GET
    @Path("/{imageId}")
    @Produces("application/json")
    @APIResponses(
        value = [
            APIResponse(
                responseCode = "200",
                description = "Image found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = Image::class))],
            ),
            APIResponse(responseCode = "404", description = "App or image not found"),
        ],
    )
    fun getImage(
        @RestPath appId: String,
        @RestPath imageId: String,
    ): Uni<Image> =
        appRepository.findApp(appId).flatMap { app ->
            if (app == null) {
                Uni.createFrom().failure(NotFoundException("App with id $appId not found"))
            } else {
                app.findImage(imageId)?.let { uni { it.toImage() } }
                    ?: Uni.createFrom().failure(NotFoundException("Image with id $imageId not found"))
            }
        }

    @DELETE
    @Path("/{imageId}")
    @WithTransaction
    @Produces("application/json")
    @ResponseStatus(202)
    @APIResponses(
        value = [
            APIResponse(
                responseCode = "202",
                description = "Image deletion started",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = Image::class))],
            ),
            APIResponse(responseCode = "404", description = "App or image not found"),
            APIResponse(responseCode = "400", description = "Image cannot be deleted"),
        ],
    )
    fun deleteImage(
        @RestPath appId: String,
        @RestPath imageId: String,
    ): Uni<Image> =
        appRepository.findApp(appId).flatMap { app ->
            if (app == null) {
                Uni.createFrom().failure(NotFoundException("App with id $appId not found"))
            } else {
                findImageToDelete(app, imageId)
                    .apply {
                        status =
                            when {
                                status == ImageStatus.FAILED && location == null -> ImageStatus.DELETED
                                else -> ImageStatus.DELETING
                            }
                    }.let { uni { it.toImage() } }
            }
        }

    private fun findImageToDelete(
        app: AppEntity,
        imageId: String,
    ): ImageEntity {
        val imageEntity = app.findImage(imageId) ?: throw NotFoundException("Image with id $imageId not found")
        return imageEntity.takeUnless { listOf(ImageStatus.DELETING, ImageStatus.UPLOADING).contains(it.status) }
            ?: throw BadRequestException("Image cannot be deleted in status ${imageEntity.status}")
    }
}
