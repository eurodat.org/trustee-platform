package org.eurodat.appservice.repository

import io.quarkus.hibernate.reactive.panache.PanacheRepositoryBase
import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import org.eurodat.appservice.model.AppEntity
import org.eurodat.appservice.model.WorkflowEntity
import java.time.ZonedDateTime

@ApplicationScoped
class PanacheWorkflowRepository :
    WorkflowRepository,
    PanacheRepositoryBase<WorkflowEntity, Int> {
    @WithSession
    override fun getAllWorkflowResourceNames(): Uni<List<String>> {
        val now = ZonedDateTime.now()
        return list(
            "from WorkflowEntity w inner join AppEntity a on w.app.appDatabaseId = a.appDatabaseId where a.validFrom < ?1 and a.validTo > ?2",
            now,
            now,
        ).map { entities ->
            entities.map { entity -> entity.resourceName }
        }
    }

    override fun deleteWorkflowById(id: Int): Uni<Boolean> = deleteById(id)

    override fun addWorkflow(workflowEntity: WorkflowEntity): Uni<WorkflowEntity> = persistAndFlush(workflowEntity)

    override fun getWorkflowsForApp(app: AppEntity): Uni<List<WorkflowEntity>> = list("app", app)
}
