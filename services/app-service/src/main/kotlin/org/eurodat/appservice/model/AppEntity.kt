package org.eurodat.appservice.model

import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.OneToMany
import jakarta.persistence.Table
import java.time.ZoneId
import java.time.ZonedDateTime

@Entity
@Table(name = "apps")
class AppEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "app_database_id", unique = true)
    val appDatabaseId: Int? = null,
    @Column(name = "app_owner")
    val appOwner: String,
    @Column(name = "app_id")
    val appId: String = "",
    @Column(name = "valid_from")
    val validFrom: ZonedDateTime = ZonedDateTime.now(),
    @Column(nullable = true, name = "valid_to")
    val validTo: ZonedDateTime? = ZonedDateTime.of(9999, 12, 30, 0, 0, 0, 0, ZoneId.systemDefault()),
    @OneToMany(
        mappedBy = "app",
        fetch = FetchType.EAGER,
        cascade = [CascadeType.ALL],
        targetEntity = WorkflowEntity::class,
    )
    val workflows: MutableList<WorkflowEntity> = mutableListOf(),
    @Column(name = "transaction_ddl")
    val transactionDDL: String = "",
    @Column(name = "safe_deposit_ddl")
    val safeDepositDDL: String = "",
    @OneToMany(
        mappedBy = "app",
        fetch = FetchType.EAGER,
        cascade = [CascadeType.ALL],
        targetEntity = TableSecurityMappingEntity::class,
    )
    val tableSecurityMappings: List<TableSecurityMappingEntity> = listOf(),
    @OneToMany(mappedBy = "app", fetch = FetchType.EAGER, cascade = [CascadeType.ALL], targetEntity = ImageEntity::class)
    val images: MutableList<ImageEntity> = mutableListOf(),
    @Column(name = "transaction_timeout_in_days")
    val transactionTimeoutInDays: Int,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AppEntity

        return appId == other.appId
    }

    override fun hashCode(): Int = appId.hashCode()

    fun addImage(image: ImageEntity) {
        images += image
    }

    fun findImage(imageId: String) = images.find { it.imageId == imageId }
}
