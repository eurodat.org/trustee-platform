package org.eurodat.appservice.model

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table
import org.eurodat.client.appservice.model.Image
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

@Entity
@Table(name = "images")
class ImageEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "image_database_id", unique = true)
    val imageDatabaseId: Int? = null,
    @Column(name = "image_id", unique = true)
    val imageId: String,
    @Column(name = "source_location")
    val sourceLocation: String,
    @Column(name = "location", nullable = true)
    var location: String? = null,
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    var status: ImageStatus,
    @ManyToOne
    @JoinColumn(name = "app_database_id", nullable = false)
    val app: AppEntity,
    @Column(name = "created_at")
    val createdAt: ZonedDateTime = ZonedDateTime.now().truncatedTo(ChronoUnit.MICROS),
    @Column(name = "upload_job")
    var uploadJob: String? = null,
    @Column(name = "delete_job")
    var deleteJob: String? = null,
    @Column(name = "credentials_supplied")
    var credentialsSupplied: Boolean,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ImageEntity

        return imageId == other.imageId
    }

    override fun hashCode(): Int = imageId.hashCode()

    override fun toString(): String =
        "ImageEntity(imageId='$imageId', sourceLocation='$sourceLocation', location='$location', status=$status, appId=${app.appId})"

    fun toImage(): Image =
        Image(imageId, sourceLocation, location, org.eurodat.client.appservice.model.ImageStatus.valueOf(status.name), createdAt)
}
