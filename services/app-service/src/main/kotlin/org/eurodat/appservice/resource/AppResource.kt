package org.eurodat.appservice.resource

import io.quarkus.security.Authenticated
import io.quarkus.security.PermissionsAllowed
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.inject.Inject
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.ClientErrorException
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.ForbiddenException
import jakarta.ws.rs.GET
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.Response
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses
import org.eurodat.appservice.model.AppEntity
import org.eurodat.appservice.model.ImageStatus
import org.eurodat.appservice.model.TableSecurityMappingEntity
import org.eurodat.appservice.repository.AppRepository
import org.eurodat.appservice.service.WorkflowHelper
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.appservice.model.AppResponse
import org.jboss.resteasy.reactive.ResponseStatus
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.RestResponse

@Path("/apps")
@Authenticated
class AppResource(
    @ConfigProperty(name = "app.transaction-timeout-in-days") private val defaultTransactionTimeoutInDays: Int,
) {
    @Inject
    private lateinit var appRepository: AppRepository

    @Inject
    private lateinit var workflowHelper: WorkflowHelper

    @Inject
    private lateinit var jwt: JsonWebToken

    private fun checkParticipant(jwt: JsonWebToken): String =
        jwt.getClaim("participant") ?: throw ForbiddenException("Missing 'participant' claim in access token")

    /***
     * Provides app registration endpoint.
     * Access restricted to 'app:write' permission.
     *
     * @param app The AppRegistration object.
     * @return A `Uni<Boolean>` object.
     */
    @POST
    @PermissionsAllowed("app:write")
    @Produces("application/json")
    @APIResponses(
        value = [
            APIResponse(
                responseCode = "201",
                description = "App registered successfully",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = AppResponse::class))],
            ),
            APIResponse(responseCode = "409", description = "App already exists"),
        ],
    )
    fun startAppRegistration(app: AppRegistration): Uni<RestResponse<AppResponse>> {
        val timeoutInDays: Int = app.transactionTimeoutInDays ?: defaultTransactionTimeoutInDays

        return appRepository
            .startAppRegistration(
                AppEntity(
                    appId = app.id,
                    appOwner = checkParticipant(jwt),
                    transactionDDL = app.transactionDDL,
                    safeDepositDDL = app.safeDepositDDL,
                    tableSecurityMappings =
                        app.tableSecurityMapping.map {
                            TableSecurityMappingEntity(
                                null,
                                it.rowBaseOutputSecurityColumn,
                                it.tableName,
                            )
                        },
                    transactionTimeoutInDays = timeoutInDays,
                ),
            ).map { RestResponse.status(Response.Status.CREATED, createResponseFromEntity(it)) }
    }

    @GET
    @Path("/{appId}")
    @Produces("application/json")
    fun findApp(
        @RestPath appId: String,
    ): Uni<AppResponse> =
        appRepository.findApp(appId).flatMap { foundApp ->
            if (foundApp != null) {
                Uni.createFrom().item(createResponseFromEntity(foundApp))
            } else {
                Uni.createFrom().failure(NotFoundException("App with id $appId not found"))
            }
        }

    /**
     * Retrieves all IDs of the apps in the database that are currently valid. An app is considered valid if its
     * validity period has not yet expired, i.e., the current date is between the app's `validFrom` and `validTo`
     * dates.
     *
     * @return A `Uni<List<String>>` object. If the retrieval process is successful, it emits a list of app IDs. Each
     * ID in the list corresponds to an app that is currently valid.
     */
    @GET
    @Produces("application/json")
    fun getAllAppIds(): Uni<List<String>> = appRepository.getAllAppIds()

    /**
     * Sets an app to be valid until a date in the past, which effectively makes it invalid, and deletes its associated
     * resources (e.g., workflow templates in Argo workflows server, roles in the database)
     * After deletion, the app will still exist in the database, but it will not be considered valid.
     *
     * This function is internal and used by the `deleteApp` function, only if the app with the given ID exists in the
     * database.
     *
     * @param app The app to be deleted.
     * @return A `Uni<Unit>` object.
     */
    private fun deleteAppInternal(app: AppEntity): Uni<Unit> {
        val undeletedImages = app.images.filter { it.status != ImageStatus.DELETED }
        if (undeletedImages.isNotEmpty()) {
            val imageIdsString = undeletedImages.foldRight("") { it, acc -> "$acc \n ${it.imageId}" }
            return Uni.createFrom().failure(BadRequestException("The following associated images were not deleted: \n $imageIdsString"))
        }
        val workflowTemplateUniJoinBuilder = Uni.join().builder<Unit>().add(uni { })
        app.workflows.map { workflowTemplateUniJoinBuilder.add(workflowHelper.deleteWorkflow(app.appId, it.workflowId)) }
        return workflowTemplateUniJoinBuilder
            .joinAll()
            .andFailFast()
            .flatMap { appRepository.deleteApp(app.appId) }
            .map { }
    }

    /**
     * Checks if an app with the specified ID exists in the database and token claim 'participant' matches 'appOwner'.
     * If it does, the function deletes it using the `deleteAppInternal` function.
     * Access restricted to 'app:write' permission.
     *
     * @param appId The ID of the app to be deleted.
     * @return A `Uni<Unit>` object.
     */
    @DELETE
    @Path("/{appId}")
    @PermissionsAllowed("app:write")
    @ResponseStatus(204)
    @APIResponses(
        value = [
            APIResponse(responseCode = "204", description = "App deleted successfully"),
            APIResponse(responseCode = "404", description = "App with the specified ID not found"),
        ],
    )
    fun deleteApp(
        @RestPath appId: String,
    ): Uni<Unit> =
        appRepository
            .findApp(appId)
            .chain { app ->
                val participant: String = checkParticipant(jwt)
                if (app != null) {
                    if (app.appOwner == participant) {
                        deleteAppInternal(app)
                    } else {
                        Uni.createFrom().failure(ClientErrorException("App with id ${app.appId} not owned by $participant", 401))
                    }
                } else {
                    Uni.createFrom().failure { NotFoundException("App with id $appId not found") }
                }
            }
}
