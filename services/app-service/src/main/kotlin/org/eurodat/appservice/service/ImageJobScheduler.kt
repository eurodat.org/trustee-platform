package org.eurodat.appservice.service

import io.smallrye.mutiny.Uni
import org.eurodat.appservice.model.ImageEntity

interface ImageJobScheduler {
    fun startImageUpload(imageEntity: ImageEntity): Uni<String>

    fun findImageJobStatus(jobName: String): Uni<ImageJobStatus>

    fun startImageDeletion(imageEntity: ImageEntity): Uni<String>
}

enum class ImageJobStatus {
    RUNNING,
    COMPLETED,
    FAILED,
}
