package org.eurodat.appservice.repository

import io.smallrye.mutiny.Uni
import org.eurodat.appservice.model.AppEntity
import org.eurodat.appservice.model.WorkflowEntity

interface WorkflowRepository {
    fun getAllWorkflowResourceNames(): Uni<List<String>>

    fun deleteWorkflowById(id: Int): Uni<Boolean>

    fun addWorkflow(workflowEntity: WorkflowEntity): Uni<WorkflowEntity>

    fun getWorkflowsForApp(app: AppEntity): Uni<List<WorkflowEntity>>
}
