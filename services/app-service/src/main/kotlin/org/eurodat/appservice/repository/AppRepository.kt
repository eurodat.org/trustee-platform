package org.eurodat.appservice.repository

import io.smallrye.mutiny.Uni
import org.eurodat.appservice.model.AppEntity

interface AppRepository {
    fun startAppRegistration(app: AppEntity): Uni<AppEntity>

    fun findApp(appId: String): Uni<AppEntity?>

    fun getAllAppIds(): Uni<List<String>>

    fun deleteApp(appId: String): Uni<Boolean>
}
