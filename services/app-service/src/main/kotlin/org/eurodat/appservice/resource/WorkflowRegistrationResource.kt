package org.eurodat.appservice.resource

import io.argoproj.workflow.apis.ApiException
import io.argoproj.workflow.models.ConfigMapVolumeSource
import io.argoproj.workflow.models.Container
import io.argoproj.workflow.models.ContainerPort
import io.argoproj.workflow.models.EnvVar
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1ExecutorConfig
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1Inputs
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1Metadata
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1Parameter
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1PodGC
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1TTLStrategy
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1Template
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1WorkflowSpec
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1WorkflowTemplate
import io.argoproj.workflow.models.KeyToPath
import io.argoproj.workflow.models.LocalObjectReference
import io.argoproj.workflow.models.ObjectMeta
import io.argoproj.workflow.models.Volume
import io.argoproj.workflow.models.VolumeMount
import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.quarkus.logging.Log
import io.quarkus.security.Authenticated
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.inject.Inject
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.ClientErrorException
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses
import org.eurodat.appservice.model.AppEntity
import org.eurodat.appservice.model.ImageEntity
import org.eurodat.appservice.model.ImageStatus
import org.eurodat.appservice.model.WorkflowEntity
import org.eurodat.appservice.repository.AppRepository
import org.eurodat.appservice.repository.WorkflowRepository
import org.eurodat.appservice.service.WorkflowHelper
import org.eurodat.client.appservice.model.WorkflowDefinition
import org.eurodat.client.appservice.model.WorkflowRegistrationRequest
import org.hibernate.exception.ConstraintViolationException
import org.jboss.resteasy.reactive.ResponseStatus
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.RestResponse
import java.util.UUID

private const val CA_BUNDLE_PATH = "/etc/certs/eurodat/ca-bundle.crt"

@Path("apps/{appId}/workflows")
class WorkflowRegistrationResource {
    @ConfigProperty(name = "workflow-template.namespace")
    private lateinit var workflowTemplateNamespace: String

    @ConfigProperty(name = "workflow-template.service-account")
    private lateinit var workflowTemplateServiceAccount: String

    @Inject
    internal lateinit var appRepository: AppRepository

    @Inject
    internal lateinit var workflowRepository: WorkflowRepository

    @Inject
    internal lateinit var workflowHelper: WorkflowHelper

    @POST
    @WithTransaction
    @Authenticated
    @APIResponses(
        value = [
            APIResponse(responseCode = "409", description = "Workflow with the same name already exists for the app"),
            APIResponse(responseCode = "400", description = "Bad request"),
            APIResponse(
                responseCode = "201",
                description = "Workflow created",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = WorkflowDefinition::class))],
            ),
        ],
    )
    fun addWorkflow(
        @RestPath appId: String,
        request: WorkflowRegistrationRequest,
    ): Uni<RestResponse<WorkflowDefinition>> {
        if (!Regex("[a-z0-9]([-a-z0-9]*[a-z0-9])?(\\\\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*").matches(request.workflowId)) {
            return Uni.createFrom().failure(
                BadRequestException(
                    "workflowId must consist of lower case alphanumeric characters, '-' or '.', " +
                        "and must start and end with an alphanumeric character (RFC1123)",
                ),
            )
        }
        val workflowResourceName = "${request.workflowId}-${UUID.randomUUID()}"
        return getAppOrFailure(appId).flatMap { app ->
            getActiveImageOrFailure(app, request.imageId).flatMap { image ->
                workflowRepository
                    .addWorkflow(
                        WorkflowEntity(
                            workflowId = request.workflowId,
                            resourceName = workflowResourceName,
                            entryPoint = createWorkflowTemplateName(request.workflowId),
                            app = app,
                            startCommand = request.startCommand,
                            image = image,
                        ),
                    ).onFailure { it is ConstraintViolationException && it.constraintName == "unique_app_workflow_id" }
                    .transform { _ ->
                        ClientErrorException("Workflowtemplate with name ${request.workflowId} already exists for app $appId", 409)
                    }.call { it ->
                        workflowHelper
                            .addWorkflowTemplateToArgo(
                                createWorkflowTemplate(
                                    request.workflowId,
                                    workflowResourceName,
                                    appId,
                                    image.location!!,
                                    request.startCommand,
                                ),
                            ).onFailure { it is ApiException && it.response.status == 409 }
                            .invoke { _ -> Log.error("Resource name $workflowResourceName is already in use.") }
                    }
            }
        }
            .map { RestResponse.status(RestResponse.Status.CREATED, workflowDefinition(it)) }
    }

    @DELETE
    @Path("{workflowId}")
    @WithTransaction
    @Authenticated
    @ResponseStatus(204)
    @APIResponses(
        value = [
            APIResponse(responseCode = "404", description = "Workflow or app not found"),
            APIResponse(responseCode = "204", description = "Workflow deleted"),
        ],
    )
    fun deleteWorkflow(
        @RestPath appId: String,
        @RestPath workflowId: String,
    ): Uni<Unit> = workflowHelper.deleteWorkflow(appId, workflowId)

    @GET
    @Path("{workflowId}")
    @WithTransaction
    @Authenticated
    @APIResponses(
        value = [
            APIResponse(responseCode = "404", description = "Workflow or app not found"),
            APIResponse(
                responseCode = "200",
                description = "Workflow found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = WorkflowDefinition::class))],
            ),
        ],
    )
    fun getWorkflow(
        @RestPath appId: String,
        @RestPath workflowId: String,
    ): Uni<WorkflowDefinition> =
        getAppOrFailure(appId)
            .flatMap { app ->
                val workflow = app.workflows.find { it.workflowId == workflowId }
                if (workflow == null) {
                    Uni.createFrom().failure(NotFoundException("Workflow with id $workflowId not found for app $appId"))
                } else {
                    uni { workflowDefinition(workflow) }
                }
            }

    private fun workflowDefinition(workflow: WorkflowEntity) =
        WorkflowDefinition(
            workflow.workflowId,
            workflow.resourceName,
            workflow.entryPoint,
            workflow.startCommand,
            workflow.image.imageId,
        )

    @GET
    @Authenticated
    @WithTransaction
    @APIResponses(
        value = [
            APIResponse(responseCode = "404", description = "App not found"),
            APIResponse(
                responseCode = "200",
                description = "Workflows found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = List::class))],
            ),
        ],
    )
    fun getWorkflows(
        @RestPath appId: String,
    ): Uni<List<String>> =
        getAppOrFailure(appId)
            .onItem().transform { appEntity ->
                appEntity.workflows.map { it.workflowId }
            }

    private fun createWorkflowTemplate(
        workflowId: String,
        resourceName: String,
        appId: String,
        imagePath: String,
        startCommand: List<String>,
    ): IoArgoprojWorkflowV1alpha1WorkflowTemplate {
        val templateMetadata =
            ObjectMeta()
                .name(resourceName)
                .namespace(workflowTemplateNamespace)
                .generateName("$resourceName-")
        val templateSpec =
            IoArgoprojWorkflowV1alpha1WorkflowSpec()
                .serviceAccountName(workflowTemplateServiceAccount)
                .entrypoint(createWorkflowTemplateName(workflowId))
                .executor(
                    IoArgoprojWorkflowV1alpha1ExecutorConfig().serviceAccountName(workflowTemplateServiceAccount),
                ).imagePullSecrets(
                    listOf(LocalObjectReference().name("regcred")),
                ).ttlStrategy(
                    IoArgoprojWorkflowV1alpha1TTLStrategy()
                        .secondsAfterCompletion(300)
                        .secondsAfterFailure(86400),
                ).podGC(
                    IoArgoprojWorkflowV1alpha1PodGC().strategy("OnPodSuccess"),
                ).templates(
                    listOf(
                        IoArgoprojWorkflowV1alpha1Template()
                            .name(createWorkflowTemplateName(workflowId))
                            .podSpecPatch(
                                """
                                {
                                    "containers": [
                                        {
                                            "name": "main",
                                            "resources": {
                                                "requests": {
                                                    "cpu": "100m",
                                                    "memory": "500Mi"
                                                }
                                            }
                                        }
                                    ]
                                }
                                """.trimIndent(),
                            )
                            .inputs(
                                IoArgoprojWorkflowV1alpha1Inputs()
                                    .parameters(
                                        listOf(
                                            IoArgoprojWorkflowV1alpha1Parameter().name("messaging_url"),
                                            IoArgoprojWorkflowV1alpha1Parameter().name("messaging_token"),
                                            IoArgoprojWorkflowV1alpha1Parameter().name("database_host_name"),
                                            IoArgoprojWorkflowV1alpha1Parameter().name("database_name"),
                                            IoArgoprojWorkflowV1alpha1Parameter().name("database_user_login"),
                                            IoArgoprojWorkflowV1alpha1Parameter().name("database_password_login"),
                                        ),
                                    ),
                            ).metadata(
                                IoArgoprojWorkflowV1alpha1Metadata().labels(
                                    mapOf(
                                        "app" to appId,
                                        "requester" to "{{workflow.uid}}",
                                        "priviledged" to "false",
                                    ),
                                ),
                            ).container(
                                Container()
                                    .image(imagePath)
                                    .imagePullPolicy(Container.ImagePullPolicyEnum.ALWAYS)
                                    .env(
                                        listOf(
                                            EnvVar().name("MESSAGING_URL").value("{{workflow.parameters.messaging_url}}"),
                                            EnvVar().name("MESSAGING_TOKEN").value("{{workflow.parameters.messaging_token}}"),
                                            EnvVar().name("CA_BUNDLE").value(CA_BUNDLE_PATH),
                                            EnvVar().name("PG_HOSTNAME").value("{{workflow.parameters.database_host_name}}"),
                                            EnvVar().name("PG_DATABASENAME").value("{{workflow.parameters.database_name}}"),
                                            EnvVar().name("PG_USERNAME").value("{{workflow.parameters.database_user_login}}"),
                                            EnvVar().name("PG_PASSWORD").value("{{workflow.parameters.database_password_login}}"),
                                        ),
                                    ).command(startCommand)
                                    .ports(
                                        listOf(
                                            ContainerPort()
                                                .name("app-port")
                                                .containerPort(8080),
                                        ),
                                    ).volumeMounts(
                                        listOf(
                                            VolumeMount()
                                                .name("ca-bundle")
                                                .mountPath(CA_BUNDLE_PATH)
                                                .subPath("ca-bundle.crt")
                                                .readOnly(true),
                                        ),
                                    ),
                            ).volumes(
                                listOf(
                                    Volume()
                                        .name("ca-bundle")
                                        .configMap(
                                            ConfigMapVolumeSource()
                                                .name("local-ca-bundle")
                                                .items(listOf(KeyToPath().key("ca-bundle.crt").path("ca-bundle.crt")))
                                                .defaultMode(420),
                                        ),
                                ),
                            ),
                    ),
                )

        return IoArgoprojWorkflowV1alpha1WorkflowTemplate()
            .metadata(templateMetadata)
            .spec(templateSpec)
            .kind("WorkflowTemplate")
            .apiVersion("argoproj.io/v1alpha1")
    }

    private fun getActiveImageOrFailure(
        app: AppEntity,
        id: String,
    ): Uni<ImageEntity> {
        val image = app.images.find { it.imageId == id }
        return when {
            image == null -> Uni.createFrom().failure(BadRequestException("No image with id $id is registered for app ${app.appId}"))
            image.status == ImageStatus.ACTIVE && image.location != null -> Uni.createFrom().item(image)
            else -> Uni.createFrom().failure(BadRequestException("Image with id $id is not active or has no location."))
        }
    }

    private fun getAppOrFailure(appId: String): Uni<AppEntity> =
        appRepository.findApp(appId).flatMap { app ->
            if (app == null) {
                Uni.createFrom().failure(NotFoundException("App with id $appId not found"))
            } else {
                Uni.createFrom().item(app)
            }
        }

    private fun createWorkflowTemplateName(workflowId: String): String {
        return "start-$workflowId"
    }
}
