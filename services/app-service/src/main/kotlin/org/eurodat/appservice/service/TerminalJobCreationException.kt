package org.eurodat.appservice.service

class TerminalJobCreationException(
    originalException: Exception,
) : Exception(originalException.message)
