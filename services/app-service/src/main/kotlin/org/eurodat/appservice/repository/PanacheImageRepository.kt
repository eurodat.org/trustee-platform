package org.eurodat.appservice.repository

import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.quarkus.hibernate.reactive.panache.kotlin.PanacheRepositoryBase
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import org.eurodat.appservice.model.ImageEntity
import org.eurodat.appservice.model.ImageStatus

@ApplicationScoped
class PanacheImageRepository :
    ImageRepository,
    PanacheRepositoryBase<ImageEntity, String> {
    @WithSession
    override fun findTransitioningImages(): Uni<List<ImageEntity>> =
        find("status in ?1", listOf(ImageStatus.UPLOADING, ImageStatus.DELETING)).list()
}
