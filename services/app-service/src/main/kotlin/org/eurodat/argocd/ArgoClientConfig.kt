package org.eurodat.argocd

import io.smallrye.config.ConfigMapping

@ConfigMapping(prefix = "eurodat.argo")
interface ArgoClientConfig {
    fun apiKeyPath(): String

    fun serverBaseUrl(): String
}
