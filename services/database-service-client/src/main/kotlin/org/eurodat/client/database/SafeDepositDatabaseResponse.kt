package org.eurodat.client.database

data class SafeDepositDatabaseResponse(
    val response: String = "",
)
