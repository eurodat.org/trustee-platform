package org.eurodat.client.database

import io.smallrye.mutiny.Uni
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam

@Path("/transaction-database")
interface TransactionDatabaseClientBase {
    @POST
    fun createTransactionDatabase(request: TransactionDatabaseRequest): Uni<Unit>

    @POST
    @Path("/roles")
    fun createClientRole(request: ClientRoleCreationRequest): Uni<Unit>

    @DELETE
    @Path("/roles/{clientId}")
    fun deleteClientRole(
        @PathParam("clientId") clientId: String,
    ): Uni<Unit>

    @DELETE
    fun deleteTransactionDatabase(request: TransactionDatabaseRequest): Uni<Unit>
}
