package org.eurodat.client.database

data class TransactionDatabaseRequest(
    val appId: String = "",
    val transactionId: String = "",
    val consumer: List<String> = emptyList(),
    val provider: List<String> = emptyList(),
)
