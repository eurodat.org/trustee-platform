package org.eurodat.client.database

data class ClientRoleCreationRequest(
    val client: String = "",
)
