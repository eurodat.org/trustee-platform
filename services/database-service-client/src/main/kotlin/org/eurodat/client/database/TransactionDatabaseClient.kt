package org.eurodat.client.database

import io.quarkus.oidc.token.propagation.reactive.AccessTokenRequestReactiveFilter
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient

@RegisterRestClient(configKey = "transaction-database-service")
@RegisterProvider(AccessTokenRequestReactiveFilter::class)
interface TransactionDatabaseClient : TransactionDatabaseClientBase
