package org.eurodat.client.database

import io.quarkus.oidc.client.filter.OidcClientFilter
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient

@RegisterRestClient(configKey = "transaction-database-service-oidc-client-filter")
@OidcClientFilter("transaction-database-service-oidc-client-filter")
interface TransactionDatabaseOidcClientFilterClient : TransactionDatabaseClientBase
