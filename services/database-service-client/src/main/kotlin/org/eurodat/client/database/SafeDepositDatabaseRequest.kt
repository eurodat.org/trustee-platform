package org.eurodat.client.database

data class SafeDepositDatabaseRequest(
    val appId: String = "",
)
