package org.eurodat.client.datamanagement.model

import com.fasterxml.jackson.annotation.JsonProperty

data class PersistentDataRequest(
    @JsonProperty("data") val data: List<String>,
    @JsonProperty("table") val table: String,
    @JsonProperty("appId") val appId: String,
)
