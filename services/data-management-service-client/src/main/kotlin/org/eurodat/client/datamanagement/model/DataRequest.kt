package org.eurodat.client.datamanagement.model

import com.fasterxml.jackson.annotation.JsonProperty

data class DataRequest(
    @JsonProperty("data") val data: List<String>,
    @JsonProperty("table") val table: String,
    @JsonProperty("transactionId") val transactionId: String,
)
