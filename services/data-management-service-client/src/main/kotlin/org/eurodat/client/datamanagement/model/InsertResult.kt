package org.eurodat.client.datamanagement.model

data class InsertResult(val result: String)
