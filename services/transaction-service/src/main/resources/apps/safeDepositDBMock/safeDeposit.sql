create schema if not exists input;

create table input.input
(
    input_id integer not null primary key,
    poll_id integer,
    submission integer,
    participant_identifier text not null,
    created_at    timestamp not null default NOW()
);

comment on column input.input.input_id is 'the tables primary key';

comment on column input.input.poll_id is 'the submission round number';

comment on column input.input.submission is 'the submission of the participant';

comment on column input.input.participant_identifier is 'the participants identifier';

comment on column input.input.created_at is 'the timestamp when the record was created';
