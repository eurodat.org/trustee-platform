ALTER TABLE message
ALTER COLUMN workflow_id TYPE VARCHAR(64);

DROP INDEX IF EXISTS idx_message_app_id_selector;

CREATE INDEX idx_message_selector_app_id_correlation_id ON message (selector, app_id, correlation_id);
