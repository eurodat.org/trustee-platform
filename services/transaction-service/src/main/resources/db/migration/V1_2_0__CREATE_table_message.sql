CREATE TABLE message
(
    correlation_id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    app_id         VARCHAR(42)                 NOT NULL,
    transaction_id VARCHAR(42)                 NOT NULL,
    workflow_id    VARCHAR(42)                 NOT NULL,
    message_label  VARCHAR                     NOT NULL,
    selector       VARCHAR(42)                 NOT NULL,
    created_at     TIMESTAMP(6) WITH TIME ZONE NOT NULL
);

CREATE INDEX idx_message_app_id_selector ON message (app_id, selector);
