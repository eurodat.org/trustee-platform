CREATE TABLE policies
(
    participant_id VARCHAR   NOT NULL, -- owner of the safe-deposit box
    principal_id   VARCHAR   NOT NULL, -- need dummy value - legal person for whom the owner of the safe deposit box stores data
    valid_from     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    valid_to       TIMESTAMP,
    table_naming   VARCHAR   NOT NULL, -- table level security, schema.table
    app_name       VARCHAR   NOT NULL, -- application of the data service provider
    workflow_name  VARCHAR   NOT NULL, -- dummy-value ALL_WORKFLOWS
    PRIMARY KEY (participant_id, principal_id, valid_from)
);
