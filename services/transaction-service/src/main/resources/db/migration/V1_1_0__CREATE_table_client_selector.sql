CREATE TABLE client_selector_mapping
(
    client_id       VARCHAR NOT NULL,
    client_selector VARCHAR NOT NULL UNIQUE,
    PRIMARY KEY (client_id)
);
