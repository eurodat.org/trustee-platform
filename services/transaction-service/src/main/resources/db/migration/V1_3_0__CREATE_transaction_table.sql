CREATE TABLE transactions
(
    id         VARCHAR PRIMARY KEY,
    app_id     VARCHAR NOT NULL,
    client_id  VARCHAR NOT NULL,
    start_time TIMESTAMP,
    end_time   TIMESTAMP
);

CREATE TABLE consumer
(
    transaction_id VARCHAR REFERENCES transactions(id),
    consumer VARCHAR,
    PRIMARY KEY (transaction_id, consumer)
);

CREATE TABLE provider
(
    transaction_id VARCHAR REFERENCES transactions(id),
    provider VARCHAR,
    PRIMARY KEY (transaction_id, provider)
);
