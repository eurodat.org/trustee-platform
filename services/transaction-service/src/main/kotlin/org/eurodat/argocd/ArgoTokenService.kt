package org.eurodat.argocd

import io.argoproj.workflow.apis.TokenService
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import java.io.File

@ApplicationScoped
class ArgoTokenService(
    @Inject
    internal var config: ArgoClientConfig,
) : TokenService {
    override fun getToken(): String = File(config.apiKeyPath()).bufferedReader().readLine()
}
