package org.eurodat.openapi

import io.smallrye.config.ConfigMapping

@ConfigMapping(prefix = "eurodat.openapi")
interface OpenApiConfig {
    fun fqdn(): String

    fun version(): String

    fun docs(): Docs

    interface Docs {
        fun url(): String

        fun description(): String
    }
}
