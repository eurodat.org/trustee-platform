package org.eurodat.openapi

import io.smallrye.config.SmallRyeConfig
import io.smallrye.openapi.api.models.ExternalDocumentationImpl
import io.smallrye.openapi.api.util.FilterUtil
import io.smallrye.openapi.api.util.UnusedSchemaFilter
import org.eclipse.microprofile.config.ConfigProvider
import org.eclipse.microprofile.openapi.OASFilter
import org.eclipse.microprofile.openapi.models.OpenAPI

/**
 * Filter and update the paths of the OpenAPI specification at runtime.
 */
class Filter : OASFilter {
    override fun filterOpenAPI(openAPI: OpenAPI?) {
        val ignoreTags = listOf("User Resource")
        val pathsToRemove = mutableSetOf<String>()
        openAPI?.paths?.pathItems?.forEach pathIteration@{ (name, path) ->
            path.operations.forEach operationIteration@{ (_, operation) ->
                if (operation.tags[0] in ignoreTags) {
                    pathsToRemove.add(name)
                    return@pathIteration
                }
            }
        }
        pathsToRemove.forEach {
            openAPI?.paths?.removePathItem(it)
        }
        if (pathsToRemove.isNotEmpty()) {
            FilterUtil.applyFilter(UnusedSchemaFilter(), openAPI)
        }

        val config = ConfigProvider.getConfig().unwrap(SmallRyeConfig::class.java)
        val openApiConfig: OpenApiConfig

        try {
            openApiConfig = config.getConfigMapping(OpenApiConfig::class.java)
        } catch (ex: NoSuchElementException) {
            return
        }

        openAPI?.info?.version(openApiConfig.version())
        val externalDocumentation = ExternalDocumentationImpl()
        externalDocumentation.description(openApiConfig.docs().description())
        externalDocumentation.url(openApiConfig.docs().url())
        openAPI?.externalDocs(externalDocumentation)
    }
}
