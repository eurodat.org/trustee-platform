package org.eurodat.transactionservice.model

import io.quarkus.hibernate.reactive.panache.kotlin.PanacheEntityBase
import jakarta.persistence.CollectionTable
import jakarta.persistence.Column
import jakarta.persistence.ElementCollection
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.Table
import java.time.LocalDateTime

@Entity
@Table(name = "transactions")
class TransactionEntity(
    @Id
    @Column(name = "id")
    val id: String,
    @Column(name = "app_id")
    val appId: String,
    @Column(name = "client_id")
    val clientId: String,
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "consumer", joinColumns = [JoinColumn(name = "transaction_id")])
    val consumer: List<String> = mutableListOf(),
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "provider", joinColumns = [JoinColumn(name = "transaction_id")])
    val provider: List<String> = mutableListOf(),
    @Column(nullable = true, name = "start_time")
    var startTime: LocalDateTime?,
    @Column(nullable = true, name = "end_time")
    var endTime: LocalDateTime?,
) : PanacheEntityBase
