package org.eurodat.transactionservice.service

import org.eurodat.client.transactionservice.Transaction
import org.eurodat.transactionservice.model.TransactionEntity

class TransactionMapper {
    companion object {
        fun createDataClassFromEntity(transactionEntity: TransactionEntity): Transaction =
            Transaction(
                transactionEntity.id,
                transactionEntity.appId,
                transactionEntity.clientId,
                transactionEntity.consumer,
                transactionEntity.provider,
                transactionEntity.startTime,
                transactionEntity.endTime,
            )
    }
}
