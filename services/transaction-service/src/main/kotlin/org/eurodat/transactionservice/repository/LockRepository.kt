package org.eurodat.transactionservice.repository

import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.persistence.LockModeType
import jakarta.ws.rs.NotFoundException
import org.eurodat.transactionservice.model.LockEntity
import org.hibernate.reactive.mutiny.Mutiny.Session

@ApplicationScoped
class LockRepository {
    /**
     * Inserts the identifier into the LOCK table if it does not exist already.
     */
    fun createLockIdentifier(
        session: Session,
        identifier: String,
    ): Uni<Int> =
        session
            .createNativeQuery<Int>("INSERT INTO LOCK (identifier) VALUES (:identifier) ON CONFLICT DO NOTHING")
            .setParameter("identifier", identifier)
            .executeUpdate()

    /**
     * Acquire an exclusive lock for the duration of the transaction on the identifier.
     */
    fun acquireLock(
        session: Session,
        identifier: String,
    ): Uni<String> =
        session.find(LockEntity::class.java, identifier, LockModeType.PESSIMISTIC_WRITE).map {
            if (it == null) {
                throw NotFoundException("Lock with identifier $identifier not found.")
            }
            identifier
        }
}
