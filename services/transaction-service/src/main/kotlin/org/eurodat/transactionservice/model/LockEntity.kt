package org.eurodat.transactionservice.model

import io.quarkus.hibernate.reactive.panache.kotlin.PanacheEntityBase
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

@Table(name = "LOCK")
@Entity
class LockEntity(
    @Id
    val identifier: String,
) : PanacheEntityBase {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as LockEntity

        return identifier == other.identifier
    }

    override fun hashCode(): Int = identifier.hashCode()
}
