package org.eurodat.transactionservice.model

data class ClientCertificateValues(
    val name: String,
    val certificate: String,
)
