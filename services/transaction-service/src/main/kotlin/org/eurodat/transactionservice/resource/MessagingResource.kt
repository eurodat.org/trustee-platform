package org.eurodat.transactionservice.resource

import io.quarkus.security.Authenticated
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.GET
import jakarta.ws.rs.HeaderParam
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.Produces
import jakarta.ws.rs.QueryParam
import jakarta.ws.rs.WebApplicationException
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement
import org.eurodat.client.transactionservice.Message
import org.eurodat.client.transactionservice.MessageRequest
import org.eurodat.transactionservice.model.MessageClaims
import org.eurodat.transactionservice.repository.ClientMappingRepository
import org.eurodat.transactionservice.repository.MessageRepository
import org.eurodat.transactionservice.repository.TransactionRepository
import org.eurodat.transactionservice.service.TokenService

/**
 * The MessagingResource class represents the REST endpoints for messages for fetching and posting messages.
 */
@Path("")
class MessagingResource {
    @Inject
    internal lateinit var jwt: JsonWebToken

    @Inject
    internal lateinit var messageRepository: MessageRepository

    @Inject
    internal lateinit var transactionRepository: TransactionRepository

    @Inject
    internal lateinit var clientMapper: ClientMappingRepository

    /**
     * Fetches messages for a specific appId.
     *
     * @param appId The ID of the app to fetch messages for.
     * @param excludeUntil (Optional) Exclude messages before this timestamp.
     * @return A list of messages as `Uni<List<Message>>`.
     *         On success, returns the list of messages of a participant filtered by the provided appId and `excludeUntil` timestamp.
     *         If no messages are found, an empty list will be returned. If the `Authorization` header is missing or invalid,
     *         a `WebApplicationException` is thrown with a 401 status code (Unauthorized).
     */
    @Authenticated
    @GET
    @Path("apps/{appId}/messages")
    @Produces("application/json")
    fun getMessages(
        @PathParam("appId") appId: String,
        @QueryParam("excludeUntil") excludeUntil: Long?,
    ): Uni<List<Message>> {
        val clientId = jwt.getClaim<String>("client_id")
        return clientMapper.getClientSelector(clientId).chain { selector ->
            messageRepository.findMessages(appId, selector, excludeUntil)
        }
    }

    /**
     * Posts a new message for each selector.
     *
     * @param request The message request payload. It contains the message content and the selectors that define the message recipients.
     * @param authorizationHeader The token for authorization.
     * @return A list of created messages as `Uni<List<Message>>`.
     *         On success, returns the list of created messages. If the `Authorization` header is missing or invalid,
     *         a `WebApplicationException` is thrown with a 401 status code (Unauthorized).
     *         If the provided selectors are invalid (i.e., they don't match the allowed selectors for the client),
     *         a `WebApplicationException` is thrown with a 400 status code (Bad Request).
     */
    @POST
    @Path("messages")
    @Consumes("application/json")
    @Produces("application/json")
    @APIResponse(responseCode = "200", description = "OK")
    @APIResponse(responseCode = "401", description = "Unauthorized")
    @APIResponse(responseCode = "400", description = "Invalid selectors")
    @SecurityRequirement(name = "custom-bearer")
    fun postMessage(
        @RequestBody(
            description = "Message request",
            required = true,
            content = [Content(schema = Schema(implementation = MessageRequest::class))],
        ) request: MessageRequest,
        @Parameter(
            description = "Bearer token",
            required = true,
            schema = Schema(type = SchemaType.STRING),
        )
        @HeaderParam("Authorization") authorizationHeader: String,
    ): Uni<List<Message>> {
        if (!authorizationHeader.startsWith("Bearer ")) {
            throw WebApplicationException("Authorization header is invalid", 401)
        }
        val token = authorizationHeader.substring("Bearer ".length)

        val claims = TokenService.extractClaimsFromToken(token)
        validateSelectors(request.selectors, claims.allowedSelectors)
        val messages =
            request.selectors.map { selector ->
                createMessage(claims, request.message, selector)
            }
        return validateTransaction(claims.transactionId).chain { _ ->
            messageRepository.storeMessages(messages)
        }
    }

    private fun validateTransaction(transactionId: String): Uni<Unit> {
        return transactionRepository.findTransaction(transactionId)
            .map { transaction ->
                transaction ?: throw NotFoundException("Transaction id not found")
            }
            .map { transaction ->
                if (transaction.endTime != null) {
                    throw WebApplicationException("Cannot send messages for an ended transaction", 400)
                }
            }
    }
}

/**
 * Validates the selectors from the request.
 *
 * @param requestSelectors The selectors provided in the request.
 * @param allowedSelectors The allowed selectors for the client.
 * @throws WebApplicationException If any of the selectors are invalid.
 */
fun validateSelectors(
    requestSelectors: List<String>,
    allowedSelectors: List<String>,
) {
    if (!requestSelectors.all { allowedSelectors.contains(it) }) {
        throw WebApplicationException("Invalid selectors", 400)
    }
}

/**
 * Creates a new message based on the claims and label.
 *
 * @param claims The extracted message claims.
 * @param label The message label.
 * @param selector The message selector.
 * @return A new Message object.
 */
fun createMessage(
    claims: MessageClaims,
    label: String,
    selector: String,
): Message =
    Message(
        appId = claims.appId,
        transactionId = claims.transactionId,
        workflowId = claims.workflowId,
        label = label,
        selector = selector,
    )
