package org.eurodat.transactionservice.service

import io.github.oshai.kotlinlogging.KotlinLogging
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.infrastructure.Infrastructure
import io.smallrye.mutiny.vertx.MutinyHelper
import io.vertx.core.Vertx
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import org.eurodat.transactionservice.repository.LockRepository
import java.util.function.Supplier

val logger = KotlinLogging.logger { }

@ApplicationScoped
class LockService {
    @Inject
    lateinit var lockRepository: LockRepository

    @Inject
    lateinit var transactionProvider: TransactionProvider

    /**
     * Executes the given supplier inside a transaction where a lock on the lock identifier is acquired.
     * The lock identifier is created if it does not exist already.
     */
    fun <R> executeWithLock(
        lockIdentifier: String,
        supplier: Supplier<Uni<R>>,
    ): Uni<R> =
        ensureLockExists(lockIdentifier)
            .chain { _ ->
                transactionProvider.withTransaction {
                    lockRepository
                        .acquireLock(it, lockIdentifier)
                        .chain { _ -> supplier.get() }
                }
            }

    /**
     * Executes the given blocking supplier inside a transaction where a lock on the lock identifier is acquired.
     * The lock identifier is created if it does not exist already.
     */
    fun <R> executeBlockingWithLock(
        lockIdentifier: String,
        supplier: Supplier<R>,
    ): Uni<R> {
        val currentContext = Vertx.currentContext()
        return executeWithLock(lockIdentifier) {
            Uni
                .createFrom()
                .item(supplier)
                .runSubscriptionOn(Infrastructure.getDefaultWorkerPool())
                .emitOn(MutinyHelper.executor(currentContext))
        }
    }

    /**
     * Creates the lock identifier if it does not exist already.
     */
    private fun ensureLockExists(lockIdentifier: String): Uni<String> =
        transactionProvider.withTransaction {
            lockRepository.createLockIdentifier(it, lockIdentifier).map {
                lockIdentifier
            }
        }
}
