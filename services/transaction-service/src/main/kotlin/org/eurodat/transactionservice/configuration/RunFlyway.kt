package org.eurodat.transactionservice.configuration

import io.quarkus.runtime.StartupEvent
import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.event.Observes
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.flywaydb.core.Flyway

@ApplicationScoped
class RunFlyway {
    @ConfigProperty(name = "quarkus.datasource.reactive.url")
    internal lateinit var datasourceUrl: String

    @ConfigProperty(name = "quarkus.datasource.username")
    internal lateinit var datasourceUsername: String

    @ConfigProperty(name = "quarkus.datasource.password")
    internal lateinit var datasourcePassword: String

    @ConfigProperty(name = "quarkus.flyway.schemas")
    internal lateinit var schema: String

    fun runFlywayMigration(
        @Observes event: StartupEvent?,
    ) {
        val datasourceJDBCUrl: String = datasourceUrl.replaceFirst("vertx-reactive:", "jdbc:")
        val flyway: Flyway =
            Flyway
                .configure()
                .dataSource(datasourceJDBCUrl, datasourceUsername, datasourcePassword)
                .schemas(schema)
                .load()
        flyway.migrate()
    }
}
