package org.eurodat.transactionservice.repository

import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.quarkus.hibernate.reactive.panache.kotlin.PanacheRepositoryBase
import io.quarkus.panache.common.Sort
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import org.eurodat.client.transactionservice.Message
import org.eurodat.transactionservice.model.MessageEntity

@ApplicationScoped
class PanacheMessageRepository :
    MessageRepository,
    PanacheRepositoryBase<MessageEntity, Long> {
    @WithTransaction
    override fun storeMessages(messages: List<Message>): Uni<List<Message>> {
        val uniJoinBuilder = Uni.join().builder<MessageEntity>()

        messages.forEach { message ->
            uniJoinBuilder.add(
                persist(
                    MessageEntity(
                        null,
                        message.appId,
                        message.transactionId,
                        message.workflowId,
                        message.label,
                        message.selector,
                    ),
                ),
            )
        }

        return uniJoinBuilder
            .joinAll()
            .andFailFast()
            .call { _ -> flush() }
            .map { list -> list.map { it.toMessage() } }
    }

    @WithSession
    override fun findMessages(
        appId: String,
        participantId: String,
        excludeUntil: Long?,
    ): Uni<List<Message>> =
        find(
            "appId = ?1 and selector = ?2 and correlationId > ?3",
            Sort.ascending("correlationId"),
            appId,
            participantId,
            excludeUntil ?: Long.MIN_VALUE,
        ).list()
            .map { it.map(MessageEntity::toMessage) }

    @WithTransaction
    override fun deleteMessagesForTransaction(transactionId: String): Uni<Unit> = delete("transactionId", transactionId).map { }
}
