package org.eurodat.transactionservice.service

import io.quarkus.arc.profile.UnlessBuildProfile
import io.quarkus.scheduler.Scheduled
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject

@ApplicationScoped
class TransactionLifecycleSchedule {
    @Inject
    private lateinit var transactionLifecycle: TransactionLifecycle

    @Scheduled(every = "\${transactionlifecycle.schedule}", delayed = "2s")
    @UnlessBuildProfile("test")
    fun schedule(): Uni<Void> =
        transactionLifecycle
            .terminateOverdueTransactions()
            .replaceWithVoid()
}
