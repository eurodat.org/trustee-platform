package org.eurodat.transactionservice.repository

import io.smallrye.mutiny.Uni
import org.eurodat.client.transactionservice.Transaction
import org.eurodat.client.transactionservice.TransactionRequest
import org.eurodat.transactionservice.model.TransactionEntity

interface TransactionRepository {
    fun createTransaction(transactionRequest: TransactionRequest): Uni<Transaction>

    fun findTransaction(id: String): Uni<Transaction?>

    fun endTransaction(transactionId: String): Uni<Transaction>

    fun getAllTransactionIds(): Uni<List<String>>

    fun getParticipantsByTransactionId(transactionId: String): Uni<List<String>>

    fun getTransactionsByAppId(appId: String): Uni<List<TransactionEntity>>
}
