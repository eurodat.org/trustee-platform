package org.eurodat.transactionservice.service

import io.smallrye.mutiny.Uni
import org.hibernate.reactive.mutiny.Mutiny
import java.util.function.Function

interface TransactionProvider {
    fun <T> withTransaction(work: Function<Mutiny.Session, Uni<T>>): Uni<T>
}
