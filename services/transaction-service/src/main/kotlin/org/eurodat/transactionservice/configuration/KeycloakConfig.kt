package org.eurodat.transactionservice.configuration

import io.smallrye.config.ConfigMapping

@ConfigMapping(prefix = "eurodat.keycloak")
interface KeycloakConfig {
    fun realm(): String

    fun serverUrl(): String
}
