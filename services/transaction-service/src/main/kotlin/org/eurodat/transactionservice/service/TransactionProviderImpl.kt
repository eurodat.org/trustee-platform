package org.eurodat.transactionservice.service

import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import org.hibernate.reactive.mutiny.Mutiny.Session
import org.hibernate.reactive.mutiny.Mutiny.SessionFactory
import java.util.function.Function

@ApplicationScoped
class TransactionProviderImpl : TransactionProvider {
    @Inject
    lateinit var sessionFactory: SessionFactory

    override fun <T> withTransaction(work: Function<Session, Uni<T>>): Uni<T> = sessionFactory.withTransaction { session -> work.apply(session) }
}
