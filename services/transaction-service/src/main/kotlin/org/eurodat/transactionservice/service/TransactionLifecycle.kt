package org.eurodat.transactionservice.service

import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.quarkus.logging.Log
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.replaceWithUnit
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.AppServiceExternalClient
import org.eurodat.client.appservice.model.AppResponse
import org.eurodat.client.database.TransactionDatabaseOidcClientFilterClient
import org.eurodat.client.database.TransactionDatabaseRequest
import org.eurodat.transactionservice.model.TransactionEntity
import org.eurodat.transactionservice.repository.MessageRepository
import org.eurodat.transactionservice.repository.TransactionRepository
import java.time.LocalDateTime

@ApplicationScoped
class TransactionLifecycle {
    @Inject
    private lateinit var transactionRepository: TransactionRepository

    @RestClient
    private lateinit var appServiceClient: AppServiceExternalClient

    @RestClient
    private lateinit var transactionDatabaseClient: TransactionDatabaseOidcClientFilterClient

    @Inject
    private lateinit var messageRepository: MessageRepository

    @WithTransaction
    fun terminateOverdueTransactions(): Uni<Unit> =
        getAppIds()
            .flatMap { appIds ->
                if (appIds.isEmpty()) {
                    Uni.createFrom().item(Unit)
                } else {
                    Uni
                        .join()
                        .all(
                            appIds.map { appId ->
                                getApp(appId).flatMap { app ->
                                    retrieveTransactions(appId).flatMap { transactions ->
                                        if (transactions.isEmpty()) {
                                            Uni.createFrom().item(Unit)
                                        } else {
                                            val builder = Uni.join().builder<Unit>()
                                            transactions.map { transaction ->
                                                builder.add(
                                                    terminateOverdueTransaction(transaction, app.transactionTimeoutInDays),
                                                )
                                            }
                                            builder
                                                .joinAll()
                                                .andCollectFailures()
                                        }
                                    }
                                }
                            },
                        ).andCollectFailures()
                        .replaceWithUnit()
                }
            }

    private fun getAppIds(): Uni<List<String>> = appServiceClient.getAllAppIds()

    private fun getApp(appId: String): Uni<AppResponse> = appServiceClient.findApp(appId)

    private fun retrieveTransactions(appId: String): Uni<List<TransactionEntity>> = transactionRepository.getTransactionsByAppId(appId)

    private fun terminateOverdueTransaction(
        transaction: TransactionEntity,
        timeoutInDays: Int,
    ): Uni<Unit> =
        if (isTransactionOverdue(transaction, timeoutInDays)) {
            transactionDatabaseClient
                .deleteTransactionDatabase(TransactionDatabaseRequest(transaction.appId, transaction.id))
                .call { _ ->
                    messageRepository.deleteMessagesForTransaction(transaction.id)
                }.chain { _ -> transactionRepository.endTransaction(transaction.id) }
                .invoke { it -> Log.info("Terminated overdue transaction ${it.id}.") }
                .map { }
        } else {
            Uni.createFrom().item(Unit)
        }

    private fun isTransactionOverdue(
        transaction: TransactionEntity,
        timeoutInDays: Int,
    ): Boolean =
        if (transaction.endTime != null || transaction.startTime == null) {
            false
        } else {
            transaction.startTime!! <
                LocalDateTime
                    .now()
                    .minusDays(timeoutInDays.toLong())
        }
}
