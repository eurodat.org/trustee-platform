package org.eurodat.transactionservice.model

import io.quarkus.hibernate.reactive.panache.kotlin.PanacheEntityBase
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.Table
import org.eurodat.client.transactionservice.Message
import java.time.ZonedDateTime
import java.time.ZonedDateTime.now
import java.time.temporal.ChronoUnit

@Entity
@Table(name = "message")
class MessageEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "correlation_id")
    val correlationId: Long? = null,
    @Column(name = "app_id")
    val appId: String,
    @Column(name = "transaction_id")
    val transactionId: String,
    @Column(name = "workflow_id")
    val workflowId: String,
    @Column(name = "message_label")
    val messageLabel: String,
    @Column(name = "selector")
    val selector: String,
    @Column(name = "created_at")
    val createdAt: ZonedDateTime = now().truncatedTo(ChronoUnit.MICROS),
) : PanacheEntityBase {
    fun toMessage(): Message =
        Message(
            correlationId = correlationId,
            appId = appId,
            transactionId = transactionId,
            workflowId = workflowId,
            label = messageLabel,
            selector = selector,
            createdAt = createdAt,
        )
}
