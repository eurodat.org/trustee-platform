package org.eurodat.transactionservice.service

import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1SubmitOpts
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1WorkflowSubmitRequest
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eurodat.client.appservice.model.WorkflowDefinition
import org.eurodat.client.credential.Credentials
import org.eurodat.transactionservice.model.MessageClaims
import org.eurodat.transactionservice.repository.TransactionRepository
import java.util.UUID

@ApplicationScoped
class ArgoWorkflowsHelperService {
    @ConfigProperty(name = "eurodat.messaging.url")
    private lateinit var messagingUrl: String

    @Inject
    private lateinit var transactionRepository: TransactionRepository

    private fun toSubmitRequest(
        workflow: WorkflowDefinition,
        namespace: String,
        parameters: List<String>,
        workflowName: String,
    ): IoArgoprojWorkflowV1alpha1WorkflowSubmitRequest =
        IoArgoprojWorkflowV1alpha1WorkflowSubmitRequest().apply {
            this.resourceName = workflow.resourceName
            this.namespace = namespace
            this.resourceKind = "WorkflowTemplate"
            this.submitOptions =
                IoArgoprojWorkflowV1alpha1SubmitOpts().apply {
                    this.name = workflowName // truncate to 63 chars
                    this.parameters = parameters
                }
        }

    fun prepareArgoRequest(
        dbCredentials: Credentials,
        provisionNamespace: String,
        workflow: WorkflowDefinition,
        transactionId: String,
        appId: String,
    ): Uni<IoArgoprojWorkflowV1alpha1WorkflowSubmitRequest> =
        transactionRepository.getParticipantsByTransactionId(transactionId)
            .map { allowedSelectors ->
                createArgoRequestWithVaultCredentials(
                    dbCredentials,
                    provisionNamespace,
                    workflow,
                    transactionId,
                    appId,
                    allowedSelectors,
                )
            }

    private fun createArgoRequestWithVaultCredentials(
        dbCredentials: Credentials,
        provisionNamespace: String,
        workflow: WorkflowDefinition,
        transactionId: String,
        appId: String,
        allowedSelectors: List<String>,
    ): IoArgoprojWorkflowV1alpha1WorkflowSubmitRequest {
        val splittedDBURL = dbCredentials.jdbcUrl.split('/')
        val databaseHost = splittedDBURL[splittedDBURL.size - 2].split(':').first()
        val workflowName = "${workflow.resourceName.take(26)}-${UUID.randomUUID()}"

        val messageClaims =
            MessageClaims(
                appId = appId,
                transactionId = transactionId,
                workflowId = workflowName,
                allowedSelectors = allowedSelectors,
            )
        val messagingToken = TokenService.generateToken(messageClaims)

        val parameters =
            listOf(
                "database_host_name=$databaseHost",
                "database_name=${splittedDBURL.last()}",
                "database_user_login=${dbCredentials.username}",
                "database_password_login=${dbCredentials.password}",
                "messaging_url=$messagingUrl",
                "messaging_token=$messagingToken",
            )

        return toSubmitRequest(workflow, provisionNamespace, parameters, workflowName)
    }
}
