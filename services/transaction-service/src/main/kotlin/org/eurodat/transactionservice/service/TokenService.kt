package org.eurodat.transactionservice.service

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import jakarta.ws.rs.WebApplicationException
import org.eurodat.transactionservice.model.MessageClaims
import java.util.Base64

object TokenService {
    private val objectMapper = ObjectMapper()

    fun generateToken(claims: MessageClaims): String {
        val rootNode = objectMapper.createObjectNode()
        rootNode.put("app_id", claims.appId)
        rootNode.put("transaction_id", claims.transactionId)
        rootNode.put("workflow_id", claims.workflowId)

        val allowedSelectorsArray = objectMapper.createArrayNode()
        claims.allowedSelectors.forEach { allowedSelectorsArray.add(it) }
        rootNode.set<JsonNode>("allowed_selectors", allowedSelectorsArray)

        val claimsJson = objectMapper.writeValueAsString(rootNode)
        val base64Token = Base64.getEncoder().encodeToString(claimsJson.toByteArray())

        return base64Token
    }

    fun extractClaimsFromToken(token: String): MessageClaims =
        try {
            val decodedJson = String(Base64.getDecoder().decode(token))
            val rootNode: JsonNode = objectMapper.readTree(decodedJson)

            val appId = rootNode["app_id"]?.asText() ?: throw IllegalArgumentException("Missing app_id")
            val transactionId = rootNode["transaction_id"]?.asText() ?: throw IllegalArgumentException("Missing transaction_id")
            val workflowId = rootNode["workflow_id"]?.asText() ?: throw IllegalArgumentException("Missing workflow_id")

            val allowedSelectorsNode = rootNode["allowed_selectors"] ?: throw IllegalArgumentException("Missing allowed_selectors")
            val allowedSelectors = mutableListOf<String>()
            if (allowedSelectorsNode.isArray) {
                for (selectorNode in allowedSelectorsNode) {
                    allowedSelectors.add(selectorNode.asText())
                }
            } else {
                throw IllegalArgumentException("allowed_selectors is not a valid array")
            }

            MessageClaims(
                appId = appId,
                transactionId = transactionId,
                workflowId = workflowId,
                allowedSelectors = allowedSelectors,
            )
        } catch (e: Exception) {
            throw WebApplicationException("Invalid token", e)
        }
}
