package org.eurodat.transactionservice.repository

import io.quarkus.hibernate.reactive.panache.PanacheRepositoryBase
import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.quarkus.panache.common.Parameters
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.ws.rs.NotFoundException
import org.eurodat.client.transactionservice.ClientSelectorMapping
import org.eurodat.transactionservice.model.ClientSelectorEntity

@ApplicationScoped
class PanacheClientMappingRepository :
    ClientMappingRepository,
    PanacheRepositoryBase<ClientSelectorEntity, String> {
    @WithSession
    override fun getClientMappings(): Uni<List<ClientSelectorMapping>> =
        listAll().map {
            it.stream().map { entity -> ClientSelectorMapping(entity.clientId, entity.clientSelector) }.toList()
        }

    @WithSession
    override fun getAllSelectors(): Uni<List<String>> =
        listAll().map {
            it.stream().map { entity -> entity.clientSelector }.toList()
        }

    @WithSession
    override fun getClientSelector(clientId: String): Uni<String> =
        findById(clientId).map {
            it ?: throw NotFoundException("Client with ID $clientId not found")
            it.clientSelector
        }

    @WithTransaction
    override fun saveClientMapping(clientMapping: ClientSelectorMapping): Uni<ClientSelectorMapping> =
        find(
            "clientId = :clientId or clientSelector = :clientSelector",
            Parameters
                .with("clientId", clientMapping.clientId)
                .and("clientSelector", clientMapping.clientSelector),
        ).firstResult<ClientSelectorEntity>()
            .chain { entity ->
                if (entity != null) {
                    require(entity.clientId != clientMapping.clientId) { "Client with client id ${clientMapping.clientId} already exists" }
                    throw IllegalArgumentException("Client with selector ${clientMapping.clientSelector} already exists")
                }
                persist(ClientSelectorEntity(clientMapping.clientId, clientMapping.clientSelector))
            }.map { clientMapping }

    /**
     Finds clientMapping for the client and deletes it.
     Returns "false" if the client was not found.
     */
    @WithTransaction
    override fun deleteClientMapping(clientId: String): Uni<Boolean> =
        findById(clientId)
            .chain { entity ->
                if (entity != null) {
                    delete(entity).map { _ ->
                        true
                    }
                } else {
                    Uni.createFrom().item(false)
                }
            }

    @WithSession
    override fun clientExists(clientId: String): Uni<Boolean> = findById(clientId).map { it != null }
}
