package org.eurodat.transactionservice.model

class MessageClaims(
    val appId: String,
    val transactionId: String,
    val workflowId: String,
    val allowedSelectors: List<String>,
)
