package org.eurodat.transactionservice.resource

import io.quarkus.security.Authenticated
import io.quarkus.security.UnauthorizedException
import io.smallrye.common.annotation.Blocking
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import jakarta.ws.rs.WebApplicationException
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.AppServiceClient
import org.eurodat.client.database.TransactionDatabaseClient
import org.eurodat.client.database.TransactionDatabaseRequest
import org.eurodat.client.transactionservice.AppRequest
import org.eurodat.client.transactionservice.Transaction
import org.eurodat.client.transactionservice.TransactionRequest
import org.eurodat.transactionservice.repository.ClientMappingRepository
import org.eurodat.transactionservice.repository.MessageRepository
import org.eurodat.transactionservice.repository.TransactionRepository
import org.jboss.resteasy.reactive.RestPath
import org.slf4j.MDC

/**
 * The TransactionResource provides REST endpoints to manage transactions in the EuroDaT platform.
 * This Resource can start new transactions, find existing transactions, end transactions
 * and perform cleanup tasks (e.g., deleting transaction databases and messages).
 */
@Path("transactions")
@Authenticated
class TransactionResource(
    @RestClient private val appServiceClient: AppServiceClient,
    @RestClient private val transactionDatabaseClient: TransactionDatabaseClient,
    private val transactionRepository: TransactionRepository,
    private val clientMappingRepository: ClientMappingRepository,
    private val messageRepository: MessageRepository,
    private val jwt: JsonWebToken,
) {
    /**
     * Finds a specific transaction.
     *
     * @param transactionId The ID of the transaction.
     * @return A Uni of a `Transaction` containing the transaction details.
     * @throws NotFoundException If the transaction is not found.
     */
    @GET
    @Path("/{transactionId}")
    @Produces("application/json")
    fun findTransaction(
        @RestPath transactionId: String,
    ): Uni<Transaction> {
        MDC.put("transactionId", transactionId)
        return transactionRepository.findTransaction(transactionId)
            .map { transaction ->
                transaction ?: throw NotFoundException("Transaction id not found")
            }
    }

    /**
     * Ends a transaction by:
     * 1) Deleting the associated transaction database.
     * 2) Deleting all messages associated with the transaction.
     * 3) Marking the transaction as ended.
     *
     * @param transactionId The ID of the transaction to end.
     * @return A Uni `Transaction` representing the ended transaction.
     * @throws NotFoundException If the transaction is not found or has already been ended.
     * @throws UnauthorizedException if the provided token is invalid.
     */
    @DELETE
    @Path("/{transactionId}")
    @Produces("application/json")
    fun endTransaction(
        @RestPath transactionId: String,
    ): Uni<Transaction> {
        MDC.put("transactionId", transactionId)
        return transactionRepository
            .findTransaction(transactionId)
            .map { transaction ->
                transaction ?: throw NotFoundException("Transaction id not found")
            }
            .chain { transaction ->
                return@chain if (transaction.endTime != null) {
                    Uni.createFrom().failure(NotFoundException("Transaction has already been ended"))
                } else {
                    transactionDatabaseClient.deleteTransactionDatabase(TransactionDatabaseRequest(transaction.appId, transaction.id))
                }
            }.call { _ ->
                messageRepository.deleteMessagesForTransaction(transactionId)
            }.chain { _ -> transactionRepository.endTransaction(transactionId) }
    }

    /**
     * Starts a new transaction. It retrieves the app details and the participants for the transaction (consumer and provider lists).
     * A transaction is created in the transaction repository including the associated transaction database.
     *
     * @param request The details of the transaction to start, including app ID, consumer, and provider participants.
     * @return A Uni `Transaction` representing the newly created transaction.
     * @throws NotFoundException If the specified app ID does not exist.
     * @throws UnauthorizedException if the provided token is invalid.
     */
    @POST
    @Blocking
    @Produces("application/json")
    fun startTransaction(request: AppRequest): Uni<Transaction> {
        val clientId = jwt.getClaim<String>("client_id")
        return appServiceClient
            .findApp(request.appId)
            .onFailure { it is WebApplicationException && it.response.status == 404 }
            .transform { NotFoundException("App id ${request.appId} not found") }
            .chain { _ ->
                getParticipantsForTransaction(request).chain { (consumerList, providerList) ->
                    val transactionRequest = TransactionRequest(clientId, request.appId, consumerList, providerList)
                    transactionRepository.createTransaction(transactionRequest)
                }
            }.call { transaction ->
                MDC.put("transactionId", transaction.id)
                transactionDatabaseClient.createTransactionDatabase(
                    TransactionDatabaseRequest(request.appId, transaction.id, transaction.consumer, transaction.provider),
                )
            }
    }

    private fun getParticipantsForTransaction(request: AppRequest): Uni<Pair<List<String>, List<String>>> =
        clientMappingRepository.getAllSelectors().map { clients ->
            if (request.consumer.isNotEmpty()) {
                if (request.provider.isNotEmpty()) {
                    Pair(request.consumer, request.provider)
                } else {
                    Pair(request.consumer, clients)
                }
            } else {
                if (request.provider.isNotEmpty()) {
                    Pair(clients, request.provider)
                } else {
                    Pair(clients, clients)
                }
            }
        }
}
