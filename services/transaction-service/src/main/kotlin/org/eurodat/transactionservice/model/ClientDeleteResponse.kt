package org.eurodat.transactionservice.model

data class ClientDeleteResponse(
    var keycloakClientDelete: Boolean,
    var clientMapperDelete: Boolean,
)
