package org.eurodat.transactionservice.resource

import io.argoproj.workflow.apis.WorkflowServiceApi
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1Workflow
import io.quarkus.security.Authenticated
import io.quarkus.security.UnauthorizedException
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.GET
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.Produces
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.AppServiceClient
import org.eurodat.client.credential.DatabaseCredentialClient
import org.eurodat.client.transactionservice.WorkflowGetResponse
import org.eurodat.client.transactionservice.WorkflowMetadata
import org.eurodat.client.transactionservice.WorkflowRequest
import org.eurodat.client.transactionservice.WorkflowStartResponse
import org.eurodat.client.transactionservice.WorkflowStatus
import org.eurodat.transactionservice.repository.ClientMappingRepository
import org.eurodat.transactionservice.repository.TransactionRepository
import org.eurodat.transactionservice.service.ArgoWorkflowsHelperService
import org.slf4j.MDC

/**
 * The WorkflowResource provides REST endpoints to manage workflows for transactions.
 * This includes starting workflows for a given transaction and
 * retrieving the status of a specific workflow associated with a transaction.
 */
@Path("transactions")
@Authenticated
class WorkflowResource(
    @RestClient private val appServiceClient: AppServiceClient,
    @RestClient private val dbCredService: DatabaseCredentialClient,
    @RestClient private val workflowClient: WorkflowServiceApi,
    private val argoWorkflowsHelperService: ArgoWorkflowsHelperService,
    private val transactionRepository: TransactionRepository,
    private val clientMappingRepository: ClientMappingRepository,
    private val jwt: JsonWebToken,
    @ConfigProperty(name = "eurodat.provision.namespace") private val provisionNamespace: String,
) {
    /**
     * Starts a workflow for the specified transaction ID.
     *
     * @param transactionId The transaction ID.
     * @param workflowRequest The workflow request includes the workflow definition ID.
     * @return A Uni containing the WorkflowStartResponse, which includes the transaction ID,
     *         workflow definition ID, and the Argo Workflow name.
     * @throws NotFoundException If the transaction is not found, the client is not authorized,
     *                           or the workflow definition ID is invalid for the given app.
     * @throws UnauthorizedException if the provided token is invalid.
     */
    @POST
    @Path("/{transactionId}/workflows")
    @Produces("application/json")
    fun startWorkflow(
        @PathParam("transactionId") transactionId: String,
        workflowRequest: WorkflowRequest,
    ): Uni<WorkflowStartResponse> =
        transactionRepository.findTransaction(transactionId)
            .map { transaction ->
                transaction ?: throw NotFoundException("Transaction id not found")
            }
            .flatMap { transaction ->
                val clientId = jwt.getClaim<String>("client_id")
                clientMappingRepository.getClientSelector(clientId).flatMap { clientSelector ->
                    if (transaction.consumer.contains(clientSelector)) {
                        MDC.put("transactionId", transactionId)
                        appServiceClient.findApp(transaction.appId).flatMap { app ->
                            val appId = app.id
                            MDC.put("appId", appId)
                            val workflow =
                                app.workflows.find { w -> w.id == workflowRequest.workflowDefinitionId }
                                    ?: throw NotFoundException(
                                        "Workflow definition id ${workflowRequest.workflowDefinitionId} does not correspond to app id $appId",
                                    )

                            dbCredService
                                .readInternalAccessCredentials(transactionId)
                                .chain { dbCreds ->
                                    argoWorkflowsHelperService.prepareArgoRequest(
                                        dbCreds,
                                        provisionNamespace,
                                        workflow,
                                        transactionId,
                                        appId,
                                    )
                                }
                                .chain { request ->
                                    workflowClient.workflowServiceSubmitWorkflow(provisionNamespace, request)
                                }.map {
                                    WorkflowStartResponse(transactionId, workflowRequest.workflowDefinitionId, it.metadata.name)
                                }
                        }
                    } else {
                        throw NotFoundException("ClientId: $clientId is not authorized to start workflow")
                    }
                }
            }

    /**
     * Retrieves a specific workflow associated with a transaction.
     *
     * @param transactionId The ID of the transaction associated with the workflow.
     * @param workflowId The ID of the workflow to retrieve.
     * @return A Uni containing the WorkflowGetResponse, which includes metadata and status of the workflow.
     * @throws NotFoundException If the workflow is not found.
     * @throws UnauthorizedException if the provided token is invalid.
     */
    @GET
    @Path("/{transactionId}/workflows/{workflowId}")
    @Produces("application/json")
    fun getWorkflow(
        @PathParam("transactionId") transactionId: String,
        @PathParam("workflowId") workflowId: String,
    ): Uni<WorkflowGetResponse> =
        workflowClient
            .workflowServiceGetWorkflow(provisionNamespace, workflowId, "", "")
            .onFailure { it is io.argoproj.workflow.apis.ApiException && it.response.status == 404 }
            .transform { NotFoundException(it.message) }
            .map { createWorkflowGetResponse(it) }

    private fun createWorkflowGetResponse(workflow: IoArgoprojWorkflowV1alpha1Workflow): WorkflowGetResponse {
        val metadata = WorkflowMetadata(name = workflow.metadata.name)
        val status =
            WorkflowStatus.fromString(
                workflow.status.phase,
                workflow.status.startedAt?.toString(),
                workflow.status.finishedAt?.toString(),
            )

        return WorkflowGetResponse(metadata, status)
    }
}
