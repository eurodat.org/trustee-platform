package org.eurodat.transactionservice.repository

import io.smallrye.mutiny.Uni
import org.eurodat.client.transactionservice.ClientSelectorMapping

interface ClientMappingRepository {
    fun saveClientMapping(clientMapping: ClientSelectorMapping): Uni<ClientSelectorMapping>

    fun deleteClientMapping(clientId: String): Uni<Boolean>

    fun getClientMappings(): Uni<List<ClientSelectorMapping>>

    fun getAllSelectors(): Uni<List<String>>

    fun getClientSelector(clientId: String): Uni<String>

    fun clientExists(clientId: String): Uni<Boolean>
}
