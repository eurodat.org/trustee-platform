package org.eurodat.transactionservice.model

data class ClientRegistrationForm(
    val name: String,
    val certificate: String,
    val clientSelector: String,
    val purpose: Set<Purpose>,
)

enum class Purpose {
    APPS,
    TRANSACTIONS,
    SAFEDEPOSITS,
}
