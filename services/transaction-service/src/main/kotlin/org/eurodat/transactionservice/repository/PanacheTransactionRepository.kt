package org.eurodat.transactionservice.repository

import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.quarkus.hibernate.reactive.panache.kotlin.PanacheRepositoryBase
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import org.eurodat.client.transactionservice.Transaction
import org.eurodat.client.transactionservice.TransactionRequest
import org.eurodat.transactionservice.model.TransactionEntity
import org.eurodat.transactionservice.service.TransactionMapper
import java.time.LocalDateTime
import java.util.UUID

@ApplicationScoped
class PanacheTransactionRepository :
    TransactionRepository,
    PanacheRepositoryBase<TransactionEntity, String> {
    @WithTransaction
    override fun createTransaction(transactionRequest: TransactionRequest): Uni<Transaction> =
        persist(
            TransactionEntity(
                UUID.randomUUID().toString().replace("-", "_"),
                transactionRequest.appId,
                transactionRequest.clientId,
                transactionRequest.consumer,
                transactionRequest.provider,
                LocalDateTime.now(),
                null,
            ),
        ).map { t ->
            TransactionMapper.createDataClassFromEntity(t)
        }

    @WithSession
    override fun findTransaction(id: String): Uni<Transaction?> =
        findById(id).map { entity ->
            entity?.let { TransactionMapper.createDataClassFromEntity(it) }
        }

    @WithTransaction
    override fun endTransaction(transactionId: String): Uni<Transaction> =
        findById(transactionId).map { entity ->
            check(entity.endTime == null) { "Transaction is already terminated" }
            entity.endTime = LocalDateTime.now()
            TransactionMapper.createDataClassFromEntity(entity)
        }

    @WithTransaction
    override fun getAllTransactionIds(): Uni<List<String>> =
        find("endTime is null").list().map {
            it
                .stream()
                .map { entity ->
                    entity.id
                }.toList()
        }

    @WithTransaction
    override fun getParticipantsByTransactionId(transactionId: String): Uni<List<String>> =
        findById(transactionId).map { entity ->
            entity?.let {
                (it.provider + it.consumer).distinct()
            } ?: emptyList()
        }

    @WithTransaction
    override fun getTransactionsByAppId(appId: String): Uni<List<TransactionEntity>> = list("appId", appId)
}
