package org.eurodat.transactionservice.repository

import io.smallrye.mutiny.Uni
import org.eurodat.client.transactionservice.Message

interface MessageRepository {
    fun storeMessages(messages: List<Message>): Uni<List<Message>>

    fun findMessages(
        appId: String,
        participantId: String,
        excludeUntil: Long? = null,
    ): Uni<List<Message>>

    fun deleteMessagesForTransaction(transactionId: String): Uni<Unit>
}
