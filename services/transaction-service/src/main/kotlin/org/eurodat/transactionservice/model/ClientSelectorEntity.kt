package org.eurodat.transactionservice.model

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

@Entity
@Table(name = "client_selector_mapping")
class ClientSelectorEntity(
    @Id
    @Column(name = "client_id")
    val clientId: String,
    @Column(name = "client_selector", unique = true)
    val clientSelector: String,
)
