package org.eurodat.transactionservice.resource

import io.smallrye.mutiny.Uni
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.ClientErrorException
import jakarta.ws.rs.NotFoundException
import org.jboss.resteasy.reactive.RestResponse
import org.jboss.resteasy.reactive.server.ServerExceptionMapper

class ExceptionMappers {
    @ServerExceptionMapper
    fun mapNotFound(e: NotFoundException): Uni<RestResponse<String>> =
        Uni.createFrom().item(
            RestResponse.ResponseBuilder
                .create<String>(RestResponse.Status.NOT_FOUND)
                .entity(e.localizedMessage)
                .build(),
        )

    @ServerExceptionMapper
    fun mapBadRequest(e: BadRequestException): Uni<RestResponse<String>> =
        Uni.createFrom().item(
            RestResponse.ResponseBuilder
                .create<String>(RestResponse.Status.BAD_REQUEST)
                .entity(e.localizedMessage)
                .build(),
        )

    @ServerExceptionMapper
    fun mapClientError(exception: ClientErrorException): Uni<RestResponse<String>> =
        Uni.createFrom().item(RestResponse.status(exception.response.statusInfo, exception.message))
}
