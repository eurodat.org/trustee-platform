package org.eurodat.transactionservice.resource

import io.github.oshai.kotlinlogging.KotlinLogging
import io.quarkus.security.Authenticated
import io.quarkus.security.PermissionsAllowed
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.infrastructure.Infrastructure
import io.smallrye.mutiny.vertx.MutinyHelper
import io.vertx.core.Vertx
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.ClientErrorException
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.PATCH
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.MediaType
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.credential.DatabaseCredentialClient
import org.eurodat.client.database.ClientRoleCreationRequest
import org.eurodat.client.database.TransactionDatabaseClient
import org.eurodat.client.transactionservice.ClientSelectorMapping
import org.eurodat.transactionservice.configuration.KeycloakConfig
import org.eurodat.transactionservice.model.ClientCertificateValues
import org.eurodat.transactionservice.model.ClientDeleteResponse
import org.eurodat.transactionservice.model.ClientRegistrationForm
import org.eurodat.transactionservice.model.Purpose
import org.eurodat.transactionservice.repository.ClientMappingRepository
import org.eurodat.transactionservice.service.LockService
import org.jboss.resteasy.reactive.RestResponse
import org.keycloak.admin.client.Keycloak
import org.keycloak.representations.idm.ClientRepresentation
import org.keycloak.representations.idm.ProtocolMapperRepresentation
import java.util.UUID

@Authenticated
@Path("/clients")
class ClientResource(
    private val keycloak: Keycloak,
    private val keycloakConfig: KeycloakConfig,
    private val clientMappingRepository: ClientMappingRepository,
    private val lockService: LockService,
    @RestClient private val credService: DatabaseCredentialClient,
    @RestClient private val databaseClient: TransactionDatabaseClient,
) {
    private val logger = KotlinLogging.logger {}

    /**
     * Registers a client, i.e., creates an internal and public keycloak client, creates the client mapping,
     * the database roles and registers a vault connection.
     * If the keycloak client already exists, a BadRequestException is thrown.
     * If the client mapping creation, role creation or the vault connection creation fails,
     * all those steps are being reverted, i.e., deleted.
     */
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @PermissionsAllowed("client:write")
    fun registerClient(clientRegistrationForm: ClientRegistrationForm): Uni<String> {
        if (!clientRegistrationForm.clientSelector.lowercase().equals(clientRegistrationForm.clientSelector)) {
            throw BadRequestException("The client selector \"clientSelector\" should be in lower case. ")
        }
        if (clientRegistrationForm.purpose.isEmpty()) {
            throw BadRequestException("The purpose field of the client is not set!")
        }

        val currentContext = Vertx.currentContext()
        return lockService.executeWithLock(getLockIdentifier(clientRegistrationForm.name)) {
            val clientRegistrationUni =
                Uni.createFrom().item {
                    val existingClientRepresentation = clientExists(clientRegistrationForm.name)
                    return@item if (existingClientRepresentation == null) {
                        createClient(clientRegistrationForm)
                    } else {
                        throw ClientErrorException("Client already registered.", RestResponse.StatusCode.CONFLICT)
                    }
                }

            clientRegistrationUni
                .runSubscriptionOn(Infrastructure.getDefaultWorkerPool())
                .emitOn(MutinyHelper.executor(currentContext))
                .call { _ ->
                    // Additional creation steps (client mappings, roles and vault connection)
                    clientMappingRepository
                        .saveClientMapping(
                            ClientSelectorMapping(
                                clientRegistrationForm.name,
                                clientRegistrationForm.clientSelector,
                            ),
                        ).call { _ ->
                            databaseClient.createClientRole(ClientRoleCreationRequest(clientRegistrationForm.name))
                        }.call { _ ->
                            credService.registerClientConnection(clientRegistrationForm.name)
                        }
                }.onFailure { !(it is ClientErrorException && it.response.status == RestResponse.StatusCode.CONFLICT) }
                .call { _ ->
                    // This onFailure only catches the failures of the additional creation steps and not the
                    // keycloak registration itself.
                    deleteClientWithoutLock(clientRegistrationForm.name)
                }
        }
    }

    /**
     * Removes the client from keycloak. True will be returned on success, else false.
     * If the client does not exist, no error is thrown.
     */
    private fun removeKeycloakClient(clientId: String): Boolean {
        return clientExists(clientId)?.let {
            keycloak.realm(keycloakConfig.realm()).clients()[it.id].remove()
            true
        } ?: false
    }

    @PATCH
    @Produces(MediaType.TEXT_PLAIN)
    @PermissionsAllowed("client:write")
    fun updateCertificate(clientCertificateValues: ClientCertificateValues): Uni<String> =
        lockService.executeBlockingWithLock(getLockIdentifier(clientCertificateValues.name)) {
            clientExists(clientCertificateValues.name)
                ?.let { updateClient(it, clientCertificateValues.certificate) }
                ?: throw NotFoundException("Client does not exist.")
        }

    @GET
    @Path("/clientmappings")
    fun getClientSelectorMapping(): Uni<List<ClientSelectorMapping>> = clientMappingRepository.getClientMappings()

    @GET
    @Path("/clientselector")
    fun getAllClientSelectors(): Uni<List<String>> = clientMappingRepository.getAllSelectors()

    @GET
    @Path("/clientselector/{clientId}")
    fun getClientSelector(
        @PathParam("clientId") clientId: String,
    ): Uni<String> = clientMappingRepository.getClientSelector(clientId)

    private fun updateClient(
        clientRepresentation: ClientRepresentation,
        certificate: String,
    ): String {
        clientRepresentation.attributes["jwt.credential.certificate"] = certificate
        keycloak.realm(keycloakConfig.realm()).clients()[clientRepresentation.id].update(clientRepresentation)
        return clientRepresentation.id
    }

    private fun createClient(clientRegistrationForm: ClientRegistrationForm): String {
        val participantMapper = ProtocolMapperRepresentation()
        participantMapper.apply {
            protocol = "openid-connect"
            config =
                mapOf(
                    "access.token.claim" to "true",
                    "access.tokenResponse.claim" to "false",
                    "claim.name" to "participant",
                    "claim.value" to clientRegistrationForm.clientSelector,
                    "id.token.claim" to "false",
                    "jsonType.label" to "string",
                )
            name = "participant"
            protocolMapper = "oidc-hardcoded-claim-mapper"
        }

        val clientRepresentation = ClientRepresentation()
        clientRepresentation.apply {
            id = UUID.randomUUID().toString()
            clientId = clientRegistrationForm.name
            isPublicClient = false
            isDirectAccessGrantsEnabled = true
            isServiceAccountsEnabled = true
            clientAuthenticatorType = "client-jwt"
            attributes =
                mapOf(
                    "token.endpoint.auth.signing.alg" to "RS256",
                    "jwt.credential.certificate" to clientRegistrationForm.certificate,
                )
            defaultClientScopes = clientRegistrationForm.purpose.map { mapPurposeToScope(it) }

            protocolMappers = listOf(participantMapper)
        }
        return createKeycloakClient(clientRepresentation)
    }

    private fun mapPurposeToScope(it: Purpose) =
        when (it) {
            Purpose.APPS -> "app:write"
            Purpose.SAFEDEPOSITS -> "safedeposit:write"
            Purpose.TRANSACTIONS -> "transaction:write"
        }

    private fun createKeycloakClient(clientRepresentation: ClientRepresentation): String {
        val response = keycloak.realm(keycloakConfig.realm()).clients().create(clientRepresentation)
        if (response.status > 300) {
            logger.warn { "Client creation failed with status code ${response.status}: ${response.readEntity(String::class.java)}" }
        }
        return clientRepresentation.id
    }

    private fun clientExists(name: String) = keycloak.realm(keycloakConfig.realm()).clients().findByClientId(name).firstOrNull()

    /**
     * This function deletes the client in keycloak, postgres and vault. If the client does not exist, false will be returned.
     * If it exists, true will be returned. In particular, the function does not fail if the client does not exist.
     */
    @DELETE
    @Path("/{clientId}")
    @Produces(MediaType.TEXT_PLAIN)
    @PermissionsAllowed("client:write")
    fun deleteClient(
        @PathParam("clientId") clientId: String,
    ): Uni<ClientDeleteResponse> =
        lockService.executeWithLock(getLockIdentifier(clientId)) {
            deleteClientWithoutLock(clientId)
        }

    /**
     * This function deletes the client in keycloak, postgres and vault. If the client does not exist, false will be returned.
     * If it exists, true will be returned. In particular, the function does not fail if the client does not exist.
     * This function is not acquiring a lock, so only use it inside a function that acquires a lock.
     */
    fun deleteClientWithoutLock(clientId: String): Uni<ClientDeleteResponse> {
        return Uni
            .createFrom()
            .item {
                // delete client in keycloak (both public and not public roles)
                return@item removeKeycloakClient(clientId)
            }.runSubscriptionOn(Infrastructure.getDefaultWorkerPool())
            .emitOn(MutinyHelper.executor(Vertx.currentContext()))
            .chain { keycloakClientDelete ->
                // delete the clientMapping of the User
                clientMappingRepository
                    .deleteClientMapping(clientId)
                    .chain { clientMapperDelete ->
                        Uni.createFrom().item { ClientDeleteResponse(keycloakClientDelete, clientMapperDelete) }
                    }.call { _ ->
                        // delete client role
                        databaseClient.deleteClientRole(clientId)
                    }.call { _ ->
                        // delete vault-connection and role
                        credService.deleteUserConnection(clientId)
                    }
            }
    }
}

private fun getLockIdentifier(clientId: String): String = "user_management_$clientId"
