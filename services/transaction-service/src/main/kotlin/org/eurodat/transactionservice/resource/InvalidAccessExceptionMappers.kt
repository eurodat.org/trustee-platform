package org.eurodat.transactionservice.resource

import io.github.oshai.kotlinlogging.KotlinLogging
import io.quarkus.security.AuthenticationFailedException
import io.quarkus.security.UnauthorizedException
import io.smallrye.mutiny.Uni
import org.jboss.resteasy.reactive.ClientWebApplicationException
import org.jboss.resteasy.reactive.RestResponse
import org.jboss.resteasy.reactive.server.ServerExceptionMapper

private val logger = KotlinLogging.logger {}

class InvalidAccessExceptionMappers {
    private val unauthorizedLogMessage = "Unauthorized asset request, will be declined"

    @ServerExceptionMapper
    fun mapUnauthorized(e: UnauthorizedException): Uni<RestResponse<String>> = toNotFound(e)

    @ServerExceptionMapper
    fun mapAuthenticationFailed(e: AuthenticationFailedException): Uni<RestResponse<String>> = toNotFound(e)

    @ServerExceptionMapper
    fun mapAuthenticationFailed(e: ClientWebApplicationException): Uni<RestResponse<String>> {
        if (e.response.status == 409) {
            logger.error(e) { e.localizedMessage }
            return Uni.createFrom().item(RestResponse.status(409))
        }
        return toNotFound(e)
    }

    private fun toNotFound(e: Exception): Uni<RestResponse<String>> {
        logger.error(e) { e.localizedMessage }
        return Uni.createFrom().item(RestResponse.notFound())
    }
}
