package org.eurodat.transactionservice.repository

import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import org.eurodat.client.transactionservice.TransactionRequest
import org.eurodat.transactionservice.model.TransactionEntity
import org.eurodat.transactionservice.resource.PostgresTestContainer
import org.hibernate.reactive.mutiny.Mutiny
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

@QuarkusTest
@QuarkusTestResource(PostgresTestContainer::class)
class PanacheTransactionRepositoryTest {
    @Inject
    private lateinit var sessionFactory: Mutiny.SessionFactory

    @Inject
    private lateinit var transactionRepository: PanacheTransactionRepository

    private val testTransactionRequest =
        TransactionRequest("test_client", "appId", listOf("consumer1", "consumer2"), listOf("provider1", "provider2"))

    private fun setup(): Uni<Long> =
        sessionFactory.withTransaction { session ->
            transactionRepository.deleteAll().call { _ ->
                session.persistAll(
                    TransactionEntity(
                        "test_id",
                        "test_app_id",
                        "test_client_id",
                        listOf("consumer1", "consumer2"),
                        listOf("provider1", "provider2"),
                        LocalDateTime.now(),
                        null,
                    ),
                    TransactionEntity(
                        "test_id_2",
                        "test_app_id",
                        "test_client_id",
                        listOf("consumer1", "consumer2"),
                        listOf("provider1", "provider2"),
                        LocalDateTime.now(),
                        null,
                    ),
                    TransactionEntity(
                        "test_id_3",
                        "test_app_id",
                        "test_client_id",
                        listOf("consumer1", "consumer2"),
                        listOf("provider1", "provider2"),
                        LocalDateTime.now(),
                        LocalDateTime.now(),
                    ),
                )
            }
        }

    @Test
    @RunOnVertxContext
    fun testCreateTransaction(asserter: UniAsserter) {
        asserter.assertThat(
            {
                setup().chain { _ ->
                    sessionFactory.withTransaction { _ -> transactionRepository.createTransaction(testTransactionRequest) }
                }
            },
            { transaction ->
                Assertions.assertEquals(testTransactionRequest.appId, transaction.appId)
                Assertions.assertEquals(testTransactionRequest.clientId, transaction.clientId)
                Assertions.assertEquals(testTransactionRequest.consumer, transaction.consumer)
                Assertions.assertEquals(testTransactionRequest.provider, transaction.provider)
                Assertions.assertNotNull(transaction.id)
                Assertions.assertNotNull(transaction.startTime)
                Assertions.assertNull(transaction.endTime)
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testFindTransaction(asserter: UniAsserter) {
        asserter.assertThat(
            {
                setup().chain { _ ->
                    sessionFactory.withSession { _ ->
                        transactionRepository.findTransaction("test_id")
                    }
                }
            },
            { transaction ->
                Assertions.assertEquals("test_app_id", transaction!!.appId)
                Assertions.assertEquals("test_client_id", transaction.clientId)
                Assertions.assertEquals("test_id", transaction.id)
                Assertions.assertNotNull(transaction.startTime)
                Assertions.assertNull(transaction.endTime)
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testFindNonExistentTransaction(asserter: UniAsserter) {
        asserter.assertNull {
            setup().chain { _ ->
                sessionFactory.withSession { _ ->
                    transactionRepository.findTransaction("non_existent_test_id")
                }
            }
        }
    }

    @Test
    @RunOnVertxContext
    fun testEndTransaction(asserter: UniAsserter) {
        asserter.assertThat(
            {
                setup().chain { _ ->
                    sessionFactory.withTransaction { _ ->
                        transactionRepository.endTransaction("test_id_2")
                    }
                }
            },
            { transaction ->
                Assertions.assertFalse(transaction.endTime!!.isBefore(transaction.startTime))
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testEndAlreadyTerminatedTransaction(asserter: UniAsserter) {
        asserter.assertFailedWith(
            {
                setup().chain { _ ->
                    sessionFactory.withTransaction { _ ->
                        transactionRepository.endTransaction("test_id_3")
                    }
                }
            },
            IllegalStateException::class.java,
        )
    }

    @Test
    @RunOnVertxContext
    fun testGetAllTransactionIds(asserter: UniAsserter) {
        asserter.assertThat({
            sessionFactory.withSession {
                setup().flatMap { transactionRepository.getAllTransactionIds() }
            }
        }, {
            Assertions.assertTrue(it.size == 2 && it[0] == "test_id" && it[1] == "test_id_2")
        })
    }

    @Test
    @RunOnVertxContext
    fun testGetParticipantsByTransactionId(asserter: UniAsserter) {
        asserter.assertThat(
            {
                setup().chain { _ ->
                    sessionFactory.withSession { _ ->
                        transactionRepository.getParticipantsByTransactionId("test_id")
                    }
                }
            },
            { participants ->
                Assertions.assertTrue(participants.contains("consumer1"))
                Assertions.assertTrue(participants.contains("consumer2"))
                Assertions.assertTrue(participants.contains("provider1"))
                Assertions.assertTrue(participants.contains("provider2"))
                Assertions.assertEquals(4, participants.size)
            },
        )
    }
}
