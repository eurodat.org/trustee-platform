package org.eurodat.transactionservice.messaging

import io.quarkus.test.InjectMock
import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.common.http.TestHTTPEndpoint
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.security.TestSecurity
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.smallrye.mutiny.Uni
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eurodat.client.transactionservice.Transaction
import org.eurodat.transactionservice.repository.ClientMappingRepository
import org.eurodat.transactionservice.repository.TransactionRepository
import org.eurodat.transactionservice.resource.MessagingResource
import org.eurodat.transactionservice.resource.PostgresTestContainer
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import java.time.LocalDateTime
import java.util.Base64

const val MOCK_SELECTOR = "selector1"
const val MOCK_APP = "mockApp"
const val INVALID_SELECTOR = "invalidSelector"

@QuarkusTest
@QuarkusTestResource(PostgresTestContainer::class)
@TestHTTPEndpoint(MessagingResource::class)
class MessagingResourceTest {
    @InjectMock
    lateinit var jwt: JsonWebToken

    @InjectMock
    lateinit var clientMapper: ClientMappingRepository

    @InjectMock
    lateinit var transactionRepository: TransactionRepository

    private val activeTransaction =
        Transaction(
            id = "t1",
            appId = "mockApp",
            consumer = listOf(),
            provider = listOf(),
            endTime = null,
        )

    @Test
    @TestSecurity(authorizationEnabled = false)
    fun testGetMessages() {
        Mockito.`when`(jwt.getClaim<String>("client_id")).thenReturn("mockParticipantId")
        Mockito
            .`when`(clientMapper.getClientSelector("mockParticipantId"))
            .thenReturn(Uni.createFrom().item(MOCK_SELECTOR))
        Mockito.`when`(transactionRepository.findTransaction("t1")).thenReturn(Uni.createFrom().item(activeTransaction))

        RestAssured
            .given()
            .contentType(ContentType.JSON)
            .pathParam("appId", MOCK_APP)
            .`when`()
            .get("apps/{appId}/messages")
            .then()
            .statusCode(200)
            .assertThat()
            .contentType(ContentType.JSON)
            .body("size()", Matchers.equalTo(1))
    }

    @Test
    @TestSecurity(authorizationEnabled = false)
    fun testPostMessage() {
        val authorizationHeader =
            generateAuthorizationHeader(
                appId = "mockApp",
                transactionId = "t1",
                workflowId = "w1",
                allowedSelectors = listOf(MOCK_SELECTOR, "selector2"),
            )

        val requestBody = """
            {
                "message": "Test Message",
                "selectors": ["$MOCK_SELECTOR", "selector2"]
            }
        """
        Mockito.`when`(transactionRepository.findTransaction("t1")).thenReturn(Uni.createFrom().item(activeTransaction))

        RestAssured
            .given()
            .header("Authorization", authorizationHeader)
            .contentType(ContentType.JSON)
            .body(requestBody)
            .`when`()
            .post("/messages")
            .then()
            .statusCode(200)
            .assertThat()
            .contentType(ContentType.JSON)
            .body("size()", Matchers.equalTo(2))
            .body("[0].correlationId", Matchers.notNullValue())
            .body("[0].selector", Matchers.equalTo(MOCK_SELECTOR))
            .body("[0].label", Matchers.equalTo("Test Message"))
            .body("[1].selector", Matchers.equalTo("selector2"))
    }

    @Test
    @TestSecurity(authorizationEnabled = false)
    fun testSelectorNotAllowed() {
        val authorizationHeader =
            generateAuthorizationHeader(
                appId = "mockApp",
                transactionId = "t1",
                workflowId = "w1",
                allowedSelectors = listOf("selector1"),
            )
        val requestBody = """
                {
                    "message": "Test Message",
                    "selectors": ["$INVALID_SELECTOR"]
                }
            """

        Mockito.`when`(transactionRepository.findTransaction("t1")).thenReturn(Uni.createFrom().item(activeTransaction))

        RestAssured
            .given()
            .header("Authorization", authorizationHeader)
            .contentType(ContentType.JSON)
            .body(requestBody)
            .`when`()
            .post("/messages")
            .then()
            .statusCode(400)
    }

    @Test
    @TestSecurity(authorizationEnabled = false)
    fun testPostMessageWithEndedTransaction() {
        val transactionId = "t1"
        val authorizationHeader =
            generateAuthorizationHeader(
                appId = "mockApp",
                transactionId = transactionId,
                workflowId = "w1",
                allowedSelectors = listOf(MOCK_SELECTOR, "selector2"),
            )

        val requestBody = """
            {
                "message": "Test Message",
                "selectors": ["$MOCK_SELECTOR"]
            }
        """

        Mockito.`when`(transactionRepository.findTransaction(transactionId))
            .thenReturn(
                Uni.createFrom().item(
                    Transaction(
                        id = transactionId,
                        appId = "mockApp",
                        consumer = listOf(),
                        provider = listOf(),
                        endTime = LocalDateTime.now(),
                    ),
                ),
            )

        RestAssured
            .given()
            .header("Authorization", authorizationHeader)
            .contentType(ContentType.JSON)
            .body(requestBody)
            .`when`()
            .post("/messages")
            .then()
            .statusCode(400)
    }

    private fun generateAuthorizationHeader(
        appId: String,
        transactionId: String,
        workflowId: String,
        allowedSelectors: List<String>,
    ): String {
        val claimsJson =
            """
            {
                "app_id": "$appId",
                "transaction_id": "$transactionId",
                "workflow_id": "$workflowId",
                "allowed_selectors": ${allowedSelectors.map { "\"$it\"" }}
            }
            """.trimIndent()
        val base64Token = Base64.getEncoder().encodeToString(claimsJson.toByteArray())
        return "Bearer $base64Token"
    }
}
