package org.eurodat.transactionservice.messaging

import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import org.eurodat.client.transactionservice.Message
import org.eurodat.transactionservice.repository.PanacheMessageRepository
import org.eurodat.transactionservice.resource.PostgresTestContainer
import org.hibernate.reactive.mutiny.Mutiny
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

private const val PARTICIPANT = "participant"
private const val APP_ID = "appId"
private const val OTHER_APP_ID = "otherAppId"
private const val TRANSACTION_ID = "transactionId"
private const val OTHER_TRANSACTION_ID = "otherTransactionId"

@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@QuarkusTestResource(PostgresTestContainer::class)
class PanacheMessageRepositoryTest {
    @Inject
    lateinit var messageRepository: PanacheMessageRepository

    @Inject
    private lateinit var sessionFactory: Mutiny.SessionFactory

    @BeforeEach
    fun setup() {
        sessionFactory
            .withTransaction { _ ->
                messageRepository.deleteAll()
            }.await()
            .indefinitely()
    }

    @Test
    @RunOnVertxContext
    fun `returns empty list if no messages`(asserter: UniAsserter) {
        asserter.assertEquals(
            {
                messageRepository.findMessages(APP_ID, PARTICIPANT).map { it.size }
            },
            0,
        )
    }

    @Test
    @RunOnVertxContext
    fun `stores a message`(asserter: UniAsserter) {
        asserter.assertThat(
            { messageRepository.storeMessages(listOf(Message(null, APP_ID, TRANSACTION_ID, "workflow1", "failed", PARTICIPANT, null))) },
            {
                Assertions.assertNotNull(it[0].correlationId)
                Assertions.assertEquals(APP_ID, it[0].appId)
                Assertions.assertEquals(TRANSACTION_ID, it[0].transactionId)
                Assertions.assertEquals("workflow1", it[0].workflowId)
                Assertions.assertEquals("failed", it[0].label)
                Assertions.assertNotNull(it[0].createdAt)
            },
        )
    }

    @Nested
    inner class GivenAMessage {
        lateinit var message: Message

        @BeforeEach
        fun `store a message`() {
            sessionFactory
                .withTransaction { _ ->
                    messageRepository
                        .storeMessages(listOf(Message(null, APP_ID, TRANSACTION_ID, "workflow1", "failed", PARTICIPANT)))
                        .onItem()
                        .invoke { m -> message = m[0] }
                }.await()
                .indefinitely()
        }

        @Test
        @RunOnVertxContext
        fun `retrieves a message for app and participant`(asserter: UniAsserter) {
            asserter.assertThat(
                { messageRepository.findMessages(APP_ID, PARTICIPANT).map { it[0] } },
                { Assertions.assertEquals(message, it) },
            )
        }

        @Test
        @RunOnVertxContext
        fun `finds no message for app and different participant`(asserter: UniAsserter) {
            asserter.assertEquals(
                { messageRepository.findMessages(APP_ID, "differentParticipant").map { it.size } },
                0,
            )
        }

        @Test
        @RunOnVertxContext
        fun `finds no message for different app and participant`(asserter: UniAsserter) {
            asserter.assertEquals(
                { messageRepository.findMessages("differentAppId", PARTICIPANT).map { it.size } },
                0,
            )
        }

        @Test
        @RunOnVertxContext
        fun `stores multiple messages`(asserter: UniAsserter) {
            val messagesToStore =
                listOf(
                    Message(null, APP_ID, TRANSACTION_ID, "workflow1", "started", PARTICIPANT, null),
                    Message(null, APP_ID, TRANSACTION_ID, "workflow2", "in-progress", PARTICIPANT, null),
                    Message(null, APP_ID, TRANSACTION_ID, "workflow3", "completed", PARTICIPANT, null),
                )

            asserter.assertThat(
                { messageRepository.storeMessages(messagesToStore) },
                { storedMessages ->
                    Assertions.assertEquals(3, storedMessages.size)
                    Assertions.assertEquals("workflow1", storedMessages[0].workflowId)
                    Assertions.assertEquals("workflow2", storedMessages[1].workflowId)
                    Assertions.assertEquals("workflow3", storedMessages[2].workflowId)
                    Assertions.assertNotNull(storedMessages[0].correlationId)
                    Assertions.assertNotNull(storedMessages[1].correlationId)
                    Assertions.assertNotNull(storedMessages[2].correlationId)
                },
            )
        }

        @Nested
        inner class GivenMoreMessagesForOtherApp {
            lateinit var firstMessage: Message
            lateinit var secondMessage: Message

            @BeforeEach
            fun `store messages for another app`() {
                sessionFactory
                    .withTransaction { _ ->
                        messageRepository
                            .storeMessages(
                                listOf(Message(null, OTHER_APP_ID, TRANSACTION_ID, "workflow1", "started", PARTICIPANT)),
                            ).invoke { m -> firstMessage = m[0] }
                            .chain { _ ->
                                messageRepository.storeMessages(
                                    listOf(Message(null, OTHER_APP_ID, TRANSACTION_ID, "workflow1", "completed", PARTICIPANT)),
                                )
                            }.invoke { m -> secondMessage = m[0] }
                    }.await()
                    .indefinitely()
            }

            @Test
            @RunOnVertxContext
            fun `finds both messages for other app and participant`(asserter: UniAsserter) {
                asserter.assertEquals(
                    { messageRepository.findMessages(OTHER_APP_ID, PARTICIPANT) },
                    listOf(firstMessage, secondMessage),
                )
            }

            @Test
            @RunOnVertxContext
            fun `excludes first message`(asserter: UniAsserter) {
                asserter.assertEquals(
                    { messageRepository.findMessages(OTHER_APP_ID, PARTICIPANT, firstMessage.correlationId!!) },
                    listOf(secondMessage),
                )
            }

            @Test
            @RunOnVertxContext
            fun `finds one message for first app and participant`(asserter: UniAsserter) {
                asserter.assertEquals(
                    { messageRepository.findMessages(APP_ID, PARTICIPANT) },
                    listOf(message),
                )
            }

            @Test
            @RunOnVertxContext
            fun `sorts messages ascending by correlation id`(asserter: UniAsserter) {
                asserter.assertEquals(
                    {
                        messageRepository
                            .findMessages(OTHER_APP_ID, PARTICIPANT)
                            .map { list -> list.map { it.correlationId } }
                    },
                    listOf(firstMessage.correlationId, secondMessage.correlationId),
                )
            }

            @Test
            @RunOnVertxContext
            fun `deletes messages for transaction id`(asserter: UniAsserter) {
                asserter.assertTrue {
                    lateinit var otherTxMessage: Message
                    sessionFactory
                        .withTransaction { _ ->
                            messageRepository
                                .storeMessages(
                                    listOf(
                                        Message(
                                            null,
                                            OTHER_APP_ID,
                                            OTHER_TRANSACTION_ID,
                                            "workflow1",
                                            "failed",
                                            PARTICIPANT,
                                        ),
                                    ),
                                ).invoke { m -> otherTxMessage = m[0] }
                        }.call { _ ->
                            sessionFactory.withTransaction { _ -> messageRepository.deleteMessagesForTransaction(TRANSACTION_ID) }
                        }.chain { _ ->
                            sessionFactory.withSession { messageRepository.findMessages(OTHER_APP_ID, PARTICIPANT) }
                        }.map { it == listOf(otherTxMessage) }
                }
            }
        }
    }
}
