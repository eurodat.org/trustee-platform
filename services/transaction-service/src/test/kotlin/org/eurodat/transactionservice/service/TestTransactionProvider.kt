package org.eurodat.transactionservice.service

import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.inject.Alternative
import jakarta.inject.Inject
import org.hibernate.reactive.mutiny.Mutiny
import org.hibernate.reactive.mutiny.Mutiny.SessionFactory
import java.util.function.Function
import javax.annotation.Priority

@ApplicationScoped
@Alternative
@Priority(1)
class TestTransactionProvider : TransactionProvider {
    @Inject
    lateinit var sessionFactory: SessionFactory

    override fun <T> withTransaction(work: Function<Mutiny.Session, Uni<T>>): Uni<T> =
        sessionFactory
            .openSession()
            .chain { session ->
                Uni
                    .createFrom()
                    .voidItem()
                    .chain { _ ->
                        session.withTransaction { _ ->
                            work.apply(session)
                        }
                    }.eventually {
                        session.close()
                    }
            }
}
