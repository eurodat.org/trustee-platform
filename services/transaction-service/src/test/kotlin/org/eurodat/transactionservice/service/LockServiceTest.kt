package org.eurodat.transactionservice.service

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.infrastructure.Infrastructure
import io.smallrye.mutiny.vertx.MutinyHelper
import io.vertx.core.Vertx
import jakarta.inject.Inject
import org.eurodat.transactionservice.model.LockEntity
import org.hibernate.reactive.mutiny.Mutiny.SessionFactory
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.OffsetDateTime
import java.time.temporal.ChronoUnit

@QuarkusTest
class LockServiceTest {
    @Inject
    lateinit var sessionFactory: SessionFactory

    @Inject
    lateinit var lockService: LockService

    private fun setup(): Uni<Int> =
        sessionFactory.withTransaction { session ->
            session
                .createNativeQuery<Int>("TRUNCATE TABLE LOCK")
                .executeUpdate()
                .call { _ -> session.persist(LockEntity("foo")) }
        }

    @Test
    @RunOnVertxContext
    fun executeWithLock(asserter: UniAsserter) {
        val finishTimes = HashMap<String, OffsetDateTime>()
        asserter.assertThat(
            {
                val context = Vertx.currentContext()
                val slowUni =
                    lockService.executeWithLock("foo") {
                        Uni
                            .createFrom()
                            .item {
                                Thread.sleep(1000)
                                finishTimes.put("slow", OffsetDateTime.now())
                            }.runSubscriptionOn(Infrastructure.getDefaultWorkerPool())
                            .emitOn(MutinyHelper.executor(context))
                    }
                val fastUni =
                    Uni
                        .createFrom()
                        .item {
                            // Delay lock acquisition such that this uni is always the second one to get the lock
                            Thread.sleep(100)
                        }.runSubscriptionOn(Infrastructure.getDefaultWorkerPool())
                        .emitOn(MutinyHelper.executor(context))
                        .chain { _ ->
                            lockService.executeWithLock("foo") {
                                Uni.createFrom().item { finishTimes.put("fast", OffsetDateTime.now()) }
                            }
                        }
                val uniWithoutLock = Uni.createFrom().item { finishTimes.put("noLock", OffsetDateTime.now()) }
                val uniDifferentLockId =
                    lockService.executeWithLock("bar") {
                        Uni.createFrom().item { finishTimes.put("differentLockId", OffsetDateTime.now()) }
                    }
                finishTimes["start"] = OffsetDateTime.now()
                setup().chain { _ ->
                    Uni
                        .combine()
                        .all()
                        .unis(slowUni, fastUni, uniWithoutLock, uniDifferentLockId)
                        .asTuple()
                }
            },
            {
                Assertions.assertTrue(finishTimes.getValue("fast").isAfter(finishTimes.getValue("slow")))
                Assertions.assertTrue(finishTimes.getValue("start").until(finishTimes.getValue("slow"), ChronoUnit.MILLIS) > 1000)
                Assertions.assertTrue(finishTimes.getValue("noLock").isBefore(finishTimes.getValue("slow")))
                Assertions.assertTrue(finishTimes.getValue("differentLockId").isBefore(finishTimes.getValue("slow")))
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun executeBlockingWithLock(asserter: UniAsserter) {
        asserter.assertTrue {
            setup().chain { _ ->
                lockService.executeBlockingWithLock("foo") {
                    Thread.sleep(100)
                    true
                }
            }
        }
    }
}
