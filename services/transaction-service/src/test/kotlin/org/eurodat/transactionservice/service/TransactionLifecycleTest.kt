package org.eurodat.transactionservice.service

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.MockitoConfig
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.CompositeException
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import jakarta.ws.rs.NotFoundException
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.AppServiceExternalClient
import org.eurodat.client.appservice.model.AppResponse
import org.eurodat.client.database.TransactionDatabaseOidcClientFilterClient
import org.eurodat.client.transactionservice.Transaction
import org.eurodat.transactionservice.model.TransactionEntity
import org.eurodat.transactionservice.repository.MessageRepository
import org.eurodat.transactionservice.repository.TransactionRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito.anyString
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.kotlin.any
import java.time.LocalDateTime

@QuarkusTest
class TransactionLifecycleTest {
    @Inject
    private lateinit var transactionLifecycle: TransactionLifecycle

    @InjectMock
    private lateinit var transactionRepository: TransactionRepository

    @InjectMock
    @MockitoConfig(convertScopes = true)
    @RestClient
    private lateinit var appServiceClient: AppServiceExternalClient

    @InjectMock
    @MockitoConfig(convertScopes = true)
    @RestClient
    private lateinit var transactionDatabaseClient: TransactionDatabaseOidcClientFilterClient

    @InjectMock
    private lateinit var messageRepository: MessageRepository

    @Test
    @RunOnVertxContext
    fun `terminateOverdueTransactions check full pipeline with all conditions`(asserter: UniAsserter) {
        `when`(appServiceClient.getAllAppIds()).thenReturn(Uni.createFrom().item(listOf("app1", "app2")))
        `when`(appServiceClient.findApp("app1")).thenReturn(Uni.createFrom().item(AppResponse(id = "app1", transactionTimeoutInDays = 30)))
        `when`(appServiceClient.findApp("app2")).thenReturn(Uni.createFrom().item(AppResponse(id = "app2", transactionTimeoutInDays = 30)))
        val overdueTransaction1 =
            TransactionEntity(
                id = "9",
                appId = "app2",
                clientId = "client",
                startTime = LocalDateTime.now().minusDays(60),
                endTime = null,
            )
        `when`(transactionRepository.getTransactionsByAppId("app1"))
            .thenReturn(
                Uni.createFrom().item(
                    listOf(
                        // Just now -> No deletion
                        TransactionEntity(id = "1", appId = "app1", clientId = "client", startTime = LocalDateTime.now(), endTime = null),
                        // Just below deletion window -> No deletion
                        TransactionEntity(
                            id = "5",
                            appId = "app1",
                            clientId = "client",
                            startTime = LocalDateTime.now().minusDays(29),
                            endTime = null,
                        ),
                        // startTime is above deletion window, but endTime has value -> No deletion
                        TransactionEntity(
                            id = "6",
                            appId = "app1",
                            clientId = "client",
                            startTime = LocalDateTime.now().minusDays(60),
                            endTime = LocalDateTime.now(),
                        ),
                        // startTime is way above deletion window -> Deletion
                        overdueTransaction1,
                    ),
                ),
            )
        val overdueTransaction2 =
            TransactionEntity(
                id = "3",
                appId = "app2",
                clientId = "client",
                startTime = LocalDateTime.now().minusDays(31),
                endTime = null,
            )
        `when`(transactionRepository.getTransactionsByAppId("app2"))
            .thenReturn(
                Uni.createFrom().item(
                    listOf(
                        // Just now -> No deletion
                        TransactionEntity(id = "2", appId = "app2", clientId = "client", startTime = LocalDateTime.now(), endTime = null),
                        // Just above deletion window -> Deletion
                        overdueTransaction2,
                    ),
                ),
            )
        `when`(transactionRepository.endTransaction(overdueTransaction1.id)).thenReturn(
            Uni.createFrom().item(Transaction(id = overdueTransaction1.id)),
        )
        `when`(transactionDatabaseClient.deleteTransactionDatabase(any())).thenReturn(
            Uni.createFrom().item(Unit),
        )
        `when`(messageRepository.deleteMessagesForTransaction(anyString())).thenReturn(
            Uni.createFrom().item(Unit),
        )
        `when`(transactionRepository.endTransaction(overdueTransaction2.id)).thenReturn(
            Uni.createFrom().item(Transaction(id = overdueTransaction2.id)),
        )

        asserter.assertThat(
            {
                transactionLifecycle.terminateOverdueTransactions()
            },
            {
                verify(transactionDatabaseClient, times(2)).deleteTransactionDatabase(any())
                verify(messageRepository, times(2)).deleteMessagesForTransaction(anyString())
                verify(transactionRepository).endTransaction(overdueTransaction1.id)
                verify(transactionRepository).endTransaction(overdueTransaction2.id)
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun `terminateOverdueTransactions GIVEN two apps available with no overdue transaction THEN endTransaction is never called`(
        asserter: UniAsserter,
    ) {
        `when`(appServiceClient.getAllAppIds()).thenReturn(Uni.createFrom().item(listOf("app1", "app2")))
        `when`(appServiceClient.findApp("app1")).thenReturn(Uni.createFrom().item(AppResponse(id = "app1", transactionTimeoutInDays = 30)))
        `when`(appServiceClient.findApp("app2")).thenReturn(Uni.createFrom().item(AppResponse(id = "app2", transactionTimeoutInDays = 30)))
        `when`(transactionRepository.getTransactionsByAppId("app1"))
            .thenReturn(
                Uni.createFrom().item(
                    listOf(
                        TransactionEntity(id = "1", appId = "app1", clientId = "client", startTime = LocalDateTime.now(), endTime = null),
                    ),
                ),
            )
        `when`(transactionRepository.getTransactionsByAppId("app2"))
            .thenReturn(
                Uni.createFrom().item(
                    listOf(
                        TransactionEntity(id = "2", appId = "app2", clientId = "client", startTime = LocalDateTime.now(), endTime = null),
                        TransactionEntity(
                            id = "3",
                            appId = "app2",
                            clientId = "client",
                            startTime = LocalDateTime.now().minusDays(20),
                            endTime = null,
                        ),
                    ),
                ),
            )

        asserter.assertThat(
            {
                transactionLifecycle.terminateOverdueTransactions()
            },
            {
                verify(transactionDatabaseClient, times(0)).deleteTransactionDatabase(any())
                verify(messageRepository, times(0)).deleteMessagesForTransaction(anyString())
                verify(transactionRepository, times(0)).endTransaction(anyString())
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun `terminateOverdueTransactions GIVEN empty transactions THEN return Unit`(asserter: UniAsserter) {
        `when`(appServiceClient.getAllAppIds()).thenReturn(Uni.createFrom().item(listOf("app1")))
        `when`(appServiceClient.findApp("app1")).thenReturn(Uni.createFrom().item(AppResponse(id = "app1", transactionTimeoutInDays = 30)))
        `when`(transactionRepository.getTransactionsByAppId("app1"))
            .thenReturn(
                Uni.createFrom().item(
                    emptyList(),
                ),
            )

        asserter.assertEquals(
            {
                transactionLifecycle.terminateOverdueTransactions()
            },
            Unit,
        )
    }

    @Test
    @RunOnVertxContext
    fun `terminateOverdueTransactions GIVEN endTransaction throws Exception THEN error propagates`(asserter: UniAsserter) {
        val overdueTransaction =
            TransactionEntity(
                id = "3",
                appId = "app2",
                clientId = "client",
                startTime = LocalDateTime.now().minusDays(60),
                endTime = null,
            )
        `when`(appServiceClient.getAllAppIds()).thenReturn(Uni.createFrom().item(listOf("app1", "app2")))
        `when`(appServiceClient.findApp("app1")).thenReturn(Uni.createFrom().item(AppResponse(id = "app1", transactionTimeoutInDays = 30)))
        `when`(appServiceClient.findApp("app2")).thenReturn(Uni.createFrom().item(AppResponse(id = "app2", transactionTimeoutInDays = 30)))
        `when`(transactionRepository.getTransactionsByAppId("app1"))
            .thenReturn(
                Uni.createFrom().item(
                    listOf(
                        TransactionEntity(id = "1", appId = "app1", clientId = "client", startTime = LocalDateTime.now(), endTime = null),
                    ),
                ),
            )
        `when`(transactionRepository.getTransactionsByAppId("app2"))
            .thenReturn(
                Uni.createFrom().item(
                    listOf(
                        TransactionEntity(id = "2", appId = "app2", clientId = "client", startTime = LocalDateTime.now(), endTime = null),
                        overdueTransaction,
                    ),
                ),
            )
        `when`(transactionDatabaseClient.deleteTransactionDatabase(any())).thenReturn(
            Uni.createFrom().item(Unit),
        )
        `when`(messageRepository.deleteMessagesForTransaction(anyString())).thenReturn(
            Uni.createFrom().item(Unit),
        )
        `when`(transactionRepository.endTransaction(overdueTransaction.id)).thenThrow(NotFoundException("transaction not found"))

        asserter.assertFailedWith(
            {
                transactionLifecycle.terminateOverdueTransactions()
            },
            { exc ->
                Assertions.assertEquals(CompositeException::class.java, exc.javaClass)
                if (exc is CompositeException) {
                    val innerException = exc.cause!!.cause!!
                    Assertions.assertEquals(NotFoundException::class.java, innerException.javaClass)
                    Assertions.assertEquals("transaction not found", innerException.message)
                }
                verify(transactionDatabaseClient).deleteTransactionDatabase(any())
                verify(messageRepository).deleteMessagesForTransaction(anyString())
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun `terminateOverdueTransactions GIVEN transactionDatabaseClient throws Exception THEN error propagates`(asserter: UniAsserter) {
        val overdueTransaction =
            TransactionEntity(
                id = "3",
                appId = "app2",
                clientId = "client",
                startTime = LocalDateTime.now().minusDays(60),
                endTime = null,
            )
        `when`(appServiceClient.getAllAppIds()).thenReturn(Uni.createFrom().item(listOf("app1", "app2")))
        `when`(appServiceClient.findApp("app1")).thenReturn(Uni.createFrom().item(AppResponse(id = "app1")))
        `when`(appServiceClient.findApp("app2")).thenReturn(Uni.createFrom().item(AppResponse(id = "app2")))
        `when`(transactionRepository.getTransactionsByAppId("app1"))
            .thenReturn(
                Uni.createFrom().item(
                    listOf(
                        TransactionEntity(id = "1", appId = "app1", clientId = "client", startTime = LocalDateTime.now(), endTime = null),
                    ),
                ),
            )
        `when`(transactionRepository.getTransactionsByAppId("app2"))
            .thenReturn(
                Uni.createFrom().item(
                    listOf(
                        TransactionEntity(id = "2", appId = "app2", clientId = "client", startTime = LocalDateTime.now(), endTime = null),
                        overdueTransaction,
                    ),
                ),
            )
        `when`(transactionDatabaseClient.deleteTransactionDatabase(any())).thenThrow(
            NotFoundException("transaction not found"),
        )

        asserter.assertFailedWith(
            {
                transactionLifecycle.terminateOverdueTransactions()
            },
            { exc ->
                Assertions.assertEquals(CompositeException::class.java, exc.javaClass)
                if (exc is CompositeException) {
                    val innerException = exc.cause!!
                    Assertions.assertEquals(NotFoundException::class.java, innerException.javaClass)
                    Assertions.assertEquals("transaction not found", innerException.message)
                }
                verify(messageRepository, times(0)).deleteMessagesForTransaction(anyString())
                verify(transactionRepository, times(0)).endTransaction(anyString())
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun `terminateOverdueTransactions GIVEN getTransactionByAppId returns empty list THEN IllegalArgumentException is thrown`(
        asserter: UniAsserter,
    ) {
        `when`(appServiceClient.getAllAppIds()).thenReturn(Uni.createFrom().item(listOf("app1")))
        `when`(appServiceClient.findApp("app1")).thenReturn(Uni.createFrom().item(AppResponse(id = "app1", transactionTimeoutInDays = 30)))
        `when`(transactionRepository.getTransactionsByAppId("app1"))
            .thenReturn(
                Uni.createFrom().item(
                    emptyList(),
                ),
            )

        asserter.assertEquals(
            {
                transactionLifecycle.terminateOverdueTransactions()
            },
            Unit,
        )
    }

    @Test
    @RunOnVertxContext
    fun `terminateOverdueTransactions GIVEN findApp throws exception THEN error propagates`(asserter: UniAsserter) {
        `when`(appServiceClient.getAllAppIds()).thenReturn(Uni.createFrom().item(listOf("app1")))
        `when`(appServiceClient.findApp("app1")).thenThrow(NotFoundException("Did not find App"))

        asserter.assertFailedWith(
            {
                transactionLifecycle.terminateOverdueTransactions()
            },
            { exc ->
                Assertions.assertEquals(NotFoundException::class.java, exc.javaClass)
                Assertions.assertEquals("Did not find App", exc.message)
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun `terminateOverdueTransactions GIVEN getAllAppIds returns empty list THEN IllegalArgumentException is thrown`(asserter: UniAsserter) {
        `when`(appServiceClient.getAllAppIds()).thenReturn(Uni.createFrom().item(emptyList()))

        asserter.assertEquals(
            {
                transactionLifecycle.terminateOverdueTransactions()
            },
            Unit,
        )
    }
}
