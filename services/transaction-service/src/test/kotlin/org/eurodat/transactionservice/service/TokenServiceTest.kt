package org.eurodat.transactionservice.service

import jakarta.ws.rs.WebApplicationException
import org.eurodat.transactionservice.model.MessageClaims
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class TokenServiceTest {
    private val testClaims =
        MessageClaims(
            appId = "testAppId",
            transactionId = "testTransactionId",
            workflowId = "testWorkflowId",
            allowedSelectors = listOf("dbselector", "cobaselector"),
        )

    @Test
    fun `test generateToken`() {
        val token = TokenService.generateToken(testClaims)
        Assertions.assertNotNull(token)
    }

    @Test
    fun `test extractClaimsFromToken`() {
        val token = TokenService.generateToken(testClaims)
        val extractedClaims = TokenService.extractClaimsFromToken(token)
        Assertions.assertEquals(testClaims.appId, extractedClaims.appId)
        Assertions.assertEquals(testClaims.transactionId, extractedClaims.transactionId)
        Assertions.assertEquals(testClaims.workflowId, extractedClaims.workflowId)
        Assertions.assertEquals(testClaims.allowedSelectors, extractedClaims.allowedSelectors)
    }

    @Test
    fun `test extractClaimsFromInvalidToken`() {
        val invalidToken = "InvalidTokenString"
        Assertions.assertThrows(WebApplicationException::class.java) {
            TokenService.extractClaimsFromToken(invalidToken)
        }
    }
}
