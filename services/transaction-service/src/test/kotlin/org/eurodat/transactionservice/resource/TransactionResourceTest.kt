package org.eurodat.transactionservice.resource

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.quarkus.test.junit.QuarkusTest
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.ws.rs.NotFoundException
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eurodat.client.appservice.AppServiceClient
import org.eurodat.client.appservice.model.AppResponse
import org.eurodat.client.database.TransactionDatabaseClient
import org.eurodat.client.database.TransactionDatabaseRequest
import org.eurodat.client.transactionservice.AppRequest
import org.eurodat.client.transactionservice.Transaction
import org.eurodat.client.transactionservice.TransactionRequest
import org.eurodat.transactionservice.repository.ClientMappingRepository
import org.eurodat.transactionservice.repository.MessageRepository
import org.eurodat.transactionservice.repository.TransactionRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TransactionResourceTest {
    private val transactionId = "foo"
    private val appId = "safeAMLAppMock"

    private val appServiceClient =
        mockk<AppServiceClient>().apply {
            every { findApp(appId) } returns
                Uni.createFrom().item(
                    AppResponse(
                        appId,
                        "",
                        "",
                        "",
                        mutableListOf(),
                        emptyList(),
                    ),
                )
            every { findApp("nonsense") } returns Uni.createFrom().failure(NotFoundException())
        }
    private val transactionDatabaseClient =
        mockk<TransactionDatabaseClient>().apply {
            every { createTransactionDatabase(any()) } returns uni { }
            every { deleteTransactionDatabase(any()) } returns uni { }
        }
    private val transactionRepository =
        mockk<TransactionRepository>().apply {
            every { createTransaction(any()) } returns uni { Transaction(transactionId, appId) }
            every { findTransaction(transactionId) } returns
                uni {
                    Transaction(
                        transactionId,
                        appId,
                        "data_consumer",
                        listOf("dbselector", "cobaselector"),
                        listOf("dbselector", "cobaselector"),
                        null,
                        null,
                    )
                }
            every { endTransaction(transactionId) } returns uni { Transaction(transactionId, appId) }
        }
    private val jwt =
        mockk<JsonWebToken>().apply {
            every { getClaim<String>("client_id") } returns "data_provider"
        }
    private val clientMappingRepository =
        mockk<ClientMappingRepository>().apply {
            every { getClientSelector(any()) } returns uni { "dbselector" }
            every { getAllSelectors() } returns uni { listOf("dbselector", "cobaselector") }
        }
    private val messageRepository =
        mockk<MessageRepository>().apply {
            every { deleteMessagesForTransaction(any()) } returns uni { }
        }

    private val transactionResource: TransactionResource =
        TransactionResource(
            appServiceClient,
            transactionDatabaseClient,
            transactionRepository,
            clientMappingRepository,
            messageRepository,
            jwt,
        )

    @Test
    fun testStartTransaction() {
        val appId = appId
        val clientId = "data_provider"
        val appRequest = AppRequest(appId = appId)
        val transaction = transactionResource.startTransaction(appRequest).await().indefinitely()

        verify {
            transactionRepository.createTransaction(
                TransactionRequest(
                    clientId,
                    appId,
                    listOf("dbselector", "cobaselector"),
                    listOf("dbselector", "cobaselector"),
                ),
            )
        }
        verify {
            transactionDatabaseClient.createTransactionDatabase(
                TransactionDatabaseRequest(
                    appId,
                    transaction.id,
                ),
            )
        }
    }

    @Test
    fun testEndTransaction() {
        val appId = appId
        val clientId = "data_provider"
        val appRequest = AppRequest(appId = appId)
        val transaction = transactionResource.startTransaction(appRequest).await().indefinitely()
        val transactionID = transaction.id
        transactionResource.endTransaction(transaction.id).await().indefinitely()

        verify {
            transactionRepository.createTransaction(
                TransactionRequest(
                    clientId,
                    appId,
                    listOf("dbselector", "cobaselector"),
                    listOf("dbselector", "cobaselector"),
                ),
            )
        }
        verify {
            transactionDatabaseClient.deleteTransactionDatabase(TransactionDatabaseRequest(appId, transactionID))
        }
        verify { transactionRepository.endTransaction(any()) }
        verify { messageRepository.deleteMessagesForTransaction(transactionID) }
    }

    @Test
    fun testStartNonExistentTransaction() {
        val appId = "nonsense"
        val appRequest = AppRequest(appId = appId)

        Assertions.assertThrows(NotFoundException::class.java) {
            transactionResource.startTransaction(appRequest).await().indefinitely()
        }
    }

    @Test
    fun testFindNonExistentTransaction() {
        val nonExistentTransactionId = "nonExistentTransactionId"

        every { transactionRepository.findTransaction(nonExistentTransactionId) } returns Uni.createFrom().nullItem()

        Assertions.assertThrows(NotFoundException::class.java) {
            transactionResource.findTransaction(nonExistentTransactionId).await().indefinitely()
        }

        verify { transactionRepository.findTransaction(nonExistentTransactionId) }
    }
}
