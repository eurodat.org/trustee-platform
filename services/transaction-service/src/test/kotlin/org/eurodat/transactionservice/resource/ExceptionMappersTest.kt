package org.eurodat.transactionservice.resource

import io.quarkus.test.junit.QuarkusTest
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.ClientErrorException
import jakarta.ws.rs.NotFoundException
import org.jboss.resteasy.reactive.RestResponse
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@QuarkusTest
class ExceptionMappersTest {
    private val exceptionMappers = ExceptionMappers()

    @Test
    fun testNotFoundExceptionMappers() {
        val notFoundException = NotFoundException("exception")
        val result: Uni<RestResponse<String>> = exceptionMappers.mapNotFound(notFoundException)
        val response: RestResponse<String> = result.await().indefinitely()
        Assertions.assertEquals(RestResponse.StatusCode.NOT_FOUND, response.status)
        Assertions.assertEquals("exception", response.entity)
    }

    @Test
    fun testBadRequestExceptionMappers() {
        val exceptionText = "Bad Request Dummy Text"
        val badRequestException = BadRequestException(exceptionText)
        val result: Uni<RestResponse<String>> = exceptionMappers.mapBadRequest(badRequestException)
        val response: RestResponse<String> = result.await().indefinitely()
        Assertions.assertEquals(RestResponse.StatusCode.BAD_REQUEST, response.status)
        Assertions.assertEquals(exceptionText, response.entity)
    }

    @Test
    fun testClientErrorExceptionMappers() {
        val exceptionText = "409 Conflict Dummy Text"
        val clientErrorException = ClientErrorException(exceptionText, RestResponse.StatusCode.CONFLICT)
        val result: Uni<RestResponse<String>> = exceptionMappers.mapClientError(clientErrorException)
        val response: RestResponse<String> = result.await().indefinitely()
        Assertions.assertEquals(RestResponse.StatusCode.CONFLICT, response.status)
        Assertions.assertEquals(exceptionText, response.entity)
    }
}
