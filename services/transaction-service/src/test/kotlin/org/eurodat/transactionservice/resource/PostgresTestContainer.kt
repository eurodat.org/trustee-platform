package org.eurodat.transactionservice.resource

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager
import org.testcontainers.containers.PostgreSQLContainer

class PostgresTestContainer : QuarkusTestResourceLifecycleManager {
    private lateinit var container: PostgreSQLContainer<*>

    override fun start(): MutableMap<String, String> {
        container =
            PostgreSQLContainer("postgres:15")
                .withUsername("postgres")
                .withPassword("pwd")
        container.start()

        val databaseUrl = container.jdbcUrl.replace("jdbc", "vertx-reactive")

        return mutableMapOf(
            "quarkus.datasource.reactive.url" to databaseUrl,
            "quarkus.datasource.username" to container.username,
            "quarkus.datasource.password" to container.password,
        )
    }

    override fun stop() {
        container.close()
    }
}
