package org.eurodat.transactionservice.resource

import io.quarkus.test.common.http.TestHTTPEndpoint
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.security.TestSecurity
import io.restassured.RestAssured
import org.hamcrest.Matchers.not
import org.junit.jupiter.api.Test

@TestHTTPEndpoint(ClientResource::class)
@QuarkusTest
class ClientResourceScopeTest {
    @Test
    @TestSecurity(user = "testUser", permissions = ["openid"])
    fun testRegisterClientWithoutAppropriateRole() {
        RestAssured
            .given()
            .contentType("application/json")
            .body(
                """
                    {"name": "testClient",
                    "certificate": "testCertificate",
                    "createPublicClient": true,
                    "clientSelector": "selector" }
                """,
            ).post()
            .then()
            .statusCode(403)
    }

    @Test
    @TestSecurity(user = "testUser", permissions = ["openid", "client:write"])
    fun testRegisterClientWithClientWriteRole() {
        RestAssured
            .given()
            .contentType("application/json")
            .body(
                """
                    {"name": "testClient",
                    "certificate": "testCertificate",
                    "createPublicClient": true,
                    "clientSelector": "selector" }
                """,
            ).post()
            .then()
            .statusCode(not(403))
    }
}
