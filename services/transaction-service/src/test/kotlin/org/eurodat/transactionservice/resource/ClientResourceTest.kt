package org.eurodat.transactionservice.resource

import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.spyk
import io.mockk.verify
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.CompositeException
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.ws.rs.BadRequestException
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.core.Response
import org.eurodat.client.credential.DatabaseCredentialClient
import org.eurodat.client.database.ClientRoleCreationRequest
import org.eurodat.client.database.TransactionDatabaseClient
import org.eurodat.client.transactionservice.ClientSelectorMapping
import org.eurodat.transactionservice.configuration.KeycloakConfig
import org.eurodat.transactionservice.model.ClientCertificateValues
import org.eurodat.transactionservice.model.ClientRegistrationForm
import org.eurodat.transactionservice.model.Purpose
import org.eurodat.transactionservice.repository.ClientMappingRepository
import org.eurodat.transactionservice.service.LockService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.keycloak.admin.client.Keycloak
import org.keycloak.admin.client.resource.ClientResource
import org.keycloak.admin.client.resource.ClientsResource
import org.keycloak.admin.client.resource.RealmResource
import org.keycloak.representations.idm.ClientRepresentation
import java.util.UUID
import java.util.function.Supplier

@QuarkusTest
class ClientResourceTest {
    private val credService: DatabaseCredentialClient = mockk(relaxed = true)
    private val databaseClient: TransactionDatabaseClient = mockk(relaxed = true)
    private val keycloak: Keycloak = mockk(relaxed = true)
    private val keycloakConfig: KeycloakConfig = mockk(relaxed = true)
    private val clientMappingRepository: ClientMappingRepository = mockk(relaxed = true)
    private val lockService: LockService = mockk(relaxed = true)
    private val clientResource =
        spyk(ClientResource(keycloak, keycloakConfig, clientMappingRepository, lockService, credService, databaseClient))
    private val realm = "eurodat"
    private val keycloakUrl = "https://localhost/auth"
    private val clientName = "foo"
    private val certificate = "cert"
    private val clientSelector = "selector"
    private val clientPurposes: Set<Purpose> = setOf(Purpose.APPS, Purpose.TRANSACTIONS)
    private val clientsResource: ClientsResource = mockk()
    private val realmResource: RealmResource = mockk()
    private val clientSelectorMapping = ClientSelectorMapping("client1", "selector1")
    private val clientMappings: Uni<List<ClientSelectorMapping>> =
        Uni.createFrom().item(
            listOf(clientSelectorMapping),
        )
    private val allSelectors: Uni<List<String>> = Uni.createFrom().item(listOf("selector1"))
    private val clientRegistrationFormNoPublic = ClientRegistrationForm(clientName, certificate, clientSelector, clientPurposes)

    @BeforeEach
    fun setup() {
        clearAllMocks()
        every { credService.registerClientConnection(any()) } returns Uni.createFrom().item(Unit)
        every { keycloakConfig.realm() } returns realm
        every { keycloakConfig.serverUrl() } returns keycloakUrl
        every { keycloak.realm(any()) } returns realmResource
        every { realmResource.clients() } returns clientsResource
        every { databaseClient.deleteClientRole(any()) } returns uni { }
        every { databaseClient.createClientRole(any()) } returns uni { }
        every { credService.deleteUserConnection(any()) } returns uni { }
        every { clientMappingRepository.getClientMappings() } returns clientMappings
        every { clientMappingRepository.saveClientMapping(any()) } returns uni { clientSelectorMapping }
        every { clientMappingRepository.deleteClientMapping(any()) } returns uni { true }
        every { clientMappingRepository.getClientSelector("foo") } returns uni { "selector" }
        every { lockService.executeWithLock(any(), any<Supplier<Uni<Any>>>()) } answers {
            val supplier: Supplier<Uni<Any>> = secondArg()
            supplier.get()
        }
        every { lockService.executeBlockingWithLock(any(), any<Supplier<Any>>()) } answers {
            val supplier: Supplier<Any> = secondArg()
            uni { supplier.get() }
        }
    }

    /**
     * Helper function that sets up the required mocking for the registerUser tests that expect a failure.
     */
    private fun setupRegisterClientFailure() {
        val clientRepresentationSlot = slot<ClientRepresentation>()
        every {
            clientsResource.create(capture(clientRepresentationSlot))
        } returns Response.ok().build()
        every { clientsResource.findByClientId(any()) } returns emptyList()
    }

    /**
     * Helper function that executes registerUser, expects an Exception and a call to the User resource deletion method.
     */
    private fun testRegisterClientExpectDeletionOnFailure(asserter: UniAsserter) {
        asserter
            .assertFailedWith(
                {
                    clientResource.registerClient(clientRegistrationFormNoPublic)
                },
                {
                    Exception::class.java
                },
            ).execute {
                verify { clientResource.deleteClientWithoutLock(clientName) }
            }
    }

    @Test
    @RunOnVertxContext
    fun registerClient(asserter: UniAsserter) {
        val clientRepresentationSlot = slot<ClientRepresentation>()
        every {
            clientsResource.create(capture(clientRepresentationSlot))
        } returns Response.ok().build()
        every { clientsResource.findByClientId(any()) } returns emptyList()
        asserter.assertThat(
            {
                clientResource.registerClient(clientRegistrationFormNoPublic)
            },
            {
                verify { keycloak.realm(realm) }
                verify { databaseClient.createClientRole(ClientRoleCreationRequest(clientRegistrationFormNoPublic.name)) }
                val clientRepresentation = clientRepresentationSlot.captured
                Assertions.assertEquals(clientName, clientRepresentation.clientId)
                Assertions.assertEquals(clientRepresentation.defaultClientScopes.sorted(), setOf("app:write", "transaction:write").sorted())
                Assertions.assertEquals(certificate, clientRepresentation.attributes["jwt.credential.certificate"])
                Assertions.assertEquals(1, clientRepresentation.protocolMappers.size)
                verify { clientMappingRepository.saveClientMapping(ClientSelectorMapping(clientName, clientSelector)) }
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun registerClientClientExists(asserter: UniAsserter) {
        val existingClientRepresentation =
            ClientRepresentation().apply {
                attributes = mutableMapOf()
                id = UUID.randomUUID().toString()
            }
        every { clientsResource.findByClientId(any()) } returns listOf(existingClientRepresentation)
        val clientResource: ClientResource = mockk()
        every { clientsResource.get(any()) } returns clientResource
        val clientRepresentationSlot = slot<ClientRepresentation>()
        every { clientResource.update(capture(clientRepresentationSlot)) } returns Unit
        val that = this
        asserter
            .assertFailedWith(
                { this.clientResource.registerClient(clientRegistrationFormNoPublic) },
                { it is CompositeException && it.cause is BadRequestException },
            ).execute {
                verify(exactly = 0) { clientMappingRepository.saveClientMapping(any()) }
                verify(exactly = 0) { that.clientResource.deleteClientWithoutLock(any()) }
            }
    }

    /**
     * Tests that the deleteUserWithoutLock is called after a failure in the clientMapping creation.
     */
    @Test
    @RunOnVertxContext
    fun registerClientWithClientMappingFailure(asserter: UniAsserter) {
        setupRegisterClientFailure()
        every { clientMappingRepository.saveClientMapping(any()) } returns Uni.createFrom().failure { Exception() }
        testRegisterClientExpectDeletionOnFailure(asserter)
    }

    /**
     * Tests that the deleteUserWithoutLock is called after a failure in the role creation.
     */
    @Test
    @RunOnVertxContext
    fun registerClientWithDatabaseServiceFailure(asserter: UniAsserter) {
        setupRegisterClientFailure()
        every { databaseClient.createClientRole(any()) } returns Uni.createFrom().failure { Exception() }
        testRegisterClientExpectDeletionOnFailure(asserter)
    }

    /**
     * Tests that the deleteUserWithoutLock is called after a failure in the vault connection registration.
     */
    @Test
    @RunOnVertxContext
    fun registerClientWithCredentialServiceFailure(asserter: UniAsserter) {
        setupRegisterClientFailure()
        every { credService.registerClientConnection(any()) } returns Uni.createFrom().failure { Exception() }
        testRegisterClientExpectDeletionOnFailure(asserter)
    }

    @Test
    @RunOnVertxContext
    fun registerClientWithKeycloakFailure(asserter: UniAsserter) {
        every { clientsResource.findByClientId(any()) } returns emptyList()
        every { clientsResource.create(any()) } returns (Response.status(400).build())
        asserter.assertThat(
            {
                clientResource.registerClient(clientRegistrationFormNoPublic)
            },
            { verify(exactly = 0) { clientResource.deleteClientWithoutLock(any()) } },
        )
    }

    @Test
    @RunOnVertxContext
    fun updateCertificate(asserter: UniAsserter) {
        val existingClientRepresentation =
            ClientRepresentation().apply {
                attributes = mutableMapOf()
                id = UUID.randomUUID().toString()
                clientId = UUID.randomUUID().toString()
            }
        every { clientsResource.findByClientId(any()) } returns listOf(existingClientRepresentation)
        val clientResource: ClientResource = mockk()
        every { clientsResource.get(any()) } returns clientResource
        val clientRepresentationSlot = slot<ClientRepresentation>()
        every { clientResource.update(capture(clientRepresentationSlot)) } returns Unit
        val clientCertificateValues = ClientCertificateValues(clientName, certificate)
        asserter.assertThat(
            {
                this.clientResource.updateCertificate(clientCertificateValues)
            },
            {
                verify { keycloak.realm(realm) }
                val clientRepresentation = clientRepresentationSlot.captured
                Assertions.assertEquals(certificate, clientRepresentation.attributes["jwt.credential.certificate"])
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun updateCertificateClientDoesNotExist(asserter: UniAsserter) {
        val clientRepresentationSlot = slot<ClientRepresentation>()
        every {
            clientsResource.create(capture(clientRepresentationSlot))
        } returns Response.ok().build()
        every { clientsResource.findByClientId(any()) } returns emptyList()
        val clientCertificateValues = ClientCertificateValues(clientName, certificate)
        asserter.assertFailedWith(
            {
                clientResource.updateCertificate(clientCertificateValues)
            },
            NotFoundException::class.java,
        )
    }

    @Test
    fun getClientSelectorMapping() {
        clientResource
            .getClientSelectorMapping()
            .onItem()
            .transform { actualClientMappings ->
                Assertions.assertEquals(clientMappings.await().indefinitely(), actualClientMappings)
            }.await()
            .indefinitely()
    }

    @Test
    fun getAllSelectors() {
        clientResource
            .getAllClientSelectors()
            .onItem()
            .transform { actualClientSelectors ->
                Assertions.assertEquals(allSelectors.await().indefinitely(), actualClientSelectors)
            }.await()
            .indefinitely()
    }

    @Test
    @RunOnVertxContext
    fun deleteClientFull(asserter: UniAsserter) {
        val existingClientRepresentation =
            ClientRepresentation().apply {
                attributes = mutableMapOf()
                id = UUID.randomUUID().toString()
                clientId = clientName
            }
        every { clientsResource.findByClientId("foo") } returns listOf(existingClientRepresentation)
        val clientResource: ClientResource = mockk()
        every { clientsResource.get(existingClientRepresentation.id) } returns clientResource
        every { clientResource.remove() } returns Unit
        asserter.assertThat(
            { this.clientResource.deleteClient(clientName) },
            { _ ->
                verify { credService.deleteUserConnection("foo") }
                verify { clientMappingRepository.deleteClientMapping("foo") }
                verify { keycloak.realm(realm) }
                verify { clientResource.remove() }
                verify { databaseClient.deleteClientRole("foo") }
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun deleteClientNoPublic(asserter: UniAsserter) {
        val existingClientRepresentation =
            ClientRepresentation().apply {
                attributes = mutableMapOf()
                id = UUID.randomUUID().toString()
                clientId = clientName
            }
        every { clientsResource.findByClientId("foo") } returns listOf(existingClientRepresentation)
        val clientResource: ClientResource = mockk()
        every { clientsResource.get(existingClientRepresentation.id) } returns clientResource
        every { clientResource.remove() } returns Unit
        asserter.assertThat(
            { this.clientResource.deleteClient(clientName) },
            { _ ->
                verify { credService.deleteUserConnection("foo") }
                verify { clientMappingRepository.deleteClientMapping("foo") }
                verify { keycloak.realm(realm) }
                verify { clientResource.remove() }
                verify { databaseClient.deleteClientRole("foo") }
            },
        )
    }
}
