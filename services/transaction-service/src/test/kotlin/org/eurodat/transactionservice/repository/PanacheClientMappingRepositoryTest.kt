package org.eurodat.transactionservice.repository

import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import org.eurodat.client.transactionservice.ClientSelectorMapping
import org.eurodat.transactionservice.model.ClientSelectorEntity
import org.eurodat.transactionservice.resource.PostgresTestContainer
import org.hibernate.reactive.mutiny.Mutiny.SessionFactory
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import java.util.UUID

@QuarkusTest
@QuarkusTestResource(PostgresTestContainer::class)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class PanacheClientMappingRepositoryTest {
    @Inject
    lateinit var sessionFactory: SessionFactory

    @Inject
    lateinit var clientMappingRepository: PanacheClientMappingRepository

    private fun setup(): Uni<Long> =
        sessionFactory.withTransaction { session ->
            clientMappingRepository.deleteAll().call { _ ->
                session.persistAll(
                    ClientSelectorEntity("foo", "bar"),
                    ClientSelectorEntity("consumer", "Selector"),
                    ClientSelectorEntity("data_consumer", "dbselector"),
                )
            }
        }

    @Test
    @RunOnVertxContext
    fun getClientMappings(asserter: UniAsserter) {
        asserter.assertThat(
            { setup().chain { _ -> sessionFactory.withSession { clientMappingRepository.getClientMappings() } } },
            { mappings -> Assertions.assertEquals(3, mappings.size) },
        )
    }

    @Test
    @RunOnVertxContext
    fun getAllSelectors(asserter: UniAsserter) {
        asserter.assertThat(
            { setup().chain { _ -> sessionFactory.withSession { clientMappingRepository.getAllSelectors() } } },
            { mappings -> Assertions.assertEquals(3, mappings.size) },
        )
    }

    @Test
    @RunOnVertxContext
    fun saveClientMapping(asserter: UniAsserter) {
        val clientSelectorMapping = ClientSelectorMapping(UUID.randomUUID().toString(), UUID.randomUUID().toString())
        asserter.assertEquals(
            {
                setup().chain { _ ->
                    sessionFactory.withTransaction { _ -> clientMappingRepository.saveClientMapping(clientSelectorMapping) }
                }
            },
            clientSelectorMapping,
        )
    }

    @Test
    @RunOnVertxContext
    fun saveClientMappingClientIdExists(asserter: UniAsserter) {
        asserter.assertFailedWith(
            {
                setup().chain { _ ->
                    sessionFactory.withTransaction { _ ->
                        clientMappingRepository.saveClientMapping(ClientSelectorMapping("foo", UUID.randomUUID().toString()))
                    }
                }
            },
            IllegalArgumentException::class.java,
        )
    }

    @Test
    @RunOnVertxContext
    fun saveClientMappingClientSelectorExists(asserter: UniAsserter) {
        asserter.assertFailedWith(
            {
                setup().chain { _ ->
                    sessionFactory.withTransaction { _ ->
                        clientMappingRepository.saveClientMapping(ClientSelectorMapping(UUID.randomUUID().toString(), "bar"))
                    }
                }
            },
            IllegalArgumentException::class.java,
        )
    }

    @Test
    @RunOnVertxContext
    fun deleteClientMapping(asserter: UniAsserter) {
        // insert test user clientTest
        val clientSelectorMapping = ClientSelectorMapping("clientTest", "selectorTest")
        asserter.assertEquals(
            { sessionFactory.withTransaction { _ -> clientMappingRepository.saveClientMapping(clientSelectorMapping) } },
            clientSelectorMapping,
        )
        asserter.assertThat(
            { sessionFactory.withSession { clientMappingRepository.getClientMappings() } },
            { mappings -> Assertions.assertEquals(4, mappings.size) },
        )
        asserter.assertEquals(
            {
                sessionFactory.withTransaction { _ ->
                    clientMappingRepository.deleteClientMapping("clientTest")
                }
            },
            true,
        )
        asserter.assertThat(
            { sessionFactory.withSession { clientMappingRepository.getClientMappings() } },
            { mappings -> Assertions.assertEquals(3, mappings.size) },
        )
    }

    @Test
    @RunOnVertxContext
    fun clientExists(asserter: UniAsserter) {
        asserter.assertTrue {
            setup().chain { _ ->
                sessionFactory.withSession { clientMappingRepository.clientExists("foo") }
            }
        }
    }

    @Test
    @RunOnVertxContext
    fun clientDoesNotExist(asserter: UniAsserter) {
        asserter.assertFalse {
            setup().chain { _ ->
                sessionFactory.withSession { clientMappingRepository.clientExists("bar") }
            }
        }
    }
}
