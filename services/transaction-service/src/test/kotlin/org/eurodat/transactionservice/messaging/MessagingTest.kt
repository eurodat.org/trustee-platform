package org.eurodat.transactionservice.messaging

import jakarta.ws.rs.WebApplicationException
import org.eurodat.transactionservice.model.MessageClaims
import org.eurodat.transactionservice.resource.createMessage
import org.eurodat.transactionservice.resource.validateSelectors
import org.eurodat.transactionservice.service.TokenService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.util.Base64

class MessagingTest {
    private fun generateBase64EncodedToken(
        appId: String,
        transactionId: String,
        workflowId: String,
        allowedSelectors: List<String>,
    ): String {
        val claimsJson =
            """
            {
                "app_id": "$appId",
                "transaction_id": "$transactionId",
                "workflow_id": "$workflowId",
                "allowed_selectors": ${allowedSelectors.map { "\"$it\"" }}
            }
            """.trimIndent()

        return Base64.getEncoder().encodeToString(claimsJson.toByteArray())
    }

    @Test
    fun extractClaimsFromTokenTest() {
        val token =
            generateBase64EncodedToken(
                appId = "mockApp",
                transactionId = "t1",
                workflowId = "w1",
                allowedSelectors = listOf("selector1", "selector2"),
            )
        val claims = TokenService.extractClaimsFromToken(token)

        Assertions.assertEquals("mockApp", claims.appId)
        Assertions.assertEquals("t1", claims.transactionId)
        Assertions.assertEquals("w1", claims.workflowId)
        Assertions.assertEquals(listOf("selector1", "selector2"), claims.allowedSelectors)
    }

    @Test
    fun validateSelectorsTest() {
        val requestSelectors = listOf("selector1", "selector2")
        val allowedSelectors = listOf("selector1", "selector2", "selector3")

        Assertions.assertDoesNotThrow {
            validateSelectors(requestSelectors, allowedSelectors)
        }

        val invalidRequestSelectors = listOf("selector1", "invalid_selector")
        val exception =
            Assertions.assertThrows(WebApplicationException::class.java) {
                validateSelectors(invalidRequestSelectors, allowedSelectors)
            }

        Assertions.assertEquals(400, exception.response.status)
        Assertions.assertEquals("Invalid selectors", exception.message)
    }

    @Test
    fun createMessageTest() {
        val claims =
            MessageClaims(
                appId = "mockApp",
                transactionId = "t1",
                workflowId = "w1",
                allowedSelectors = listOf("selector1", "selector2"),
            )

        val label = "Test Message"
        val selector = "selector1"

        val message = createMessage(claims, label, selector)

        Assertions.assertEquals("mockApp", message.appId)
        Assertions.assertEquals("t1", message.transactionId)
        Assertions.assertEquals("w1", message.workflowId)
        Assertions.assertEquals("Test Message", message.label)
        Assertions.assertEquals("selector1", message.selector)
    }
}
