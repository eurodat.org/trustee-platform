package org.eurodat.transactionservice.resource

import io.argoproj.workflow.apis.WorkflowServiceApi
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1Workflow
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1WorkflowStatus
import io.argoproj.workflow.models.IoArgoprojWorkflowV1alpha1WorkflowSubmitRequest
import io.argoproj.workflow.models.ObjectMeta
import io.mockk.every
import io.mockk.mockk
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.core.Response
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eurodat.client.appservice.AppServiceClient
import org.eurodat.client.appservice.model.AppResponse
import org.eurodat.client.appservice.model.TableSecurityMapping
import org.eurodat.client.appservice.model.WorkflowDefinition
import org.eurodat.client.credential.Credentials
import org.eurodat.client.credential.DatabaseCredentialClient
import org.eurodat.client.transactionservice.Transaction
import org.eurodat.client.transactionservice.WorkflowRequest
import org.eurodat.client.transactionservice.WorkflowStatusPhase
import org.eurodat.transactionservice.repository.ClientMappingRepository
import org.eurodat.transactionservice.repository.TransactionRepository
import org.eurodat.transactionservice.service.ArgoWorkflowsHelperService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class WorkflowResourceTest {
    private val transactionId = "foo"
    private val appId = "safeAMLAppMock"
    private val workflowDefinitionId = "helloWorld"

    private val argoWorkflowsHelperService =
        mockk<ArgoWorkflowsHelperService>().apply {
            every { prepareArgoRequest(any(), any(), any(), any(), any()) } returns
                Uni.createFrom().item(mockk<IoArgoprojWorkflowV1alpha1WorkflowSubmitRequest>())
        }
    private val appServiceClient =
        mockk<AppServiceClient>().apply {
            every { findApp(appId) } returns
                Uni.createFrom().item(
                    AppResponse(
                        appId,
                        "dummyString1",
                        "dummyString2",
                        "dummyString3",
                        listOf(TableSecurityMapping("security_column", "output_transactions")),
                        mutableListOf(
                            WorkflowDefinition(
                                workflowDefinitionId,
                                "log-helloworld",
                                "hello-world",
                            ),
                            WorkflowDefinition(
                                "investigationWorkflow",
                                "safeaml-mock-all",
                                "do-all-together",
                            ),
                        ),
                    ),
                )
            every { findApp("nonsense") } returns Uni.createFrom().failure(NotFoundException())
        }
    private val transactionRepository =
        mockk<TransactionRepository>().apply {
            every { createTransaction(any()) } returns uni { Transaction(transactionId, appId) }
            every { findTransaction(transactionId) } returns
                uni {
                    Transaction(
                        transactionId,
                        appId,
                        "data_consumer",
                        listOf("dbselector", "cobaselector"),
                        listOf("dbselector", "cobaselector"),
                        null,
                        null,
                    )
                }
            every { endTransaction(transactionId) } returns uni { Transaction(transactionId, appId) }
        }
    private val dbCredService =
        mockk<DatabaseCredentialClient>().apply {
            every { registerTransactionConnection(any()) } returns uni { }
            every { readInternalAccessCredentials(any()) } returns
                uni {
                    Credentials("testurl", "testappuser", "testapppassword", 1234)
                }
            every { deleteInternalConnection(any()) } returns Uni.createFrom().nullItem()
        }
    private val workflowClient =
        mockk<WorkflowServiceApi>().apply {
            every { workflowServiceSubmitWorkflow(any(), any()) } returns
                uni {
                    IoArgoprojWorkflowV1alpha1Workflow().apply { metadata = ObjectMeta().apply { name = transactionId } }
                }
            every { workflowServiceGetWorkflow(any(), "Succeding", any(), any()) } returns
                uni {
                    IoArgoprojWorkflowV1alpha1Workflow().apply {
                        metadata = ObjectMeta().apply { name = transactionId }
                        status =
                            IoArgoprojWorkflowV1alpha1WorkflowStatus().apply {
                                phase = "Succeeded"
                            }
                    }
                }
            every { workflowServiceGetWorkflow(any(), "Running", any(), any()) } returns
                uni {
                    IoArgoprojWorkflowV1alpha1Workflow().apply {
                        metadata = ObjectMeta().apply { name = transactionId }
                        status =
                            IoArgoprojWorkflowV1alpha1WorkflowStatus().apply {
                                phase = "Running"
                            }
                    }
                }
            every { workflowServiceGetWorkflow(any(), "NotExisting", any(), any()) } returns
                Uni.createFrom().failure(
                    io.argoproj.workflow.apis
                        .ApiException(Response.status(Response.Status.NOT_FOUND).build()),
                )
        }
    private val jwt =
        mockk<JsonWebToken>().apply {
            every { getClaim<String>("client_id") } returns "data_provider"
        }
    private val clientMappingRepository =
        mockk<ClientMappingRepository>().apply {
            every { getClientSelector(any()) } returns uni { "dbselector" }
            every { getAllSelectors() } returns uni { listOf("dbselector", "cobaselector") }
        }

    private val workflowResource: WorkflowResource =
        WorkflowResource(
            appServiceClient,
            dbCredService,
            workflowClient,
            argoWorkflowsHelperService,
            transactionRepository,
            clientMappingRepository,
            jwt,
            "namespace",
        )

    @Test
    fun testStartWorkflow() {
        val status =
            workflowResource
                .startWorkflow(transactionId, WorkflowRequest(workflowDefinitionId))
                .await()
                .indefinitely()
        Assertions.assertEquals(workflowDefinitionId, status.workflowDefinitionId)
    }

    @Test
    @RunOnVertxContext
    fun testGetTransactionRunning(asserter: UniAsserter) {
        asserter.assertThat(
            { workflowResource.getWorkflow(transactionId, "Running") },
            { Assertions.assertEquals(WorkflowStatusPhase.RUNNING, it.status.phase) },
        )
    }

    @Test
    @RunOnVertxContext
    fun testGetTransactionSuccess(asserter: UniAsserter) {
        asserter.assertThat(
            { workflowResource.getWorkflow(transactionId, "Succeding") },
            { Assertions.assertEquals(WorkflowStatusPhase.SUCCEEDED, it.status.phase) },
        )
    }

    @Test
    @RunOnVertxContext
    fun testGetTransactionNotFound(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { workflowResource.getWorkflow(transactionId, "NotExisting") },
            { exception -> Assertions.assertInstanceOf(NotFoundException::class.java, exception) },
        )
    }
}
