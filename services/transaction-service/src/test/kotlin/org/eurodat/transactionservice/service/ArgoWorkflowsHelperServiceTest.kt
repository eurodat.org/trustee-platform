package org.eurodat.transactionservice.service

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import org.eurodat.client.appservice.model.WorkflowDefinition
import org.eurodat.client.credential.Credentials
import org.eurodat.transactionservice.repository.TransactionRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`

@QuarkusTest
class ArgoWorkflowsHelperServiceTest {
    @Inject
    private lateinit var argoWorkflowsHelperService: ArgoWorkflowsHelperService

    @InjectMock
    private lateinit var transactionRepository: TransactionRepository

    @Test
    @RunOnVertxContext
    fun testPrepareArgoRequest(asserter: UniAsserter) {
        `when`(transactionRepository.getParticipantsByTransactionId("txId"))
            .thenReturn(Uni.createFrom().item(listOf("selector1", "selector2")))

        val credentials =
            Credentials(
                "jdbc:postgresql://eurodat-postgresql.transaction-plane-namesuffix.svc:5432/database_4f5692ab_893a_4c22_b5d6_6575f6aab78f",
                "testuser",
                "testpw",
                5,
            )
        val workflow = WorkflowDefinition("12345678", "HelloWorld", "whalesay")
        asserter.assertThat(
            {
                argoWorkflowsHelperService.prepareArgoRequest(
                    credentials,
                    "data-plane",
                    workflow,
                    "txId",
                    "testApp",
                )
            },
            { argoRequest ->
                Assertions.assertNotNull(argoRequest)
                Assertions.assertTrue(
                    argoRequest.submitOptions.parameters
                        .contains("database_host_name=eurodat-postgresql.transaction-plane-namesuffix.svc"),
                )
                Assertions.assertTrue(
                    argoRequest.submitOptions.parameters.contains("database_name=database_4f5692ab_893a_4c22_b5d6_6575f6aab78f"),
                )
                Assertions.assertTrue(argoRequest.submitOptions.parameters.contains("database_user_login=testuser"))
                Assertions.assertTrue(argoRequest.submitOptions.parameters.contains("database_password_login=testpw"))
                Assertions.assertTrue(argoRequest.submitOptions.parameters.contains("messaging_url=http://dummy-api"))

                // assert allowed_selectors in messaging_token
                val messagingToken =
                    argoRequest.submitOptions.parameters
                        .find { it.startsWith("messaging_token=") }
                        ?.substringAfter("messaging_token=")
                Assertions.assertNotNull(messagingToken)
                val claims = TokenService.extractClaimsFromToken(messagingToken!!)
                Assertions.assertEquals(listOf("selector1", "selector2"), claims.allowedSelectors)
            },
        )
    }
}
