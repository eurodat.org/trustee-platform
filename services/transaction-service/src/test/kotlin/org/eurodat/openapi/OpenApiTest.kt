package org.eurodat.openapi

import io.quarkus.test.common.http.TestHTTPResource
import io.quarkus.test.junit.QuarkusTest
import io.smallrye.openapi.runtime.io.Format
import io.smallrye.openapi.runtime.io.OpenApiParser
import jakarta.inject.Inject
import org.eclipse.microprofile.openapi.models.OpenAPI
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.net.URL

@QuarkusTest
class OpenApiTest {
    @TestHTTPResource("openapi")
    lateinit var url: URL

    @Inject
    lateinit var config: OpenApiConfig

    @Test
    fun testOpenApi() {
        val openApi: OpenAPI
        url.openStream().use { openApi = OpenApiParser.parse(it, Format.YAML, null) }
        Assertions.assertEquals(config.version(), openApi.info.version)

        Assertions.assertEquals(config.docs().url(), openApi.externalDocs.url)
        Assertions.assertEquals(config.docs().description(), openApi.externalDocs.description)
    }
}
