# Transaction service

This project uses Quarkus as an OIDC server to authenticate clients.

## Packaging and running the application

The application can be packaged using:

```sh
./gradlew build
```

It produces the `quarkus-run.jar` file in the `build/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `build/quarkus-app/lib/` directory.

The application is now runnable using `java -jar build/quarkus-app/quarkus-run.jar`.

Run

```sh
./gradlew build -Dquarkus.container-image.build=true
```

to build the image or include the following to directly push it to a registry.

```sh
  -Dquarkus.container-image.username="$USERNAME" \
  -Dquarkus.container-image.password="$PASSWORD" \
  -Dquarkus.container-image.build=true \
  -Dquarkus.container-image.push=true \
  -Dquarkus.container-image.image="$IMG_REGISTRY_NAME/transaction-service:$IMG_TAG"
```

## Related Guides

- [RESTEasy Reactive :material-launch:](https://quarkus.io/guides/resteasy-reactive): A JAX-RS implementation utilizing build time
  processing and Vert.x. This extension is not compatible with the quarkus-resteasy extension, or any of the extensions
  that depend on it.
- [OpenID Connect :material-launch:](https://quarkus.io/guides/security-openid-connect): Verify Bearer access tokens and authenticate
  users using the `client_credentials` flow.
