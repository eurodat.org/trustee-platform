plugins {
    java
    id("io.quarkus")
    kotlin("jvm")
    id("org.sonarqube") version "4.3.1.3277"
    id("jacoco")
    kotlin("plugin.allopen") version "2.1.0"
    kotlin("plugin.jpa") version "2.1.0"
}

allOpen {
    annotations("jakarta.persistence.Entity")
}

repositories {
    mavenCentral()
    mavenLocal()
}

jacoco {
    toolVersion = "0.8.10" // match quarkus' version
}

val eurodatVersion: String by project
val kotlinLoggingVersion: String by project
val quarkusPlatformArtifactId: String by project
val quarkusPlatformGroupId: String by project
val quarkusPlatformVersion: String by project
val quarkusPluginId: String by project
val wiremockVersion: String by project
val mockkVersion: String by project
val argoClientVersion: String by project
val keycloakAdminClientVersion = "26.0.4"
val keycloakCoreVersion = "26.1.0"
val mockitoKotlinVersion: String by project

configurations.all {
    resolutionStrategy {
        force("org.keycloak:keycloak-admin-client:$keycloakAdminClientVersion")
        force("org.keycloak:keycloak-core:$keycloakCoreVersion")
    }
}

dependencies {
    implementation(project(":app-service-client"))
    implementation(project(":transaction-service-client"))
    implementation(project(":credential-service-client"))
    implementation(project(":database-service-client"))

    api("io.github.oshai:kotlin-logging-jvm:$kotlinLoggingVersion")
    implementation("io.vertx:vertx-jdbc-client")
    implementation("$quarkusPluginId:quarkus-reactive-pg-client:$quarkusPlatformVersion")
    implementation("io.vertx:vertx-web-client")
    implementation("$quarkusPluginId:quarkus-container-image-jib:$quarkusPlatformVersion")
    implementation(enforcedPlatform("$quarkusPlatformGroupId:$quarkusPlatformArtifactId:$quarkusPlatformVersion"))
    implementation("$quarkusPluginId:quarkus-jdbc-postgresql:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-arc:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-oidc:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-rest-client-oidc-filter:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-rest-client-oidc-token-propagation:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-rest-jackson:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-rest-client-jackson:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-smallrye-health:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-smallrye-openapi:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-hibernate-reactive-panache-kotlin:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-reactive-pg-client:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-keycloak-admin-rest-client:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-vertx:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-scheduler:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-opentelemetry:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-flyway:$quarkusPlatformVersion")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")

    implementation("io.argoproj.workflow:argo-client-java:$argoClientVersion")

    testImplementation(project(":test-common"))
    testImplementation("$quarkusPluginId:quarkus-junit5:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-junit5-mockito:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-test-security-oidc:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-test-oidc-server:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-jacoco:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-test-vertx:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-test-security-jwt")

    testImplementation("org.mockito.kotlin:mockito-kotlin:$mockitoKotlinVersion")
    testImplementation("io.mockk:mockk:$mockkVersion")
    testImplementation("io.rest-assured:rest-assured")
    testImplementation("com.marcinziolo:kotlin-wiremock:$wiremockVersion")
    testImplementation("org.testcontainers:testcontainers")
    testImplementation("org.testcontainers:postgresql")
}

group = "org.eurodat"
version = eurodatVersion

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

tasks.withType<Test> {
    systemProperty("java.util.logging.manager", "org.jboss.logmanager.LogManager")
    finalizedBy("jacocoTestReport")

    configure<JacocoTaskExtension> {
        excludeClassLoaders = listOf("*QuarkusClassLoader")
        destinationFile =
            layout.buildDirectory
                .file("jacoco/jacoco-quarkus.exec")
                .get()
                .asFile
    }
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
    options.compilerArgs.add("-parameters")
}

// Fixes JvmMockKGateway error when using mockk + quarkusTest
tasks.withType<io.quarkus.gradle.tasks.QuarkusTest> {
    jvmArgs = jvmArgs.plus("-Djdk.attach.allowAttachSelf")
}

tasks.withType<Jar>().configureEach {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}
