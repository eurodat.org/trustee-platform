#!/usr/bin/env bash
set -e # Set -u later, since we have to check for possible image credentials

PROGRAM_NAME=${0##*/}

help_text () {
cat << EOF
Usage: $PROGRAM_NAME -o upload -s SRC-LOCATION -d DEST-LOCATION -i IMAGE_ID [-nda|-nsh|-ndh]
Usage: $PROGRAM_NAME -o delete -d DEST-LOCATION [-nda|-ndh|-g]
Options:
-nda | --no-dest-auth:     Disables destination registry authentication (no gcp token passed)
-nsh | --no-source-https:  Skips TLS verification and also allows insecure http connection for source registry
-ndh | --no-dest-https:    Skips TLS verification and also allows insecure http connection for source registry
-g   | --gcr-delete:       Uses gcloud instead of skopeo for image deletion (required for GCP registry)
$PROGRAM_NAME uses skopeo to copy the image from SRC-LOCATION to DEST-LOCATION or delete it at SRC-LOCATION.
If the source registry requires credentials, they can be passed via the environment variables IMAGE_CRED_USERNAME and IMAGE_CRED_PASSWORD.
EOF
}

# Set default values
NO_DEST_AUTH=false
NO_SOURCE_HTTPS=false
NO_DEST_HTTPS=false
GCR_DELETE=false
SRC_USERNAME="${IMAGE_CRED_USERNAME:-""}"
SRC_PASSWORD="${IMAGE_CRED_PASSWORD:-""}"

set -u

while [[ "$#" -gt 0 ]]; do
  case "$1" in
    -o|--operation)
        OPERATION="$2"
        shift 2 ;;
    -nda|--no-dest-auth)
        NO_DEST_AUTH=true
        shift 1 ;;
    -nsh|--no-source-https)
        NO_SOURCE_HTTPS=true
        shift 1 ;;
    -ndh|--no-dest-https)
        NO_DEST_HTTPS=true
        shift 1 ;;
    -s|--source)
        SOURCE="$2"
        shift 2 ;;
    -d|--dest)
        DEST="$2"
        shift 2 ;;
    -i|--image-id)
        IMAGE_ID="$2"
        shift 2 ;;
    -g|--gcr-delete)
        GCR_DELETE=true
        shift 1;;
    -h|--help)
        help_text
        exit 0;;
    *)
        echo "Unknown parameter passed: $1"
        help_text
        exit 1
        ;;
  esac
done

SKOPEO_COPY_ADDITIONAL_ARGS=""
SKOPEO_DELETE_ADDITIONAL_ARGS=""
if [[ $NO_DEST_AUTH == false ]]; then
  TOKEN=$(gcloud auth print-access-token)
  SKOPEO_COPY_ADDITIONAL_ARGS+=" --dest-registry-token $TOKEN"
  SKOPEO_DELETE_ADDITIONAL_ARGS+=" --registry-token $TOKEN"
  SKOPEO_INSPECT_ADDITIONAL_ARGS+=" --registry-token $TOKEN"
fi

if [[ -n "$SRC_USERNAME" && -n "$SRC_PASSWORD" ]]; then
  SKOPEO_COPY_ADDITIONAL_ARGS+=" --src-username $SRC_USERNAME --src-password $SRC_PASSWORD"
fi

if [[ $NO_SOURCE_HTTPS == true ]]; then
  SKOPEO_COPY_ADDITIONAL_ARGS+=" --src-tls-verify=false"
fi
if [[ $NO_DEST_HTTPS == true ]]; then
  SKOPEO_COPY_ADDITIONAL_ARGS+=" --dest-tls-verify=false"
  SKOPEO_DELETE_ADDITIONAL_ARGS+=" --tls-verify=false"
  SKOPEO_INSPECT_ADDITIONAL_ARGS+=" --tls-verify=false"
fi

if [[ $OPERATION == "delete" ]]; then
  if [[ $GCR_DELETE == "true" ]]; then
    # Because skopeo always resolves the image to a digest and a digest can not be deleted as long as there is a tag attached to it, we can
    # not use skopeo to delete the image in the google container registry. We have to use gcloud directly.
    gcloud artifacts docker images delete "$DEST"
  else
    skopeo delete "docker://$DEST" $SKOPEO_DELETE_ADDITIONAL_ARGS
  fi
elif [[ $OPERATION == "upload" ]]; then
  skopeo copy \
  "docker://$SOURCE" \
  "docker://$DEST" \
  $SKOPEO_COPY_ADDITIONAL_ARGS
  IMAGE_DETAILS="$(skopeo inspect "docker://$DEST" $SKOPEO_INSPECT_ADDITIONAL_ARGS)"
  IMAGE_NAME="$(echo "$IMAGE_DETAILS" | jq -r '.Name')"
  IMAGE_DIGEST="$(echo "$IMAGE_DETAILS" | jq -r '.Digest')"
  kubectl create configmap "image-location-$IMAGE_ID" --from-literal=image-location="$IMAGE_NAME@$IMAGE_DIGEST"
else
  echo "Unknown operation $OPERATION"
  exit 1
fi

