package org.eurodat.database.service

import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.ws.rs.ClientErrorException
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.WebApplicationException
import jakarta.ws.rs.core.Response
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.AppServiceClient
import org.eurodat.client.appservice.model.AppResponse
import org.eurodat.client.database.SafeDepositDatabaseResponse
import org.eurodat.database.SAFEDEPOSIT_SCHEMA
import org.eurodat.database.model.DatabaseDeleteRequest
import org.eurodat.database.model.DatabaseInitRequest
import java.sql.SQLException

@ApplicationScoped
class SafeDepositService {
    @Inject
    private lateinit var provisionDatabaseHelperService: ProvisionDatabaseHelperService

    @RestClient
    private lateinit var appServiceClient: AppServiceClient

    fun createSafeDepositDatabase(
        appId: String,
        clientId: String,
    ): Uni<SafeDepositDatabaseResponse> =
        appServiceClient
            .findApp(appId)
            .onFailure { it is WebApplicationException && it.response.status == 404 }
            .transform { NotFoundException(it.localizedMessage) }
            .invoke { _ -> provisionDatabaseHelperService.createSafedepositRole(appId, clientId) }
            .map { provisionDatabaseHelperService.initDatabase(initRequest(appId, clientId, it)) }
            .onFailure { it is SQLException && it.sqlState == "42P04" }
            .transform { ClientErrorException("Database already exists", Response.Status.CONFLICT) }
            .replaceWith(SafeDepositDatabaseResponse("Database created successfully"))

    private fun initRequest(
        appId: String,
        clientId: String,
        app: AppResponse,
    ): DatabaseInitRequest {
        val roleExternal = "safedeposit_${appId.lowercase()}_${clientId}_external"
        val roleInternal = "safedeposit_${appId.lowercase()}_internal"
        val safedepositGrantDDL =
            app.safeDepositDDL +
                """
                        |GRANT USAGE ON SCHEMA $SAFEDEPOSIT_SCHEMA TO $roleExternal;
                        |GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA $SAFEDEPOSIT_SCHEMA to $roleExternal;
                        |GRANT USAGE ON SCHEMA $SAFEDEPOSIT_SCHEMA TO $roleInternal;
                        |GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA $SAFEDEPOSIT_SCHEMA to $roleInternal;
                """.trimMargin()
        return DatabaseInitRequest(
            "safedeposit_${appId.lowercase()}_$clientId",
            safedepositGrantDDL,
            listOf("safedeposit"),
        )
    }

    fun deleteSafeDepositDatabase(
        appId: String,
        clientId: String,
    ): Uni<Unit> =
        Uni
            .createFrom()
            .item { provisionDatabaseHelperService.deleteDatabase(DatabaseDeleteRequest("safedeposit_${appId}_$clientId")) }
            .invoke { _ -> provisionDatabaseHelperService.deleteSafeDepositRole(appId, clientId) }
            .onFailure { it is SQLException && it.sqlState == "3D000" }
            .recoverWithNull()
            .map { }
}
