package org.eurodat.database.model

import io.smallrye.config.ConfigMapping
import io.smallrye.config.WithConverter
import org.eclipse.microprofile.config.spi.Converter
import org.eurodat.database.convertPostgresUrlFromConfigProperty

@ConfigMapping(prefix = "provisiondb")
interface ProvisionDbConfig {
    @WithConverter(PostgresUrlConverter::class)
    fun url(): String

    fun username(): String

    fun password(): String

    class PostgresUrlConverter : Converter<String> {
        override fun convert(value: String): String = convertPostgresUrlFromConfigProperty(value)
    }
}
