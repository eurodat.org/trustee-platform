package org.eurodat.database.resource

import io.quarkus.security.Authenticated
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.MediaType
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eurodat.client.database.SafeDepositDatabaseRequest
import org.eurodat.client.database.SafeDepositDatabaseResponse
import org.eurodat.database.service.SafeDepositService
import org.jboss.resteasy.reactive.ResponseStatus
import org.jboss.resteasy.reactive.RestPath

@Path("/database-service")
@Authenticated
class SafeDepositResource {
    @Inject
    internal lateinit var jwt: JsonWebToken

    @Inject
    internal lateinit var safeDepositService: SafeDepositService

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    fun createSafeDepositDatabase(request: SafeDepositDatabaseRequest): Uni<SafeDepositDatabaseResponse> {
        val clientId = jwt.getClaim<String>("client_id")
        return safeDepositService.createSafeDepositDatabase(request.appId, clientId)
    }

    @DELETE
    @Path("/{appId}")
    @ResponseStatus(204)
    @Authenticated
    fun deleteSafeDepositDatabase(
        @RestPath appId: String,
    ): Uni<Unit> {
        val clientId = jwt.getClaim<String>("client_id")
        return safeDepositService.deleteSafeDepositDatabase(appId, clientId).map { }
    }
}
