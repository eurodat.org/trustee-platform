package org.eurodat.database.resource

import io.quarkus.security.Authenticated
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.infrastructure.Infrastructure
import io.smallrye.mutiny.vertx.MutinyHelper
import io.vertx.core.Vertx
import jakarta.inject.Inject
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.WebApplicationException
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.AppServiceClient
import org.eurodat.client.credential.DatabaseCredentialClient
import org.eurodat.client.database.ClientRoleCreationRequest
import org.eurodat.client.database.TransactionDatabaseRequest
import org.eurodat.client.transactionservice.UserClient
import org.eurodat.database.INPUT_SCHEMA
import org.eurodat.database.INTERMEDIATE_SCHEMA
import org.eurodat.database.OUTPUT_SCHEMA
import org.eurodat.database.dbNameFromTransactionId
import org.eurodat.database.model.DatabaseDeleteRequest
import org.eurodat.database.model.DatabaseInitRequest
import org.eurodat.database.service.ProvisionDatabaseHelperService
import java.util.stream.Collectors

@Path("/transaction-database")
@Authenticated
class TransactionDatabaseResource {
    @Inject
    internal lateinit var provisionDatabaseHelperService: ProvisionDatabaseHelperService

    @RestClient
    internal lateinit var appServiceClient: AppServiceClient

    @RestClient
    internal lateinit var credService: DatabaseCredentialClient

    @RestClient
    internal lateinit var userService: UserClient

    @POST
    fun createTransactionDatabase(request: TransactionDatabaseRequest): Uni<Unit> {
        val currentContext = Vertx.currentContext()
        return Uni
            .combine()
            .all()
            .unis(
                appServiceClient
                    .findApp(request.appId)
                    .onFailure { it is WebApplicationException && it.response.status == 404 }
                    .transform { NotFoundException("App id not found") },
                userService.getClientSelectorMapping(),
            ).asTuple()
            .call { tuple ->
                val app = tuple.item1
                val clientMappings = tuple.item2
                val roleCreationExternal = mutableListOf("")
                val participantsFromRequest = mutableListOf("")
                participantsFromRequest += request.consumer
                participantsFromRequest += request.provider
                for ((client, selector) in clientMappings) {
                    if (selector in participantsFromRequest) {
                        val roleExternalTransaction = "transaction_${request.transactionId}_${client.lowercase()}_external"
                        val dbRoleExt = "db_${client.lowercase()}_external"
                        val ddlStatementForTransactionRole =
                            "do\n$$\nbegin\n  if not exists (select * from pg_roles where rolname = '$roleExternalTransaction') then\n" +
                                "       create role $roleExternalTransaction NOLOGIN;\n  end if;\nend\n$$\n;" +
                                "GRANT USAGE ON SCHEMA $INPUT_SCHEMA TO $roleExternalTransaction;" +
                                "GRANT SELECT, INSERT ON ALL TABLES IN SCHEMA $INPUT_SCHEMA TO $roleExternalTransaction;" +
                                "GRANT USAGE ON SCHEMA $OUTPUT_SCHEMA TO $roleExternalTransaction;" +
                                "GRANT SELECT ON ALL TABLES IN SCHEMA $OUTPUT_SCHEMA TO $roleExternalTransaction;" +
                                "GRANT $roleExternalTransaction TO $dbRoleExt;"
                        roleCreationExternal.add(ddlStatementForTransactionRole)
                        val rowBaseSecurityPolicies =
                            @Suppress("ktlint:standard:max-line-length")
                            app.tableSecurityMapping.map { mapping ->
                                "create policy ext_${mapping.tableName}_${client}_${request.transactionId} on output.${mapping.tableName} to $roleExternalTransaction " +
                                    "using (${mapping.rowBaseOutputSecurityColumn} = \'${selector}\');"
                            }
                        roleCreationExternal.add(rowBaseSecurityPolicies.joinToString(""))
                    }
                }
                val roleInternalSafeDeposit = "safedeposit_${request.appId.lowercase()}_internal"
                val roleInternalTransaction = "transaction_${request.transactionId}_internal"
                val ddlStatementForTransactionRole =
                    "do\n$$\nbegin\n  if not exists (select * from pg_roles where rolname = '$roleInternalTransaction') then\n" +
                        "       create role $roleInternalTransaction NOLOGIN INHERIT;\n  end if;\nend\n$$\n;" +
                        "GRANT USAGE ON SCHEMA $INPUT_SCHEMA TO $roleInternalTransaction;" +
                        "GRANT SELECT ON ALL TABLES IN SCHEMA $INPUT_SCHEMA TO $roleInternalTransaction;" +
                        "GRANT USAGE ON SCHEMA $INTERMEDIATE_SCHEMA TO $roleInternalTransaction;" +
                        "GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA $INTERMEDIATE_SCHEMA TO $roleInternalTransaction;" +
                        "GRANT USAGE ON SCHEMA $OUTPUT_SCHEMA TO $roleInternalTransaction;" +
                        "GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA $OUTPUT_SCHEMA TO $roleInternalTransaction;" +
                        "do\n$$\nbegin\n  if exists (select * from pg_roles where rolname = '$roleInternalSafeDeposit') then\n" +
                        "      GRANT $roleInternalSafeDeposit TO $roleInternalTransaction;\n  end if;\nend\n$$\n;"

                val internalPolicy =
                    app.tableSecurityMapping
                        .stream()
                        .map { mapping -> mapping.tableName }
                        .map { tableName ->
                            "create policy internal_${tableName}_${request.transactionId} on output.$tableName" +
                                " to $roleInternalTransaction using (true);"
                        }.collect(Collectors.toList())

                val dbRequest =
                    DatabaseInitRequest(
                        name = dbNameFromTransactionId(request.transactionId),
                        ddl =
                            app.transactionDDL +
                                roleCreationExternal.joinToString(
                                    "",
                                ) + ddlStatementForTransactionRole + internalPolicy.joinToString(""),
                        schemas = listOf("input", "intermediate", "output", "safedeposit"),
                    )
                Uni
                    .createFrom()
                    .item { provisionDatabaseHelperService.initDatabase(dbRequest) }
                    .runSubscriptionOn(Infrastructure.getDefaultWorkerPool())
            }.emitOn(MutinyHelper.executor(currentContext))
            .chain { _ ->
                credService.registerTransactionConnection(request.transactionId)
            }
    }

    @POST
    @Path("/roles")
    fun createExternalClientRole(request: ClientRoleCreationRequest): Uni<Unit> =
        Uni
            .createFrom()
            .item { provisionDatabaseHelperService.createExternalRole(request.client) }
            .runSubscriptionOn(Infrastructure.getDefaultWorkerPool())

    @DELETE
    @Path("/roles/{clientId}")
    fun deleteExternalClientRole(
        @PathParam("clientId") clientId: String,
    ): Uni<Unit> =
        Uni
            .createFrom()
            .item { provisionDatabaseHelperService.deleteExternalRole(clientId) }
            .runSubscriptionOn(Infrastructure.getDefaultWorkerPool())

    @DELETE
    fun deleteTransactionDatabase(request: TransactionDatabaseRequest): Uni<Unit> =
        credService
            .deleteInternalConnection(request.transactionId)
            .call { _ ->
                val databaseName = "database_${request.transactionId}"
                val deleteRequest = DatabaseDeleteRequest(databaseName)
                Uni
                    .createFrom()
                    .item { provisionDatabaseHelperService.deleteDatabase(deleteRequest) }
                    .runSubscriptionOn(Infrastructure.getDefaultWorkerPool())
            }.chain { _ ->
                Uni.createFrom().item { provisionDatabaseHelperService.deleteTransactionRoles(request.transactionId) }
            }
}
