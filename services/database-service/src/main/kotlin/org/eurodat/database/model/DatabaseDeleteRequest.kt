package org.eurodat.database.model

data class DatabaseDeleteRequest(
    val name: String,
)
