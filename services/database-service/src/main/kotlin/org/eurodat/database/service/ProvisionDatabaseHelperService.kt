package org.eurodat.database.service

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.ws.rs.BadRequestException
import org.eurodat.database.model.DatabaseDeleteRequest
import org.eurodat.database.model.DatabaseInitRequest
import org.eurodat.database.model.ProvisionDbConfig
import org.eurodat.database.safeSqlString
import org.postgresql.util.PSQLException
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet

@ApplicationScoped
class ProvisionDatabaseHelperService {
    @Inject
    internal lateinit var config: ProvisionDbConfig

    private fun createSchema(
        superUserConnection: Connection,
        schemaNames: List<String>,
        ddlSql: String,
    ) {
        for (schemaName in schemaNames) {
            // Cannot use prepared statement for schema creation
            superUserConnection
                .createStatement()
                .executeUpdate("CREATE SCHEMA IF NOT EXISTS ${schemaName.safeSqlString()}") // nosemgrep
        }

        // impossible to verify ddlSql at this point
        superUserConnection.prepareStatement(ddlSql).executeUpdate() // nosemgrep
    }

    fun initDatabase(body: DatabaseInitRequest) {
        val dbName = body.name

        // Cannot use prepared statement for database creation
        DriverManager.getConnection(config.url() + "postgres", config.username(), config.password()).use { connection ->
            connection.createStatement().use {
                it.executeUpdate("CREATE DATABASE ${dbName.safeSqlString()}") // nosemgrep
            }
        }

        DriverManager.getConnection(config.url() + dbName, config.username(), config.password()).use {
            // nosemgrep because it's just a function call
            createSchema(it, body.schemas, body.ddl) // nosemgrep
        }
    }

    fun deleteDatabase(body: DatabaseDeleteRequest): String {
        val dbName = body.name
        // Cannot use prepared statement for database deletion
        DriverManager.getConnection(config.url() + "postgres", config.username(), config.password()).use { connection ->
            connection.createStatement().use {
                it.executeUpdate("DROP DATABASE ${dbName.safeSqlString()} WITH (FORCE)") // nosemgrep
            }
        }

        return "Database dropped successfully."
    }

    fun createExternalRole(client: String) {
        val dbRoleExt = "db_${client.lowercase()}_external".safeSqlString()

        val ddlStatementForRole = "CREATE ROLE $dbRoleExt NOLOGIN INHERIT;"

        DriverManager.getConnection(config.url() + "postgres", config.username(), config.password()).use { connection ->
            connection.createStatement().use {
                it.executeUpdate(ddlStatementForRole) // nosemgrep
            }
        }
    }

    fun createSafedepositRole(
        app: String,
        client: String,
    ): String {
        DriverManager.getConnection(config.url() + "postgres", config.username(), config.password()).use { connection ->
            connection.createStatement().use {
                // create safe deposit roles
                // Cannot use prepared statement for role creation
                val dbRoleSafedepositExt =
                    "safedeposit_${app.lowercase()}_${client.lowercase()}_external".safeSqlString()
                val dbRoleSafedepositInt = "safedeposit_${app.lowercase()}_internal".safeSqlString()
                val ddlStatementForSafedeposit =
                    "do\n$$\nbegin\n  if not exists (select * from pg_roles where rolname = '$dbRoleSafedepositExt') then\n" +
                        "       create role $dbRoleSafedepositExt NOLOGIN;\n  end if;\nend\n$$\n;" +
                        "do\n$$\nbegin\n  if not exists (select * from pg_roles where rolname = '$dbRoleSafedepositInt') then\n" +
                        "       create role $dbRoleSafedepositInt NOLOGIN;\n  end if;\nend\n$$\n;"
                it.executeUpdate(ddlStatementForSafedeposit) // nosemgrep

                // grant external client permissions to safedeposit roles
                val dbRoleExt = "db_${client.lowercase()}_external".safeSqlString()

                val ddlStatementForRole =
                    "do\n$$\nbegin\n  if exists (select * from pg_roles where rolname = '$dbRoleExt') then\n" +
                        "   grant $dbRoleSafedepositExt to $dbRoleExt;\n  end if;\nend\n$$\n;"
                it.executeUpdate(ddlStatementForRole) // nosemgrep
            }
        }

        return "Database roles created successfully"
    }

    fun deleteExternalRole(clientId: String) {
        DriverManager.getConnection(config.url() + "postgres", config.username(), config.password()).use { connection ->
            connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).use {
                val dbRoleExt = "db_${clientId.lowercase()}_external"
                val ddlStatementForRole = "drop role if exists ${dbRoleExt.safeSqlString()}; "
                it.executeUpdate(ddlStatementForRole) // nosemgrep
            }
        }
    }

    fun deleteSafeDepositRole(
        app: String,
        client: String,
    ): Boolean {
        DriverManager.getConnection(config.url() + "postgres", config.username(), config.password()).use { connection ->
            connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).use {
                var ddlStatementForRole = ""
                val dbRoleSafedepositExt = "safedeposit_${app.lowercase().safeSqlString()}_${
                    client.lowercase()
                }_external"
                ddlStatementForRole = ddlStatementForRole.plus("drop role if exists ${dbRoleSafedepositExt.safeSqlString()}; ")
                // Cannot use prepared statement for role deletion
                try {
                    it.executeUpdate(ddlStatementForRole)
                } catch (e: PSQLException) {
                    when (e.sqlState.toString()) {
                        "2BP01" -> throw BadRequestException(e) // Objects depend on one of the roles which were supposed to be dropped
                        else -> throw e
                    }
                }
            }
        }
        return true
    }

    fun deleteTransactionRoles(transactionId: String) {
        DriverManager.getConnection(config.url() + "postgres", config.username(), config.password()).use {
            val getTransactionRolesStatement =
                it.prepareStatement(
                    "SELECT * FROM pg_roles WHERE pg_roles.rolname LIKE ?;",
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                )
            getTransactionRolesStatement.setString(1, "transaction_$transactionId%")
            val listWithRoles = getTransactionRolesStatement.executeQuery()
            val listOfAllTransactionRoles = mutableListOf<String>()
            while (listWithRoles.next()) {
                val roleName = listWithRoles.getString("rolname")
                listOfAllTransactionRoles.add(roleName)
            }
            listWithRoles.close()

            for (role in listOfAllTransactionRoles) {
                // Cannot use prepared statement for role deletion
                val dbRoleTransactionExt =
                    "DROP ROLE IF EXISTS ${role.safeSqlString()};"
                it
                    .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)
                    .executeUpdate(dbRoleTransactionExt)
            }
        }
    }
}
