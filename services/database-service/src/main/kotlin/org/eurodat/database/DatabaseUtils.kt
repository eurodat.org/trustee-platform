package org.eurodat.database

const val INPUT_SCHEMA = "input"
const val INTERMEDIATE_SCHEMA = "intermediate"
const val OUTPUT_SCHEMA = "output"
const val SAFEDEPOSIT_SCHEMA = "safedeposit"

fun dbNameFromTransactionId(transactionId: String) = "database_" + transactionId

fun convertPostgresUrlFromConfigProperty(urlToBeConverted: String) =
    urlToBeConverted.replace("vertx-reactive", "jdbc").substringBeforeLast("/") + "/"

private fun containsMaliciousCharacters(name: String) = !name.matches("^[a-zA-Z0-9_]+$".toRegex())

fun String.safeSqlString(): String {
    if (containsMaliciousCharacters(this)) {
        throw Exception("Malicious characters found in sql parameter string")
    }
    return this
}
