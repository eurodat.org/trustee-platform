package org.eurodat.database.model

data class DatabaseInitRequest(
    val name: String,
    val ddl: String,
    val schemas: List<String>,
)
