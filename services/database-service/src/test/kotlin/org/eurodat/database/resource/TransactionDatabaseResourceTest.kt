package org.eurodat.database.resource

import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.TestProfile
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.NotFoundException
import org.eurodat.client.appservice.model.AppResponse
import org.eurodat.client.appservice.model.TableSecurityMapping
import org.eurodat.client.appservice.model.WorkflowDefinition
import org.eurodat.client.database.TransactionDatabaseRequest
import org.eurodat.client.transactionservice.ClientSelectorMapping
import org.eurodat.database.model.DatabaseDeleteRequest
import org.eurodat.database.model.DatabaseInitRequest
import org.eurodat.testcommon.DevServicesDisabledProfile
import org.jboss.resteasy.reactive.ClientWebApplicationException
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestProfile(DevServicesDisabledProfile::class)
class TransactionDatabaseResourceTest {
    private var transactionDatabaseResource: TransactionDatabaseResource =
        TransactionDatabaseResource().also {
            it.provisionDatabaseHelperService = mockk()
            it.appServiceClient = mockk()
            it.credService = mockk()
            it.userService = mockk()
            every { it.appServiceClient.findApp("appId") } returns
                uni {
                    AppResponse(
                        "Test",
                        "",
                        "",
                        "",
                        listOf(TableSecurityMapping("test", "output")),
                        mutableListOf(WorkflowDefinition()),
                    )
                }
            every { it.appServiceClient.findApp("wrongId") } returns
                Uni.createFrom().failure(ClientWebApplicationException("404 Not Found", 404))

            every { it.provisionDatabaseHelperService.initDatabase(any()) } returns Unit
            every { it.provisionDatabaseHelperService.deleteDatabase(any()) } returns ""
            every { it.provisionDatabaseHelperService.deleteTransactionRoles(any()) } returns Unit
            every {
                it.provisionDatabaseHelperService.createSafedepositRole(
                    any(),
                    any(),
                )
            } returns ("Database roles created successfully")
            every { it.provisionDatabaseHelperService.deleteSafeDepositRole(any(), any()) } returns (true)
            every { it.credService.registerTransactionConnection(any()) } returns uni { }
            every { it.credService.deleteInternalConnection(any()) } returns uni { }
            every { it.userService.getClientSelectorMapping() } returns
                uni {
                    listOf(
                        ClientSelectorMapping(
                            "foo",
                            "Selector_foo",
                        ),
                    )
                }
        }

    private fun <T> uni(func: () -> T?): Uni<T> = Uni.createFrom().item(func)

    @Test
    @RunOnVertxContext
    fun testCreateDatabase(asserter: UniAsserter) {
        asserter.assertThat(
            {
                transactionDatabaseResource.createTransactionDatabase(
                    TransactionDatabaseRequest("appId", "123456", listOf("Selector_foo")),
                )
            },
            {
                val expectedDbRequest =
                    DatabaseInitRequest(
                        name = "database_123456",
                        ddl =
                            """
                            do
                            $$
                            begin
                              if not exists (select * from pg_roles where rolname = 'transaction_123456_foo_external') then
                                   create role transaction_123456_foo_external NOLOGIN;
                              end if;
                            end
                            $$
                            ;GRANT USAGE ON SCHEMA input TO transaction_123456_foo_external;GRANT SELECT, INSERT ON ALL TABLES IN SCHEMA input TO transaction_123456_foo_external;GRANT USAGE ON SCHEMA output TO transaction_123456_foo_external;GRANT SELECT ON ALL TABLES IN SCHEMA output TO transaction_123456_foo_external;GRANT transaction_123456_foo_external TO db_foo_external;create policy ext_output_foo_123456 on output.output to transaction_123456_foo_external using (test = 'Selector_foo');do
                            $$
                            begin
                              if not exists (select * from pg_roles where rolname = 'transaction_123456_internal') then
                                   create role transaction_123456_internal NOLOGIN INHERIT;
                              end if;
                            end
                            $$
                            ;GRANT USAGE ON SCHEMA input TO transaction_123456_internal;GRANT SELECT ON ALL TABLES IN SCHEMA input TO transaction_123456_internal;GRANT USAGE ON SCHEMA intermediate TO transaction_123456_internal;GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA intermediate TO transaction_123456_internal;GRANT USAGE ON SCHEMA output TO transaction_123456_internal;GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA output TO transaction_123456_internal;do
                            $$
                            begin
                              if exists (select * from pg_roles where rolname = 'safedeposit_appid_internal') then
                                  GRANT safedeposit_appid_internal TO transaction_123456_internal;
                              end if;
                            end
                            $$
                            ;create policy internal_output_123456 on output.output to transaction_123456_internal using (true);
                            """.trimIndent(),
                        schemas = listOf("input", "intermediate", "output", "safedeposit"),
                    )
                verify { transactionDatabaseResource.provisionDatabaseHelperService.initDatabase(expectedDbRequest) }
                val testInternalClient: Uni<Unit> =
                    transactionDatabaseResource.credService.registerTransactionConnection("123456")
                try {
                    testInternalClient.await()
                    println("Registration successful")
                } catch (throwable: Throwable) {
                    println("Registration failed: $throwable")
                }
            },
        )
    }

    @Test
    @RunOnVertxContext
    fun testDeleteDatabase(asserter: UniAsserter) {
        asserter.assertThat({
            transactionDatabaseResource.deleteTransactionDatabase(TransactionDatabaseRequest("appId", "123456", listOf("Selector_foo")))
        }, {
            val expectedDeleteRequest = DatabaseDeleteRequest("database_123456")
            val slot = slot<DatabaseDeleteRequest>()
            verify { transactionDatabaseResource.provisionDatabaseHelperService.deleteDatabase(capture(slot)) }
            Assertions.assertEquals(expectedDeleteRequest, slot.captured)
            verify { transactionDatabaseResource.provisionDatabaseHelperService.deleteTransactionRoles("123456") }
        })
    }

    @Test
    @RunOnVertxContext
    fun testCreateAppIdNotFound(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { transactionDatabaseResource.createTransactionDatabase(TransactionDatabaseRequest("wrongId", "123456", listOf("Selector_foo"))) },
            NotFoundException::class.java,
        )
    }
}
