package org.eurodat.database

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class TestDatabaseUtils {
    @Test
    fun testDbNameFromTransactionId() {
        val dummyTransactionId = "42_123"
        val expectedResult = "database_42_123"
        Assertions.assertEquals(dbNameFromTransactionId(dummyTransactionId), expectedResult)
    }

    @Test
    fun testConvertPostgresUrlFromConfigProperty() {
        val dbUrl = "vertx-reactive:dummysql://eurodat-dummysql.transaction-plane.svc:42/dummysql"
        val expectedUrl = "jdbc:dummysql://eurodat-dummysql.transaction-plane.svc:42/"
        Assertions.assertEquals(convertPostgresUrlFromConfigProperty(dbUrl), expectedUrl)
    }
}
