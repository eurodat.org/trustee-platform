package org.eurodat.database.resource

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.TestProfile
import io.quarkus.test.junit.mockito.MockitoConfig
import io.quarkus.test.security.TestSecurity
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eurodat.client.database.SafeDepositDatabaseRequest
import org.eurodat.client.database.SafeDepositDatabaseResponse
import org.eurodat.database.service.SafeDepositService
import org.eurodat.testcommon.DevServicesDisabledProfile
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito

@QuarkusTest
@TestProfile(DevServicesDisabledProfile::class)
@TestSecurity(user = "testUser")
class SafeDepositResourceTest {
    @InjectMock
    @MockitoConfig(convertScopes = true)
    private lateinit var jwt: JsonWebToken

    @InjectMock
    @MockitoConfig(convertScopes = true)
    private lateinit var safeDepositService: SafeDepositService

    @Inject
    private lateinit var safeDepositResource: SafeDepositResource

    private val testSafeDepositDatabaseRequest = SafeDepositDatabaseRequest("appId")

    @Test
    fun testCreateSafeDepositDatabase() {
        Mockito.`when`(jwt.getClaim<String>("client_id")).thenReturn("testUser")
        Mockito
            .`when`(safeDepositService.createSafeDepositDatabase("appId", "testUser"))
            .thenReturn(Uni.createFrom().item(SafeDepositDatabaseResponse("Database created successfully")))

        Assertions.assertEquals(
            safeDepositResource.createSafeDepositDatabase(testSafeDepositDatabaseRequest).await().indefinitely(),
            SafeDepositDatabaseResponse("Database created successfully"),
        )
        Mockito.verify(safeDepositService).createSafeDepositDatabase("appId", "testUser")
    }

    @Test
    fun testDeleteSafeDepositDatabase() {
        Mockito.`when`(jwt.getClaim<String>("client_id")).thenReturn("testUser")
        Mockito
            .`when`(safeDepositService.deleteSafeDepositDatabase("appId", "testUser"))
            .thenReturn(Uni.createFrom().item(Unit))

        Assertions.assertDoesNotThrow {
            safeDepositResource.deleteSafeDepositDatabase("appId").await().indefinitely()
        }

        Mockito.verify(safeDepositService).deleteSafeDepositDatabase("appId", "testUser")
    }
}
