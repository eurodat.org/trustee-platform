package org.eurodat.database

import org.testcontainers.containers.PostgreSQLContainer

class DefaultPostgresContainer : PostgreSQLContainer<DefaultPostgresContainer?>(IMAGE_VERSION) {
    companion object {
        private const val IMAGE_VERSION = "postgres:15"
        private var container: DefaultPostgresContainer? = null
        val instance: DefaultPostgresContainer?
            get() {
                if (container == null) {
                    container =
                        DefaultPostgresContainer()
                            .withDatabaseName("postgres")
                            ?.withUsername("superuser")
                            ?.withPassword("pwd")
                }
                return container
            }
    }
}
