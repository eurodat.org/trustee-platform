package org.eurodat.database.service

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.MockitoConfig
import io.smallrye.config.SmallRyeConfig
import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.inject.Produces
import jakarta.inject.Inject
import jakarta.ws.rs.BadRequestException
import junit.framework.TestCase.assertTrue
import org.eclipse.microprofile.config.Config
import org.eurodat.database.DefaultPostgresContainer
import org.eurodat.database.model.DatabaseDeleteRequest
import org.eurodat.database.model.DatabaseInitRequest
import org.eurodat.database.model.ProvisionDbConfig
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.mockito.Mockito
import java.sql.DriverManager
import java.sql.SQLException
import java.sql.Statement

@QuarkusTest
@TestMethodOrder(
    MethodOrderer.OrderAnnotation::class,
)
class ProvisionTransactionDatabaseIntegrationTest {
    init {
        DefaultPostgresContainer.instance?.start()
    }

    private val superUser = "superuser"
    private val superPassword = "pwd"
    private val jdbcUrlFull =
        DefaultPostgresContainer.instance?.jdbcUrl ?: throw RuntimeException("Cannot connect to test container")
    private val jdbcUrlFinal = jdbcUrlFull.substringBeforeLast("/") + "/"
    private var request =
        DatabaseInitRequest(
            name = "example_database",
            ddl =
                "CREATE TABLE input.table_input (column1 INTEGER, column2 INTEGER); " +
                    "CREATE TABLE intermediate.table_intermediate (column1 INTEGER, column2 INTEGER); " +
                    "CREATE TABLE output.table_output (column1 INTEGER, column2 INTEGER); ",
            schemas = listOf("input", "intermediate", "output"),
        )

    private fun superUserStatement(): Statement =
        DriverManager.getConnection(jdbcUrlFinal + request.name, superUser, superPassword).createStatement()

    @Inject
    internal lateinit var config: Config

    @Produces
    @ApplicationScoped
    @io.quarkus.test.Mock
    fun provisionDbConfig(): ProvisionDbConfig = config.unwrap(SmallRyeConfig::class.java).getConfigMapping(ProvisionDbConfig::class.java)

    @Inject
    internal lateinit var provisionDatabaseHelperService: ProvisionDatabaseHelperService

    @InjectMock
    @MockitoConfig(convertScopes = true)
    internal lateinit var provisionDbConfig: ProvisionDbConfig

    @BeforeEach
    fun setupConfig() {
        Mockito.`when`(provisionDbConfig.url()).thenReturn(jdbcUrlFinal)
        Mockito.`when`(provisionDbConfig.username()).thenReturn(superUser)
        Mockito.`when`(provisionDbConfig.password()).thenReturn(superPassword)
    }

    @Test
    @Order(1)
    fun testInvalidDatabaseNameGetsRejected() {
        // provisionDatabaseHelperService.setConnectionParameters(jdbcUrlFinal, superUser, superPassword, "hardcoded")
        val requestInvalid =
            DatabaseInitRequest(
                name = "example_database SELECT * FROM table",
                ddl = "",
                schemas = listOf("input", "intermediate", "output"),
            )
        Assertions.assertThrows(Exception::class.java) {
            provisionDatabaseHelperService.initDatabase(requestInvalid)
        }
    }

    @Test
    @Order(2)
    fun testDatabaseCreation() {
        provisionDatabaseHelperService.initDatabase(request)
    }

    @Test
    @Order(3)
    fun testSuperUserCanReadFromSchema() {
        val resultSetSuperInput =
            superUserStatement()
                .executeQuery("SELECT count(*) from input.table_input; ")
        resultSetSuperInput.next()
        Assertions.assertEquals(0, resultSetSuperInput.getInt(1))
    }

    @Test
    @Order(4)
    fun createExternalClient() {
        provisionDatabaseHelperService.createExternalRole("client1")

        val resultSetSuperInput =
            superUserStatement()
                .executeQuery("SELECT count(*) from pg_roles where rolname = 'db_client1_external'; ")
        resultSetSuperInput.next()
        Assertions.assertEquals(1, resultSetSuperInput.getInt(1))
    }

    @Test
    @Order(5)
    fun testCreateSafeDepositRoleForDatabase() {
        provisionDatabaseHelperService.createSafedepositRole("app1", "client1")
        provisionDatabaseHelperService.createSafedepositRole("app2", "client1")

        val resultExternalSafeDeposit1 =
            superUserStatement()
                .executeQuery("SELECT count(*) from pg_roles where rolname = 'safedeposit_app1_client1_external'; ")
        resultExternalSafeDeposit1.next()
        Assertions.assertEquals(1, resultExternalSafeDeposit1.getInt(1))

        val resultExternalSafeDeposit3 =
            superUserStatement()
                .executeQuery("SELECT count(*) from pg_roles where rolname = 'safedeposit_app2_client1_external'; ")
        resultExternalSafeDeposit3.next()
        Assertions.assertEquals(1, resultExternalSafeDeposit3.getInt(1))

        val resultInternalSafeDeposit =
            superUserStatement()
                .executeQuery("SELECT count(*) from pg_roles where rolname = 'safedeposit_app1_internal'; ")
        resultInternalSafeDeposit.next()
        Assertions.assertEquals(1, resultInternalSafeDeposit.getInt(1))
    }

    @Test
    @Order(6)
    fun testNoDeleteRolesForDatabase() {
        provisionDatabaseHelperService.deleteSafeDepositRole("app1", "client1")
        provisionDatabaseHelperService.deleteSafeDepositRole("app2", "client1")
        val resultRolesWithClient =
            superUserStatement()
                .executeQuery("SELECT count(*) from pg_roles where rolname like '%client1%'; ")
        resultRolesWithClient.next()
        Assertions.assertEquals(1, resultRolesWithClient.getInt(1))
    }

    @Test
    @Order(7)
    fun testDeleteSafeDepositRolesForDatabase() {
        provisionDatabaseHelperService.deleteSafeDepositRole("app1", "client1")
        provisionDatabaseHelperService.deleteSafeDepositRole("app2", "client1")
        val resultRolesWithClient =
            superUserStatement()
                .executeQuery("SELECT count(*) from pg_roles where rolname like '%client1%'; ")
        resultRolesWithClient.next()
        Assertions.assertEquals(1, resultRolesWithClient.getInt(1))
    }

    @Test
    @Order(8)
    fun testDeleteExternalClient() {
        provisionDatabaseHelperService.deleteExternalRole("client1")
        val resultRolesWithClient =
            superUserStatement()
                .executeQuery("SELECT count(*) from pg_roles where rolname like '%client1%'; ")
        resultRolesWithClient.next()
        Assertions.assertEquals(0, resultRolesWithClient.getInt(1))
    }

    @Test
    @Order(8)
    fun testDeleteRolesForDatabaseNotAllowedWhenSomethingAttachedToRole() {
        superUserStatement().executeUpdate(
            "create schema testschema; create role safedeposit_app1_client1_external; " +
                " grant usage on schema testschema to safedeposit_app1_client1_external;",
        )
        Assertions.assertThrows(BadRequestException::class.java) {
            provisionDatabaseHelperService.deleteSafeDepositRole("app1", "client1")
            provisionDatabaseHelperService.deleteSafeDepositRole("app2", "client1")
        }
        val resultRolesWithClient =
            superUserStatement()
                .executeQuery("SELECT count(*) from pg_roles where rolname like 'safedeposit_app1_client1_external'; ")
        resultRolesWithClient.next()
        Assertions.assertEquals(1, resultRolesWithClient.getInt(1))
        superUserStatement().executeUpdate("drop schema testschema cascade;")
        superUserStatement().executeUpdate("drop role safedeposit_app1_client1_external;")
    }

    @Test
    @Order(9)
    fun testDeleteTransactionRoles() {
        val masterStatement = superUserStatement()
        with(masterStatement) { executeUpdate("create role transaction_654321_client1_internal NOLOGIN") }
        with(masterStatement) { executeUpdate("create role transaction_654321_client1_external NOLOGIN") }
        provisionDatabaseHelperService.deleteTransactionRoles("654321")
        val resultRolesWithClient =
            masterStatement
                .executeQuery("SELECT * FROM pg_roles WHERE TRIM(pg_roles.rolname) LIKE 'transaction_654321%';")
        Assertions.assertFalse(resultRolesWithClient.next(), "Result-set should be empty")
    }

    @Test
    @Order(10)
    fun testDeleteDatabase() {
        val deleteRequest = DatabaseDeleteRequest(request.name)
        assertTrue(provisionDatabaseHelperService.deleteDatabase(deleteRequest), true)

        // database is not accessible anymore
        Assertions.assertThrows(SQLException::class.java) {
            DriverManager.getConnection(jdbcUrlFinal + request.name, superUser, superPassword)
        }
    }
}
