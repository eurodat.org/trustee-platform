package org.eurodat.database.service

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.TestProfile
import io.quarkus.test.junit.mockito.MockitoConfig
import io.quarkus.test.security.TestSecurity
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import jakarta.ws.rs.ClientErrorException
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.WebApplicationException
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.AppServiceClient
import org.eurodat.client.appservice.model.AppResponse
import org.eurodat.client.appservice.model.TableSecurityMapping
import org.eurodat.client.appservice.model.WorkflowDefinition
import org.eurodat.client.database.SafeDepositDatabaseResponse
import org.eurodat.database.model.DatabaseDeleteRequest
import org.eurodat.database.model.DatabaseInitRequest
import org.eurodat.testcommon.DevServicesDisabledProfile
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doAnswer
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import java.sql.SQLException

@QuarkusTest
@TestProfile(DevServicesDisabledProfile::class)
@TestSecurity(user = "testUser")
@RunOnVertxContext
class SafeDepositServiceTest {
    @InjectMock
    private lateinit var provisionDatabaseHelperService: ProvisionDatabaseHelperService

    @InjectMock
    @MockitoConfig(convertScopes = true)
    @RestClient
    private lateinit var appServiceClient: AppServiceClient

    @Inject
    private lateinit var safeDepositService: SafeDepositService

    private val response =
        AppResponse(
            "Test",
            "",
            "",
            "",
            listOf(TableSecurityMapping("", "")),
            mutableListOf(WorkflowDefinition()),
        )

    private var responseInput =
        response.safeDepositDDL +
            """
GRANT USAGE ON SCHEMA safedeposit TO safedeposit_appid_testUser_external;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA safedeposit to safedeposit_appid_testUser_external;
GRANT USAGE ON SCHEMA safedeposit TO safedeposit_appid_internal;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA safedeposit to safedeposit_appid_internal;
            """.trimMargin()

    @Test
    fun testCreateSafeDepositDatabase(asserter: UniAsserter) {
        whenever(appServiceClient.findApp("appID")).thenReturn(Uni.createFrom().item(response))

        asserter.assertThat(
            { safeDepositService.createSafeDepositDatabase("appID", "testUser") },
            {
                Assertions.assertEquals(SafeDepositDatabaseResponse("Database created successfully"), it)
                verify(
                    provisionDatabaseHelperService,
                ).initDatabase(DatabaseInitRequest("safedeposit_appid_testUser", responseInput, listOf("safedeposit")))
                verify(provisionDatabaseHelperService).createSafedepositRole("appID", "testUser")
            },
        )
    }

    @Test
    fun testWrongIdCreateSafeDepositDatabase(asserter: UniAsserter) {
        whenever(appServiceClient.findApp("wrongID")).thenReturn(Uni.createFrom().failure(NotFoundException("404 Not Found")))
        asserter.assertFailedWith(
            { safeDepositService.createSafeDepositDatabase("wrongID", "testUser") },
            NotFoundException::class.java,
        )
    }

    @Test
    fun serverErrorAppServiceResultsInServerError(asserter: UniAsserter) {
        whenever(appServiceClient.findApp("appID")).thenReturn(Uni.createFrom().failure(WebApplicationException(500)))
        asserter.assertFailedWith(
            { safeDepositService.createSafeDepositDatabase("appID", "testUser") },
            { Assertions.assertTrue(it is WebApplicationException && it.response.status == 500) },
        )
    }

    @Test
    fun databaseExistsExceptionResultsInConflict(asserter: UniAsserter) {
        whenever(appServiceClient.findApp("appID")).thenReturn(Uni.createFrom().item(response))

        whenever(
            provisionDatabaseHelperService
                .initDatabase(DatabaseInitRequest("safedeposit_appid_testUser", responseInput, listOf("safedeposit"))),
        ).doAnswer { throw SQLException("Some text in the exception", "42P04") }

        asserter.assertFailedWith(
            { safeDepositService.createSafeDepositDatabase("appID", "testUser") },
            { Assertions.assertTrue(it is ClientErrorException && it.response.status == 409 && it.message == "Database already exists") },
        )
    }

    @Test
    fun otherSQLExceptionResultsInThatException(asserter: UniAsserter) {
        whenever(appServiceClient.findApp("appID")).thenReturn(Uni.createFrom().item(response))

        val sqlException = SQLException("Some text in the exception", "ABC123")
        whenever(
            provisionDatabaseHelperService
                .initDatabase(DatabaseInitRequest("safedeposit_appid_testUser", responseInput, listOf("safedeposit"))),
        ).doAnswer { throw sqlException }

        asserter.assertFailedWith(
            { safeDepositService.createSafeDepositDatabase("appID", "testUser") },
            { Assertions.assertEquals(sqlException, it) },
        )
    }

    @Test
    fun testDeleteSafeDepositDatabase(asserter: UniAsserter) {
        asserter.assertThat(
            { safeDepositService.deleteSafeDepositDatabase("appID", "testUser") },
            {
                Assertions.assertNotNull(it)
                verify(provisionDatabaseHelperService).deleteDatabase(DatabaseDeleteRequest("safedeposit_appID_testUser"))
                verify(provisionDatabaseHelperService).deleteSafeDepositRole("appID", "testUser")
            },
        )
    }

    @Test
    fun deleteNonExistingSafeDepositDatabaseReturns(asserter: UniAsserter) {
        whenever(provisionDatabaseHelperService.deleteDatabase(any()))
            .doAnswer { throw SQLException("some reason", "3D000") }
        asserter.assertNotNull { safeDepositService.deleteSafeDepositDatabase("appID", "testUser") }
    }

    @Test
    fun deleteSafeDepositDatabasePropagatesOtherError(asserter: UniAsserter) {
        val sqlException = SQLException("some other reason", "ABCD5")
        whenever(provisionDatabaseHelperService.deleteDatabase(any()))
            .doAnswer { throw sqlException }
        asserter.assertFailedWith(
            { safeDepositService.deleteSafeDepositDatabase("appID", "testUser") },
            { Assertions.assertEquals(sqlException, it) },
        )
    }
}
