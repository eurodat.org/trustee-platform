package org.eurodat.testcommon

import io.quarkus.test.junit.QuarkusTestProfile

class DevServicesDisabledProfile : QuarkusTestProfile {
    override fun getConfigOverrides(): MutableMap<String, String> =
        mutableMapOf(
            "quarkus.devservices.enabled" to "false",
            // dummy variables, empty strings cause exceptions
            "quarkus.datasource.username" to "username",
            "quarkus.datasource.password" to "password",
            "quarkus.datasource.reactive.url" to "postgresql://eurodat-postgresql.transaction-plane.svc:5432/database_123456",
        )

    override fun disableGlobalTestResources(): Boolean = true
}
