plugins {
    id("java")
    kotlin("jvm")
}

group = "org.eurodat"
version = "unspecified"

repositories {
    mavenCentral()
}

val quarkusPlatformArtifactId: String by project
val quarkusPlatformGroupId: String by project
val quarkusPlatformVersion: String by project
val quarkusPluginId: String by project

dependencies {
    implementation("$quarkusPluginId:quarkus-junit5:$quarkusPlatformVersion")
    implementation(enforcedPlatform("$quarkusPlatformGroupId:$quarkusPlatformArtifactId:$quarkusPlatformVersion"))
}

tasks.test {
    useJUnitPlatform()
}
