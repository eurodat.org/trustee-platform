plugins {
    java
    id("io.quarkus")
    kotlin("jvm")
    id("org.sonarqube") version "4.3.1.3277"
    id("jacoco")
    `maven-publish`
}

jacoco {
    toolVersion = "0.8.10" // match quarkus' version
}

val quarkusPluginId: String by project
val quarkusPlatformArtifactId: String by project
val quarkusPlatformGroupId: String by project
val quarkusPlatformVersion: String by project
val wiremockVersion: String by project

publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["kotlin"])
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/33611450/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

dependencies {
    implementation("$quarkusPluginId:quarkus-rest-jackson:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-rest-client-jackson:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-rest-client-oidc-token-propagation:$quarkusPlatformVersion")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")

    implementation(enforcedPlatform("$quarkusPlatformGroupId:$quarkusPlatformArtifactId:$quarkusPlatformVersion"))
    implementation("$quarkusPluginId:quarkus-arc:$quarkusPlatformVersion")

    testImplementation(project(":test-common"))
    testImplementation("$quarkusPluginId:quarkus-junit5:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-jacoco:$quarkusPlatformVersion")
    testImplementation("com.marcinziolo:kotlin-wiremock:$wiremockVersion")
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
    options.compilerArgs.add("-parameters")
}

tasks.withType<Test> {
    systemProperty("java.util.logging.manager", "org.jboss.logmanager.LogManager")
    finalizedBy("jacocoTestReport")

    configure<JacocoTaskExtension> {
        excludeClassLoaders = listOf("*QuarkusClassLoader")
        destinationFile =
            layout.buildDirectory
                .file("jacoco/jacoco-quarkus.exec")
                .get()
                .asFile
    }
}

// Required otherwise publishing is not possible, see: https://quarkus.io/guides/gradle-tooling#publishing-your-application
tasks.withType<GenerateModuleMetadata>().configureEach {
    suppressedValidationErrors.add("enforced-platform")
}
