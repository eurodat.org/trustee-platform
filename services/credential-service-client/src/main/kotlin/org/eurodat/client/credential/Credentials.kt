package org.eurodat.client.credential

data class Credentials(
    val jdbcUrl: String = "",
    val username: String = "",
    val password: String = "",
    val leaseDuration: Int = 0,
)
