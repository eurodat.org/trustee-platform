package org.eurodat.client.credential

import io.quarkus.oidc.token.propagation.reactive.AccessTokenRequestReactiveFilter
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient

@RegisterRestClient(configKey = "credential-service")
@Path("database")
@RegisterProvider(AccessTokenRequestReactiveFilter::class)
interface DatabaseCredentialClient {
    @GET
    @Path("/internal/{transactionId}")
    fun readInternalAccessCredentials(
        @PathParam("transactionId") transactionId: String,
    ): Uni<Credentials>

    @POST
    @Path("/internal/{transactionId}")
    fun registerTransactionConnection(
        @PathParam("transactionId") transactionId: String,
    ): Uni<Unit>

    @DELETE
    @Path("/internal/{transactionId}")
    fun deleteInternalConnection(
        @PathParam("transactionId") transactionId: String,
    ): Uni<Unit>

    @GET
    @Path("/external/{transactionId}")
    fun readExternalAccessCredentials(
        @PathParam("transactionId") transactionId: String,
    ): Uni<Credentials>

    @POST
    @Path("/external/{clientId}")
    fun registerClientConnection(
        @PathParam("clientId") clientId: String,
    ): Uni<Unit>

    @DELETE
    @Path("/external/{clientId}")
    fun deleteUserConnection(
        @PathParam("clientId") clientId: String,
    ): Uni<Unit>

    @GET
    @Path("/safedeposit/{appId}")
    fun readSafeDepositAccessCredentials(
        @PathParam("appId") appId: String,
    ): Uni<Credentials>
}
