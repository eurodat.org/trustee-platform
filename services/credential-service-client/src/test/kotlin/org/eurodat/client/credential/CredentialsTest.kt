package org.eurodat.client.credential

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.TestProfile
import org.eurodat.testcommon.DevServicesDisabledProfile
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@QuarkusTest
@TestProfile(DevServicesDisabledProfile::class)
class CredentialsTest {
    /**
     * Sonarqube demands a test for this class, so we check that the creation is well-defined and does not throw any exception.
     */
    @Test
    fun testClass() {
        Assertions.assertDoesNotThrow { Credentials("", "", "", 0) }
    }
}
