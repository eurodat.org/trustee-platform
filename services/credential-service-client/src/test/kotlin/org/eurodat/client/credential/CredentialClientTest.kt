package org.eurodat.client.credential

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.common.ConsoleNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.http.RequestMethod
import com.marcinziolo.kotlin.wiremock.contains
import com.marcinziolo.kotlin.wiremock.delete
import com.marcinziolo.kotlin.wiremock.equalTo
import com.marcinziolo.kotlin.wiremock.get
import com.marcinziolo.kotlin.wiremock.post
import com.marcinziolo.kotlin.wiremock.returns
import com.marcinziolo.kotlin.wiremock.verify
import io.quarkus.security.credential.TokenCredential
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.TestProfile
import jakarta.enterprise.inject.Produces
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.testcommon.DevServicesDisabledProfile
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test

@QuarkusTest
@TestProfile(DevServicesDisabledProfile::class)
class CredentialClientTest {
    @RestClient
    private lateinit var credentialClient: DatabaseCredentialClient

    companion object {
        private val mockServer =
            WireMockServer(
                WireMockConfiguration
                    .options()
                    .port(5555)
                    .notifier(ConsoleNotifier("HttpRequestBuilder", true)),
            )

        @BeforeAll
        @JvmStatic
        fun setup() {
            mockServer.post {
                url equalTo "/database/internal/123456"
            } returns {
                statusCode = 200
            }
            mockServer.delete {
                url equalTo "/database/internal/123456"
            } returns {
                statusCode = 200
            }
            mockServer.get {
                url equalTo "/database/internal/123456"
            } returns {
                statusCode = 200
            }
            mockServer.get {
                url equalTo "/database/external/123456"
            } returns {
                statusCode = 200
            }
            mockServer.post {
                url equalTo "/database/external/clientId"
            } returns {
                statusCode = 200
            }
            mockServer.get {
                url equalTo "/database/safedeposit/mockApp"
            } returns {
                statusCode = 200
            }
            mockServer.start()
        }

        @JvmStatic
        @AfterAll
        fun teardown() {
            mockServer.resetAll()
            mockServer.stop()
        }
    }

    @Test
    fun testRegisterTransactionConnection() {
        credentialClient
            .registerTransactionConnection("123456")
            .await()
            .indefinitely()
        mockServer.verify {
            urlPath equalTo "/database/internal/123456"
            headers contains "Authorization" equalTo "Bearer TOKEN"
            exactly = 1
            method = RequestMethod.POST
        }
    }

    @Test
    fun testDeleteConnection() {
        credentialClient
            .deleteInternalConnection("123456")
            .await()
            .indefinitely()
        mockServer.verify {
            urlPath equalTo "/database/internal/123456"
            headers contains "Authorization" equalTo "Bearer TOKEN"
            exactly = 1
            method = RequestMethod.DELETE
        }
    }

    @Test
    fun testReadInternalAccessCredentials() {
        credentialClient
            .readInternalAccessCredentials("123456")
            .await()
            .indefinitely()
        mockServer.verify {
            urlPath equalTo "/database/internal/123456"
            headers contains "Authorization" equalTo "Bearer TOKEN"
            exactly = 1
            method = RequestMethod.GET
        }
    }

    @Test
    fun testCreateExternalAccessCredentials() {
        credentialClient
            .registerClientConnection("clientId")
            .await()
            .indefinitely()
        mockServer.verify {
            urlPath equalTo "/database/external/clientId"
            headers contains "Authorization" equalTo "Bearer TOKEN"
            exactly = 1
            method = RequestMethod.POST
        }
    }

    @Test
    fun testReadExternalAccessCredentials() {
        credentialClient
            .readExternalAccessCredentials("123456")
            .await()
            .indefinitely()
        mockServer.verify {
            urlPath equalTo "/database/external/123456"
            headers contains "Authorization" equalTo "Bearer TOKEN"
            exactly = 1
            method = RequestMethod.GET
        }
    }

    @Test
    fun readSafeDepositAccessCredentials() {
        credentialClient
            .readSafeDepositAccessCredentials("mockApp")
            .await()
            .indefinitely()
        mockServer.verify {
            urlPath equalTo "/database/safedeposit/mockApp"
            headers contains "Authorization" equalTo "Bearer TOKEN"
            exactly = 1
            method = RequestMethod.GET
        }
    }

    /**
     * Create a token bean for the test, required for token propagation.
     */
    @Produces
    fun createToken(): TokenCredential = TokenCredential("TOKEN", "bearer")
}
