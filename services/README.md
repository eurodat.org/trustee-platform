The transaction service is one of the central parts of the trustee system. It is responsible for managing contracts, transfers and
logging. The transaction service delegates doing these tasks (except making contracts) to other actors. However, it ensures that
these tasks are actually completed.

This module includes the [entry point](transaction service/README.md) to the transaction service and various extensions for it and test
systems.
