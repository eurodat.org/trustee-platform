plugins {
    java
    id("io.quarkus")
    kotlin("jvm")
    id("org.sonarqube") version "4.3.1.3277"
    id("jacoco")
    kotlin("plugin.allopen") version "2.1.0"
    kotlin("plugin.jpa") version "2.1.0"
}

allOpen {
    annotations("jakarta.persistence.Entity")
}

repositories {
    mavenCentral()
    mavenLocal()
}

jacoco {
    toolVersion = "0.8.10" // match quarkus' version
}

val kotlinLibVersion: String by project
val kotlinLoggingVersion: String by project
val eurodatVersion: String by project
val quarkusPlatformArtifactId: String by project
val quarkusPlatformGroupId: String by project
val quarkusPlatformVersion: String by project
val quarkusPluginId: String by project
val wiremockVersion: String by project
val mockkVersion: String by project

dependencies {
    implementation(project(":transaction-service-client"))
    implementation(project(":credential-service-client"))
    implementation(project(":database-service-client"))
    implementation(project(":data-management-service-client"))

    api("io.github.oshai:kotlin-logging-jvm:$kotlinLoggingVersion")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("io.vertx:vertx-web-client")
    implementation("$quarkusPluginId:quarkus-container-image-jib:$quarkusPlatformVersion")
    implementation(enforcedPlatform("$quarkusPlatformGroupId:$quarkusPlatformArtifactId:$quarkusPlatformVersion"))
    implementation("$quarkusPluginId:quarkus-arc:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-rest-client-oidc-filter:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-rest-jackson:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-rest-client-jackson:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-smallrye-health:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-smallrye-openapi:$quarkusPlatformVersion")
    implementation("$quarkusPluginId:quarkus-vertx:$quarkusPlatformVersion")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")

    testImplementation("$quarkusPluginId:quarkus-junit5:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-junit5-mockito:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-test-oidc-server:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-jacoco:$quarkusPlatformVersion")

    testImplementation("io.mockk:mockk:$mockkVersion")
    testImplementation("io.rest-assured:rest-assured")
    testImplementation("com.marcinziolo:kotlin-wiremock:$wiremockVersion")
}

group = "org.eurodat"
version = eurodatVersion

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

tasks.withType<Test> {
    systemProperty("java.util.logging.manager", "org.jboss.logmanager.LogManager")
    finalizedBy("jacocoTestReport")

    configure<JacocoTaskExtension> {
        excludeClassLoaders = listOf("*QuarkusClassLoader")
        destinationFile =
            layout.buildDirectory
                .file("jacoco/jacoco-quarkus.exec")
                .get()
                .asFile
    }
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
    options.compilerArgs.add("-parameters")
}

// Fixes JvmMockKGateway error when using mockk + quarkusTest
tasks.withType<io.quarkus.gradle.tasks.QuarkusTest> {
    jvmArgs = jvmArgs.plus("-Djdk.attach.allowAttachSelf")
}
