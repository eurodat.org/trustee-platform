package org.eurodat.client

import io.quarkus.oidc.client.filter.OidcClientFilter
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient

@RegisterRestClient(configKey = "eurodat-api")
@OidcClientFilter("eurodat-api")
interface EurodatInternalClient : EurodatClientBase
