package org.eurodat.client

data class ClientError(
    val error: String,
)
