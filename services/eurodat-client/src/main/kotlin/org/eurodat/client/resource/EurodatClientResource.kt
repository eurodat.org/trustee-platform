package org.eurodat.client.resource

import io.github.oshai.kotlinlogging.KotlinLogging
import io.quarkus.oidc.client.OidcClientException
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.MediaType
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.EurodatInternalClient
import org.eurodat.client.credential.Credentials
import org.eurodat.client.database.SafeDepositDatabaseRequest
import org.eurodat.client.database.SafeDepositDatabaseResponse
import org.eurodat.client.transactionservice.AppRequest
import org.eurodat.client.transactionservice.Transaction
import org.eurodat.client.transactionservice.WorkflowGetResponse
import org.eurodat.client.transactionservice.WorkflowRequest
import org.eurodat.client.transactionservice.WorkflowStartResponse
import org.jboss.resteasy.reactive.ClientWebApplicationException
import org.jboss.resteasy.reactive.RestResponse
import org.jboss.resteasy.reactive.server.ServerExceptionMapper
import org.slf4j.MDC

@Path("")
class EurodatClientResource {
    @RestClient
    internal lateinit var eurodatInternalClient: EurodatInternalClient

    private val logger = KotlinLogging.logger {}

    @ServerExceptionMapper
    fun mapUnauthorized(e: OidcClientException): Uni<RestResponse<String>> {
        logger.error(e) { e.localizedMessage }
        return Uni.createFrom().item(
            RestResponse.status(RestResponse.Status.UNAUTHORIZED, e.localizedMessage),
        )
    }

    @ServerExceptionMapper
    fun mapClientException(e: ClientWebApplicationException): Uni<RestResponse<String>> {
        logger.error(e) { e.localizedMessage }
        return Uni.createFrom().item(RestResponse.status(e.response.statusInfo, e.message))
    }

    @POST
    @Path("transactions")
    @Produces(MediaType.APPLICATION_JSON)
    fun startTransaction(request: AppRequest): Uni<Transaction> = eurodatInternalClient.startTransaction(request)

    @DELETE
    @Path("transactions/{transactionId}")
    @Produces(MediaType.APPLICATION_JSON)
    fun endTransaction(
        @PathParam("transactionId") transactionId: String,
    ): Uni<Transaction> {
        MDC.put("transactionId", transactionId)
        return eurodatInternalClient.endTransaction(transactionId)
    }

    @POST
    @Path("transactions/{transactionId}/workflows")
    @Produces(MediaType.APPLICATION_JSON)
    fun startWorkflow(
        @PathParam("transactionId") transactionId: String,
        workflowRequest: WorkflowRequest,
    ): Uni<WorkflowStartResponse> {
        MDC.put("transactionId", transactionId)
        return eurodatInternalClient.startWorkflow(transactionId, workflowRequest)
    }

    @GET
    @Path("transactions/{transactionId}/workflows/{workflowId}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getWorkflow(
        @PathParam("transactionId") transactionId: String,
        @PathParam("workflowId") workflowId: String,
    ): Uni<WorkflowGetResponse> {
        MDC.put("workflowId", workflowId)
        return eurodatInternalClient.getWorkflow(transactionId, workflowId)
    }

    @GET
    @Path("credential-service/database/external/{transactionId}")
    @Produces(MediaType.APPLICATION_JSON)
    fun readExternalAccessCredentials(
        @PathParam("transactionId") transactionId: String,
    ): Uni<Credentials> {
        MDC.put("transactionId", transactionId)
        return eurodatInternalClient.readExternalAccessCredentials(transactionId)
    }

    @GET
    @Path("credential-service/database/safedeposit/{appId}")
    @Produces(MediaType.APPLICATION_JSON)
    fun readSafeDepositAccessCredentials(
        @PathParam("appId") appId: String,
    ): Uni<Credentials> {
        MDC.put("appId", appId)
        return eurodatInternalClient.readSafeDepositAccessCredentials(appId)
    }

    @POST
    @Path("database-service")
    @Produces(MediaType.APPLICATION_JSON)
    fun createSafeDepositDatabase(request: SafeDepositDatabaseRequest): Uni<SafeDepositDatabaseResponse> {
        MDC.put("appId", request.appId)
        return eurodatInternalClient.createSafeDepositDatabase(request)
    }

    @DELETE
    @Path("database-service/{appId}")
    fun deleteSafeDepositDatabase(
        @PathParam("appId") appId: String,
    ): Uni<Unit> {
        MDC.put("appId", appId)
        return eurodatInternalClient.deleteSafeDepositDatabase(appId)
    }
}
