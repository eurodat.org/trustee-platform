package org.eurodat.client

import io.smallrye.mutiny.Uni
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.Produces
import jakarta.ws.rs.QueryParam
import jakarta.ws.rs.core.MediaType
import org.eurodat.client.credential.Credentials
import org.eurodat.client.database.SafeDepositDatabaseRequest
import org.eurodat.client.database.SafeDepositDatabaseResponse
import org.eurodat.client.datamanagement.model.DataRequest
import org.eurodat.client.datamanagement.model.InsertResult
import org.eurodat.client.datamanagement.model.PersistentDataRequest
import org.eurodat.client.transactionservice.AppRequest
import org.eurodat.client.transactionservice.Message
import org.eurodat.client.transactionservice.Transaction
import org.eurodat.client.transactionservice.WorkflowGetResponse
import org.eurodat.client.transactionservice.WorkflowRequest
import org.eurodat.client.transactionservice.WorkflowStartResponse

@Path("")
interface EurodatClientBase {
    @GET
    @Path("credential-service/database/external/{transactionId}")
    fun readExternalAccessCredentials(
        @PathParam("transactionId") transactionId: String,
    ): Uni<Credentials>

    @GET
    @Path("credential-service/database/safedeposit/{appId}")
    fun readSafeDepositAccessCredentials(
        @PathParam("appId") appId: String,
    ): Uni<Credentials>

    @POST
    @Path("transactions")
    @Produces("application/json")
    fun startTransaction(appRequest: AppRequest): Uni<Transaction>

    @GET
    @Path("transactions/{transactionId}")
    @Produces("application/json")
    fun findTransaction(
        @PathParam("transactionId") transactionId: String,
    ): Uni<Transaction>

    @DELETE
    @Path("transactions/{transactionId}")
    @Produces("application/json")
    fun endTransaction(
        @PathParam("transactionId") transactionId: String,
    ): Uni<Transaction>

    @POST
    @Path("transactions/{transactionId}/workflows")
    @Produces("application/json")
    fun startWorkflow(
        @PathParam("transactionId") transactionId: String,
        workflowRequest: WorkflowRequest,
    ): Uni<WorkflowStartResponse>

    @GET
    @Path("transactions/{transactionId}/workflows/{workflowId}")
    @Produces("application/json")
    fun getWorkflow(
        @PathParam("transactionId") transactionId: String,
        @PathParam("workflowId") workflowId: String,
    ): Uni<WorkflowGetResponse>

    @POST
    @Path("database-service/database-service")
    fun createSafeDepositDatabase(request: SafeDepositDatabaseRequest): Uni<SafeDepositDatabaseResponse>

    @DELETE
    @Path("database-service/database-service/{appId}")
    fun deleteSafeDepositDatabase(
        @PathParam("appId") appId: String,
    ): Uni<Unit>

    @GET
    @Path("apps/{appId}/messages")
    @Produces("application/json")
    fun getMessages(
        @PathParam("appId") appId: String,
        @QueryParam("excludeUntil") excludeUntil: Long? = null,
    ): Uni<List<Message>>

    @POST
    @Path("/participants/{participantId}/data")
    @Consumes(MediaType.APPLICATION_JSON)
    fun insertData(
        @PathParam("participantId") participantId: String,
        dataRequest: DataRequest,
    ): Uni<InsertResult>

    @GET
    @Path("/participants/{participantId}/data")
    @Produces(MediaType.APPLICATION_JSON)
    fun getData(
        @PathParam("participantId") participantId: String,
        @QueryParam("table") table: String,
        @QueryParam("transactionId") transactionId: String,
        @QueryParam("correlationId") correlationId: Long?,
    ): Uni<List<String>>

    @POST
    @Path("/participants/{participantId}/persistentdata")
    @Consumes(MediaType.APPLICATION_JSON)
    fun insertDataPersistent(
        @PathParam("participantId") participantId: String,
        persistentDataRequest: PersistentDataRequest,
    ): Uni<InsertResult>

    @GET
    @Path("/participants/{participantId}/persistentdata")
    @Produces(MediaType.APPLICATION_JSON)
    fun getDataPersistent(
        @PathParam("participantId") participantId: String,
        @QueryParam("table") table: String,
        @QueryParam("appId") appId: String,
    ): Uni<List<String>>
}
