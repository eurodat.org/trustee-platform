package org.eurodat.transactionservice.resource

import com.fasterxml.jackson.databind.ObjectMapper
import io.mockk.clearAllMocks
import io.quarkus.oidc.client.OidcClientException
import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.common.http.TestHTTPEndpoint
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.oidc.server.OidcWiremockTestResource
import io.restassured.RestAssured
import io.restassured.config.ObjectMapperConfig
import io.restassured.config.RestAssuredConfig
import io.restassured.http.ContentType
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import org.eurodat.client.resource.EurodatClientResource
import org.eurodat.client.transactionservice.AppRequest
import org.eurodat.client.transactionservice.WorkflowRequest
import org.eurodat.transactionservice.service.EurodatInternalMock
import org.jboss.resteasy.reactive.ClientWebApplicationException
import org.jboss.resteasy.reactive.RestResponse
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@QuarkusTest
@TestHTTPEndpoint(EurodatClientResource::class)
@QuarkusTestResource(EurodatInternalMock::class)
@QuarkusTestResource(OidcWiremockTestResource::class, restrictToAnnotatedClass = true)
class EurodatClientResourceTest {
    @Inject
    lateinit var objectMapper: ObjectMapper

    @Inject
    lateinit var eurodatClientResource: EurodatClientResource

    init {
        // Due to Quarkus Kotlin bug in native mode, when working with REST bodies that have Kotlin data classes, register injected ObjectMapper
        RestAssured.config =
            RestAssuredConfig.config().objectMapperConfig(
                ObjectMapperConfig().jackson2ObjectMapperFactory {
                        _,
                        _,
                    ->
                    objectMapper
                },
            )
    }

    @BeforeEach
    fun beforeEach() {
        clearAllMocks()
    }

    @Test
    fun testStartTransaction() {
        val request = AppRequest(EurodatInternalMock.APP_ID)

        RestAssured
            .given()
            .body(request)
            .contentType(ContentType.JSON)
            .post("transactions")
            .then()
            .statusCode(200)
    }

    @Test
    fun testStartTransactionInvalidAppId() {
        val request = AppRequest(EurodatInternalMock.INVALID_APP_ID)

        RestAssured
            .given()
            .body(request)
            .contentType(ContentType.JSON)
            .post("transactions")
            .then()
            .statusCode(404)
    }

    @Test
    fun testStartWorkflow() {
        val request = WorkflowRequest(EurodatInternalMock.WORKFLOW_ID)

        RestAssured
            .given()
            .body(request)
            .contentType(ContentType.JSON)
            .pathParam("transactionId", EurodatInternalMock.TRANSACTION_ID)
            .post("transactions/{transactionId}/workflows")
            .then()
            .statusCode(200)
    }

    @Test
    fun testStartWorkflowInvalidTransactionId() {
        val request = WorkflowRequest(EurodatInternalMock.WORKFLOW_ID)

        RestAssured
            .given()
            .body(request)
            .contentType(ContentType.JSON)
            .pathParam("transactionId", EurodatInternalMock.INVALID_TRANSACTION_ID)
            .post("transactions/{transactionId}/workflows")
            .then()
            .statusCode(404)
    }

    @Test
    fun testEndTransaction() {
        RestAssured
            .given()
            .contentType(ContentType.JSON)
            .pathParam("transactionId", EurodatInternalMock.TRANSACTION_ID)
            .delete("transactions/{transactionId}")
            .then()
            .statusCode(200)
    }

    @Test
    fun testEndTransactionInvalidTransactionId() {
        RestAssured
            .given()
            .contentType(ContentType.JSON)
            .pathParam("transactionId", EurodatInternalMock.INVALID_TRANSACTION_ID)
            .delete("transactions/{transactionId}")
            .then()
            .statusCode(404)
    }

    @Test
    fun testGetWorkflow() {
        RestAssured
            .given()
            .contentType(ContentType.JSON)
            .pathParam("transactionId", EurodatInternalMock.TRANSACTION_ID)
            .pathParam("workflowId", EurodatInternalMock.WORKFLOW_ID)
            .get("transactions/{transactionId}/workflows/{workflowId}")
            .then()
            .statusCode(200)
    }

    @Test
    fun testGetWorkflowInvalidId() {
        RestAssured
            .given()
            .contentType(ContentType.JSON)
            .pathParam("transactionId", EurodatInternalMock.TRANSACTION_ID)
            .pathParam("workflowId", EurodatInternalMock.INVALID_WORKFLOW_ID)
            .get("transactions/{transactionId}/workflows/{workflowId}")
            .then()
            .statusCode(404)
    }

    @Test
    fun testGetCredentialsExternal() {
        RestAssured
            .given()
            .pathParam("transactionId", EurodatInternalMock.TRANSACTION_ID)
            .get("/credential-service/database/external/{transactionId}")
            .then()
            .statusCode(200)
    }

    @Test
    fun testGetCredentialsExternalInvalidTransactionId() {
        RestAssured
            .given()
            .pathParam("transactionId", EurodatInternalMock.INVALID_TRANSACTION_ID)
            .get("/credential-service/database/external/{transactionId}")
            .then()
            .statusCode(404)
    }

    @Test
    fun readSafeDepositAccessCredentials() {
        RestAssured
            .given()
            .pathParam("appId", EurodatInternalMock.APP_ID)
            .get("/credential-service/database/safedeposit/{appId}")
            .then()
            .statusCode(200)
    }

    @Test
    fun readSafeDepositAccessCredentialsInvalidAppId() {
        RestAssured
            .given()
            .pathParam("appId", EurodatInternalMock.INVALID_APP_ID)
            .get("/credential-service/database/safedeposit/{appId}")
            .then()
            .statusCode(404)
    }

    @Test
    fun createSafeDepositDatabase() {
        RestAssured
            .given()
            .body(EurodatInternalMock.APP_ID)
            .post("database-service")
            .then()
            .statusCode(200)
    }

    @Test
    fun createSafeDepositDatabaseInvalidAppId() {
        RestAssured
            .given()
            .body(EurodatInternalMock.INVALID_APP_ID)
            .post("database-service")
            .then()
            .statusCode(404)
    }

    @Test
    fun deleteSafeDepositDatabase() {
        RestAssured
            .given()
            .pathParam("appId", EurodatInternalMock.APP_ID)
            .delete("database-service/{appId}")
            .then()
            .statusCode(204)
    }

    @Test
    fun deleteSafeDepositDatabaseInvalidAppId() {
        RestAssured
            .given()
            .pathParam("appId", EurodatInternalMock.INVALID_APP_ID)
            .delete("database-service/{appId}")
            .then()
            .statusCode(404)
    }

    @Test
    fun testOIDCExceptionMappers() {
        val exceptionText = "Unauthorized"
        val unauthorizedException = OidcClientException(exceptionText)
        val result: Uni<RestResponse<String>> = eurodatClientResource.mapUnauthorized(unauthorizedException)
        val response: RestResponse<String> = result.await().indefinitely()
        Assertions.assertEquals(RestResponse.StatusCode.UNAUTHORIZED, response.status)
        Assertions.assertEquals(exceptionText, response.entity)
    }

    @Test
    fun testMapClientException() {
        val exceptionText = "Some explanation"
        val webApplicationException = ClientWebApplicationException(exceptionText)
        val result: Uni<RestResponse<String>> = eurodatClientResource.mapClientException(webApplicationException)
        val response = result.await().indefinitely()
        Assertions.assertEquals(exceptionText, response.entity)
    }
}
