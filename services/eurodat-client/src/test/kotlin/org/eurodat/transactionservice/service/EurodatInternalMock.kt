package org.eurodat.transactionservice.service

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.common.ConsoleNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import com.marcinziolo.kotlin.wiremock.contains
import com.marcinziolo.kotlin.wiremock.delete
import com.marcinziolo.kotlin.wiremock.equalTo
import com.marcinziolo.kotlin.wiremock.get
import com.marcinziolo.kotlin.wiremock.post
import com.marcinziolo.kotlin.wiremock.returns
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager
import org.eurodat.client.credential.Credentials
import org.eurodat.client.transactionservice.Transaction
import org.eurodat.client.transactionservice.WorkflowGetResponse
import org.eurodat.client.transactionservice.WorkflowMetadata
import org.eurodat.client.transactionservice.WorkflowStartResponse
import wiremock.com.fasterxml.jackson.databind.ObjectMapper

class EurodatInternalMock : QuarkusTestResourceLifecycleManager {
    companion object {
        const val APP_ID = "app-id"
        const val INVALID_APP_ID = "invalid-app-id"
        const val TRANSACTION_ID = "transaction-id"
        const val INVALID_TRANSACTION_ID = "invalid-transaction-id"
        const val WORKFLOW_ID = "workflow-id"
        const val INVALID_WORKFLOW_ID = "invalid-workflow-id"
    }

    private val objectMapper = ObjectMapper()
    private val eurodatInternalMockServer =
        WireMockServer(
            options()
                .port(8090)
                .notifier(ConsoleNotifier("HttpRequestBuilder", true)),
        )

    override fun start(): Map<String, String> {
        eurodatInternalMockServer.post {
            url equalTo "/transactions"
            body contains "appId" equalTo APP_ID
        } returns {
            header = "Content-Type" to "application/json"
            statusCode = 200
            body = objectMapper.writeValueAsString(Transaction(TRANSACTION_ID))
        }

        eurodatInternalMockServer.post {
            url equalTo "/transactions"
            body contains "appId" equalTo INVALID_APP_ID
        } returns {
            statusCode = 404
        }

        eurodatInternalMockServer.post {
            url equalTo "/transactions/$TRANSACTION_ID/workflows"
        } returns {
            header = "Content-Type" to "application/json"
            statusCode = 200
            body = objectMapper.writeValueAsString(WorkflowStartResponse(TRANSACTION_ID))
        }

        eurodatInternalMockServer.post {
            url equalTo "/transactions/$INVALID_TRANSACTION_ID/workflows"
        } returns {
            statusCode = 404
        }

        eurodatInternalMockServer.delete {
            url equalTo "/transactions/$TRANSACTION_ID"
        } returns {
            header = "Content-Type" to "application/json"
            statusCode = 200
            body = objectMapper.writeValueAsString(Transaction(TRANSACTION_ID))
        }

        eurodatInternalMockServer.delete {
            url equalTo "/transactions/$INVALID_TRANSACTION_ID"
        } returns {
            statusCode = 404
        }

        eurodatInternalMockServer.get {
            url equalTo "/transactions/$TRANSACTION_ID/workflows/$WORKFLOW_ID"
        } returns {
            header = "Content-Type" to "application/json"
            statusCode = 200
            body = objectMapper.writeValueAsString(WorkflowGetResponse(WorkflowMetadata(WORKFLOW_ID)))
        }

        eurodatInternalMockServer.get {
            url equalTo "/transactions/$TRANSACTION_ID/workflows/$INVALID_WORKFLOW_ID"
        } returns {
            statusCode = 404
        }

        eurodatInternalMockServer.get {
            url equalTo "/credential-service/database/external/${TRANSACTION_ID}"
        } returns {
            header = "Content-Type" to "application/json"
            statusCode = 200
            body =
                objectMapper.writeValueAsString(
                    Credentials(
                        "jdbc:postgresql://host/database_${TRANSACTION_ID}",
                        "internal_user1",
                        "internal_password1",
                    ),
                )
        }

        eurodatInternalMockServer.get {
            url equalTo "/credential-service/database/external/${INVALID_TRANSACTION_ID}"
        } returns {
            header = "Content-Type" to "application/json"
            statusCode = 404
        }

        eurodatInternalMockServer.get {
            url equalTo "/credential-service/database/safedeposit/${APP_ID}"
        } returns {
            header = "Content-Type" to "application/json"
            statusCode = 200
            body =
                objectMapper.writeValueAsString(
                    Credentials("jdbc:postgresql://host/safedeposit_${APP_ID}", "internal_user1", "internal_password1"),
                )
        }

        eurodatInternalMockServer.get {
            url equalTo "/credential-service/database/safedeposit/${INVALID_APP_ID}"
        } returns {
            header = "Content-Type" to "application/json"
            statusCode = 404
        }

        eurodatInternalMockServer.post {
            url equalTo "/database-service/database-service"
            body contains "appId" equalTo APP_ID
        } returns {
            header = "Content-Type" to "application/json"
            statusCode = 200
            body =
                """
                {
                    "response": "foo"
                }
                """.trimIndent()
        }

        eurodatInternalMockServer.post {
            url equalTo "/database-service/database-service"
            body contains "appId" equalTo INVALID_APP_ID
        } returns {
            header = "Content-Type" to "application/json"
            statusCode = 404
        }

        eurodatInternalMockServer.delete {
            url equalTo "/database-service/database-service/${APP_ID}"
        } returns {
            header = "Content-Type" to "application/json"
            statusCode = 204
            body =
                """
                {
                    "response": "foo"
                }
                """.trimIndent()
        }

        eurodatInternalMockServer.delete {
            url equalTo "/database-service/database-service/${INVALID_APP_ID}"
        } returns {
            header = "Content-Type" to "application/json"
            statusCode = 404
        }

        eurodatInternalMockServer.start()
        return mapOf(
            "quarkus.rest-client.eurodat-api.url" to eurodatInternalMockServer.baseUrl(),
        )
    }

    override fun stop() {
        eurodatInternalMockServer.stop()
    }
}
