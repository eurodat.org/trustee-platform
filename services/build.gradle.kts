import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.owasp.dependencycheck.gradle.extension.AnalyzerExtension
import org.owasp.dependencycheck.gradle.extension.NvdExtension

plugins {
    `java-library`
    kotlin("jvm") version "2.1.0"
    id("jacoco")
    id("org.owasp.dependencycheck") version "9.2.0"
}

jacoco {
    toolVersion = "0.8.10" // match quarkus' version
}

repositories {
    mavenCentral()
}

dependencyCheck {
    suppressionFile = "suppression.xml"
    formats = mutableListOf("HTML", "JUNIT")
    failBuildOnCVSS = 7.0f
    autoUpdate = true

    nvd(
        closureOf<NvdExtension> {
            apiKey = System.getenv("NVD_API_KEY")
            delay = 3500
        },
    )

    analyzers(
        closureOf<AnalyzerExtension> {
            // for some reason not automatically disabled
            assemblyEnabled = false
            // Deprecated - As of the 5.2.5 - please use nodeAudit
            nodeEnabled = false
            // somehow crashes, but we don't need to scan archives and scan JARs with the JAR analyzer
            archiveEnabled = false
        },
    )
}

val kotlinLibVersion: String by project

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlinLibVersion")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlinLibVersion")
}

allprojects {
}

subprojects {
    repositories {
        mavenLocal()
        mavenCentral()
    }

    apply(plugin = "org.owasp.dependencycheck")
    group = "org.eurodat"
    tasks.withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "17"
        }
    }
}
