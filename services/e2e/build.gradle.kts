plugins {
    id("io.quarkus")
    kotlin("jvm")
    id("jacoco")
}

java.sourceCompatibility = JavaVersion.VERSION_17

val arrowFxVersion: String by project
val kotlinLoggingVersion: String by project
val awaitilityVersion: String by project
val coroutineVersion: String by project
val jacksonVersion: String by project
val jupiterVersion: String by project
val quarkusPlatformVersion: String by project
val quarkusPluginId: String by project

val quarkusPlatformArtifactId: String by project
val quarkusPlatformGroupId: String by project

tasks.withType<Test> {
    useJUnitPlatform()
    finalizedBy("jacocoTestReport")
    testLogging {
        events("started")
        showStandardStreams = true
    }
    maxParallelForks = 8 // >1 for parallel execution of test classes
    systemProperties =
        mapOf(
            "junit.jupiter.execution.parallel.mode.default" to "same_thread",
            "junit.jupiter.execution.parallel.mode.classes.default" to "concurrent",
        )
    outputs.upToDateWhen { false }
}

dependencies {
    implementation(enforcedPlatform("$quarkusPlatformGroupId:$quarkusPlatformArtifactId:$quarkusPlatformVersion"))
    testImplementation(project(":eurodat-client"))
    testImplementation(project(":transaction-service-client"))
    testImplementation(project(":app-service-client"))
    testImplementation(project(":contract-service-client"))
    testImplementation(project(":database-service-client"))
    testImplementation(project(":data-management-service-client"))
    testImplementation(project(":credential-service-client"))
    testImplementation("$quarkusPluginId:quarkus-rest-client-jackson:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-rest-client-oidc-filter:$quarkusPlatformVersion")
    testImplementation("io.github.oshai:kotlin-logging-jvm:$kotlinLoggingVersion")
    testImplementation("com.fasterxml.jackson.module:jackson-module-kotlin:$jacksonVersion")
    testImplementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:$jacksonVersion")
    testImplementation("org.postgresql:postgresql")
    testImplementation("$quarkusPluginId:quarkus-junit5:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-test-vertx:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-vertx:$quarkusPlatformVersion")
    testImplementation("$quarkusPluginId:quarkus-kotlin:$quarkusPlatformVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$jupiterVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-params:$jupiterVersion")
    testImplementation("org.awaitility:awaitility:$awaitilityVersion")
    testImplementation("org.junit-pioneer:junit-pioneer:2.2.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$jupiterVersion")
}
