create table safedeposit.json
(
    uuid_json uuid not null primary key,
    blob_json JSONB null
);

create table safedeposit.pdf
(
    uuid_pdf uuid not null primary key,
    blob_pdf BYTEA null
);
