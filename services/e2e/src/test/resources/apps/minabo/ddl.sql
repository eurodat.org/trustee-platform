create table input.json
(
    uuid_json uuid not null
);

create table input.pdf
(
    uuid_pdf uuid not null
);

create table output.json
(
    uuid_json uuid not null primary key,
    blob_json JSONB null
);

create table output.pdf
(
    uuid_pdf uuid not null primary key,
    blob_pdf BYTEA null
);
