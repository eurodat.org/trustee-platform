create table input.input
(
    input_id integer not null primary key,
    poll_id integer,
    submission integer,
    participant_identifier text not null,
    created_at    timestamp not null default NOW()
);

comment on column input.input.input_id is 'the tables primary key';

comment on column input.input.poll_id is 'the submission round number';

comment on column input.input.submission is 'the submission of the participant';

comment on column input.input.participant_identifier is 'the participants identifier';

comment on column input.input.created_at is 'the timestamp when the record was created';

create table output.output
(
    output_id integer not null primary key,
    poll_id integer,
    result integer,
    submission integer,
    participant_identifier text not null,
    created_at    timestamp not null default NOW()
);

ALTER TABLE output.output ENABLE ROW LEVEL SECURITY;

comment on column output.output.output_id is 'the tables primary key';

comment on column output.output.poll_id is 'the submission round number';

comment on column output.output.result is 'the sum of the submissions';

comment on column output.output.submission is 'the submission of the participant';

comment on column output.output.participant_identifier is 'the participants identifier';

comment on column output.output.created_at is 'the timestamp when the record was created';
