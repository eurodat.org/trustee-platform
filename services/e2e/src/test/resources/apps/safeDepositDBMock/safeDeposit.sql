create table safedeposit.input_transactions
(
    transaction_id uuid not null primary key,
    bank_transaction_id text not null
);
