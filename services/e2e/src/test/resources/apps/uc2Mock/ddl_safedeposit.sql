create table safedeposit.input
(
    id           uuid      not null
        constraint input_pk
            primary key,
    created_at   timestamp not null,
    created_by   text,
    file_content bytea       not null,
    file_type    text,
    dataset_id uuid not null,
    CONSTRAINT unique_dataset_file_type UNIQUE (dataset_id, file_type)
);
