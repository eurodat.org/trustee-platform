create table input.input_transactions
(
    transaction_id uuid not null primary key,
    bank_transaction_id text not null,
    transaction_timestamp timestamp not null,
    created_at    timestamp not null,
    originating_bank_bic text not null,
    counterparty_bank_bic text not null,
    originator_iban text not null,
    counterparty_iban text not null,
    amount float not null,
    currency text not null,
    transaction_type text not null
);

create table output.output_transactions
(
    transaction_id uuid not null,
    bank_transaction_id text,
    transaction_timestamp timestamp not null,
    created_at    timestamp not null default NOW(),
    originating_bank_bic text not null,
    counterparty_bank_bic text not null,
    originator_iban text,
    counterparty_iban text,
    amount float not null,
    currency text not null,
    transaction_type text not null,
    security_column text not null,
    correlation_id bigint,
    CONSTRAINT primary_key_output PRIMARY KEY (transaction_id)
);
