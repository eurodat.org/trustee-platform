create table output.output_first
(
    transaction_id int not null primary key,
    name text,
    security_column text not null
);

ALTER TABLE output.output_first ENABLE ROW LEVEL SECURITY;

create table output.output_second
(
    transaction_id int not null primary key,
    name text,
    security_column text not null
);

ALTER TABLE output.output_second ENABLE ROW LEVEL SECURITY;


