package org.eurodat.e2e.jdbc

import org.eurodat.client.credential.Credentials
import java.sql.DriverManager

internal fun createJdbcStatement(credentials: Credentials) = getJdbcConnection(credentials).createStatement()

internal fun getJdbcConnection(credentials: Credentials) =
    DriverManager.getConnection(credentials.jdbcUrl, credentials.username, credentials.password)
