package org.eurodat.e2e.clients

import io.quarkus.oidc.client.filter.OidcClientFilter
import jakarta.ws.rs.Path
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import org.eurodat.client.EurodatClientBase

@RegisterRestClient(configKey = "provider")
@OidcClientFilter("provider")
@Path("")
interface ProviderClient : EurodatClientBase
