package org.eurodat.e2e.apps

import java.io.FileNotFoundException

class FileHelper {
    companion object {
        // Can't be fixed by ktlint, always leads to violation
        @Suppress("ktlint:standard:curly-spacing")
        fun fileToString(fileName: String): String =
            {}
                .javaClass.classLoader
                .getResourceAsStream(fileName)
                ?.bufferedReader()
                ?.readText()
                ?: throw FileNotFoundException("$fileName not found")
    }
}
