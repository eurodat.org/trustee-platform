package org.eurodat.e2e

import io.github.oshai.kotlinlogging.KotlinLogging
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.appservice.model.TableSecurityMapping
import org.eurodat.client.transactionservice.AppRequest
import org.eurodat.client.transactionservice.WorkflowRequest
import org.eurodat.e2e.apps.AppRegistrar
import org.eurodat.e2e.apps.FileHelper.Companion.fileToString
import org.eurodat.e2e.assertions.assertTransactionEnded
import org.eurodat.e2e.assertions.assertTransactionStarted
import org.eurodat.e2e.assertions.assertWorkflowSucceeded
import org.eurodat.e2e.clients.ConsumerClient
import org.eurodat.e2e.clients.ProviderClient
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestMethodOrder
import org.junitpioneer.jupiter.DisableIfTestFails

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(
    MethodOrderer.OrderAnnotation::class,
)
@QuarkusTest
@DisableIfTestFails
@RunOnVertxContext
class TestRowLevelSecurity {
    private val appId = "rlsMock"
    private val imageName = "row-level-security-mock-app"
    private val workflowDefinitionId = "start-rls"
    private val appRequest = AppRequest(appId = appId)
    private val mockConsumerParticipantId = "dbselector"
    private val mockProviderParticipantId = "cobaselector"

    private var generatedTransactionId = ""
    private var workflowId = ""
    private val logger = KotlinLogging.logger {}

    @RestClient
    private lateinit var consumerClient: ConsumerClient

    @RestClient
    private lateinit var providerClient: ProviderClient

    @Inject
    private lateinit var appRegistrar: AppRegistrar

    @Test
    @Order(0)
    fun deRegisterIfExistsTest(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }

    @Test
    @Order(1)
    fun registerRequiredApp(asserter: UniAsserter) {
        asserter.assertNotNull {
            appRegistrar.registerAppWithImageAndWorkflow(
                AppRegistration(
                    appId,
                    fileToString("apps/rlsMock/ddl.sql"),
                    "",
                    listOf(
                        TableSecurityMapping("security_column", "output_first"),
                        TableSecurityMapping("security_column", "output_second"),
                    ),
                ),
                imageName,
                workflowDefinitionId,
                listOf("python", "main.py"),
            )
        }
    }

    @Test
    @Order(2)
    fun testTransactionStart(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.startTransaction(appRequest) },
            {
                generatedTransactionId = it.id
                assertTransactionStarted(appId, it)
            },
        )
    }

    @Test
    @Order(3)
    fun testWorkflowStartWriteIntoTables(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.startWorkflow(generatedTransactionId, WorkflowRequest(workflowDefinitionId)) },
            {
                Assertions.assertEquals(generatedTransactionId, it.transactionId)
                Assertions.assertEquals(workflowDefinitionId, it.workflowDefinitionId)
                workflowId = it.workflowRunId
            },
        )
    }

    @Test
    @Order(4)
    fun testWorkflowsSucceeded(asserter: UniAsserter) {
        assertWorkflowSucceeded(generatedTransactionId, workflowId, consumerClient, logger, asserter)
    }

    @Test
    @Order(5)
    fun testReadFromTableFirstProvider(asserter: UniAsserter) {
        asserter.assertThat(
            { providerClient.getData(mockProviderParticipantId, "output_first", generatedTransactionId, null) },
            { data ->
                Assertions.assertNotNull(data)
                Assertions.assertEquals(2, data.size)
            },
        )
    }

    @Test
    @Order(6)
    fun testReadFromTableSecondProvider(asserter: UniAsserter) {
        asserter.assertThat(
            { providerClient.getData(mockProviderParticipantId, "output_second", generatedTransactionId, null) },
            { data ->
                Assertions.assertNotNull(data)
                Assertions.assertEquals(2, data.size)
            },
        )
    }

    @Test
    @Order(7)
    fun testReadFromTableFirstConsumer(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.getData(mockConsumerParticipantId, "output_first", generatedTransactionId, null) },
            { data ->
                Assertions.assertNotNull(data)
                Assertions.assertEquals(1, data.size)
            },
        )
    }

    @Test
    @Order(8)
    fun testReadFromTableSecondConsumer(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.getData(mockConsumerParticipantId, "output_second", generatedTransactionId, null) },
            { data ->
                Assertions.assertNotNull(data)
                Assertions.assertEquals(1, data.size)
            },
        )
    }

    @Test
    @Order(9)
    fun testTransactionEnd(asserter: UniAsserter) {
        asserter.assertThat(
            { providerClient.endTransaction(generatedTransactionId) },
            { assertTransactionEnded(generatedTransactionId, it) },
        )
    }

    @Test
    @Order(10)
    fun deRegisterRequiredApp(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }
}
