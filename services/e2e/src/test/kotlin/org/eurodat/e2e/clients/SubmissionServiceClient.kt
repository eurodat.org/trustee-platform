package org.eurodat.e2e.clients

import com.fasterxml.jackson.databind.JsonNode
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.QueryParam
import jakarta.ws.rs.core.MediaType
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient

@RegisterRestClient(configKey = "contract-service")
@Path("/submissions")
fun interface SubmissionServiceClient {
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    fun processSubmission(
        @QueryParam("token") passToken: String,
        submission: JsonNode,
    ): Uni<Unit>
}
