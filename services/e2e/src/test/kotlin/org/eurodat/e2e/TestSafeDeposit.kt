package org.eurodat.e2e

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.uni
import jakarta.inject.Inject
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.AppServiceExternalClient
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.credential.Credentials
import org.eurodat.client.database.SafeDepositDatabaseRequest
import org.eurodat.e2e.apps.AppRegistrar
import org.eurodat.e2e.assertions.assertNonEmptyCredentials
import org.eurodat.e2e.clients.ConsumerClient
import org.eurodat.e2e.jdbc.createJdbcStatement
import org.jboss.resteasy.reactive.ClientWebApplicationException
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestMethodOrder
import org.junitpioneer.jupiter.DisableIfTestFails
import java.sql.SQLException

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(
    MethodOrderer.OrderAnnotation::class,
)
@QuarkusTest
@DisableIfTestFails
@RunOnVertxContext
class TestSafeDeposit {
    private val appId = "safeDepositDummyAppMock"
    private val nonValidId = "invalidApp"

    private lateinit var consumerCredentials: Credentials

    @RestClient
    private lateinit var consumerClient: ConsumerClient

    @RestClient
    private lateinit var appServiceClient: AppServiceExternalClient

    @Inject
    private lateinit var appRegistrar: AppRegistrar

    @Test
    @Order(0)
    fun deRegisterIfExistsTest(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplication(appId) }
    }

    @Test
    @Order(1)
    fun registerRequiredApp(asserter: UniAsserter) {
        asserter.assertNotNull {
            appServiceClient.startAppRegistration(
                AppRegistration(
                    appId,
                    "",
                    "",
                    emptyList(),
                ),
            )
        }
    }

    @Test
    @Order(2)
    fun testDeleteNonExistingSafeDeposit(asserter: UniAsserter) {
        asserter.assertNull { consumerClient.deleteSafeDepositDatabase(appId) }
    }

    @Test
    @Order(3)
    fun testCreateNonValidAppSafeDeposit(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { consumerClient.createSafeDepositDatabase(SafeDepositDatabaseRequest(nonValidId)) },
            { Assertions.assertEquals(404, (it as ClientWebApplicationException).response.status) },
        )
    }

    @Test
    @Order(4)
    fun testCreateValidAppSafeDeposit(asserter: UniAsserter) {
        asserter.assertEquals(
            { consumerClient.createSafeDepositDatabase(SafeDepositDatabaseRequest(appId)).map { it.response } },
            "Database created successfully",
        )
    }

    @Test
    @Order(5)
    fun testGetCredentials(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.readSafeDepositAccessCredentials(appId) },
            { credentials ->
                this.consumerCredentials = credentials
                assertNonEmptyCredentials(credentials)
                createJdbcStatement(consumerCredentials).use {
                    val result = it.executeQuery("SELECT NOW();")
                    result.next()
                    Assertions.assertNotEquals("", result.getString(1))
                }
            },
        )
    }

    @Test
    @Order(6)
    fun testDeleteDeposit(asserter: UniAsserter) {
        asserter.assertNull { consumerClient.deleteSafeDepositDatabase(appId) }
    }

    @Test
    @Order(7)
    fun testDeleteAlreadyDeletedSafeDeposit(asserter: UniAsserter) {
        asserter.assertNull { consumerClient.deleteSafeDepositDatabase(appId) }
    }

    @Test
    @Order(8)
    fun testDatabaseConnectionAfterDeletedSafeDeposit(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { uni { createJdbcStatement(consumerCredentials) } },
            {
                Assertions.assertTrue(it is SQLException)
                Assertions.assertEquals(
                    "3D000",
                    (it as SQLException).sqlState,
                ) // "3D000": error code for non-existent database
            },
        )
    }

    @Test
    @Order(9)
    fun deRegisterRequiredApp(asserter: UniAsserter) {
        asserter.assertNull { appServiceClient.deleteApp(appId) }
    }
}
