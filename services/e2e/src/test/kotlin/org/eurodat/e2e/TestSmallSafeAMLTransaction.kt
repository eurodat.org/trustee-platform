package org.eurodat.e2e

import io.github.oshai.kotlinlogging.KotlinLogging
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.appservice.model.TableSecurityMapping
import org.eurodat.client.datamanagement.model.DataRequest
import org.eurodat.client.transactionservice.AppRequest
import org.eurodat.client.transactionservice.WorkflowRequest
import org.eurodat.e2e.apps.AppRegistrar
import org.eurodat.e2e.apps.FileHelper.Companion.fileToString
import org.eurodat.e2e.assertions.assertTransactionEnded
import org.eurodat.e2e.assertions.assertTransactionStarted
import org.eurodat.e2e.assertions.assertWorkflowSucceeded
import org.eurodat.e2e.clients.ConsumerClient
import org.eurodat.e2e.clients.ProviderClient
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestMethodOrder
import org.junitpioneer.jupiter.DisableIfTestFails

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(
    MethodOrderer.OrderAnnotation::class,
)
@QuarkusTest
@DisableIfTestFails
@RunOnVertxContext
class TestSmallSafeAMLTransaction {
    private val logger = KotlinLogging.logger {}

    private val appId = "safeAMLMock"
    private val imageName = "mock-safeaml-app"
    private val workflowId = "safeaml-mock-workflow"
    private val appRequest = AppRequest(appId = appId)
    private val mockConsumerParticipantId = "dbselector"
    private val mockProviderParticipantId = "cobaselector"

    private var generatedTransactionId = ""
    private var localWorkflowId = ""

    @RestClient
    private lateinit var consumerClient: ConsumerClient

    @RestClient
    private lateinit var providerClient: ProviderClient

    @Inject
    private lateinit var appRegistrar: AppRegistrar

    @Test
    @Order(0)
    fun deRegisterIfExistsTest(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }

    @Test
    @Order(1)
    fun registerRequiredApp(asserter: UniAsserter) {
        asserter.assertNotNull {
            appRegistrar.registerAppWithImageAndWorkflow(
                AppRegistration(
                    appId,
                    fileToString("apps/safeAMLMock/ddl.sql"),
                    fileToString("apps/safeAMLMock/ddl_safedeposit.sql"),
                    listOf(TableSecurityMapping("security_column", "output_transactions")),
                ),
                imageName,
                workflowId,
                listOf("python", "main.py"),
            )
        }
    }

    @Test
    @Order(2)
    fun testTransactionStart(asserter: UniAsserter) {
        asserter.assertThat({ consumerClient.startTransaction(appRequest) }, {
            generatedTransactionId = it.id
            assertTransactionStarted(appId, it)
        })
    }

    @Test
    @Order(4)
    fun insertInputSchema(asserter: UniAsserter) {
        val testData =
            """
            {
              "transaction_id": "00000000-0000-0000-0000-000000000011",
              "bank_transaction_id": "CH7XE",
              "transaction_timestamp": "2019-01-01T02:05:15",
              "created_at": "2020-01-01T02:05:15",
              "originator_iban": "959",
              "counterparty_iban": "450",
              "transaction_type": "TRANSFER",
              "amount": 406.85,
              "originating_bank_bic": "COBADEFFXXX",
              "counterparty_bank_bic": "KARSDE66",
              "currency": "EUR"
            }
            """
        val dataRequest = DataRequest(data = listOf(testData), table = "input_transactions", transactionId = generatedTransactionId)

        asserter.assertThat(
            { consumerClient.insertData(mockConsumerParticipantId, dataRequest) },
            { response ->
                Assertions.assertNotNull(response.result)
            },
        )
    }

    @Test
    @Order(5)
    fun testWorkflowStartAllSafeAMLMock(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.startWorkflow(generatedTransactionId, WorkflowRequest(workflowId)) },
            {
                Assertions.assertEquals(generatedTransactionId, it.transactionId)
                Assertions.assertEquals(workflowId, it.workflowDefinitionId)
                localWorkflowId = it.workflowRunId
            },
        )
    }

    @Test
    @Order(6)
    fun testWorkflowsSucceeded(asserter: UniAsserter) {
        assertWorkflowSucceeded(generatedTransactionId, localWorkflowId, consumerClient, logger, asserter, maxAttempts = 30L)
    }

    @Test
    @Order(7)
    fun testOutputTableDataConsumer(asserter: UniAsserter) {
        asserter.assertThat(
            { providerClient.getData(mockProviderParticipantId, "output_transactions", generatedTransactionId, null) },
            { data ->
                Assertions.assertNotNull(data)
                Assertions.assertEquals(1, data.size)
            },
        )
    }

    @Test
    @Order(8)
    fun testTransactionEnd(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.endTransaction(generatedTransactionId) },
            { assertTransactionEnded(generatedTransactionId, it) },
        )
    }

    @Test
    @Order(9)
    fun deRegisterRequiredApp(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }
}
