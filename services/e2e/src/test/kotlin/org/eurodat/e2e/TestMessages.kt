package org.eurodat.e2e

import io.github.oshai.kotlinlogging.KotlinLogging
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.transactionservice.AppRequest
import org.eurodat.client.transactionservice.WorkflowRequest
import org.eurodat.e2e.apps.AppRegistrar
import org.eurodat.e2e.assertions.assertTransactionEnded
import org.eurodat.e2e.assertions.assertTransactionStarted
import org.eurodat.e2e.assertions.assertWorkflowSucceeded
import org.eurodat.e2e.clients.ConsumerClient
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestMethodOrder
import org.junitpioneer.jupiter.DisableIfTestFails

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@QuarkusTest
@DisableIfTestFails
@RunOnVertxContext
class TestMessages {
    private val testMessage = "Test Message from Mock App"
    private val appId = "messageApp"
    private val imageName = "message-app"
    private val workflowDefinitionId = "send-messages"
    private val testSelector = "dbselector"

    @RestClient
    private lateinit var consumerClient: ConsumerClient

    @Inject
    private lateinit var appRegistrar: AppRegistrar

    private val selectors = listOf(testSelector)
    private val appRequest = AppRequest(appId = appId)
    private var generatedTransactionId = ""
    private var workflowId = ""
    private val logger = KotlinLogging.logger {}

    @Test
    @Order(0)
    fun deRegisterIfExistsTest(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }

    @Test
    @Order(1)
    fun registerRequiredApp(asserter: UniAsserter) {
        asserter.assertNotNull {
            appRegistrar.registerAppWithImageAndWorkflow(
                AppRegistration(
                    appId,
                    "",
                    "",
                    emptyList(),
                ),
                imageName,
                workflowDefinitionId,
                listOf("python", "main.py"),
            )
        }
    }

    @Test
    @Order(2)
    fun testTransactionStart(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.startTransaction(appRequest) },
            {
                generatedTransactionId = it.id
                assertTransactionStarted(appId, it)
            },
        )
    }

    @Test
    @Order(3)
    fun checkNoMessagesYet(asserter: UniAsserter) {
        asserter.assertThat({
            consumerClient.getMessages(appId)
        }, { messages ->
            Assertions.assertTrue(messages.none { it.transactionId == generatedTransactionId })
        })
    }

    @Test
    @Order(4)
    fun startMessageWorkflow(asserter: UniAsserter) {
        asserter.assertThat({
            consumerClient.startWorkflow(generatedTransactionId, WorkflowRequest(workflowDefinitionId))
        }, {
            Assertions.assertEquals(generatedTransactionId, it.transactionId)
            Assertions.assertEquals(workflowDefinitionId, it.workflowDefinitionId)
            workflowId = it.workflowRunId
        })
    }

    @Test
    @Order(5)
    fun testWorkflowsSucceeded(asserter: UniAsserter) {
        assertWorkflowSucceeded(generatedTransactionId, workflowId, consumerClient, logger, asserter)
    }

    @Test
    @Order(6)
    fun checkMessageReceived(asserter: UniAsserter) {
        asserter.assertThat({
            consumerClient.getMessages(appId)
        }, { messages ->
            val messagesForTransaction = messages.filter { it.transactionId == generatedTransactionId }
            Assertions.assertTrue(messagesForTransaction.any { it.label == testMessage }, "Message should be received after workflow starts")
            messagesForTransaction.forEach { message ->
                Assertions.assertNotNull(message.correlationId, "CorrelationId should not be null")
                Assertions.assertTrue(selectors.contains(message.selector), "Selector should be one of the requested selectors")
            }
        })
    }

    @Test
    @Order(7)
    fun testTransactionEnd(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.endTransaction(generatedTransactionId) },
            { assertTransactionEnded(generatedTransactionId, it) },
        )
    }

    @Test
    @Order(8)
    fun checkAllMessagesAreDeletedAfterDeregisteringApp(asserter: UniAsserter) {
        asserter.assertThat({
            consumerClient.getMessages(appId)
        }, { messages ->
            Assertions.assertTrue(messages.none { it.transactionId == generatedTransactionId })
        })
    }

    @Test
    @Order(9)
    fun deleteApp(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }
}
