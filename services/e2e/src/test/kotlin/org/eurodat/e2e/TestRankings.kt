package org.eurodat.e2e

import com.fasterxml.jackson.databind.ObjectMapper
import io.github.oshai.kotlinlogging.KotlinLogging
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.appservice.model.TableSecurityMapping
import org.eurodat.client.datamanagement.model.DataRequest
import org.eurodat.client.transactionservice.AppRequest
import org.eurodat.client.transactionservice.WorkflowRequest
import org.eurodat.e2e.apps.AppRegistrar
import org.eurodat.e2e.apps.FileHelper.Companion.fileToString
import org.eurodat.e2e.assertions.assertTransactionEnded
import org.eurodat.e2e.assertions.assertTransactionStarted
import org.eurodat.e2e.assertions.assertWorkflowSucceeded
import org.eurodat.e2e.clients.ConsumerClient
import org.eurodat.e2e.clients.ProviderClient
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestMethodOrder
import org.junitpioneer.jupiter.DisableIfTestFails

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(
    MethodOrderer.OrderAnnotation::class,
)
@QuarkusTest
@DisableIfTestFails
@RunOnVertxContext
class TestRankings {
    private val appId = "uc3AppMock"
    private val imageName = "uc3-mock-app"
    private val workflowDefinitionId = "data-aggregation-mock"
    private val mockConsumerParticipantId = "dbselector"
    private val mockProviderParticipantId = "cobaselector"

    private val appRequest = AppRequest(appId = appId)
    private var workflowId = ""
    private val logger = KotlinLogging.logger {}
    private val objectMapper = ObjectMapper()

    @RestClient
    private lateinit var consumerClient: ConsumerClient

    @RestClient
    private lateinit var providerClient: ProviderClient

    private var generatedTransactionId = ""

    @Inject
    private lateinit var appRegistrar: AppRegistrar

    @Test
    @Order(0)
    fun deRegisterIfExistsTest(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }

    @Test
    @Order(1)
    fun registerRequiredApp(asserter: UniAsserter) {
        asserter.assertNotNull {
            appRegistrar.registerAppWithImageAndWorkflow(
                AppRegistration(
                    appId,
                    fileToString("apps/uc3Mock/ddl.sql"),
                    "",
                    listOf(TableSecurityMapping("participant_identifier", "output")),
                ),
                imageName,
                workflowDefinitionId,
                listOf("python", "main.py"),
            )
        }
    }

    @Test
    @Order(2)
    fun preProcessing(asserter: UniAsserter) {
        asserter.assertThat(
            { providerClient.startTransaction(appRequest) },
            {
                generatedTransactionId = it.id
                assertTransactionStarted(appId, it)
            },
        )
    }

    @Test
    @Order(3)
    fun sendFirstRanking(asserter: UniAsserter) {
        val testData =
            """
            {
              "input_id": 1,
              "poll_id": 1,
              "submission": 5,
              "participant_identifier": "cobaselector",
              "created_at": "2019-01-01 13:53:24"
            }
            """
        val dataRequest = DataRequest(data = listOf(testData), table = "input", transactionId = generatedTransactionId)

        asserter.assertThat(
            { consumerClient.insertData(mockConsumerParticipantId, dataRequest) },
            { response ->
                Assertions.assertNotNull(response.result)
            },
        )
    }

    @Test
    @Order(4)
    fun sendSecondRanking(asserter: UniAsserter) {
        val testData =
            """
            {
              "input_id": 2,
              "poll_id": 1,
              "submission": 3,
              "participant_identifier": "dbselector",
              "created_at": "2019-01-01 02:05:15"
            }
            """
        val dataRequest = DataRequest(data = listOf(testData), table = "input", transactionId = generatedTransactionId)

        asserter.assertThat(
            { consumerClient.insertData(mockConsumerParticipantId, dataRequest) },
            { response ->
                Assertions.assertNotNull(response.result)
            },
        )
    }

    @Test
    @Order(5)
    fun workflowExecution(asserter: UniAsserter) {
        asserter.assertThat(
            { providerClient.startWorkflow(generatedTransactionId, WorkflowRequest(workflowDefinitionId)) },
            {
                Assertions.assertEquals(generatedTransactionId, it.transactionId)
                Assertions.assertEquals(workflowDefinitionId, it.workflowDefinitionId)
                workflowId = it.workflowRunId
            },
        )
    }

    @Test
    @Order(6)
    fun testWorkflowsSucceeded(asserter: UniAsserter) {
        assertWorkflowSucceeded(generatedTransactionId, workflowId, consumerClient, logger, asserter)
    }

    @Test
    @Order(7)
    fun getRankingAsProvider(asserter: UniAsserter) {
        asserter.assertThat(
            { providerClient.getData(mockProviderParticipantId, "output", generatedTransactionId, null) },
            { data ->
                Assertions.assertNotNull(data)
                Assertions.assertEquals(1, data.size)
                val jsonNode = objectMapper.readTree(data[0])
                val result = jsonNode.get("result").asInt()
                val participantIdentifier = jsonNode.get("participant_identifier").asText()

                Assertions.assertEquals(8, result)
                Assertions.assertEquals("cobaselector", participantIdentifier)
            },
        )
    }

    @Test
    @Order(8)
    fun getRankingAsConsumer(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.getData(mockConsumerParticipantId, "output", generatedTransactionId, null) },
            { data ->
                Assertions.assertNotNull(data)
                Assertions.assertEquals(1, data.size)
                val jsonNode = objectMapper.readTree(data[0])
                val result = jsonNode.get("result").asInt()
                val participantIdentifier = jsonNode.get("participant_identifier").asText()

                Assertions.assertEquals(8, result)
                Assertions.assertEquals("dbselector", participantIdentifier)
            },
        )
    }

    @Test
    @Order(9)
    fun notGetConsumerRankingAsProvider(asserter: UniAsserter) {
        asserter.assertThat(
            { providerClient.getData(mockProviderParticipantId, "output", generatedTransactionId, null) },
            { data ->
                Assertions.assertNotNull(data)
                data.forEach {
                    Assertions.assertNotEquals("dbselector", objectMapper.readTree(it).get("participant_identifier").asText())
                }
            },
        )
    }

    @Test
    @Order(10)
    fun notGetProviderRankingAsConsumer(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.getData(mockConsumerParticipantId, "output", generatedTransactionId, null) },
            { data ->
                Assertions.assertNotNull(data)
                data.forEach {
                    Assertions.assertNotEquals("cobaselector", objectMapper.readTree(it).get("participant_identifier").asText())
                }
            },
        )
    }

    @Test
    @Order(11)
    fun postProcess(asserter: UniAsserter) {
        asserter.assertThat(
            { providerClient.endTransaction(generatedTransactionId) },
            { assertTransactionEnded(generatedTransactionId, it) },
        )
    }

    @Test
    @Order(12)
    fun deRegisterRequiredApp(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }
}
