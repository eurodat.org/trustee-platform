package org.eurodat.e2e

import com.fasterxml.jackson.databind.ObjectMapper
import io.github.oshai.kotlinlogging.KotlinLogging
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import jakarta.ws.rs.WebApplicationException
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.ClientError
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.datamanagement.model.DataRequest
import org.eurodat.client.transactionservice.AppRequest
import org.eurodat.client.transactionservice.WorkflowRequest
import org.eurodat.e2e.apps.AppRegistrar
import org.eurodat.e2e.apps.FileHelper.Companion.fileToString
import org.eurodat.e2e.assertions.assertTransactionEnded
import org.eurodat.e2e.assertions.assertTransactionStarted
import org.eurodat.e2e.assertions.assertWorkflowSucceeded
import org.eurodat.e2e.clients.AnonymousClient
import org.eurodat.e2e.clients.ConsumerClient
import org.eurodat.e2e.clients.ProviderClient
import org.jboss.resteasy.reactive.ClientWebApplicationException
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestMethodOrder
import org.junitpioneer.jupiter.DisableIfTestFails
import java.util.UUID

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@QuarkusTest
@DisableIfTestFails
@RunOnVertxContext
class TestDataManagement {
    private val logger = KotlinLogging.logger {}
    private val appId = "transactionCopyApp"
    private val imageName = "transaction-copy-app"
    private val workflowDefinitionId = "transaction-copy"
    private val consumerParticipantId = "dbselector"
    private var workflowId = ""
    private val testSelector = "dbselector"
    private val testMessage = "Test Message from Transaction Copy App"

    private val testData = """
            {
              "transaction_id": "00000000-0000-0000-0000-000000000010",
              "bank_transaction_id": "CD7XE",
              "transaction_timestamp": "2019-01-01T12:00:00",
              "created_at": "2020-01-01T12:00:00",
              "originating_bank_bic": "COBASEFFXXX",
              "counterparty_bank_bic": "KARSDS66",
              "originator_iban": "959",
              "counterparty_iban": "450",
              "amount": 400,
              "currency": "EUR",
              "transaction_type": "TRANSFER"
            }
            """

    @RestClient
    private lateinit var consumerClient: ConsumerClient

    @RestClient
    private lateinit var providerClient: ProviderClient

    @RestClient
    private lateinit var anonymousClient: AnonymousClient

    @Inject
    private lateinit var appRegistrar: AppRegistrar

    @Inject
    private lateinit var objectMapper: ObjectMapper

    private val selectors = listOf(testSelector)
    private val appRequest = AppRequest(appId = appId)
    private var generatedTransactionId = ""
    private var correlationId: Long? = null

    @Test
    @Order(0)
    fun deRegisterIfExistsTest(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }

    @Test
    @Order(1)
    fun registerRequiredApp(asserter: UniAsserter) {
        asserter.assertNotNull {
            appRegistrar.registerAppWithImageAndWorkflow(
                AppRegistration(
                    appId,
                    fileToString("apps/transactionCopyApp/ddl.sql"),
                    "",
                    emptyList(),
                ),
                imageName,
                workflowDefinitionId,
                listOf("python", "main.py"),
            )
        }
    }

    @Test
    @Order(2)
    fun testTransactionStart(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.startTransaction(appRequest) },
            {
                generatedTransactionId = it.id
                assertTransactionStarted(appId, it)
            },
        )
    }

    @Test
    @Order(3)
    fun testInsertDataTransactionIdNotFound(asserter: UniAsserter) {
        val nonExistingTransactionId = UUID.randomUUID().toString()
        val dataRequest =
            DataRequest(data = listOf(testData), table = "input_transactions", transactionId = nonExistingTransactionId)

        asserter.assertFailedWith(
            { consumerClient.insertData(consumerParticipantId, dataRequest) },
            { exception ->
                Assertions.assertTrue(exception is ClientWebApplicationException)
                Assertions.assertEquals(404, (exception as ClientWebApplicationException).response.status)
            },
        )
    }

    @Test
    @Order(4)
    fun testInsertDataTableIdNotFound(asserter: UniAsserter) {
        val dataRequest =
            DataRequest(data = listOf(testData), table = "nonExistingTable", transactionId = generatedTransactionId)

        asserter.assertFailedWith(
            { consumerClient.insertData(consumerParticipantId, dataRequest) },
            { exception ->
                Assertions.assertTrue(exception is ClientWebApplicationException)
                Assertions.assertEquals(404, (exception as ClientWebApplicationException).response.status)
            },
        )
    }

    @Test
    @Order(5)
    fun testInsertInvalidData(asserter: UniAsserter) {
        val thisIsNotAFloat = "abc"
        val testData =
            """
            {
              "transaction_id": "00000000-0000-0000-0000-000000000010",
              "bank_transaction_id": "CD7XE",
              "transaction_timestamp": "2019-01-01T12:00:00",
              "created_at": "2020-01-01T12:00:00",
              "originating_bank_bic": "COBASEFFXXX",
              "counterparty_bank_bic": "KARSDS66",
              "originator_iban": "959",
              "counterparty_iban": "450",
              "amount": "$thisIsNotAFloat",
              "currency": "EUR",
              "transaction_type": "TRANSFER"
            }
            """

        val dataRequest = DataRequest(data = listOf(testData), table = "input_transactions", transactionId = generatedTransactionId)

        asserter.assertFailedWith(
            { consumerClient.insertData(consumerParticipantId, dataRequest) },
            {
                val badRequest = it as ClientWebApplicationException
                Assertions.assertTrue(badRequest.response.status == 400, "Status code should be 400")
                Assertions.assertTrue(
                    badRequest.response
                        .readEntity(ClientError::class.java)
                        .error
                        .startsWith("Data Exception: "),
                    "Error message should start with 'Data Exception: '",
                )
            },
        )
    }

    @Test
    @Order(5)
    fun testInsertDataSuccessful(asserter: UniAsserter) {
        val dataRequest =
            DataRequest(data = listOf(testData), table = "input_transactions", transactionId = generatedTransactionId)

        asserter.assertThat(
            { consumerClient.insertData(consumerParticipantId, dataRequest) },
            { response ->
                Assertions.assertNotNull(response.result)
            },
        )
    }

    @Test
    @Order(6)
    fun testInsertDataAnonymous(asserter: UniAsserter) {
        val dataRequest =
            DataRequest(data = listOf(testData), table = "input_transactions", transactionId = generatedTransactionId)

        asserter.assertFailedWith(
            { anonymousClient.insertData(consumerParticipantId, dataRequest) },
            { exception ->
                Assertions.assertTrue(exception is ClientWebApplicationException)
                Assertions.assertEquals(401, (exception as ClientWebApplicationException).response.status)
            },
        )
    }

    @Test
    @Order(7)
    fun testWorkflowStartDataManagement(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.startWorkflow(generatedTransactionId, WorkflowRequest(workflowDefinitionId)) },
            {
                Assertions.assertEquals(generatedTransactionId, it.transactionId)
                Assertions.assertEquals(workflowDefinitionId, it.workflowDefinitionId)
                workflowId = it.workflowRunId
            },
        )
    }

    @Test
    @Order(8)
    fun testWorkflowsSucceeded(asserter: UniAsserter) {
        assertWorkflowSucceeded(generatedTransactionId, workflowId, consumerClient, logger, asserter)
    }

    @Test
    @Order(9)
    fun testGetCorrelationId(asserter: UniAsserter) {
        asserter.assertThat({
            consumerClient.getMessages(appId)
        }, { messages ->
            val messagesForTransaction = messages.filter { it.transactionId == generatedTransactionId }
            Assertions.assertTrue(messagesForTransaction.any { it.label == testMessage }, "Message should be received after workflow starts")
            messagesForTransaction.forEach { message ->
                correlationId = message.correlationId
                Assertions.assertNotNull(message.correlationId, "CorrelationId should not be null")
                Assertions.assertTrue(selectors.contains(message.selector), "Selector should be one of the requested selectors")
            }
        })
    }

    @Test
    @Order(10)
    fun testGetDataTransactionIdNotFound(asserter: UniAsserter) {
        val nonExistingTransactionId = UUID.randomUUID().toString()
        asserter.assertFailedWith(
            { consumerClient.getData(consumerParticipantId, "output_transactions", nonExistingTransactionId, correlationId) },
            { exception ->
                Assertions.assertTrue(exception is ClientWebApplicationException)
                Assertions.assertEquals(404, (exception as ClientWebApplicationException).response.status)
            },
        )
    }

    @Test
    @Order(11)
    fun testGetDataTableNotFound(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { consumerClient.getData(consumerParticipantId, "nonExistingTable", generatedTransactionId, correlationId) },
            { exception ->
                Assertions.assertTrue(exception is ClientWebApplicationException)
                Assertions.assertEquals(404, (exception as ClientWebApplicationException).response.status)
            },
        )
    }

    @Test
    @Order(12)
    fun testGetDataSuccessful(asserter: UniAsserter) {
        val jsonData =
            """
            {
              "transaction_id": "00000000-0000-0000-0000-000000000010",
              "bank_transaction_id": "CD7XE",
              "transaction_timestamp": "2019-01-01T12:00:00",
              "created_at": "2020-01-01T12:00:00",
              "originating_bank_bic": "COBASEFFXXX",
              "counterparty_bank_bic": "KARSDS66",
              "originator_iban": "959",
              "counterparty_iban": "450",
              "amount": 400,
              "currency": "EUR",
              "transaction_type": "TRANSFER",
              "security_column": "dummy_security_column",
              "correlation_id": $correlationId
            }
            """
        val expectedJsonNode = objectMapper.readTree(jsonData)
        asserter.assertThat(
            { consumerClient.getData(consumerParticipantId, "output_transactions", generatedTransactionId, correlationId) },
            { data ->
                assert(data.isNotEmpty())
                assert(expectedJsonNode == objectMapper.readTree(data[0]))
            },
        )
    }

    @Test
    @Order(13)
    fun testGetDataWithInvalidParticipantClaim(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { providerClient.getData(consumerParticipantId, "output_transactions", generatedTransactionId, null) },
            {
                Assertions.assertTrue(it is WebApplicationException && it.response.status == 403)
            },
        )
    }

    @Test
    @Order(14)
    fun testTransactionEnd(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.endTransaction(generatedTransactionId) },
            { assertTransactionEnded(generatedTransactionId, it) },
        )
    }

    @Test
    @Order(15)
    fun testGetDataEndedTransaction(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { consumerClient.getData(consumerParticipantId, "output_transactions", generatedTransactionId, null) },
            {
                Assertions.assertTrue(it is WebApplicationException && it.response.status == 404)
            },
        )
    }

    @Test
    @Order(16)
    fun deleteApp(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }
}
