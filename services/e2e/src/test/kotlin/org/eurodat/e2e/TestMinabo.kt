package org.eurodat.e2e

import io.github.oshai.kotlinlogging.KotlinLogging
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import org.awaitility.Awaitility
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.credential.Credentials
import org.eurodat.client.database.SafeDepositDatabaseRequest
import org.eurodat.client.transactionservice.AppRequest
import org.eurodat.client.transactionservice.WorkflowRequest
import org.eurodat.e2e.apps.AppRegistrar
import org.eurodat.e2e.apps.FileHelper.Companion.fileToString
import org.eurodat.e2e.assertions.assertInDbWithCredentials
import org.eurodat.e2e.assertions.assertNonEmptyCredentials
import org.eurodat.e2e.assertions.assertTransactionEnded
import org.eurodat.e2e.assertions.assertTransactionStarted
import org.eurodat.e2e.assertions.assertWorkflowSucceeded
import org.eurodat.e2e.clients.ConsumerClient
import org.eurodat.e2e.jdbc.createJdbcStatement
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestMethodOrder
import org.junitpioneer.jupiter.DisableIfTestFails
import java.util.concurrent.TimeUnit

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(
    MethodOrderer.OrderAnnotation::class,
)
@QuarkusTest
@DisableIfTestFails
@RunOnVertxContext
class TestMinabo {
    private val appId = "minaboApp"
    private val imageName = "minabo-app"
    private val workflowDefinitionId = "start-minabo"
    private val appRequest = AppRequest(appId = appId, consumer = listOf("dbselector"), provider = listOf("dbselector"))

    private var generatedTransactionId = ""
    private lateinit var consumerCredentials: Credentials
    private var workflowId = ""
    private val logger = KotlinLogging.logger {}

    @RestClient
    private lateinit var consumerClient: ConsumerClient

    @Inject
    private lateinit var appRegistrar: AppRegistrar

    @Test
    @Order(0)
    fun deRegisterIfExistsTest(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }

    @Test
    @Order(1)
    fun registerRequiredApp(asserter: UniAsserter) {
        asserter.assertNotNull {
            appRegistrar.registerAppWithImageAndWorkflow(
                AppRegistration(
                    appId,
                    fileToString("apps/minabo/ddl.sql"),
                    fileToString("apps/minabo/ddl_safedeposit.sql"),
                    emptyList(),
                ),
                imageName,
                workflowDefinitionId,
                listOf("python", "main.py"),
            )
        }
    }

    @Test
    @Order(2)
    fun ensureNoSafeDepositBoxExistsOrIsDeleted(asserter: UniAsserter) {
        asserter.assertNull { consumerClient.deleteSafeDepositDatabase(appId) }
    }

    @Test
    @Order(3)
    fun testCreateSafeDepositBox(asserter: UniAsserter) {
        asserter.assertEquals(
            { consumerClient.createSafeDepositDatabase(SafeDepositDatabaseRequest(appId)).map { it.response } },
            "Database created successfully",
        )
    }

    @Test
    @Order(4)
    fun testGetCredentialsSafeDeposit(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.readSafeDepositAccessCredentials(appId) },
            { credentials ->
                this.consumerCredentials = credentials
                assertNonEmptyCredentials(credentials)
                createJdbcStatement(credentials).use {
                    val result = it.executeQuery("SELECT count(*) FROM safedeposit.json;")
                    result.next()
                    Assertions.assertEquals(0, result.getInt(1))
                }
            },
        )
    }

    @Test
    @Order(5)
    fun testInsertDataInSafeDeposit(asserter: UniAsserter) {
        asserter.assertInDbWithCredentials(consumerCredentials) {
            val query =
                "INSERT INTO safedeposit.json (uuid_json, blob_json)" +
                    " VALUES('00000000-0000-0000-0000-000000000011','{\n" +
                    "    \"fiscalYearStart\": \"2024-05-07\",\n" +
                    "    \"referencedReports\": {\n" +
                    "        \"ESEFReport\": {\n" +
                    "            \"fileReference\": \"50a36c418baffd520bb92d84664f06f9732a21f4e2e5ecee6d9136f16e7e0b63\",\n" +
                    "            \"fileName\": \"ESEFReport\",\n" +
                    "            \"isGroupLevel\": \"Yes\",\n" +
                    "            \"reportDate\": null,\n" +
                    "            \"currency\": \"JPY\"\n" +
                    "        }\n" +
                    "    }\n" +
                    "}');" +
                    "INSERT INTO safedeposit.pdf (uuid_pdf, blob_pdf)" +
                    "VALUES('00000000-0000-0000-0000-000000000012', '\\x013d7d16d7ad4fefb61bd95b765c8ceb'::bytea)," +
                    "('00000000-0000-0000-0000-000000000013', '\\x013d7d16d7ad4fefb61bd95b765c8abc'::bytea)," +
                    "('00000000-0000-0000-0000-000000000014', '\\x013d7d16d7ad4fefb61bd95b765c8ab2'::bytea);"

            it.executeUpdate(query)

            val resultJSON = it.executeQuery("SELECT count(*) FROM safedeposit.json;")
            resultJSON.next()
            Assertions.assertEquals(1, resultJSON.getInt(1))

            val resultPDF = it.executeQuery("SELECT count(*) FROM safedeposit.pdf;")
            resultPDF.next()
            Assertions.assertEquals(3, resultPDF.getInt(1))
        }
    }

    @Test
    @Order(6)
    fun testTransactionStart(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.startTransaction(appRequest) },
            {
                generatedTransactionId = it.id
                assertTransactionStarted(appId, it)
            },
        )
    }

    @Test
    @Order(7)
    fun testInsertDataInInputDB(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.readExternalAccessCredentials(generatedTransactionId) },
            { response ->
                consumerCredentials = response
                val query =
                    "INSERT INTO input.json (uuid_json) VALUES('00000000-0000-0000-0000-000000000011');" +
                        "INSERT INTO input.pdf (uuid_pdf) VALUES('00000000-0000-0000-0000-000000000012')," +
                        "('00000000-0000-0000-0000-000000000014');"
                createJdbcStatement(consumerCredentials).use {
                    it.executeUpdate(query)

                    val resultJSON = it.executeQuery("SELECT count(*) FROM input.json;")
                    resultJSON.next()
                    Assertions.assertEquals(1, resultJSON.getInt(1))

                    val resultPDF = it.executeQuery("SELECT count(*) FROM input.pdf;")
                    resultPDF.next()
                    Assertions.assertEquals(2, resultPDF.getInt(1))
                }
            },
        )
    }

    @Test
    @Order(8)
    fun testWorkflowStartMinabo(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.startWorkflow(generatedTransactionId, WorkflowRequest(workflowDefinitionId)) },
            {
                Assertions.assertEquals(generatedTransactionId, it.transactionId)
                Assertions.assertEquals(workflowDefinitionId, it.workflowDefinitionId)
                workflowId = it.workflowRunId
            },
        )
    }

    @Test
    @Order(9)
    fun testOutputInTransactionDB(asserter: UniAsserter) {
        asserter.assertInDbWithCredentials(consumerCredentials) {
            Awaitility.await().atMost(2, TimeUnit.MINUTES).untilAsserted {
                val result = it.executeQuery("SELECT count(*) FROM output.json;")
                result.next()
                Assertions.assertEquals(1, result.getInt(1))
            }
            Awaitility.await().atMost(2, TimeUnit.MINUTES).untilAsserted {
                val result = it.executeQuery("SELECT count(*) FROM output.pdf;")
                result.next()
                Assertions.assertEquals(2, result.getInt(1))
            }
        }
    }

    @Test
    @Order(10)
    fun testSafeDepositEnd(asserter: UniAsserter) {
        asserter.assertNull { consumerClient.deleteSafeDepositDatabase(appId) }
    }

    @Test
    @Order(11)
    fun testTransactionEnd(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.endTransaction(generatedTransactionId) },
            { assertTransactionEnded(generatedTransactionId, it) },
        )
    }

    @Test
    @Order(12)
    fun testWorkflowsSucceeded(asserter: UniAsserter) {
        assertWorkflowSucceeded(generatedTransactionId, workflowId, consumerClient, logger, asserter)
    }

    @Test
    @Order(13)
    fun deRegisterRequiredApp(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }
}
