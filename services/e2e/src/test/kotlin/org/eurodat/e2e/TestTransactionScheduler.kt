package org.eurodat.e2e

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.transactionservice.AppRequest
import org.eurodat.e2e.apps.AppRegistrar
import org.eurodat.e2e.apps.FileHelper.Companion.fileToString
import org.eurodat.e2e.assertions.assertTransactionStarted
import org.eurodat.e2e.clients.ConsumerClient
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestMethodOrder
import org.junitpioneer.jupiter.DisableIfTestFails
import java.time.Duration
import java.time.LocalDateTime

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(
    MethodOrderer.OrderAnnotation::class,
)
@QuarkusTest
@DisableIfTestFails
class TestTransactionScheduler {
    private val appId = "helloWorldApp"
    private val imageName = "uc2-mock-app" // We could use any arbitrary image here
    private val workflowDefinitionId = "hello-world"
    private val appRequest =
        AppRequest(appId = appId, consumer = listOf("dbselector"), provider = listOf("someProvider"))
    private var generatedTransactionId = ""

    @Inject
    private lateinit var appRegistrar: AppRegistrar

    @RestClient
    private lateinit var consumerClient: ConsumerClient

    @Test
    @Order(0)
    @RunOnVertxContext
    fun deRegisterIfExistsTest(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }

    @Test
    @Order(1)
    @RunOnVertxContext
    fun registerRequiredApp(asserter: UniAsserter) {
        asserter.assertNotNull {
            appRegistrar.registerAppWithImageAndWorkflow(
                AppRegistration(
                    appId,
                    fileToString("apps/helloWorld/ddl.sql"),
                    "",
                    emptyList(),
                    0,
                ),
                imageName,
                workflowDefinitionId,
                listOf("echo", "hello"),
            )
        }
    }

    @Test
    @Order(2)
    @RunOnVertxContext
    fun createTransaction(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.startTransaction(appRequest) },
            {
                generatedTransactionId = it.id
                assertTransactionStarted(appId, it)
            },
        )
    }

    @Test
    @Order(3)
    fun waitForDeletionOfTransaction() {
        val maxWaitTime = LocalDateTime.now().plusSeconds(75)
        var isTransactionWiped = false
        while (LocalDateTime.now() < maxWaitTime) {
            val transactionEndTime =
                consumerClient
                    .findTransaction(generatedTransactionId)
                    .await()
                    .atMost(Duration.ofSeconds(5))
                    .endTime
            if (transactionEndTime != null) {
                isTransactionWiped = true
                break
            }
            Thread.sleep(5000)
        }
        assertTrue(isTransactionWiped)
    }

    @Test
    @Order(4)
    @RunOnVertxContext
    fun deRegisterRequiredAppAndDeleteTransaction(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }
}
