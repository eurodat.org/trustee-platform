package org.eurodat.e2e

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.uni
import jakarta.ws.rs.WebApplicationException
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.contractservice.model.Contract
import org.eurodat.client.contractservice.model.ContractRequest
import org.eurodat.e2e.clients.ContractServiceOidcClient
import org.eurodat.e2e.clients.SubmissionServiceClient
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestMethodOrder
import org.junitpioneer.jupiter.DisableIfTestFails

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@QuarkusTest
@DisableIfTestFails
@RunOnVertxContext
class TestContracts {
    @RestClient
    private lateinit var contractClient: ContractServiceOidcClient

    @RestClient
    private lateinit var submissionClient: SubmissionServiceClient

    private val contractType = "ONBOARDING_COMPANY_MASTERDATA"
    private val testPaperlessId = "12345678-ab12-34cd-ef56-123456789abc"
    private val appId: String? = null
    private lateinit var createdContract: Contract // Will be set in the first test
    private lateinit var signedContract: Contract // Will be set in the third test
    private lateinit var createdTermsAndConditionsContract: Contract // Will be set in the third test
    private lateinit var signedTermsAndConditionsContract: Contract // Will be set in the fourth test

    @ConfigProperty(name = "paperless.password")
    private lateinit var paperlessToken: String
    private val testSubmissionId = 276905
    private val testSubmissionTnCId = 276906

    private fun createTestSubmissionJson(submissionId: Int): JsonNode {
        val objectMapper = ObjectMapper()

        val jsonString = """
        {
           "event":"submission.completed",
           "object":{
              "completed_at":"2024-07-12T10:49:41.814Z",
              "submittable":{
                 "submission_id": $submissionId
              },
              "aggregated_submit_event_values":[
                 {
                    "slug":"companydirectors_slug",
                    "value":[
                       "$testPaperlessId"
                    ],
                    "mapped_value":"[\"$testPaperlessId\"]"
                 },
                 {
                    "slug":"companydirectors_slug.$testPaperlessId.email_slug",
                    "value":"info+e2e@eurodat.org",
                    "mapped_value":"info+e2e@eurodat.org"
                 },
                 {
                    "slug":"companydirectors_slug.$testPaperlessId.companydirectorsfirstname_slug",
                    "value":"Technical",
                    "mapped_value":"Technical"
                 },
                 {
                    "slug":"companydirectors_slug.$testPaperlessId.companydirectorslastname_slug",
                    "value":"Dummy",
                    "mapped_value":"Dummy"
                 },
                 {
                    "slug":"companydirectors_slug.$testPaperlessId.radio3",
                    "mapped_value":"Partner"
                 },
                 {
                    "slug":"companyname_slug",
                    "value":"Test Company"
                 },
                 {
                    "slug":"companystreet_slug",
                    "value":"Test Street 2",
                    "mapped_value":"Test Street 2"
                 },
                 {
                    "slug":"companyzip_slug",
                    "value":"11232"
                 },
                 {
                    "slug":"companytown_slug",
                    "value":"FFm"
                 },
                 {
                    "slug":"companycountry_slug",
                    "value":"asd"
                 },
                 {
                    "slug":"companyregisterlocation_slug",
                    "value":"loc"
                 },
                 {
                    "slug":"companyregisterno_slug",
                    "value":"asdfkblw"
                 },
                 {
                    "slug":"companyvatid_slug",
                    "value":"de123456"
                 }
              ]
           }
        }
        """
        return objectMapper.readTree(jsonString)
    }

    /**
     * Clean up in case the ContractService has a contract for the given owner already,
     * i.e. if last test got cancelled
     */
    @Test
    @Order(0)
    fun testTerminateExistingContracts(asserter: UniAsserter) {
        asserter.assertThat(
            {
                contractClient.getAllContracts().chain { contracts ->
                    val uniJoinBuilder = Uni.join().builder<Unit>()
                    uniJoinBuilder.add(uni {})
                    contracts.forEach { contract ->
                        uniJoinBuilder.add(contractClient.terminateContract(contract.id))
                    }
                    uniJoinBuilder.joinAll().andFailFast()
                }
            },
            {
                Assertions.assertNotNull(it) // asserting that the deletion of the contracts did not fail
            },
        )
    }

    /**
     * This test sets the ownerId for the following tests.
     */
    @Test
    @Order(1)
    fun testCreateContract(asserter: UniAsserter) {
        asserter.assertThat({
            // when
            contractClient.createContract(ContractRequest(contractType, appId))
        }, {
            // then
            createdContract = it
            Assertions.assertEquals(contractType, it.contractType)
            Assertions.assertNotNull(it.id)
            Assertions.assertEquals(appId, it.appId)
            Assertions.assertNull(it.contractValidFrom)
            Assertions.assertNull(it.contractValidTo)
            Assertions.assertEquals("SENT", it.status)
            Assertions.assertNotNull(it.companyId)
        })
    }

    @Test
    @Order(2)
    fun testFindContract(asserter: UniAsserter) {
        asserter.assertThat({
            contractClient.findContract(createdContract.id)
        }, {
            Assertions.assertEquals(createdContract, it)
        })
    }

    @Test
    @Order(2)
    fun testGetAllContracts(asserter: UniAsserter) {
        asserter.assertThat({
            contractClient.getAllContracts()
        }, {
            Assertions.assertTrue(it.contains(createdContract))
        })
    }

    @Test
    @Order(3)
    fun testProcessSubmission(asserter: UniAsserter) {
        val submissionBody: JsonNode = createTestSubmissionJson(testSubmissionId)
        asserter.assertThat({
            submissionClient.processSubmission(paperlessToken, submissionBody)
        }, {
            Assertions.assertTrue(it is Unit)
        })

        asserter.assertThat(
            {
                contractClient.findContract(createdContract.id)
            },
            {
                signedContract = it
                Assertions.assertEquals("SIGNED", it.status)
                Assertions.assertEquals(createdContract.companyId, it.companyId)
            },
        )

        // Fetch all contracts and filter the newly created Terms & Conditions contract
        asserter.assertThat(
            {
                contractClient.getAllContracts()
            },
            { contracts ->
                val termsAndConditionsContract =
                    contracts.find { contract ->
                        contract.contractType == "TERMS_AND_CONDITIONS_1"
                    }
                Assertions.assertNotNull(termsAndConditionsContract, "TERMS_AND_CONDITIONS_1 contract was not found")
                createdTermsAndConditionsContract = termsAndConditionsContract!!
            },
        )

        // verify that a Terms & Conditions contract is created
        asserter.assertThat(
            {
                contractClient.findContract(createdTermsAndConditionsContract.id)
            },
            {
                createdTermsAndConditionsContract = it
                Assertions.assertNotNull(createdTermsAndConditionsContract)
                Assertions.assertEquals("SENT", it.status)
                Assertions.assertEquals(1, it.participants!!.size)
                Assertions.assertEquals("info+e2e@eurodat.org", it.participants!![0].email)
                Assertions.assertEquals("Technical", it.participants!![0].firstName)
                Assertions.assertEquals("Dummy", it.participants!![0].lastName)
                Assertions.assertEquals("Partner", it.participants!![0].role)
                Assertions.assertEquals("sent", it.participants!![0].status)
            },
        )
    }

    @Test
    @Order(4)
    fun testProcessTermsAndConditionsSubmission(asserter: UniAsserter) {
        val submissionBody: JsonNode = createTestSubmissionJson(testSubmissionTnCId)
        asserter.assertThat({
            submissionClient.processSubmission(paperlessToken, submissionBody)
        }, {
            Assertions.assertTrue(it is Unit)
        })

        asserter.assertThat(
            {
                contractClient.findContract(createdTermsAndConditionsContract.id)
            },
            {
                signedTermsAndConditionsContract = it
                Assertions.assertEquals("SIGNED", it.status)
            },
        )
    }

    @Test
    @Order(5)
    fun testTerminateContract(asserter: UniAsserter) {
        asserter.assertNull { contractClient.terminateContract(signedContract.id) }
        // terminate Onboarding contract should terminate also signed T&C contract
    }

    @Test
    @Order(6)
    fun testTerminateAlreadyTerminatedContract(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { contractClient.terminateContract(signedTermsAndConditionsContract.id) },
            { it is WebApplicationException && it.response.status == 404 },
        )
    }

    @Test
    @Order(6)
    fun testFindNotExistentContract(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { contractClient.terminateContract("NON_EXISTENT_ID") },
            { it is WebApplicationException && it.response.status == 404 },
        )
    }

    @Test
    @Order(6)
    fun testFindTerminatedContract(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { contractClient.terminateContract(signedTermsAndConditionsContract.id) },
            { it is WebApplicationException && it.response.status == 404 },
        )
    }

    /*
    After the contract is terminated, it should not be contained in the list of all contracts of this owner.
     */
    @Test
    @Order(6)
    fun testGetAllContractsAfterTermination(asserter: UniAsserter) {
        asserter.assertThat({
            contractClient.getAllContracts()
        }, {
            Assertions.assertTrue(!it.contains(signedTermsAndConditionsContract))
        })
    }
}
