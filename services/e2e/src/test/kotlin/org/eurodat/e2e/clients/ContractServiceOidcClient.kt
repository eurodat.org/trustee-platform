package org.eurodat.e2e.clients

import io.quarkus.oidc.client.filter.OidcClientFilter
import io.smallrye.mutiny.Uni
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.MediaType
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import org.eurodat.client.contractservice.model.Contract
import org.eurodat.client.contractservice.model.ContractRequest

@RegisterRestClient(configKey = "contract-service-external")
@Path("/contracts")
@OidcClientFilter("contract-service-external")
interface ContractServiceOidcClient {
    /**
     * Retrieves a contract based on the specified contract ID.
     *
     * @param contractId The ID of the contract to retrieve.
     * @return The contract.
     */
    @GET
    @Path("/{contractId}")
    @Produces(MediaType.APPLICATION_JSON)
    fun findContract(
        @PathParam("contractId") contractId: String,
    ): Uni<Contract>

    /**
     * Retrieves all contracts.
     *
     * @return A list of all contracts.
     */
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    fun getAllContracts(): Uni<List<Contract>>

    /**
     * Terminates a contract based on the specified contract ID by setting the validation-to-time to now.
     *
     * @param contractId The ID of the contract to terminate.
     * @return A Unit.
     */
    @DELETE
    @Path("/{contractId}")
    fun terminateContract(
        @PathParam("contractId") contractId: String,
    ): Uni<Unit>

    /**
     * Creates a new contract.
     *
     * @param contractRequest
     * @return The created contract.
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    fun createContract(contractRequest: ContractRequest): Uni<Contract>
}
