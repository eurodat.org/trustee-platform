package org.eurodat.e2e.clients

import jakarta.ws.rs.Path
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import org.eurodat.client.EurodatClientBase

@RegisterRestClient(configKey = "anonymous")
@Path("")
interface AnonymousClient : EurodatClientBase
