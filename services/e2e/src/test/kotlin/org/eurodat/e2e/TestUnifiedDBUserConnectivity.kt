package org.eurodat.e2e

import com.fasterxml.jackson.databind.ObjectMapper
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.uni
import jakarta.inject.Inject
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.AppServiceExternalClient
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.appservice.model.TableSecurityMapping
import org.eurodat.client.credential.Credentials
import org.eurodat.client.database.SafeDepositDatabaseRequest
import org.eurodat.client.datamanagement.model.PersistentDataRequest
import org.eurodat.client.transactionservice.AppRequest
import org.eurodat.e2e.apps.AppRegistrar
import org.eurodat.e2e.apps.FileHelper.Companion.fileToString
import org.eurodat.e2e.assertions.assertInDbWithCredentials
import org.eurodat.e2e.assertions.assertNonEmptyCredentials
import org.eurodat.e2e.assertions.assertTransactionEnded
import org.eurodat.e2e.assertions.assertTransactionStarted
import org.eurodat.e2e.clients.ConsumerClient
import org.eurodat.e2e.jdbc.getJdbcConnection
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestMethodOrder
import org.junitpioneer.jupiter.DisableIfTestFails
import java.sql.SQLException

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(
    MethodOrderer.OrderAnnotation::class,
)
@QuarkusTest
@DisableIfTestFails
@RunOnVertxContext
class TestUnifiedDBUserConnectivity {
    private val appId = "uc2AppNoWorkflowsMock"
    private val appRequest = AppRequest(appId = appId)
    private var generatedTransactionId = ""
    private val consumerParticipantId = "dbselector"

    private lateinit var transactionCredentials: Credentials
    private lateinit var safeDepositCredentials: Credentials

    private val objectMapper = ObjectMapper()

    @RestClient
    private lateinit var consumerClient: ConsumerClient

    @RestClient
    private lateinit var appServiceClient: AppServiceExternalClient

    @Inject
    private lateinit var appRegistrar: AppRegistrar

    @Test
    @Order(0)
    fun deRegisterIfExistsTest(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplication(appId) }
    }

    @Test
    @Order(1)
    fun registerRequiredApp(asserter: UniAsserter) {
        asserter.assertNotNull {
            appServiceClient.startAppRegistration(
                AppRegistration(
                    appId,
                    fileToString("apps/uc2Mock/ddl.sql"),
                    fileToString("apps/uc2Mock/ddl_safedeposit.sql"),
                    listOf(TableSecurityMapping("participant_identifier", "output")),
                ),
            )
        }
    }

    @Test
    @Order(2)
    fun testTransactionStart(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.startTransaction(appRequest) },
            {
                this.generatedTransactionId = it.id
                assertTransactionStarted(appId, it)
            },
        )
    }

    @Test
    @Order(3)
    fun ensureNoSafeDepositBoxExistsOrIsDeleted(asserter: UniAsserter) {
        asserter.assertNull { consumerClient.deleteSafeDepositDatabase(appId) }
    }

    @Test
    @Order(4)
    fun testCreateValidAppSafeDeposit(asserter: UniAsserter) {
        asserter.assertEquals(
            { consumerClient.createSafeDepositDatabase(SafeDepositDatabaseRequest(appId)).map { it.response } },
            "Database created successfully",
        )
    }

    @Test
    @Order(5)
    fun testGetCredentials(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.readExternalAccessCredentials(generatedTransactionId) },
            { credentials ->
                this.transactionCredentials = credentials
                assertNonEmptyCredentials(credentials)
            },
        )
    }

    @Test
    @Order(6)
    fun testGetCredentialsSafeDeposit(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.readSafeDepositAccessCredentials(appId) },
            { credentials ->
                this.safeDepositCredentials = credentials
                assertNonEmptyCredentials(credentials)
            },
        )
    }

    @Test
    @Order(7)
    fun testAllowTransactCredentialsToSafeDeposit(asserter: UniAsserter) {
        asserter.assertInDbWithCredentials(transactionCredentials.copy(jdbcUrl = safeDepositCredentials.jdbcUrl)) {
            val result = it.executeQuery("SELECT count(*) from safedeposit.input;")
            result.next()
            Assertions.assertEquals(0, result.getInt(1))
        }
    }

    @Test
    @Order(8)
    fun testAllowSafeDepositCredentialsToTransaction(asserter: UniAsserter) {
        asserter.assertInDbWithCredentials(safeDepositCredentials.copy(jdbcUrl = transactionCredentials.jdbcUrl)) {
            val result = it.executeQuery("SELECT count(*) from input.input;")
            result.next()
            Assertions.assertEquals(0, result.getInt(1))
        }
    }

    @Test
    @Order(9)
    fun insertIntoTransactionUsingTransactionCredentials(asserter: UniAsserter) {
        asserter.assertInDbWithCredentials(transactionCredentials) {
            val query =
                "INSERT INTO input.input (id,created_at,created_by,file_content,file_type,dataset_id)" +
                    " VALUES('00000000-0000-0000-0000-000000000013','2019-01-01 02:05:15','creator','content'," +
                    "'type','00000000-0000-0000-0000-000000000014');"

            it.executeUpdate(query)

            val result = it.executeQuery("SELECT count(*) FROM input.input;")
            result.next()

            Assertions.assertEquals(1, result.getInt(1))
        }
    }

    @Test
    @Order(10)
    fun insertIntoTransactionUsingSafeDepositCredentials(asserter: UniAsserter) {
        asserter.assertInDbWithCredentials(safeDepositCredentials.copy(jdbcUrl = transactionCredentials.jdbcUrl)) {
            val query =
                "INSERT INTO input.input (id,created_at,created_by,file_content,file_type,dataset_id)" +
                    " VALUES('00000000-0000-0000-0000-000000000014','2019-01-01 02:05:15','creator','content','type'," +
                    "'00000000-0000-0000-0000-000000000015');"

            it.executeUpdate(query)

            val result = it.executeQuery("SELECT count(*) FROM input.input;")
            result.next()

            Assertions.assertEquals(2, result.getInt(1))
        }
    }

    @Test
    @Order(11)
    fun insertIntoSafedepsoitUsingTransactionCredentials(asserter: UniAsserter) {
        asserter.assertInDbWithCredentials(transactionCredentials.copy(jdbcUrl = safeDepositCredentials.jdbcUrl)) {
            val query =
                "INSERT INTO safedeposit.input (id,created_at,created_by,file_content,file_type,dataset_id)" +
                    " VALUES('00000000-0000-0000-0000-000000000011','2019-01-01 02:05:15','creator','content','type'," +
                    "'00000000-0000-0000-0000-000000000012');"

            it.executeUpdate(query)

            val result = it.executeQuery("SELECT count(*) FROM safedeposit.input;")
            result.next()

            Assertions.assertEquals(1, result.getInt(1))
        }
    }

    @Test
    @Order(12)
    fun insertIntoSafedepositUsingSafeDepositCredentials(asserter: UniAsserter) {
        asserter.assertInDbWithCredentials(safeDepositCredentials) {
            val query =
                "INSERT INTO safedeposit.input (id,created_at,created_by,file_content,file_type,dataset_id)" +
                    " VALUES('00000000-0000-0000-0000-000000000012','2019-01-01 02:05:15','creator','content','type'," +
                    "'00000000-0000-0000-0000-000000000016');"
            it.executeUpdate(query)

            val result = it.executeQuery("SELECT count(*) FROM safedeposit.input;")
            result.next()

            Assertions.assertEquals(2, result.getInt(1))
        }
    }

    @Test
    @Order(13)
    fun testInsertPersistentDataSuccessful(asserter: UniAsserter) {
        val testData =
            """
            {
              "id": "00000000-0000-0000-0000-100000000010",
              "created_at": "2021-01-01T12:00:00",
              "created_by": "creator",
              "file_content": "",
              "file_type": "type",
              "dataset_id": "00000000-0000-0000-0000-100000000016"
            }
            """

        val persistentDataRequest =
            PersistentDataRequest(data = listOf(testData), table = "input", appId = appId)

        asserter.assertThat(
            { consumerClient.insertDataPersistent(consumerParticipantId, persistentDataRequest) },
            { response ->
                Assertions.assertNotNull(response.result)
            },
        )
    }

    @Test
    @Order(14)
    fun testGetPersistentDataSuccessful(asserter: UniAsserter) {
        val testData =
            """
            [
                {
                  "id": "00000000-0000-0000-0000-100000000010",
                  "created_at": "2021-01-01T12:00:00",
                  "created_by": "creator",
                  "file_content": "\\x",
                  "file_type": "type",
                  "dataset_id": "00000000-0000-0000-0000-100000000016"
                }
            ]
            """
        val expectedJsonNode = objectMapper.readTree(testData).elements().next()

        asserter.assertThat(
            { consumerClient.getDataPersistent(consumerParticipantId, "input", appId = appId) },
            { data ->
                assert(data.isNotEmpty())

                val actualDataNodes = data.map { objectMapper.readTree(it) }
                val matches =
                    actualDataNodes.any { node ->
                        node == expectedJsonNode
                    }
                assert(matches)
            },
        )
    }

    @Test
    @Order(15)
    fun testTransactionEnd(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.endTransaction(generatedTransactionId) },
            { assertTransactionEnded(generatedTransactionId, it) },
        )
    }

    @Test
    @Order(16)
    fun testDatabaseDeletionAfterTransactionEnd(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { uni { getJdbcConnection(transactionCredentials) } },
            {
                Assertions.assertTrue(it is SQLException)
                // "3D000": error code for non-existent database
                Assertions.assertEquals("3D000", (it as SQLException).sqlState)
            },
        )
    }

    @Test
    @Order(17)
    fun testDeleteDepositAtTheEnd(asserter: UniAsserter) {
        asserter.assertNull { consumerClient.deleteSafeDepositDatabase(appId) }
    }

    @Test
    @Order(18)
    fun testDatabaseDeletionAfterSafeDepositDeletion(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { uni { getJdbcConnection(transactionCredentials) } },
            {
                Assertions.assertTrue(it is SQLException)
                // "3D000": error code for non-existent database
                Assertions.assertEquals("3D000", (it as SQLException).sqlState)
            },
        )
    }

    @Test
    @Order(19)
    fun deRegisterRequiredApp(asserter: UniAsserter) {
        asserter.assertNull { appServiceClient.deleteApp(appId) }
    }
}
