package org.eurodat.e2e.apps

import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.ws.rs.WebApplicationException
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.AppServiceExternalClient
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.appservice.model.AppResponse
import org.eurodat.client.appservice.model.ImageRequest
import org.eurodat.client.appservice.model.ImageStatus
import org.eurodat.client.appservice.model.WorkflowRegistrationRequest
import java.time.Duration

@ApplicationScoped
class AppRegistrar {
    @RestClient
    private lateinit var appServiceClient: AppServiceExternalClient

    @ConfigProperty(name = "image-upload.registry-username", defaultValue = "")
    private lateinit var imageRegistryUsername: String

    @ConfigProperty(name = "image-upload.registry-password", defaultValue = "")
    private lateinit var imageRegistryPassword: String

    @ConfigProperty(name = "image-upload.registry.address", defaultValue = "")
    private lateinit var registry: String

    @ConfigProperty(name = "images.tag", defaultValue = "")
    private lateinit var tag: String

    fun waitForImageStatus(
        appId: String,
        imageId: String,
        desiredStatus: ImageStatus,
    ) = appServiceClient
        .findImage(appId, imageId)
        .repeat()
        .withDelay(Duration.ofSeconds(5L))
        .atMost(20)
        .filter { it.status == desiredStatus }
        .toUni()

    fun removeApplication(appId: String): Uni<Void> =
        appServiceClient.deleteApp(appId).onFailure { it is WebApplicationException && it.response.status == 404 }.recoverWithNull()

    fun removeApplicationWithImages(appId: String): Uni<Void> =
        appServiceClient
            .findApp(appId)
            .call { app ->
                appServiceClient.getAllImages(app.id).call { images ->
                    val builder = Uni.join().builder<Unit>()
                    var nonEmpty = false
                    for (image in images.filter { it.status.name != "DELETED" }) {
                        nonEmpty = true
                        builder.add(
                            appServiceClient
                                .deleteImage(app.id, image.id)
                                .flatMap { waitForImageStatus(app.id, image.id, ImageStatus.DELETED) }
                                .map { },
                        )
                    }
                    if (nonEmpty) {
                        builder.joinAll().andFailFast()
                    } else {
                        Uni.createFrom().item(Unit)
                    }
                }
            }.flatMap { app -> appServiceClient.deleteApp(app.id) }
            .onFailure { it is WebApplicationException && it.response.status == 404 }
            .recoverWithNull()

    fun registerAppWithImageAndWorkflow(
        appRegistration: AppRegistration,
        imageName: String,
        workflowId: String,
        startCommand: List<String>,
    ): Uni<AppResponse> =
        appServiceClient
            .startAppRegistration(
                AppRegistration(
                    appRegistration.id,
                    appRegistration.transactionDDL,
                    appRegistration.safeDepositDDL,
                    appRegistration.tableSecurityMapping,
                    appRegistration.transactionTimeoutInDays,
                ),
            ).call { _ ->
                appServiceClient
                    .createImage(
                        appRegistration.id,
                        ImageRequest("$registry/$imageName:$tag", imageRegistryUsername, imageRegistryPassword),
                    ).call { image ->
                        waitForImageStatus(appRegistration.id, image.id, ImageStatus.ACTIVE)
                    }.call { image ->
                        appServiceClient.addWorkflow(
                            appRegistration.id,
                            WorkflowRegistrationRequest(workflowId, image.id, startCommand),
                        )
                    }
            }
}
