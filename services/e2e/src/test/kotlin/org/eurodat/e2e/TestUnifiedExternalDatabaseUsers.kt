package org.eurodat.e2e

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.uni
import jakarta.inject.Inject
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.AppServiceExternalClient
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.credential.Credentials
import org.eurodat.client.database.SafeDepositDatabaseRequest
import org.eurodat.e2e.apps.AppRegistrar
import org.eurodat.e2e.apps.FileHelper.Companion.fileToString
import org.eurodat.e2e.assertions.assertInDbWithCredentials
import org.eurodat.e2e.assertions.assertNonEmptyCredentials
import org.eurodat.e2e.clients.ConsumerClient
import org.eurodat.e2e.clients.ProviderClient
import org.eurodat.e2e.jdbc.createJdbcStatement
import org.eurodat.e2e.jdbc.getJdbcConnection
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestMethodOrder
import org.junitpioneer.jupiter.DisableIfTestFails
import java.sql.SQLException

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(
    MethodOrderer.OrderAnnotation::class,
)
@QuarkusTest
@DisableIfTestFails
@RunOnVertxContext
class TestUnifiedExternalDatabaseUsers {
    private val appId1 = "uc2SafeDepositMock"
    private val appId2 = "safeDepositTransactionsAppMock"

    private val appRequest1 = SafeDepositDatabaseRequest(appId1)
    private val appRequest2 = SafeDepositDatabaseRequest(appId2)

    private lateinit var appId1Credentials: Credentials
    private lateinit var appId1Client2Credentials: Credentials
    private lateinit var appId2Credentials: Credentials

    @RestClient
    private lateinit var consumerClient: ConsumerClient

    @RestClient
    private lateinit var providerClient: ProviderClient

    @RestClient
    private lateinit var appServiceClient: AppServiceExternalClient

    @Inject
    private lateinit var appRegistrar: AppRegistrar

    @Test
    @Order(0)
    fun deRegisterIfExistsTest(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplication(appId1) }
        asserter.assertNull { appRegistrar.removeApplication(appId2) }
    }

    @Test
    @Order(1)
    fun registerRequiredApp(asserter: UniAsserter) {
        asserter.assertNotNull {
            appServiceClient.startAppRegistration(
                AppRegistration(
                    appId1,
                    "",
                    fileToString("apps/uc2Mock/ddl_safedeposit.sql"),
                    emptyList(),
                ),
            )
        }
        asserter.assertNotNull {
            appServiceClient.startAppRegistration(
                AppRegistration(
                    appId2,
                    "",
                    fileToString("apps/safeDepositDBMock/safeDeposit.sql"),
                    emptyList(),
                ),
            )
        }
    }

    @Test
    @Order(2)
    fun ensureNoSafeDepositBoxesExistOrAreDeleted(asserter: UniAsserter) {
        asserter.assertNull { consumerClient.deleteSafeDepositDatabase(appId1) }
        asserter.assertNull { consumerClient.deleteSafeDepositDatabase(appId2) }
        asserter.assertNull { providerClient.deleteSafeDepositDatabase(appId1) }
        asserter.assertNull { providerClient.deleteSafeDepositDatabase(appId2) }
    }

    @Test
    @Order(3)
    fun testCreateSafeDepositForAppId1(asserter: UniAsserter) {
        asserter.assertEquals(
            { consumerClient.createSafeDepositDatabase(appRequest1).map { it.response } },
            "Database created successfully",
        )
    }

    @Test
    @Order(4)
    fun testCreateSafeDepositForAppId1AndDifferentClient(asserter: UniAsserter) {
        asserter.assertEquals(
            { providerClient.createSafeDepositDatabase(appRequest1).map { it.response } },
            "Database created successfully",
        )
    }

    @Test
    @Order(5)
    fun testCreateSafeDepositForAppId2(asserter: UniAsserter) {
        asserter.assertEquals(
            { consumerClient.createSafeDepositDatabase(appRequest2).map { it.response } },
            "Database created successfully",
        )
    }

    @Test
    @Order(6)
    fun testGetCredentialsForAppId1(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.readSafeDepositAccessCredentials(appId1) },
            { credentials ->
                appId1Credentials = credentials
                assertNonEmptyCredentials(credentials)
                createJdbcStatement(appId1Credentials).use {
                    val result = it.executeQuery("SELECT count(*) from safedeposit.input;")
                    result.next()
                    Assertions.assertEquals("0", result.getString(1))
                }
            },
        )
    }

    @Test
    @Order(7)
    fun testGetCredentialsForAppId1AndDifferentClient(asserter: UniAsserter) {
        asserter.assertThat(
            { providerClient.readSafeDepositAccessCredentials(appId1) },
            { credentials ->
                appId1Client2Credentials = credentials
                assertNonEmptyCredentials(credentials)
                createJdbcStatement(appId1Client2Credentials).use {
                    val result = it.executeQuery("SELECT count(*) from safedeposit.input;")
                    result.next()
                    Assertions.assertEquals("0", result.getString(1))
                }
            },
        )
    }

    @Test
    @Order(8)
    fun testGetCredentialsForAppId2(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.readSafeDepositAccessCredentials(appId2) },
            { credentials ->
                appId2Credentials = credentials
                assertNonEmptyCredentials(credentials)
                createJdbcStatement(appId2Credentials).use {
                    val result = it.executeQuery("SELECT count(*) from safedeposit.input_transactions;")
                    result.next()
                    Assertions.assertEquals("0", result.getString(1))
                }
            },
        )
    }

    @Test
    @Order(9)
    fun testDBAccessAllowedUsingApp1CredentialsToApp2SameClient(asserter: UniAsserter) {
        asserter.assertInDbWithCredentials(appId1Credentials.copy(jdbcUrl = appId2Credentials.jdbcUrl)) {
            val result = it.executeQuery("SELECT count(*) from safedeposit.input_transactions;")
            result.next()
            Assertions.assertEquals("0", result.getString(1))
        }
    }

    @Test
    @Order(10)
    fun testDBAccessAllowedUsingApp2CredentialsToApp1SameClient(asserter: UniAsserter) {
        asserter.assertInDbWithCredentials(appId2Credentials.copy(jdbcUrl = appId1Credentials.jdbcUrl)) {
            val result = it.executeQuery("SELECT count(*) from safedeposit.input;")
            result.next()
            Assertions.assertEquals("0", result.getString(1))
        }
    }

    @Test
    @Order(11)
    fun testDBAccessDeniedUsingApp1CredentialsToApp1ForDifferentClient(asserter: UniAsserter) {
        asserter.assertInDbWithCredentials(appId1Client2Credentials.copy(jdbcUrl = appId1Credentials.jdbcUrl)) {
            Assertions
                .assertThrows(SQLException::class.java) {
                    val result = it.executeQuery("SELECT count(*) from safedeposit.input;")
                    result.next()
                }.apply {
                    Assertions.assertEquals("42501", this.sqlState)
                }
        }
    }

    @Test
    @Order(12)
    fun testDeleteDepositForAppId1(asserter: UniAsserter) {
        asserter.assertNull { consumerClient.deleteSafeDepositDatabase(appId1) }
    }

    @Test
    @Order(13)
    fun testDeleteDepositForAppId1DifferentClient(asserter: UniAsserter) {
        asserter.assertNull { providerClient.deleteSafeDepositDatabase(appId1) }
    }

    @Test
    @Order(14)
    fun testDeleteDepositForAppId2(asserter: UniAsserter) {
        asserter.assertNull { consumerClient.deleteSafeDepositDatabase(appId2) }
    }

    @Test
    @Order(15)
    fun testAlreadyDeletedSafeDepositDeletionForAppId1(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { uni { getJdbcConnection(appId1Credentials) } },
            {
                Assertions.assertTrue(it is SQLException)
                // "3D000": error code for non-existent database
                Assertions.assertEquals("3D000", (it as SQLException).sqlState)
            },
        )
    }

    @Test
    @Order(16)
    fun testAlreadyDeletedSafeDepositDeletionForAppId2(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { uni { getJdbcConnection(appId2Credentials) } },
            {
                Assertions.assertTrue(it is SQLException)
                // "3D000": error code for non-existent database
                Assertions.assertEquals("3D000", (it as SQLException).sqlState)
            },
        )
    }

    @Test
    @Order(17)
    fun deRegisterRequiredApp(asserter: UniAsserter) {
        asserter.assertNull { appServiceClient.deleteApp(appId1) }
        asserter.assertNull { appServiceClient.deleteApp(appId2) }
    }
}
