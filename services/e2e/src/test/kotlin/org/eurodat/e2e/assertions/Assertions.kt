package org.eurodat.e2e.assertions

import io.github.oshai.kotlinlogging.KLogger
import io.quarkus.test.vertx.UniAsserter
import io.smallrye.mutiny.uni
import org.eurodat.client.credential.Credentials
import org.eurodat.client.transactionservice.Transaction
import org.eurodat.client.transactionservice.WorkflowStatusPhase
import org.eurodat.e2e.clients.ConsumerClient
import org.eurodat.e2e.jdbc.createJdbcStatement
import org.junit.jupiter.api.Assertions
import java.sql.Statement
import java.time.Duration
import java.util.function.Consumer

internal fun assertNonEmptyCredentials(credentials: Credentials) {
    Assertions.assertNotNull(credentials.jdbcUrl)
    Assertions.assertNotEquals("", credentials.jdbcUrl)
    Assertions.assertNotNull(credentials.username)
    Assertions.assertNotEquals("", credentials.username)
    Assertions.assertNotNull(credentials.password)
    Assertions.assertNotEquals("", credentials.password)
    Assertions.assertEquals(2764800, credentials.leaseDuration)
}

internal fun assertTransactionStarted(
    appId: String,
    transaction: Transaction,
) {
    Assertions.assertEquals(appId, transaction.appId)
    Assertions.assertNotNull(transaction.startTime)
    Assertions.assertNull(transaction.endTime)
}

internal fun assertTransactionEnded(
    generatedTransactionId: String,
    transaction: Transaction,
) {
    Assertions.assertEquals(generatedTransactionId, transaction.id)
    Assertions.assertNotNull(transaction.endTime)
    Assertions.assertTrue(transaction.endTime!! >= transaction.startTime)
}

internal fun UniAsserter.assertInDbWithCredentials(
    credentials: Credentials,
    function: Consumer<Statement>,
) = assertThat({ uni { createJdbcStatement(credentials) } }, { statement -> statement.use { function.accept(it) } })

internal fun assertWorkflowSucceeded(
    transactionId: String,
    workflowId: String,
    consumerClient: ConsumerClient,
    logger: KLogger,
    asserter: UniAsserter,
    delay: Long = 5L,
    maxAttempts: Long = 15L,
) {
    asserter.assertNotNull {
        consumerClient
            .getWorkflow(transactionId, workflowId)
            .invoke { it -> logger.debug { "Workflow $workflowId: Phase: ${it.status.phase}" } }
            .repeat()
            .withDelay(Duration.ofSeconds(delay))
            .atMost(maxAttempts)
            .filter { it.status.phase == WorkflowStatusPhase.SUCCEEDED }
            .toUni()
    }
}
