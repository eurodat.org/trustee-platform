package org.eurodat.e2e

import io.github.oshai.kotlinlogging.KotlinLogging
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.transactionservice.AppRequest
import org.eurodat.client.transactionservice.WorkflowRequest
import org.eurodat.e2e.apps.AppRegistrar
import org.eurodat.e2e.apps.FileHelper.Companion.fileToString
import org.eurodat.e2e.assertions.assertNonEmptyCredentials
import org.eurodat.e2e.assertions.assertTransactionEnded
import org.eurodat.e2e.assertions.assertTransactionStarted
import org.eurodat.e2e.assertions.assertWorkflowSucceeded
import org.eurodat.e2e.clients.ConsumerClient
import org.eurodat.e2e.clients.ProviderClient
import org.eurodat.e2e.jdbc.createJdbcStatement
import org.jboss.resteasy.reactive.ClientWebApplicationException
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestMethodOrder
import org.junitpioneer.jupiter.DisableIfTestFails
import java.sql.SQLException

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(
    MethodOrderer.OrderAnnotation::class,
)
@QuarkusTest
@DisableIfTestFails
@RunOnVertxContext
class TestCredentialManagement {
    private val appId = "helloWorldApp"
    private val imageName = "uc2-mock-app" // We could use any arbitrary image here
    private val workflowDefinitionId = "hello-world"
    private val appRequest =
        AppRequest(appId = appId, consumer = listOf("dbselector"), provider = listOf("testNotValidInput"))
    private var generatedTransactionId = ""
    private var consumerWorkflowId = ""
    private val logger = KotlinLogging.logger {}

    @Inject
    private lateinit var appRegistrar: AppRegistrar

    @RestClient
    private lateinit var consumerClient: ConsumerClient

    @RestClient
    private lateinit var providerClient: ProviderClient

    @Test
    @Order(0)
    fun deRegisterIfExistsTest(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }

    @Test
    @Order(1)
    fun registerRequiredApp(asserter: UniAsserter) {
        asserter.assertNotNull {
            appRegistrar.registerAppWithImageAndWorkflow(
                AppRegistration(
                    appId,
                    fileToString("apps/helloWorld/ddl.sql"),
                    "",
                    emptyList(),
                ),
                imageName,
                workflowDefinitionId,
                listOf("echo", "hello"),
            )
        }
    }

    @Test
    @Order(2)
    fun testStartTransactionWithParticipantsInput(uniAsserter: UniAsserter) {
        uniAsserter.assertThat(
            { consumerClient.startTransaction(appRequest) },
            {
                generatedTransactionId = it.id
                assertTransactionStarted(appId, it)
            },
        )
    }

    @Test
    @Order(3)
    fun testStartWorkflowAsConsumer(uniAsserter: UniAsserter) {
        uniAsserter.assertThat(
            { consumerClient.startWorkflow(generatedTransactionId, WorkflowRequest(workflowDefinitionId)) },
            {
                Assertions.assertEquals(generatedTransactionId, it.transactionId)
                Assertions.assertEquals(workflowDefinitionId, it.workflowDefinitionId)
                consumerWorkflowId = it.workflowRunId
            },
        )
    }

    @Test
    @Order(4)
    fun testStartWorkflowAsProvider(uniAsserter: UniAsserter) {
        uniAsserter.assertFailedWith(
            { providerClient.startWorkflow(generatedTransactionId, WorkflowRequest(workflowDefinitionId)) },
            { x -> Assertions.assertEquals(404, (x as ClientWebApplicationException).response.status) },
        )
    }

    @Test
    @Order(5)
    fun testGetCredentialsAsValidParticipant(uniAsserter: UniAsserter) {
        uniAsserter.assertThat(
            { consumerClient.readExternalAccessCredentials(generatedTransactionId) },
            { credentials ->
                assertNonEmptyCredentials(credentials)
                createJdbcStatement(credentials).use {
                    val result = it.executeQuery("SELECT count(*) FROM input.hello_world;")
                    result.next()
                    Assertions.assertEquals(0, result.getInt(1))
                }
            },
        )
    }

    @Test
    @Order(6)
    fun testGetCredentialsAsNotValidParticipant(uniAsserter: UniAsserter) {
        uniAsserter.assertThat(
            { providerClient.readExternalAccessCredentials(generatedTransactionId) },
            {
                assertNonEmptyCredentials(it)
                Assertions
                    .assertThrows(SQLException::class.java) {
                        val result = createJdbcStatement(it).executeQuery("SELECT count(*) FROM input.hello_world;")
                        result.next()
                    }.apply {
                        Assertions.assertEquals("42501", this.sqlState)
                    }
            },
        )
    }

    @Test
    @Order(7)
    fun testTransactionEnd(uniAsserter: UniAsserter) {
        uniAsserter.assertThat(
            { consumerClient.endTransaction(generatedTransactionId) },
            { assertTransactionEnded(generatedTransactionId, it) },
        )
    }

    @Test
    @Order(8)
    fun testWorkflowsSucceeded(asserter: UniAsserter) {
        assertWorkflowSucceeded(generatedTransactionId, consumerWorkflowId, consumerClient, logger, asserter)
    }

    @Test
    @Order(9)
    fun deRegisterRequiredApp(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }
}
