package org.eurodat.e2e

import io.github.oshai.kotlinlogging.KotlinLogging
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import jakarta.ws.rs.WebApplicationException
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.eurodat.client.appservice.model.AppRegistration
import org.eurodat.client.appservice.model.TableSecurityMapping
import org.eurodat.client.credential.Credentials
import org.eurodat.client.database.SafeDepositDatabaseRequest
import org.eurodat.client.datamanagement.model.DataRequest
import org.eurodat.client.transactionservice.AppRequest
import org.eurodat.client.transactionservice.WorkflowRequest
import org.eurodat.e2e.apps.AppRegistrar
import org.eurodat.e2e.apps.FileHelper.Companion.fileToString
import org.eurodat.e2e.assertions.assertInDbWithCredentials
import org.eurodat.e2e.assertions.assertNonEmptyCredentials
import org.eurodat.e2e.assertions.assertTransactionEnded
import org.eurodat.e2e.assertions.assertTransactionStarted
import org.eurodat.e2e.assertions.assertWorkflowSucceeded
import org.eurodat.e2e.clients.ConsumerClient
import org.eurodat.e2e.jdbc.createJdbcStatement
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestMethodOrder
import org.junitpioneer.jupiter.DisableIfTestFails

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(
    MethodOrderer.OrderAnnotation::class,
)
@QuarkusTest
@DisableIfTestFails
@RunOnVertxContext
class TestUC2 {
    private val appId = "uc2AppMock"
    private val appRequest = AppRequest(appId = appId)
    private val mockConsumerParticipantId = "dbselector"
    private val imageName = "uc2-mock-app"

    private var generatedTransactionId = ""

    private lateinit var safeDepositCredentials: Credentials
    private var workflowId = ""
    private val logger = KotlinLogging.logger {}

    @RestClient
    private lateinit var consumerClient: ConsumerClient

    @Inject
    private lateinit var appRegistrar: AppRegistrar

    @Test
    @Order(0)
    fun deRegisterIfExistsTest(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }

    @Test
    @Order(1)
    fun registerRequiredApp(asserter: UniAsserter) {
        asserter.assertNotNull {
            appRegistrar.registerAppWithImageAndWorkflow(
                AppRegistration(
                    appId,
                    fileToString("apps/uc2Mock/ddl.sql"),
                    fileToString("apps/uc2Mock/ddl_safedeposit.sql"),
                    listOf(TableSecurityMapping("participant_identifier", "output")),
                ),
                imageName,
                "start-uc2",
                listOf("python", "main.py"),
            )
        }
    }

    @Test
    @Order(4)
    fun ensureNoSafeDepositBoxExistsOrIsDeleted(asserter: UniAsserter) {
        asserter.assertNull { consumerClient.deleteSafeDepositDatabase(appId) }
    }

    @Test
    @Order(5)
    fun testSafeDepositDatabaseStart(asserter: UniAsserter) {
        asserter.assertEquals(
            { consumerClient.createSafeDepositDatabase(SafeDepositDatabaseRequest(appId)).map { it.response } },
            "Database created successfully",
        )
    }

    @Test
    @Order(6)
    fun testSafeDepositDatabaseMessageAlreadyExists(asserter: UniAsserter) {
        asserter.assertFailedWith(
            { consumerClient.createSafeDepositDatabase(SafeDepositDatabaseRequest(appId)).map { it.response } },
            {
                Assertions.assertTrue(it is WebApplicationException && it.response.status == 409)
            },
        )
    }

    @Test
    @Order(7)
    fun testTransactionStart(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.startTransaction(appRequest) },
            {
                generatedTransactionId = it.id
                assertTransactionStarted(appId, it)
            },
        )
    }

    @Test
    @Order(8)
    fun insertInputTransaction(asserter: UniAsserter) {
        val testData =
            """
            {
              "id": "00000000-0000-0000-0000-000000000013",
              "created_at": "2019-01-01 02:05:15",
              "created_by": "creator",
              "file_content": "content",
              "file_type": "type",
              "dataset_id": "00000000-0000-0000-0000-000000000014"
            }
            """
        val dataRequest = DataRequest(data = listOf(testData), table = "input", transactionId = generatedTransactionId)

        asserter.assertThat(
            { consumerClient.insertData(mockConsumerParticipantId, dataRequest) },
            { response ->
                Assertions.assertNotNull(response.result)
            },
        )
    }

    @Test
    @Order(9)
    fun testGetCredentialsSafeDeposit(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.readSafeDepositAccessCredentials(appId) },
            { credentials ->
                this.safeDepositCredentials = credentials
                assertNonEmptyCredentials(credentials)
                createJdbcStatement(safeDepositCredentials).use {
                    val result = it.executeQuery("SELECT count(*) FROM safedeposit.input;")
                    result.next()

                    Assertions.assertEquals(0, result.getInt(1))
                }
            },
        )
    }

    @Test
    @Order(10)
    fun insertInputSafeDeposit(asserter: UniAsserter) {
        asserter.assertInDbWithCredentials(safeDepositCredentials) {
            // Insert into safe deposit
            val query =
                "INSERT INTO safedeposit.input (id,created_at,created_by,file_content,file_type,dataset_id)" +
                    " VALUES('00000000-0000-0000-0000-000000000011','2019-01-01 02:05:15','creator','content','type'," +
                    "'00000000-0000-0000-0000-000000000012');"

            it.executeUpdate(query)

            val result = it.executeQuery("SELECT count(*) FROM safedeposit.input;")
            result.next()

            Assertions.assertEquals(1, result.getInt(1))
        }
    }

    @Test
    @Order(11)
    fun testWorkflowStartAllSafeAMLMock(asserter: UniAsserter) {
        val workflowDefinitionId = "start-uc2"
        asserter.assertThat(
            { consumerClient.startWorkflow(generatedTransactionId, WorkflowRequest(workflowDefinitionId)) },
            {
                Assertions.assertEquals(generatedTransactionId, it.transactionId)
                Assertions.assertEquals(workflowDefinitionId, it.workflowDefinitionId)
                workflowId = it.workflowRunId
            },
        )
    }

    @Test
    @Order(12)
    fun testWorkflowsSucceeded(asserter: UniAsserter) {
        assertWorkflowSucceeded(generatedTransactionId, workflowId, consumerClient, logger, asserter)
    }

    @Test
    @Order(13)
    fun testOutputInTransactionDB(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.getData(mockConsumerParticipantId, "output", generatedTransactionId, null) },
            { data ->
                Assertions.assertNotNull(data)
                Assertions.assertEquals(2, data.size)
            },
        )
    }

    @Test
    @Order(14)
    fun testSafeDepositEnd(asserter: UniAsserter) {
        asserter.assertNull { consumerClient.deleteSafeDepositDatabase(appId) }
    }

    @Test
    @Order(15)
    fun testTransactionEnd(asserter: UniAsserter) {
        asserter.assertThat(
            { consumerClient.endTransaction(generatedTransactionId) },
            { assertTransactionEnded(generatedTransactionId, it) },
        )
    }

    @Test
    @Order(16)
    fun deRegisterAfterTest(asserter: UniAsserter) {
        asserter.assertNull { appRegistrar.removeApplicationWithImages(appId) }
    }
}
