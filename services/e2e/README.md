# Description

This module houses our [End-to-End](src/test/kotlin/org/eurodat/e2e/TestTransaction.kt) test.

## Running and Testing Locally

To run the E2E test one must start a number of applications and services which is described in the
[local development guide](../../../docs/guides/local-development/guide-local-development.md) and the required
parameters and a flow chart can be found in the [e2e guide](../../../docs/guides/local-development/end-to-end-test.md).
