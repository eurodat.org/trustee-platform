---
hide:
  - toc
  - navigation
---

# The European Data Trustee Platform

The EuroDaT platform is a platform operated by the European Data Trustee
[EuroDaT](https://www.eurodat.org/en/). EuroDaT is based on the principle of a data transaction that
ensures a secure and legally compliant exchange of data between parties. It guarantees data sovereignty for the data
provider and enables independent analysis of shared data for the consumer. The EuroDaT platform enforces contracts
between the parties that govern usage of data and services and ensures deletion of all data when a transaction is
completed.

You may find all our documentation here on [docs.eurodat.org](https://docs.eurodat.org/). Refer to [EuroDaT Platform](eurodat)
to get started with the EuroDaT platform or read up on the [EuroDaT Client](eurodat-client) to learn how to connect to EuroDaT.
See [Tools](docs/tools) for an overview of tech we use. Our [Gitlab repository](https://gitlab.com/eurodat/trustee-platform/-/tree/main/docs) contains our official
[releases](https://gitlab.com/eurodat/trustee-platform/-/releases).
Changes between releases are documented in [Release Notes](release_notes.md). Finally, the infrastructure required to run EuroDaT is
defined in our [Infrastructure as Code Gitlab repository](https://gitlab.com/eurodat/eurodat-trustee-iac).

**At present, the information shown here addresses mostly developers who want to contribute to the EuroDaT platform or
want to participate as a data service provider.**

## License

COPYRIGHT (c) 2023 EuroDaT GmbH.

Licensed under BSD 3-Clause Licence.

SPDX identifier: BSD-3-Clause

Pleaser also refer the full [LICENSE :](https://gitlab.com/eurodat/trustee-platform/-/blob/main/LICENSE).

## Contributing

Contributions are welcome and appreciated. Please follow the guideline
[linked here :](https://gitlab.com/eurodat/trustee-platform/-/blob/main/CONTRIBUTING.md).

## Disclaimer

The views and conclusions contained in the software and documentation are those of the authors and should not be
interpreted as representing official policies, either expressed or implied, of EuroDaT GmbH.
