# When using Python Version manager like pyenv set the desired Python3 version in .python-version
# and run 'python3 -m pip install -r requirements.txt' to install the dependencies.
# This will install the packages in the current chosen Python3 version (not on system or global level).
# To update this file run python3 -m pip freeze -r requirements.txt and copy the output into this file.
# Checkout https://builtin.com/software-engineering-perspectives/pip-freeze to switch
mkdocs==1.5.3
mkdocs-material==9.5.18
mkdocs-material-extensions==1.3.1
mkdocs-awesome-pages-plugin==2.9.2
mkdocs-exclude-search==0.6.6
