## Summary

<!--
Briefly summarize the bug
-->

## Steps to reproduce

<!-- Describe what you did (step-by-step) so we can reproduce: -->

- With a registered EuroDaT client, run
- curl -X POST /api/v1/...
- curl ...

## What happened?

...

## What should have happened?

...

## Relevant logs and/or screenshots

...

## EuroDaT and EuroDaT Client Version

- EuroDaT Version: ...
- EuroDaT Client Version: ...

## Environment description

<!--
Are you running the EuroDaT Client as a docker container, by building from scratch or deployed in
a k8s cluster via our provided helm chart? Are there any other relevant details about the environment?
-->
...

## Possible fixes

<!--
(If you can, link to the line of code that might be responsible for the problem)
--->

/label ~bug
