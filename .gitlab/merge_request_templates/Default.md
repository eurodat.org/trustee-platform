# Merge Request
`<Description here>`

## Things to do during Peer Review
Please read all boxes before the Merge Request is approved and merged. Not all boxes must be checked, **use good
judgement**
whether to skip a box or not.

### **Feature Implementation**
- [ ] The MR actually implements what is described in the JIRA-Issue.
- [ ] The part of acceptance criteria and to-dos covered by this MR are fulfilled (_if applicable_).

### **Code Quality**
- [ ] The code has been manually inspected by someone who did not implement the feature.
- [ ] The changes made to the code have maintained or improved its quality.
- [ ] Sufficient error handling and edge cases are addressed, _if applicable_.

### **Testing**
- [ ] Relevant tests (unit, integration, e2e) are added or updated, _if applicable_.
- [ ] Any test failures or flaky tests in the pipeline are resolved or tracked in a (bug-)ticket before merging.

### **Validation**
- [ ] The new feature is manually used/tested/observed in the appropriate environment(s). This MR has been tested in the following environment:
  - [ ] workstation
  - [ ] dev cluster
  - [ ] locally
- [ ] Tilt configuration changes (_if applicable_) have been verified by starting a fresh Tilt session (`make teardown`).
- [ ] The automated deployment is updated.
- [ ] Frontend changes are validated for functionality and styling, _if applicable_.

### **Documentation**
- [ ] Documentation is updated, _if applicable_.
    - [ ] MkDocs successfully renders the updated documentation.
    - [ ] Changes to the pipeline or architecture changes are documented in the appropriate diagrams.
    - [ ] If a new module is created, a corresponding README.md file was created.

### **Follow-up tasks**
- [ ] Dependencies or related tickets are updated, _if applicable_.
