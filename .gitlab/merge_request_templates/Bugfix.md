# Bug Ticket Template
`<Description here>`

## Things to do during Peer Review
### **Fix Implementation**
- [ ] The fix addresses the issue described in the bug ticket.
- [ ] Affected code is reviewed and unnecessary changes are avoided.

### **Testing**
- [ ] The issue cannot be reproduced after applying the fix.
- [ ] A new test case is added to verify the fix, _if applicable_.

#### **Validation**
- [ ] The fix is manually tested in an appropriate environment:
  - [ ] workstation
  - [ ] dev cluster
  - [ ] locally
- [ ] Dependencies affected by this bug are reviewed for additional impacts.