# Hotfix Template
`<Briefly explain why this is a hotfix and its critical impact on the system or users>`

## Things to do during Peer Review
#### **Fix Implementation**
- [ ] The change is scoped to only address the urgent issue.
- [ ] Non-critical code or features are not modified unnecessarily.
- [ ] Changes are reviewed and approved by a second team member.