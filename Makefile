.DEFAULT_GOAL = help
# For linux installation on AMD64 / x86_64:
CTLPTL_VERSION="0.8.20"

define REGISTRY_DEFINITION
apiVersion: ctlptl.dev/v1alpha1
kind: Registry
port: 5002
listenAddress: 0.0.0.0
name: eurodat-registry
endef
export REGISTRY_DEFINITION

define CLUSTER_DEFINITION
apiVersion: ctlptl.dev/v1alpha1
kind: Cluster
product: kind
name: kind-eurodat
registry: eurodat-registry
kindV1Alpha4Cluster:
  nodes:
    - role: control-plane
      kubeadmConfigPatches:
        - |
          kind: InitConfiguration
          nodeRegistration:
            kubeletExtraArgs:
              node-labels: "ingress-ready=true"
      extraPortMappings:
        - containerPort: 443
          hostPort: 443
        - containerPort: 5432
          hostPort: 5432
    - role: worker
      extraPortMappings:
        - containerPort: 31000
          hostPort: 31000
  networking:
    apiServerAddress: "127.0.0.1"
    apiServerPort: 50000
    disableDefaultCNI: true   # do not install kindnet
#    podSubnet: "192.168.0.0/18"
#    serviceSubnet: "192.168.0.0/18"
endef
export CLUSTER_DEFINITION

# Excluded services
xcl_frontend_only = push_hello-world app-service-argo minabo-app db-credentials-poc-app mock-safeaml-app message-app uc2-mock-app uc3-mock-app row-level-security-mock-app transaction-copy-app eurodat-transaction-service transaction-service-config transaction-service-argo argo-workflows-example transaction-service transaction-service-ingress argo-workflows-config row-level-security-mock tempo tempo-config opentelemetry-collector credential-service-config database-service-config eurodat-app-service app-service-ingress app-service-config argo-workflows eurodat-credential-service data-plane eurodat-database-service flash-vaults grafana grafana-config opentelemetry register_test_users tempo transaction-vault eurodat-data-management-service data-management-service-config

define USAGE
make help                           - Displays this message
make prerequisites-Ubuntu           - Installs prerequisites (helpers (maven, jq, curl, ca-certificates, gnupg),
				openapi generator, Docker, kubectl, helm, Temurin-17-jdk) on Ubuntu
make prep                           - Installs all dependencies needed (kind, kubectx, ctlptl, tilt) on Windows,
				Ubuntu, other Linux and macOS - needs admin privileges
make setup                          - Creates the resources necessary to start Tilt (i.e. KinD clusters, Kubernetes CNIs, Helm dependencies)
make start	                        - Starts Tilt session for the eurodat cluster
make teardown                       - Deletes the cluster
make update                         - Updates the Helm dependencies
make update-app-service             - Updates the app-service chart and the charts that depend on it
make update-transaction-service              - Updates the transaction-service chart and the charts that depend on it
make update-credential-service		- Updates the credential-service chart and the charts that depend on it
make update-contract-service        - Updates the contract-service chart and the charts that depend on it
make update-data-management-service - Updates the data-management-service chart and the charts that depend on it
make update-gui                     - Updates the demo gui

Optional:
make nox=1 prep             - Do not install `kubectx` and `kubens`
make noa=1 setup			- Do not add helm repositories

You can also run separate installation steps if needed, like:
make install-mvn-Ubuntu,
make install-kind-Windows,
make install-kubectx-macOS,
etc. (see Makefile for details).
endef
export USAGE

help:
ifeq ($(OS), Windows_NT)
	@echo "Currently running on $(OS)"
else
ifeq ($(shell uname -s), Darwin)
	@echo "Currently running on macOS"
else
ifeq ($(shell uname -s), Linux)
	@echo "Currently running on Linux (excluding Ubuntu)"
ifeq ($(shell grep -c "WSL2" /proc/version), 1)
	@echo "Running in WSL2"
endif
else
ifeq ($(shell grep -c "Ubuntu" /proc/version), 1)
	@echo "Currently running on Ubuntu"
endif
endif
endif
endif
	@echo "Usage:"
	@echo "$$USAGE"

apt-update:
	@sudo apt update

install-helpers-Ubuntu: apt-update
	@echo "****************** Installing helpers...\n"
	@sudo apt -y install maven jq curl ca-certificates gnupg

install-openapi-Linux:
	@echo "***************** Installing OpenAPI Generator...\n"
	@mkdir -p ~/bin/openapitools
	@curl https://raw.githubusercontent.com/OpenAPITools/openapi-generator/master/bin/utils/openapi-generator-cli.sh > ~/bin/openapitools/openapi-generator-cli
	@chmod u+x ~/bin/openapitools/openapi-generator-cli

install-docker-Ubuntu:
	@echo "Do you want to install Docker? yes/no"; read ANSWER; if [ $$ANSWER != "yes" ]; then echo aborting; exit 1; fi
	@echo "****************** Installing docker... \n"
	@sudo install -m 0755 -d /etc/apt/keyrings;	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --batch --yes --dearmor -o /etc/apt/keyrings/docker.gpg
	@sudo chmod a+r /etc/apt/keyrings/docker.gpg
	@echo "deb [arch=\"$$(dpkg --print-architecture)\" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    	\"$$(awk -F= '/^VERSION_CODENAME/{print$$2}' /etc/os-release)\" stable" | \
		sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
	@sudo apt update
	@sudo apt -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
	@echo "****************** add docker group if not existent... \n"
	@getent group docker || sudo groupadd docker
	@sudo usermod -aG docker $$USER

install-kubectl-Ubuntu:
	@echo "Do you want to install kubectl? yes/no"; read ANSWER; if [ $$ANSWER != "yes" ]; then echo aborting; exit 1; fi
	@echo "****************** Installing kubectl... \n"
	@sudo snap install kubectl --classic
	@sudo snap refresh kubectl

install-helm-Ubuntu:
	@echo "Do you want to install Helm? yes/no"; read ANSWER; if [ $$ANSWER != "yes" ]; then echo aborting; exit 1; fi
	@echo "***************** Installing Helm...\n"
	@sudo snap install helm --classic
	@sudo snap refresh helm

install-temurin-Ubuntu: apt-update
	@echo "***************** Installing Temurin-17-jdk (Java)...\n"
	@echo "Do you want to install Temurin-17-jdk? yes/no"; read ANSWER; if [ $$ANSWER != "yes" ]; then echo aborting; exit 1; fi
	@sudo apt-get -y install wget apt-transport-https
	@mkdir -p /etc/apt/keyrings
	@wget -O - https://packages.adoptium.net/artifactory/api/gpg/key/public | sudo tee /etc/apt/keyrings/adoptium.asc > /dev/null
	@echo "deb [signed-by=/etc/apt/keyrings/adoptium.asc] https://packages.adoptium.net/artifactory/deb \
		\"$$(awk -F= '/^VERSION_CODENAME/{print$$2}' /etc/os-release)\" main" | \
		sudo tee /etc/apt/sources.list.d/adoptium.list > /dev/null
	@sudo apt update
	@sudo apt-get -y install temurin-17-jdk

install-kind-Windows:
	@echo "***************** Installing kind...\n"
	@choco install -y --global --upgrade kind

install-kind-Linux:
	@echo "***************** Installing kind...\n"
# For AMD64 / x86_64
ifeq ($(shell uname -m), x86_64)
	@curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.19.0/kind-$$(uname)-amd64
endif
# For ARM64
ifeq ($(shell uname -m), aarch64)
	@curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.19.0/kind-$$(uname)-arm64
endif
	@chmod +x ./kind
	@sudo mv ./kind /usr/local/bin/kind

install-kind-macOS:
	@echo "***************** Installing kind...\n"
	@brew install kind

install-kubectx-Windows:
ifeq ($(nox),)
	@echo "***************** Installing kubectx...\n"
	@choco install -y --global --force --upgrade kubectx
endif

install-kubectx-Linux:
ifeq ($(nox),)
	@echo "***************** Installing kubectx... \n"
# For AMD64 / x86_64
ifeq ($(shell uname -m), x86_64)
	@curl -fsSL https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubectx_v0.9.4_linux_x86_64.tar.gz | \
	sudo tar -xzv -C /usr/local/bin kubectx
	@curl -fsSL https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubens_v0.9.4_linux_x86_64.tar.gz | \
	sudo tar -xzv -C /usr/local/bin kubens
endif
# For ARM64
ifeq ($(shell uname -m), aarch64)
	@curl -fsSL https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubectx_v0.9.4_linux_arm64.tar.gz | \
	sudo tar -xzv -C /usr/local/bin kubectx
	@curl -fsSL https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubens_v0.9.4_linux_arm64.tar.gz | \
	sudo tar -xzv -C /usr/local/bin kubens
endif
endif

install-kubectx-macOS:
ifeq ($(nox),)
	@echo "\n\n***************** Installing kubectx (including kubens)...\n"
	@brew install kubectx
endif

install-ctlptl-Linux:
	@echo "***************** Installing ctlptl... \n"
# For AMD64 / x86_64
ifeq ($(shell uname -m), x86_64)
	@curl -fsSL https://github.com/tilt-dev/ctlptl/releases/download/v$(CTLPTL_VERSION)/ctlptl.$(CTLPTL_VERSION).linux.x86_64.tar.gz | \
	sudo tar -xzv -C /usr/local/bin ctlptl
endif
# For ARM64
ifeq ($(shell uname -m), aarch64)
	@curl -fsSL https://github.com/tilt-dev/ctlptl/releases/download/v$(CTLPTL_VERSION)/ctlptl.$(CTLPTL_VERSION).linux.arm64.tar.gz | \
	sudo tar -xzv -C /usr/local/bin ctlptl
endif

install-ctlptl-tilt-Windows: install-kind-Windows
	@echo "***************** Installing ctlptl and tilt...\n"
	@scoop bucket add tilt-dev https://github.com/tilt-dev/scoop-bucket
	@scoop install ctlptl
	@scoop update ctlptl
	@scoop install tilt
	@scoop update tilt

install-ctlptl-macOS:
	@echo "***************** Installing ctlptl...\n"
	brew install tilt-dev/tap/ctlptl

install-tilt-Linux:
	@echo "***************** Installing tilt... \n"
	@cd /tmp; \
	curl -fsSL https://raw.githubusercontent.com/tilt-dev/tilt/master/scripts/install.sh | bash

install-maven-script:
	@echo "***************** Setting up maven repo... \n"
	@export PATH=$$PATH:$$HOME/bin/openapitools/; ./cluster/scripts/publish_argo_client_to_maven_local.sh

prerequisites-Ubuntu: install-docker-Ubuntu install-kubectl-Ubuntu install-helm-Ubuntu install-temurin-Ubuntu
	@echo "####################################"
	@echo "Ubuntu prerequisites SUCCESSFULLY DONE!"
	@echo "Please run \"newgrp docker\" to activate this group."
	@echo "####################################"

prep-Windows: install-kind-Windows install-kubectx-Windows install-ctlptl-tilt-Windows
	@echo "####################################"
	@echo "WINDOWS MAKE PREP SUCCESSFULLY DONE!"
	@echo "####################################"

prep-Ubuntu: install-helpers-Ubuntu install-kind-Linux install-kubectx-Linux install-ctlptl-Linux install-tilt-Linux install-openapi-Linux install-maven-script
	@echo "####################################"
	@echo "To not run into any resource issues please hardwire the following lines into your /etc/sysctl.conf file:"
	@echo ""
	@echo "fs.inotify.max_user_watches = 524288"
	@echo "fs.inotify.max_user_instances = 512"
	@echo "fs.file-max = 65536"
	@echo ""
	@echo "UBUNTU MAKE PREP SUCCESSFULLY DONE!"
	@echo "####################################"

prep-Linux: install-kind-Linux install-kubectx-Linux install-ctlptl-Linux install-tilt-Linux install-openapi-Linux install-maven-script
	@echo "####################################"
	@echo "LINUX MAKE PREP SUCCESSFULLY DONE!"
	@echo "####################################"

prep-macOS: install-kind-macOS install-kubectx-macOS install-ctlptl-macOS install-tilt-Linux
	@echo "####################################"
	@echo "MACOS MAKE PREP SUCCESSFULLY DONE!"
	@echo "####################################"

create-venv:
ifeq ("$(wildcard ./venv)","")
	@sudo apt -y install python3-venv
	@python3 -m venv venv
	./venv/bin/pip3 install -r requirements.txt
endif

create-venv-Windows:
ifeq ("$(wildcard ./venv)","")
	@py -m venv venv
	./venv/Scripts/pip3.exe install -r requirements.txt
endif

ifeq ($(OS), Windows_NT)
prep: prep-Windows create-venv-Windows
else
ifeq ($(shell uname -s), Darwin)
prep: prep-macOS create-venv
else
ifeq ($(shell uname -s), Linux)
prep: prep-Linux create-venv
else
ifeq ($(shell grep -c "Ubuntu" /proc/version), 1)
prep: prep-Ubuntu create-venv
endif
endif
endif
endif

# Required for patching CoreDNS' upstream nameserver for Docker Desktop + WSL2
ifeq ($(OS), Windows_NT)
WSL_NIC_IP := $(shell wsl ip route | awk '/default/ { print $$3 }')
PATCH_DNS := 1
else
ifeq ($(shell uname -s), Darwin)
else
ifeq ($(shell grep -c "WSL2" /proc/version), 1)
WSL_NIC_IP := $(shell ip route | awk '/default/ { print $$3 }')
PATCH_DNS := 1
endif
endif
endif

ifeq (1, $(PATCH_DNS))
define WSL2_COREDNS_COREFILE
apiVersion: v1
kind: ConfigMap
metadata:
  name: coredns
  namespace: kube-system
data:
  Corefile: |
    .:53 {
        errors
        health {
           lameduck 5s
        }
        ready
        kubernetes cluster.local in-addr.arpa ip6.arpa {
           pods insecure
           fallthrough in-addr.arpa ip6.arpa
           ttl 30
        }
        prometheus :9153
        forward . $(WSL_NIC_IP) {
           max_concurrent 1000
        }
        cache 30
        loop
        reload
        loadbalance
    }
endef
export WSL2_COREDNS_COREFILE
endif

housekeeping:
	@echo "Deleting Chart.lock files (list the exact versions of immediate dependencies and their dependencies)"
	@find cluster/helm/ -name Chart.lock -type f -delete
	@echo "Deleting external chart packages (tgz files)"
	@find cluster/helm/ -name \*.tgz -type f -delete
	@echo "Deleting Kotlin build artifacts and temporary folders"
	@find services -name build -type d -exec rm -rf {} +
	@find services -name .gradle -type d -exec rm -rf {} +
	@find services -name bin -type d -exec rm -rf {} +

setup:
	@echo "********************* Creating eurodat registry...\n"
	@echo "$$REGISTRY_DEFINITION" | sed -e "s/\t/    /g" | ctlptl apply -f -

	@echo "********************* Creating eurodat KinD cluster...\n"
	@echo "$$CLUSTER_DEFINITION" | sed -e "s/\t/    /g" | ctlptl apply -f -

ifeq ($(noa),)
	@echo "\n\n********************* Adding Helm charts...\n"
	@helm repo add codecentric https://codecentric.github.io/helm-charts
	@helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
	@helm repo add jetstack https://charts.jetstack.io
	@helm repo add bitnami https://charts.bitnami.com/bitnami
	@helm repo add hashicorp https://helm.releases.hashicorp.com
	@helm repo add mittwald https://helm.mittwald.de
	@helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server
	find cluster/helm/ -name Chart.lock -type f -delete
endif
	@helm dep build ./cluster/helm/app-service
	@helm dep build ./cluster/helm/ingress-nginx
	@helm dep build ./cluster/helm/argo-workflows
	@helm dep build ./cluster/helm/common-library
	@helm dep build ./cluster/helm/transaction-service
	@helm dep build ./cluster/helm/contract-service
	@helm dep build ./cluster/helm/credential-service
	@helm dep build ./cluster/helm/database-service
	@helm dep build ./cluster/helm/data-management-service
	@helm dep build ./cluster/helm/eurodat
	@helm dep build ./cluster/helm/transaction-vault
	@helm dep build ./cluster/helm/eurodat-cluster
	@helm dep build ./cluster/helm/postgres
ifneq (, $(shell which kubectx 2>/dev/null))
	@echo "Changing cluster to kind-eurodat"
	@kubectx kind-eurodat
	@echo "Installing Calico as cluster CNI within kind-eurodat..."
	@kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
	@echo "Installing Kubernetes metrics server into kind-eurodat"
	@helm upgrade --install metrics-server metrics-server/metrics-server --set args="{--kubelet-insecure-tls}" -n kube-system
endif
# patch coredns
ifeq ($(PATCH_DNS), 1)
	@echo "********************* Running post-setup hook..."
	@echo "********************* Patching CoreDNS upstream nameserver IP to $(WSL_NIC_IP) in kind-eurodat cluster..."
	@echo "$$WSL2_COREDNS_COREFILE" | kubectl apply --context kind-eurodat -f -
	@kubectl rollout --context kind-eurodat restart deployment coredns -n kube-system
endif
	@kubectx kind-eurodat
# variable set via make command 'make ... xcl="..." ' to exclude (xcl) services from the tilt start-up
xcl := default

start:
	@echo "********************* Starting up EuroDaT Tilt session...\n"
ifeq ($(dev-profile),frontend)
	@kubectx kind-eurodat
	tilt up $(xcl_frontend_only) --context kind-eurodat
endif
	@kubectx kind-eurodat
	tilt up $(xcl) --context kind-eurodat

update-app-service:
	find cluster/helm/ -name Chart.lock -type f -delete
	@helm dep build ./cluster/helm/app-service

update-transaction-service:
	find cluster/helm/ -name Chart.lock -type f -delete
	@helm dep build ./cluster/helm/transaction-service

update-credential-service:
	find cluster/helm/ -name Chart.lock -type f -delete
	@helm dep build ./cluster/helm/credential-service

update-contract-service:
	find cluster/helm/ -name Chart.lock -type f -delete
	@helm dep build ./cluster/helm/contract-service

update-data-management-service:
	find cluster/helm/ -name Chart.lock -type f -delete
	@helm dep build ./cluster/helm/data-management-service

update-database-service:
	find cluster/helm/ -name Chart.lock -type f -delete
	@helm dep build ./cluster/helm/database-service

update:
	@echo "********************* Updating Helm dependencies...\n"
	find cluster/helm/ -name Chart.lock -type f -delete
	@helm dep update ./cluster/helm/app-service
	@helm dep update ./cluster/helm/ingress-nginx
	@helm dep update ./cluster/helm/argo-workflows
	@helm dep update ./cluster/helm/transaction-service
	@helm dep update ./cluster/helm/eurodat
	@helm dep update ./cluster/helm/eurodat-cluster
	@helm dep update ./cluster/helm/transaction-vault
	@helm dep update ./cluster/helm/data-management-service
	@helm dep update ./cluster/helm/database-service
	@helm dep update ./cluster/helm/credential-service
	@helm dep update ./cluster/helm/contract-service
	@helm dep update ./cluster/helm/postgres

teardown:
	@echo "********************** Deleting Main Cluster..."
	@kind delete clusters eurodat
	@ctlptl delete registry eurodat-registry
