// This is the config file for the renovate bot. It parses the actual config file renovate.json
// which is then used by the bot. We have to do it this way, because
// 1. There are arguments that can not be set in the actual renovate.json
//    config file (cf. https://docs.renovatebot.com/self-hosted-configuration/)
// 2. It is not possible to override the config from the default branch except by ignoring it
//    and then configuring the bot with the local js file. (cf. https://github.com/renovatebot/renovate/discussions/13036)
//    Note that the approach with `baseBranches` + `useBaseBranchConfig` does not work properly.
const fs = require('fs');
const configFilePath = "renovate.json";
const configFileText = fs.readFileSync(configFilePath, "utf8");
module.exports = {
    ...JSON.parse(configFileText)
};
