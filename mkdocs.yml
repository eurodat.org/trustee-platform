site_name: EuroDaT Platform
site_url: !ENV [ SITE_URL, https://eurodat.gitlab.io/trustee-platform ]
repo_url: !ENV [ REPO_URL, https://gitlab.com/eurodat/trustee-platform ]
site_dir: !ENV [ SITE_DIR, public ]
copyright: !ENV [ VERSION_LINE, "0.0.0" ]
extra_css:
  - stylesheets/extra.css
use_directory_urls: !ENV [ USE_DIRECTORY_URLS, true ]
theme:
  name: material
  features:
    - content.code.copy
    - navigation.instant
    - navigation.tracking
    - navigation.tabs
    - navigation.tabs.sticky
    - navigation.indexes
    - navigation.top
    - navigation.expand
  palette:
    - media: "(prefers-color-scheme: light)"
      scheme: eurodat
      primary: eurodat-fg-color--dark
      toggle:
        icon: material/brightness-7
        name: Switch to dark mode
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      primary: eurodat-fg-color--light
      toggle:
        icon: material/brightness-4
        name: Switch to light mode
markdown_extensions:
  - admonition
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.highlight:
      anchor_linenums: true
  - pymdownx.inlinehilite
  - pymdownx.snippets:
      base_path: docs
  - tables
  - attr_list
  - pymdownx.emoji:
      emoji_index: !!python/name:material.extensions.emoji.twemoji
      emoji_generator: !!python/name:material.extensions.emoji.to_svg
plugins:
  - search
  - awesome-pages
  - exclude-search:
      exclude:
        - RELEASE_NOTES_TEMPLATE.md

nav:
  - Home: README.md
  - EuroDaT Platform:
      - eurodat/README.md
      - Set Up Gitlab: eurodat/gitlab.md
      - Run EuroDaT in the Cloud:
          - Register a EuroDaT Client: eurodat/provision/register-eurodat-client.md
          - Generate Client Secret: eurodat/provision/generate-client-secret.md
      - Run EuroDaT on your Local Machine:
          - eurodat/local-development/README.md
          - Build and Deploy Locally: eurodat/local-development/guide-local-development.md
          - Run End-to-End Tests: eurodat/local-development/end-to-end-test.md
      - System Architecture: eurodat/system-architecture.md
      - Frontend: eurodat/frontend.md
  - EuroDaT API Integration:
      - eurodat-api-integration/README.md
      - Usage:
          - eurodat-api-integration/usage/README.md
          - Apps: eurodat-api-integration/usage/app-definition.md
          - Transactions and Workflows: eurodat-api-integration/usage/transactions.md
          - Images: eurodat-api-integration/usage/images.md
          - Workflows: eurodat-api-integration/usage/workflows.md
          - Messaging: eurodat-api-integration/usage/messaging.md
          - Data Management: eurodat-api-integration/usage/data-management.md
  - Tools:
      - tools/README.md
      - Documentation:
          - tools/documentation/README.md
          - OpenAPI and Swagger UI: tools/documentation/openapi_swagger.md
          - MkDocs: tools/documentation/mkdocs.md
      - IAM and Certificates:
          - tools/iam-certificates/README.md
          - Cert/Trust-Manager, Secrets and Certificates Update: tools/iam-certificates/cert-manager_reflector_reloader.md
          - HashiCorp Vault: tools/iam-certificates/hashicorp-vault.md
          - Keycloak: tools/iam-certificates/keycloak.md
      - Cluster:
          - tools/cluster/README.md
          - Docker: tools/cluster/docker.md
          - Helm: tools/cluster/helm.md
      - Workflows and Data:
          - tools/workflows-data/README.md
          - Argo Workflows: tools/workflows-data/argo-workflows.md
          - PostgreSQL: tools/workflows-data/postgresql.md
      - Monitoring:
          - tools/monitoring/README.md
          - Grafana: tools/monitoring/grafana.md
          - Opentelemetry-Collector: tools/monitoring/opentelemetry-collector.md
          - Prometheus: tools/monitoring/prometheus.md
          - Tempo: tools/monitoring/tempo.md
          - Logs: tools/monitoring/logs.md
          - Kotlin-Logging: tools/monitoring/kotlin_logging.md
      - Dev Environment:
          - Images: tools/dev-environment/images.md
      - Static Analysis:
          - tools/static-analysis/README.md
          - Pre-commit hooks: tools/static-analysis/pre-commit_hooks.md
          - Code Quality Checks: tools/static-analysis/static-analysis_sonarcloud.md
  - Release Notes: release_notes.md
